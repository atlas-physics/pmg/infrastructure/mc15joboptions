#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen Wt production (antitop), DR scheme, dilepton, with CT10 and A14_NNPDF23LO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', '2lepton']
evgenConfig.contact     = [ 'timothee.theveneaux-pelzer@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------
#if runArgs.trfSubstepName == 'generate' :

include('PowhegControl/PowhegControl_Wt_DR_Common.py')
PowhegConfig.topdecaymode = 11100 # leptonic W-from-top decays
PowhegConfig.wdecaymode = 11100 # leptonic W decays
PowhegConfig.ttype  = -1 # anti-top
#PowhegConfig.nEvents *= 3.
PowhegConfig.PDF     = 10800
PowhegConfig.mu_F    = 1.0
PowhegConfig.mu_R    = 1.0
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 A14_NNPDF23LO showering + EvtGen
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

# Enable POWHEG LHEF reading in Pythia8
include('MC15JobOptions/Pythia8_Powheg.py')




#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal top mass, A14 tune, all-hadronic boosted'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic' ]
evgenConfig.contact     = [ 'riccardo.di.sipio@cern.ch', 'kcormier@physics.utoronto.ca' ]
 
include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 00022
PowhegConfig.hdamp        = 172.5
PowhegConfig.mu_F         = 1.0
PowhegConfig.mu_R         = 1.0
PowhegConfig.PDF          = 260000
#Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
#PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).
# Weight group configuration
# tested mu_F, muF, mu_R, muR and no change

PowhegConfig.define_event_weight_group( group_name='scales_pdf', parameters_to_vary=['mu_F','mu_R','PDF'] )

# scale variation with NNPDF
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_NNPDF',           parameter_values=[2.0, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_NNPDF',         parameter_values=[0.5, 1.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_NNPDF',           parameter_values=[1.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_NNPDF',         parameter_values=[1.0, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_NNPDF',  parameter_values=[0.5, 0.5, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_NNPDF',      parameter_values=[2.0, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_NNPDF',    parameter_values=[0.5, 2.0, 260000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_NNPDF',    parameter_values=[2.0, 0.5, 260000] )

# scale variation with MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_MMHT',           parameter_values=[2.0, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_MMHT',         parameter_values=[0.5, 1.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_MMHT',           parameter_values=[1.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_MMHT',         parameter_values=[1.0, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_MMHT',  parameter_values=[0.5, 0.5, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_MMHT',      parameter_values=[2.0, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_MMHT',    parameter_values=[0.5, 2.0, 25200] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_MMHT',    parameter_values=[2.0, 0.5, 25200] )

# scale variation with CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_CT14',           parameter_values=[2.0, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_CT14',         parameter_values=[0.5, 1.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_CT14',           parameter_values=[1.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_CT14',         parameter_values=[1.0, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_CT14',  parameter_values=[0.5, 0.5, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_CT14',      parameter_values=[2.0, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_CT14',    parameter_values=[0.5, 2.0, 13165] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_CT14',    parameter_values=[2.0, 0.5, 13165] )

# scale variation with PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_PDF4LHC',           parameter_values=[2.0, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_PDF4LHC',         parameter_values=[0.5, 1.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_PDF4LHC',           parameter_values=[1.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_PDF4LHC',         parameter_values=[1.0, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_PDF4LHC',  parameter_values=[0.5, 0.5, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_PDF4LHC',      parameter_values=[2.0, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_PDF4LHC',    parameter_values=[0.5, 2.0, 90900] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_PDF4LHC',    parameter_values=[2.0, 0.5, 90900] )


# Enhance event generation towards high-pT 
PowhegConfig.bornsuppfact = 2000
PowhegConfig.foldx = 5
PowhegConfig.foldy = 5
PowhegConfig.foldphi = 5

PowhegConfig.generate()
 
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 ttbar with hdamp=1.5*top mass, H7UE tune, only hadronic W decays (no charm), ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.de','daniel.rauch@cern.ch' ]
evgenConfig.generators += ["Powheg"]

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 20 # to recover missing phase space from DSID 410526
PowhegConfig.hdamp   = 258.75
PowhegConfig.PDF     = 260000
PowhegConfig.mu_F    = 1.0
PowhegConfig.mu_R    = 1.0

PowhegConfig.nEvents     *= 1.1 # Safety factor
PowhegConfig.generate()
#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
include('MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py')
  
from Herwig7_i import config as hw
genSeq.Herwig7.Commands += hw.powhegbox_cmds().splitlines()



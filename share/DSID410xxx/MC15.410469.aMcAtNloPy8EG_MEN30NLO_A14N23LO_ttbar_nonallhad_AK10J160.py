#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'MG5_aMC@NLO+Pythia8 ttbar production A14 tune NNPDF23LO EvtGen with at least one lepton filter + at least one AntiKt10TruthJets with pT>160GeV from DSID 410440 LHE files'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'aknue@cern.ch' ]
evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.minevents   = 2000

#if runArgs.trfSubstepName == 'generate' :
#  evgenConfig.inputfilecheck = "aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttbar_incl_LHE"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

include('MC15JobOptions/AntiKt10TruthJets.py')
if not hasattr(filtSeq, "TruthJetFilter"):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()

filtSeq.TruthJetFilter.Njet = -1
filtSeq.TruthJetFilter.NjetMinPt = 160000.
filtSeq.TruthJetFilter.NjetMaxEta = 2.5
filtSeq.TruthJetFilter.jet_pt1 = 160000.
filtSeq.TruthJetFilter.applyDeltaPhiCut = False
filtSeq.TruthJetFilter.MinDeltaPhi = 0.2
filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"

filtSeq.Expression = "TTbarWToLeptonFilter and TruthJetFilter"

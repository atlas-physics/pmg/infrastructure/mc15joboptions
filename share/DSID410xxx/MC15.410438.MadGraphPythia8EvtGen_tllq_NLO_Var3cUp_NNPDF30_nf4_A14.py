evgenConfig.description = 'MG+Pythia8  NLO single top + Z production with Pythia A14 tune'
evgenConfig.keywords    = [ 'SM','singleTop','tZ','lepton','NLO']
evgenConfig.contact     = ['dylan.frizzell@cern.ch' ]

#Common MG_Control file
include("MC15JobOptions/MadGraphControl_tllq_NLO_Pythia8_A14.py")

#Below is showering
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var3cUp_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

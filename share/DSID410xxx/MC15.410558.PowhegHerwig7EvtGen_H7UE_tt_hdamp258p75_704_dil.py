#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig704 ttbar production with Powheg hdamp equal 1.5*top mass, H7UE tune, dilepton filter, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.tune        = "MMHT2014"
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.de','daniel.rauch@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.minevents   = 1000

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------                                                                                                                                                         
# Event filter                                                                                                                                                                                                            
#--------------------------------------------------------------                                                                                                                                                           
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

#
#  These JO take unfiltered Powheg LHE files as input
#  Filter efficiency: 5.3e-3
#  For standard 16,500 event LHE files, 14 files are required per job to get 1000 events
#  CPU time for 1000 events: 8 hours
#  Change to 50 events per file, 20 files to get 1000 events total
#
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass from LHE files with, at least two lepton + m12 m34 filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', '2lepton', '4lepton' ]
evgenConfig.contact     = [ 'jahreda@gmail.com','bnachman@cern.ch','roberto.di.nardo@cern.ch' ,'mdshapiro@lbl.gov']
evgenConfig.minevents   = 50

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]

  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt = 4000.
filtSeq.FourLeptonMassFilter.MaxEta = 4.
filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu = True
filtSeq.FourLeptonMassFilter.AllowSameCharge = True

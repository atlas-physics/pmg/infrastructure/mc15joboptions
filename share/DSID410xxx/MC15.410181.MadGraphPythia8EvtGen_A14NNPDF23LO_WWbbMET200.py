evgenConfig.minevents = 1000
include('MC15JobOptions/MadGraphControl_WWbb_LO_Pythia8_A14_CKKWLkTMerge.py')

include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 200*GeV

evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.description = 'MG5_aMC@NLO+Pythia8 ttbar production with all-hadronic final state with leading top pT > 300 GeV'
evgenConfig.keywords+=['top', 'ttbar']
evgenConfig.contact     = [ 'kyle.james.read.cormier@cern.ch' ]

include("MC15JobOptions/MadGraphControl_ttbar.py")





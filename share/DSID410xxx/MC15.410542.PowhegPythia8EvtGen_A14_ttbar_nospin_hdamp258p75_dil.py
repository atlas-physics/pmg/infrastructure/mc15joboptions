#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, no spin correlations and dilepton filter.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'jhowarth@cern.ch' ]

include('PowhegControl/PowhegControl_tt_Common.py')

# Initial settings
PowhegConfig.topdecaymode = "00000"
PowhegConfig.hdamp        = 258.75
#PowhegConfig.mu_F         = [1.0]
#PowhegConfig.mu_R         = [1.0]
#PowhegConfig.PDF          = [260000]

PowhegConfig.MadSpin_decays= ["decay t > w+ b, w+ > l+ vl", "decay t~ > w- b~, w- > l- vl~"]
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.MadSpin_mode = "none"

# Parameters are hardcoded to avoid madspin picking up it's own defaults.
PowhegConfig.alphaem_inv  = 1.323489e+02
PowhegConfig.G_F          = 1.166370e-05
PowhegConfig.alphaqcd     = 1.184000e-01
PowhegConfig.mass_W       = 80.399
PowhegConfig.mass_Z       = 9.118760e+01
PowhegConfig.mass_H       = 125.0

PowhegConfig.t_Wb_BR = 1.000000
PowhegConfig.t_Ws_BR = 0.00000
PowhegConfig.t_Wd_BR = 0.00000

PowhegConfig.nEvents     *= 1.1 # safety factor
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.





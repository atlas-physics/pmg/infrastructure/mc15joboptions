#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp=1.5*mtop, mtop=174.5, A14 tune, at least one lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch']

from PowhegControl import ATLASCommonParameters
ATLASCommonParameters.mass_t  = 174.5
ATLASCommonParameters.width_t = 1.374

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
PowhegConfig.topdecaymode = 22222       # Inclusive
PowhegConfig.hdamp        = 261.75      # 1.5 * mtop
PowhegConfig.PDF          = 260000      # NNPDF30

PowhegConfig.nEvents     *= 3. # compensate filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

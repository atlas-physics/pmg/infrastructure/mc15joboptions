include("MC15JobOptions/MadGraphControl_ttbb_4F.py")
# --------------------------------------------------------------
# Apply TTbarWToLeptonFilter
# --------------------------------------------------------------
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

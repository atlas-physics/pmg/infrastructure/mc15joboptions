#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, at least two lepton filter and tt+bhadron filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', '2lepton', 'bottom' ]
evgenConfig.contact     = [ 'jahreda@gmail.com' ]

if runArgs.trfSubstepName == 'generate' :
  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 250.
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()
#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
#--------------------------------------------------------------
# Event filter leptons
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
#--------------------------------------------------------------
# Event filter b's
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarPlusHFFilter.py')
  filtSeq.TTbarPlusBFilter.SelectB = True
#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')

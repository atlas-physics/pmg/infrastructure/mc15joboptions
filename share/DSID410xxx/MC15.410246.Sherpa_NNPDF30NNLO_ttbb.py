include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa ttbb + 0,1,2 jets at LO"
evgenConfig.keywords = ["top", "SM" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "alan.james.taylor@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.inputconfcheck = "Sherpa_ttbb"

evgenConfig.process="""
(run){

  %scales, tags for scale variations                        
  FSF:=1.; RSF:=1.; QSF:=1.;                                                                          
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};       

  QCUT:=30.; NJET:=2; LJET:=0;

  % decays
  HARD_DECAYS=1
  STABLE[6]=0
  WIDTH[6]=0
  STABLE[24]=0

}(run)

(processes){
  Process 93 93 -> 6 -6 5 -5 93{NJET}
  CKKW sqr(QCUT/E_CMS)
  Order (*,0); 
  Integration_Error 0.1;
  End process;
}(processes)

(me){
  ME_SIGNAL_GENERATOR = Comix;
}(me)

(selector){
PT 5 15 E_CMS
PT -5 15 E_CMS
Mass 5 -5 30 10000000000
    }(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0" ]


include("MC15JobOptions/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 20000
filtSeq.LeptonFilter.Etacut = 4.5                                     

filtSeq.Expression = "LeptonFilter"

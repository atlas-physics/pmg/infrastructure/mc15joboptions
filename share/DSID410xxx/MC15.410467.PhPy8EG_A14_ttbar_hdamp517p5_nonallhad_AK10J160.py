#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 3*top mass, A14 tune, at least one lepton filter + at least one AntiKt10TruthJets with pT>160GeV, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410451 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'james.robinson@cern.ch','andrea.helen.knue@cern.ch','onofrio@liverpool.ac.uk','ian.connelly@cern.ch','mdshapiro@lbl.gov']
##evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"
evgenConfig.minevents   = 2000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

include('MC15JobOptions/AntiKt10TruthJets.py')
if not hasattr(filtSeq, "TruthJetFilter"):
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter()

filtSeq.TruthJetFilter.Njet = -1
filtSeq.TruthJetFilter.NjetMinPt = 160000.
filtSeq.TruthJetFilter.NjetMaxEta = 2.5
filtSeq.TruthJetFilter.jet_pt1 = 160000.
filtSeq.TruthJetFilter.applyDeltaPhiCut = False
filtSeq.TruthJetFilter.MinDeltaPhi = 0.2
filtSeq.TruthJetFilter.TruthJetContainer = "AntiKt10TruthJets"

filtSeq.Expression = "TTbarWToLeptonFilter and TruthJetFilter"

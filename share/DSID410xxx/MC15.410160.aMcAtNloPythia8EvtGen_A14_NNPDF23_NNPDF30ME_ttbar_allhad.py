evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.description = 'MG5_aMC@NLO+Pythia8 ttbar production'
evgenConfig.keywords+=['top', 'ttbar']
evgenConfig.contact     = [ 'maria.moreno.llacer@cern.ch' ]

if runArgs.trfSubstepName == 'generate' :
  evgenConfig.inputfilecheck = "ttbar_inclusive_NNPDF30_13TeV"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 0 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

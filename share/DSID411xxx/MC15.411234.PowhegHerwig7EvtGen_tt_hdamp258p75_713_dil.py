# based on the JobOptions MC15.429304

# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune           = "H7.1-Default"
evgenConfig.description    = "PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal top mass, H7.1-Default tune, exactly two leptons filter, with EvtGen"
evgenConfig.keywords       = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact        = ['aknue@cern.ch']
evgenConfig.minevents = 1000
evgenConfig.inputfilecheck="TXT"

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

## NonAllHad filter
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

filtSeq.Expression="TTbarWToLeptonFilter"

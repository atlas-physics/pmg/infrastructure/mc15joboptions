#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# Note:  These JO are designed to run Powheg and make an LHE file and to not run a showering
# The configuration is similar to 410450

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, one lepton, ME NNPDF30 NLO, A14 NNPDF23 LO, Shower Weights added, 0.75 mtop'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'ian.connelly@cern.ch','nedaa.asbah@cern.ch']

# This is to fake the system into not spending all its time running Pythia8, since we intend
# to throw out the EVNT file
evgenConfig.minevents   = 1

include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.topdecaymode = 22222
PowhegConfig.hdamp        = 129.375                                       # 0.75 * mtop
PowhegConfig.mu_F         = [1.0, 2.0, 2.0, 2.0, 1.0, 1.0, 0.5, 0.5, 0.5] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 2.0, 1.0, 0.5, 2.0, 0.5, 2.0, 1.0, 0.5] # List of renormalisation scales
# PDF variations with nominal scale variation
PowhegConfig.PDF          = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118 - 

PowhegConfig.nEvents     *= 16500. # we'll request 1 event from Pythia8 just to get the metadata
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.UserModes += [ 'Main31:pTHard = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTdef = 2' ]
genSeq.Pythia8.UserModes += [ 'Main31:veto = 1' ]
genSeq.Pythia8.UserModes += [ 'Main31:vetoCount = 3' ]
genSeq.Pythia8.UserModes += [ 'Main31:pTemt  = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:emitted = 0' ]
genSeq.Pythia8.UserModes += [ 'Main31:MPIveto = 0' ]


#Override the generator names since we won't keep the Pythia8
evgenConfig.generators = ["Powheg"]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen single-top-quark t-channel (2->3) production (top),MadSpin, A14 tune, ME NNPDF3.04f NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch']
evgenConfig.minevents   = 500

include('PowhegControl/PowhegControl_t_tch_4FS_Common.py')

PowhegConfig.decay_mode   = "t~ > undecayed"

PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [260400, 25410, 13191, 92000]                 # NNPDF30, MMHT, CT14, PDF4LHC - PDF variations with nominal scale variation
PowhegConfig.PDF.extend(range(260401, 260501))                            # Include the NNPDF error set
PowhegConfig.PDF.extend(range(92001 , 92031 ))                            # Include the PDF4LHC error set

# Information on how to run with multiple weights: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PowhegForATLAS#Running_with_multiple_scale_PDF
# PDFs - you can see a listing here: https://lhapdf.hepforge.org/pdfsets.html; picked these three as they are the inputs to the PDF4LHC2015 prescription (http://arxiv.org/pdf/1510.03865v2.pdf).

# Define a weight group configuration for scale variations with different PDFs
# Nominal mu_F = mu_R = 1.0 is not required as this is captured by the PDF variation above
PowhegConfig.define_event_weight_group( group_name='scales_pdf', parameters_to_vary=['mu_F','mu_R','PDF'] )

# Scale variations, MMHT2014nlo68clas118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_MMHT',                   parameter_values=[ 2.0, 1.0, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_MMHT',                 parameter_values=[ 0.5, 1.0, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_MMHT',                   parameter_values=[ 1.0, 2.0, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_MMHT',                 parameter_values=[ 1.0, 0.5, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_MMHT',          parameter_values=[ 0.5, 0.5, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_MMHT',              parameter_values=[ 2.0, 2.0, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_MMHT',            parameter_values=[ 0.5, 2.0, 25410] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_MMHT',            parameter_values=[ 2.0, 0.5, 25410] )

# Scale variations, CT14nlo_as_0118
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_CT14',                   parameter_values=[ 2.0, 1.0, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_CT14',                 parameter_values=[ 0.5, 1.0, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_CT14',                   parameter_values=[ 1.0, 2.0, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_CT14',                 parameter_values=[ 1.0, 0.5, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_CT14',          parameter_values=[ 0.5, 0.5, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_CT14',              parameter_values=[ 2.0, 2.0, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_CT14',            parameter_values=[ 0.5, 2.0, 13191] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_CT14',            parameter_values=[ 2.0, 0.5, 13191] )

# Scale variations, PDF4LHC15_nlo_30
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_PDF4LHC15',              parameter_values=[ 2.0, 1.0, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_PDF4LHC15',            parameter_values=[ 0.5, 1.0, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muR_PDF4LHC15',              parameter_values=[ 1.0, 2.0, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muR_PDF4LHC15',            parameter_values=[ 1.0, 0.5, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_0p5muR_PDF4LHC15',     parameter_values=[ 0.5, 0.5, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_2muR_PDF4LHC15',         parameter_values=[ 2.0, 2.0, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='0p5muF_2muR_PDF4LHC15',       parameter_values=[ 0.5, 2.0, 92000] )
PowhegConfig.add_weight_to_group( group_name='scales_pdf', weight_name='2muF_0p5muR_PDF4LHC15',       parameter_values=[ 2.0, 0.5, 92000] )

PowhegConfig.nEvents *= 1.1 # Safety factor

PowhegConfig.MadSpin_decays= ["decay t~ > w- b~, w- > l- vl~"]
PowhegConfig.MadSpin_process = "generate p p > t~ b j $$ w+ w- [QCD]" 


# Parameters are hardcoded to avoid madspin picking up it's own defaults.
PowhegConfig.alphaem_inv  = 1.323489e+02
PowhegConfig.G_F          = 1.166370e-05
PowhegConfig.alphaqcd     = 1.184000e-01
PowhegConfig.mass_W       = 80.399
PowhegConfig.mass_Z       = 9.118760e+01
PowhegConfig.mass_H       = 125.0

## varied top mass
PowhegConfig.mass_t  = 172.25
PowhegConfig.width_t = 1.313

# use diagonal CKM matrix
PowhegConfig.CKM_Vud = 1.0
PowhegConfig.CKM_Vus = 0.000001
PowhegConfig.CKM_Vub = 0.000001
PowhegConfig.CKM_Vcd = 0.000001
PowhegConfig.CKM_Vcs = 1.0
PowhegConfig.CKM_Vcb = 0.000001
PowhegConfig.CKM_Vtd = 0.000001
PowhegConfig.CKM_Vts = 0.000001
PowhegConfig.CKM_Vtb = 1.0

# use BWcutoff of 50 to be consistent with ttbar
PowhegConfig.bwcutoff     = 50

# Initial settings
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]



#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'MEtop+Herwig7 FCNC single top production ug->t with H7UE tune'
evgenConfig.keywords    = [ 'FCNC', 'top', 'lepton']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch' ]
evgenConfig.generators += ['MEtop','Herwig7']
evgenConfig.tune        = "H7UE"

evgenConfig.inputfilecheck = "ugt"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
""")

# run Herwig7
Herwig7Config.run()







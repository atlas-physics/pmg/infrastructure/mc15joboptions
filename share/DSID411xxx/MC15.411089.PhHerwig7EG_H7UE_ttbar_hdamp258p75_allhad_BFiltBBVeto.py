evgenConfig.description = 'POWHEG+Herwig7.0.4 ttbar production with Powheg hdamp equal 1.5*top mass, H7UE tune, zero lepton filter, B filter and BB veto, ME NNPDF30 NLO, H7UE MMHT2014 LO from DSID 410450 LHE files'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'tpelzer@cern.ch']
evgenConfig.tune        = 'MMHT2014'
evgenConfig.generators += [ 'Powheg', 'Herwig7', 'EvtGen']
evgenConfig.minevents   = 5000
#evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"

#--------------------------------------------------------------
# allhad filter
# This is a filter on the input LHE files, applied before any other algorithm is processed
#--------------------------------------------------------------
include('MC15JobOptions/LHEFilter.py')
include('MC15JobOptions/LHEFilter_NLeptons.py')

nleptonFilter = LHEFilter_NLeptons()
nleptonFilter.NumLeptons = 0
nleptonFilter.Ptcut = 0.
lheFilters.addFilter(nleptonFilter)

lheFilters.run_filters()

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# B filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBFilter.py')

#--------------------------------------------------------------
# BB filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBBFilter.py')

# Combine the filters
filtSeq.Expression = "(TTbarPlusBFilter) and (not TTbarPlusBBFilter)"

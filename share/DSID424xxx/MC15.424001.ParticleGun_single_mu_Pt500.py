evgenConfig.description = "Single mu with flat phi, eta in [-3.0, 3.0], and pT = 500 GeV"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=500000, eta=[-3.0, 3.0])

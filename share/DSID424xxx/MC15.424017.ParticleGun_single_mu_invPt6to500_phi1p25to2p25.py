evgenConfig.description = "Single mu with flat in: 1/pT for [1/6, 1/500] 1/GeV, phi for [1.25, 2.25], eta for [-2.7, 2.7], phi for [1.25,2.25]"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.InvSampler(6000, 500000), eta=[-2.7,2.7], phi=[1.25,2.25])


#################################################################################
# job options fragment for pp->Jpsi->mu15mu2p5 for trigger study for run2
#################################################################################

include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description  = "pp->Jpsi->mu15mu2p5 production"
evgenConfig.keywords     = [ "charmonium", "muon", "SM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "pp>Jpsi>mumu"

include("MC15JobOptions/Pythia8B_Charmonium_Common.py")
include("MC15JobOptions/nonStandard/Pythia8B_Photospp.py")

genSeq.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.Commands += [ '443:onMode = off' ]
genSeq.Pythia8B.Commands += [ '443:2:onMode = on' ]
genSeq.Pythia8B.SignalPDGCodes = [ 443, -13, 13 ]

genSeq.Pythia8B.TriggerPDGCode      = 13
genSeq.Pythia8B.TriggerStateEtaCut  = 2.7
genSeq.Pythia8B.TriggerStatePtCut   = [ 2.5, 15 ]
genSeq.Pythia8B.MinimumCountPerCut  = [   2,  1 ]

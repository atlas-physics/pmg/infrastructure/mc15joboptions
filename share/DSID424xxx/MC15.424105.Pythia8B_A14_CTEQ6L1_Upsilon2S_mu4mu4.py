#--------------------------------------------------------------
# JO  fragment for pp->Upsilon2S(mu4mu4) production with 
#     Pythia8B and Photos for FSR
#--------------------------------------------------------------

evgenConfig.description = "Inclusive pp->Upsilon2S(mu4mu4) production with Photos"
evgenConfig.keywords = ["bottomonium","2muon","inclusive"]
evgenConfig.minevents = 5000

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Bottomonium_Common.py")

genSeq.Pythia8B.Commands += ['100553:onMode = off']
genSeq.Pythia8B.Commands += ['100553:1:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [100553,-13,13]

include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [4.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2]

##
## 11.0GeV leptophilic Zprime from pp --> 2mu+Zp --> 4mu
##
zpm=11.0
gzpmul=9.500000e-03
include("MC15JobOptions/MGCtrl_Py8EG_NNPDF30nlo_Leptophilic_2muZp_4mu.py")

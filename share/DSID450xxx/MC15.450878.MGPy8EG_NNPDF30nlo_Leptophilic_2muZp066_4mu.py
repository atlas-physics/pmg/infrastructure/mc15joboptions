##
## 66.0GeV leptophilic Zprime from pp --> 2mu+Zp --> 4mu
##
zpm=66.0
gzpmul=4.000000e-01
include("MC15JobOptions/MGCtrl_Py8EG_NNPDF30nlo_Leptophilic_2muZp_4mu.py")

include("MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")


#--------------------------------------------------------------
# Higgs at Pythia 8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [  '25:onMode = off', #decay of higgs
                              '25:addChannel = 1 0.002 100 443 22',
                              '443:onMode = off',
                              '443:onIfMatch = 13 -13',
                           ]
evgenConfig.process     = 'ttH H->JPsi Gamma'
evgenConfig.description = 'aMcAtNloPythia8 ttH dilep, H to JPsiGamma'
evgenConfig.keywords    = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs', 'photon' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.contact = ["Will Heidorn <william.dale.heidorn@cern.ch>"]

#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
""")

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
if (runArgs.runNumber == 450000):
    evgenConfig.description = "SM diHiggs production, decay to 4b, with MG5_aMC@NLO, inclusive of box diagram FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "bbbar"]
    evgenConfig.minevents = 5000

evgenConfig.contact = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 

# run Herwig7
Herwig7Config.run()

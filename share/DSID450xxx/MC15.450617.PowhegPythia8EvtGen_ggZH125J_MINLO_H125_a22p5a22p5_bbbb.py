#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

if runArgs.trfSubstepName == 'generate' :
  evgenConfig.inputfilecheck = "TXT."

import os, sys, glob
lhefiles = glob.glob('*.events')
if len(lhefiles)>0:
    infile = lhefiles[0]
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        print line
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2']
#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
H_Mass = 125.0
H_Width = 0.00407
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 36 36', # h->aa

                             '36:onMode = off', # decay of the a
                             '36:onIfAny = 5', # decay a->bb
                             '36:m0 22.5', #scalar mass
                             '36:mMin 19.975',
                             '36:tau0 0', #scalar mass
                             ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->H+Z->l+l-b+b-b+b- production"
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125" ]
evgenConfig.contact     = [ 'roger.caminal@cern.ch' ]
evgenConfig.generators += ["Powheg","Pythia8"]
evgenConfig.minevents = 5000

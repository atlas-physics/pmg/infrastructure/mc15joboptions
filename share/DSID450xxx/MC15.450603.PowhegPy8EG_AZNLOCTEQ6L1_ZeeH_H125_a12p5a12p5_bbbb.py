#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+W+jet->mu-vmubbar production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HZj_Common.py')

PowhegConfig.ncall1 = 20000
PowhegConfig.ncall2 = 10000

PowhegConfig.runningscales = 1 # CHECK
PowhegConfig.vdecaymode = 1 # Z->e+e-
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 120 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 200
#PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
#PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations
#PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
#PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.nEvents *= 2.2

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()


#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
H_Mass = 125.0
H_Width = 0.00407
genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:onIfMatch = 36 36', # h->aa

                             '36:onMode = off', # decay of the a
                             '36:onIfAny = 5', # decay a->bb
                             '36:m0 12.5', #scalar mass 
                             '36:mMin 19.975',
                             '36:tau0 0', #scalar mass 
                             ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = [ 'roger.caminal@cern.ch' ]

evgenConfig.minevents=5000

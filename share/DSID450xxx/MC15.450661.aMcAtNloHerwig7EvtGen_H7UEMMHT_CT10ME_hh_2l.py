#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwig7", "EvtGen"]
if (runArgs.runNumber == 450661):
  evgenConfig.description = "SM diHiggs production, decay to multi-lepton, with MG5_aMC@NLO, inclusive of box diagram FF."
  evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "multilepton"]
evgenConfig.contact = ['Xiaozhong Huang <xiaozhong.huang@cern.ch>']
#evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIG7_CT10' 
evgenConfig.minevents   = 5000
evgenConfig.tune = "H7-UE-MMHT"

#--------------------------------------------------------------
# Herwig 7 showering setup                                    
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop No")
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->W+,W-; h0->Z0,Z0;
do /Herwig/Particles/h0:PrintDecayModes
""")


# #---------------------------------------------------------------------------------------------------
# # Generator Filters
# #---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepTwoFilter")
filtSeq.LepTwoFilter.IncludeHadTaus = True
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.MinPt = 7000.
filtSeq.LepTwoFilter.MinVisPtHadTau = 15000.
filtSeq.LepTwoFilter.MaxEta = 3.

filtSeq += MultiElecMuTauFilter("LepThreeFilter")
filtSeq.LepThreeFilter.IncludeHadTaus = True
filtSeq.LepThreeFilter.NLeptons = 3
filtSeq.LepThreeFilter.MinPt = 7000.
filtSeq.LepThreeFilter.MinVisPtHadTau = 15000.
filtSeq.LepThreeFilter.MaxEta = 3.

filtSeq.Expression="LepTwoFilter and not LepThreeFilter"

# run Herwig7
Herwig7Config.run()

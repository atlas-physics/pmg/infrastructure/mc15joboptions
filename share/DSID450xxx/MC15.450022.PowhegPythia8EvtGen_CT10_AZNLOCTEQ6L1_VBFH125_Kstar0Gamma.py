#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 125.0
PowhegConfig.width_H = 0.00407

# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 1

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
# PowhegConfig.nEvents *= 4.

# Generate Powheg events
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.UserModes += [ 'WeakZ0:gmZmode = 2']
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 313 22',
                             '313:mMax = 5' 
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.minevents=2000
evgenConfig.process     = "VBF H->Kstar0Gamma"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H(125)->Kstar0Gamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","VBF","photon", "mH125" ]
evgenConfig.contact = ["Rhys Owen <rhys.owen@cern.ch>"]
evgenConfig.generators += [ "Powheg", "Pythia8", "EvtGen"]

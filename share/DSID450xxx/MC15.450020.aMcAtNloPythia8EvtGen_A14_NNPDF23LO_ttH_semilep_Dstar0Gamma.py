include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:addChannel = 1 0.002 100 423 22',
                             '25:addChannel = 1 0.002 100 -423 22',
                             '421:onMode = off',
                             '421:onIfMatch = -321 211',
                             '-421:onMode = off',
                             '-421:onIfMatch = 321 -211',
                             ]

evgenConfig.process        = 'ttH H->DStar0Gamma'
evgenConfig.description    = 'aMcAtNloPythia8 ttH semilep, H to DStar0'
evgenConfig.keywords       = [ 'SM', 'Higgs', 'SMHiggs', 'mH125', 'ttHiggs','photon' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.contact = ["Rhys Owen <rhys.owen@cern.ch>"]

#Author: Will Buttinger <will@cern.ch>

#Usage instructions: (see e.g. https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MC15JobOptions/trunk/common/MadGraph/MadGraphControl_HAHM.py for an example usage)
#specify the following with this fragment:
#proc_name : the name of the process - will match the name of the process directory ... if not specified, we will take it from the 'output' line of the proc_card string
#proc_card : the proc_card string (with or without including the output line)
#param_card : the LOCATION of the param card to use (can be retrieved with get_files)
#param_card_extras : dictionary of dictionaries
#run_card : the LOCATION of the param card to use (can be retrieved with get_files)
#run_card_extras: dictionary ... should not specify pdlabel and lhaid, as those are provided in this fragment
#save_proc_dir: bool ... if true, then wont delete the process directory (given by the proc_name)
#you can also specify a post_lhe_hook method to call code after the lhe making
#to specifically support long-lived particles, this joboption provides a method to lifetimes to particles
#e.g. you can do
#def post_lhe_hook():
#  add_lifetimes(pdgId=32,avgtau=2000)



proc_card = """
    import model ./Type-II_seesaw_mix_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define fermions = u c d s u~ c~ d~ s~ e+ e- mu+ mu- ta+ ta- ve vm vt 
    define vl~ = ve vm vt
    define EXCL = H h ha 
    generate p p  > H++ H-- / EXCL """

param_card_extras = {
   'mass': {
   'mh__2':'400',
   'mha':'400',
   'mhp':'400',
   'mHpp':'200'}}

#Fetch default LO run_card.dat and set parameters
run_card_extras = { 'lhe_version':'3.0',
           'cut_decays':'F',
           'use_syst':"False"
           }

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2. 
else: 
    raise RuntimeError("No center of mass energy found.")
  

evgenConfig.description = 'MadGraph and madspin HppHmm4W production with 2LSS filter'
evgenConfig.keywords+=['exotic','BSM']
evgenConfig.generators +=["MadGraph", "Pythia8"]
evgenConfig.contact += ['Hanlin Xu <hanlin.xu@cern.ch>']


filtereff=0.10
safefactor=1.2/filtereff

def add_lifetimes(pdgId, avgtau):
    #first ensure the lifetimes wont cause problems with TestHepMC
    #relax the cuts on displaced vertices and non G4 particles
    print("Updating TestHepMC to allow for displaced vertices")
    testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
    testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
    testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV
    import random
    random.seed(runArgs.randomSeed)
    #lifetime function
    def lifetime(myavgtau = 21):
        import math
        t = random.random()
        return -1.0 * myavgtau * math.log(t)
    rname = 'Test' #comes from the common joboptions
    if os.path.isfile(process_dir+'/Events/'+rname+'/unweighted_events.lhe'):
           os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')
    import subprocess
    unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
    unzip1.wait()
    
    oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
    newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
    init = True
    for line in oldlhe:
        if init==True:
            newlhe.write(line)
            if '</init>' in line:
                init = False
        else:  
            if 'vent' in line:
                newlhe.write(line)
                continue
            newline = line.rstrip('\n')
            columns = newline.split()
            pdgid = columns[0]
            if pdgid == str(pdgId):
                #replace second to last column with the new lifetime
                columns[len(columns)-2] = "%.11E" % (lifetime(avgtau))
                newlhe.write(" ".join(columns) + "\n")           
            else:
                newlhe.write(line)
    
    oldlhe.close()
    newlhe.close()
    zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
    zip1.wait()
    import shutil
    shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
    os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')


from MadGraphControl import MadGraphUtils

#check the variables, adding bits if necessary:
try:
   proc_card
except NameError:
   raise RuntimeError("You must specify a proc_card string")

try:
   proc_name
except NameError:
   proc_name = ""

hasOutputLine=False
for line in proc_card.splitlines():
   if "output" in line:
      #must be a word on its own
      ss = line.split(" ")
      if "output" not in ss: continue
      hasOutputLine=True
      #look for the word after output ... that is the proc_name
      ss = line.split(" ")
      if proc_name == "": proc_name = ss[ss.index("output")+1]
      elif ss[ss.index("output")+1] != proc_name:
         raise RuntimeError("proc_name (you gave %s) must match that given in proc_card's output line (which is %s)" % (proc_name,ss[ss.index("output")+1]))


if not hasOutputLine:
   proc_card += "\noutput %s -f" % proc_name

try:
   run_card
except NameError:
   print("No run_card given, will use the default one for this process (LO: $MADPATH/Template/LO/Cards/run_card.dat)");
   run_card = ""

try:
   run_card_extras
except NameError:
   run_card_extras = {}

try:
   save_proc_dir
except NameError:
   save_proc_dir=False


#modify the run_card_extras 
if 'pdlabel' in run_card_extras:
   raise RuntimeError("Cannot specify a pdlabel in run_card_extras, as this is set for you")

if 'lhaid' in run_card_extras:
   raise RuntimeError("Cannot specify a lhaid in run_card_extras, as this is set for you")

run_card_extras['pdlabel'] = 'lhapdf'
run_card_extras['lhaid'] = '260000'#NNPDF30_nlo_as_0118




try:
  safefactor
except(NameError):
   safefactor=1.1 #generate extra 10% events in case any fail showering


nevents = runArgs.maxEvents*safefactor

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
   beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")

#make the proc card
fcard = open('proc_card_mg5.dat','w')
fcard.write(proc_card)
fcard.close()

#does mg5 to determine all the diagrams etc ... generates the process directory corresponding to proc_name
process_dir = MadGraphUtils.new_process()

if proc_name != "" and process_dir != proc_name:
   raise RuntimeError("Unexpected process_dir %s when expected %s" % (process_dir,proc_name))


if( MadGraphUtils.build_param_card(param_card_old='HppHmm4W_param_card.dat',param_card_new='new_card.dat',params=param_card_extras) == -1):
   raise RuntimeError("Could not create param_card.dat")


#create the run card: FIXME: Should check for success
if os.access('run_card.dat',os.R_OK): 
   print("Deleting old run_card.dat")
   os.remove('run_card.dat')
if run_card == "": run_card = MadGraphUtils.get_default_runcard(proc_dir=process_dir)
if MadGraphUtils.build_run_card(run_card_old=run_card,run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=run_card_extras)==-1:
   raise RuntimeError("Could not create run_card.dat")


#create the madspin card: FIXME: Should check for success

madspin_card_loc='madspin_card.dat'                                                                                                                                    
if os.access('madspin_card.dat',os.R_OK):
   print("Deleting old madspin_card.dat")
   os.remove('madspin_card.dat')


mscard = open(madspin_card_loc,'w')                                                                                                                                    
mscard.write("""
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay H++ > w+ w+, w+ > fermions fermions
decay H-- >  w- w- , w- > fermions fermions
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

MadGraphUtils.generate(run_card_loc='run_card.dat',param_card_loc='new_card.dat',mode=0,njobs=1,run_name='run_01',proc_dir=process_dir,madspin_card_loc=madspin_card_loc)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(proc_name)

if "post_lhe_hook" in dir():
   print("Calling post_lhe_hook....")
   post_lhe_hook()


outputDS = MadGraphUtils.arrange_output(run_name='run_01',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',saveProcDir=save_proc_dir)


#MadGraphUtils.arrange_output(run_name='run_01',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',saveProcDir=save_proc_dir)

if 'ATHENA_PROC_NUMBER' in os.environ:
   njobs = os.environ.pop('ATHENA_PROC_NUMBER')
   if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
   else: opts.nprocs = 0

#evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile=outputDS

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


#genSeq.Pythia8.Commands += [ '9000055:all = H++ H-- 1 6 0 200 6.962315e-07 0.0 0.0 0.0',  '9000055:addChannel = 1 1 0 24 24' ]


#SS2LFilter

evgenLog.info('Using LeptonPairFilter accepting SS lepton pairs with massive parents')
from GeneratorFilters.GeneratorFiltersConf import LeptonPairFilter
filtSeq += LeptonPairFilter('SS3LFilter')
myFilter = filtSeq.SS3LFilter
myFilter.NLeptons_Min = 0
myFilter.NLeptons_Max = 2 # NB: filter is always passed if nLeptons > nLeptons_Max
myFilter.Ptcut = 10000
myFilter.Etacut = 10.0
myFilter.NSFOS_Min = -1
myFilter.NSFOS_Max = -1
myFilter.NOFOS_Min = -1
myFilter.NOFOS_Max = -1
myFilter.NSFSS_Min = -1
myFilter.NSFSS_Max = -1
myFilter.NOFSS_Min = -1
myFilter.NOFSS_Max = -1
myFilter.NPairSum_Max = -1
myFilter.NPairSum_Min = 1
myFilter.UseSFOSInSum = False
myFilter.UseSFSSInSum = True
myFilter.UseOFOSInSum = False
myFilter.UseOFSSInSum = True
myFilter.OnlyMassiveParents = True













#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, allhadronic, 1.1TeV<Mtt<1.3TeV '
evgenConfig.process     = 'SM ttbar'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'allHadronic']
evgenConfig.contact     = [ 'jiahang.zhong@cern.ch' ]
evgenConfig.minevents      = 2000   

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 400
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = 0
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

  include('MC15JobOptions/MassRangeFilter.py')
  filtSeq.MassRangeFilter.PartId    = 6
  filtSeq.MassRangeFilter.PartId2   = 6
  filtSeq.MassRangeFilter.PartStatus= 3
  filtSeq.MassRangeFilter.InvMassMin= 1100000.
  filtSeq.MassRangeFilter.InvMassMax= 1300000.

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')


evgenConfig.description = "Quantum black holes (n = 1, M_th = 5.0 TeV) decaying to electron+tauon."
evgenConfig.process = "QBH -> e+- tau-+"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions", "RandallSundrum", "warpedED"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_etau"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "23:onMode = off",                  # turn off all decays modes
                            "23:onIfAny = 15",                  # turn on the tautau decay mode
                            "PhaseSpace:mHatMin = 250.",       # lower invariant mass
                            "PhaseSpace:mHatMax = 400."]       # upper invariant mass

evgenConfig.description = "Pythia 8 DY->tautau production with NNPDF23LO tune"
evgenConfig.contact = ["Will Davey <will.davey@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2tau"]
evgenConfig.generators += ["Pythia8"]


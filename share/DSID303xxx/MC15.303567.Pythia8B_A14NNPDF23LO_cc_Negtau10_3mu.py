#################################################################################
# job options fragment for cc->XNegtau10->3mu for HF tau3mu study for run2
#################################################################################

include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
evgenConfig.description  = "cc->XNegtau10->3mu production"
evgenConfig.keywords     = [ "charmonium", "tau", "muon", "BSM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "pp>cc>tauX>3muX"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.SelectBQuarks      = False
genSeq.Pythia8B.SelectCQuarks      = True
genSeq.Pythia8B.QuarkPtCut         = 10.0
genSeq.Pythia8B.AntiQuarkPtCut     = 10.0
genSeq.Pythia8B.QuarkEtaCut        = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut    = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents  = True

genSeq.Pythia8B.NHadronizationLoops = 10

genSeq.Pythia8B.Commands += [ ' 15:onMode = 3' ]
genSeq.Pythia8B.Commands += [ ' 15:addChannel = 2 1.0 0 13 -13 13' ]

genSeq.Pythia8B.SignalPDGCodes  = [ 15,13,-13,13 ]
genSeq.Pythia8B.SignalPtCuts    = [ 10.0,0.0,0.0,0.0 ] # only for tau pt
genSeq.Pythia8B.SignalEtaCuts   = [ 102.5,102.5,102.5,102.5 ] # default

genSeq.Pythia8B.BPDGCodes = [511,521,531,541,5122,5132,5232,5332,-511,-521,-531,-541,-5122,-5132,-5232,-5332]

genSeq.Pythia8B.TriggerPDGCode        = 15
genSeq.Pythia8B.TriggerStateEtaCut    = 102.5 # default
genSeq.Pythia8B.TriggerStatePtCut     = [10.0]
genSeq.Pythia8B.MinimumCountPerCut    = [1]

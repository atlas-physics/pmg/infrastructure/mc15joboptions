evgenConfig.description = "Quantum black holes (n = 6, M_th = 9.5 TeV) decaying to electron+quark."
evgenConfig.process = "QBH -> e + q"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_ejet"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")

# Increase tolerance on displaced vertieces due to highly boosted heavy flavours.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000 #in mm

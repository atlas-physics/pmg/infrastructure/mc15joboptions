#################################################################################
# job options fragment for W^{+}->nutau->3mu for run2
#################################################################################

evgenConfig.description  = "PosW->nutau->3mu production"
evgenConfig.keywords     = [ "W", "muon", "tau", "BSM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "pp>W>taunu>3munu"

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on",
                            "24:onMode = 3",
                            "24:onIfAny = 15",
                            "15:onMode = 2",
                            "15:addChannel = 3 1.0 0 13 -13 13"
]

#Add the Filters:
from GeneratorFilters.GeneratorFiltersConf import TrimuMassRangeFilter
filtSeq += TrimuMassRangeFilter()

TrimuMassRangeFilter = filtSeq.TrimuMassRangeFilter
TrimuMassRangeFilter.EtaCut1 = 102.5 # default
TrimuMassRangeFilter.EtaCut2 = 102.5 # default
TrimuMassRangeFilter.EtaCut3 = 102.5 # default
TrimuMassRangeFilter.PartId1 = -13
TrimuMassRangeFilter.PartId2 = 13
TrimuMassRangeFilter.PartId3 = -13
TrimuMassRangeFilter.PtCut1  = 0
TrimuMassRangeFilter.PtCut2  = 0
TrimuMassRangeFilter.PtCut3  = 0
TrimuMassRangeFilter.InvMassMin  = 0
TrimuMassRangeFilter.InvMassMax  = 100000


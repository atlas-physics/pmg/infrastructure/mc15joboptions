include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Lepton-neutrino Mass-Binned TTbar Sample muvqq 5000M99999"
evgenConfig.keywords = ["top", "lepton", "neutrino", "2jet", "SM" ]
evgenConfig.contact  = [ "oleksandr.viazlo@cern.ch", "daniel.hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 2000



evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE=DEFAULT;
  EXCLUSIVE_CLUSTER_MODE 1;

  MASSIVE[15]=1
  NJET:=2;
}(run)

(processes){
  # l nu q qbar b bbar; W+ -> enu, W- -> qq
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -13 14
  Decay -24[d] -> 94 94
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

  # l nu q qbar b bbar; W+ -> qq, W- -> enu
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> 94 94
  Decay -24[d] -> 13 -14
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

}(processes)

(selector){
  "PT" 90 1.0,E_CMS:1.0,E_CMS [PT_UP]
  "PT" 91 1.0,E_CMS:1.0,E_CMS [PT_UP]
  "PT" 94 1.0,E_CMS:1.0,E_CMS [PT_UP]
  Mass 94 94 0.2500 E_CMS
  Mass 90 91 5000 E_CMS
}(selector)
"""

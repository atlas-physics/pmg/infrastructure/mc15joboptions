model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 1030.
mZp = 1000.
mHD = 125.
widthZp = 4.770293e+00
widthN2 = 6.037583e-01
filteff = 8.756567e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

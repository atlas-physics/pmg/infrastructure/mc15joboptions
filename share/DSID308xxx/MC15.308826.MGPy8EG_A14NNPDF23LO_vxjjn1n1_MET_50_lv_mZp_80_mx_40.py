model="LightVector"
mDM1 = 40.
mDM2 = 160.
mZp = 80.
mHD = 125.
widthZp = 3.183053e-01
widthN2 = 5.255678e+00
filteff = 6.075334E-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

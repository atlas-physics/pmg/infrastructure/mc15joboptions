model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 800.
mHD = 800.
widthZp = 3.810994e+00
widthhd = 3.182850e-01
filteff = 9.900990e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

# W' -> WZ' -> lnuqq                                                                                                                                                                
# Wprime Mass (in GeV)                                                                                                                                                                                     

mass_w = 3250
mass_z = 3000

print "mass w: ", mass_w
print "mass z: ", mass_z

#M_Wprime = mass_w
#M_Z = mass_z

evgenConfig.contact = ["Alex Martyniuk, martyniu@cern.ch"]
evgenConfig.description = "Wprime->WZprime->lnuqq 400GeV with NNPDF23LO PDF and A14 tune"
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>Wprime>WZprime>lnuqq"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]# Allow Wprime               
#genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]# Allow Zprime 				                                                         
genSeq.Pythia8.Commands += ["34:m0 = 3250."] # W' mass
genSeq.Pythia8.Commands += ["34:onMode = off"]# Turn off W' decays       
genSeq.Pythia8.Commands += ["34:addChannel = 1 0.5 103 24 32"]# add channel W'->WZ' 	                                                           
genSeq.Pythia8.Commands += ["32:m0 = 3000."] # Z' mass                                             
genSeq.Pythia8.Commands += ["32:onMode = off"]# Turn off all Z' decays
genSeq.Pythia8.Commands += ["32:onIfAny = 1 2 3 4 5"]# Turn on hadronic Z'                                                                                                                                                                                                                                                                                      
genSeq.Pythia8.Commands += ["24:onMode = off"]# Turn off all W decays                                                                                                                                      
                                                                                                                                                                                                                                                                       
genSeq.Pythia8.Commands += ["24:onIfAny = 11 13 15"]# switch on leptonic W decays
genSeq.Pythia8.Commands += ["Init:showAllParticleData = on"]
genSeq.Pythia8.Commands += ["Next:numberShowEvent = 5"]    


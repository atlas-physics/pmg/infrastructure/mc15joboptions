model       = 'dmA'
mR          = 750
mDM         = 10000
gSM         = 0.20
gDM         = 1.00
widthR      = 13.601690
phminpt     = 50
filteff     = 0.30200

evgenConfig.description = "Zprime sample - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>", "Chase Shimmin <cshimmin@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")

model="LightVector"
mDM1 = 70.
mDM2 = 280.
mZp = 140.
mHD = 125.
widthZp = 5.570415e-01
widthN2 = 9.197437e+00
filteff = 8.319468e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

model="InelasticVectorEFT"
mDM1 = 475.
mDM2 = 1900.
mZp = 950.
mHD = 125.
widthZp = 4.530812e+00
widthN2 = 3.124138e+00
filteff = 9.98004e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

model="LightVector"
mDM1 = 75.
mDM2 = 300.
mZp = 150.
mHD = 125.
widthZp = 5.968303e-01
widthN2 = 9.854397e+00
filteff = 8.389262e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

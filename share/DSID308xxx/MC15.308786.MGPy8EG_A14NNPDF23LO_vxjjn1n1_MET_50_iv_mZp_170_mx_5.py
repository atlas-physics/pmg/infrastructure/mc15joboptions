model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 200.
mZp = 170.
mHD = 125.
widthZp = 6.764080e-01
widthN2 = 4.343147e-03
filteff = 5.246590e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 900.
mHD = 125.
widthZp = 4.291147e+00
widthhd = 2.570046e+00
filteff = 8.827684e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

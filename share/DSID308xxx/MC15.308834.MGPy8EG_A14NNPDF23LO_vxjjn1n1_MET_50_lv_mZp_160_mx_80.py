model="LightVector"
mDM1 = 80.
mDM2 = 320.
mZp = 160.
mHD = 125.
widthZp = 6.366192e-01
widthN2 = 1.051136e+01
filteff = 8.620690e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

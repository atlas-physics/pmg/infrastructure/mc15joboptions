model="InelasticVectorEFT"
mDM1 = 65.
mDM2 = 260.
mZp = 130.
mHD = 125.
widthZp = 5.172525e-01
widthN2 = 8.091358e-03
filteff = 8.305648e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

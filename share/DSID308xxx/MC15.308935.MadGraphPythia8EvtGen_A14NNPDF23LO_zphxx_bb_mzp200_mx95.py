evt_multiplier=5
include("MC15JobOptions/MadGraphControl_monoHiggs_zprime.py")

evgenConfig.description = "Simplified Model of vector mediator for MonoHiggs(h->bb) with mDM="+str(mDM)+"GeV and mZp="+str(mZp)+"GeV"
evgenConfig.process = "pp -> Zprime -> Zprime + Higgs -> bbXX"
evgenConfig.keywords = ["BSM", "BSMHiggs", "Higgs", "bbbar", "Zprime", "simplifiedModel"]
evgenConfig.contact = ['Dilia Portillo <dilia.maria.portillo.quintero@cern.ch>']
genSeq.Pythia8.Commands += [
    '25:oneChannel = on 1.0 100 5 -5 '
]
#--------------------------------------------------------------
# MET filter
#--------------------------------------------------------------
include("MC15JobOptions/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 100*GeV
filtSeq.MissingEtFilter.UseNeutrinosFromHadrons = True

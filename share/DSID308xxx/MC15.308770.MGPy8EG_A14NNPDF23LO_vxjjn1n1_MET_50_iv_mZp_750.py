model="InelasticVectorEFT"
mDM1 = 375.
mDM2 = 1500.
mZp = 750.
mHD = 125.
widthZp = 3.570290e+00
widthN2 = 1.537516e+00
filteff = 9.940358e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

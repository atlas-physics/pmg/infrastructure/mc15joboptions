## Pythia8 W->mu+HNL

evgenConfig.description = "W->mu+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W"]
evgenConfig.contact = ["Arnaud Dubreuil, arnaud.dubreuil@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from EvgenProdTools.EvgenProdToolsConf import TestHepMC
genSeq += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm

genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 3.0 0.0 0.0 0.0 100.0 0 1 0 1 0",
                            "50:isResonance = false",
                    	    "50:addChannel = 1 0.25 0 -13  11 12",#decay in mu e ve
                            "50:addChannel = 1 0.25 0  13 -11 12",#decay in mu -e ve
                            "50:addChannel = 1 0.25 0 -13  13 14",#decay in mu mu vmu
                            "50:addChannel = 1 0.25 0  13 -13 14",#decay in mu -mu vmu
                            "50:mayDecay = on",
                            "WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
			    "24:addchannel = 1 1. 103 -13 50",
			    "ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
			    "ParticleDecays:tau0Max = 600.0"]
	


#--------------------------------------------------------------
# EVGEN configuration                                                                                                                                                                             
#--------------------------------------------------------------                                                                                                                                
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ZZ->4nu mh=125 GeV CPS"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "ZZ" ]
evgenConfig.contact     = [ 'rzou@cern.ch','bcarlson@cern.ch']
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.minevents = 1000
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 12 12',
                             '23:onIfMatch = 14 14',
                             '23:onIfMatch = 16 16' ]

#--------------------------------------------------------------
# Missing Et filter 
#--------------------------------------------------------------
include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 125*GeV
#MET > 125 GeV 
filtSeq.Expression = "MissingEtFilter" 

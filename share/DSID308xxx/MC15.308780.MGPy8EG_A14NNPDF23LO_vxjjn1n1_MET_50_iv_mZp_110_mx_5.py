model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 140.
mZp = 110.
mHD = 125.
widthZp = 4.376743e-01
widthN2 = 1.449829e-03
filteff = 3.866976e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

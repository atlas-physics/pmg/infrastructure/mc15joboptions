include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Lepton+MET Mt-Binned TTbar Sample evlv 500MT1000"
evgenConfig.keywords = ["top", "lepton", "neutrino", "2jet", "SM" ]
evgenConfig.contact  = [ "julie.kirk@cern.ch","oleksandr.viazlo@cern.ch", "daniel.hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
##evgenConfig.minevents = 10

##evgenConfig.inputconfcheck = "Sherpa_CT10_ttbar_mtbinned"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
##  CORE_SCALE=DEFAULT;
  CORE_SCALE VAR{sqr(172.5)+0.5*(PPerp2(p[2]+p[3]+p[4])+PPerp2(p[5]+p[6]+p[7]))};
  EXCLUSIVE_CLUSTER_MODE 1;

  MASSIVE[15]=1
  NJET:=0;

  INTEGRATION_ERROR=0.05;

  SHERPA_LDADD=SherpaTwoParticleMass
}(run)

(processes){
  # l l nu nu b bbar; W+ -> enu(munu), W- -> lnu
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order (*,4);
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -11 12
  Decay -24[d] -> 13 -14
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

  # l l nu nu b bbar; W+ -> enu(munu), W- -> lnu
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order (*,4);
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -15 16
  Decay -24[d] -> 13 -14
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

  # l l nu nu b bbar; W+ -> enu(munu), W- -> lnu
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order (*,4);
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -13 14
  Decay -24[d] -> 13 -14
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

  # l l nu nu b bbar; W+ -> enu(munu), W- -> lnu
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order (*,4);
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -13 14
  Decay -24[d] -> 11 -12
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

  # l l nu nu b bbar; W+ -> enu(munu), W- -> lnu
  Process 93 93 ->  6[a] -6[b] 93{NJET}
  Order (*,4);
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -13 14
  Decay -24[d] -> 15 -16
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process;

}(processes)

(selector){
  TwoParticleMass 13 0 500 1000 1
  "PT" 90 1.0,E_CMS:1.0,E_CMS [PT_UP]
  "PT" 91 1.0,E_CMS:1.0,E_CMS [PT_UP]
  "PT" 94 1.0,E_CMS:1.0,E_CMS [PT_UP]
  Mass 11 -12 0.25 E_CMS
  Mass -11 12 0.25 E_CMS
  Mass 13 -14 0.3557 E_CMS
  Mass -13 14 0.3557 E_CMS
  Mass 15 -16 2.0268 E_CMS
  Mass -15 16 2.0268 E_CMS
}(selector)
"""

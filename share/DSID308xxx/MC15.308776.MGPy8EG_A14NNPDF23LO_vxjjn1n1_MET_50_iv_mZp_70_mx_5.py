model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 100.
mZp = 70.
mHD = 125.
widthZp = 2.785143e-01
widthN2 = 5.220818e-04
filteff = 2.729258e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

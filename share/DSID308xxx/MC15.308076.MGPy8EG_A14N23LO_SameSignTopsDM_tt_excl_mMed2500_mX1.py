include("MC15JobOptions/MadGraphControl_tt_exclusive.py")

evgenConfig.contact  = ["sergey.senkin@cern.ch"]
evgenConfig.description = "Same-sign tt production (exclusive) with mDM="+str(mDM)+"GeV and mMed="+str(mMed)+"GeV"
evgenConfig.keywords = ["exotic"]

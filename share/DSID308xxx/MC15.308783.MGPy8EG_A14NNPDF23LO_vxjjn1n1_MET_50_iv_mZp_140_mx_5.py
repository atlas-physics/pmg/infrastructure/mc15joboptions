model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 170.
mZp = 140.
mHD = 125.
widthZp = 5.570415e-01
widthN2 = 2.640658e-03
filteff = 4.725898e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

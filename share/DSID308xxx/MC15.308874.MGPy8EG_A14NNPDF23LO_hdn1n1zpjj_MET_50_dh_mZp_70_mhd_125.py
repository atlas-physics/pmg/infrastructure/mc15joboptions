model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 70.
mHD = 125.
widthZp = 2.785143e-01
widthhd = 1.554719e-02
filteff = 6.553080e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Lepton-neutrino Mt-Binned Diboson Sample evll 500MT1000"
evgenConfig.keywords = ["diboson", "lepton", "neutrino", "SM" ]
evgenConfig.contact  = [ "magnar.kopangen.bugge@cern.ch", "daniel.hayden@cern.ch", "frank.siegert@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.inputconfcheck = "Sherpa_CT10_VV_mtbinned"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=1; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=Internal;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16
  OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  MCATNLO_MASSIVE_SPLITTINGS=0
  MASSIVE[5]=1;

  SHERPA_LDADD=SherpaTwoParticleMass
}(run)

(processes){

  Process 93 93 -> 11 -11 11 -12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 15 -16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -11 13 -14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -11 15 -16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 13 -13 11 -12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 13 -13 15 -16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 11 -12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 13 -14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -11 -11 12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 -15 16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -11 -13 14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 11 -11 -15 16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 13 -13 -11 12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 13 -13 -15 16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 -11 12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 -13 14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {5,6,7,8};
  End process;

}(processes)

(selector){
  "PT" 90 5.0,E_CMS [PT_UP]
  "PT" 91 5.0,E_CMS
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
  TwoParticleMass 11 0 500 1000 1
}(selector)
"""

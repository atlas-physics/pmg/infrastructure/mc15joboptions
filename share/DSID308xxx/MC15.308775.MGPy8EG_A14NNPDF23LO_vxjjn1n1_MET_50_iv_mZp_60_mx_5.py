model="InelasticVectorEFT"
mDM1 = 5.
mDM2 = 90.
mZp = 60.
mHD = 125.
widthZp = 2.387215e-01
widthN2 = 3.954365e-04
filteff = 2.520161e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

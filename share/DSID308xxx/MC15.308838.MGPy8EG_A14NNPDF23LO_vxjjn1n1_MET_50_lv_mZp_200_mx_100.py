model="LightVector"
mDM1 = 100.
mDM2 = 400.
mZp = 200.
mHD = 125.
widthZp = 7.957744e-01
widthN2 = 1.313920e+01
filteff = 9.025271e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

model="LightVector"
mDM1 = 50.
mDM2 = 200.
mZp = 100.
mHD = 125.
widthZp = 3.978850e-01
widthN2 = 6.569598e+00
filteff = 7.163324e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

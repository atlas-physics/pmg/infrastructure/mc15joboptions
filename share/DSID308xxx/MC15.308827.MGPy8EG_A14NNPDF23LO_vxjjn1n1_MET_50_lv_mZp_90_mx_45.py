model="LightVector"
mDM1 = 45.
mDM2 = 180.
mZp = 90.
mHD = 125.
widthZp = 3.580954e-01
widthN2 = 5.192638e+00
filteff = 6.693440e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
if hasattr(PowhegConfig, "vdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.vdecaymode = 2  # enu
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "z > mu+ mu-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents *= 25.0 # (filter reduction * 1.2) 
PowhegConfig.running_width = 1
PowhegConfig.mass_low = 110.
#PowhegConfig.mass_high = 13000.	

PowhegConfig.generate()
#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with the AZNLO CTEQ6L1 tune 
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')


#--------------------------------------------------------------
# FILTER
#--------------------------------------------------------------
include('MC15JobOptions/MultiLeptonFilter.py')
## Default cut params
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2
#ChargedTrackFilter
from GeneratorFilters.GeneratorFiltersConf import ChargedTracksFilter
chtrkfilter = ChargedTracksFilter("ChargedTracksFilter")
chtrkfilter.NTracks=-1
chtrkfilter.NTracksMax=11
chtrkfilter.Ptcut=500
chtrkfilter.Etacut=2.5
filtSeq += chtrkfilter

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z/gamma->mumu production with dilepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2muon' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]









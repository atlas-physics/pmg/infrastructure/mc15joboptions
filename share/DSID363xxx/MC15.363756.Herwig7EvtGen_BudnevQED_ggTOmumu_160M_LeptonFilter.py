evgenConfig.description = "gammagamma->mumu with Budnev parameterization, central lepton filter pt>12 GeV"
evgenConfig.keywords = ["QED", "2lepton", "exclusive"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]
evgenConfig.generators  = ["Herwig7", "EvtGen"]

# mass range and flavor deduced from JO name
include("MC15JobOptions/Herwig7_QED_EvtGen_ll.py")

#LeptonKtCut:MinKt defines generator efficiency, relax if lower-pt lepton filter needed
Herwig7Config.add_commands("set /Herwig/Cuts/LeptonKtCut:MinKT 8*GeV\n")
Herwig7Config.run()

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 12000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

evgenConfig.minevents = 5000




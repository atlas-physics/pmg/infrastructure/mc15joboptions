evgenConfig.description = "Pythia8 multijets with pdf NNPDF23lo, slice JZ1 to be combined with MadGraph multijet HT slices"
evgenConfig.keywords+=['QCD', 'jets']
evgenConfig.contact = ['jdickinson@lbl.gov']
evgenConfig.minevents = 5000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_forMGHT_EvtGen.py")

import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=500
ihtmax=700
HTrange='midHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=50

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (HeavyFlavorCHadronPt4Eta3_Filter)"

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Zee_Ht500_700_13TeV"

#--------------------------------------------------------------
# Powheg WZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WZ_Common.py')
PowhegConfig.decay_mode = 'WZlvll'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 20.0   # GeV
PowhegConfig.PDF = 11000 # CT10nlo 0
PowhegConfig.mu_F = [ 1.0 ]
PowhegConfig.mu_R = [ 1.0 ]
PowhegConfig.generate()

# Herwig showering                                                                                                                                                    
#--------------------------------------------------------------                                                                                                  
include('MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py')
#--------------------------------------------------------------       

cmds = """                                                                                                                                                                                                   
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General                                                                                                                                       
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost                                                                                                                           
"""
genSeq.Herwigpp.Commands += cmds.splitlines()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP Diboson WZ->lvll production with mllmin20'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WZ', '3lepton', 'neutrino' ]
evgenConfig.minevents   = 5000

evgenConfig.generators += ["Powheg", "Herwigpp", "EvtGen"]

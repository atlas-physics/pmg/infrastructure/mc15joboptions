import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=1000
ihtmax=2000
HTrange='highHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns.py')
evgenConfig.minevents=10

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Znunu_Ht1000_2000_13TeV"

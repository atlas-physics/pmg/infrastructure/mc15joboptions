include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa W+/W- -> e nu + 0,1,2j@NLO + 3,4j@LO with 2000 GeV < ptV < E_CMS GeV with light-jet filter."
evgenConfig.keywords = ["SM", "W", "electron", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 50
evgenConfig.inputconfcheck = "Wenu_Pt2000_E_CMS"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 93 93 -> 11 -12 93 93{3};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;

  Process 93 93 -> -11 12 93 93{3};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 11 -12 2.0 E_CMS
  Mass -11 12 2.0 E_CMS
  "PT" 11,-12  2000,E_CMS
  "PT" -11,12  2000,E_CMS
}(selector)
"""


# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter)"


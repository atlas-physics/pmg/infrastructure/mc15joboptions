from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'MadGraph_ggWW_NLO'
evgenConfig.keywords = ['SM','diboson','VBS','WW','electroweak','2lepton']
evgenConfig.contact = ["Karolos.Potamianos@cern.ch"]

include("MC15JobOptions/MGaMcAtNloPy8EG_NNPDF30nnlo_ggWW.py")


# genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]

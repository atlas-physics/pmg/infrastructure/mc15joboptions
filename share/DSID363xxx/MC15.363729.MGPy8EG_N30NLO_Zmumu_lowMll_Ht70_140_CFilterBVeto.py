import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=70
ihtmax=140
HTslice='midlowerHT'
include('MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_25ns_Mll10to40_ptl5.py')
evgenConfig.minevents=100

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (HeavyFlavorCHadronPt4Eta3_Filter)"

evgenConfig.inputconfcheck="MGPy8EG_N30NLO_Zmumu_Mll10to40_ptl5_Ht70_140_13TeV"

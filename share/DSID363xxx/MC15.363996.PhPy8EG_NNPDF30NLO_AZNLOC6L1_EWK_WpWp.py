#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 4' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBS WpWp + 2 jets"
evgenConfig.keywords    = [ "SM", "VBS", "electroweak", "WW", "2jet", "NLO" ]
evgenConfig.contact     = [ "Christian Johnson <christian.johnson@cern.ch>" ]
evgenConfig.process     = "VBS ssWW"
evgenConfig.inputfilecheck = 'EWK_WpWp'
evgenConfig.minevents   = 5000

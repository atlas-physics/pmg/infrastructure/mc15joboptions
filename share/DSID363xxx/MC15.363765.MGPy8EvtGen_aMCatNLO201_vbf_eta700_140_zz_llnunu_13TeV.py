###########################################
# MadGraph5, ZZ, ZZ->llnunu                 #
###########################################

#### Shower 
evgenConfig.description = 'qq->eta->ZZ, Z->ll + Z->nunu, meta= 700GeV'
evgenConfig.keywords+=['VBF','ZZ']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.contact     = ['Ashutosh Kotwal <ashutosh.kotwal@cern.ch>', 'Sourav Sen <sourav.sen@cern.ch>']
# comment out inputfilecheck, to allow running on multiple inputs
#evgenConfig.inputfilecheck = 'VBFH125_sbi_4l_m4l130'
#evgenConfig.minevents = 1000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include('MC15JobOptions/Pythia8_LHEF.py')

# boson decays already done in the lhe file
#genSeq.Pythia8.Commands += [ '9000006:onMode = off' ]
genSeq.Pythia8.Commands += [ '25:onMode = off' ]
genSeq.Pythia8.Commands += [ '24:onMode = off' ]
genSeq.Pythia8.Commands += [ '23:onMode = off' ]

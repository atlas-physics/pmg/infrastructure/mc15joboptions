evgenConfig.description = "gammagamma -> mumu production with LPAIR SingleDiss, 60<M<200GeV, central dilepton filter pt>3.5 GeV"

evgenConfig.keywords = ["QCD", "2lepton", "exclusive", "dissociation", "diphoton"]

evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

evgenConfig.inputfilecheck = 'SDiss_ggTOmumu_13TeV_60M200'

include("MC15JobOptions/HepMCReadFromFile_Common.py")

evgenConfig.tune = "none"

include("MC15JobOptions/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[-5334, 5334]


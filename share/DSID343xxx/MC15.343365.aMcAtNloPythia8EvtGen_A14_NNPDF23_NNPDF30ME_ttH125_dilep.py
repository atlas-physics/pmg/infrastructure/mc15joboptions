evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.description = 'MG5_aMC@NLO_Pythia8'
evgenConfig.keywords+=['Higgs','top']
evgenConfig.inputfilecheck = 'ttH'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")

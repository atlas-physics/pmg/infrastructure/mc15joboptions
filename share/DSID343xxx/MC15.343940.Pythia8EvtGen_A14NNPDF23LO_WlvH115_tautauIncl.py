evgenConfig.description = "PYTHIA8+EVTGEN, WH125, W->lv, H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2tau" ]
evgenConfig.contact     = [ 'xin.chen@cern.ch' ]
evgenConfig.process     = "WH, H->tautau, W->lv"

#Higgs mass (in GeV)
H_Mass = 115.0

#Higgs width (in GeV)
H_Width = 0.00312

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:onMode = off',
                             '25:doForceWidth = true',
                             '25:onIfMatch = 15 15', # Higgs decay
                             'HiggsSM:ffbar2HW = on',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16'
                             ]

if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  filtSeq += TauFilter()

filtSeq.TauFilter.Ntaus = 2
filtSeq.TauFilter.Ptcute = 5000.0 #MeV
filtSeq.TauFilter.Ptcutmu = 5000.0 #MeV
filtSeq.TauFilter.Ptcuthad = 10000.0 #MeV
evgenConfig.generators  = [ "Pythia8", "EvtGen"] 

#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+W+jet->mu+vmuWW production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HWj_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.idvecbos = 24
PowhegConfig.vdecaymode = 2 # W->mu+vmu
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 120
#PowhegConfig.Vstep = 10
PowhegConfig.PDF = range( 10800, 10853 ) # CT10 PDF variations 
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->WW at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->mu+vmuWW->mu+vmulvlv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'olivier.arnaez@cern.ch', 'daniele.puddu@cern.ch' ]

evgenConfig.process = "WpH, W->muvmu, H->WW->lvlv"

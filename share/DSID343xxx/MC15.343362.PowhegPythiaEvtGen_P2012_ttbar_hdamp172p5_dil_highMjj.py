#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, at least two lepton filter, high mjj filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', '2lepton'  ]
evgenConfig.contact     = [ 'jahreda@gmail.com','bnachman@cern.ch', 'jvannieu@cern.ch' ]
evgenConfig.minevents   = 100

if runArgs.trfSubstepName == 'generate' :
  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 10000.
  PowhegConfig.generateRunCard()
  PowhegConfig.generateEvents()
#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
#--------------------------------------------------------------
# Event filter leptons
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.
#--------------------------------------------------------------
# VBF Mjj Interval filter
#--------------------------------------------------------------
  include("MC15JobOptions/VBFMjjIntervalFilter.py")
  filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0
  filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 15.*GeV
  filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 15.*GeV
  filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
  filtSeq.VBFMjjIntervalFilter.NoJetProbability  = -1.0
  filtSeq.VBFMjjIntervalFilter.OneJetProbability = -1.0
  filtSeq.VBFMjjIntervalFilter.LowMjjProbability = 0.00502
  filtSeq.VBFMjjIntervalFilter.HighMjjProbability = 1.0
  filtSeq.VBFMjjIntervalFilter.LowMjj = 500.*GeV
  filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = True
  filtSeq.VBFMjjIntervalFilter.HighMjj = 2500.*GeV
  filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
  filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = True
  filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = True
  filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = True
#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')

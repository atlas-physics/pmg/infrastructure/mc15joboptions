from MadGraphControl.MadGraphUtils import *

mode = 0



cmdsps = """set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-; h0->gamma,gamma;
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
"""


# LHEHandler generated events/number of attempts ~75%
safefactor = 3 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> WW, yyWW"
evgenConfig.keywords = ["BSM", "BSMHiggs", "resonance", "diphoton"]
run_number_min = 343822
run_number_max = 343822
offset = 23 

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
filtSeq.Expression = "hyyFilter and hWWFilter"

evgenConfig.generators  = [  "aMcAtNlo", "Herwigpp", "EvtGen"] 

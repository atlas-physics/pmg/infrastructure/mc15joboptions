from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->gamma,gamma;
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
"""



# LHEHandler generated events/number of attempts ~75%
safefactor = 7 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> yy,tautau yytt had had"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "tau"] 

run_number_min = 343790 
run_number_max = 343803
offset = 0

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq += ParentChildFilter("hTauTauFilter", PDGParent = [25], PDGChild = [15])
include("MC15JobOptions/XtoVVDecayFilterExtended.py")
filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [24,211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [24,211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.Expression = "hyyFilter and hTauTauFilter and XtoVVDecayFilterExtended"

evgenConfig.generators  = [  "aMcAtNlo", "Herwigpp", "EvtGen"] 

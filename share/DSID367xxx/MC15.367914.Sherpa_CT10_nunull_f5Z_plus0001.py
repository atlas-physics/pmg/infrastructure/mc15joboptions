include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "nu nu l l production with up to one jet in ME+PS with aTGC F5_Z 0.001"
evgenConfig.keywords = [ "electroweak", "lepton", "neutrino", "SM" ]
evgenConfig.contact  = [ "Evgeny.Soldatov@cern.ch" ]
evgenConfig.inputconfcheck = "f5Z_plus0001"
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
  SCALES=STRICT_METS{MU_F2}{MU_R2}{MU_Q2}
  EXCLUSIVE_CLUSTER_MODE=1
}(run)

(model){
  MODEL = SM+AGC
  F5_Z = 0.001
  UNITARIZATION_SCALE=2000000 
  UNITARIZATION_N=3 
}(model)

(processes){
  Process 93 93 -> 11 -11 14 -14 93{1}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  End process;

  Process 93 93 -> 11 -11 16 -16 93{1}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  End process;

  Process 93 93 -> 13 -13 12 -12 93{1}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  End process;

  Process 93 93 -> 13 -13 16 -16 93{1}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  End process;

  Process 93 93 -> 15 -15 12 -12 93{1}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  End process;

  Process 93 93 -> 15 -15 14 -14 93{1}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 4.0 E_CMS
  Mass 13 -13 4.0 E_CMS
  Mass 15 -15 4.0 E_CMS
}(selector)
"""

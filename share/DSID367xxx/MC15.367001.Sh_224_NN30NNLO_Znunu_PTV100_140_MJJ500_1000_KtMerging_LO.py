include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> nu nu + 0,1,2,3,4j@LO with 100 GeV < pTV < 140 GeV, 500 < Mjj < 1000 GeV."
evgenConfig.keywords = ["SM", "Z", "neutrino", "jets", "LO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch", "bjr@sas.upenn.edu"]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Znunu_PTV100_140_MJJ500_1000_KtMerging_LO"

genSeq.Sherpa_i.RunCard = """
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=0; QCUT:=20.;

  % Fastjet/antikt merging criterion.
  % SHERPA_LDADD=SherpaFastJetCriterion SherpaFastjetSelector;
  JET_CRITERION FASTJET[A:antikt,R:0.4,y:5];

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
}(run)

(processes){
  Process 93 93 -> 91 91 93 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 91 91 2.0 E_CMS
  "PT" 91,91  100,140
  FastjetSelector Mass(p[4]+p[5])>500.&&Mass(p[4]+p[5])<1000. antikt 2 20. 0. 0.4
}(selector)
"""

genSeq.Sherpa_i.OpenLoopsLibs = ["ppll", "ppllj", "pplljj"]
genSeq.Sherpa_i.ExtraFiles = [ "libSherpaFastJetCriterion.so", "libSherpaFastjetSelector.so" ]

include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> mu mu + 0,1,2,3,4j@LO with 220 GeV < pTV < 280 GeV, 0 < Mjj < 500 GeV."
evgenConfig.keywords = ["SM", "Z", "2muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch", "bjr@sas.upenn.edu"]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Zmumu_PTV220_280_MJJ0_500_KtMerging_LO"

genSeq.Sherpa_i.RunCard = """
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=0; QCUT:=20.;

  % Fastjet/antikt merging criterion.
  % SHERPA_LDADD=SherpaFastJetCriterion SherpaFastjetSelector;
  JET_CRITERION FASTJET[A:antikt,R:0.4,y:5];

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93{NJET};

  % This uses a different selector for 0- and 1- parton matrix elements.
  % 0-parton matrix elements might be forbidden, but... hopefully it works.
  Selector_File *|(sel_0_1){|}(sel_0_1) {2,3}

  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

% This selector imposes the ptv and mjj requirements.
(selector){
  Mass 13 -13 40.0 E_CMS
  "PT" 13,-13  220,280
  FastjetSelector Mass(p[4]+p[5])<500. antikt 2 20. 0. 0.4
}(selector)

% This selector only imposes the ptv requirement.
(sel_0_1){
  Mass 13 -13 40.0 E_CMS
  "PT" 13,-13  220,280
}(sel_0_1)
"""

genSeq.Sherpa_i.OpenLoopsLibs = ["ppll", "ppllj", "pplljj"]
genSeq.Sherpa_i.ExtraFiles = [ "libSherpaFastJetCriterion.so", "libSherpaFastjetSelector.so" ]

evgenConfig.description = 'Herwig7 NLO dijets with MMHT2014 and dipole shower, slice JZ 3'
evgenConfig.keywords+=['SM','QCD','jets']
evgenConfig.contact = ['javier.llorente.merino@cern.ch']
evgenConfig.minevents = 1000

include("MC15JobOptions/Herwig7EG_Matchbox_MG_H7UEMMHT2014_dipole_multijet_withGridpack.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(3,filtSeq)


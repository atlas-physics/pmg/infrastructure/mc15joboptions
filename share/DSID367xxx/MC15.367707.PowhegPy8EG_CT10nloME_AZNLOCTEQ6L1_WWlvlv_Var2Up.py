#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------

include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode = 'WWlvlv'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = range( 11000, 11053 )+[ 21100, 260000 ] # CT10nlo 0-52, MSTW2008nlo68cl, NNPDF3.0
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 2.0     # Dependent on filter efficiency
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_Var2Up_EvtGen_Common.py')
genSeq.Pythia8.UserModes += ['Main31:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WW_lvlv production with AZNLO CTEQ6L1 tune'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'christian.johnson@cern.ch' ]
evgenConfig.minevents   = 5000

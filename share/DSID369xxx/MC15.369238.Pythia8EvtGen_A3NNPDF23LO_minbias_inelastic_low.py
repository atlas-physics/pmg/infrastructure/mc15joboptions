#based on 361238
evgenConfig.description = "Low-pT inelastic minimum bias events for pile-up, with the A3 NNPDF23LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]

evgenConfig.saveJets = True

include("MC15JobOptions/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += \
    ["SoftQCD:inelastic = on"]

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq,filtSeq,runArgs.ecmEnergy, 0.6)

from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

evgenConfig.minevents = 1000

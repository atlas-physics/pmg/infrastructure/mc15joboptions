evgenConfig.description = "PYTHIA8+EVTGEN, ttH, top->any, H->gamgam, mH=500GeV"
evgenConfig.keywords    = [ "BSM", "Higgs", "diphoton" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 500.

#Higgs width (in GeV)
H_Width = 0.00407
	
include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:doForceWidth = true',
                             '25:onMode = off',
                             '25:onIfMatch = 22 22', # Higgs decay
                             'HiggsSM:gg2Httbar = on',
                             'HiggsSM:qqbar2Httbar = on'
                             ]

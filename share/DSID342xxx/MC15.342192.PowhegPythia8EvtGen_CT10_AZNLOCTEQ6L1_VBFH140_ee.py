#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 140.
PowhegConfig.width_H = 0.00817

# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 1

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.

# Generate Powheg events
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 11 11' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "VBF H->ee"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ee"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "VBF", "2electron" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen"] 

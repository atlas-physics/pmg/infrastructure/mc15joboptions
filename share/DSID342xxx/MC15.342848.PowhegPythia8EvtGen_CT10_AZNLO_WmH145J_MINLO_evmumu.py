#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 W+H145+jet->evmumu production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HWj_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 145.
PowhegConfig.width_H = 0.0114

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 4.

PowhegConfig.runningscales = 1 # 
PowhegConfig.idvecbos = -24
PowhegConfig.vdecaymode = 1 # W->ev
PowhegConfig.hdecaymode = -1
PowhegConfig.mass_W_low = 10

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 120
#PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->mumu at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 W+H145+jet->evmumu production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "2muon" ]
evgenConfig.contact     = [ 'xin.chen@cern.ch' ]
evgenConfig.process     = "WmH, H->mumu, W->ev"
evgenConfig.minevents   = 200

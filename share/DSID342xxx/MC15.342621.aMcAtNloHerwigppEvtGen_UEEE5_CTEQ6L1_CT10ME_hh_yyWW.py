#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")
## To modify Higgs BR
cmds = """
set /Herwig/EventHandlers/LHEReader:AllowedToReOpen 0
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma; h0->W+,W-;
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
"""

from Herwigpp_i import config as hw
genSeq.Herwigpp.Commands += cmds.splitlines()
del cmds

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
if (runArgs.runNumber == 342621):
    evgenConfig.description = "SM diHiggs production, decay to gammagammaWW, with MG5_aMC@NLO, inclusive of box diagrami FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "diphoton", "WW"]

evgenConfig.contact = ['Biagio Di Miccol <Biagio.di.micco@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
filtSeq += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "hWWFilter and hyyFilter"

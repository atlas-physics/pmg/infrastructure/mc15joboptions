#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 Z+H140+jet->mumumumu production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HZj_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 140.
PowhegConfig.width_H = 0.00817

PowhegConfig.runningscales = 1 #
PowhegConfig.vdecaymode = 2 # Z->mumu
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 120
#PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->mumu at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 Z+H140+jet->mumumumu production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "2muon"]
evgenConfig.contact     = [ 'xin.chen@cern.ch' ]
evgenConfig.process     = "ZH, H->mumu, Z->mumu"
evgenConfig.minevents   = 200

include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

#set all Higgs to decay to Z0,gamma
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL

set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->c,cbar;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->g,g;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.
set /Herwig/Particles/h0/h0->mu-,mu+;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->t,tbar;:BranchingRatio 0.
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio 0.

decaymode h0->Z0,gamma; 1.0 1 /Herwig/Decays/Mambo
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = "aMC@NLO showered with Herwig++ ttH(100) H->Zgam"
evgenConfig.keywords += ['Higgs', 'SMHiggs', 'ttHiggs', 'Zgamma']
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'tth'

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector -- this way the commands in cmds are executed
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up (just in case)
del cmds

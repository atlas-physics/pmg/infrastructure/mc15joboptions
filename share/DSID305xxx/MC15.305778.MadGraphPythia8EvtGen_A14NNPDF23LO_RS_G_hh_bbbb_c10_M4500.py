include("MC15JobOptions/MadGraphControl_RS_G_hh_bbbb.py")

evgenConfig.description = "Bulk Randall-Sundrum model KK graviton -> hh -> bbbb with NNPDF2.3 LO A14 tune"
evgenConfig.keywords = ["exotic", "RandallSundrum", "warpedED", "graviton", "Higgs"]

evgenConfig.description = "dmS sample to dibjet"
evgenConfig.keywords = ["exotic", "BSM", "scalar"]
model  = 'dmS'
mR     = 800
mDM    = 10000
gSM    = 1.18
gDM    = 1.00
widthR = 48.33

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet_withb.py")

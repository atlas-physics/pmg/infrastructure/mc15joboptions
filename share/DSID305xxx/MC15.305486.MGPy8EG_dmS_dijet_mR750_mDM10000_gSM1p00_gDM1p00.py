evgenConfig.description = "dmS sample to dibjet"
evgenConfig.keywords = ["exotic", "BSM", "scalar"]
model  = 'dmS'
mR     = 750
mDM    = 10000
gSM    = 1.00
gDM    = 1.00
widthR = 31.00

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet_withb.py")

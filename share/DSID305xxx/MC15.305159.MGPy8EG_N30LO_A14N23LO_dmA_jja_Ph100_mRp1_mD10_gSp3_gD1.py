model  = 'dmA'
mR     = 100
mDM    = 10000
gSM    = 0.30
gDM    = 1.00
widthR = 3.570973
phminpt= 100.000000
filteff = 0.018000

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")

model  = 'dmA'
mR     = 300
mDM    = 10000
gSM    = 0.30
gDM    = 1.00
widthR = 10.739615
phminpt= 100.000000
filteff = 0.061031

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")

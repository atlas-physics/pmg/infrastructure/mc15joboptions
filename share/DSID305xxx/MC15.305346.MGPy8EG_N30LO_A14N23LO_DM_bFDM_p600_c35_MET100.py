mchi = 35
mphi = 600
gx = 1.99236178806
filter_string = "T"
evt_multiplier = 15
include("MC15JobOptions/MadGraphControl_bFDMmodels.py")
evgenConfig.minevents = 500
evgenConfig.keywords = ['exotic','BSM','WIMP']

model   = 'dmA'
mR      = 450
mDM     = 10000
gSM     = 0.10
gDM     = 1.00
widthR  = 1.883354
jetminpt= 350
filteff = 0.028007

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetjet.py")

evgenConfig.description = "DM axial Z'->jj, m_{R}=450 g_{SM}=0.10 m_{DM}=10000 g_{DM}=1.00"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.process = "p p > xi j, xi > j j"
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>"]

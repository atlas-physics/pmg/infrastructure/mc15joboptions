include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z(->qq)+Gamma with + 0,1,2,3j@LO."
evgenConfig.keywords = ["SM", "diboson", "photon", "2jet" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "Shu.Li@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_ZqqGamma"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic;
  LOOPGEN:=Internal;
  EXCLUSIVE_CLUSTER_MODE=1

  % massive b-quarks such that top-quark processes are not included by the 93 container
  %MASSIVE[5]=1;
  %OL_PARAMETERS=nq_nondecoupled 5 mass(5) 0.0
  %MCATNLO_MASSIVE_SPLITTINGS=0

  % decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
  HDH_ONLY_DECAY={23,1,-1}|{23,2,-2}|{23,3,-3}|{23,4,-4}|{23,5,-5}
}(run)

(processes){
  Process 93 93 -> 22 23 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes)

(selector){
  PT  22  1000  2000
  IsolationCut  22  0.3  2  0.025;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]

model  = 'dmA'
mR     = 400
mDM    = 10000
gSM    = 0.30
gDM    = 1.00
widthR = 14.679444
phminpt= 100.000000
filteff = 0.100050

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetgamma.py")

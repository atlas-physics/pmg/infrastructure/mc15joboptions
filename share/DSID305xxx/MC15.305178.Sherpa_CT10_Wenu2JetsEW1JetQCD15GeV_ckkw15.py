﻿include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa VBF-W production, with W -> enu. Includes dibosons."
evgenConfig.keywords = [ "SM", "W", "electron", "jets", "VBF", "diboson"]
evgenConfig.contact  = [ "bill.balunas@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_Wenu2JetsEW1JetQCD15GeV_ckkw15"

evgenConfig.process="""

(run){
  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=1.0; QSCF:=1.0;
  QCUT:=15.;
  SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;
}(run)

(processes){
  Process 93 93 -> 11 -12 93 93 93{1};
  Order_EW 4;
  CKKW sqr(QCUT/E_CMS);
  Integration_Error 0.05;
  End process;

  Process 93 93 -> -11 12 93 93 93{1};
  Order_EW 4;
  CKKW sqr(QCUT/E_CMS);
  Integration_Error 0.05;
  End process;
}(processes)

(selector){
  Mass 11 -12 10 E_CMS
  Mass -11 12 10 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

(model){
  MASSIVE[5]=1
}(model)

"""

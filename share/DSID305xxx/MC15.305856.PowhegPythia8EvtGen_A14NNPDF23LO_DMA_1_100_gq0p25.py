#--------------------------------------------------------------
# Powheg DMV setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_DMV_Common.py')
PowhegConfig.vdecaymode = -2
PowhegConfig.DM_mass = 1
PowhegConfig.V_mass  = 100
PowhegConfig.V_width = 5.1296405117
PowhegConfig.gDM = 1.0
PowhegConfig.gSM = 0.25
PowhegConfig.runningwidth = 0
PowhegConfig.bornktmin = 150 #Intended for analyses with MET>300 GeV
PowhegConfig.bornsuppfact = 1000 #Ensure sufficient statistics at high MET
PowhegConfig.PDF = range(260000, 260101) # NNPDF30_nlo_as_0118
#The mass_low and mass_high parameters are computed internally
PowhegConfig.mass_low = -1
PowhegConfig.mass_high = -1
#Keep negative weights to make sure no events are discarded
PowhegConfig.withnegweights = 1
#The paramters below are set following the recommendation of the authors.
#It is a compromise between fast production with too many events with negative weights
#and small amount of events with negative weights at the cost of large running time.
# NOTE: since Powheg-00-03-05 (r3133), foldsci, foldy and foldphi parameters are no longer
# overridden
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with A14 NNPDF23LO 
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('MC15JobOptions/Pythia8_Powheg.py')

# id:all = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
genSeq.Pythia8.Commands += [ '1000022:all = X Xbar 2 0 0 %f 0.0 0.0 0.0 0.0'%PowhegConfig.DM_mass,
                             '1000022:isVisible = false' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 DMV axial-vector mediator (DM mass = 1 GeV, mediator mass = 100 GeV, gSM = 0.25, gDM=1.0) for mono-jet analysis (suitable for MET > 300 GeV)'
evgenConfig.keywords    = [ 'BSM', 'WIMP', 'invisible', 'exotic' ]
evgenConfig.contact     = [ 'Valerio Ippolito <vippolit@cern.ch>' ]
evgenConfig.process     = 'pp->A(XXbar)j'

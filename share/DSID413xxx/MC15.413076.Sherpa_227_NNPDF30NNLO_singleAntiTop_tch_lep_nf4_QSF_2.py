include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa tj Production t-channel with NF4"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "kai.chung.tam@cern.ch" ]
evgenConfig.minevents = 5000

genSeq.Sherpa_i.RunCard="""
(run){
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=2.0
  SCALES VAR{FSF*16*MPerp2(p[3])}{RSF*16*MPerp2(p[3])}{QSF*16*MPerp2(p[3])}

  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  MASS[5] 4.18 
  MASSIVE[5] 1
}(run)

(processes){
  Process 93 93 -> -6 5 93
  NLO_QCD_Mode MC@NLO
  Order (*,2)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Min_N_TChannels 1  
  End process
}(processes)
"""
genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptj", "pptjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "EW_SCHEME=3", "PDF_SET=NNPDF30_nlo_as_0118_nf_4" ]

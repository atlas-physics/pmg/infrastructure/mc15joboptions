mass = float(runArgs.jobConfig[0].split('.')[-2].split('_')[-1].split('m')[1])

evgenConfig.contact = ["Miles Wu, mileswu@uchicago.edu"]
evgenConfig.description = "Wprime->WZ->munubb " + str(mass) + " GeV with NNPDF23LO PDF and A14 tune"
evgenConfig.keywords = ["exotic", "SSM", "Wprime", "jets"]
evgenConfig.process = "pp>Wprime>WZ>munubb"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]# Allow Wprime                                                                                                                               
genSeq.Pythia8.Commands += ["Wprime:coup2WZ = 1."]    # Wprime Coupling to WZ                                                                                                                              
genSeq.Pythia8.Commands += ["34:m0 = " + str(mass)] # Wprime mass
genSeq.Pythia8.Commands += ["34:onMode = off"]# Turn off  decays                                                                                                                                           
genSeq.Pythia8.Commands += ["34:onIfAll = 23 24"]# Turn on ->WZ                                                                                                                                            
genSeq.Pythia8.Commands += ["23:onMode = off"]# Turn off all Z decays                                                                                                                                      
genSeq.Pythia8.Commands += ["23:onIfAny = 5"]# Turn on Zbb                                                                                                                                  
genSeq.Pythia8.Commands += ["24:onMode = off"]# Turn off all W decays                                                                                                                                      
genSeq.Pythia8.Commands += ["24:onIfAny = 13,14"]# Turn on Wmunu                                                                                                                                  

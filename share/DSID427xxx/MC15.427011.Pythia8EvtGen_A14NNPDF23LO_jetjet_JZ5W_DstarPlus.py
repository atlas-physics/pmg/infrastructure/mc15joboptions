# JO for Pythia 8 jet jet JZ5W slice

evgenConfig.description = "Dijet truth jet slice JZ5W, with the A14 NNPDF23 LO tune and DstarPlusFilter"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.process = "pp to jetjet"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 350."]

include("MC15JobOptions/JetFilter_JZ5W.py")
include("MC15JobOptions/DstarPlusFilter.py")

genSeq.EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02Kpi.DEC"
evgenConfig.auxfiles += [ 'DstarP2D0PiP_D02Kpi.DEC']


evgenConfig.minevents = 50

# JO for Pythia 8 jet jet JZ3W slice

evgenConfig.description = "Dijet truth jet slice JZ3W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/JetFilter_JZ3W.py")
include("MC15JobOptions/DstarMinusFilter.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 50.",
                            "PhaseSpace:bias2Selection = on",
                            "PhaseSpace:bias2SelectionRef = "+str(minDict[3]) ,
                            "PhaseSpace:bias2SelectionPow = 5.0"
                           ]

filtSeq.QCDTruthJetFilter.DoShape = False
filtSeq.UseEventWeight = True

evgenConfig.minevents = 5000


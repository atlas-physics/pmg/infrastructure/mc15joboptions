evgenConfig.description = "a8 Z'(1000 GeV)->bb with A14 tune and NNPDF23LO PDF m=1000 GeVe"
evgenConfig.process = "Z' -> b + bbar"
evgenConfig.contact = ["fdibello@cern.ch"] 
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create W' bosons
"PhaseSpace:mHatMin = 25.0",         # minimum inv.mass cut
"32:onMode = off",                     # switch off all Z' decays
"32:m0 = 4000.0"]


genSeq.Pythia8.Commands += ['32:oneChannel = on 0.3333 100 -5 5'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.3333 100 -4 4'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.1111 100 -3 3'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.1111 100 -2 2'] # onMode bRatio meMode products
genSeq.Pythia8.Commands += ['32:addChannel = 1 0.1111 100 -1 1'] # onMode bRatio meMode products


#Only Z' - no gamma/Z
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3"]

genSeq.Pythia8.UserHook = "ZprimeFlatpT"


## Job options file for Herwig++, QCD jet slice production
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

evgenConfig.description = "QCD dijet production JZ0W with PDF and UE-EE5 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "Haifeng.Li@cern.ch" ]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

create Herwig::MPIXSecReweighter MPIXSecReweighter
insert LHCGenerator:EventHandler:PostSubProcessHandlers 0 MPIXSecReweighter

## The cuts are set by creating a new Cuts objects. 
## This is done due to an issue in 2.7.1, once it will be fixed in a new version, we can go back to the usual setup (as in DSID 187503 in MC12).
cd /Herwig/Cuts
create ThePEG::Cuts MinBiasCuts
set MinBiasCuts:ScaleMin 2.0*GeV
set MinBiasCuts:X1Min 0.01
set MinBiasCuts:X2Min 0.01
set MinBiasCuts:MHatMin 0.0*GeV
set  /Herwig/Generators/LHCGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
"""

genSeq.Herwigpp.Commands += cmds.splitlines()

include('MC15JobOptions/Herwigpp_EvtGen.py')
include("MC15JobOptions/JetFilter_JZ0W.py")
evgenConfig.minevents = 1000


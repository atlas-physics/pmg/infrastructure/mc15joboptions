evgenConfig.description = "pi+ and pi0 close by" 
evgenConfig.keywords = ["singleParticle", "pi+","pi0"]
evgenConfig.contact  = ["Mark Hodgkinson"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG

class MyParticleSampler(PG.ParticleSampler):
    "A special sampler with two _correlated_ particles."
    
    def __init__(self):
        self.mom1 = PG.PtEtaMPhiSampler(pt=600, eta=[0.0,2.5])
        self.mom2 = PG.PtEtaMPhiSampler(pt=2000, eta=[-0.2,2.7])
        super(MyParticleSampler,self).__init__()

    def shoot(self):
        "Return a vector of sampled particles"
        p1 = PG.SampledParticle(211, self.mom1.shoot())      
        p2 = PG.SampledParticle(111, self.mom2.shoot())
        return [p1, p2]

genSeq.ParticleGun.sampler = MyParticleSampler()

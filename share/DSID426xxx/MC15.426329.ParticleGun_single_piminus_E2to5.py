evgenConfig.description = "Single Pi- with flat eta-phi and E in [2, 5] GeV"
evgenConfig.keywords = ["singleParticle", "pi-"]
 
include("MC15JobOptions/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = -211
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=[2000,5000], eta=[-2.8, 2.8])

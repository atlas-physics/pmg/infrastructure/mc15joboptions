from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff Off
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff Off
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff Off
set /Herwig/Particles/W+/W+->u,dbar;:OnOff On
set /Herwig/Particles/W+/W+->c,sbar;:OnOff On
set /Herwig/Particles/W+/W+->sbar,u;:OnOff On
set /Herwig/Particles/W+/W+->c,dbar;:OnOff On
set /Herwig/Particles/W+/W+->bbar,c;:OnOff On
##set W- decay
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff Off
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff Off
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff Off
set /Herwig/Particles/W-/W-->ubar,d;:OnOff On
set /Herwig/Particles/W-/W-->cbar,s;:OnOff On
set /Herwig/Particles/W-/W-->s,ubar;:OnOff On
set /Herwig/Particles/W-/W-->cbar,d;:OnOff On
set /Herwig/Particles/W-/W-->b,cbar;:OnOff On
"""

safefactor = 1.5 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> W+ W-, W+W- -> fully hadronic"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "hh", "WW", "allHadronic"] 

evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.inputconfcheck = 'aMcAtNloHerwigppEvtGen.343670.Xhh_13TeV'

run_number_min = 426323
run_number_max = 426323
offset = 18

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")


include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Electroweak Sherpa W+/W- -> mu nu + 2,3j@LO using Min_N_TChannels option."
evgenConfig.keywords = ["SM", "W", "muon", "jets", "VBF" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Wmunu2jets_Min_N_TChannel"

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  EXCLUSIVE_CLUSTER_MODE=1;

  % tags for process setup
  NJET:=1; QCUT:=15.;

  % Reweights
  SCALE_VARIATIONS 0.25,0.25 0.25,1.0 0.25,4.0 1.0,0.25 1.0,1.0 1.0,4.0 4.0,0.25 4.0,1.0 4.0,4.0;
  PDF_VARIATIONS CT10nlo[all] MMHT2014nlo68cl[all] NNPDF30_nlo_as_0118[all];
  HEPMC_USE_NAMED_WEIGHTS=1

  EW_TCHAN_MODE=1
}(run)

(processes){
  Process 93 93 -> 13 -14 93 93 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;

  Process 93 93 -> 14 -13 93 93 93{NJET};
  Order (*,4); CKKW sqr(QCUT/E_CMS);
  Min_N_TChannels 1
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  Mass 13 -14 40 E_CMS
  Mass 14 -13 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 -1
}(selector)

(model){
  MASSIVE[5]=1
}(model)
"""


from MadGraphControl.MadGraphUtils import *

mode = 0

cmdsps = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
"""

safefactor = 3.5 
evgenConfig.description = "h2->h1h1 diHiggs production with MG5_aMC@NLO, h1 -> W+ W-, W+W- -> 1lep 1nu 6j"
evgenConfig.keywords = ["BSM",  "BSMHiggs", "resonance", "hh", "WW", "1lepton"] 

evgenConfig.contact = ['Jason Veatch <Jason.Veatch@cern.ch>']
evgenConfig.inputconfcheck = 'aMcAtNloHerwigppEvtGen.343670.Xhh_13TeV'

run_number_min = 344870
run_number_max = 344870
offset = 0

include("MC15JobOptions/MadGraphControl_HerwigppEvtGen_UEEE5_CT10ME_NLO_h2h1h1.py")

if not hasattr( filtSeq, "DecaysFinalStateFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
  filtSeq += DecaysFinalStateFilter()
  pass

DecaysFinalStateFilter = filtSeq.DecaysFinalStateFilter
DecaysFinalStateFilter.PDGAllowedParents = [ -24, 24 ]
DecaysFinalStateFilter.NChargedLeptons = 1




#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal top mass, Perugia 2012 tune, at least two lepton + m12 m34 filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', '2lepton', '4lepton' ]
evgenConfig.contact     = [ 'jahreda@gmail.com','bnachman@cern.ch','roberto.di.nardo@cern.ch' ]
evgenConfig.minevents   = 200
evgenConfig.generators  =['Powheg']

if runArgs.trfSubstepName == 'generate' :
  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 172.5
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 250.
  PowhegConfig.generate()


  hold = runArgs.maxEvents
  evgenConfig.minevents *= 25
  runArgs.maxEvents = 25*(hold-1)
 
#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
#--------------------------------------------------------------
# Event filter leptons
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')
if runArgs.trfSubstepName == 'afterburn':

  include("MC15JobOptions/FourLeptonMassFilter.py")
  #from GeneratorFilters.GeneratorFiltersConf import FourLeptonMassFilter
  #filtSeq += FourLeptonMassFilter('FourLeptonMassFilter')
  filtSeq.FourLeptonMassFilter.MinPt = 4000.
  filtSeq.FourLeptonMassFilter.MaxEta = 4.
  filtSeq.FourLeptonMassFilter.MinMass1 = 40000.
  filtSeq.FourLeptonMassFilter.MaxMass1 = 14000000.
  filtSeq.FourLeptonMassFilter.MinMass2 = 8000.
  filtSeq.FourLeptonMassFilter.MaxMass2 = 14000000.
  filtSeq.FourLeptonMassFilter.AllowElecMu = True
  filtSeq.FourLeptonMassFilter.AllowSameCharge = True


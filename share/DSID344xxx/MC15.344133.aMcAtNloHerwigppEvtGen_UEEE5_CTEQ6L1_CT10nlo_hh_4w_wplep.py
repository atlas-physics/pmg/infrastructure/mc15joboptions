#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------


include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")
## To modify Higgs BR
cmds = """
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff On
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff On
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff On
set /Herwig/Particles/W+/W+->u,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->c,sbar;:OnOff Off
set /Herwig/Particles/W+/W+->sbar,u;:OnOff Off
set /Herwig/Particles/W+/W+->c,dbar;:OnOff Off
set /Herwig/Particles/W+/W+->bbar,c;:OnOff Off
##set W- decay
set /Herwig/Particles/W-/W-->ubar,d;:OnOff On
set /Herwig/Particles/W-/W-->cbar,s;:OnOff On
set /Herwig/Particles/W-/W-->s,ubar;:OnOff On
set /Herwig/Particles/W-/W-->cbar,d;:OnOff On
set /Herwig/Particles/W-/W-->b,cbar;:OnOff On
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff Off
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff Off
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff Off
"""

from Herwigpp_i import config as hw
genSeq.Herwigpp.Commands += cmds.splitlines()
del cmds

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
if (runArgs.runNumber == 344133):
    evgenConfig.description = "SM diHiggs production, decay to WWWW, with MG5_aMC@NLOh, inclusive of box diagrami FF."
    evgenConfig.keywords = ["SM", "SMHiggs", "nonResonant", "WW"]

evgenConfig.contact = ['Biagio Di Miccol <Biagio.di.micco@cern.ch>']
evgenConfig.inputfilecheck = 'aMCatNLO_2.2.3.342053.hh_NLO_EFT_FF_HERWIGPP_CT10' 

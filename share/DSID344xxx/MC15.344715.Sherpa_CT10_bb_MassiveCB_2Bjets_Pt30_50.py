include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa bbb SM background, leading jet pt 30-50 GeV"
evgenConfig.keywords = [ "bbbar", "jets"]
evgenConfig.contact  = [ "timb@slac.stanford.edu" ]
evgenConfig.inputconfcheck = "bb_MassiveCB_2Bjets_Pt30_50"

evgenConfig.process="""
(run){
  MASSIVE[5] 1; 
  MASSIVE[4] 1; 
}(run);

(me){
  EVENT_GENERATION_MODE = PartiallyUnweighted
}(me)

(processes){

# 1 the class anything -> b
Process 93 5 -> 93 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 5 93 -> 93 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 93 -5 -> 93 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -5 93 -> 93 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 5 93 -> 5 4 -4 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 93 5 -> 5 4 -4 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -5 93 -> -5 4 -4 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 93 -5 -> -5 4 -4 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;

# 2 the class anything -> b b~
Process 93 93 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 5 -5 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -5 5 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;

# 3 the class anything -> b b
Process 5 5 -> 5 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -5 -5 -> -5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;

# 4 the class anything -> b b b~
Process 93 5 -> 5 -5 5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 5 93 -> 5 -5 5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 93 -5 -> 5 -5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 93 -> 5 -5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 5 the class anything -> b b b~ b~
Process 93 93 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 5 -5 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 5 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 6 the class anything -> b b b b~
Process 5 5 -> 5 5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 -5 -> -5 -5 -5 5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 7 the class c+x -> b
Process 4 5 -> 4 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 5 4 -> 4 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 4 5 -> 4 5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 5 4 -> 4 5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 4 -5 -> 4 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -5 4 -> 4 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 4 -5 -> 4 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 4 -> 4 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 8 the class c~+x -> b
Process -4 5 -> -4 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -4 5 -> -4 5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 -5 -> -4 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -4 -5 -> -4 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 5 -4 -> -4 5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 5 -4 -> -4 5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 -4 -> -4 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process -5 -4 -> -4 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 9 the class charm+x -> b b~
Process 4 -4 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 4 93 -> 4 5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 93 -> -4 5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 4 -> 5 -5 93{2};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS);
Order (*,0);
End process;
Process 93 4 -> 4 5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 93 -4 -> -4 5 -5 93{1};
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 10 the class x + x  -> b b~ c c~ or c c~ -> b b~ b b~
Process 5 -5 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 4 -4 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 93 93 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 4 -4 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 5 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 4 -> 5 -5 4 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 4 -> 5 -5 5 -5;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 11 the class charm+x -> b b b~
Process 4 5 -> 5 -5 5 4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 4 -5 -> 5 -5 -5 4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 5 -> 5 -5 5 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -4 -5 -> 5 -5 -5 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 5 4 -> 5 -5 5 4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 4 -> 5 -5 -5 4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process 5 -4 -> 5 -5 5 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;
Process -5 -4 -> 5 -5 -5 -4;
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

# 12 the class b b -> b b c c~
Process 5 5 -> 5 5 4 -4
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
Process -5 -5 -> -5 -5 4 -4
Max_Epsilon 0.1
CKKW sqr(20/E_CMS); Cut_Core 1;
Order (*,0);
End process;

}(processes);

(selector){
   NJetFinder  1 20 0 0.4 1 3 10;
}(selector)

"""

include("MC15JobOptions/LeadingDiBjetFilter.py")

filtSeq.LeadingDiBjetFilter.LeadJetPtMin=30.*GeV
filtSeq.LeadingDiBjetFilter.LeadJetPtMax=50.*GeV
filtSeq.LeadingDiBjetFilter.BottomPtMin=5.0*GeV
filtSeq.LeadingDiBjetFilter.BottomEtaMax=2.8
filtSeq.LeadingDiBjetFilter.JetPtMin=15.*GeV
filtSeq.LeadingDiBjetFilter.JetEtaMax=2.8
filtSeq.LeadingDiBjetFilter.DeltaRFromTruth=0.3
filtSeq.LeadingDiBjetFilter.TruthContainerName="AntiKt4TruthJets"
filtSeq.LeadingDiBjetFilter.AcceptSomeLightEvents=False


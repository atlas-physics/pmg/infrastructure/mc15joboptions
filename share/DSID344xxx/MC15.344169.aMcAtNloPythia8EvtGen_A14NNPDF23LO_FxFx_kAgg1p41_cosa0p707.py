# Job options file for producing either exclusive or FxFx-merged events
# at NLO QCD in H+jets using MadGraph5 + MadSpin + Pythia8.
# AUTHOR: David Di Valentino, 23/03/16
# LAST MODIFIED: 24/05/16

######################################################################
# Small script to correct the FxFx file after using MadSpin,
# which currently loses FxFx-specific info when producing the 
# decayed LHE file.
######################################################################

def fix_madspin_file( infile, infile_madspin ) : 
    event_text_array = []
    fname_in = infile
    event_counter = 0
    for line in open(fname_in, 'r') :
        if "<event" in line :
            event_text_array.append(line)
    file_out = open("my_events.lhe", 'w')
    num_events = 0
    fname_in_decayed = infile_madspin
    for line in open(fname_in_decayed, 'r') :
        if "<event>" in line : 
            file_out.write(event_text_array[num_events])
            num_events = num_events + 1
        else : 
            file_out.write(line)

######################################################################
    
#Some setup variables.
options = {}
mode=0
safefactor=5.0

gridpack_mode=True
gridpack_dir='madevent/'

#Define the number of events.
if hasattr(runArgs,"maxEvents") and runArgs.maxEvents > 0:
    options["nevents"]=runArgs.maxEvents*safefactor
else:
    options["nevents"]=5000*safefactor

from MadGraphControl.MadGraphUtils import *

#Used for shell commands.
import subprocess

### DSID lists (extensions can include systematics samples)
test=[344169]

######################################################################
# Configure event generation.
######################################################################

#Make process card.
fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test :
    fcard.write("""
    import model HC_NLO_X0_UFO-heft
    define p = g u c b d s u~ c~ d~ s~ b~
    define j = g u c b d s u~ c~ d~ s~ b~
    generate p p > x0 / t a [QCD] @0
    add process p p > x0 j / t a [QCD] @1
    add process p p > x0 j j / t a [QCD] @2
    output -f""")
    fcard.close()

else : 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

runName='run_01'     
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

#Define process directory.
from MadGraphControl.MadGraphUtils import new_process
process_dir = new_process(grid_pack=gridpack_dir)
options["processdir"] = process_dir

#Fetch default run_card.dat and set parameters
extras = {'lhe_version'   : '2.0',
          'ickkw'         : 3,
          'ptj'           : 10,
          'jetradius'     : 1.0,
          'maxjetflavor'  : 5,
          'pdlabel'       : "'lhapdf'",
          'lhaid'         : 260000,
          'parton_shower' :'PYTHIA8', 
          'reweight_PDF'  : 'True',
          'reweight_scale': 'True',
          'PDF_set_min'   : 260001,
          'PDF_set_max'   : 260100,
          'mll_sf'        : 10.0,
          'muR_ref_fixed' : 125.0,
          'muF1_ref_fixed': 125.0,
          'muF2_ref_fixed': 125.0,
          'QES_ref_fixed' : 125.0,
          'ptgmin'        : 0.0}

#Create parameter config. card.
def writeparamcard(options,masses,parameters):
    from MadGraphControl.MadGraphUtils import build_param_card
    from os.path import join as pathjoin
    build_param_card(param_card_old=pathjoin(options["processdir"],'Cards/param_card.dat'),param_card_new='param_card_new.dat',
                     masses=masses,params=parameters)

#Set couplings here.
writeparamcard(masses={'25': '1.250000e+02'},options=options,
               parameters={'frblock':
                           {'kAza'  : 0.0,
                            'kAgg' : 1.4142,
                            'kAaa'  : 0.0,
                            'cosa' : 0.7071,
                            'kHdwR' : 0.0,
                            'kHaa'  : 0.0,
                            'kAll'  : 0.0,
                            'kHll'  : 0.0,
                            'kAzz'  : 0.0,
                            'kSM' : 1.4142,
                            'kHdwI' : 0.0,
                            'kHdz'  : 0.0,
                            'kAww'  : 0.0,
                            'kHgg' : 1.4142,
                            'kHda'  : 0.0,
                            'kHza'  : 0.0,
                            'kHww'  : 0.0,
                            'kHzz'  : 0.0,
                            'Lambda': 1000.0}})

#Create run card.
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=options["nevents"],rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

madspin_card_loc='madspin_card.dat'

if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open('madspin_card.dat','w')
    fMadSpinCard.write('import Events/'+runName+'/events.lhe.gz\n')                  
    fMadSpinCard.write('set ms_dir MadSpin\n')                                        
else:                                                                               
    os.unlink(gridpack_dir+'Cards/madspin_card.dat')                                  
    fMadSpinCard = open(gridpack_dir+'Cards/madspin_card.dat','w')                    
    fMadSpinCard.write('import '+gridpack_dir+'Events/'+runName+'/events.lhe.gz\n')  
    fMadSpinCard.write('set ms_dir '+gridpack_dir+'MadSpin\n')                        
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')        
fMadSpinCard.write('''
set spinmode none
decay x0 > z z > l+ l- l+ l-
launch''')
fMadSpinCard.close()  

#Create MadSpin decay card.
# madspin_card_loc='madspin_card.dat'                                                                                                                                    

# #A TECHNICAL NOTE: Can't set random seed in MadSpin due to crashes when using "set spinmode none".
# mscard = open(madspin_card_loc,'w')                                                                                                                                    
# mscard.write("""#************************************************************
# #*                        MadSpin                           *
# #*                                                          *
# #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
# #*                                                          *
# #*    Part of the MadGraph5_aMC@NLO Framework:              *
# #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
# #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
# #*                                                          *
# #************************************************************
# #Some options (uncomment to apply)
# #
# # set seed 1
# # set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# # set BW_cut 15                # cut on how far the particle can be off-shell
#  set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#  set spinmode none
# #
# # specify the decay for the final state particles
# decay x0 > z z > l+ l- l+ l-
# # running the actual code
# launch""")
# mscard.close()

print_cards()

#Run the event generation!
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,
         grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=options["nevents"],random_seed=runArgs.randomSeed)

######################################################################
# NOTE: Need to add python hack here if we're using FxFx merging!
######################################################################

print "Have to hack the input files first!"
# shutil.move('madevent/Events/run_01/events.lhe.gz','events_madspin.lhe.gz')
# shutil.move('madevent/Events/run_01/events.lhe','events.lhe')
shutil.move('madevent/Events/run_01_decayed_1/events.lhe.gz','events_madspin.lhe.gz')
shutil.move('madevent/Events/run_01/events.lhe','events.lhe')
unzip = subprocess.Popen(['gunzip','events_madspin.lhe.gz'])
unzip.wait()

#Adding FxFx-specific info to input file.
fix_madspin_file('events.lhe', 'events_madspin.lhe')

#Rename the file over to usable format.
shutil.move('my_events.lhe','unweighted_events.lhe')

#Zip it back up.
rezip = subprocess.Popen(['gzip','unweighted_events.lhe'])
rezip.wait()

#Put it back in the right directory.
shutil.move('unweighted_events.lhe.gz',gridpack_dir+'/Events/'+runName+'/unweighted_events.lhe.gz')

#Now we can run arrange_output as normal ...
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)

print "Now performing parton showering ..."
   
######################################################################
# End of event generation, start configuring parton shower here.
######################################################################

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.keywords    = ['BSM', 'Higgs', 'mH125', 'gluonFusionHiggs', 'resonance', 'ZZ', 'jets']
evgenConfig.contact     = ['David Di Valentino <David.Di.Valentino@cern.ch>']
evgenConfig.generators  = ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.description = """ggF 125 GeV Higgs FxFx merged production in the Higgs Characterization model decaying to zz4l."""

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")   

testSeq.TestHepMC.MaxVtxDisp = 100000000

# Extra configuration options for if we're using FxFx merging.
genSeq.Pythia8.UserHook = 'JetMatchingMadgraph'
genSeq.Pythia8.FxFxXS = True

genSeq.Pythia8.Commands += [
    "JetMatching:setMad           = off",
    "JetMatching:merge            = on",
    "JetMatching:scheme           = 1",
    "JetMatching:nQmatch          = 5",     # Should match maxjetflavour in 'mg5'.
    "JetMatching:jetAlgorithm     = 2",
    "JetMatching:slowJetPower     = 1",  
    "JetMatching:clFact           = 1.0",
    "JetMatching:eTjetMin         = 40.0",  # Should match qcut.
    "JetMatching:coneRadius       = 1.0",   # Default.
    "JetMatching:etaJetMax        = 4.5",
    "JetMatching:exclusive        = 1",     # Exclusive - all PS jets must match HS jets.
    "JetMatching:nJetMax          = 2",     # *** Must match highest Born-level jet multiplicity. ***
    "JetMatching:nJet             = 0", 
    "JetMatching:jetAllow         = 1",
    "JetMatching:doShowerKt       = off",
    "JetMatching:qCut             = 40.0",  # Merging scale for FxFx.
    "JetMatching:doFxFx           = on",
    "JetMatching:qCutME           = 10.0"   # Should match ptj.
    ]

#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 125.0
PowhegConfig.width_H = 0.00407

# CPS for the SM Higgs
PowhegConfig.complexpolescheme = 1

#
PowhegConfig.withdamp = 1

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 4.

# Generate Powheg events
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/nonStandard/Pythia8_AZNLO_CTEQ6L1_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.UserModes += [ 'WeakZ0:gmZmode = 2']
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 553  22',
                             '25:addChannel = 1 0.002 100 100553  22',
                             '25:addChannel = 1 0.002 100 200553  22',
                             '553:onMode = off',
                             '553:onIfMatch = 13 -13',
                             '100553:onMode = off',
                             '100553:onIfMatch = 13 -13',
                             '200553:onMode = off',
                             '200553:onIfMatch = 13 -13'
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "VBF H->UpsilonGamma"
evgenConfig.description = "POWHEG+PYTHIA8, VBF H(125)->UpsilonGamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","VBF","photon", "mH125" ]
evgenConfig.contact     = [ 'J.Broughton@cern.ch' ]
evgenConfig.generators += ["Powheg", "Pythia8" ]
evgenConfig.minevents = 5000

include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "3 charged leptons + neutrino with 2j@LO and mll>5GeV,BVeto,BottomPtMin 5GeV, pTl1>5 GeV, pTl2>5 GeV."
evgenConfig.keywords = ["SM", "diboson", "3lepton", "neutrino"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch","rohin.narayan@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_lllvSFPlus_BFilter"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  SP_NLOCT 1; FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=2; LJET:=4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix;
  EXCLUSIVE_CLUSTER_MODE=1
  METS_CLUSTER_MODE=16
}(run)

(processes){
  Process 93 93 -> 11 -11 -11 12 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 13 -13 -13 14 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> 15 -15 -15 16 93{NJET};
  Order_EW 4; CKKW sqr(QCUT/E_CMS);
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 5 E_CMS
  Mass 13 -13 5 E_CMS
  Mass 15 -15 5 E_CMS
}(selector)
"""
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter
myBFilter=filtSeq.HeavyFlavorBHadronFilter
myBFilter.BottomPtMin=5*GeV

filtSeq.Expression = "(not HeavyFlavorBHadronFilter)"
evgenConfig.generators  = [ "Sherpa"] 

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413741E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401076E+03   # H
        36     2.00000000E+03   # A
        37     2.00151511E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994926E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994926E+03   # ~c_R
   1000005     4.99949326E+03   # ~b_1
   2000005     5.00066640E+03   # ~b_2
   1000006     4.98751277E+03   # ~t_1
   2000006     5.01691428E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     2.60000000E+03   # ~g
   1000022     1.40098561E+03   # ~chi_10
   1000023    -1.45008040E+03   # ~chi_20
   1000025     1.48816894E+03   # ~chi_30
   1000035     3.00392585E+03   # ~chi_40
   1000024     1.44618516E+03   # ~chi_1+
   1000037     3.00392292E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19596632E-01   # N_11
  1  2    -3.36079272E-02   # N_12
  1  3     4.92117498E-01   # N_13
  1  4    -4.88744885E-01   # N_14
  2  1     3.01757793E-03   # N_21
  2  2    -3.48704901E-03   # N_22
  2  3    -7.07001239E-01   # N_23
  2  4    -7.07197272E-01   # N_24
  3  1     6.94384320E-01   # N_31
  3  2     3.68759702E-02   # N_32
  3  3    -5.06846558E-01   # N_33
  3  4     5.09487139E-01   # N_34
  4  1     1.41319049E-03   # N_41
  4  2    -9.98748472E-01   # N_42
  4  3    -3.28052163E-02   # N_43
  4  4     3.77267827E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.63556144E-02   # U_11
  1  2     9.98925001E-01   # U_12
  2  1     9.98925001E-01   # U_21
  2  2     4.63556144E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.33162952E-02   # V_11
  1  2     9.98577675E-01   # V_12
  2  1     9.98577675E-01   # V_21
  2  2     5.33162952E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07789278E-01   # cos(theta_t)
  1  2     7.06423625E-01   # sin(theta_t)
  2  1    -7.06423625E-01   # -sin(theta_t)
  2  2     7.07789278E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73915948E-01   # cos(theta_b)
  1  2     7.38808023E-01   # sin(theta_b)
  2  1    -7.38808023E-01   # -sin(theta_b)
  2  2     6.73915948E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04328063E-01   # cos(theta_tau)
  1  2     7.09874623E-01   # sin(theta_tau)
  2  1    -7.09874623E-01   # -sin(theta_tau)
  2  2     7.04328063E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202817E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52209320E+02   # vev(Q)              
         4     3.27082022E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52759239E-01   # gprime(Q) DRbar
     2     6.27025650E-01   # g(Q) DRbar
     3     1.07628003E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02531805E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71476943E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79781080E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.92762229E+05   # M^2_Hd              
        22    -7.25300068E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36981931E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.14436962E-03   # gluino decays
#          BR         NDA      ID1       ID2
     6.58723840E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.25985532E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     6.09372795E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.26854795E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.95055381E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.99212548E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.56969598E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.27611225E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.16986280E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.26854795E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.95055381E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.99212548E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.56969598E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.27611225E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.16986280E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.34307286E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.72035370E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.08374348E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.06048312E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.79054562E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.13366873E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.24133162E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.24133162E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.24133162E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.24133162E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.15825825E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.15825825E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.56284108E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.10598672E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.19776329E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.70429552E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.29723271E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.26146794E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.56748711E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.77326857E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.43774939E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.22704316E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20995714E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.75368532E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.85373255E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.14614652E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.67679087E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.69277115E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.79852766E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.84309666E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.01463906E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.96143081E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.81647971E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.38213952E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.66609455E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.98054314E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.93331286E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.03761101E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.25367125E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.54547888E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.30222798E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.82095053E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.64800704E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.41694140E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.67268325E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.80817737E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.05207277E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.60916927E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.60833719E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36995282E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.21843398E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.57604501E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.52349979E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.05534613E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.30829940E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.78369258E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.70315120E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.41609025E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.67275796E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.71349290E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55005019E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.14227866E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.61374474E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.05942606E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.22587502E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.57668589E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.45683549E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.98805915E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.38783053E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.27783890E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.49110500E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.84733948E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.67268325E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.80817737E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.05207277E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.60916927E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.60833719E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36995282E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.21843398E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.57604501E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.52349979E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.05534613E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.30829940E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.78369258E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.70315120E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.41609025E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.67275796E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.71349290E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55005019E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.14227866E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.61374474E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.05942606E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.22587502E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.57668589E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.45683549E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.98805915E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.38783053E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.27783890E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.49110500E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.84733948E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.92198364E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.83144247E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.79749588E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01610563E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72198816E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.41503005E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45459369E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08016654E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.23307297E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.09080064E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76682641E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.70713852E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.92198364E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.83144247E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.79749588E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01610563E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72198816E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.41503005E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45459369E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08016654E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.23307297E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.09080064E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76682641E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.70713852E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.50563315E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.70308699E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45624540E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.53789293E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.57006691E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.67854567E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.14627602E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.43544132E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50496719E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.56058501E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40228392E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61065596E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60467485E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.12597952E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21555592E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.92208730E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09230473E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.50891716E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.95702088E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.73014442E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19443357E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.44975353E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.92208730E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09230473E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.50891716E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.95702088E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.73014442E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19443357E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.44975353E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92478342E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09129782E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.50752621E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.95060776E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72762772E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.11171171E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44474581E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22363109E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.94746890E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.24305564E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.22740153E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08101981E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08099048E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07278565E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.72302302E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53265829E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11771574E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46264186E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35137217E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53553660E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.53379843E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.54033719E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95906035E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92842169E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12517959E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.49676196E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.72939586E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.17569449E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.35750629E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.30339546E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.15800790E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.49946130E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15046377E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.49925838E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37051207E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.42459350E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.42447496E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.39114006E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.83915547E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.83915547E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.83915547E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.20668734E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.20668734E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.16826212E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.16826212E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.35562494E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.35562494E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.32868755E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.32868755E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.67714650E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.67714650E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18677879E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.77338529E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.82205280E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.49275792E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.30076111E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.72487412E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.03532994E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22800345E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.44130268E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.23560059E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.67230860E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.11926095E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.11923679E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.33018913E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.02308394E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.02308394E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.02308394E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.23440622E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.89325308E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.21233705E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.89262497E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.50037358E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.60786152E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.60752889E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.51361906E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.31962922E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.31962922E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.31962922E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31133958E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31133958E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30398583E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30398583E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37112552E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37112552E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37098765E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37098765E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33246082E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33246082E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.72303392E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.71063433E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51810516E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54811068E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46486274E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46486274E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14406167E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20313266E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38374229E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80475991E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.72813498E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.43276269E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.35023962E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.47338867E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08727230E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17919251E-01    2           5        -5   # BR(h -> b       bb     )
     6.37240130E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25559537E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77980993E-04    2           3        -3   # BR(h -> s       sb     )
     2.06210612E-02    2           4        -4   # BR(h -> c       cb     )
     6.69566528E-02    2          21        21   # BR(h -> g       g      )
     2.30272203E-03    2          22        22   # BR(h -> gam     gam    )
     1.53692161E-03    2          22        23   # BR(h -> Z       gam    )
     2.00628625E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56072125E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78101264E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46746074E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427399E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71212600E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547037E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668415E-05    2           4        -4   # BR(H -> c       cb     )
     9.96067419E-01    2           6        -6   # BR(H -> t       tb     )
     7.97666783E-04    2          21        21   # BR(H -> g       g      )
     2.71270896E-06    2          22        22   # BR(H -> gam     gam    )
     1.16011324E-06    2          23        22   # BR(H -> Z       gam    )
     3.34869391E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66978023E-04    2          23        23   # BR(H -> Z       Z      )
     9.03252054E-04    2          25        25   # BR(H -> h       h      )
     7.38141087E-24    2          36        36   # BR(H -> A       A      )
     2.96187712E-11    2          23        36   # BR(H -> Z       A      )
     6.73168981E-12    2          24       -37   # BR(H -> W+      H-     )
     6.73168981E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381056E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47039459E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898495E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269285E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677757E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179645E-06    2           4        -4   # BR(A -> c       cb     )
     9.96999157E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678643E-04    2          21        21   # BR(A -> g       g      )
     3.16290754E-06    2          22        22   # BR(A -> gam     gam    )
     1.35269880E-06    2          23        22   # BR(A -> Z       gam    )
     3.26394887E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471978E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35625872E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238450E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147954E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49801694E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695976E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731992E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402062E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34519681E-04    2          24        25   # BR(H+ -> W+      h      )
     2.80385213E-13    2          24        36   # BR(H+ -> W+      A      )

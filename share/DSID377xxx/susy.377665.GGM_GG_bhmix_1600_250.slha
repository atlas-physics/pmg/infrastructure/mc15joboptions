#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.90000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375330E+01   # W+
        25     1.25000000E+02   # h
        35     2.00416785E+03   # H
        36     2.00000000E+03   # A
        37     2.00208349E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013325E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989208E+03   # ~u_L
   2000002     4.99994934E+03   # ~u_R
   1000003     5.00013325E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989208E+03   # ~c_L
   2000004     4.99994934E+03   # ~c_R
   1000005     4.99996563E+03   # ~b_1
   2000005     5.00019439E+03   # ~b_2
   1000006     4.99969926E+03   # ~t_1
   2000006     5.00476801E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001288E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     1.60000000E+03   # ~g
   1000022     2.20550062E+02   # ~chi_10
   1000023    -2.50217200E+02   # ~chi_20
   1000025     3.17401770E+02   # ~chi_30
   1000035     3.00226537E+03   # ~chi_40
   1000024     2.47883525E+02   # ~chi_1+
   1000037     3.00226488E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.30445255E-01   # N_11
  1  2    -2.36118806E-02   # N_12
  1  3     6.06648170E-01   # N_13
  1  4    -5.91648805E-01   # N_14
  2  1     1.63094060E-02   # N_21
  2  2    -4.82915527E-03   # N_22
  2  3    -7.05467873E-01   # N_23
  2  4    -7.08537764E-01   # N_24
  3  1     8.47562160E-01   # N_31
  3  2     1.54230955E-02   # N_32
  3  3    -3.66085380E-01   # N_33
  3  4     3.83903643E-01   # N_34
  4  1     4.68653409E-04   # N_41
  4  2    -9.99590559E-01   # N_42
  4  3    -1.65702446E-02   # N_43
  4  4     2.33221114E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.34252528E-02   # U_11
  1  2     9.99725591E-01   # U_12
  2  1     9.99725591E-01   # U_21
  2  2     2.34252528E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.29777607E-02   # V_11
  1  2     9.99456086E-01   # V_12
  2  1     9.99456086E-01   # V_21
  2  2     3.29777607E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.11087583E-01   # cos(theta_t)
  1  2     7.03103441E-01   # sin(theta_t)
  2  1    -7.03103441E-01   # -sin(theta_t)
  2  2     7.11087583E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13935777E-01   # cos(theta_b)
  1  2     8.57828664E-01   # sin(theta_b)
  2  1    -8.57828664E-01   # -sin(theta_b)
  2  2     5.13935777E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89410071E-01   # cos(theta_tau)
  1  2     7.24371282E-01   # sin(theta_tau)
  2  1    -7.24371282E-01   # -sin(theta_tau)
  2  2     6.89410071E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90208166E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51795098E+02   # vev(Q)              
         4     3.84227788E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53061981E-01   # gprime(Q) DRbar
     2     6.28967137E-01   # g(Q) DRbar
     3     1.08402977E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02684821E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72319634E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79955094E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.90000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.60000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99429962E+06   # M^2_Hd              
        22    -5.52125318E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37847986E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.14935785E-03   # gluino decays
#          BR         NDA      ID1       ID2
     9.31865586E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.32176758E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.67529260E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.23999958E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.44226520E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.53532238E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     6.71397864E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.74068545E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.62061624E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.23999958E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.44226520E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.53532238E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     6.71397864E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.74068545E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.62061624E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.36194840E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.84382681E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.59234685E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.45911495E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.03821094E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.25433605E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.52336127E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.52336127E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.52336127E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.52336127E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.32103047E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.32103047E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.38926370E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.99645248E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.08530746E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.34551961E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09700563E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.06290019E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.18634608E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.28925997E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.32047558E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.96864925E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.07726083E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.46573621E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.24081889E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.09460430E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.47478920E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.21313551E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.93308355E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.61799553E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.12501701E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.02291223E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -3.88653155E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -8.87821327E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -7.82544643E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.08553449E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.25472591E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.40332484E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.44960361E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.77390026E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.62467352E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.74640353E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.26742591E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.44826434E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.33926580E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     4.62723820E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.55240804E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.43907279E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.32878073E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.75734523E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.65694068E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.97065000E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.20418177E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.35285507E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.27756509E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.44008591E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.35103326E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.52057810E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.33929879E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.22089405E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.01772015E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.65105104E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.33042223E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.86744538E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.66147778E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.97115363E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.12497365E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.50826057E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.31301665E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.92092543E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.12835990E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87567500E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.33926580E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     4.62723820E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.55240804E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.43907279E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.32878073E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.75734523E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.65694068E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.97065000E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.20418177E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.35285507E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.27756509E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.44008591E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.35103326E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.52057810E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.33929879E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.22089405E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.01772015E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.65105104E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.33042223E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.86744538E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.66147778E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.97115363E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.12497365E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.50826057E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.31301665E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.92092543E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.12835990E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87567500E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03216836E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.85794745E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.20824694E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.55300352E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.64992295E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.08665148E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30407131E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46288085E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.82218050E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.66497801E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.17515362E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.04347151E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03216836E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.85794745E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.20824694E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.55300352E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.64992295E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.08665148E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30407131E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46288085E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.82218050E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.66497801E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.17515362E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.04347151E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.74105318E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.64662658E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.29204276E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15250470E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39120751E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.94189729E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78463762E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31257631E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.76397005E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.42324896E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.79336747E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.98226337E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.52738723E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.07210069E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.05719986E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03202651E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.67572005E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.26263352E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.36407137E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65242163E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40447493E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30062761E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03202651E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.67572005E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.26263352E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.36407137E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65242163E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40447493E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30062761E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.03523101E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.66867201E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26130047E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.36263123E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.64962128E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.45837798E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29503521E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.11072600E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.15583827E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.35581384E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.31140063E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11860559E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11852174E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09534262E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.18406615E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.11694024E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.41939206E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.06690687E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.44194141E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.12291878E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.06331435E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.08830479E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.05021839E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.05534258E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.03841981E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     8.79187407E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.99602614E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.01882706E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.91911172E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.20612180E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.34930673E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.53069019E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.17677496E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.36784708E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.27783210E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.21062197E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.56699996E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.19189679E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.56644949E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.23151134E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.57614125E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.57586741E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.49808230E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.13999459E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.13999459E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.13999459E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.89537994E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.89537994E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.67627081E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.67627081E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.31793426E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.31793426E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.25388763E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.25388763E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.03925247E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.03925247E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.14094829E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99908884E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     6.58180481E-05    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     2.48989735E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.99365294E-07    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.18410485E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.65326602E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.06077059E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.13293851E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.06321401E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.06321401E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42452950E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.15552453E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.83318909E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.90139241E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.77115482E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.74991167E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.41767825E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     6.24016880E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.75896494E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.08110798E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.08110798E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.10788505E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.68321622E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.51574657E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.97960117E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.05323073E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08297685E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17390724E-01    2           5        -5   # BR(h -> b       bb     )
     6.37920719E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25800440E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78491346E-04    2           3        -3   # BR(h -> s       sb     )
     2.06426074E-02    2           4        -4   # BR(h -> c       cb     )
     6.70278980E-02    2          21        21   # BR(h -> g       g      )
     2.31776212E-03    2          22        22   # BR(h -> gam     gam    )
     1.53832724E-03    2          22        23   # BR(h -> Z       gam    )
     2.00952166E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56341517E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01280195E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38971691E-03    2           5        -5   # BR(H -> b       bb     )
     2.32209683E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20947682E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05110132E-06    2           3        -3   # BR(H -> s       sb     )
     9.48614802E-06    2           4        -4   # BR(H -> c       cb     )
     9.38614569E-01    2           6        -6   # BR(H -> t       tb     )
     7.51634559E-04    2          21        21   # BR(H -> g       g      )
     2.63971276E-06    2          22        22   # BR(H -> gam     gam    )
     1.09207177E-06    2          23        22   # BR(H -> Z       gam    )
     3.17137931E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58136395E-04    2          23        23   # BR(H -> Z       Z      )
     8.53094305E-04    2          25        25   # BR(H -> h       h      )
     8.56176142E-24    2          36        36   # BR(H -> A       A      )
     3.38147110E-11    2          23        36   # BR(H -> Z       A      )
     2.57805203E-12    2          24       -37   # BR(H -> W+      H-     )
     2.57805203E-12    2         -24        37   # BR(H -> W-      H+     )
     7.97791400E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.12322682E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.75325595E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.61371405E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.82172339E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.43268821E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.71759985E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05770536E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39251763E-03    2           5        -5   # BR(A -> b       bb     )
     2.29839665E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12566243E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07125128E-06    2           3        -3   # BR(A -> s       sb     )
     9.38757724E-06    2           4        -4   # BR(A -> c       cb     )
     9.39529997E-01    2           6        -6   # BR(A -> t       tb     )
     8.89282991E-04    2          21        21   # BR(A -> g       g      )
     2.65445715E-06    2          22        22   # BR(A -> gam     gam    )
     1.27350693E-06    2          23        22   # BR(A -> Z       gam    )
     3.09078185E-04    2          23        25   # BR(A -> Z       h      )
     6.64180474E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.64650498E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.78128798E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.01355761E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.43298590E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     8.38270350E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.54049530E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97703819E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23443428E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34745847E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29911366E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42063045E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13945342E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02405546E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41267283E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16785027E-04    2          24        25   # BR(H+ -> W+      h      )
     1.29790990E-12    2          24        36   # BR(H+ -> W+      A      )
     1.63447137E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.41596878E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.16815047E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.45000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05413748E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401311E+03   # H
        36     2.00000000E+03   # A
        37     2.00151325E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002537E+03   # ~d_R
   1000002     4.99989245E+03   # ~u_L
   2000002     4.99994926E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002537E+03   # ~s_R
   1000004     4.99989245E+03   # ~c_L
   2000004     4.99994926E+03   # ~c_R
   1000005     4.99949306E+03   # ~b_1
   2000005     5.00066661E+03   # ~b_2
   1000006     4.98751213E+03   # ~t_1
   2000006     5.01691550E+03   # ~t_2
   1000011     5.00008218E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008218E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99969262E+03   # ~tau_1
   2000015     5.00046627E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     1.40098555E+03   # ~chi_10
   1000023    -1.45008040E+03   # ~chi_20
   1000025     1.48816900E+03   # ~chi_30
   1000035     3.00392585E+03   # ~chi_40
   1000024     1.44618516E+03   # ~chi_1+
   1000037     3.00392292E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.19596610E-01   # N_11
  1  2    -3.36079322E-02   # N_12
  1  3     4.92117516E-01   # N_13
  1  4    -4.88744899E-01   # N_14
  2  1     3.01758213E-03   # N_21
  2  2    -3.48704956E-03   # N_22
  2  3    -7.07001239E-01   # N_23
  2  4    -7.07197273E-01   # N_24
  3  1     6.94384343E-01   # N_31
  3  2     3.68759763E-02   # N_32
  3  3    -5.06846540E-01   # N_33
  3  4     5.09487125E-01   # N_34
  4  1     1.41319268E-03   # N_41
  4  2    -9.98748472E-01   # N_42
  4  3    -3.28052214E-02   # N_43
  4  4     3.77267887E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.63556216E-02   # U_11
  1  2     9.98925000E-01   # U_12
  2  1     9.98925000E-01   # U_21
  2  2     4.63556216E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.33163034E-02   # V_11
  1  2     9.98577674E-01   # V_12
  2  1     9.98577674E-01   # V_21
  2  2     5.33163034E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07789234E-01   # cos(theta_t)
  1  2     7.06423669E-01   # sin(theta_t)
  2  1    -7.06423669E-01   # -sin(theta_t)
  2  2     7.07789234E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.73927694E-01   # cos(theta_b)
  1  2     7.38797309E-01   # sin(theta_b)
  2  1    -7.38797309E-01   # -sin(theta_b)
  2  2     6.73927694E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04328191E-01   # cos(theta_tau)
  1  2     7.09874496E-01   # sin(theta_tau)
  2  1    -7.09874496E-01   # -sin(theta_tau)
  2  2     7.04328191E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90204423E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.45000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52209780E+02   # vev(Q)              
         4     3.29500009E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52759085E-01   # gprime(Q) DRbar
     2     6.27024603E-01   # g(Q) DRbar
     3     1.07892724E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02538105E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71571047E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79780749E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.44300000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.91769452E+05   # M^2_Hd              
        22    -7.32486390E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36981471E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.89582307E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.63779377E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.95033358E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.35532076E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.89957685E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.80176170E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.74978206E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.75769574E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.16130986E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.07510708E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.89957685E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.80176170E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.74978206E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.75769574E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.16130986E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.07510708E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.96859061E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.70755379E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.83954840E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     8.67988041E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.15645725E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     7.82678405E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.27612179E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.27612179E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.27612179E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.27612179E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.85557363E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.85557363E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.85442905E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.20848820E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.08045116E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.24414488E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.16482590E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.13804526E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.30536346E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.18922134E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.74117332E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.78260146E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08212938E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.71625350E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.65081073E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.02494760E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.27421747E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.15053470E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.09570516E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.01746534E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.75781510E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.53232386E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.57083980E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.20461013E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.17050504E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.24488171E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.22484945E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.65338179E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.09563277E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.10157371E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.99183181E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.57893309E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.02137839E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.76110071E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96555548E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.36516692E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.86711696E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.22513423E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.92455057E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.57972140E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.85059967E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.78728188E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.81637936E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.56834775E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.46202639E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.33982220E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.78707701E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.50917806E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96562143E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.31429770E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.32195889E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.94392840E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.92919912E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.46208676E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.85702332E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.78781554E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.74956746E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.66620442E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.15812946E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.07306557E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.24253241E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87260602E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96555548E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.36516692E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.86711696E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.22513423E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.92455057E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.57972140E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.85059967E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.78728188E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.81637936E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.56834775E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.46202639E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.33982220E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.78707701E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.50917806E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96562143E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.31429770E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.32195889E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.94392840E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.92919912E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.46208676E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.85702332E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.78781554E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.74956746E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.66620442E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.15812946E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.07306557E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.24253241E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87260602E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.92197517E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.83145948E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.79748724E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01610755E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.72198695E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.41502973E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.45459128E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08016472E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.23307273E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.09082595E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.76682666E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.70716861E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.92197517E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.83145948E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.79748724E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01610755E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.72198695E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.41502973E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.45459128E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08016472E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.23307273E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.09082595E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.76682666E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.70716861E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.50562815E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.70308927E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45623693E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.53789535E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.57006536E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.67854125E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.14627293E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.43544419E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.50496186E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.56058888E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40227525E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.61065968E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60467232E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.12597662E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21555087E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.92207883E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09230672E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.50891941E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.95703693E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.73014323E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.19443315E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.44975113E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.92207883E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09230672E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.50891941E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.95703693E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.73014323E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.19443315E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.44975113E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.92477494E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09129981E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.50752846E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.95062381E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72762654E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.11171057E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.44474341E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.22363176E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.94746728E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.24305567E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.22740160E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08101982E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08099049E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07278569E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.72301249E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53265927E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.11771546E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46264133E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.35137190E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53553669E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.53381972E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     8.54041468E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95905779E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92842608E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12516124E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.49676305E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.72933575E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.17570063E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     9.35749961E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.30339526E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.15800751E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.49946109E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15046339E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.49925818E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37051218E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.42459447E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.42447593E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.39114108E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.83915835E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.83915835E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.83915835E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.20667093E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.20667093E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.16825343E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.16825343E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.35557022E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.35557022E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.32863303E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.32863303E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.67712659E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.67712659E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.18678123E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.77332385E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.82210549E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.49275588E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.30076064E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.72481094E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.03535045E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22802918E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.44132304E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.23562638E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.67336730E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.11931357E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.11928941E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.33025625E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.02320815E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.02320815E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.02320815E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.23440788E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.89325583E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.21233877E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.89262772E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.50037717E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.60787058E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.60753795E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.51362824E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.31963121E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.31963121E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.31963121E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31133926E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31133926E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30398553E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30398553E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.37112444E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.37112444E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.37098657E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.37098657E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.33245987E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.33245987E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.72302340E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.71063698E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51810615E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.54811338E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46486218E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46486218E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.14406181E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.20311250E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.38374246E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.80476839E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.72814781E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.43270680E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.35021029E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.47339325E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08674425E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17869808E-01    2           5        -5   # BR(h -> b       bb     )
     6.37325524E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25589763E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78045002E-04    2           3        -3   # BR(h -> s       sb     )
     2.06236812E-02    2           4        -4   # BR(h -> c       cb     )
     6.69651344E-02    2          21        21   # BR(h -> g       g      )
     2.30302092E-03    2          22        22   # BR(h -> gam     gam    )
     1.53712043E-03    2          22        23   # BR(h -> Z       gam    )
     2.00654526E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56105210E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78104086E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46833910E-03    2           5        -5   # BR(H -> b       bb     )
     2.46425318E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71205244E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546072E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668248E-05    2           4        -4   # BR(H -> c       cb     )
     9.96065831E-01    2           6        -6   # BR(H -> t       tb     )
     7.97664704E-04    2          21        21   # BR(H -> g       g      )
     2.71271256E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010447E-06    2          23        22   # BR(H -> Z       gam    )
     3.35357128E-04    2          24       -24   # BR(H -> W+      W-     )
     1.67221227E-04    2          23        23   # BR(H -> Z       Z      )
     9.03235642E-04    2          25        25   # BR(H -> h       h      )
     7.36687982E-24    2          36        36   # BR(H -> A       A      )
     2.97054102E-11    2          23        36   # BR(H -> Z       A      )
     6.78860315E-12    2          24       -37   # BR(H -> W+      H-     )
     6.78860315E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381576E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47127646E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898163E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62268112E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677602E-06    2           3        -3   # BR(A -> s       sb     )
     9.96178290E-06    2           4        -4   # BR(A -> c       cb     )
     9.96997800E-01    2           6        -6   # BR(A -> t       tb     )
     9.43677359E-04    2          21        21   # BR(A -> g       g      )
     3.16290325E-06    2          22        22   # BR(A -> gam     gam    )
     1.35269720E-06    2          23        22   # BR(A -> Z       gam    )
     3.26871120E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472302E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35817300E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238003E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81146374E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49924209E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695066E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731811E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99401573E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.35006990E-04    2          24        25   # BR(H+ -> W+      h      )
     2.78668865E-13    2          24        36   # BR(H+ -> W+      A      )

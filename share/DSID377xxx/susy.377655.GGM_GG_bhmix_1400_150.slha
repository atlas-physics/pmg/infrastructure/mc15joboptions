#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.02000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05377091E+01   # W+
        25     1.25000000E+02   # h
        35     2.00415843E+03   # H
        36     2.00000000E+03   # A
        37     2.00209667E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013344E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989189E+03   # ~u_L
   2000002     4.99994933E+03   # ~u_R
   1000003     5.00013344E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989189E+03   # ~c_L
   2000004     4.99994933E+03   # ~c_R
   1000005     4.99999897E+03   # ~b_1
   2000005     5.00016125E+03   # ~b_2
   1000006     5.00071241E+03   # ~t_1
   2000006     5.00375328E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984122E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984122E+03   # ~nu_muL
   1000015     5.00003958E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984122E+03   # ~nu_tauL
   1000021     1.40000000E+03   # ~g
   1000022     1.36440157E+02   # ~chi_10
   1000023    -1.50249824E+02   # ~chi_20
   1000025     3.13615226E+02   # ~chi_30
   1000035     3.00219444E+03   # ~chi_40
   1000024     1.47959374E+02   # ~chi_1+
   1000037     3.00219399E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     2.54062394E-01   # N_11
  1  2    -2.61865542E-02   # N_12
  1  3     6.90096155E-01   # N_13
  1  4    -6.77151284E-01   # N_14
  2  1     1.96736875E-02   # N_21
  2  2    -5.03510356E-03   # N_22
  2  3    -7.04013718E-01   # N_23
  2  4    -7.09895964E-01   # N_24
  3  1     9.66987610E-01   # N_31
  3  2     7.45365989E-03   # N_32
  3  3    -1.66982228E-01   # N_33
  3  4     1.92344327E-01   # N_34
  4  1     4.55693768E-04   # N_41
  4  2    -9.99616604E-01   # N_42
  4  3    -1.57771360E-02   # N_43
  4  4     2.27490496E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.23039645E-02   # U_11
  1  2     9.99751236E-01   # U_12
  2  1     9.99751236E-01   # U_21
  2  2     2.23039645E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.21678480E-02   # V_11
  1  2     9.99482481E-01   # V_12
  2  1     9.99482481E-01   # V_21
  2  2     3.21678480E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13750978E-01   # cos(theta_t)
  1  2     7.00399558E-01   # sin(theta_t)
  2  1    -7.00399558E-01   # -sin(theta_t)
  2  2     7.13750978E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08534223E-01   # cos(theta_b)
  1  2     9.12743003E-01   # sin(theta_b)
  2  1    -9.12743003E-01   # -sin(theta_b)
  2  2     4.08534223E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76620794E-01   # cos(theta_tau)
  1  2     7.36331652E-01   # sin(theta_tau)
  2  1    -7.36331652E-01   # -sin(theta_tau)
  2  2     6.76620794E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90206736E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51754880E+02   # vev(Q)              
         4     3.88198492E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146719E-01   # gprime(Q) DRbar
     2     6.29570550E-01   # g(Q) DRbar
     3     1.08619176E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02676625E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72351888E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79967993E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.02000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04073682E+06   # M^2_Hd              
        22    -5.50260639E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111852E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.10813889E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.56400225E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.71927921E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.10942870E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.79525580E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.59876867E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.39775535E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.46573073E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.04263609E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.87475098E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.79525580E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.59876867E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.39775535E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.46573073E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.04263609E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.87475098E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.51528553E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.92631346E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.41224140E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.89090096E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.03444268E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.20274295E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.06825046E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.06825046E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.06825046E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.06825046E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36179657E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36179657E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.46917660E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09049489E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.06809610E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.06147273E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.13402797E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.69158908E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.26066208E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.42663383E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.45251330E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.49967681E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.04290542E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.53757707E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.13381600E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.12886831E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.26196349E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.28492293E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.74240491E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.03364220E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.72745609E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.82789096E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.59690405E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.34027999E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -3.20721750E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.26258392E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.65292834E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.92578802E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.28755924E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -3.92038550E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.89830759E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.09973039E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.81539026E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.02826566E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.44208121E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.53802444E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.43233688E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.74242621E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.19420830E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.61086806E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.38779344E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.01240946E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.30668319E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.97610812E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.78406860E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.28492402E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.93191260E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.54156807E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.44210843E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.22796464E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.90989226E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.32539114E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.19576192E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.74455419E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.39215803E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.01289258E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.22741915E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     7.70515492E-04    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.61896025E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.10936862E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.01800813E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88131178E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.44208121E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.53802444E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.43233688E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.74242621E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.19420830E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.61086806E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.38779344E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.01240946E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.30668319E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.97610812E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.78406860E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.28492402E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.93191260E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.54156807E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.44210843E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.22796464E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.90989226E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.32539114E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.19576192E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.74455419E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.39215803E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.01289258E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.22741915E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     7.70515492E-04    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.61896025E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.10936862E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.01800813E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88131178E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03671647E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.77129414E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.33320971E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.94740379E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65135281E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.44755595E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30684959E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46265753E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.49347008E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.89251188E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.34675962E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.55556624E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03671647E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.77129414E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.33320971E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.94740379E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65135281E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.44755595E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30684959E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46265753E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.49347008E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.89251188E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.34675962E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.55556624E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.73305830E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     4.01233324E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57101479E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.53161766E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.34678305E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.88032339E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.69567527E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31645107E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.77632196E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.87978094E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06905432E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.97605549E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.57423481E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.20365215E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.15092070E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03656932E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.84474847E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.67358518E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.84324304E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65377737E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.34114272E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30341972E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03656932E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.84474847E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.67358518E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.84324304E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65377737E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.34114272E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30341972E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.03978459E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.84279722E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.67181497E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.84129339E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65097039E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.39711652E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29781352E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.85942641E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.68105605E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.45730285E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.21134953E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.15243477E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.15194955E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.02639519E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.13814458E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.04165564E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.82263086E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.99683623E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.65614199E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.04463909E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.81096833E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.83590401E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.59828044E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.79293206E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     2.47262954E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     8.91966428E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.81889651E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.97594226E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     5.02284541E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.21232978E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     3.49573728E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.17688733E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     5.44595756E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.12096188E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.09142067E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.32288066E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.71209844E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.23569073E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.70941842E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.24459382E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.90636535E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.90515872E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.54628581E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.79858645E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.79858645E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.79858645E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.62882316E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.62882316E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     8.52454024E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     8.52454024E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.20960818E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.20960818E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.18636305E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.18636305E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.84576879E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.84576879E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     7.13453565E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.99318854E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.40097281E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.80950682E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.80950682E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.95525070E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.83091113E-04    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.51226278E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     8.58262437E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.47960630E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.13818167E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.45022056E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.98105255E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.88801817E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.99049918E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.99049918E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.81866017E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.58471440E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.33885742E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.59890353E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.53074998E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.52763530E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.62308483E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.81484833E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.39598358E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.82843215E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.82843215E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.13634874E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.78256912E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.55004228E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.80550937E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.10696293E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08266833E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17356070E-01    2           5        -5   # BR(h -> b       bb     )
     6.37966202E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25816540E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78525500E-04    2           3        -3   # BR(h -> s       sb     )
     2.06442069E-02    2           4        -4   # BR(h -> c       cb     )
     6.70331414E-02    2          21        21   # BR(h -> g       g      )
     2.32888315E-03    2          22        22   # BR(h -> gam     gam    )
     1.53845304E-03    2          22        23   # BR(h -> Z       gam    )
     2.00962194E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56360890E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01381415E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38980472E-03    2           5        -5   # BR(H -> b       bb     )
     2.32150478E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20738371E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05083405E-06    2           3        -3   # BR(H -> s       sb     )
     9.48367692E-06    2           4        -4   # BR(H -> c       cb     )
     9.38369819E-01    2           6        -6   # BR(H -> t       tb     )
     7.51442286E-04    2          21        21   # BR(H -> g       g      )
     2.63034694E-06    2          22        22   # BR(H -> gam     gam    )
     1.09184376E-06    2          23        22   # BR(H -> Z       gam    )
     3.16642434E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57889326E-04    2          23        23   # BR(H -> Z       Z      )
     8.52927882E-04    2          25        25   # BR(H -> h       h      )
     8.40584495E-24    2          36        36   # BR(H -> A       A      )
     3.34260537E-11    2          23        36   # BR(H -> Z       A      )
     2.44068972E-12    2          24       -37   # BR(H -> W+      H-     )
     2.44068972E-12    2         -24        37   # BR(H -> W-      H+     )
     8.42577288E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.14145773E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.45752346E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     9.36339107E-05    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     5.04249726E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.74297054E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.04421661E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05855000E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39266538E-03    2           5        -5   # BR(A -> b       bb     )
     2.29791832E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12397137E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07102834E-06    2           3        -3   # BR(A -> s       sb     )
     9.38562356E-06    2           4        -4   # BR(A -> c       cb     )
     9.39334468E-01    2           6        -6   # BR(A -> t       tb     )
     8.89097920E-04    2          21        21   # BR(A -> g       g      )
     2.69489655E-06    2          22        22   # BR(A -> gam     gam    )
     1.27329777E-06    2          23        22   # BR(A -> Z       gam    )
     3.08613276E-04    2          23        25   # BR(A -> Z       h      )
     6.35172014E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     9.73035369E-03    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.66480832E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     6.15440683E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     5.35237065E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.92917700E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.96123490E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97803697E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23486930E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34688453E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29708458E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42091117E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13819228E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02380417E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41037031E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16301242E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33914176E-12    2          24        36   # BR(H+ -> W+      A      )
     3.76579367E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.33936406E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.44988808E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

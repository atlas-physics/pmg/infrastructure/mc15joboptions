#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.78800000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.80000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419020E+01   # W+
        25     1.25000000E+02   # h
        35     2.00399948E+03   # H
        36     2.00000000E+03   # A
        37     2.00151621E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994924E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994924E+03   # ~c_R
   1000005     4.99935244E+03   # ~b_1
   2000005     5.00080720E+03   # ~b_2
   1000006     4.98395320E+03   # ~t_1
   2000006     5.02045006E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99959919E+03   # ~tau_1
   2000015     5.00055969E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.74783464E+03   # ~chi_10
   1000023    -1.80007135E+03   # ~chi_20
   1000025     1.83519161E+03   # ~chi_30
   1000035     3.00504510E+03   # ~chi_40
   1000024     1.79506142E+03   # ~chi_1+
   1000037     3.00503877E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.34875481E-01   # N_11
  1  2    -4.18884307E-02   # N_12
  1  3     4.80051158E-01   # N_13
  1  4    -4.77236076E-01   # N_14
  2  1     2.43317852E-03   # N_21
  2  2    -3.23204754E-03   # N_22
  2  3    -7.07030920E-01   # N_23
  2  4    -7.07171062E-01   # N_24
  3  1     6.78193695E-01   # N_31
  3  2     4.88263726E-02   # N_32
  3  3    -5.17487814E-01   # N_33
  3  4     5.19495582E-01   # N_34
  4  1     2.32792762E-03   # N_41
  4  2    -9.97923293E-01   # N_42
  4  3    -4.31801575E-02   # N_43
  4  4     4.77405087E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.09860429E-02   # U_11
  1  2     9.98138619E-01   # U_12
  2  1     9.98138619E-01   # U_21
  2  2     6.09860429E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.74336517E-02   # V_11
  1  2     9.97723761E-01   # V_12
  2  1     9.97723761E-01   # V_21
  2  2     6.74336517E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07656278E-01   # cos(theta_t)
  1  2     7.06556857E-01   # sin(theta_t)
  2  1    -7.06556857E-01   # -sin(theta_t)
  2  2     7.07656278E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.80471105E-01   # cos(theta_b)
  1  2     7.32774914E-01   # sin(theta_b)
  2  1    -7.32774914E-01   # -sin(theta_b)
  2  2     6.80471105E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04892277E-01   # cos(theta_tau)
  1  2     7.09314372E-01   # sin(theta_tau)
  2  1    -7.09314372E-01   # -sin(theta_tau)
  2  2     7.04892277E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200664E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.80000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52294212E+02   # vev(Q)              
         4     3.15248158E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52722358E-01   # gprime(Q) DRbar
     2     6.26791903E-01   # g(Q) DRbar
     3     1.08213412E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02492730E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71496403E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79743628E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.78800000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -5.82853792E+05   # M^2_Hd              
        22    -8.43407471E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36877574E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.17567096E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.00413230E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.78683177E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.16802100E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.78683177E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.16802100E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.54816205E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.08582894E-13    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.08582894E-13    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.08582894E-13    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.08582894E-13    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.01128543E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     6.88056558E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.29177431E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.83652868E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.05618726E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.93720701E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.07756458E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.69201726E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.91847895E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.02913775E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.17113946E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.54124827E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.61613378E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.52552560E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.19128501E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.69255301E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.34426360E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.24924786E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.20721102E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.82448880E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.45987688E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.02637511E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.96187932E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.46999118E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.43170418E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.45510884E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.94141726E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.44510968E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.75999842E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.25446790E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.57152816E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.16248312E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22283283E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.02477598E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.95604174E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.97037473E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.45829386E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.86019374E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.91990953E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.93058599E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.06597051E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.14029772E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.30699697E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.77136787E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.14099060E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60882999E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22289569E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.11375504E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.75234196E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.90709472E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.46479494E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.79336568E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.92655022E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.93101872E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00540831E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.51246475E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.94181213E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.56226841E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.93876488E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89925178E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22283283E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.02477598E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.95604174E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.97037473E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.45829386E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.86019374E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.91990953E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.93058599E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.06597051E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.14029772E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.30699697E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.77136787E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.14099060E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60882999E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22289569E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.11375504E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.75234196E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.90709472E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.46479494E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.79336568E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.92655022E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.93101872E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00540831E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.51246475E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.94181213E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.56226841E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.93876488E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89925178E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.87087602E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.24423642E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78925175E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.44333768E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.75862577E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.84194296E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.53417950E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.88236947E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.47171097E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.89762650E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.52820097E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.90780638E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.87087602E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.24423642E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78925175E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.44333768E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.75862577E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.84194296E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.53417950E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.88236947E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.47171097E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.89762650E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.52820097E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.90780638E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.38120449E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.66881113E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.32582597E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.32674519E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.64619620E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.98807169E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.30253062E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.51030847E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.37970440E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.53637479E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.96836196E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.39833763E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.68082659E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     6.64032870E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.37185229E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.87107435E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.08766722E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.09153879E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.64404273E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77252289E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.69656255E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52833083E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.87107435E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.08766722E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.09153879E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.64404273E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77252289E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.69656255E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52833083E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.87350809E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.08674601E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.09061430E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.63926247E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77017469E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.53689078E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.52367508E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.56278296E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.83916509E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.11235370E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.09862263E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.03745241E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.03742670E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03022806E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.59525981E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53718050E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.05836851E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46158381E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.41565669E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52713237E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.80973382E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.49407422E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.01698084E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87603011E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.06989051E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.07405932E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.44703141E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.00740372E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.00047143E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.95156939E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.12391190E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.45537670E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.11726966E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.45520104E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.34340667E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.32421502E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.32410942E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.29443043E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.63888860E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.63888860E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.63888860E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     5.59117991E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     5.59117991E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.81350773E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.81350773E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.86372677E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.86372677E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.85959409E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.85959409E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.02088541E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.02088541E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
     2.56970319E-26    3     1000021        -2         2   # BR(~chi_20 -> ~g      ub      u)
     3.36082551E-26    3     1000021        -1         1   # BR(~chi_20 -> ~g      db      d)
     2.56970319E-26    3     1000021        -4         4   # BR(~chi_20 -> ~g      cb      c)
     3.36082551E-26    3     1000021        -3         3   # BR(~chi_20 -> ~g      sb      s)
#
#         PDG            Width
DECAY   1000025     1.87510861E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.42173948E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.76289316E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     4.83382486E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.33852923E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.29378652E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.79270144E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.15449254E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.97462002E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.17660119E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.38171100E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.85536076E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.85531675E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.02871032E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.04467519E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.04467519E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.04467519E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.79247293E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.32111263E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.77212712E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.32052534E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.95622195E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.30164175E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.30133849E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.21555243E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.05880158E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.05880158E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.05880158E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26583011E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26583011E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25805446E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25805446E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.21942782E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.21942782E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.21928197E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.21928197E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.17855678E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.17855678E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     6.38910574E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.56182494E-08    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     6.38910574E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.56182494E-08    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     1.14762690E-08    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.59520662E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.87771779E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52394999E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.50198365E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46830851E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46830851E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.08065002E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.20800062E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.44611716E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.86397324E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.94466204E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     9.52300332E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.45444097E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.37118064E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08707086E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17917221E-01    2           5        -5   # BR(h -> b       bb     )
     6.37267441E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25569204E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78001537E-04    2           3        -3   # BR(h -> s       sb     )
     2.06221370E-02    2           4        -4   # BR(h -> c       cb     )
     6.69594174E-02    2          21        21   # BR(h -> g       g      )
     2.30233058E-03    2          22        22   # BR(h -> gam     gam    )
     1.53702619E-03    2          22        23   # BR(h -> Z       gam    )
     2.00623078E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56084749E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78096198E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46784681E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430024E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71221881E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548318E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668628E-05    2           4        -4   # BR(H -> c       cb     )
     9.96069219E-01    2           6        -6   # BR(H -> t       tb     )
     7.97654649E-04    2          21        21   # BR(H -> g       g      )
     2.71557231E-06    2          22        22   # BR(H -> gam     gam    )
     1.16028123E-06    2          23        22   # BR(H -> Z       gam    )
     3.34213081E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66650779E-04    2          23        23   # BR(H -> Z       Z      )
     9.02055992E-04    2          25        25   # BR(H -> h       h      )
     7.27573299E-24    2          36        36   # BR(H -> A       A      )
     2.92053939E-11    2          23        36   # BR(H -> Z       A      )
     6.56648426E-12    2          24       -37   # BR(H -> W+      H-     )
     6.56648426E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380944E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47076186E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898566E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269538E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677790E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179937E-06    2           4        -4   # BR(A -> c       cb     )
     9.96999448E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678919E-04    2          21        21   # BR(A -> g       g      )
     3.14200638E-06    2          22        22   # BR(A -> gam     gam    )
     1.35287709E-06    2          23        22   # BR(A -> Z       gam    )
     3.25756495E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472078E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35695604E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238521E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81148205E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49846322E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45696090E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732015E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402715E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33865747E-04    2          24        25   # BR(H+ -> W+      h      )
     2.81404093E-13    2          24        36   # BR(H+ -> W+      A      )

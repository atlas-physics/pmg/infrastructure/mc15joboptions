#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05410116E+01   # W+
        25     1.25000000E+02   # h
        35     2.00401917E+03   # H
        36     2.00000000E+03   # A
        37     2.00151560E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002536E+03   # ~d_R
   1000002     4.99989244E+03   # ~u_L
   2000002     4.99994927E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002536E+03   # ~s_R
   1000004     4.99989244E+03   # ~c_L
   2000004     4.99994927E+03   # ~c_R
   1000005     4.99957350E+03   # ~b_1
   2000005     5.00058618E+03   # ~b_2
   1000006     4.98954674E+03   # ~t_1
   2000006     5.01489059E+03   # ~t_2
   1000011     5.00008220E+03   # ~e_L
   2000011     5.00007609E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008220E+03   # ~mu_L
   2000013     5.00007609E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99974600E+03   # ~tau_1
   2000015     5.00041289E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.20171199E+03   # ~chi_10
   1000023    -1.25008717E+03   # ~chi_20
   1000025     1.28888771E+03   # ~chi_30
   1000035     3.00348747E+03   # ~chi_40
   1000024     1.24662773E+03   # ~chi_1+
   1000037     3.00348544E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.17341086E-01   # N_11
  1  2    -2.99896018E-02   # N_12
  1  3     4.94135365E-01   # N_13
  1  4    -4.90257719E-01   # N_14
  2  1     3.50044047E-03   # N_21
  2  2    -3.65193901E-03   # N_22
  2  3    -7.06974454E-01   # N_23
  2  4    -7.07220992E-01   # N_24
  3  1     6.96712475E-01   # N_31
  3  2     3.24929800E-02   # N_32
  3  3    -5.05168124E-01   # N_33
  3  4     5.08272663E-01   # N_34
  4  1     1.11380438E-03   # N_41
  4  2    -9.99015262E-01   # N_42
  4  3    -2.86797551E-02   # N_43
  4  4     3.38339728E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.05324869E-02   # U_11
  1  2     9.99178221E-01   # U_12
  2  1     9.99178221E-01   # U_21
  2  2     4.05324869E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.78229766E-02   # V_11
  1  2     9.98855827E-01   # V_12
  2  1     9.98855827E-01   # V_21
  2  2     4.78229766E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07898882E-01   # cos(theta_t)
  1  2     7.06313792E-01   # sin(theta_t)
  2  1    -7.06313792E-01   # -sin(theta_t)
  2  2     7.07898882E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.68499977E-01   # cos(theta_b)
  1  2     7.43712163E-01   # sin(theta_b)
  2  1    -7.43712163E-01   # -sin(theta_b)
  2  2     6.68499977E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.03859501E-01   # cos(theta_tau)
  1  2     7.10339217E-01   # sin(theta_tau)
  2  1    -7.10339217E-01   # -sin(theta_tau)
  2  2     7.03859501E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201841E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52155631E+02   # vev(Q)              
         4     3.34937477E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52784612E-01   # gprime(Q) DRbar
     2     6.27186443E-01   # g(Q) DRbar
     3     1.07402800E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02544435E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71500016E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79804190E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.24400000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.29956950E+06   # M^2_Hd              
        22    -6.66919274E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37053739E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.38236456E-02   # gluino decays
#          BR         NDA      ID1       ID2
     2.78157157E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.51399506E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.73251623E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.53382293E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.62607114E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.04653517E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.36992737E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.83684221E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.17212181E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     4.53382293E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.62607114E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.04653517E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.36992737E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.83684221E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.17212181E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.60930559E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.68051028E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.13597775E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.11924432E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.07426291E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.25888489E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.65268860E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.65268860E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.65268860E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.65268860E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.24458593E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.24458593E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.29163097E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.06339278E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.39233665E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.43258200E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.47637522E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.45714465E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.92826387E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.10340382E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.15743157E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.00843348E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.42438579E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.14576850E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.05558029E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.35921773E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.08377285E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     4.95584931E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.47520088E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.41405740E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.27098476E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.97099070E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.11265765E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.64786465E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.25576603E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.60017152E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.65361254E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.52885882E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.54177142E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.29533102E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.78195987E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.31106480E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.60637754E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.77031779E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.35844408E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.33900142E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.08925417E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.20319481E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.67484385E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.55615184E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.13508275E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.24644386E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.21312785E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.97347809E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.36797873E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.68045473E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.44491778E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.23459691E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.35851933E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.31700330E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.25300223E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.15937660E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.68016294E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.99126180E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.13592999E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.24727612E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.14354382E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.05383584E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.48455249E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.76121498E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.17890538E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.79700166E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.35844408E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.33900142E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.08925417E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.20319481E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.67484385E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.55615184E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.13508275E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.24644386E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.21312785E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.97347809E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.36797873E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.68045473E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.44491778E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.23459691E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.35851933E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.31700330E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.25300223E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.15937660E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.68016294E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.99126180E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.13592999E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.24727612E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.14354382E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.05383584E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.48455249E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.76121498E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.17890538E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.79700166E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.94776936E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.22039548E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.65224241E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.04177830E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70280807E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.91809460E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.41417660E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.17866666E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.19203034E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.22390262E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.80784151E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.75997505E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.94776936E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.22039548E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.65224241E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.04177830E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70280807E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.91809460E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.41417660E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.17866666E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.19203034E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.22390262E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.80784151E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.75997505E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.56760303E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.75210502E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.48537306E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.60512964E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53333058E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.20181489E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.07153037E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.40087129E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.56767131E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60252681E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.64108968E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.67598128E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56963932E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     9.98306826E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.14421320E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.94782317E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.10716463E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.84284928E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.47092484E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.70912246E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.66994536E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.40973669E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.94782317E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.10716463E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.84284928E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.47092484E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.70912246E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.66994536E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.40973669E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.95064876E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10610439E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.84108453E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.46377055E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.70652816E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.62376901E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.40456860E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16377950E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.46813143E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.29259364E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.27649401E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.09753252E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.09750236E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08906433E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.67469109E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53123187E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.12689359E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46309095E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.34074033E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53796696E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.62931278E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     6.73231680E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98396542E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.90396242E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.12072159E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.36626673E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.41415433E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.08757562E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.85698934E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.18913447E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.17196277E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.51748418E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16414515E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.51727312E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38346510E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.46553703E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.46541453E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.43096162E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.92078178E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.92078178E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.92078178E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.33728718E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.33728718E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     5.93319081E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     5.93319081E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.45762420E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.45762420E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.43694929E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.43694929E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.21061559E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.21061559E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.23018552E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.70743427E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.07934216E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     7.13074097E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     6.21468878E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.76123001E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.08237771E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.29086162E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.37793559E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.29639399E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.33686830E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26556612E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26554012E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.87623096E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.30526559E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.30526559E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.30526559E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.38622620E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.08974471E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.36338086E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.08909683E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.68393749E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     7.05618478E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.05583955E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.95841253E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.40913229E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.40913229E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.40913229E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.31458898E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.31458898E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30731389E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30731389E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.38195665E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.38195665E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.38182026E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.38182026E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.34370396E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.34370396E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.67471810E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.92093359E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51327848E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     9.13165129E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46391813E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46391813E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.15288448E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.67881663E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.37208373E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.82957283E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     5.79888651E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     5.61223765E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     6.60093228E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     9.53954256E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08693470E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17876389E-01    2           5        -5   # BR(h -> b       bb     )
     6.37290911E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25577511E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78019109E-04    2           3        -3   # BR(h -> s       sb     )
     2.06227915E-02    2           4        -4   # BR(h -> c       cb     )
     6.69626492E-02    2          21        21   # BR(h -> g       g      )
     2.30332585E-03    2          22        22   # BR(h -> gam     gam    )
     1.53702846E-03    2          22        23   # BR(h -> Z       gam    )
     2.00655801E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56093279E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78101853E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46794307E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428371E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71216038E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547414E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668328E-05    2           4        -4   # BR(H -> c       cb     )
     9.96066798E-01    2           6        -6   # BR(H -> t       tb     )
     7.97671601E-04    2          21        21   # BR(H -> g       g      )
     2.70980168E-06    2          22        22   # BR(H -> gam     gam    )
     1.16000787E-06    2          23        22   # BR(H -> Z       gam    )
     3.34576104E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66831768E-04    2          23        23   # BR(H -> Z       Z      )
     9.03827824E-04    2          25        25   # BR(H -> h       h      )
     7.45792262E-24    2          36        36   # BR(H -> A       A      )
     2.99302772E-11    2          23        36   # BR(H -> Z       A      )
     6.83915973E-12    2          24       -37   # BR(H -> W+      H-     )
     6.83915973E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381135E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47086734E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898444E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269107E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677733E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179439E-06    2           4        -4   # BR(A -> c       cb     )
     9.96998950E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678447E-04    2          21        21   # BR(A -> g       g      )
     3.18687724E-06    2          22        22   # BR(A -> gam     gam    )
     1.35257637E-06    2          23        22   # BR(A -> Z       gam    )
     3.26105303E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472206E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35736458E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238359E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147633E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49872469E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695759E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731949E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402357E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34223016E-04    2          24        25   # BR(H+ -> W+      h      )
     2.80838456E-13    2          24        36   # BR(H+ -> W+      A      )

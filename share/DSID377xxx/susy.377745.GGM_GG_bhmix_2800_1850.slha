#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419608E+01   # W+
        25     1.25000000E+02   # h
        35     2.00399221E+03   # H
        36     2.00000000E+03   # A
        37     2.00152120E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994923E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994923E+03   # ~c_R
   1000005     4.99933313E+03   # ~b_1
   2000005     5.00082650E+03   # ~b_2
   1000006     4.98344657E+03   # ~t_1
   2000006     5.02095156E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.80000000E+03   # ~g
   1000022     1.79827746E+03   # ~chi_10
   1000023    -1.85007025E+03   # ~chi_20
   1000025     1.88553235E+03   # ~chi_30
   1000035     3.00526044E+03   # ~chi_40
   1000024     1.84484591E+03   # ~chi_1+
   1000037     3.00525325E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.30160742E-01   # N_11
  1  2    -4.39669819E-02   # N_12
  1  3     4.83512542E-01   # N_13
  1  4    -4.80778345E-01   # N_14
  2  1     2.36658539E-03   # N_21
  2  2    -3.19866406E-03   # N_22
  2  3    -7.07034052E-01   # N_23
  2  4    -7.07168308E-01   # N_24
  3  1     6.83266621E-01   # N_31
  3  2     5.06938478E-02   # N_32
  3  3    -5.14080796E-01   # N_33
  3  4     5.16040496E-01   # N_34
  4  1     2.53260160E-03   # N_41
  4  2    -9.97740852E-01   # N_42
  4  3    -4.51597790E-02   # N_43
  4  4     4.96726629E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.37749371E-02   # U_11
  1  2     9.97964307E-01   # U_12
  2  1     9.97964307E-01   # U_21
  2  2     6.37749371E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.01549220E-02   # V_11
  1  2     9.97536108E-01   # V_12
  2  1     9.97536108E-01   # V_21
  2  2     7.01549220E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07641478E-01   # cos(theta_t)
  1  2     7.06571680E-01   # sin(theta_t)
  2  1    -7.06571680E-01   # -sin(theta_t)
  2  2     7.07641478E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81173070E-01   # cos(theta_b)
  1  2     7.32122427E-01   # sin(theta_b)
  2  1    -7.32122427E-01   # -sin(theta_b)
  2  2     6.81173070E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954677E-01   # cos(theta_tau)
  1  2     7.09252355E-01   # sin(theta_tau)
  2  1    -7.09252355E-01   # -sin(theta_tau)
  2  2     7.04954677E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90196206E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52304655E+02   # vev(Q)              
         4     3.05071722E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52718299E-01   # gprime(Q) DRbar
     2     6.26766168E-01   # g(Q) DRbar
     3     1.07511281E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02472914E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71196724E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79739160E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.83113532E+05   # M^2_Hd              
        22    -8.43116985E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36866085E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.21521315E-03   # gluino decays
#          BR         NDA      ID1       ID2
     8.63156136E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.63607326E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.02559810E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.69842566E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.13656600E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.61003284E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.63293067E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.63306534E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.07316338E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.69842566E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.13656600E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.61003284E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.63293067E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.63306534E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.07316338E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.76464686E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.62334349E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.69802388E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     9.22727803E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.46388647E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     9.73873576E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     3.90913946E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.90913946E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.90913946E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.90913946E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.93476935E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.93476935E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.29633448E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.87202317E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.18648117E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.83085570E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.37366278E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.27263604E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.69881738E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.76334689E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.17364426E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.00275920E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.19506699E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.77245757E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.18007343E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.10670109E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.30138240E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.67256466E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.62821329E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.97571485E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.00903640E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.63122926E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.08367295E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.42113369E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.23129767E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     7.86029077E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.71947012E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.37363221E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.22483195E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.39201271E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.49741259E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.73849345E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.07309950E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.43557406E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.51629121E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     8.12637117E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     7.00762919E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.91854000E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.05890605E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.10606286E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.01233525E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.43534931E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.35820810E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.14849613E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.25091611E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.67695251E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.05079961E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.41744983E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.51637746E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.07591556E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.22531579E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.47616200E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.06905406E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.52530394E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.01330518E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.43601654E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.29892185E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.23070620E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.49845464E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.99801793E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.36127412E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.84771137E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.51629121E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     8.12637117E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     7.00762919E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.91854000E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.05890605E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.10606286E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.01233525E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.43534931E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.35820810E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.14849613E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.25091611E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.67695251E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.05079961E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.41744983E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.51637746E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.07591556E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.22531579E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.47616200E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.06905406E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.52530394E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.01330518E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.43601654E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.29892185E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.23070620E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.49845464E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.99801793E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.36127412E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.84771137E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.86273794E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.96595183E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77190512E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.51192691E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76414049E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.14314221E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.54662249E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.85052774E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.40534192E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.58029896E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.59456728E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.50017029E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.86273794E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.96595183E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77190512E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.51192691E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76414049E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.14314221E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.54662249E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.85052774E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.40534192E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.58029896E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.59456728E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.50017029E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36119278E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.60922886E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.30077930E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.34503379E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.65876049E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25843123E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.32856868E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.52308839E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35961397E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47806656E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.90464829E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.41603244E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.69370938E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.76097905E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.39852599E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.86295099E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07030615E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04338673E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.59542888E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77933673E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.01279200E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54058198E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.86295099E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07030615E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04338673E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.59542888E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77933673E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.01279200E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54058198E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86534393E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06941230E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.04251536E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.59075596E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77701562E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.84086161E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.53598362E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.49288575E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.21510094E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.06658771E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.05266288E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.02219705E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.02217097E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.01487130E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55083485E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53819418E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07438247E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46136925E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.40076803E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52520696E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.90885587E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.64962853E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95462852E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93611551E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09255963E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02954136E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.32182748E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.99678965E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.34561925E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.80906394E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11355230E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44196894E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10688358E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44179215E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32932217E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.29362390E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.29351806E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.26376635E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.57781524E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.57781524E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.57781524E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.04292427E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.04292427E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.95342337E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.95342337E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.34764159E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.34764159E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.34285338E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.34285338E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.35026026E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.35026026E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.00295009E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.12811425E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.04208514E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.27120688E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.61300748E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.36934828E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.42690267E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.70181583E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.42887642E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.72055872E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.27896498E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.97438558E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.97435170E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.42704991E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.30206184E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.30206184E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.30206184E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.74196460E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.25571934E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.72252177E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.25515899E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.90726390E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.15232924E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.15203905E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.06997223E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02898547E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02898547E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02898547E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26072696E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26072696E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25319462E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25319462E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.20241726E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.20241726E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.20227599E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.20227599E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.16282150E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.16282150E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55076556E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.64042438E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52455787E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.10977099E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46911519E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46911519E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09607402E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.58539824E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42972305E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88598929E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.02172445E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.02774432E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.60507836E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.84498232E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08898863E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18098578E-01    2           5        -5   # BR(h -> b       bb     )
     6.36960080E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25460409E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77771110E-04    2           3        -3   # BR(h -> s       sb     )
     2.06125882E-02    2           4        -4   # BR(h -> c       cb     )
     6.69283618E-02    2          21        21   # BR(h -> g       g      )
     2.30119103E-03    2          22        22   # BR(h -> gam     gam    )
     1.53630800E-03    2          22        23   # BR(h -> Z       gam    )
     2.00527269E-01    2          24       -24   # BR(h -> W+      W-     )
     2.55964648E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78087905E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46471194E-03    2           5        -5   # BR(H -> b       bb     )
     2.46436007E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71243034E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11551095E-06    2           3        -3   # BR(H -> s       sb     )
     1.00669182E-05    2           4        -4   # BR(H -> c       cb     )
     9.96074496E-01    2           6        -6   # BR(H -> t       tb     )
     7.97658566E-04    2          21        21   # BR(H -> g       g      )
     2.71585248E-06    2          22        22   # BR(H -> gam     gam    )
     1.16032450E-06    2          23        22   # BR(H -> Z       gam    )
     3.32862399E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65977279E-04    2          23        23   # BR(H -> Z       Z      )
     9.01928068E-04    2          25        25   # BR(H -> h       h      )
     7.19897457E-24    2          36        36   # BR(H -> A       A      )
     2.89416333E-11    2          23        36   # BR(H -> Z       A      )
     6.40614902E-12    2          24       -37   # BR(H -> W+      H-     )
     6.40614902E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82379236E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46762287E-03    2           5        -5   # BR(A -> b       bb     )
     2.43899656E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62273390E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13678298E-06    2           3        -3   # BR(A -> s       sb     )
     9.96184387E-06    2           4        -4   # BR(A -> c       cb     )
     9.97003903E-01    2           6        -6   # BR(A -> t       tb     )
     9.43683135E-04    2          21        21   # BR(A -> g       g      )
     3.14001770E-06    2          22        22   # BR(A -> gam     gam    )
     1.35290295E-06    2          23        22   # BR(A -> Z       gam    )
     3.24437881E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74470794E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35011182E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239996E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81153422E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49408287E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45699137E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732622E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99404069E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32516474E-04    2          24        25   # BR(H+ -> W+      h      )
     2.86066392E-13    2          24        36   # BR(H+ -> W+      A      )

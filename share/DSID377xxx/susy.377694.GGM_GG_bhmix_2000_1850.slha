#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.85000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05419658E+01   # W+
        25     1.25000000E+02   # h
        35     2.00399756E+03   # H
        36     2.00000000E+03   # A
        37     2.00151777E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013292E+03   # ~d_L
   2000001     5.00002538E+03   # ~d_R
   1000002     4.99989246E+03   # ~u_L
   2000002     4.99994923E+03   # ~u_R
   1000003     5.00013292E+03   # ~s_L
   2000003     5.00002538E+03   # ~s_R
   1000004     4.99989246E+03   # ~c_L
   2000004     4.99994923E+03   # ~c_R
   1000005     4.99933250E+03   # ~b_1
   2000005     5.00082713E+03   # ~b_2
   1000006     4.98344402E+03   # ~t_1
   2000006     5.02095575E+03   # ~t_2
   1000011     5.00008215E+03   # ~e_L
   2000011     5.00007615E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008215E+03   # ~mu_L
   2000013     5.00007615E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99958584E+03   # ~tau_1
   2000015     5.00057304E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.79827729E+03   # ~chi_10
   1000023    -1.85007025E+03   # ~chi_20
   1000025     1.88553251E+03   # ~chi_30
   1000035     3.00526045E+03   # ~chi_40
   1000024     1.84484590E+03   # ~chi_1+
   1000037     3.00525325E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.30160627E-01   # N_11
  1  2    -4.39670136E-02   # N_12
  1  3     4.83512633E-01   # N_13
  1  4    -4.80778426E-01   # N_14
  2  1     2.36659457E-03   # N_21
  2  2    -3.19866626E-03   # N_22
  2  3    -7.07034052E-01   # N_23
  2  4    -7.07168309E-01   # N_24
  3  1     6.83266745E-01   # N_31
  3  2     5.06938820E-02   # N_32
  3  3    -5.14080708E-01   # N_33
  3  4     5.16040417E-01   # N_34
  4  1     2.53261315E-03   # N_41
  4  2    -9.97740848E-01   # N_42
  4  3    -4.51598101E-02   # N_43
  4  4     4.96726971E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.37749803E-02   # U_11
  1  2     9.97964304E-01   # U_12
  2  1     9.97964304E-01   # U_21
  2  2     6.37749803E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.01549695E-02   # V_11
  1  2     9.97536105E-01   # V_12
  2  1     9.97536105E-01   # V_21
  2  2     7.01549695E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07641379E-01   # cos(theta_t)
  1  2     7.06571779E-01   # sin(theta_t)
  2  1    -7.06571779E-01   # -sin(theta_t)
  2  2     7.07641379E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.81195248E-01   # cos(theta_b)
  1  2     7.32101792E-01   # sin(theta_b)
  2  1    -7.32101792E-01   # -sin(theta_b)
  2  2     6.81195248E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04954936E-01   # cos(theta_tau)
  1  2     7.09252098E-01   # sin(theta_tau)
  2  1    -7.09252098E-01   # -sin(theta_tau)
  2  2     7.04954936E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200216E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.85000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52305941E+02   # vev(Q)              
         4     3.11217617E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52717870E-01   # gprime(Q) DRbar
     2     6.26763404E-01   # g(Q) DRbar
     3     1.08044698E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02490845E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71424477E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79738237E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.83900000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -7.86441041E+05   # M^2_Hd              
        22    -8.57546667E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36864856E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.71229137E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.16824896E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.91758089E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     7.18185424E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.37053920E-05    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     8.70159444E-10    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     3.56785443E-06    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.39862452E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.65084038E-10    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.46697678E-05    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     8.37053920E-05    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     8.70159444E-10    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     3.56785443E-06    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.39862452E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.65084038E-10    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.46697678E-05    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     8.27556770E-05    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     6.89486321E-07    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     3.76348852E-06    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.96401883E-06    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.96401883E-06    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.96401883E-06    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.96401883E-06    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.87566133E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.13954143E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.56351165E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.89461381E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.09715834E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.02571636E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.15535348E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.58926576E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.77545974E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     3.17456989E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.46489835E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     7.72708016E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.71214996E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     8.76626288E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.37786588E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.57771729E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.21628819E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.39966928E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.49986873E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.92416039E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.54564224E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.06766030E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.13917865E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.39986933E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.30144155E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.53708409E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.25732020E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.55926360E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.85647099E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.29994958E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.77131599E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.08538252E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09872230E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.89307205E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.08128248E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.11613616E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.66033753E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.60294525E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.32468400E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86783539E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.94056160E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.21186445E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.28362942E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.88031449E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.43749293E-07    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59077838E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09879182E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.23063341E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.88509277E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.97069555E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.66776669E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.45684023E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.33187498E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86829308E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88105305E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.70473510E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.88983144E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.84962230E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.70760601E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89445547E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09872230E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.89307205E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.08128248E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.11613616E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.66033753E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.60294525E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.32468400E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86783539E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.94056160E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.21186445E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.28362942E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.88031449E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.43749293E-07    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59077838E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09879182E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.23063341E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.88509277E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.97069555E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.66776669E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.45684023E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.33187498E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86829308E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88105305E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.70473510E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.88983144E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.84962230E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.70760601E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89445547E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.86271565E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.96599118E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77188959E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.51197477E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76413753E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.14314356E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.54661672E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.85052324E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.40534051E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.58034225E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.59456868E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.50020219E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.86271565E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.96599118E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77188959E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.51197477E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76413753E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.14314356E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.54661672E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.85052324E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.40534051E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.58034225E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.59456868E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.50020219E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.36117974E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.60923502E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.30075651E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.34504064E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.65875618E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.25841790E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.32856013E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.52309680E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35960015E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47807608E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.90462516E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.41604180E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.69370308E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.76096941E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.39851345E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.86292869E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.07031112E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.04339100E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.59546514E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77933388E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.01279362E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54057622E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.86292869E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.07031112E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.04339100E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.59546514E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77933388E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.01279362E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54057622E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.86532161E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06941727E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.04251963E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.59079220E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.77701277E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.84086117E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.53597787E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.49288752E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     8.21509094E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.06658799E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.05266326E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.02219714E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.02217106E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.01487145E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.55080964E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53819719E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07438206E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46136805E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.40076726E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.52520633E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.90891197E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.64966051E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.95462077E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93612768E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09251554E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.02954650E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.32173719E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.99680065E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.34561333E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.80905843E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11355138E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44196851E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.10688270E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44179172E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.32932248E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.29362652E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.29352067E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.26376909E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.57782279E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.57782279E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.57782279E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.04284780E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.04284780E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.95337624E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.95337624E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.34761610E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.34761610E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.34282796E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.34282796E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.35024848E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.35024848E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.00295855E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.12791510E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.04223866E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.27118222E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     4.61299578E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.36928776E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.42698410E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.70191837E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.42895631E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.72066155E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.28522768E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.97459688E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.97456299E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     8.42734692E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.30255400E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.30255400E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.30255400E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.74196848E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.25572555E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.72252579E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.25516520E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.90727222E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.15234907E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.15205888E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.06999238E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.02898979E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.02898979E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.02898979E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26072676E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26072676E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25319448E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25319448E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.20241660E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.20241660E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.20227533E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.20227533E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.16282117E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.16282117E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.55074040E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.64042814E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52456090E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.10977383E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46911396E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46911396E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.09607401E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.58516193E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42972270E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88601179E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.02175810E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.02771975E-09    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.60506774E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.84501501E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08763193E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17971734E-01    2           5        -5   # BR(h -> b       bb     )
     6.37179117E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25537940E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77935298E-04    2           3        -3   # BR(h -> s       sb     )
     2.06193188E-02    2           4        -4   # BR(h -> c       cb     )
     6.69501513E-02    2          21        21   # BR(h -> g       g      )
     2.30195808E-03    2          22        22   # BR(h -> gam     gam    )
     1.53681868E-03    2          22        23   # BR(h -> Z       gam    )
     2.00593675E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56049599E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78094876E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46695279E-03    2           5        -5   # BR(H -> b       bb     )
     2.46430798E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71224617E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11548684E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668762E-05    2           4        -4   # BR(H -> c       cb     )
     9.96070495E-01    2           6        -6   # BR(H -> t       tb     )
     7.97653528E-04    2          21        21   # BR(H -> g       g      )
     2.71586205E-06    2          22        22   # BR(H -> gam     gam    )
     1.16030369E-06    2          23        22   # BR(H -> Z       gam    )
     3.34077052E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66582952E-04    2          23        23   # BR(H -> Z       Z      )
     9.01877604E-04    2          25        25   # BR(H -> h       h      )
     7.25135165E-24    2          36        36   # BR(H -> A       A      )
     2.91355132E-11    2          23        36   # BR(H -> Z       A      )
     6.52063241E-12    2          24       -37   # BR(H -> W+      H-     )
     6.52063241E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82380552E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46987192E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898816E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62270422E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677907E-06    2           3        -3   # BR(A -> s       sb     )
     9.96180959E-06    2           4        -4   # BR(A -> c       cb     )
     9.97000472E-01    2           6        -6   # BR(A -> t       tb     )
     9.43679887E-04    2          21        21   # BR(A -> g       g      )
     3.14000693E-06    2          22        22   # BR(A -> gam     gam    )
     1.35289998E-06    2          23        22   # BR(A -> Z       gam    )
     3.25624051E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74471827E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35499939E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238882E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81149482E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49721095E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45696824E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08732161E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402852E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33730672E-04    2          24        25   # BR(H+ -> W+      h      )
     2.82855185E-13    2          24        36   # BR(H+ -> W+      A      )

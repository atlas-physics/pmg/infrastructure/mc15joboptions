##############################################################
# JO intended for electron decay channel studies.
# Python snippet to generate EvtGen user decay file on the fly
# B0 -> K*0 (K+pi- 0.5GeV) e5.5e5.5 (flat angles)
##############################################################
f = open("B0_KSTAR_EE_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias my_K*0   K*0\n")
f.write("Decay B0\n")
f.write("1.0000   my_K*0  e+ e-   PHSP;\n")
f.write("Enddecay\n")
f.write("Decay my_K*0\n")
f.write("1.0000   K+ pi-   VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive B0->K*0_Kpi_e5p5e5p5 production"
evgenConfig.keywords    = ["exclusive", "B0", "2electron"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut      = 0.0
genSeq.Pythia8B.AntiQuarkPtCut  = 11.0
genSeq.Pythia8B.QuarkEtaCut     = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 2

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [511]
genSeq.EvtInclusiveDecay.userDecayFile = "B0_KSTAR_EE_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn  = False
filtSeq.BSignalFilter.LVL2MuonCutOn  = False
if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 2
filtSeq.MultiElectronFilter.Ptcut      = 5500.0
filtSeq.MultiElectronFilter.Etacut     = 2.6

filtSeq.BSignalFilter.B_PDGCode = 511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6

#--------------------------------------------------------------
# JO  fragment for pp->chi_c1->J/psi(mu2p5mu2p5)mumu(mu2p5mu2p5), Pythia 8B
#--------------------------------------------------------------

evgenConfig.description = "pp->chi_c1->J/psi(mu2p5mu2p5)mumu(mu2p5mu2p5) production with Photos"
evgenConfig.keywords = ["charmonium","Jpsi","4muon"]
evgenConfig.minevents = 5000

include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

# Turn off inclusive charmonium production (turned on in Pythia8B_Charmonium_Common.py)
genSeq.Pythia8B.Commands += ['Charmonium:all = off']

# Turn on only direct chi_c1 production
genSeq.Pythia8B.Commands += ['Charmonium:gg2ccbar(3PJ)[3PJ(1)]g = off,on,off']
genSeq.Pythia8B.Commands += ['Charmonium:qg2ccbar(3PJ)[3PJ(1)]q = off,on,off']
genSeq.Pythia8B.Commands += ['Charmonium:qqbar2ccbar(3PJ)[3PJ(1)]g = off,on,off']
genSeq.Pythia8B.Commands += ['Charmonium:gg2ccbar(3PJ)[3S1(8)]g = off,on,off']
genSeq.Pythia8B.Commands += ['Charmonium:qg2ccbar(3PJ)[3S1(8)]q = off,on,off']
genSeq.Pythia8B.Commands += ['Charmonium:qqbar2ccbar(3PJ)[3S1(8)]g = off,on,off']

# Force decay of chi_c1 -> J/psi mu+ mu-
genSeq.Pythia8B.Commands += ['20443:onMode = off']
genSeq.Pythia8B.Commands += ['20443:addChannel = 1 1.0 0 443 13 -13']
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

# This is low enough not to distort chi_c pT distribution
# above ~25 GeV, where acceptance for 4mu2p5 begins
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 15.']

# 4mu2p5 filter
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [2.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [4]

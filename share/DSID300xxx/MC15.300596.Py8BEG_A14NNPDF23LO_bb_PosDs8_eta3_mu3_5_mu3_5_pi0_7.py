#################################################################################
# Production channel of bb is included in this fragment.
# Ds meson force decaying into phi+pi and phi also must decay into dimuon.
# Ds pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>3.5GeV, mu2>3.5GeV, pi>0.7GeV, and D_s>8GeV
#################################################################################
f = open("PosDsD_PhiPi_2MuPi_USER.DEC","w")
f.write("Alias myPhi phi\n")
f.write("Decay D_s+\n")
f.write("1.0000   myPhi   pi+           SVS;\n")
f.write("Enddecay\n")
f.write("Decay myPhi\n")
f.write("1.0000    mu+  mu-          PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description  = "bb->PosDs->phipi->2mupi inclusive production"
evgenConfig.keywords     = [ "charmonium", "muon", "SM" ]
evgenConfig.minevents    = 100
evgenConfig.contact      = [ 'ychow@cern.ch' ]
evgenConfig.process      = "bb>PosDs>phipi>2mupi"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 7.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.SelectBQuarks      = True
genSeq.Pythia8B.SelectCQuarks      = False
genSeq.Pythia8B.QuarkPtCut         = 7.0
genSeq.Pythia8B.AntiQuarkPtCut     = 7.0
genSeq.Pythia8B.QuarkEtaCut        = 5.0
genSeq.Pythia8B.AntiQuarkEtaCut    = 5.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops   = 8
genSeq.Pythia8B.NDecayLoops           = 1

genSeq.EvtInclusiveDecay.userDecayFile = "PosDsD_PhiPi_2MuPi_USER.DEC"

#Add the Filter
from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
filtSeq += TripletChainFilter()
TripletChainFilter = filtSeq.TripletChainFilter

TripletChainFilter.NTriplet       = 1
TripletChainFilter.PdgId1         = 13
TripletChainFilter.PdgId2         = -13
TripletChainFilter.PdgId3         = 211
TripletChainFilter.NStep1         = 2
TripletChainFilter.NStep2         = 2
TripletChainFilter.NStep3         = 1

TripletChainFilter.TripletPdgId   = 431
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000

TripletChainFilter.DoubletPdgId   = 333   # phi
TripletChainFilter.DoubletPtMin   = 0
TripletChainFilter.DoubletEtaMax  = 100
TripletChainFilter.DoubletMassMin = 0
TripletChainFilter.DoubletMassMax = 10000000

TripletChainFilter.TripletPtMin   = 8000.
TripletChainFilter.TripletEtaMax  = 3.0
TripletChainFilter.PtMin1         = 3500.
TripletChainFilter.PtMin2         = 3500.
TripletChainFilter.PtMin3         = 700.
TripletChainFilter.EtaMax1        = 3.0
TripletChainFilter.EtaMax2        = 3.0
TripletChainFilter.EtaMax3        = 3.0

###########################################################
f = open("JpsiMuMu.dec","w")
f.write("Decay J/psi\n")
f.write("1.00 mu+ mu- VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
###########################################################

evgenConfig.description = "ParticleGun J/psi->mu3p5mu3p5 uniform pt"
evgenConfig.keywords = ["Jpsi","2muon"]
evgenConfig.minevents = 10000

include("MC15JobOptions/ParticleGun_Common.py")

import ROOT,math

import ParticleGun as PG

class PtRapidityMPhiSampler(PG.PtEtaMPhiSampler):

    def __init__(self, pt, rap, mass=0.0, phi=[0, 2*math.pi]):
        self.pt = pt
        self.rap = rap
        self.mass = mass
        self.phi = phi

    @property
    def rap(self):
        "Rapidity sampler"
        return self._rap
    @rap.setter
    def rap(self, rap):
        self._rap = PG.mksampler(rap)

    def shoot(self):
        """
        rap = arctanh(pz/E) =>
        pz = E * tanh(rap),
        E^2 = pT^2 + pz^2 + m^2 =>
        E = sqrt( (pT^2 + m^2) / (1 - tanh(rap)^2) )
        """
        rap = self.rap()
        pt = self.pt()
        phi = self.phi()
        m = self.mass()
        e = math.sqrt( (pt**2 + m**2) / (1 - (math.tanh(rap))**2) )
        px = pt * math.cos(phi)
        py = pt * math.sin(phi)
        pz = e * math.tanh(rap)
        v4 = ROOT.TLorentzVector(px, py, pz, e)
        return v4
      
class PosCylindricalSampler(PG.PosSampler):
  
    def __init__(self, r, z, phi=[0, 2*math.pi], t=0):
        self.r = r
        self.phi = phi
        self.z = z
        self.t = t
        
    @property
    def r(self):
        "r sampler"
        return self._r
    @r.setter
    def r(self, r):
        self._r = PG.mksampler(r)
        
    @property
    def phi(self):
        "phi sampler"
        return self._phi
    @phi.setter
    def phi(self, phi):
        self._phi = PG.mksampler(phi)
        
    def shoot(self):
        r = self.r()
        phi = self.phi()
        x = r * math.cos(phi)
        y = r * math.sin(phi)
        z = self.z()
        t = self.t()
        #print "POS =", x, y, z, t
        return ROOT.TLorentzVector(x, y, z, t)


#genSeq.ParticleGun.sampler.mom = PtRapidityMPhiSampler(pt=[7.5e3,400e3], rap=[-2.7,2.7], mass=3096.900)
#genSeq.ParticleGun.sampler.pid = 443
#genSeq.ParticleGun.sampler.pos = PosCylindricalSampler(r=[0.,150.],z=0.)

class MyParticleSampler(PG.Sampler):
    """
    A simple N-independent-particle sampler.
    Requires that the momentum-phi and position-phi are aligned, so as
    not to obtain large and unphysical -ve Lxy.
    """

    def __init__(self,
                 r, z,
                 mom=PG.NullMomSampler(),
                 n=1,
                 pid=999,):                 
        self.pid = pid
        self.mom = mom
        self.n = n
        self.r = r
        self.z = z
        self.massdict = PG.MASSES
        self.mass_override = True

    @property
    def pid(self):
        "Particle ID code sampler"
        return self._pid
    @pid.setter
    def pid(self, x):
        self._pid = PG.mksampler(x)

    @property
    def n(self):
        "Particle number sampler"
        return self._n
    @n.setter
    def n(self, x):
        self._n = PG.mksampler(x)

    @property
    def r(self):
        "r sampler"
        return self._r
    @r.setter
    def r(self, r):
        self._r = PG.mksampler(r)

    @property
    def z(self):
        "z sampler"
        return self._z
    @z.setter
    def z(self, z):
        self._z = PG.mksampler(z)


    def shoot(self):
        "Return a vector of sampled particles"
        numparticles = self.n()
        rtn = []
        for i in xrange(numparticles):
            ## Sample the particle ID and create a particle
            pid = self.pid()
            p = PG.SampledParticle(pid)
            ## Pass mass info to the v4 sampler and set same generated mass
            if self.mass_override and self.massdict.has_key(abs(pid)):
                m = self.massdict[abs(pid)]
                self.mom.mass = m
                p.mass = m
            # TODO: Should the particle generated_mass be set from the sampler by default?
            ## Sample momentum and vertex positions into the particle

            mom4 = self.mom()
            r   = self.r()
            phi = mom4.Phi()
            x   = r * math.cos(phi)
            y   = r * math.sin(phi)
            z   = self.z()
            t   = 0.
            
            pos4 = ROOT.TLorentzVector(x, y, z, t)

            p.mom = mom4
            p.pos = pos4
            ## Add particle to output list
            rtn.append(p)
        return rtn


genSeq.ParticleGun.sampler = MyParticleSampler(pid=443,
                                                mom = PtRapidityMPhiSampler(pt=[7.5e3,400e3], rap=[-2.7,2.7], mass=3096.900),
                                                r=[0.,150.],z=0.,
                                                n   = 1,
                                                )

include("MC15JobOptions/EvtGen_Fragment.py")
genSeq.EvtInclusiveDecay.decayFile = "JpsiMuMu.dec"
evgenConfig.auxfiles += ['inclusiveP8.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8.pdt"
genSeq.EvtInclusiveDecay.OutputLevel = 3

include("MC15JobOptions/MultiMuonFilter.py")
filtSeq.MultiMuonFilter.Ptcut = 3500.
filtSeq.MultiMuonFilter.Etacut = 2.7
filtSeq.MultiMuonFilter.NMuons = 2 





############################################################################
# Job options for Pythia8B_i generation of Bs_Jpsimu10p5mu10p5_phi_K0p8K0p8.
# Intended for ECFA2016 studies.
############################################################################

evgenConfig.description = "Exclusive Bs->Jpsimu10p5mu10p5+phiK0p8K0p8 production"
evgenConfig.keywords = ["exclusive", "Bs", "Jpsi", "2muon"]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

# 7. is used for 3p5 => 7+2x7 to be used for 10p5
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 21.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 21.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents         = True

genSeq.Pythia8B.Commands      += ['531:87:onMode = on']
genSeq.Pythia8B.Commands      += ['443:onMode = off']
genSeq.Pythia8B.Commands      += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [  531,   443,   -13,    13,   333, 321, -321]
genSeq.Pythia8B.SignalPtCuts   = [  0.0,   0.0,   0.0,   0.0,   0.0, 0.8,  0.8]
genSeq.Pythia8B.SignalEtaCuts  = [102.5, 102.5, 102.5, 102.5, 102.5, 2.5,  2.5]

genSeq.Pythia8B.NHadronizationLoops = 7

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [10.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

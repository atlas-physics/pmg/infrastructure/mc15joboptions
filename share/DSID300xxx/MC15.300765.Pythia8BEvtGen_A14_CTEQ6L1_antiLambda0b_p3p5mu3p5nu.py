###############################################################
# Python snippet to generate EvtGen user decay file on the fly
# Lambda_0b->p3p5mu3p5nu
###############################################################
f = open("LAMBDAB0_PMUNU.DEC","w")
f.write("Decay Lambda_b0\n")
f.write("1.0000  anti-p-    mu+  nu_mu       PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
###############################################################

evgenConfig.description = "Exclusive Lambda_b0-> production"
evgenConfig.keywords = ["exclusive","Lambda_b0","1muon"]
evgenConfig.minevents = 100

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False

# Fix Lambda_b mass in Pythia to match that in EvtGen
genSeq.Pythia8B.Commands += ['5122:m0 = 5.6202']

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.'] #was 7.

genSeq.Pythia8B.QuarkPtCut = 7.0 #was 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0 # was 7.0
genSeq.Pythia8B.QuarkEtaCut = 2.6 # was 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5 # was 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.NHadronizationLoops = 5

genSeq.Pythia8B.SignalPDGCodes = [5122]
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.EvtInclusiveDecay.userDecayFile = "LAMBDAB0_PMUNU.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 3500 
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutOn = False

filtSeq.BSignalFilter.B_PDGCode = 5122;
filtSeq.BSignalFilter.Cuts_Final_mu_switch = False
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 3500
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

filtSeq.BSignalFilter.InvMass_switch = True;
filtSeq.BSignalFilter.InvMass_PartId1 = -13
filtSeq.BSignalFilter.InvMass_PartId2 = -2212
filtSeq.BSignalFilter.InvMassMin = 3000.0
filtSeq.BSignalFilter.InvMassMax = 7000.0

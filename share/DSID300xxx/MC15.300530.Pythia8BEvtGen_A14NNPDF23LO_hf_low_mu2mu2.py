#################################################################################
# job options fragment for hf->mu2mu2(inclusive)
# For HF tau3mu background validation 
#################################################################################
# All production channels of bb and cc are included in this fragment.
# thresholds: mu1>2GeV, mu2>2GeV
#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description  = "hf->lowmassdimu inclusive production"
evgenConfig.keywords     = [ "muon" ]
evgenConfig.minevents    = 200
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "hf>dimu"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.SelectBQuarks      = True
genSeq.Pythia8B.SelectCQuarks      = True
genSeq.Pythia8B.QuarkPtCut         = 10.0
genSeq.Pythia8B.AntiQuarkPtCut     = 10.0
genSeq.Pythia8B.QuarkEtaCut        = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut    = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops   = 10
genSeq.Pythia8B.NDecayLoops           = 1

#Add the Filters:
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
genSeq += MultiMuonFilter()
genSeq.MultiMuonFilter.Ptcut = 2000.
genSeq.MultiMuonFilter.Etacut = 3.0
genSeq.MultiMuonFilter.NMuons = 2
from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
genSeq += DiLeptonMassFilter()
genSeq.DiLeptonMassFilter.MinPt = 2000.
genSeq.DiLeptonMassFilter.MaxEta = 3.0
genSeq.DiLeptonMassFilter.MinMass = 0.
genSeq.DiLeptonMassFilter.MaxMass = 2700.
genSeq.DiLeptonMassFilter.MinDilepPt = 0.
genSeq.DiLeptonMassFilter.AllowSameCharge = False

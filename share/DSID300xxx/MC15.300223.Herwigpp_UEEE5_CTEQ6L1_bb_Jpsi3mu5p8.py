# setup Herwig++
include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )


evgenConfig.description = "Herwig++ QCD bbar sample with CTEQ6L1 PDF and UE-EE5 tune, bb->J/psi(mumu)+mu"
evgenConfig.keywords = ["inclusive","bottom","bbbar"]
evgenConfig.minevents = 10

# Configure Herwig
cmds = """\
## Set up pp -> X in QCD 

insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#exclude bb, to avoid double counting
set /Herwig/MatrixElements/MEQCD2to2:MaximumFlavour 4

set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 10*GeV

## Set up pp -> bbar in QCD 
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEHeavyQuark
set /Herwig/MatrixElements/MEHeavyQuark:QuarkType Bottom

## tell Herwig++ to include all subprocesses
set /Herwig/MatrixElements/MEHeavyQuark:Process All

## tell Herwig++ to produce the b quark on its mass shell and use this mass in the calculation
set /Herwig/MatrixElements/MEHeavyQuark:CharmBottomMassOption OnMassShell
set /Herwig/Cuts/JetKtCut:MinKT 6*GeV


##turn off B mixing:
set /Herwig/Particles/B0:DeltaM 0
set /Herwig/Particles/B0:DeltaGamma 0
set /Herwig/Particles/Bbar0:DeltaM 0
set /Herwig/Particles/Bbar0:DeltaGamma 0

set /Herwig/Particles/B_s0:DeltaM 0
set /Herwig/Particles/B_s0:DeltaGamma 0
set /Herwig/Particles/B_sbar0:DeltaM 0
set /Herwig/Particles/B_sbar0:DeltaGamma 0


##force some decays:

do  /Herwig/Particles/B0:SelectDecayModes B0->K_10,Jpsi; B0->Jpsi,K*0; B0->Jpsi,K*_20; B0->Jpsi,K+,pi-; B0->Jpsi,K0,pi0; B0->K'_10,Jpsi; B0->Jpsi,phi,K0; B0->Jpsi,omega; B0->Jpsi,rho0; B0->psi(2S),K*0; B0->psi(2S),K+,pi-; B0->psi(2S),K_10; B0->psi(2S),K0,pi+,pi-; B0->psi(2S),K0,pi0; B0->psi(2S),K0,pi0,pi0; B0->psi(2S),K+,pi-,pi0; B0->psi(3770),K*0; B0->psi(3770),K_10; B0->psi(3770),K0,pi+,pi-; B0->psi(3770),K0,pi0; B0->psi(3770),K+,pi-; B0->psi(3770),K0,pi0,pi0; B0->psi(3770),K+,pi-,pi0; B0->chi_c0,K*0; B0->chi_c0,K0,pi+,pi-; B0->chi_c0,K+,pi-; B0->chi_c0,K0,pi0; B0->chi_c0,K0,pi0,pi0; B0->chi_c0,K+,pi-,pi0; B0->chi_c1,K0,pi+,pi-; B0->chi_c1,K+,pi-; B0->chi_c1,K*0; B0->chi_c1,K0,pi0; B0->chi_c1,K0,pi0,pi0; B0->chi_c1,K+,pi-,pi0; B0->chi_c2,K0,pi+,pi-; B0->chi_c2,K+,pi-; B0->chi_c2,K0,pi0; B0->chi_c2,K0,pi0,pi0; B0->chi_c2,K+,pi-,pi0; B0->chi_c2,K*0;

do  /Herwig/Particles/B+:SelectDecayModes B+->K_1+,Jpsi; B+->Jpsi,K*+; B+->Jpsi,K+; B+->Jpsi,K*_2+; B+->Jpsi,K0,pi+; B+->Jpsi,K+,pi0; B+->K'_1+,Jpsi; B+->Jpsi,phi,K+; B+->Jpsi,rho+; B+->Jpsi,pi+; B+->psi(2S),K+; B+->psi(2S),K*+; B+->psi(2S),K0,pi+; B+->psi(2S),K_1+; B+->psi(2S),K+,pi+,pi-; B+->psi(2S),K+,pi0; B+->psi(2S),K0,pi+,pi0; B+->psi(2S),K+,pi0,pi0; B+->psi(3770),K*+; B+->psi(3770),K+; B+->psi(3770),K0,pi+; B+->psi(3770),K_1+; B+->psi(3770),K+,pi+,pi-; B+->psi(3770),K+,pi0; B+->psi(3770),K0,pi+,pi0; B+->psi(3770),K+,pi0,pi0; B+->chi_c0,K*+; B+->chi_c0,K+; B+->chi_c0,K0,pi+; B+->chi_c0,K+,pi+,pi-; B+->chi_c0,K0,pi+,pi0; B+->chi_c0,K+,pi0; B+->chi_c0,K+,pi0,pi0; B+->chi_c1,K+; B+->chi_c1,K0,pi+; B+->chi_c1,K+,pi+,pi-; B+->chi_c1,K*+; B+->chi_c1,K0,pi+,pi0; B+->chi_c1,K+,pi0; B+->chi_c1,K+,pi0,pi0; B+->chi_c2,K0,pi+; B+->chi_c2,K+,pi+,pi-; B+->chi_c2,K0,pi+,pi0; B+->chi_c2,K+,pi0; B+->chi_c2,K+,pi0,pi0; B+->chi_c2,K+; B+->chi_c2,K*+;

do  /Herwig/Particles/B_s0:SelectDecayModes B_s0->Jpsi,phi; B_s0->Jpsi,K+,K-; B_s0->Jpsi,K+,K-,pi0; B_s0->Jpsi,K0,Kbar0; B_s0->Jpsi,K0,Kbar0,pi0; B_s0->Jpsi,K-,K0,pi+; B_s0->Jpsi,eta'; B_s0->Jpsi,eta; B_s0->Jpsi,Kbar0; B_s0->psi(2S),phi; B_s0->psi(2S),eta'; B_s0->psi(2S),K+,K-; B_s0->psi(2S),K+,K-,pi0; B_s0->psi(2S),K0,Kbar0; B_s0->psi(2S),K0,Kbar0,pi0; B_s0->psi(2S),K-,K0,pi+; B_s0->psi(2S),eta; B_s0->chi_c0,phi; B_s0->chi_c0,eta'; B_s0->chi_c0,eta; B_s0->chi_c0,K+,K-; B_s0->chi_c0,K+,K-,pi0; B_s0->chi_c0,K0,Kbar0; B_s0->chi_c0,K0,Kbar0,pi0; B_s0->chi_c0,K-,K0,pi+; B_s0->chi_c1,phi; B_s0->chi_c1,eta'; B_s0->chi_c1,eta; B_s0->chi_c1,K+,K-; B_s0->chi_c1,K+,K-,pi0; B_s0->chi_c1,K0,Kbar0; B_s0->chi_c1,K0,Kbar0,pi0; B_s0->chi_c1,K-,K0,pi+; B_s0->chi_c2,eta'; B_s0->chi_c2,eta; B_s0->chi_c2,K+,K-; B_s0->chi_c2,K+,K-,pi0; B_s0->chi_c2,K0,Kbar0; B_s0->chi_c2,K0,Kbar0,pi0; B_s0->chi_c2,K-,K0,pi+;

do  /Herwig/Particles/B_c+:SelectDecayModes B_c+->Jpsi,nu_mu,mu+; B_c+->Jpsi,nu_e,e+; B_c+->Jpsi,D_s*+; B_c+->Jpsi,nu_tau,tau+; B_c+->Jpsi,rho+; B_c+->Jpsi,D_s+; B_c+->Jpsi,pi+; B_c+->Jpsi,D*+; B_c+->Jpsi,K*+; B_c+->Jpsi,K+; B_c+->Jpsi,D+; B_c+->psi(2S),nu_mu,mu+; B_c+->psi(2S),nu_e,e+; B_c+->psi(2S),nu_tau,tau+;


do  /Herwig/Particles/Lambda_bbar0:SelectDecayModes Lambda_bbar0->Lambdabar0,Jpsi; Lambda_bbar0->psi(2S),Lambdabar0;

do  /Herwig/Particles/Xi_bbar+:SelectDecayModes Xi_bbar+->Xibar+,Jpsi; Xi_bbar+->psi(2S),Xibar+;

do  /Herwig/Particles/Xi_bbar0:SelectDecayModes Xi_bbar0->Xibar0,Jpsi; Xi_bbar0->psi(2S),Xibar0;

do  /Herwig/Particles/Omega_bbar+:SelectDecayModes Omega_bbar+->Omegabar+,Jpsi; Omega_bbar+->psi(2S),Omegabar+;



do /Herwig/Particles/Jpsi:SelectDecayModes Jpsi->mu-,mu+;


#do /Herwig/Particles/B0:PrintDecayModes
#do /Herwig/Particles/Bbar0:PrintDecayModes
#do /Herwig/Particles/B+:PrintDecayModes
#do /Herwig/Particles/B-:PrintDecayModes
#do /Herwig/Particles/B_s0:PrintDecayModes
#do /Herwig/Particles/B_sbar0:PrintDecayModes
#do /Herwig/Particles/B_c+:PrintDecayModes
#do /Herwig/Particles/B_c-:PrintDecayModes
#do /Herwig/Particles/Lambda_b0:PrintDecayModes
#do /Herwig/Particles/Lambda_bbar0:PrintDecayModes
#do /Herwig/Particles/Xi_b-:PrintDecayModes
#do /Herwig/Particles/Xi_bbar+:PrintDecayModes
#do /Herwig/Particles/Xi_b0:PrintDecayModes
#do /Herwig/Particles/Xi_bbar0:PrintDecayModes
#do /Herwig/Particles/Omega_b-:PrintDecayModes
#do /Herwig/Particles/Omega_bbar+:PrintDecayModes

#do /Herwig/Particles/Jpsi:PrintDecayModes
#do /Herwig/Particles/psi(2S):PrintDecayModes
#do /Herwig/Particles/psi(3770):PrintDecayModes
#do /Herwig/Particles/chi_c0:PrintDecayModes
#do /Herwig/Particles/chi_c1:PrintDecayModes
#do /Herwig/Particles/chi_c2:PrintDecayModes
"""

genSeq.Herwigpp.Commands += cmds.splitlines()


include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter
HeavyFlavorBHadronFilter.RequestCharm=0
HeavyFlavorBHadronFilter.RequestBottom=1
HeavyFlavorBHadronFilter.Request_cQuark=0
HeavyFlavorBHadronFilter.Request_bQuark=0
HeavyFlavorBHadronFilter.BottomPtMin=5000.
HeavyFlavorBHadronFilter.BottomEtaMax=3.0


from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter()
MultiMuonFilter.Ptcut=5800.
MultiMuonFilter.Etacut=2.7
MultiMuonFilter.NMuons=3

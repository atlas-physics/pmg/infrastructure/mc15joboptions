##############################################################
# JO intended for electron decay channel studies.
# Pythia8B_i generation with ISR/FSR for decay:
# Bs -> phi (K+K- 0.5GeV) J/psi (mu5.5mu5.5) (flat angles)
##############################################################

evgenConfig.description = "Exclusive Bs->phi_KK_Jpsi_mu5p5mu5p5 production"
evgenConfig.keywords    = ["exclusive", "Bs", "2muon"]
evgenConfig.minevents   = 100

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 11.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands      += ['531:87:onMode = on']
genSeq.Pythia8B.Commands      += ['443:onMode = off']
genSeq.Pythia8B.Commands      += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [  531,   443,   -13,    13,   333, 321, -321]
genSeq.Pythia8B.SignalPtCuts   = [  0.0,   0.0,   0.0,   0.0,   0.0, 0.5,  0.5]
genSeq.Pythia8B.SignalEtaCuts  = [102.5, 102.5, 102.5, 102.5, 102.5, 2.6,  2.6]

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [5.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

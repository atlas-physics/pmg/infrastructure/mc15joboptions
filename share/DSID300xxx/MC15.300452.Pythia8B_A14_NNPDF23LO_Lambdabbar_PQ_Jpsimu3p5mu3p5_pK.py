########################################################################
# Job options for Pythia8B_i generation of Lambda_b->PQ(->J/psi(mumu)p)K
########################################################################
evgenConfig.description = "Signal Lambda_b->PQ(->J/psi(mumu)p)K"
evgenConfig.keywords = ["bottom","exclusive","Lambda_b0","Jpsi","2muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->Lambda_b->PQ(->J/psi(mumu)p)K"
evgenConfig.minevents = 200

#include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/nonStandard/Pythia8B_A14_NNPDF23LO_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2017
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

#
# Lambda_b:
#
genSeq.Pythia8B.Commands += ['5122:m0 = 5.61958']  # PDG 2017
genSeq.Pythia8B.Commands += ['5122:tau0 = 0.4407'] # PDG 2017
#
# Lambda_b decays:
#
genSeq.Pythia8B.Commands += ['5122:onMode = 2']
#
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 0.5 0 443124 -321']
genSeq.Pythia8B.Commands += ['5122:addChannel = 3 0.5 0 443126 -321']

#
# P_c+(4380), 3/2-, with LHCb mass and width
#
genSeq.Pythia8B.Commands += ['443124:new = Pc(4380)+ Pc(4380)bar- 4 0 0 4.380 0.205 4.036 5.125 0.']
genSeq.Pythia8B.Commands += ['443124:addChannel = 1 1. 0 443 2212']

#
# P_c+(4450), 5/2+, with LHCb mass and width
#
genSeq.Pythia8B.Commands += ['443126:new = Pc(4450)+ Pc(4450)bar- 6 0 0 4.4498 0.039 4.036 5.125 0.']
genSeq.Pythia8B.Commands += ['443126:addChannel = 1 1. 0 443 2212']


genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
genSeq.Pythia8B.SignalPtCuts = [0.,3.5,3.5]
genSeq.Pythia8B.SignalEtaCuts = [100.,2.5,2.5]

genSeq.Pythia8B.NHadronizationLoops = 4

include("MC15JobOptions/ParentChildwStatusFilter.py")
filtSeq.ParentChildwStatusFilter.PDGParent  = [5122]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
filtSeq.ParentChildwStatusFilter.PtMinParent =  9000.
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 2.7
filtSeq.ParentChildwStatusFilter.PDGChild = [321]
filtSeq.ParentChildwStatusFilter.PtMinChild = 900.
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 2.7

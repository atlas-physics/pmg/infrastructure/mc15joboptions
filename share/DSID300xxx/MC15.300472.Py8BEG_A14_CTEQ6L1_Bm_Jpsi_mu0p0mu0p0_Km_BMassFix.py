##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B- -> J/psi(mu0.0mu0.0) K-: an analog of MC15.300405
# w/o cuts for muons and kaon   
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B-\n")
f.write("1.0000  myJ/psi   K-             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

evgenConfig.description = "Exclusive Bminus->Jpsi_mu0p0mu0p0_Kminus production"
evgenConfig.keywords = ["exclusive","Bminus","Jpsi","2muon"]
evgenConfig.minevents = 2000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")

### Put the content of MC15JobOptions/Pythia8B_exclusiveB_Common.py
### except actual closing B decays

# Set B+ mass in Pythia equal to that in EvtGen 
genSeq.Pythia8B.Commands += ['521:m0 = 5.27929'] # PDG2014 mass

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")
###
###

#include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

genSeq.Pythia8B.QuarkPtCut = 7.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 2.6
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.NHadronizationLoops = 1

# Make Pythia select the events with B+
genSeq.Pythia8B.SignalPDGCodes = [ -521 ]

genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"

include("MC15JobOptions/ParentChildwStatusFilter.py")
filtSeq.ParentChildwStatusFilter.PDGParent  = [521]
filtSeq.ParentChildwStatusFilter.StatusParent  = [2]
filtSeq.ParentChildwStatusFilter.PtMinParent =  8900.
filtSeq.ParentChildwStatusFilter.EtaRangeParent = 2.5
filtSeq.ParentChildwStatusFilter.PDGChild = [443]
filtSeq.ParentChildwStatusFilter.PtMinChild = 0.
filtSeq.ParentChildwStatusFilter.EtaRangeChild = 999999.

# Final state selections
#filtSeq.BSignalFilter.LVL1MuonCutOn = True
#filtSeq.BSignalFilter.LVL2MuonCutOn = True
#filtSeq.BSignalFilter.LVL1MuonCutPT = 0
#filtSeq.BSignalFilter.LVL1MuonCutEta = 999999.
#filtSeq.BSignalFilter.LVL2MuonCutPT = 0
#filtSeq.BSignalFilter.LVL2MuonCutEta = 999999.

#filtSeq.BSignalFilter.B_PDGCode = -521
#filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
#filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 0.0
#filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 999999.

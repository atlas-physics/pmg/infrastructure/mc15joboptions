# setup Herwig++
include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )


evgenConfig.description = "Herwig++ QCD bbar sample with CTEQ6L1 PDF and UE-EE5 tune"
evgenConfig.keywords = ["inclusive","bottom","Jpsi","3muon","bbbar"]
evgenConfig.minevents = 1000

# Configure Herwig
cmds = """\
## Set up pp -> X in QCD 

insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#exclude bb, to avoid double counting
set /Herwig/MatrixElements/MEQCD2to2:MaximumFlavour 4

set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 10*GeV

## Set up pp -> bbar in QCD 
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEHeavyQuark
set /Herwig/MatrixElements/MEHeavyQuark:QuarkType Bottom

## tell Herwig++ to include all subprocesses
set /Herwig/MatrixElements/MEHeavyQuark:Process All

## tell Herwig++ to produce the b quark on its mass shell and use this mass in the calculation
set /Herwig/MatrixElements/MEHeavyQuark:CharmBottomMassOption OnMassShell
set /Herwig/Cuts/JetKtCut:MinKT 6*GeV


##turn off B mixing:
set /Herwig/Particles/B0:DeltaM 0
set /Herwig/Particles/B0:DeltaGamma 0
set /Herwig/Particles/Bbar0:DeltaM 0
set /Herwig/Particles/Bbar0:DeltaGamma 0

set /Herwig/Particles/B_s0:DeltaM 0
set /Herwig/Particles/B_s0:DeltaGamma 0
set /Herwig/Particles/B_sbar0:DeltaM 0
set /Herwig/Particles/B_sbar0:DeltaGamma 0
"""

genSeq.Herwigpp.Commands += cmds.splitlines()


include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter
HeavyFlavorBHadronFilter.RequestCharm=0
HeavyFlavorBHadronFilter.RequestBottom=1
HeavyFlavorBHadronFilter.Request_cQuark=0
HeavyFlavorBHadronFilter.Request_bQuark=0
HeavyFlavorBHadronFilter.BottomPtMin=10000.
HeavyFlavorBHadronFilter.BottomEtaMax=2.7


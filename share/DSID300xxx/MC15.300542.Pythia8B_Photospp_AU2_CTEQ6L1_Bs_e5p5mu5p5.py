##############################################################
# JO intended for electron decay channel studies.
# Pythia8B_i generation with ISR/FSR for LFV decay:
# Bs -> (e5.5mu5.5) (flat angles)
##############################################################

evgenConfig.description = "Exclusive Bs->e5p5mu5p5 production"
evgenConfig.keywords    = ["exclusive", "Bs", "muon", "electron", "rareDecay"]
evgenConfig.minevents   = 1000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 11.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands      += ['531:addChannel = 2 1.0 0 -13 11']
genSeq.Pythia8B.SignalPDGCodes = [531, -13, 11]

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [5.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [1]

if not hasattr( filtSeq, "MultiElectronFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
  filtSeq += MultiElectronFilter()
  pass
filtSeq.MultiElectronFilter.NElectrons = 1
filtSeq.MultiElectronFilter.Ptcut      = 5500.0
filtSeq.MultiElectronFilter.Etacut     = 2.6

#######################################################################
# Job options for Pythia8B_i generation of Lambda_b->J/psi(mumu)Lambda
#######################################################################
evgenConfig.description = "Signal Bd->J/psi(mumu)Kpi"
evgenConfig.keywords = ["exclusive","Jpsi","2muon","Bd"]
evgenConfig.minevents = 200

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.'] 
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 9.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# define pseudo-J/psi for exclusive decay
genSeq.Pythia8B.Commands += ['999443:all = myJ/psi void 3 0 0 3.09692 0.00009 3.09602 3.09782 0']  # name antiName spinType chargeType colType m0 mWidth mMin mMax tau0

# force its decay to mu+ mu-
genSeq.Pythia8B.Commands += ['999443:onMode = off'] 
genSeq.Pythia8B.Commands += ['999443:oneChannel = on 1. 0 -13 13'] # onMode bRatio meMode products


genSeq.Pythia8B.Commands += ['511:addChannel = 2 1. 0 999443 321 -211'] # onMode bRatio meMode products


genSeq.Pythia8B.SignalPDGCodes = [   511, 999443,   -13,    13, 321, -211 ]
genSeq.Pythia8B.SignalPtCuts   = [   0.0,    0.0,   0.0,   0.0, 0.8,  0.8 ] # no muon eta cut here - see trigger cuts below
genSeq.Pythia8B.SignalEtaCuts  = [ 102.5,  102.5, 102.5, 102.5, 2.6,  2.6 ] # no muon eta cut here - see trigger cuts below

genSeq.Pythia8B.OutputLevel = INFO

genSeq.Pythia8B.NHadronizationLoops = 25

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.TriggerOppositeCharges = True
genSeq.Pythia8B.MinimumCountPerCut = [2]

include('MC15JobOptions/BSignalFilter.py')
filtSeq.BSignalFilter.LVL1MuonCutOn  = False
filtSeq.BSignalFilter.LVL2MuonCutOn  = False
filtSeq.BSignalFilter.Cuts_Final_mu_switch = True
filtSeq.BSignalFilter.B_PDGCode = 511
filtSeq.BSignalFilter.InvMass_switch = True
filtSeq.BSignalFilter.InvMass_PartId1 =  321
filtSeq.BSignalFilter.InvMass_PartId2 = -211
filtSeq.BSignalFilter.InvMassMax = 1050.
filtSeq.BSignalFilter.InvMassMin = 990.
filtSeq.BSignalFilter.TotalInvMass_switch = True
filtSeq.BSignalFilter.TotalInvMassMax = 5850.
filtSeq.BSignalFilter.TotalInvMassMin = 4950.
filtSeq.BSignalFilter.InvMass_PartFakeMass1 = 493.677
filtSeq.BSignalFilter.InvMass_PartFakeMass2 = 493.677

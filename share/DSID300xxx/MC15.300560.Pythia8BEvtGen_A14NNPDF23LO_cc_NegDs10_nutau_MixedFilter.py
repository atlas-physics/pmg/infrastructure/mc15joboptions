#################################################################################
# job options fragment for cc->NegDs10->nutau->mu4mu2mu1
# For HF tau3mu based on Meson Decay(MD) study for run2
#################################################################################
# All production channels of cc are included in this fragment.
# D_s meson force decaying into tau+nu and tau also must decay into 3muons
# D_s pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>10.5 or 3.5GeV, mu2>5.5 or 3.5GeV, mu3> 2 or 3.5GeV, and D_s>10GeV
#################################################################################

f = open("NegDs_TauNu_3MuNu_USER.DEC", "w")
f.write("Alias mytau- tau-\n")
f.write("Decay D_s-\n")
f.write("1.0000   mytau-   anti-nu_tau               SLN;\n")
f.write("Enddecay\n")
f.write("Decay mytau-\n")
f.write("1.0000   mu-      mu+          mu-          PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description = "cc->NegDs->taunu->3munu production"
evgenConfig.keywords = ["charmonium", "tau", "muon", "BSM"]
evgenConfig.minevents = 200
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch']
evgenConfig.process = "cc>Ds>taunu>3munu"

genSeq.Pythia8B.Commands += ['HardQCD:all = on']
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']

genSeq.Pythia8B.SelectBQuarks = False
genSeq.Pythia8B.SelectCQuarks = True
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 10.0
genSeq.Pythia8B.QuarkEtaCut = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = False
genSeq.Pythia8B.VetoDoubleCEvents = True

genSeq.Pythia8B.NHadronizationLoops = 10
genSeq.Pythia8B.NDecayLoops = 1

genSeq.EvtInclusiveDecay.userDecayFile = "NegDs_TauNu_3MuNu_USER.DEC"

from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter

filtSeq += TripletChainFilter("LowPtTripletFilter")
filtSeq += TripletChainFilter("HighPtTripletFilter")
filtSeq.Expression = "LowPtTripletFilter or HighPtTripletFilter"

TripletChainFilter = filtSeq.LowPtTripletFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = 13
TripletChainFilter.PdgId2 = -13
TripletChainFilter.PdgId3 = 13
TripletChainFilter.NStep1 = 1
TripletChainFilter.NStep2 = 1
TripletChainFilter.NStep3 = 1
TripletChainFilter.PtMin1 = 3500
TripletChainFilter.PtMin2 = 3500
TripletChainFilter.PtMin3 = 3500
TripletChainFilter.EtaMax1 = 3.0
TripletChainFilter.EtaMax2 = 3.0
TripletChainFilter.EtaMax3 = 3.0
TripletChainFilter.TripletPdgId = 15
TripletChainFilter.TripletPtMin = 0
TripletChainFilter.TripletEtaMax = 100
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.ParentPdgId = -431
TripletChainFilter.ParentPtMin = 10000
TripletChainFilter.ParentEtaMax = 3.0
TripletChainFilter.ParentMassMin = 0
TripletChainFilter.ParentMassMax = 10000000

TripletChainFilter2 = filtSeq.HighPtTripletFilter
TripletChainFilter2.NTriplet = 1
TripletChainFilter2.PdgId1 = 13
TripletChainFilter2.PdgId2 = -13
TripletChainFilter2.PdgId3 = 13
TripletChainFilter2.NStep1 = 1
TripletChainFilter2.NStep2 = 1
TripletChainFilter2.NStep3 = 1
TripletChainFilter2.PtMin1 = 10500
TripletChainFilter2.PtMin2 = 5500
TripletChainFilter2.PtMin3 = 2000
TripletChainFilter2.EtaMax1 = 3.0
TripletChainFilter2.EtaMax2 = 3.0
TripletChainFilter2.EtaMax3 = 3.0
TripletChainFilter2.TripletPdgId = 15
TripletChainFilter2.TripletPtMin = 0
TripletChainFilter2.TripletEtaMax = 100
TripletChainFilter2.TripletMassMin = 0
TripletChainFilter2.TripletMassMax = 10000000
TripletChainFilter2.ParentPdgId = -431
TripletChainFilter2.ParentPtMin = 10000
TripletChainFilter2.ParentEtaMax = 3.0
TripletChainFilter2.ParentMassMin = 0
TripletChainFilter2.ParentMassMax = 10000000

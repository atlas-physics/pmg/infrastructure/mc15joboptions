######################################################
# Job options for Pythia8B_i generation of Bds->h0p8h0p8   
######################################################

evgenConfig.description = "Exclusive Bds->hh production, 0.8 GeV cut on final hadrons"
evgenConfig.keywords = ["exclusive","Bs","B0","bottom"]
evgenConfig.minevents = 1000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")
include("MC15JobOptions/BSignalFilter.py")

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

#include("MC12JobOptions/BSignalFilter.py")
# Instead, manually create two separate instances of
# BSignalFilter, for B0 and B0s decays

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 2.']

genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 2.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.NHadronizationLoops = 1

# pi+ pi-
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.00073 0 -211 211']
# K+ K-
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.0245 0 -321 321']
# pi+ K-
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.0027 0 -321 211']
# K+ pi-
genSeq.Pythia8B.Commands += ['531:addChannel = 2 0.0027 0 -211 321']

# pi+ pi-
genSeq.Pythia8B.Commands += ['511:addChannel = 2 0.00510 0 -211 211']
# K+ K-
genSeq.Pythia8B.Commands += ['511:addChannel = 2 0.00012 0 -321 321']
# pi+ K-
genSeq.Pythia8B.Commands += ['511:addChannel = 2 0.0098 0 -321 211']
# K+ pi-
genSeq.Pythia8B.Commands += ['511:addChannel = 2 0.0098 0 -211 321']

# Just require a b-quark - it automatically makes Pythia8B to reject events with
# undecayed 521, 541 etc, while signal channel filtering is not really needed
# since only signal decays are switched on.
genSeq.Pythia8B.SignalPDGCodes = [5] 

# Fitering B->hh
from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
BSignalFilter511 = BSignalFilter("BSignalFilter511")
filtSeq += BSignalFilter511
BSignalFilter531 = BSignalFilter("BSignalFilter531")
filtSeq += BSignalFilter531

filtSeq.Expression = "BSignalFilter511 or BSignalFilter531" 

# Filtering B0 hadron daughters
filtSeq.BSignalFilter511.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter511.Cuts_Final_hadrons_pT = 800. 
filtSeq.BSignalFilter511.Cuts_Final_hadrons_eta = 2.6
filtSeq.BSignalFilter511.B_PDGCode = 511

# Filtering B0s hadron daughters
filtSeq.BSignalFilter531.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter531.Cuts_Final_hadrons_pT = 800.
filtSeq.BSignalFilter531.Cuts_Final_hadrons_eta = 2.6
filtSeq.BSignalFilter531.B_PDGCode = 531

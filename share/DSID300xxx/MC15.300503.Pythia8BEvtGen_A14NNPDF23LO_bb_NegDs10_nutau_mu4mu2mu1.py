#################################################################################
# job options fragment for bb->NegDs10->nutau->mu4mu2mu1
# For HF tau3mu based on Meson Decay(MD) study for run2
#################################################################################
# All production channels of bb are included in this fragment.
# D_s meson force decaying into tau+nu and tau also must decay into 3muons
# D_s pT is limited as high because we can get only boosted signature, at least.
# thresholds: mu1>4GeV, mu2>2GeV, mu3>1GeV, and D_s>10GeV
#################################################################################

f = open("NegDs_TauNu_3MuNu_USER.DEC","w")
f.write("Alias mytau- tau-\n")
f.write("Decay D_s-\n")
f.write("1.0000   mytau-   anti-nu_tau               SLN;\n")
f.write("Enddecay\n")
f.write("Decay mytau-\n")
f.write("1.0000   mu-      mu+          mu-          PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.description  = "bb->NegDs->taunu->3munu production"
evgenConfig.keywords     = [ "charmonium", "tau", "muon", "BSM" ]
evgenConfig.minevents    = 500
evgenConfig.contact      = [ 'dai.kobayashi@cern.ch' ]
evgenConfig.process      = "bb>Ds>taunu>3munu"

genSeq.Pythia8B.Commands       += [ 'HardQCD:all = on' ]
genSeq.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
genSeq.Pythia8B.Commands       += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands       += [ 'HadronLevel:all = off' ]

genSeq.Pythia8B.SelectBQuarks      = True
genSeq.Pythia8B.SelectCQuarks      = False
genSeq.Pythia8B.QuarkPtCut         = 10.0
genSeq.Pythia8B.AntiQuarkPtCut     = 10.0
genSeq.Pythia8B.QuarkEtaCut        = 4.5
genSeq.Pythia8B.AntiQuarkEtaCut    = 4.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = False

genSeq.Pythia8B.NHadronizationLoops   = 10
genSeq.Pythia8B.NDecayLoops           = 1

genSeq.EvtInclusiveDecay.userDecayFile = "NegDs_TauNu_3MuNu_USER.DEC"

#Add the Filters:
from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter
genSeq += TripletChainFilter()

TripletChainFilter = genSeq.TripletChainFilter
TripletChainFilter.NTriplet       = 1
TripletChainFilter.PdgId1         = 13
TripletChainFilter.PdgId2         = -13
TripletChainFilter.PdgId3         = 13
TripletChainFilter.NStep1         = 1
TripletChainFilter.NStep2         = 1
TripletChainFilter.NStep3         = 1
TripletChainFilter.PtMin1         = 4000
TripletChainFilter.PtMin2         = 2000
TripletChainFilter.PtMin3         = 1000
TripletChainFilter.EtaMax1        = 3.0
TripletChainFilter.EtaMax2        = 3.0
TripletChainFilter.EtaMax3        = 3.0
TripletChainFilter.TripletPdgId   = 15
TripletChainFilter.TripletPtMin   = 0
TripletChainFilter.TripletEtaMax  = 100
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.ParentPdgId    = -431
TripletChainFilter.ParentPtMin    = 10000
TripletChainFilter.ParentEtaMax   = 3.0
TripletChainFilter.ParentMassMin  = 0
TripletChainFilter.ParentMassMax  = 10000000

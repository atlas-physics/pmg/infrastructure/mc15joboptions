##############################################################
# Sample for B to lepton-lepton X study
# Pythia8B_i generation with ISR/FSR for decay:
#  B+ -> K+ J/psi(mumu) 
##############################################################
f = open("Bp_Kp_JPSI_MUMU_USER.DEC","w") 
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_J/psi J/psi\n")
f.write("Decay  my_J/psi\n")
f.write("1.0000   mu+ mu-   PHOTOS VLL; #[Reconstructed PDG2011]\n")
f.write("Enddecay\n")
f.write("Decay B+\n")
f.write("1.0000   my_J/psi   K+   SVS;\n")
f.write("Enddecay\n") 
f.write("End\n") 
f.close() 
##############################################################
evgenConfig.description = "Exclusive B+ -> K+ J/psi(mumu)  production"
evgenConfig.keywords    = ["exclusive", "bottom", "2muon"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/BSignalFilter.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
genSeq.Pythia8B.VetoDoubleBEvents = True
genSeq.Pythia8B.VetoDoubleCEvents = True

# List of B-species
include("MC15JobOptions/Pythia8B_BPDGCodes.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7']
genSeq.Pythia8B.NDecayLoops = 1
genSeq.Pythia8B.NHadronizationLoops = 2
        
genSeq.Pythia8B.QuarkPtCut      = 0.0
genSeq.Pythia8B.QuarkEtaCut     = 102.5
genSeq.Pythia8B.AntiQuarkPtCut  = 7.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [521]
genSeq.EvtInclusiveDecay.userDecayFile = "Bp_Kp_JPSI_MUMU_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutOn  = True
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.7
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.7
       
filtSeq.BSignalFilter.LVL1MuonCutPT  = 4000.0
filtSeq.BSignalFilter.LVL2MuonCutPT  = 4000.0
filtSeq.BSignalFilter.B_PDGCode = 521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

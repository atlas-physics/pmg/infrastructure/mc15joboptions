##############################################################
# B0d -> J/psi (mu3.5mu3.5) K0S
##############################################################

evgenConfig.description = "Exclusive B0d -> J/psi (mu3.5mu3.5) K0S"
evgenConfig.keywords    = ["exclusive", "B0", "2muon", "Jpsi", "K0S"]
evgenConfig.minevents   = 1000

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 8.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands      += ['511:onIfMatch = 443 310']
genSeq.Pythia8B.Commands      += ['443:onMode = off']
genSeq.Pythia8B.Commands      += ['443:2:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [ 511, 443, -13, 13, 310 ]

genSeq.Pythia8B.NHadronizationLoops = 1

genSeq.Pythia8B.TriggerPDGCode     = 13
genSeq.Pythia8B.TriggerStatePtCut  = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [2]

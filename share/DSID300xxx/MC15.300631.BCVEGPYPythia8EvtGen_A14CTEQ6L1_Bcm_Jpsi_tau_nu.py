f = open("BcMinus_JpsiTau_USER.DEC", "w")
f.write("Alias myJPsi J/psi\n")
f.write("Alias myTau tau-\n")
f.write("Decay B_c-\n")
f.write("1.0000  myJPsi  myTau anti-nu_tau                                  PHOTOS BC_VMN 1 ;\n")
f.write("Enddecay\n")
f.write("Decay myJPsi\n")
f.write("1.0000    mu+  mu-          PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("Decay myTau\n")
f.write("1.0000    mu-  anti-nu_mu nu_tau         TAULNUNU;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

#################################################################################

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_BCVEGPY.py")
include("MC15JobOptions/Pythia8_BcStates.py")

evgenConfig.description = "hf->Bc->JPsi + tau + nu inclusive production"
evgenConfig.keywords = ["tau", "muon", "BSM"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "BcMinus"
evgenConfig.contact = ['marcus.matthias.morgenstern@cern.ch','jia.jian.teoh@cern.ch']
evgenConfig.process = "Bc-->tau(->mu+nu+nu) + J/Psi(->2mu) + nu"

genSeq.EvtInclusiveDecay.userDecayFile = "BcMinus_JpsiTau_USER.DEC"
evgenConfig.auxfiles += ['inclusiveP8_BcPDG18.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG18.pdt"

from GeneratorFilters.GeneratorFiltersConf import TripletChainFilter

filtSeq += TripletChainFilter()

TripletChainFilter = filtSeq.TripletChainFilter
TripletChainFilter.NTriplet = 1
TripletChainFilter.PdgId1 = 13
TripletChainFilter.PdgId2 = -13
TripletChainFilter.PdgId3 = 13
TripletChainFilter.NStep1 = 2
TripletChainFilter.NStep2 = 2
TripletChainFilter.NStep3 = 2
TripletChainFilter.PtMin1 = 5500
TripletChainFilter.PtMin2 = 5500
TripletChainFilter.PtMin3 = 3500
TripletChainFilter.EtaMax1 = 2.7
TripletChainFilter.EtaMax2 = 2.7
TripletChainFilter.EtaMax3 = 2.7
TripletChainFilter.TripletPdgId = -541
TripletChainFilter.TripletPtMin = 1000
TripletChainFilter.TripletEtaMax = 3.0
TripletChainFilter.TripletMassMin = 0
TripletChainFilter.TripletMassMax = 10000000
TripletChainFilter.DoubletPdgId = 443
TripletChainFilter.DoubletPtMin = 0
TripletChainFilter.DoubletEtaMax = 100
TripletChainFilter.DoubletMassMin = 0
TripletChainFilter.DoubletMassMax = 10000000
TripletChainFilter.ThirdParentPdgId = 15
TripletChainFilter.ThirdParentPtMin = 0
TripletChainFilter.ThirdParentEtaMax = 100

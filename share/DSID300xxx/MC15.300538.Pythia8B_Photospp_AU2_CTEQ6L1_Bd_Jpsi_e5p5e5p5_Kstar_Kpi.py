##############################################################
# JO intended for electron decay channel studies.
# Pythia8B_i generation with ISR/FSR for decay:
# B0 -> K*0 (K+pi- 0.5GeV) J/psi (e5.5e5.5) (flat angles)
##############################################################

evgenConfig.description = "Exclusive B0->K*0_Kpi_Jpsi_e5p5e5p5 production"
evgenConfig.keywords    = ["exclusive", "B0", "2electron"]
evgenConfig.minevents   = 500

include("MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py")
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_exclusiveB_Common.py")
genSeq.Pythia8B.VetoDoubleBEvents = True

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.']

genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 11.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

genSeq.Pythia8B.Commands      += ['511:onIfMatch = 443 313']
genSeq.Pythia8B.Commands      += ['443:onMode = off']
genSeq.Pythia8B.Commands      += ['443:1:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [  511,   443,   -11,    11,   313, 321, -211]
genSeq.Pythia8B.SignalPtCuts   = [  0.0,   0.0,   0.0,   0.0,   0.0, 0.5,  0.5]
genSeq.Pythia8B.SignalEtaCuts  = [102.5, 102.5, 102.5, 102.5, 102.5, 2.6,  2.6]

genSeq.Pythia8B.NHadronizationLoops = 4

genSeq.Pythia8B.TriggerPDGCode     = 11
genSeq.Pythia8B.TriggerStatePtCut  = [5.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

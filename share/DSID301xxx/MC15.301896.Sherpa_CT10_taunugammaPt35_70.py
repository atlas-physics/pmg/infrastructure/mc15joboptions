include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W to tau nu gamma production with up to three jets in ME+PS and 35<pT_gamma<70 GeV."
evgenConfig.keywords = [ "electroweak", "tau", "photon", "neutrino", "SM" ]
evgenConfig.inputconfcheck = "taunugammaPt35_70"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch", "atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  16 -15 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;

  Process 93 93 ->  -16 15 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  PT 22  35 70
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""

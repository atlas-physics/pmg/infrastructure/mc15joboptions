evgenConfig.description = "Long-lived Z' -> mumu (ct = 250 mm, m = 250 GeV)"
evgenConfig.keywords = ["BSM","exotic","longLived"]
evgenConfig.contact = ["siinn.che@cern.ch"]
evgenConfig.process = "Long-lived Z' -> mu + mu"

# Specify A14NNPDF23LO PDF
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# Z' Parameters
ZprimeMass = 250.
ZprimeLifetime = 250.


genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"]		# Allow long-lived particles to decay
genSeq.Pythia8.Commands += ["32:name = Zprime"]       			# set Z' name
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]	# create Z' bosons
genSeq.Pythia8.Commands += ["Zprime:gmZmode = 3"]			# Z',Z,g with interference
genSeq.Pythia8.Commands += ["32:onMode = off"]				# Turn off all Z' decays
genSeq.Pythia8.Commands += ["32:onIfAny = 13"]				# Switch on Z'->mumu decay
genSeq.Pythia8.Commands += ["32:m0 = "+str(ZprimeMass)] 		# Z' mass
genSeq.Pythia8.Commands += ["32:tau0 = "+str(ZprimeLifetime)]           # Set Z' ctau


# Turn off checks for displaced vertices. Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000				#In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

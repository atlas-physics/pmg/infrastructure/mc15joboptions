evgenConfig.description = "Quantum black holes RS1 (n = 1, M_th = 1.0 TeV) decaying to electron+muon."
evgenConfig.process = "QBH -> e+- mu-+"
evgenConfig.keywords = ["exotic", "blackhole", "extraDimensions", "RandallSundrum", "warpedED"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_emu"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")

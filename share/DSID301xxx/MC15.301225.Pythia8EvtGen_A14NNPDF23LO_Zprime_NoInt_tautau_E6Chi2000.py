# Zprime resonance mass (in GeV)
ZprimeMass = 2000

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z' bosons
                            "Zprime:gmZmode = 3",                  # Z',Z,g with interference
                            "Zprime:vd = 0.783156008298",
                            "Zprime:ad = -0.391578004149",
                            "Zprime:vu = 0.0",
                            "Zprime:au = 0.391578004149",
                            "Zprime:ve = -0.783156008298",
                            "Zprime:ae = -0.391578004149",
                            "Zprime:vnue = -0.587367006224",
                            "Zprime:anue = -0.587367006224",
                            "32:onMode = off",                     # switch off all Z decays
                            "32:onIfAny  = 15",                    # switch on Z->tautau decays
                            "32:m0 = "+str(ZprimeMass)]


# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime decaying to two taus'
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'resonance', 'electroweak', '2tau' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Zprime>tautau"

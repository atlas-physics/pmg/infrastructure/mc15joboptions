#--------------------------------------------------------------
# Powheg DMV setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_DMS_tloop_Common.py')
PowhegConfig.decaymode = -2 #pseudo-scalar mediator
PowhegConfig.DM_mass = 1
PowhegConfig.phi_mass  = 1000
PowhegConfig.phi_width = 95.347976
PowhegConfig.gDM = 1
PowhegConfig.gSM = 1
PowhegConfig.runningwidth = 0
PowhegConfig.bornktmin = 150 #Intended for analyses with MET>300 GeV
PowhegConfig.bornsuppfact = 1000 #Ensure sufficient statistics at high MET
#Strictly speaking one should use LO PDF, using NLO is not wrong.
PowhegConfig.PDF = range(263000, 263101) # NNPDF30_lo_as_0130
#The mass_low and mass_high parameters are computed internally
PowhegConfig.mass_low = -1
PowhegConfig.mass_high = -1
PowhegConfig.LOevents = 1 #This is a LO process.
PowhegConfig.bornonly = 1 #This is a LO process.
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with A14 NNPDF23LO 
#--------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include('MC15JobOptions/Pythia8_Powheg.py')

# id:all = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0
genSeq.Pythia8.Commands += [ '1000022:all = X Xbar 2 0 0 %f 0.0 0.0 0.0 0.0'%PowhegConfig.DM_mass,
                             '1000022:isVisible = false' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 DMS t-loop pseudo-scalar mediator (DM mass = 1 GeV, mediator mass = 1000 GeV, gSM = gDM = 1) for mono-jet analysis (suitable for MET > 300 GeV)'
evgenConfig.keywords    = [ 'BSM', 'WIMP', 'invisible', 'exotic' ]
evgenConfig.contact     = [ 'David Salek <david.salek@cern.ch>' ]
evgenConfig.process     = 'pp->P->XXbar'

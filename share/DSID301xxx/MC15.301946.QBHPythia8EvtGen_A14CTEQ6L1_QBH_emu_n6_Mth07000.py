evgenConfig.description = "Quantum black holes (n = 6, M_th = 7.0 TeV) decaying to electron+muon."
evgenConfig.process = "QBH -> e+- mu-+"
evgenConfig.keywords = ["exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_emu"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")

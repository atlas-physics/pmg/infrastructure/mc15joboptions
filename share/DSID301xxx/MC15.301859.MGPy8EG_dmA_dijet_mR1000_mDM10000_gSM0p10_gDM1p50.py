model  = 'dmA'
mR     = 1000
mDM    = 10000
gSM    = 0.10
gDM    = 1.50
widthR = 4.635677

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijet.py")

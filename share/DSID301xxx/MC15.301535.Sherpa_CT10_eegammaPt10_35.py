include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z to e e gamma production with up to three jets in ME+PS and 10<pT_gamma<35 GeV."
evgenConfig.keywords = [ "electroweak", "2electron", "photon","SM" ]
evgenConfig.inputconfcheck = "eegammaPt10_35"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "stefano.manzoni@cern.ch","atlas-generators-sherpa@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  Process 93 93 ->  11 -11 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  PT 22  10 35
  Mass 90 90 10 7000
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""

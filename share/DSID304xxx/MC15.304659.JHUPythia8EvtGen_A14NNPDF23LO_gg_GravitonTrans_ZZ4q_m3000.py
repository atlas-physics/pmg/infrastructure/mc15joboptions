include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "JHU+Pythia8, Graviton, Transverse, mVV = 3000, @LO"
evgenConfig.keywords = ["BSM", "spin2", "exotic", "diboson", "jets" ]
evgenConfig.contact  = [ "sebastian.andres.olivares.pino@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.generators = ["JHU", "Pythia8", "EvtGen"]
evgenConfig.process = "gg -> G -> ZZ -> jet+jet"
evgenConfig.inputfilecheck = 'gg_GravitonTransverse_ZZ4q_m3000'

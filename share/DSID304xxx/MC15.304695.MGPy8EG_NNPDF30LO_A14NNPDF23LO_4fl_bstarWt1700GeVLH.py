from MadGraphControl.MadGraphUtils import *
import re


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model Bprime
define p = p
define l- = l- ta-
define l+ = l+ ta+
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define j = j b b~
define ferm = j l+ l- vl vl~
define tt = t t~
define WW = w+ w-
define ZH = Z h
define bb = b b~
define bstar = bp bp~
generate p p > bstar bb QAD=2 QCD=1 QED=2, (bstar > WW tt, tt > bb ferm ferm, WW > ferm ferm)
output -f
""")
fcard.flush()

process_dir = new_process()
print "process_dir"
print "process_dir"
print "process_dir"
print "process_dir"
print process_dir

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

#Bprime Mass [GeV]  Width [GeV] 
bprimemasswidth = [
[ 300 , 9.52617 ],
[ 350 , 12.4645 ],
[ 400 , 15.3614 ],
[ 450 , 18.214 ],
[ 500 , 21.0219 ],
[ 550 , 23.7891 ],
[ 600 , 26.5207 ],
[ 650 , 29.2219 ],
[ 700 , 31.8972 ],
[ 750 , 34.5506 ],
[ 800 , 37.1852 ],
[ 850 , 39.8038 ],
[ 900 , 42.4087 ],
[ 950 , 45.0017 ],
[ 1000 , 47.5845 ],
[ 1050 , 50.1582 ],
[ 1100 , 52.7241 ],
[ 1150 , 55.2831 ],
[ 1200 , 57.8359 ],
[ 1250 , 60.3833 ],
[ 1300 , 62.9258 ],
[ 1350 , 65.4639 ],
[ 1400 , 67.9982 ],
[ 1450 , 70.5289 ],
[ 1500 , 73.0564 ],
[ 1550 , 75.581 ],
[ 1600 , 78.103 ],
[ 1650 , 80.6225 ],
[ 1700 , 83.1399 ],
[ 1750 , 85.6553 ],
[ 1800 , 88.1688 ],
[ 1850 , 90.6805 ],
[ 1900 , 93.1907 ],
[ 1950 , 95.6995 ],
[ 2000 , 98.2068 ],
[ 2050 , 100.713 ],
[ 2100 , 103.218 ],
[ 2150 , 105.722 ],
[ 2200 , 108.225 ],
[ 2250 , 110.727 ],
]

width=0.0
mass=int(re.findall(r'\d+',re.findall(r'bstarWt\d+GeV',runArgs.jobConfig[0])[0])[0])
LH=False
RH=False
if re.findall(r'GeVRH',runArgs.jobConfig[0]) == []:
    LH=True
RH = not LH
if not re.findall(r'GeVTH',runArgs.jobConfig[0]) == []:
    LH=True
    RH=True

for [x,y] in bprimemasswidth:
    if(x==mass):
        width=y


print "Detected mass: " + str(mass)
print "Corresponding bstar width: " + str(width)
print "Is LH? " + str(LH)

# allow usage of all PDF sets
os.environ['LHAPATH']=os.environ["LHAPDF_DATA_PATH"]=(os.environ['LHAPATH'].split(':')[0])+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
# especially 263400 = NNPDF30_lo_as_0130_nf_4

extras = { 'lhe_version'   : '2.0',
           'cut_decays'    : 'F',
           'pdlabel'       : "'lhapdf'",
           'lhaid'         : '263400',
           'bwcutoff'      : '10000',
           'cut_decays'    : 'F',
}
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir), run_card_new='run_card.dat', nevts=runArgs.maxEvents * 1.1, rand_seed=runArgs.randomSeed, beamEnergy=beamEnergy, xqcut=0., extras=extras)

if not os.access(process_dir+'/Cards/param_card.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open(process_dir+'/Cards/param_card.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if 'MBP' in line:
            newcard.write('  1005 %i # MBP \n'%(mass))  ## is set at top
        elif 'DECAY 1005' in line:
            newcard.write('DECAY 1005 %f  # WBP \n'%(width))  ## got from table if mass is known and in table
        elif 'fnewL' in line:
            newcard.write('    1 %fe+00 # fnewL \n'%(LH))  ## is set at top
        elif 'fnewR' in line:
            newcard.write('    2 %fe+00 # fnewR \n'%(not LH))  ## is set at top as well
        elif '# MB ' in line:
            newcard.write('    5 4.950000e+00 # MB \n')  
        elif '# MT ' in line:
            newcard.write('    6 172.5000e+00 # MT \n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

runName='run_01'
generate(run_card_loc='./run_card.dat',param_card_loc='./param_card.dat',run_name=runName,proc_dir=process_dir)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')  

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")



evgenConfig.description = "MadGraph+Pythia8 production JO A14NNPDF23LO for bstar"

evgenConfig.keywords = ["BSM", "exotic"]

evgenConfig.process = "b*_Wt"

evgenConfig.contact =  ['dsperlic@cern.ch']

evgenConfig.minevents = 5000

runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


###############################################################
# Pythia8 H_v -> Z + Z_d -> l+l- + CRjet
# contact: Michael Werner (mdwerner@iastate.edu)
#===============================================================
evgenConfig.description = "Massive Scalar to a SM Z boson which decays leptonically and a Z_dark displaced decay"
evgenConfig.keywords = ["exotic",  "hiddenValley", "longLived", "BSM"]
evgenConfig.process = "Phi -> Z(l+ l-) + Zd(displaced jet)"
evgenConfig.contact = ["mdwerner@iastate.edu"]

# Specify MSTW2008LO PDF
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay
 
#
genSeq.Pythia8.Commands += ["35:name = H_v"]       # Set H_v name
genSeq.Pythia8.Commands += ["36:name = Z_d"]      # Set Z_d name

genSeq.Pythia8.Commands += ["Higgs:useBSM = on"]   # Turn on BSM Higgses
genSeq.Pythia8.Commands += ["HiggsBSM:gg2H2 = on"] # Turn on gg->H_v production

genSeq.Pythia8.Commands += ["35:onMode = off"]     # Turn off all H_v decays
genSeq.Pythia8.Commands += ["35:onIfAll = 36 23"]  # Turn back on H_v->Z Z_d
genSeq.Pythia8.Commands += ["23:onMode = off", "23:onIfAll = 11 -11", "23:onIfAll = 13 -13"] # Restrict SM Z boson decays to electrons and muons
genSeq.Pythia8.Commands += ["36:onMode = off", "36:onIfAll = 1 -1", "36:onIfAll = 2 -2", "36:onIfAll = 3 -3", "36:onIfAll = 4 -4", "36:onIfAll = 5 -5", "36:onIfAll = 6 -6", "36:onIfAll = 21 21"] #Restrict the Z_d decays to hadronic channels

genSeq.Pythia8.Commands += ["35:m0 = 500"]         # Set H_v mass

genSeq.Pythia8.Commands += ["36:m0 = 20"]          # Set Z_d mass
genSeq.Pythia8.Commands += ["36:tau0 = 400"]      # Set Z_d lifetime
genSeq.Pythia8.Commands += ["36:mMin = 19"]         # Explicitly set min and max as Pythia
genSeq.Pythia8.Commands += ["36:mMax = 21"]        # bug prevents it doing it automatically.

# Turn off checks for displaced verticies. Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

evgenConfig.description = "pp->Jpsi(ee) + Z->mumu via second hard process"
evgenConfig.keywords = ["charmonium","Jpsi","2electron","Z","2muon","SM"]
evgenConfig.contact = ["Ketevi Adikle Assamagan <ketevi.adikle.assamagan@cern.ch>"]
evgenConfig.process = "pp -> Jpsi Z -> e+-e-+ mu+-mu-+"
evgenConfig.minevents = 5000

include('MC15JobOptions/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:1:onMode = on']
genSeq.Pythia8B.Commands += ['23:onMode = off']
genSeq.Pythia8B.Commands += ['23:7:onMode = on']
genSeq.Pythia8B.Commands += ['SecondHard:generate = on']
genSeq.Pythia8B.Commands += ['SecondHard:SingleGmZ = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-11,11]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [20.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.5
genSeq.Pythia8B.MinimumCountPerCut = [1]


evgenConfig.description = "Rotating string ball: n=6, gs=0.6, Ms=6.8TeV, Mth=7.3TeV"
evgenConfig.process = "SB2"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions"]
evgenConfig.generators += ["Charybdis2"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "Charybdis2"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")

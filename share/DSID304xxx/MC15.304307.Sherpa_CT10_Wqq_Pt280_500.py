include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa W+/W- -> qq + 1,2,3,4j@LO with 280 GeV < ptV < 500 GeV."
evgenConfig.keywords = ["SM", "W", "jets" ]
evgenConfig.contact  = [ "craig.sawyer@cern.ch", "atlas-generators-sherpa@cern.ch", "Thorsten.Kuhl@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Wqq_Pt280_500"

evgenConfig.process="""
(run){
  %INIT_ONLY 1
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  QCUT:=20.;
}(run)

(processes){
  Process 93 93 -> 24[a] 93 93{3}
  Decay 24[a] -> 94 94
  Selector_File *|(selwplus){|}(selwplus)
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  Max_N_Quarks 6;
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.05 {5,6,7,8}
  End process

  Process 93 93 -> -24[a] 93 93{3}
  Decay -24[a] -> 94 94
  Selector_File *|(selwminus){|}(selwminus)
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  Max_N_Quarks 6;
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.05 {5,6,7,8}
  End process
}(processes)

(selwminus){
  DecayMass -24 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) -24 280.0 500.0
}(selwminus)

(selwplus){
  DecayMass 24 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) 24 280.0 500.0
}(selwplus)
"""

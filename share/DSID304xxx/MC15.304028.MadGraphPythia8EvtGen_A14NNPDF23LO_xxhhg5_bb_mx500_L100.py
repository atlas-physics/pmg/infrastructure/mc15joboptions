mDM=500
Lambda=100

include("MC15JobOptions/MadGraphControl_monoHiggs_xxhhg5.py")

evgenConfig.description = "xxhhg5 EFT Model for MonoHiggs(h->bb) with Lambda=100GeV and mDM="+str(mDM)+"GeV"
evgenConfig.keywords = [ "Higgs", "BSMHiggs", "BSM"]
evgenConfig.contact = ['Nikola Whallon <alokin@uw.edu>']

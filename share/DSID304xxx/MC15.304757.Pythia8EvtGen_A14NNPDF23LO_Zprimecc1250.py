evgenConfig.description = "Pythia8 Z'(1250 GeV)->cc with A14 tune and NNPDF23LO PDF m=1250 GeV"
evgenConfig.keywords = ["exotic","Zprime","BSM"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Ning.Zhou@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += [
    "NewGaugeBoson:ffbar2gmZZprime = on", #create Z' bosons
    "PhaseSpace:mHatMin = 200.", #lower invariant mass
    "Zprime:gmZmode = 3",
    "32:m0 = 1250", #set Z' mass [GeV]
    "32:onMode = off", #switch off all Z' decays
    "32:onIfAny = 4"] #switch on Z'->cc decay

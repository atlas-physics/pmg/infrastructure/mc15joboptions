include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


evgenConfig.description = 'Production of VBF to higgs boson decaying to polarized WW with aL = 1 aT = 1 '
evgenConfig.contact = ['Lucrezia Stella Bruni  <lucrezia.stella.bruni@cern.ch>']
evgenConfig.keywords+=['VBFHiggs','WW']
evgenConfig.inputfilecheck = 'VBFHWWtautauvtvtPolarizedAL1AT1'

genSeq.Pythia8.Commands += ["SpaceShower:pTmaxFudge = 0.25"]
evgenConfig.minevents=1000


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("Multi1TLeptonFilter")
filtSeq += MultiLeptonFilter("Multi2LLeptonFilter")


Multi1TLeptonFilter = filtSeq.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 18000.
Multi1TLeptonFilter.Etacut = 3.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = filtSeq.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 8000.
Multi2LLeptonFilter.Etacut = 3.0
Multi2LLeptonFilter.NLeptons = 2
filtSeq.Expression = "(Multi1TLeptonFilter) and (Multi2LLeptonFilter)"



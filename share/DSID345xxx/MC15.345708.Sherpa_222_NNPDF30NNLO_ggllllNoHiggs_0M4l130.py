include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa+OpenLoops gg->llll + 0,1j, cf. arXiv:1309.0500, excluding the gg->h diagrams, with 0<m4l<130."
evgenConfig.keywords = ["SM", "diboson", "4lepton" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "ggllllNoHiggs_0M4l130"

Sherpa_iRunCard="""
(run){
  % scales, tags for scale variations
  SCALES STRICT_METS{MU_F2}{MU_R2}{MU_Q2};
  ## for mu_F=mu_R=mu_Q = mVV/2
  CORE_SCALE VAR{Abs2(p[2]+p[3]+p[4]+p[5])/4.0}

  % tags for process setup
  NJET:=1; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  INTEGRATION_ERROR=0.1
  EXCLUSIVE_CLUSTER_MODE 1;
  AMEGIC_ALLOW_MAPPING=0
  SHERPA_LDADD=Proc_fsrchannels4 Proc_fsrchannels5

  MASS[25]=1e10
}(run)

(processes){
  Process 93 93 -> 90 90 90 90 93{NJET}
  CKKW sqr(QCUT/E_CMS);
  Order (2,4) {4}
  Order (3,4) {5}
  Integrator fsrchannels4 {4}
  Integrator fsrchannels5 {5}
  Enable_MHV 10 # initialises external process
  Loop_Generator LOOPGEN;
  End process;
}(processes)

(selector){
  Mass 11 -11 10.0 E_CMS
  Mass 13 -13 10.0 E_CMS
  Mass 15 -15 10.0 E_CMS

  "m" 90,90,90,90 1.0,130.0
}(selector)

"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "OL_PARAMETERS=preset=3=write_parameters=1" ]

Sherpa_iOpenLoopsLibs = [ "ppllll2", "ppllllj2" ]
Sherpa_iNCores = 96
Sherpa_iExtraFiles = [ "libProc_fsrchannels4.so", "libProc_fsrchannels5.so" ]

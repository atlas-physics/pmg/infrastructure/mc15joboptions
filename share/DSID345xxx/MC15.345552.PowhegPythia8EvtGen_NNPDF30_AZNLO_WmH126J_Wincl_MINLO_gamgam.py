#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
 
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# H->gamgam decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22']


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Wm+jet->gamgam+all production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs" ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch', 'cyril.becot@cern.ch' ]
evgenConfig.process = "qq->WmH, H->GamGam, W->all"
evgenConfig.inputfilecheck = 'TXT'
evgenConfig.minevents   = 500

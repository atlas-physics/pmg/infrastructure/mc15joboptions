include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/nonStandard/Pythia8_Photospp.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22', # H -> gamma gamma
    "PartonLevel:MPI = on"
    ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "MadGraph5 LO showered with Pythia8"
evgenConfig.keywords   += ['Higgs', 'SMHiggs', 'ttHiggs', 'tHiggs']
evgenConfig.contact     = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'thjb'

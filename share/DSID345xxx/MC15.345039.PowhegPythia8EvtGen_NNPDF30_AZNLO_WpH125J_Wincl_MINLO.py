#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 W+H125+jet->muvmumu production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HWj_Common.py')
 
PowhegConfig.mass_H  = 125.
PowhegConfig.width_H = 0.00407
 
# Increase number of events requested to compensate for filter efficiency
 
PowhegConfig.runningscales = 1 #
PowhegConfig.idvecbos = 24
PowhegConfig.vdecaymode = 10 # all
PowhegConfig.mass_W_low = 10
 
PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

#NNPDF3.0 and PDF4LHC PDF variations
PowhegConfig.PDF = range(260000, 260101) + range(90400, 90433)
# scale variations: first pair is the nominal setting
PowhegConfig.mu_F =  [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] 
PowhegConfig.mu_R =  [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]



PowhegConfig.generate()
 
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
 
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13 15']

 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Wp+jet->4l+all production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch' ]
evgenConfig.process = "qq->WpH, H->4l, W->all"
evgenConfig.inputconfcheck = "WpH125J_Wincl"
evgenConfig.minevents   = 500

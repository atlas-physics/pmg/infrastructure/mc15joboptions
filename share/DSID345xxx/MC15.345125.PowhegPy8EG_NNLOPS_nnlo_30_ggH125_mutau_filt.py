
evgenConfig.process     = "gg->H->mutau"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN H+jet production with NNLOPS"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "1jet", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 1000
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31, "Power-shower + jet veto"
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:oneChannel = 1 0.5 100 15 -13',
	                     '25:addChannel = 1 0.5 100 13 -15' ]

# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lfvfilter = TauFilter("lfvfilter")
  filtSeq += lfvfilter

filtSeq.lfvfilter.UseNewOptions = True
filtSeq.lfvfilter.Ntaus = 1
filtSeq.lfvfilter.Nleptaus = 0
filtSeq.lfvfilter.Nhadtaus = 0
filtSeq.lfvfilter.EtaMaxlep = 2.6
filtSeq.lfvfilter.EtaMaxhad = 2.6
filtSeq.lfvfilter.Ptcutlep = 10000.0 #MeV
filtSeq.lfvfilter.Ptcutlep_lead = 10000.0 #MeV
filtSeq.lfvfilter.Ptcuthad = 20000.0 #MeV
filtSeq.lfvfilter.Ptcuthad_lead = 20000.0 #MeV

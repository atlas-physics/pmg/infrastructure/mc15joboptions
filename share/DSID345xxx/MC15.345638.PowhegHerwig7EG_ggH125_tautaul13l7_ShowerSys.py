include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3nnloME_LHEF_EvtGen_Common.py")
include("MC15JobOptions/Herwig7_701_StripWeights.py")
 
# To modify Higgs BR:
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL
 
set /Herwig/Particles/h0:NominalMass 125*GeV
set /Herwig/Particles/h0:Width 0.00407*GeV
set /Herwig/Particles/h0:WidthCut 0.00407*GeV
set /Herwig/Particles/h0:WidthLoCut 0.00407*GeV
set /Herwig/Particles/h0:WidthUpCut 0.00407*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 1



set /Herwig/Particles/tau+/tau+->nu_taubar,nu_e,e+;:OnOff 1
set /Herwig/Particles/tau+/tau+->nu_taubar,nu_mu,mu+;:OnOff 1
set /Herwig/Particles/tau-/tau-->nu_tau,nu_ebar,e-;:OnOff 1
set /Herwig/Particles/tau-/tau-->nu_tau,nu_mubar,mu-;:OnOff 1


set /Herwig/Particles/tau+/tau+->f_1,pi+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi0,nu_taubar,[omega->pi0,gamma;];:OnOff 0
set /Herwig/Particles/tau+/tau+->phi,K+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K0,pi+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi-,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K0,pi+,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,pi+,pi-,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi0,gamma,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,Kbar0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,Kbar0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,K-,pi+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->eta,pi+,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K_S0,pi+,K_L0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi0,pi0,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,pi+,pi+,pi-,pi-,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K0,Kbar0,pi+,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,pi0,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,omega,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,pi+,pi-,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K_S0,K_S0,pi+,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->pi+,K_L0,K_L0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,eta,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K*+,eta,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->eta,pi+,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->eta,pi+,pi+,pi-,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->omega,pi+,pi+,pi-,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->omega,pi+,pi0,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau+/tau+->K+,K-,pi+,pi0,nu_taubar;:OnOff 0
set /Herwig/Particles/tau-/tau-->f_1,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,nu_tau,[omega->pi0,gamma;];:OnOff 0
set /Herwig/Particles/tau-/tau-->phi,K-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->Kbar0,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi-,pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->Kbar0,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi+,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,gamma,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,K0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,K0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K+,K-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->eta,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K_S0,pi-,K_L0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,pi0,pi0,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi+,pi+,pi-,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K0,Kbar0,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi0,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,omega,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,pi+,pi-,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K_S0,K_S0,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->pi-,K_L0,K_L0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K-,eta,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K*-,eta,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->eta,pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->eta,pi+,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->omega,pi+,pi-,pi-,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->omega,pi-,pi0,pi0,nu_tau;:OnOff 0
set /Herwig/Particles/tau-/tau-->K+,K-,pi-,pi0,nu_tau;:OnOff 0

"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Herwig7"]
evgenConfig.description = "Powheg showered with Herwig7, mH=125 GeV, for systematics"
evgenConfig.process     = "ggF H->tautau->ll"
evgenConfig.keywords += ['Higgs', 'SMHiggs']
evgenConfig.contact  = ["Alessia Murrone <alessia.murrone@cern.ch>"]
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = "TXT"
 
# print checks
log.info('*** Begin Herwig7 commands ***')
log.info(cmds)
log.info('*** End Herwig7 commands ***')
 
# Set the command vector
genSeq.Herwig7.Commands += cmds.splitlines()


# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep13lep7filter = TauFilter("lep13lep7filter")
  filtSeq += lep13lep7filter

filtSeq.lep13lep7filter.UseNewOptions = True
filtSeq.lep13lep7filter.Ntaus = 2
filtSeq.lep13lep7filter.Nleptaus = 2
filtSeq.lep13lep7filter.Nhadtaus = 0
filtSeq.lep13lep7filter.EtaMaxlep = 2.6
filtSeq.lep13lep7filter.EtaMaxhad = 2.6
filtSeq.lep13lep7filter.Ptcutlep = 7000.0 #MeV
filtSeq.lep13lep7filter.Ptcutlep_lead = 13000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep13lep7filter.Ptcuthad_lead = 20000.0 #MeV
 
# clean up
del cmds

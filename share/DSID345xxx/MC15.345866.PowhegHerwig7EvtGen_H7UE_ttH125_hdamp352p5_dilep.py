#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig704 ttH production with Powheg hdamp=352.5=1.5*(m_top+m_antitop+m_H)/2, H7UE tune, ttbar di-lepton filter, ME NNPDF30 NLO, H7UE MMHT2014 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'Higgs']
evgenConfig.tune        = "MMHT2014"
evgenConfig.contact     = [ 'mamolla@cern.ch' ]
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio      0.5770
set /Herwig/Particles/h0/h0->c,cbar;:BranchingRatio      0.0291
set /Herwig/Particles/h0/h0->t,tbar;:BranchingRatio      0.00000
set /Herwig/Particles/h0/h0->mu-,mu+;:BranchingRatio     0.000219
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio   0.0632
set /Herwig/Particles/h0/h0->g,g;:BranchingRatio         0.0857
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio 0.00228
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio       0.0264
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio       0.2150
decaymode h0->Z0,gamma; 0.00154 1 /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff On
set /Herwig/Particles/h0/h0->Z0,gamma;:BranchingRatio    0.00154
decaymode h0->s,sbar; 0.000246 1 /Herwig/Decays/Hff
set /Herwig/Particles/h0/h0->s,sbar;:OnOff On
set /Herwig/Particles/h0/h0->s,sbar;:BranchingRatio      0.000246
""")

# run Herwig7
Herwig7Config.run()


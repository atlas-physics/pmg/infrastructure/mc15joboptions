#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 411.6 
PowhegConfig.width_H = 2.2 

# Turn on the heavy quark effect
PowhegConfig.use_massive_b = True
PowhegConfig.use_massive_c = True

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 1

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# Note: r2330 does not support this option. please use newer versions.
PowhegConfig.runningscale = 2

# EW correction
if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = 74.5

# Additonal setting for MSSM group
PowhegConfig.storeinfo_rwgt = 1
PowhegConfig.hdecaymode = 0

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 20.

#PowhegConfig.generateRunCard()
#PowhegConfig.generateEvents()

PowhegConfig.generate()

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys
infile = 'PowhegOTF._1.events'
f1 = open( infile )
newfile = infile+'.temp'
f2 = open(newfile,'w')
for line in f1:
    if line.startswith('      25     1'):
        f2.write(line.replace('      25     1','      35     1'))
    else:
        f2.write(line)
f1.close()
f2.close()
os.system('mv %s %s '%(infile, infile+'.old') )
os.system('mv %s %s '%(newfile, infile) )

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 1' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
#genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
#                             '25:onIfMatch = 15 15' ]

H_Mass = 411.6
H_Width = 2.2
A_Width = 3.8
A_Mass  = 400.0

genSeq.Pythia8.Commands += [ 
                             'Higgs:useBSM = on',

                             '35:m0 = '+str(H_Mass),
                             '35:mWidth = '+str(H_Width),
                             '35:doForceWidth = on',
                             '35:onMode = off',
                             '35:oneChannel = 1 0.1666   100 6 -6',
                             '35:addChannel = 1 0.1666   100 5 -5',
                             '35:addChannel = 1 0.1666   100 15 -15',
                             '35:addChannel = 1 0.1666   100 24 -24',
                             '35:addChannel = 1 0.1666   100 23 23',
                             '35:addChannel = 1 0.1666   100 25 25',

                             '36:m0 = '+str(A_Mass), 
                             '36:mWidth = '+str(A_Width), # narrow width
                             ]

include('MC15JobOptions/Pythia8_SMHiggs125_inc.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, MSSM ggH Higgs mass 411GeV width 2GeV H->inclusive"
evgenConfig.keywords    = [ "BSMHiggs"]
evgenConfig.contact     = [ 'ketevi.adikle.assamagan@cern.ch' ]

# ... Filter H->VV->Children
#include("MC15JobOptions/XtoVVDecayFilterExtended.py")
#filtSeq.XtoVVDecayFilterExtended.PDGGrandParent = 25
#filtSeq.XtoVVDecayFilterExtended.PDGParent = 15
#filtSeq.XtoVVDecayFilterExtended.StatusParent = 2
#filtSeq.XtoVVDecayFilterExtended.PDGChild1 = [111,130,211,221,223,310,311,321,323]
#filtSeq.XtoVVDecayFilterExtended.PDGChild2 = [111,130,211,221,223,310,311,321,323]


#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 H+W+jet->l+vbbar production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_HWj_Common.py')

PowhegConfig.runningscales = 1 # 
PowhegConfig.idvecbos = 24
PowhegConfig.vdecaymode = 11 # W->l+v
PowhegConfig.hdecaymode = -1

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
PowhegConfig.ptVlow = 120
PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
PowhegConfig.PDF = range(260000, 260101) + range( 90400, 90433 ) + [11068] + [25200] + [13165] + [25300] # PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.define_event_weight_group( group_name='kappas_var', parameters_to_vary=['kappa_ghw','kappa_ght', 'kappa_ghb'] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.0.5, kht.1, khb.1', parameter_values=[0.5,1.,1.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.1, kht.0, khb.1', parameter_values=[1.,0.,1.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.1, kht.1, khb.0', parameter_values=[1.,1.,0.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.1, kht.0, khb.0', parameter_values=[1.,0.,0.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.0.5, kht.1, khb.0', parameter_values=[0.5,1.,0.] )
PowhegConfig.add_weight_to_group( group_name='kappas_var', weight_name='khw.0.5, kht.0, khb.1', parameter_values=[0.5,0.,1.] )

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3']
#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 5' ]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vbbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch' ]
evgenConfig.minevents = 50
evgenConfig.process = "WpH, H->bb, W->lv"

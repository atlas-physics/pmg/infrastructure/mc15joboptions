
evgenConfig.process     = "gg->H -> tautau -> l-h+ unpolarized"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN H+jet production with NNLOPS"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "1jet", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 1000
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31, "Power-shower + jet veto"
#--------------------------------------------------------------
genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = off', # decay of taus
                             '15:onPosIfAny = 11 13', # particle
                             '15:onNegIfAny = 111 130 211 221 223 310 311 321 323', # antiparticle
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]

# Set tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  evtNumberFilter = TauFilter("evtNumberFilter")
  filtSeq += evtNumberFilter

filtSeq.evtNumberFilter.UseNewOptions = True
filtSeq.evtNumberFilter.Ntaus = 0
filtSeq.evtNumberFilter.Nleptaus = 0
filtSeq.evtNumberFilter.Nhadtaus = 0
filtSeq.evtNumberFilter.filterEventNumber = 1 # keep odd EventNumber events

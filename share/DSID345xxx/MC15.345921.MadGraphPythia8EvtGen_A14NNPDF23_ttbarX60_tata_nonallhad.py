from MadGraphControl.MadGraphUtils import *
from os import path

safefactor=1.1
mode=0


### DSID lists (extensions can include systematics samples)
test=[345921]

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:
    fcard.write("""
    import model 2HDM
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate g g > t t~ h1, (h1 > ta+ ta-) 
    output -f""")
    fcard.close()

else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'use_syst':"False"}

XMass  = { '25':'60',
           '35':'1.2500e+02',
           '36':'1.2500e+10',
           '37':'1.2500e+10',
} #MA0

runName='run_01'     


process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=runArgs.maxEvents/0.53*safefactor,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


#build_param_card(param_card_old='2HDM_param_card.dat',param_card_new='param_card.dat',masses=XMass,)
build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),param_card_new='param_card_new.dat',masses=XMass,)    
print_cards()

#generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir,run_name=runName)
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)  


#### Shower 
evgenConfig.description = 'MG5 ttbarX to tautau, mH = 60 GeV'
evgenConfig.keywords+=['ttbar','jets','tau']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
evgenConfig.contact = ['Zirui Wang <zirui.wang@cern.ch>']

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

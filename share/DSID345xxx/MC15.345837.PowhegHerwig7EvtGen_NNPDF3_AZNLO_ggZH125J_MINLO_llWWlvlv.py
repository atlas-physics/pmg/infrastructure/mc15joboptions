# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3nnloME_LHEF_EvtGen_Common.py")
#include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_NNPDF3ME_LHEF_EvtGen_Common.py")
#include("changeWgt.py")
#include("MC15JobOptions/Herwig7_701_StripWeights.py")

# only consider H->WW->lvlv decays
Herwig7Config.add_commands("""
# force H->WW decays
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W+:PrintDecayModes
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
do /Herwig/Particles/W-:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Powheg+Herwig7 H+Z+jet->lllvlv'
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.process     = "gg->ZH, H->WW, Z->ll"
evgenConfig.contact     = [ 'ada.farilla@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = "TXT"

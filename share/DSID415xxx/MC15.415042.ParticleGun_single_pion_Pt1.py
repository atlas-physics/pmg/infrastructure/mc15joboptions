evgenConfig.description = "Single pion with flat phi, eta in [-5.0, 5.0], and pT 1 GeV"
evgenConfig.keywords = ["singleParticle", "pi+","pi-"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (211, -211)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=1000, eta=[-5.0, 5.0])

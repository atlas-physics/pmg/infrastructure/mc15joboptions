#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16871827E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     9.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04050111E+01   # W+
        25     1.24757773E+02   # h
        35     3.00008902E+03   # H
        36     2.99999996E+03   # A
        37     3.00089911E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04198356E+03   # ~d_L
   2000001     3.03688290E+03   # ~d_R
   1000002     3.04108175E+03   # ~u_L
   2000002     3.03780100E+03   # ~u_R
   1000003     3.04198356E+03   # ~s_L
   2000003     3.03688290E+03   # ~s_R
   1000004     3.04108175E+03   # ~c_L
   2000004     3.03780100E+03   # ~c_R
   1000005     1.05565629E+03   # ~b_1
   2000005     3.03577205E+03   # ~b_2
   1000006     1.05191792E+03   # ~t_1
   2000006     3.01844178E+03   # ~t_2
   1000011     3.00623292E+03   # ~e_L
   2000011     3.00223821E+03   # ~e_R
   1000012     3.00485034E+03   # ~nu_eL
   1000013     3.00623292E+03   # ~mu_L
   2000013     3.00223821E+03   # ~mu_R
   1000014     3.00485034E+03   # ~nu_muL
   1000015     2.98546884E+03   # ~tau_1
   2000015     3.02200886E+03   # ~tau_2
   1000016     3.00454859E+03   # ~nu_tauL
   1000021     2.35219340E+03   # ~g
   1000022     4.01629520E+02   # ~chi_10
   1000023     8.35376021E+02   # ~chi_20
   1000025    -2.99452043E+03   # ~chi_30
   1000035     2.99557633E+03   # ~chi_40
   1000024     8.35538931E+02   # ~chi_1+
   1000037     2.99603884E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99881897E-01   # N_11
  1  2    -6.79601403E-04   # N_12
  1  3     1.51052947E-02   # N_13
  1  4    -2.74970127E-03   # N_14
  2  1     1.12927201E-03   # N_21
  2  2     9.99563557E-01   # N_22
  2  3    -2.81784011E-02   # N_23
  2  4     8.79760894E-03   # N_24
  3  1    -8.72501619E-03   # N_31
  3  2     1.37136285E-02   # N_32
  3  3     7.06886557E-01   # N_33
  3  4     7.07140160E-01   # N_34
  4  1    -1.26012491E-02   # N_41
  4  2     2.61566436E-02   # N_42
  4  3     7.06603994E-01   # N_43
  4  4    -7.07013320E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99205195E-01   # U_11
  1  2    -3.98620004E-02   # U_12
  2  1     3.98620004E-02   # U_21
  2  2     9.99205195E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99922537E-01   # V_11
  1  2    -1.24466487E-02   # V_12
  2  1     1.24466487E-02   # V_21
  2  2     9.99922537E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98447774E-01   # cos(theta_t)
  1  2    -5.56959836E-02   # sin(theta_t)
  2  1     5.56959836E-02   # -sin(theta_t)
  2  2     9.98447774E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99904491E-01   # cos(theta_b)
  1  2     1.38205962E-02   # sin(theta_b)
  2  1    -1.38205962E-02   # -sin(theta_b)
  2  2     9.99904491E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06969552E-01   # cos(theta_tau)
  1  2     7.07243984E-01   # sin(theta_tau)
  2  1    -7.07243984E-01   # -sin(theta_tau)
  2  2     7.06969552E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01769173E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.68718272E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43950649E+02   # higgs               
         4     7.65240351E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.68718272E+03  # The gauge couplings
     1     3.62568155E-01   # gprime(Q) DRbar
     2     6.36896304E-01   # g(Q) DRbar
     3     1.02370259E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.68718272E+03  # The trilinear couplings
  1  1     2.72275955E-06   # A_u(Q) DRbar
  2  2     2.72279530E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.68718272E+03  # The trilinear couplings
  1  1     7.58014323E-07   # A_d(Q) DRbar
  2  2     7.58138573E-07   # A_s(Q) DRbar
  3  3     1.61965865E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.68718272E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.50435220E-07   # A_mu(Q) DRbar
  3  3     1.52186998E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.68718272E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50372304E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.68718272E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16521854E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.68718272E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07929593E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.68718272E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.63493470E+04   # M^2_Hd              
        22    -9.03025281E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     9.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40782365E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.11537727E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47652182E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47652182E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52347818E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52347818E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.63541631E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     6.36004949E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.05589973E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.30809532E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10129675E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.82301714E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.36182739E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.77227263E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.19501429E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.01598174E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.68393113E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59242040E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.11131142E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.44576933E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.02172324E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.26828329E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.92954438E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.18187001E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.88162157E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.60754273E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.60910576E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.50826851E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.19203237E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30686990E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.87977832E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.17619981E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.98857414E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56520063E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.15226560E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52761457E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.73338416E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.01558777E-07    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.05592208E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.46200421E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.35493886E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.13940366E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57060878E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.77828479E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.13602492E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.25872103E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42938910E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.57051949E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.23802152E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52567929E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.60407986E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.52065554E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.05028786E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.81559650E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.36164483E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64437724E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45839499E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.04752754E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.09523449E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.14763656E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55415990E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56520063E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.15226560E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52761457E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.73338416E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.01558777E-07    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.05592208E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.46200421E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.35493886E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.13940366E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57060878E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.77828479E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.13602492E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.25872103E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42938910E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.57051949E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.23802152E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52567929E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.60407986E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.52065554E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.05028786E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.81559650E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.36164483E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64437724E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45839499E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.04752754E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.09523449E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.14763656E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55415990E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47390808E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08868019E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97453704E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61566280E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.30210995E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93678218E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.08659270E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51424895E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998869E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12561292E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.08140761E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     3.23603789E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47390808E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08868019E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97453704E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61566280E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.30210995E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93678218E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.08659270E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51424895E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998869E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12561292E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.08140761E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     3.23603789E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51853034E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75336266E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08521287E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16142448E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46688435E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84156930E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05577529E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.70578916E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.87687605E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10210320E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.93940039E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47393768E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09334295E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96504807E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.73760670E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.47272708E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94160874E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.70862403E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47393768E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09334295E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96504807E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.73760670E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.47272708E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94160874E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.70862403E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47381884E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09326257E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96475197E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.40856708E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.37877332E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94196162E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.36439127E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.72399684E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.85188780E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.82972984E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.99874779E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.13312426E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.50648686E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.36961829E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.30708339E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.99957234E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.46893249E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.86421240E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.41357876E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     7.86851159E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.65280723E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.86414726E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.27880642E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.27880642E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.22952049E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.64454442E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.54545716E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.54545716E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27437434E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27437434E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.21522208E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.21522208E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.78782317E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.00841223E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.63325428E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.35994950E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.35994950E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.74392447E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.39599722E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.50476746E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.50476746E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30532824E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30532824E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.90830647E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.90830647E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.74561086E-03   # h decays
#          BR         NDA      ID1       ID2
     6.72728147E-01    2           5        -5   # BR(h -> b       bb     )
     5.48882512E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94306949E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.12539846E-04    2           3        -3   # BR(h -> s       sb     )
     1.78113169E-02    2           4        -4   # BR(h -> c       cb     )
     5.76638878E-02    2          21        21   # BR(h -> g       g      )
     1.96742838E-03    2          22        22   # BR(h -> gam     gam    )
     1.28865154E-03    2          22        23   # BR(h -> Z       gam    )
     1.71650578E-01    2          24       -24   # BR(h -> W+      W-     )
     2.13948920E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40763298E+01   # H decays
#          BR         NDA      ID1       ID2
     7.40407202E-01    2           5        -5   # BR(H -> b       bb     )
     1.76660385E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24628907E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.67045891E-04    2           3        -3   # BR(H -> s       sb     )
     2.16400828E-07    2           4        -4   # BR(H -> c       cb     )
     2.15873695E-02    2           6        -6   # BR(H -> t       tb     )
     2.59858970E-05    2          21        21   # BR(H -> g       g      )
     2.99158010E-08    2          22        22   # BR(H -> gam     gam    )
     8.33716908E-09    2          23        22   # BR(H -> Z       gam    )
     2.99308385E-05    2          24       -24   # BR(H -> W+      W-     )
     1.49469734E-05    2          23        23   # BR(H -> Z       Z      )
     7.88753701E-05    2          25        25   # BR(H -> h       h      )
     1.01177150E-23    2          36        36   # BR(H -> A       A      )
     4.90924609E-19    2          23        36   # BR(H -> Z       A      )
     1.59084361E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.03685702E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.93616640E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.99624453E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.67101859E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.41841617E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32986306E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83786926E-01    2           5        -5   # BR(A -> b       bb     )
     1.86990229E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61151839E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12031748E-04    2           3        -3   # BR(A -> s       sb     )
     2.29143849E-07    2           4        -4   # BR(A -> c       cb     )
     2.28459890E-02    2           6        -6   # BR(A -> t       tb     )
     6.72792507E-05    2          21        21   # BR(A -> g       g      )
     6.45312204E-08    2          22        22   # BR(A -> gam     gam    )
     6.63206448E-08    2          23        22   # BR(A -> Z       gam    )
     3.15623042E-05    2          23        25   # BR(A -> Z       h      )
     2.59883837E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22616686E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29629269E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.86723265E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.04107488E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10537244E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.38931834E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.44804687E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07437260E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.96474227E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02140713E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.18505886E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.03789910E-05    2          24        25   # BR(H+ -> W+      h      )
     7.42803203E-14    2          24        36   # BR(H+ -> W+      A      )
     1.81280062E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.15407447E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.76807924E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

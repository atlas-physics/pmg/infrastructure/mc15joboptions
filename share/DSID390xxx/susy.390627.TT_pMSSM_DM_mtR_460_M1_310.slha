#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90329842E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.05959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.22900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.80485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04211558E+01   # W+
        25     1.24569799E+02   # h
        35     4.00000554E+03   # H
        36     3.99999555E+03   # A
        37     4.00098906E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01278200E+03   # ~d_L
   2000001     4.01223770E+03   # ~d_R
   1000002     4.01212743E+03   # ~u_L
   2000002     4.01286257E+03   # ~u_R
   1000003     4.01278200E+03   # ~s_L
   2000003     4.01223770E+03   # ~s_R
   1000004     4.01212743E+03   # ~c_L
   2000004     4.01286257E+03   # ~c_R
   1000005     1.82887795E+03   # ~b_1
   2000005     4.01862103E+03   # ~b_2
   1000006     4.51312202E+02   # ~t_1
   2000006     1.84280432E+03   # ~t_2
   1000011     4.00167459E+03   # ~e_L
   2000011     4.00351418E+03   # ~e_R
   1000012     4.00056173E+03   # ~nu_eL
   1000013     4.00167459E+03   # ~mu_L
   2000013     4.00351418E+03   # ~mu_R
   1000014     4.00056173E+03   # ~nu_muL
   1000015     4.00381062E+03   # ~tau_1
   2000015     4.00910933E+03   # ~tau_2
   1000016     4.00314709E+03   # ~nu_tauL
   1000021     1.96368398E+03   # ~g
   1000022     2.91376778E+02   # ~chi_10
   1000023    -3.33002062E+02   # ~chi_20
   1000025     3.54568002E+02   # ~chi_30
   1000035     2.06406420E+03   # ~chi_40
   1000024     3.30407572E+02   # ~chi_1+
   1000037     2.06422774E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.60808388E-01   # N_11
  1  2     1.82575299E-02   # N_12
  1  3     4.87831584E-01   # N_13
  1  4     4.27618528E-01   # N_14
  2  1    -5.15766608E-02   # N_21
  2  2     2.48196399E-02   # N_22
  2  3    -7.02867211E-01   # N_23
  2  4     7.09014469E-01   # N_24
  3  1     6.46923027E-01   # N_31
  3  2     2.50273844E-02   # N_32
  3  3     5.17666383E-01   # N_33
  3  4     5.59361907E-01   # N_34
  4  1    -1.02099997E-03   # N_41
  4  2     9.99211828E-01   # N_42
  4  3    -4.42100872E-03   # N_43
  4  4    -3.94351891E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.25774701E-03   # U_11
  1  2     9.99980420E-01   # U_12
  2  1    -9.99980420E-01   # U_21
  2  2     6.25774701E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.57809705E-02   # V_11
  1  2    -9.98443030E-01   # V_12
  2  1    -9.98443030E-01   # V_21
  2  2    -5.57809705E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.01365612E-01   # cos(theta_t)
  1  2     9.94849241E-01   # sin(theta_t)
  2  1    -9.94849241E-01   # -sin(theta_t)
  2  2    -1.01365612E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999206E-01   # cos(theta_b)
  1  2    -1.26015847E-03   # sin(theta_b)
  2  1     1.26015847E-03   # -sin(theta_b)
  2  2     9.99999206E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05633762E-01   # cos(theta_tau)
  1  2     7.08576738E-01   # sin(theta_tau)
  2  1    -7.08576738E-01   # -sin(theta_tau)
  2  2    -7.05633762E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00245160E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.03298424E+02  # DRbar Higgs Parameters
         1    -3.22900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44620227E+02   # higgs               
         4     1.62595243E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.03298424E+02  # The gauge couplings
     1     3.60855919E-01   # gprime(Q) DRbar
     2     6.35073817E-01   # g(Q) DRbar
     3     1.03766187E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.03298424E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.72178800E-07   # A_c(Q) DRbar
  3  3     2.05959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.03298424E+02  # The trilinear couplings
  1  1     2.11404441E-07   # A_d(Q) DRbar
  2  2     2.11424794E-07   # A_s(Q) DRbar
  3  3     3.76451008E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.03298424E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.75874032E-08   # A_mu(Q) DRbar
  3  3     4.80712862E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.03298424E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74137997E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.03298424E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87843688E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.03298424E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03041854E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.03298424E+02  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57618049E+07   # M^2_Hd              
        22    -9.78842938E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.80485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.39907444E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.71392476E+01   # gluino decays
#          BR         NDA      ID1       ID2
     9.74856802E-03    2     1000005        -5   # BR(~g -> ~b_1  bb)
     9.74856802E-03    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.90251432E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     4.90251432E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.46011055E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.72318227E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     9.13601217E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.20030319E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.25707870E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.29216830E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.88801272E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.21178735E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     4.10597469E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.93054898E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.05994332E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.78267127E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.94470025E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.37798339E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.62947081E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.66163555E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82715712E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.55306122E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.16793887E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68051103E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.61616972E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12322037E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.44310718E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.31345179E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.46255950E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.21642179E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74066757E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.39606950E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.99080390E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.91009006E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83372031E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.47014881E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65521389E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51237576E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57024952E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.24736005E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.48778779E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.33650609E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.21383619E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44012528E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74084842E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.32394633E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.05147410E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.34050642E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83888431E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.88480920E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68781537E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51462974E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.50378869E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.47582621E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.88322158E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.09842738E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.38650107E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85386906E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74066757E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.39606950E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.99080390E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.91009006E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83372031E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.47014881E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65521389E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51237576E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57024952E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.24736005E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.48778779E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.33650609E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.21383619E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44012528E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74084842E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.32394633E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.05147410E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.34050642E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83888431E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.88480920E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68781537E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51462974E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.50378869E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.47582621E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.88322158E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.09842738E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.38650107E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85386906E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10788248E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.76188394E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.02555878E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.83819428E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77504269E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.99070827E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56444786E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04796067E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80076186E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.65719731E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.17266048E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.69143130E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10788248E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.76188394E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.02555878E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.83819428E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77504269E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.99070827E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56444786E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04796067E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80076186E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.65719731E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.17266048E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.69143130E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06918228E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.15200957E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.41418902E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66249442E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39537111E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.50733115E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79797287E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06890346E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11362248E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.19883861E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.48415031E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42307842E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05761626E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85350330E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10899334E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03688287E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.49150090E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.96316307E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77860662E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.16888731E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54159032E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10899334E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03688287E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.49150090E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.96316307E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77860662E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.16888731E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54159032E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.43729981E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.38466187E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.34993947E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.39720004E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51719455E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.70878898E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02024096E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.38568265E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33709287E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33709287E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11237782E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11237782E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10105862E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.91843545E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.13070577E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.91424302E-02    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     5.63820232E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.12480797E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.98691211E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.07564496E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.36552176E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.06701899E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.80314756E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.87077839E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18820290E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54099991E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18820290E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54099991E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.36779294E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53202418E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53202418E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48854597E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06067998E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06067998E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06067972E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     9.46473595E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     9.46473595E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     9.46473595E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     9.46473595E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.15491514E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.15491514E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.15491514E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.15491514E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.43352733E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.43352733E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.45558024E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.60830486E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.25495912E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.95105125E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.64529083E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.95105125E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.64529083E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.29943109E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.69614402E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.69614402E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.69511467E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.53139410E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.53139410E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.53136360E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.03221457E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.63570395E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.03221457E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.63570395E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.67482199E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.04166496E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.04166496E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.81127767E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.20757515E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.20757515E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.20757518E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.23189228E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.23189228E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.23189228E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.23189228E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.10624839E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.10624839E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.10624839E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.10624839E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.99791943E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.99791943E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.91689670E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.98587612E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.30292904E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.24134717E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.07347552E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.07347552E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.05818508E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.05070738E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.52624630E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.04236766E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.04236766E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31199175E-02    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     2.31199175E-02    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     3.96505919E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     3.96505919E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.91102449E-03   # h decays
#          BR         NDA      ID1       ID2
     6.09901561E-01    2           5        -5   # BR(h -> b       bb     )
     6.60974315E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.33988824E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.96975126E-04    2           3        -3   # BR(h -> s       sb     )
     2.15862554E-02    2           4        -4   # BR(h -> c       cb     )
     6.88692318E-02    2          21        21   # BR(h -> g       g      )
     2.37748842E-03    2          22        22   # BR(h -> gam     gam    )
     1.53822875E-03    2          22        23   # BR(h -> Z       gam    )
     2.03545857E-01    2          24       -24   # BR(h -> W+      W-     )
     2.53529820E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.51866398E+01   # H decays
#          BR         NDA      ID1       ID2
     3.57290940E-01    2           5        -5   # BR(H -> b       bb     )
     6.00797539E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12427458E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.51265068E-04    2           3        -3   # BR(H -> s       sb     )
     7.04686705E-08    2           4        -4   # BR(H -> c       cb     )
     7.06242793E-03    2           6        -6   # BR(H -> t       tb     )
     4.33893948E-07    2          21        21   # BR(H -> g       g      )
     1.86903316E-09    2          22        22   # BR(H -> gam     gam    )
     1.81940656E-09    2          23        22   # BR(H -> Z       gam    )
     1.65978866E-06    2          24       -24   # BR(H -> W+      W-     )
     8.29319567E-07    2          23        23   # BR(H -> Z       Z      )
     6.35473758E-06    2          25        25   # BR(H -> h       h      )
    -2.34355652E-24    2          36        36   # BR(H -> A       A      )
    -1.61990917E-20    2          23        36   # BR(H -> Z       A      )
     1.85274262E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.59037750E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.59037750E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.78256241E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.19239870E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.79215617E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.99999779E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.38464897E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.72494545E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.77978110E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.02144488E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.20387072E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.70266826E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.82180346E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     4.74778600E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.74778600E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     6.08206438E-08    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.51840735E+01   # A decays
#          BR         NDA      ID1       ID2
     3.57333314E-01    2           5        -5   # BR(A -> b       bb     )
     6.00828429E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12438213E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.51318774E-04    2           3        -3   # BR(A -> s       sb     )
     7.09337202E-08    2           4        -4   # BR(A -> c       cb     )
     7.07691150E-03    2           6        -6   # BR(A -> t       tb     )
     1.45412682E-05    2          21        21   # BR(A -> g       g      )
     5.76913633E-08    2          22        22   # BR(A -> gam     gam    )
     1.59957801E-08    2          23        22   # BR(A -> Z       gam    )
     1.65645473E-06    2          23        25   # BR(A -> Z       h      )
     1.86259709E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.59042674E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.59042674E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.38533275E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     9.05439882E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.49025681E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.47469670E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.03475254E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.86880693E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.02111002E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.73714404E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.34360534E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     4.96458398E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     4.96458398E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.51652737E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.71378360E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.01182468E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12563392E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65681839E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20421285E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.47745307E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63818124E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.65883412E-06    2          24        25   # BR(H+ -> W+      h      )
     2.30900873E-14    2          24        36   # BR(H+ -> W+      A      )
     4.62819880E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.09003535E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.00046826E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.59301565E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.60835799E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57706806E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.56552595E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     9.95102666E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.35869425E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

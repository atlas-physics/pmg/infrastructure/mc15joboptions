#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13069009E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.24900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     8.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04179889E+01   # W+
        25     1.25831519E+02   # h
        35     4.00000089E+03   # H
        36     3.99999651E+03   # A
        37     4.00106401E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02609623E+03   # ~d_L
   2000001     4.02273730E+03   # ~d_R
   1000002     4.02544617E+03   # ~u_L
   2000002     4.02334779E+03   # ~u_R
   1000003     4.02609623E+03   # ~s_L
   2000003     4.02273730E+03   # ~s_R
   1000004     4.02544617E+03   # ~c_L
   2000004     4.02334779E+03   # ~c_R
   1000005     2.03533385E+03   # ~b_1
   2000005     4.02559068E+03   # ~b_2
   1000006     8.77019816E+02   # ~t_1
   2000006     2.04980528E+03   # ~t_2
   1000011     4.00452420E+03   # ~e_L
   2000011     4.00365445E+03   # ~e_R
   1000012     4.00341166E+03   # ~nu_eL
   1000013     4.00452420E+03   # ~mu_L
   2000013     4.00365445E+03   # ~mu_R
   1000014     4.00341166E+03   # ~nu_muL
   1000015     4.00561380E+03   # ~tau_1
   2000015     4.00686382E+03   # ~tau_2
   1000016     4.00484630E+03   # ~nu_tauL
   1000021     1.99058772E+03   # ~g
   1000022     9.14387989E+01   # ~chi_10
   1000023    -1.35406838E+02   # ~chi_20
   1000025     1.52925506E+02   # ~chi_30
   1000035     2.06391111E+03   # ~chi_40
   1000024     1.29995073E+02   # ~chi_1+
   1000037     2.06409116E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.50997594E-01   # N_11
  1  2     1.41228880E-02   # N_12
  1  3     5.42902890E-01   # N_13
  1  4     3.75579032E-01   # N_14
  2  1    -1.36969618E-01   # N_21
  2  2     2.72302180E-02   # N_22
  2  3    -6.84901974E-01   # N_23
  2  4     7.15127349E-01   # N_24
  3  1     6.45942047E-01   # N_31
  3  2     2.35854060E-02   # N_32
  3  3     4.85968629E-01   # N_33
  3  4     5.88249175E-01   # N_34
  4  1    -8.99511580E-04   # N_41
  4  2     9.99251114E-01   # N_42
  4  3    -4.79453124E-04   # N_43
  4  4    -3.86803971E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.82318765E-04   # U_11
  1  2     9.99999767E-01   # U_12
  2  1    -9.99999767E-01   # U_21
  2  2     6.82318765E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47155445E-02   # V_11
  1  2    -9.98501983E-01   # V_12
  2  1    -9.98501983E-01   # V_21
  2  2    -5.47155445E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.17488617E-01   # cos(theta_t)
  1  2     9.93074229E-01   # sin(theta_t)
  2  1    -9.93074229E-01   # -sin(theta_t)
  2  2    -1.17488617E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999874E-01   # cos(theta_b)
  1  2    -5.01996000E-04   # sin(theta_b)
  2  1     5.01996000E-04   # -sin(theta_b)
  2  2     9.99999874E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03576894E-01   # cos(theta_tau)
  1  2     7.10619134E-01   # sin(theta_tau)
  2  1    -7.10619134E-01   # -sin(theta_tau)
  2  2    -7.03576894E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00325011E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30690088E+03  # DRbar Higgs Parameters
         1    -1.24900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43510370E+02   # higgs               
         4     1.60721284E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30690088E+03  # The gauge couplings
     1     3.62236080E-01   # gprime(Q) DRbar
     2     6.36785061E-01   # g(Q) DRbar
     3     1.02938481E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30690088E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37331569E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30690088E+03  # The trilinear couplings
  1  1     5.02893664E-07   # A_d(Q) DRbar
  2  2     5.02940811E-07   # A_s(Q) DRbar
  3  3     9.00586528E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30690088E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.07901594E-07   # A_mu(Q) DRbar
  3  3     1.09016070E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30690088E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67825391E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30690088E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80151049E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30690088E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04271129E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30690088E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58739951E+07   # M^2_Hd              
        22     1.48769238E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     8.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40676313E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.78291883E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.57494271E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.34066161E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.26607968E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.05109635E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.74875781E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.55522547E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.37343530E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.30503201E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.44244931E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.41358717E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.92537831E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.04843811E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.99355631E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.47429843E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.55759331E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.50307310E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.56975897E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.07444075E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -7.30699529E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64022866E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.83830253E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.72214609E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42481959E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.57733189E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60652524E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.98671136E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14030538E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.19815818E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.43044030E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.31459915E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.44143010E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76180470E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.45907282E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.40271493E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.88875403E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.84340985E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.30431386E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.67440840E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.50943419E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58715962E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.19170705E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.06057021E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.35742566E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.50395688E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43448078E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76199313E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.17248641E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.50721628E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.65876512E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84809677E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.24912185E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.70621515E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51167714E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51931664E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.33427847E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.76939098E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.15577567E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.53701148E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85233000E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76180470E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.45907282E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.40271493E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.88875403E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.84340985E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.30431386E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.67440840E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.50943419E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58715962E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.19170705E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.06057021E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.35742566E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.50395688E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43448078E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76199313E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.17248641E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.50721628E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.65876512E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84809677E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.24912185E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.70621515E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51167714E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51931664E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.33427847E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.76939098E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.15577567E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.53701148E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85233000E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13659612E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.77809307E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31991473E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.85112225E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77019454E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.78483583E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55368000E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08639619E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.64452321E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.87524382E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.16794804E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.37026749E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13659612E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.77809307E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31991473E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.85112225E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77019454E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.78483583E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55368000E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08639619E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.64452321E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.87524382E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.16794804E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.37026749E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10785950E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.23260795E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.01489944E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66495676E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38429613E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41423876E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77522534E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11508396E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.07065377E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.37801215E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.42605364E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41117291E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.25193270E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82912520E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13767956E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00133430E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.67669224E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.06738596E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77302960E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.07498281E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53138075E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13767956E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00133430E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.67669224E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.06738596E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77302960E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.07498281E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53138075E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47191072E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.05263608E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.13205975E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.48527207E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50827613E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.83341405E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00327106E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.41858956E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33721931E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33721931E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11241417E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11241417E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10073302E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.70412635E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.08384816E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.84588990E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44340193E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.15339992E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.41403313E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.72812938E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38686486E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.63383491E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.38233717E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18476657E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53676369E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18476657E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53676369E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37475484E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52347785E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52347785E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48023354E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04510585E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04510585E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04510559E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.68245231E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.68245231E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.68245231E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.68245231E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.22748716E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.22748716E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.22748716E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.22748716E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.33183202E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.33183202E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.35153588E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.90581681E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.17076226E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.58951488E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.64244182E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.58951488E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.64244182E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.37308597E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.05375042E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.05375042E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.04437764E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.13352027E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.13352027E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.13351709E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.99409353E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.07289138E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.99409353E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.07289138E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.37712284E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.08081672E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.08081672E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.96140926E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.15888415E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.15888415E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.15888426E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.20051113E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.20051113E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.20051113E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.20051113E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.06678407E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.06678407E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.06678407E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.06678407E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.97686293E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.97686293E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.70455236E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.35234100E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.27951331E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.26748574E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38063536E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38063536E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.88837312E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.16645252E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.24186302E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.00947548E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.00947548E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.93102745E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.93102745E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.10130226E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91930964E-01    2           5        -5   # BR(h -> b       bb     )
     6.36911552E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25464986E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77930435E-04    2           3        -3   # BR(h -> s       sb     )
     2.07523976E-02    2           4        -4   # BR(h -> c       cb     )
     6.78654115E-02    2          21        21   # BR(h -> g       g      )
     2.36016774E-03    2          22        22   # BR(h -> gam     gam    )
     1.64108243E-03    2          22        23   # BR(h -> Z       gam    )
     2.22764118E-01    2          24       -24   # BR(h -> W+      W-     )
     2.82913079E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.40693021E+01   # H decays
#          BR         NDA      ID1       ID2
     3.37743277E-01    2           5        -5   # BR(H -> b       bb     )
     6.13211768E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16816828E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56456974E-04    2           3        -3   # BR(H -> s       sb     )
     7.19477714E-08    2           4        -4   # BR(H -> c       cb     )
     7.21066453E-03    2           6        -6   # BR(H -> t       tb     )
     1.15539018E-06    2          21        21   # BR(H -> g       g      )
     3.75785739E-10    2          22        22   # BR(H -> gam     gam    )
     1.84976171E-09    2          23        22   # BR(H -> Z       gam    )
     2.12796587E-06    2          24       -24   # BR(H -> W+      W-     )
     1.06324584E-06    2          23        23   # BR(H -> Z       Z      )
     7.71727592E-06    2          25        25   # BR(H -> h       h      )
    -2.12356144E-24    2          36        36   # BR(H -> A       A      )
     4.72922982E-20    2          23        36   # BR(H -> Z       A      )
     1.86587157E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67106973E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67106973E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.40286958E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.64478628E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.70734821E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.42284071E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.49315415E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.11863656E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.12146013E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.41389790E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.22625738E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.49356237E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.75519609E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.75519609E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.40676798E+01   # A decays
#          BR         NDA      ID1       ID2
     3.37779530E-01    2           5        -5   # BR(A -> b       bb     )
     6.13234528E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16824705E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56508077E-04    2           3        -3   # BR(A -> s       sb     )
     7.23983804E-08    2           4        -4   # BR(A -> c       cb     )
     7.22303764E-03    2           6        -6   # BR(A -> t       tb     )
     1.48415161E-05    2          21        21   # BR(A -> g       g      )
     4.06695086E-08    2          22        22   # BR(A -> gam     gam    )
     1.63170639E-08    2          23        22   # BR(A -> Z       gam    )
     2.12353892E-06    2          23        25   # BR(A -> Z       h      )
     1.86919163E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67114024E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67114024E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.96902528E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.27527155E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34754191E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.89923488E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.39384537E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.78105499E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.39680484E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.29770704E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.67510109E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.47812440E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.47812440E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.38963086E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.37106152E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.15348574E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17572180E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43747639E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23258570E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53582516E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42577556E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.13274323E-06    2          24        25   # BR(H+ -> W+      h      )
     3.38457346E-14    2          24        36   # BR(H+ -> W+      A      )
     4.72613298E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.51282319E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.25184422E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67867423E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.94190490E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57787111E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     7.93517637E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.02884154E-06    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

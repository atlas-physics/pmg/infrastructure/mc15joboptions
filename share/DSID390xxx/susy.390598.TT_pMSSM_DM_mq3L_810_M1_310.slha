#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13088521E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.46900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04168832E+01   # W+
        25     1.25665994E+02   # h
        35     4.00000954E+03   # H
        36     3.99999708E+03   # A
        37     4.00103611E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02611400E+03   # ~d_L
   2000001     4.02268516E+03   # ~d_R
   1000002     4.02546053E+03   # ~u_L
   2000002     4.02363272E+03   # ~u_R
   1000003     4.02611400E+03   # ~s_L
   2000003     4.02268516E+03   # ~s_R
   1000004     4.02546053E+03   # ~c_L
   2000004     4.02363272E+03   # ~c_R
   1000005     8.79466787E+02   # ~b_1
   2000005     4.02550375E+03   # ~b_2
   1000006     8.65464894E+02   # ~t_1
   2000006     2.14845447E+03   # ~t_2
   1000011     4.00469629E+03   # ~e_L
   2000011     4.00325268E+03   # ~e_R
   1000012     4.00358142E+03   # ~nu_eL
   1000013     4.00469629E+03   # ~mu_L
   2000013     4.00325268E+03   # ~mu_R
   1000014     4.00358142E+03   # ~nu_muL
   1000015     4.00456563E+03   # ~tau_1
   2000015     4.00762803E+03   # ~tau_2
   1000016     4.00499665E+03   # ~nu_tauL
   1000021     1.98060381E+03   # ~g
   1000022     2.99624818E+02   # ~chi_10
   1000023    -3.57990472E+02   # ~chi_20
   1000025     3.69689690E+02   # ~chi_30
   1000035     2.05222328E+03   # ~chi_40
   1000024     3.55470236E+02   # ~chi_1+
   1000037     2.05238806E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.63808446E-01   # N_11
  1  2    -1.37684163E-02   # N_12
  1  3    -3.86690083E-01   # N_13
  1  4    -3.22670387E-01   # N_14
  2  1    -4.96486343E-02   # N_21
  2  2     2.45075664E-02   # N_22
  2  3    -7.03232931E-01   # N_23
  2  4     7.08800280E-01   # N_24
  3  1     5.01367070E-01   # N_31
  3  2     2.81830430E-02   # N_32
  3  3     5.96582053E-01   # N_33
  3  4     6.26040439E-01   # N_34
  4  1    -1.02081721E-03   # N_41
  4  2     9.99207449E-01   # N_42
  4  3    -4.90696897E-03   # N_43
  4  4    -3.94886515E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.94512995E-03   # U_11
  1  2     9.99975882E-01   # U_12
  2  1    -9.99975882E-01   # U_21
  2  2     6.94512995E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.58566019E-02   # V_11
  1  2    -9.98438801E-01   # V_12
  2  1    -9.98438801E-01   # V_21
  2  2    -5.58566019E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94950887E-01   # cos(theta_t)
  1  2    -1.00363003E-01   # sin(theta_t)
  2  1     1.00363003E-01   # -sin(theta_t)
  2  2     9.94950887E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999390E-01   # cos(theta_b)
  1  2    -1.10453593E-03   # sin(theta_b)
  2  1     1.10453593E-03   # -sin(theta_b)
  2  2     9.99999390E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05843155E-01   # cos(theta_tau)
  1  2     7.08368153E-01   # sin(theta_tau)
  2  1    -7.08368153E-01   # -sin(theta_tau)
  2  2    -7.05843155E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00273039E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30885214E+03  # DRbar Higgs Parameters
         1    -3.46900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43659028E+02   # higgs               
         4     1.61396975E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30885214E+03  # The gauge couplings
     1     3.61944488E-01   # gprime(Q) DRbar
     2     6.36202423E-01   # g(Q) DRbar
     3     1.03002616E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30885214E+03  # The trilinear couplings
  1  1     1.38626891E-06   # A_u(Q) DRbar
  2  2     1.38628217E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30885214E+03  # The trilinear couplings
  1  1     5.09950365E-07   # A_d(Q) DRbar
  2  2     5.09997891E-07   # A_s(Q) DRbar
  3  3     9.12520378E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30885214E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11415221E-07   # A_mu(Q) DRbar
  3  3     1.12551950E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30885214E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66724970E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30885214E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83890344E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30885214E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03381079E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30885214E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57709096E+07   # M^2_Hd              
        22    -1.07519808E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40423838E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.47884609E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43764044E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43764044E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56235956E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56235956E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.88985456E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.06518527E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.29823800E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.56657200E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.07000473E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.10821163E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.81434384E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27797494E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.04780906E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.52630446E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.56878436E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.71294222E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.80204735E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.97332664E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.28327541E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.70530084E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.12287403E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.67445127E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.84973739E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66787512E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53767783E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79932924E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62560439E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.61217295E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62137268E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.70430091E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13565408E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.29179540E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.72363737E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.89781443E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.60418158E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78789482E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.94543543E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.95937243E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.30562068E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80157946E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.36881745E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59103230E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52266351E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61091903E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.11582633E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.35348351E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.37881777E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.19817338E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44918179E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78809869E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.72519049E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.89953846E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.84572466E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80662804E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.30070095E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62324099E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52484251E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54354728E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.07360482E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.53052623E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.59660826E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.33968797E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85632030E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78789482E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.94543543E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.95937243E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.30562068E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80157946E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.36881745E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59103230E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52266351E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61091903E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.11582633E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.35348351E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.37881777E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.19817338E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44918179E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78809869E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.72519049E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.89953846E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.84572466E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80662804E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.30070095E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62324099E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52484251E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54354728E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.07360482E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.53052623E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.59660826E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.33968797E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85632030E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14707987E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.15590568E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.04559706E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.94727676E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77814108E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.86488635E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57066862E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06029936E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.47278142E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.45678696E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.50264497E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.73581190E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14707987E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.15590568E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.04559706E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.94727676E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77814108E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.86488635E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57066862E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06029936E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.47278142E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.45678696E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.50264497E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.73581190E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09459512E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.69528860E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40462175E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.10442410E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40120371E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48948617E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80967280E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09025258E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.77806145E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.13812120E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.81449751E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42849795E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00762461E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86436851E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14813699E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.29221851E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.40221159E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.32574516E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78179343E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.14478059E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54794362E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14813699E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.29221851E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.40221159E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.32574516E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78179343E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.14478059E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54794362E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47497411E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17110263E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.27079074E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.01404538E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52231391E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.62019303E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03045172E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.85599678E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33510412E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33510412E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11171561E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11171561E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10636053E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.45594377E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.63097728E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.48956706E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.31487563E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.31175353E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.13845651E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.79955539E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.22991554E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.74392628E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.44990566E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17755927E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52748106E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17755927E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52748106E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43548578E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50266962E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50266962E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47608231E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00276431E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00276431E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00276413E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.27385653E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.27385653E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.27385653E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.27385653E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.57952916E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.57952916E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.57952916E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.57952916E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.76904238E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.76904238E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.57934157E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.76519520E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.63972181E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.48538003E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.22620609E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.48538003E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.22620609E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.17945597E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.77834335E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.77834335E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.77261711E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.63707612E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.63707612E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.63706684E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.14871917E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.78732820E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.14871917E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.78732820E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.91451318E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.39221463E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.39221463E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.57611453E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.27777526E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.27777526E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.27777527E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.92939918E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.92939918E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.92939918E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.92939918E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.43126987E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.43126987E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.43126987E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.43126987E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.95860584E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.95860584E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.45460948E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.60780308E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.56494046E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.18957522E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.13063004E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.13063004E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.74681618E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.72069088E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.71482011E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.77546991E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.77546991E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.78519266E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.78519266E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04149957E-03   # h decays
#          BR         NDA      ID1       ID2
     5.90603036E-01    2           5        -5   # BR(h -> b       bb     )
     6.45349787E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28452817E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.84389240E-04    2           3        -3   # BR(h -> s       sb     )
     2.10371810E-02    2           4        -4   # BR(h -> c       cb     )
     6.88701253E-02    2          21        21   # BR(h -> g       g      )
     2.38332124E-03    2          22        22   # BR(h -> gam     gam    )
     1.64125896E-03    2          22        23   # BR(h -> Z       gam    )
     2.22086452E-01    2          24       -24   # BR(h -> W+      W-     )
     2.81308054E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49582555E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55305705E-01    2           5        -5   # BR(H -> b       bb     )
     6.03294646E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13310374E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52309373E-04    2           3        -3   # BR(H -> s       sb     )
     7.07694524E-08    2           4        -4   # BR(H -> c       cb     )
     7.09257261E-03    2           6        -6   # BR(H -> t       tb     )
     8.80601971E-07    2          21        21   # BR(H -> g       g      )
     6.73425317E-10    2          22        22   # BR(H -> gam     gam    )
     1.82300947E-09    2          23        22   # BR(H -> Z       gam    )
     1.81020356E-06    2          24       -24   # BR(H -> W+      W-     )
     9.04474698E-07    2          23        23   # BR(H -> Z       Z      )
     7.10232500E-06    2          25        25   # BR(H -> h       h      )
     1.87208816E-24    2          36        36   # BR(H -> A       A      )
     3.36070267E-22    2          23        36   # BR(H -> Z       A      )
     1.85517006E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60266291E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60266291E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.18615706E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.81985339E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.32684944E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.68001169E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.20466834E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.33942179E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.11943401E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.15567087E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.58306659E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.88419346E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.69238850E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.69238850E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.35234469E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.49522537E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55370078E-01    2           5        -5   # BR(A -> b       bb     )
     6.03363294E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13334479E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52379064E-04    2           3        -3   # BR(A -> s       sb     )
     7.12329826E-08    2           4        -4   # BR(A -> c       cb     )
     7.10676830E-03    2           6        -6   # BR(A -> t       tb     )
     1.46026093E-05    2          21        21   # BR(A -> g       g      )
     5.96177761E-08    2          22        22   # BR(A -> gam     gam    )
     1.60513154E-08    2          23        22   # BR(A -> Z       gam    )
     1.80658382E-06    2          23        25   # BR(A -> Z       h      )
     1.86888452E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60278915E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60278915E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.89242963E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.62284795E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.11346703E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.26691207E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.83387204E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.46630786E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.24370699E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.74231514E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.86390497E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.84562335E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.84562335E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50583963E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.70460880E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02356546E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12978517E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.65094652E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20656283E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48228774E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.63258711E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.80513803E-06    2          24        25   # BR(H+ -> W+      h      )
     2.89430287E-14    2          24        36   # BR(H+ -> W+      A      )
     6.11949013E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.44485586E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.50971395E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60184026E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.80449080E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58741225E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14465690E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.01879162E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.51723037E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

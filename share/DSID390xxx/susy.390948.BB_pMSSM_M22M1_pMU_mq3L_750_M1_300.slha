#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15008937E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04025945E+01   # W+
        25     1.23957635E+02   # h
        35     3.00014277E+03   # H
        36     2.99999982E+03   # A
        37     3.00091749E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03535365E+03   # ~d_L
   2000001     3.03029314E+03   # ~d_R
   1000002     3.03444187E+03   # ~u_L
   2000002     3.03161498E+03   # ~u_R
   1000003     3.03535365E+03   # ~s_L
   2000003     3.03029314E+03   # ~s_R
   1000004     3.03444187E+03   # ~c_L
   2000004     3.03161498E+03   # ~c_R
   1000005     8.46825138E+02   # ~b_1
   2000005     3.02945952E+03   # ~b_2
   1000006     8.45131734E+02   # ~t_1
   2000006     3.01598293E+03   # ~t_2
   1000011     3.00631329E+03   # ~e_L
   2000011     3.00181438E+03   # ~e_R
   1000012     3.00492238E+03   # ~nu_eL
   1000013     3.00631329E+03   # ~mu_L
   2000013     3.00181438E+03   # ~mu_R
   1000014     3.00492238E+03   # ~nu_muL
   1000015     2.98560465E+03   # ~tau_1
   2000015     3.02202628E+03   # ~tau_2
   1000016     3.00478468E+03   # ~nu_tauL
   1000021     2.34368237E+03   # ~g
   1000022     3.01747074E+02   # ~chi_10
   1000023     6.30554654E+02   # ~chi_20
   1000025    -2.99763441E+03   # ~chi_30
   1000035     2.99847018E+03   # ~chi_40
   1000024     6.30717710E+02   # ~chi_1+
   1000037     2.99902090E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885191E-01   # N_11
  1  2    -7.72263109E-04   # N_12
  1  3     1.49674175E-02   # N_13
  1  4    -2.23280117E-03   # N_14
  2  1     1.19474774E-03   # N_21
  2  2     9.99605700E-01   # N_22
  2  3    -2.72335017E-02   # N_23
  2  4     6.73448826E-03   # N_24
  3  1    -8.99095815E-03   # N_31
  3  2     1.45048099E-02   # N_32
  3  3     7.06870389E-01   # N_33
  3  4     7.07137205E-01   # N_34
  4  1    -1.21383947E-02   # N_41
  4  2     2.40303656E-02   # N_42
  4  3     7.06660149E-01   # N_43
  4  4    -7.07040759E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99257559E-01   # U_11
  1  2    -3.85270033E-02   # U_12
  2  1     3.85270033E-02   # U_21
  2  2     9.99257559E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99954603E-01   # V_11
  1  2    -9.52848446E-03   # V_12
  2  1     9.52848446E-03   # V_21
  2  2     9.99954603E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98824883E-01   # cos(theta_t)
  1  2    -4.84649678E-02   # sin(theta_t)
  2  1     4.84649678E-02   # -sin(theta_t)
  2  2     9.98824883E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99912761E-01   # cos(theta_b)
  1  2     1.32087240E-02   # sin(theta_b)
  2  1    -1.32087240E-02   # -sin(theta_b)
  2  2     9.99912761E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06950644E-01   # cos(theta_tau)
  1  2     7.07262884E-01   # sin(theta_tau)
  2  1    -7.07262884E-01   # -sin(theta_tau)
  2  2     7.06950644E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01841177E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.50089367E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44263083E+02   # higgs               
         4     7.38490805E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.50089367E+03  # The gauge couplings
     1     3.62176281E-01   # gprime(Q) DRbar
     2     6.37388898E-01   # g(Q) DRbar
     3     1.02633713E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.50089367E+03  # The trilinear couplings
  1  1     2.13422798E-06   # A_u(Q) DRbar
  2  2     2.13425915E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.50089367E+03  # The trilinear couplings
  1  1     5.74032897E-07   # A_d(Q) DRbar
  2  2     5.74134329E-07   # A_s(Q) DRbar
  3  3     1.25483741E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.50089367E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.18699772E-07   # A_mu(Q) DRbar
  3  3     1.20105030E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.50089367E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51774873E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.50089367E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15345544E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.50089367E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07011518E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.50089367E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.53896054E+04   # M^2_Hd              
        22    -9.05806240E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41010833E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.16903523E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48221881E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48221881E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51778119E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51778119E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.85690323E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.51784870E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.03819094E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.51002419E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01422040E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.40046242E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.61348136E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.26189271E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.68548968E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.31716120E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.57909105E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.49734536E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.95491223E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.58329928E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.17679611E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.47280583E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.90951456E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.22133046E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.88505073E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.83568800E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.52502610E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.46797635E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -8.03419573E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28195794E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.94453976E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.24685533E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.10097370E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.79255313E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.00046837E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57820248E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.89446300E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     5.22072777E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.15703506E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.94813669E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.20475686E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16943914E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57535341E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.10492910E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.48799188E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.30033339E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42464428E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.79778952E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.09639991E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57612955E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.18104214E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     8.03720376E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.15134673E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.34263667E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.21155525E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66610886E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47701246E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.98165833E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.97180797E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.41733241E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55229810E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.79255313E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.00046837E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57820248E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.89446300E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     5.22072777E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.15703506E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.94813669E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.20475686E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16943914E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57535341E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.10492910E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.48799188E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.30033339E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42464428E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.79778952E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.09639991E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57612955E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.18104214E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     8.03720376E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.15134673E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.34263667E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.21155525E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66610886E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47701246E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.98165833E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.97180797E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.41733241E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55229810E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.71159238E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03261422E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99331896E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.60575359E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.60909274E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97406655E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.28150142E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53484124E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998667E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.33129961E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.39096106E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.45823181E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.71159238E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03261422E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99331896E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.60575359E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.60909274E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97406655E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.28150142E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53484124E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998667E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.33129961E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.39096106E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.45823181E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64988383E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.61671458E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13086050E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25242492E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59425165E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.70240637E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10231658E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.25920995E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.40720332E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19486770E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.42700758E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.71175322E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03769896E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98343370E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.95414896E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.75880841E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97886725E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     9.14751155E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.71175322E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03769896E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98343370E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.95414896E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.75880841E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97886725E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     9.14751155E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.71188930E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03761143E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98316242E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.84372751E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.51593048E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.97921626E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     9.81147285E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.34155030E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.38962563E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.78905591E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.15325447E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.71137308E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.09254926E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.21594926E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.91090006E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.45895368E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.17525826E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.29267873E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.47073213E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.40567389E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.47627379E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.17915660E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.88642351E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.88642351E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.37216689E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.91999680E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61155549E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61155549E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.29166098E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.29166098E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.86257103E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.86257103E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.32822810E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.17660571E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.90824352E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.95438742E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.95438742E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.54728996E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.56825268E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.58064071E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.58064071E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31842466E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31842466E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.74710738E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.74710738E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.69670209E-03   # h decays
#          BR         NDA      ID1       ID2
     6.87392568E-01    2           5        -5   # BR(h -> b       bb     )
     5.51190659E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.95127111E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.14799584E-04    2           3        -3   # BR(h -> s       sb     )
     1.79038549E-02    2           4        -4   # BR(h -> c       cb     )
     5.73808626E-02    2          21        21   # BR(h -> g       g      )
     1.93357706E-03    2          22        22   # BR(h -> gam     gam    )
     1.21057436E-03    2          22        23   # BR(h -> Z       gam    )
     1.58915719E-01    2          24       -24   # BR(h -> W+      W-     )
     1.95338516E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39706180E+01   # H decays
#          BR         NDA      ID1       ID2
     7.45883520E-01    2           5        -5   # BR(H -> b       bb     )
     1.78000186E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29366122E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72861361E-04    2           3        -3   # BR(H -> s       sb     )
     2.18104195E-07    2           4        -4   # BR(H -> c       cb     )
     2.17572998E-02    2           6        -6   # BR(H -> t       tb     )
     2.83166078E-05    2          21        21   # BR(H -> g       g      )
     3.40356138E-08    2          22        22   # BR(H -> gam     gam    )
     8.37524492E-09    2          23        22   # BR(H -> Z       gam    )
     3.21792013E-05    2          24       -24   # BR(H -> W+      W-     )
     1.60697657E-05    2          23        23   # BR(H -> Z       Z      )
     8.37283460E-05    2          25        25   # BR(H -> h       h      )
    -4.39698069E-23    2          36        36   # BR(H -> A       A      )
     4.69245918E-18    2          23        36   # BR(H -> Z       A      )
     1.96531563E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08211888E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.80203473E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.63624844E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.90728933E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.96322581E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32975615E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83707114E-01    2           5        -5   # BR(A -> b       bb     )
     1.87005254E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61204965E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12097003E-04    2           3        -3   # BR(A -> s       sb     )
     2.29162263E-07    2           4        -4   # BR(A -> c       cb     )
     2.28478248E-02    2           6        -6   # BR(A -> t       tb     )
     6.72846611E-05    2          21        21   # BR(A -> g       g      )
     4.46865219E-08    2          22        22   # BR(A -> gam     gam    )
     6.62983691E-08    2          23        22   # BR(A -> Z       gam    )
     3.36815084E-05    2          23        25   # BR(A -> Z       h      )
     2.63551028E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22076611E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31435474E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.93257469E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01827461E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10296280E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.44283270E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.63726060E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.05895087E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.07593524E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04428310E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.17643855E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.40522940E-05    2          24        25   # BR(H+ -> W+      h      )
     8.40947008E-14    2          24        36   # BR(H+ -> W+      A      )
     1.94275832E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.69241836E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.30172713E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

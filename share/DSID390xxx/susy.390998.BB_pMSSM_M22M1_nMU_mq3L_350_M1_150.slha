#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10165295E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03995181E+01   # W+
        25     1.26860847E+02   # h
        35     3.00022482E+03   # H
        36     3.00000026E+03   # A
        37     3.00111162E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01279379E+03   # ~d_L
   2000001     3.00715801E+03   # ~d_R
   1000002     3.01187287E+03   # ~u_L
   2000002     3.01162081E+03   # ~u_R
   1000003     3.01279379E+03   # ~s_L
   2000003     3.00715801E+03   # ~s_R
   1000004     3.01187287E+03   # ~c_L
   2000004     3.01162081E+03   # ~c_R
   1000005     3.88303104E+02   # ~b_1
   2000005     3.01089146E+03   # ~b_2
   1000006     3.81786896E+02   # ~t_1
   2000006     3.01715164E+03   # ~t_2
   1000011     3.00789186E+03   # ~e_L
   2000011     2.99865546E+03   # ~e_R
   1000012     3.00650014E+03   # ~nu_eL
   1000013     3.00789186E+03   # ~mu_L
   2000013     2.99865546E+03   # ~mu_R
   1000014     3.00650014E+03   # ~nu_muL
   1000015     2.98642449E+03   # ~tau_1
   2000015     3.02278784E+03   # ~tau_2
   1000016     3.00740314E+03   # ~nu_tauL
   1000021     2.31531745E+03   # ~g
   1000022     1.52340269E+02   # ~chi_10
   1000023     3.21226642E+02   # ~chi_20
   1000025    -3.01293067E+03   # ~chi_30
   1000035     3.01295319E+03   # ~chi_40
   1000024     3.21384345E+02   # ~chi_1+
   1000037     3.01387201E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891910E-01   # N_11
  1  2     3.82631991E-04   # N_12
  1  3    -1.46976681E-02   # N_13
  1  4     3.31787040E-07   # N_14
  2  1     1.67264661E-06   # N_21
  2  2     9.99657471E-01   # N_22
  2  3     2.61384205E-02   # N_23
  2  4     1.31260671E-03   # N_24
  3  1     1.03960790E-02   # N_31
  3  2    -1.94084807E-02   # N_32
  3  3     7.06762700E-01   # N_33
  3  4     7.07107996E-01   # N_34
  4  1    -1.03966049E-02   # N_41
  4  2     1.75529039E-02   # N_42
  4  3    -7.06814861E-01   # N_43
  4  4     7.07104348E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99317018E-01   # U_11
  1  2     3.69526281E-02   # U_12
  2  1    -3.69526281E-02   # U_21
  2  2     9.99317018E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998281E-01   # V_11
  1  2    -1.85441073E-03   # V_12
  2  1    -1.85441073E-03   # V_21
  2  2    -9.99998281E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98662639E-01   # cos(theta_t)
  1  2    -5.17004204E-02   # sin(theta_t)
  2  1     5.17004204E-02   # -sin(theta_t)
  2  2     9.98662639E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99714519E-01   # cos(theta_b)
  1  2    -2.38931057E-02   # sin(theta_b)
  2  1     2.38931057E-02   # -sin(theta_b)
  2  2     9.99714519E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06897232E-01   # cos(theta_tau)
  1  2     7.07316268E-01   # sin(theta_tau)
  2  1    -7.07316268E-01   # -sin(theta_tau)
  2  2    -7.06897232E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00230850E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.01652947E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44879726E+02   # higgs               
         4     1.23678321E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.01652947E+03  # The gauge couplings
     1     3.60853171E-01   # gprime(Q) DRbar
     2     6.38239187E-01   # g(Q) DRbar
     3     1.03501771E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.01652947E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.03466718E-07   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.01652947E+03  # The trilinear couplings
  1  1     3.23177258E-07   # A_d(Q) DRbar
  2  2     3.23216097E-07   # A_s(Q) DRbar
  3  3     6.73723903E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.01652947E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.56203748E-07   # A_mu(Q) DRbar
  3  3     1.58398460E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.01652947E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.61539990E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.01652947E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.09062099E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.01652947E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02607712E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.01652947E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.71094289E+04   # M^2_Hd              
        22    -9.21577954E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41400856E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.64228370E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48051932E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48051932E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51948068E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51948068E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.03004333E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     5.43097990E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.45690201E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.26365438E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.16747655E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.43933398E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.90664381E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.84846863E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.73101174E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64880678E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.24761922E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.14495257E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.02618026E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.97381974E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.85116976E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.79859568E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.21590775E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -7.61666519E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.10524331E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.97108775E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.14388770E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.67390118E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02849038E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.85935072E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61546437E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.23306083E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.09288129E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.23368963E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.54993280E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.26656821E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.45006720E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.03361191E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.80956691E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61481574E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.22736495E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.09972365E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.70538650E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42047818E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.21678274E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.55795218E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02849038E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.85935072E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61546437E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.23306083E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.09288129E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.23368963E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.54993280E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.26656821E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.45006720E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.03361191E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.80956691E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61481574E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.22736495E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.09972365E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.70538650E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42047818E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.21678274E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.55795218E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.95737483E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80571233E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00789109E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.01153768E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54528719E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.74866529E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.95737483E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80571233E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00789109E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.01153768E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54528719E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.74866529E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.77911787E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.47923057E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17481156E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.34595787E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.72233406E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.55992727E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14788596E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.40994216E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.65704690E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.29212652E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.95737112E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.95765569E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.77393598E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00621040E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.01639600E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.95765569E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.77393598E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00621040E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.01639600E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.95922832E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.77301835E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00596055E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.01673761E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.37491776E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     1.02567999E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.71439771E-12    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.21949520E-10    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     2.71439771E-12    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.21949520E-10    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.50955033E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61137609E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     9.77450243E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     9.77450243E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     2.12556687E-07    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.55530676E-09    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.55530676E-09    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.53853407E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.66801641E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.79574289E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.52887615E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.79792467E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.26970924E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.26314323E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.73685677E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.02242252E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.20826887E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.55709875E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.54833961E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.54833961E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.68307274E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.11028001E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     4.25126879E-11    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     4.25126879E-11    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.29566643E-11    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.29566643E-11    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     5.14608849E-13    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     5.14608849E-13    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     5.63878687E-11    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     5.63878687E-11    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     4.25126879E-11    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     4.25126879E-11    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.29566643E-11    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.29566643E-11    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     5.14608849E-13    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     5.14608849E-13    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     5.63878687E-11    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     5.63878687E-11    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     3.37875728E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.37875728E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.48564962E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.48564962E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.22080018E-10    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     1.22080018E-10    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     7.37353379E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     7.37353379E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.22080018E-10    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     1.22080018E-10    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     7.37353379E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     7.37353379E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.81083676E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.81083676E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     6.94087801E-10    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     6.94087801E-10    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     6.94087801E-10    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     6.94087801E-10    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     5.12995295E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     5.12995295E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.03451067E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.10647843E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.09447431E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.48294046E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.48294046E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13611630E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.91284951E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.49475684E-11    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.49475684E-11    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.32348531E-11    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.32348531E-11    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.75436633E-13    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.75436633E-13    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.61528107E-11    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.61528107E-11    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.49475684E-11    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.49475684E-11    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.32348531E-11    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.32348531E-11    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.75436633E-13    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.75436633E-13    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.61528107E-11    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.61528107E-11    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     3.25902276E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25902276E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.39962998E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.39962998E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     9.06289013E-11    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.06289013E-11    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     7.31101599E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     7.31101599E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.06289013E-11    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.06289013E-11    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     7.31101599E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     7.31101599E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.57639728E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.57639728E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     5.93132514E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     5.93132514E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     5.93132514E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     5.93132514E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     4.38879962E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     4.38879962E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.88093166E-03   # h decays
#          BR         NDA      ID1       ID2
     5.34219872E-01    2           5        -5   # BR(h -> b       bb     )
     6.78341102E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.40126305E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.08205933E-04    2           3        -3   # BR(h -> s       sb     )
     2.20751372E-02    2           4        -4   # BR(h -> c       cb     )
     7.31682021E-02    2          21        21   # BR(h -> g       g      )
     2.58299576E-03    2          22        22   # BR(h -> gam     gam    )
     1.89477990E-03    2          22        23   # BR(h -> Z       gam    )
     2.63584951E-01    2          24       -24   # BR(h -> W+      W-     )
     3.38916192E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.49763750E+01   # H decays
#          BR         NDA      ID1       ID2
     9.16127697E-01    2           5        -5   # BR(H -> b       bb     )
     5.52930462E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.95502998E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.40076789E-04    2           3        -3   # BR(H -> s       sb     )
     6.73156731E-08    2           4        -4   # BR(H -> c       cb     )
     6.71517651E-03    2           6        -6   # BR(H -> t       tb     )
     2.22415250E-06    2          21        21   # BR(H -> g       g      )
     2.88679609E-08    2          22        22   # BR(H -> gam     gam    )
     2.74446624E-09    2          23        22   # BR(H -> Z       gam    )
     8.21030380E-07    2          24       -24   # BR(H -> W+      W-     )
     4.10009009E-07    2          23        23   # BR(H -> Z       Z      )
     4.76497932E-06    2          25        25   # BR(H -> h       h      )
     4.57302278E-26    2          36        36   # BR(H -> A       A      )
     1.37268657E-17    2          23        36   # BR(H -> Z       A      )
     7.22099117E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.43290511E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.61078549E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.23587266E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.00739049E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.18299800E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.40766532E+01   # A decays
#          BR         NDA      ID1       ID2
     9.34809034E-01    2           5        -5   # BR(A -> b       bb     )
     5.64179457E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.99480095E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.45002976E-04    2           3        -3   # BR(A -> s       sb     )
     6.91363670E-08    2           4        -4   # BR(A -> c       cb     )
     6.89300055E-03    2           6        -6   # BR(A -> t       tb     )
     2.02992243E-05    2          21        21   # BR(A -> g       g      )
     5.15891383E-08    2          22        22   # BR(A -> gam     gam    )
     1.99910693E-08    2          23        22   # BR(A -> Z       gam    )
     8.34384348E-07    2          23        25   # BR(A -> Z       h      )
     7.64518919E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.53925347E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.82266889E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.32083553E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.76913188E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48928145E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.21611795E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.84429208E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.53138973E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.08384234E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.22981221E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.35699767E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.72484987E-07    2          24        25   # BR(H+ -> W+      h      )
     4.67732819E-14    2          24        36   # BR(H+ -> W+      A      )
     4.25795933E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.82994802E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.79542361E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90097580E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.25900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04196329E+01   # W+
        25     1.24777770E+02   # h
        35     3.99999919E+03   # H
        36     3.99999486E+03   # A
        37     4.00101850E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01271135E+03   # ~d_L
   2000001     4.01221417E+03   # ~d_R
   1000002     4.01205591E+03   # ~u_L
   2000002     4.01272740E+03   # ~u_R
   1000003     4.01271135E+03   # ~s_L
   2000003     4.01221417E+03   # ~s_R
   1000004     4.01205591E+03   # ~c_L
   2000004     4.01272740E+03   # ~c_R
   1000005     2.02670281E+03   # ~b_1
   2000005     4.01855629E+03   # ~b_2
   1000006     4.20371752E+02   # ~t_1
   2000006     2.03757771E+03   # ~t_2
   1000011     4.00160640E+03   # ~e_L
   2000011     4.00372056E+03   # ~e_R
   1000012     4.00049142E+03   # ~nu_eL
   1000013     4.00160640E+03   # ~mu_L
   2000013     4.00372056E+03   # ~mu_R
   1000014     4.00049142E+03   # ~nu_muL
   1000015     4.00413445E+03   # ~tau_1
   2000015     4.00901411E+03   # ~tau_2
   1000016     4.00310355E+03   # ~nu_tauL
   1000021     1.96782418E+03   # ~g
   1000022     9.16107031E+01   # ~chi_10
   1000023    -1.36135343E+02   # ~chi_20
   1000025     1.54324109E+02   # ~chi_30
   1000035     2.06765452E+03   # ~chi_40
   1000024     1.30678840E+02   # ~chi_1+
   1000037     2.06783008E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.56396619E-01   # N_11
  1  2     1.39916534E-02   # N_12
  1  3     5.38531047E-01   # N_13
  1  4     3.71015767E-01   # N_14
  2  1    -1.36509900E-01   # N_21
  2  2     2.72970803E-02   # N_22
  2  3    -6.85071754E-01   # N_23
  2  4     7.15050074E-01   # N_24
  3  1     6.39709611E-01   # N_31
  3  2     2.37793384E-02   # N_32
  3  3     4.90571862E-01   # N_33
  3  4     5.91223650E-01   # N_34
  4  1    -9.02990675E-04   # N_41
  4  2     9.99246539E-01   # N_42
  4  3    -5.00332362E-04   # N_43
  4  4    -3.87980433E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.11893749E-04   # U_11
  1  2     9.99999747E-01   # U_12
  2  1    -9.99999747E-01   # U_21
  2  2     7.11893749E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48821668E-02   # V_11
  1  2    -9.98492838E-01   # V_12
  2  1    -9.98492838E-01   # V_21
  2  2    -5.48821668E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.40218470E-02   # cos(theta_t)
  1  2     9.96463913E-01   # sin(theta_t)
  2  1    -9.96463913E-01   # -sin(theta_t)
  2  2    -8.40218470E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999868E-01   # cos(theta_b)
  1  2    -5.13809286E-04   # sin(theta_b)
  2  1     5.13809286E-04   # -sin(theta_b)
  2  2     9.99999868E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03086941E-01   # cos(theta_tau)
  1  2     7.11103898E-01   # sin(theta_tau)
  2  1    -7.11103898E-01   # -sin(theta_tau)
  2  2    -7.03086941E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00304660E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.00975800E+02  # DRbar Higgs Parameters
         1    -1.25900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44525935E+02   # higgs               
         4     1.61666079E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.00975800E+02  # The gauge couplings
     1     3.61041293E-01   # gprime(Q) DRbar
     2     6.36053243E-01   # g(Q) DRbar
     3     1.03762357E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.00975800E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.69028301E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.00975800E+02  # The trilinear couplings
  1  1     2.08940919E-07   # A_d(Q) DRbar
  2  2     2.08961362E-07   # A_s(Q) DRbar
  3  3     3.73029321E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.00975800E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.57945158E-08   # A_mu(Q) DRbar
  3  3     4.62594034E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.00975800E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.74146298E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.00975800E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83069899E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.00975800E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03805477E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.00975800E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58484367E+07   # M^2_Hd              
        22    -4.28679014E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40345971E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.71564504E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.16926623E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     7.25907597E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.79211679E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.83702778E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.64494784E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.90761121E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.66924322E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.52368346E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.63524698E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.51789947E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.74720673E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.77514856E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.11727269E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.24993666E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.30915569E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.39931688E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.16281664E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.75967156E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.34624715E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63455018E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.82066285E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.71060516E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42093019E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.85724667E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58074937E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.54918455E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14616299E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.26902824E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.50087384E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.22667578E-07    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.59992693E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74919333E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48000635E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.91125755E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.85852318E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81328393E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34238627E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61416683E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51852533E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57951024E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.22205187E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.04832616E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.30081268E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.49521793E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43723003E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74937485E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.19112932E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.50274109E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.38327614E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81800003E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.98939970E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64605734E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52079605E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51240763E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.41142086E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.73673750E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.00645070E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.51277897E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85308448E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74919333E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48000635E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.91125755E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.85852318E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81328393E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34238627E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61416683E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51852533E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57951024E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.22205187E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.04832616E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.30081268E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.49521793E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43723003E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74937485E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.19112932E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.50274109E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.38327614E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81800003E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.98939970E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64605734E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52079605E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51240763E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.41142086E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.73673750E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.00645070E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.51277897E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85308448E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11529458E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.90873424E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.29910868E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.71468504E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77043178E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.22824454E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55422998E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07267022E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.72600504E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.86264864E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.08772571E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.39271881E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11529458E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.90873424E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.29910868E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.71468504E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77043178E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.22824454E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55422998E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07267022E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.72600504E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.86264864E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.08772571E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.39271881E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08825826E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.26226185E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.02047685E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.63698995E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38301068E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43004064E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.77268577E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10093159E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.09515101E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.37332996E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.39272769E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41444353E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.24623489E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83572129E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11640811E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01430953E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.65885261E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.93083788E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77323018E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10535519E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53173442E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11640811E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01430953E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.65885261E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.93083788E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77323018E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10535519E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53173442E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45099982E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.16566486E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.11355071E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.35933140E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50837084E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.84575612E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00341841E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.57409342E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33711621E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33711621E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11237979E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11237979E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10100798E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.71159473E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.75170467E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.72892563E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.45245376E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.02171443E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.42498980E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.75242428E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37550099E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.81048568E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.52914070E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18585925E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53766269E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18585925E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53766269E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37908829E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52260393E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52260393E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48015227E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.04230959E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.04230959E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.04230910E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.67896489E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.67896489E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.67896489E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.67896489E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.22632468E-05    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.22632468E-05    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.22632468E-05    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.22632468E-05    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.38697469E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.38697469E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.57946637E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.38677321E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.34119496E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.75948806E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.86153796E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.75948806E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.86153796E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.59328249E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.10330426E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.10330426E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.09396880E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.23165229E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.23165229E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.23164663E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.25812008E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.41221595E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.25812008E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.41221595E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.82638108E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.15683582E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.15683582E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.04168851E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.31019262E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.31019262E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.31019282E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     9.23571958E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     9.23571958E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     9.23571958E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     9.23571958E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.07852100E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.07852100E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.07852100E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.07852100E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.99350160E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.99350160E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.71310362E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.27635198E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.28438742E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.36789428E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.39044003E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.39044003E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.79592002E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.15941341E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     9.28443626E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85477008E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85477008E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.59524220E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.59524220E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.96260984E-03   # h decays
#          BR         NDA      ID1       ID2
     6.08480191E-01    2           5        -5   # BR(h -> b       bb     )
     6.53616862E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31383304E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.91279955E-04    2           3        -3   # BR(h -> s       sb     )
     2.13338498E-02    2           4        -4   # BR(h -> c       cb     )
     6.89333494E-02    2          21        21   # BR(h -> g       g      )
     2.35402095E-03    2          22        22   # BR(h -> gam     gam    )
     1.54687119E-03    2          22        23   # BR(h -> Z       gam    )
     2.05580413E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56869554E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.41663883E+01   # H decays
#          BR         NDA      ID1       ID2
     3.40232662E-01    2           5        -5   # BR(H -> b       bb     )
     6.12112531E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16428165E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55997266E-04    2           3        -3   # BR(H -> s       sb     )
     7.18129502E-08    2           4        -4   # BR(H -> c       cb     )
     7.19715261E-03    2           6        -6   # BR(H -> t       tb     )
     1.03351546E-06    2          21        21   # BR(H -> g       g      )
     2.44140684E-10    2          22        22   # BR(H -> gam     gam    )
     1.84866845E-09    2          23        22   # BR(H -> Z       gam    )
     2.00908303E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00384570E-06    2          23        23   # BR(H -> Z       Z      )
     7.19377114E-06    2          25        25   # BR(H -> h       h      )
    -3.70541607E-24    2          36        36   # BR(H -> A       A      )
     2.19640580E-20    2          23        36   # BR(H -> Z       A      )
     1.87393226E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66357200E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66357200E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.38529304E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.63162914E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.69747264E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45250008E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.05893109E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     5.01167052E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.08733469E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.38786092E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.28202195E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     4.70229356E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.03888915E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.03888915E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.41656727E+01   # A decays
#          BR         NDA      ID1       ID2
     3.40263283E-01    2           5        -5   # BR(A -> b       bb     )
     6.12124852E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16432351E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56043928E-04    2           3        -3   # BR(A -> s       sb     )
     7.22673760E-08    2           4        -4   # BR(A -> c       cb     )
     7.20996760E-03    2           6        -6   # BR(A -> t       tb     )
     1.48146682E-05    2          21        21   # BR(A -> g       g      )
     4.07707931E-08    2          22        22   # BR(A -> gam     gam    )
     1.62922067E-08    2          23        22   # BR(A -> Z       gam    )
     2.00496954E-06    2          23        25   # BR(A -> Z       h      )
     1.87713096E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66361460E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66361460E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.95536246E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.25882679E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.34130159E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.93502624E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.34299920E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.68575619E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.35540909E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.26100240E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.73282757E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.24029360E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.24029360E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.39895459E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.41059970E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.14278910E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17193973E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.46278081E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23044485E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.53142074E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.45034520E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.01375911E-06    2          24        25   # BR(H+ -> W+      h      )
     2.73939523E-14    2          24        36   # BR(H+ -> W+      A      )
     4.79117837E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.40300533E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.17097809E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67121445E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.73936436E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57165138E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.04966012E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     6.97191617E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

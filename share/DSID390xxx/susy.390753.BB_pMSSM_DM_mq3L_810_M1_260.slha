#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13088793E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.04900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.13485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04165798E+01   # W+
        25     1.25668054E+02   # h
        35     4.00000824E+03   # H
        36     3.99999696E+03   # A
        37     4.00104159E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02611713E+03   # ~d_L
   2000001     4.02268835E+03   # ~d_R
   1000002     4.02546345E+03   # ~u_L
   2000002     4.02363786E+03   # ~u_R
   1000003     4.02611713E+03   # ~s_L
   2000003     4.02268835E+03   # ~s_R
   1000004     4.02546345E+03   # ~c_L
   2000004     4.02363786E+03   # ~c_R
   1000005     8.78914507E+02   # ~b_1
   2000005     4.02549383E+03   # ~b_2
   1000006     8.64925300E+02   # ~t_1
   2000006     2.14840541E+03   # ~t_2
   1000011     4.00470024E+03   # ~e_L
   2000011     4.00327026E+03   # ~e_R
   1000012     4.00358496E+03   # ~nu_eL
   1000013     4.00470024E+03   # ~mu_L
   2000013     4.00327026E+03   # ~mu_R
   1000014     4.00358496E+03   # ~nu_muL
   1000015     4.00476508E+03   # ~tau_1
   2000015     4.00745907E+03   # ~tau_2
   1000016     4.00500318E+03   # ~nu_tauL
   1000021     1.98060543E+03   # ~g
   1000022     2.50898575E+02   # ~chi_10
   1000023    -3.15738642E+02   # ~chi_20
   1000025     3.25575829E+02   # ~chi_30
   1000035     2.05223472E+03   # ~chi_40
   1000024     3.12996149E+02   # ~chi_1+
   1000037     2.05239958E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.86061088E-01   # N_11
  1  2    -1.18900670E-02   # N_12
  1  3    -3.63494407E-01   # N_13
  1  4    -2.87447717E-01   # N_14
  2  1    -5.76861127E-02   # N_21
  2  2     2.49647417E-02   # N_22
  2  3    -7.02234435E-01   # N_23
  2  4     7.09165617E-01   # N_24
  3  1     4.59964223E-01   # N_31
  3  2     2.81794409E-02   # N_32
  3  3     6.12145621E-01   # N_33
  3  4     6.42585848E-01   # N_34
  4  1    -9.86859464E-04   # N_41
  4  2     9.99220350E-01   # N_42
  4  3    -4.04394576E-03   # N_43
  4  4    -3.92602282E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.72442544E-03   # U_11
  1  2     9.99983615E-01   # U_12
  2  1    -9.99983615E-01   # U_21
  2  2     5.72442544E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.55341924E-02   # V_11
  1  2    -9.98456786E-01   # V_12
  2  1    -9.98456786E-01   # V_21
  2  2    -5.55341924E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94958417E-01   # cos(theta_t)
  1  2    -1.00288327E-01   # sin(theta_t)
  2  1     1.00288327E-01   # -sin(theta_t)
  2  2     9.94958417E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999532E-01   # cos(theta_b)
  1  2    -9.67470817E-04   # sin(theta_b)
  2  1     9.67470817E-04   # -sin(theta_b)
  2  2     9.99999532E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05654776E-01   # cos(theta_tau)
  1  2     7.08555811E-01   # sin(theta_tau)
  2  1    -7.08555811E-01   # -sin(theta_tau)
  2  2    -7.05654776E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00282371E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30887926E+03  # DRbar Higgs Parameters
         1    -3.04900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43640972E+02   # higgs               
         4     1.61271220E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30887926E+03  # The gauge couplings
     1     3.61970580E-01   # gprime(Q) DRbar
     2     6.36345291E-01   # g(Q) DRbar
     3     1.03002627E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30887926E+03  # The trilinear couplings
  1  1     1.38641159E-06   # A_u(Q) DRbar
  2  2     1.38642462E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30887926E+03  # The trilinear couplings
  1  1     5.09358837E-07   # A_d(Q) DRbar
  2  2     5.09406394E-07   # A_s(Q) DRbar
  3  3     9.11917979E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30887926E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.10873503E-07   # A_mu(Q) DRbar
  3  3     1.12006453E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30887926E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66807124E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30887926E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83036855E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30887926E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03533898E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30887926E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57978931E+07   # M^2_Hd              
        22    -8.00040200E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.09899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.13485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40488241E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.48210646E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.43771375E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.43771375E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.56228625E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.56228625E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.80160542E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     8.28594593E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.32959954E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.78718773E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.05461814E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11598899E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.43968189E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.28726846E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.09272842E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.54080138E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.70798596E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.69914619E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.74873652E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.96121000E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.01912696E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.47483912E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.04298068E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.81702345E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.86651567E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66743002E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52900219E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79151982E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63938929E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.01459726E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61362725E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.18697989E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13813006E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.91414296E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.30511741E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.19544307E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.22429463E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78804362E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.11351108E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.69888163E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.14886890E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80349367E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.32693688E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59482896E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52204711E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61125958E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.34399087E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.83326842E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.16467874E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.98872129E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44729947E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78824695E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.80658026E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11304489E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.00705111E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80842184E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.66036607E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62685674E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52422964E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54364461E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13329026E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.78274717E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.03848685E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.79465907E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85580775E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78804362E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.11351108E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.69888163E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.14886890E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80349367E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.32693688E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59482896E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52204711E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61125958E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.34399087E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.83326842E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.16467874E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.98872129E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44729947E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78824695E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.80658026E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11304489E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.00705111E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80842184E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.66036607E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62685674E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52422964E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54364461E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13329026E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.78274717E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.03848685E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.79465907E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85580775E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15020229E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.23042002E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.11527361E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.24436881E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77679833E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.31504252E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56770174E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06824064E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.86012338E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.31621854E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.10670909E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.34075436E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15020229E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.23042002E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.11527361E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.24436881E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77679833E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.31504252E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56770174E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06824064E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.86012338E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.31621854E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.10670909E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.34075436E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10177138E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.83161542E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35197223E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.81843682E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39918506E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46674159E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80548445E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09909269E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.94468572E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.24674454E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.64396607E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42464276E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.05524523E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85650648E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15125891E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.35136810E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68717209E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.75242474E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78023352E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11800375E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54510415E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15125891E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.35136810E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68717209E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.75242474E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78023352E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11800375E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54510415E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47975208E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.22423753E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.52845562E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.49349870E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51993915E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.65232712E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02595619E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.86837346E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33471791E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33471791E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11158467E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11158467E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10739484E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.46761468E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.62647593E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.48596396E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.33574486E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.02489826E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.16326510E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.10146232E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.25023059E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.53614690E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.66791669E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17579548E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52514789E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17579548E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52514789E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44915148E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49704412E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49704412E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47382528E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99163227E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99163227E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99163206E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.38055403E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.38055403E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.38055403E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.38055403E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.93518889E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.93518889E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.93518889E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.93518889E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.56201741E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.56201741E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.96110812E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.96895148E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.26519606E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.11188881E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.43912427E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.11188881E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.43912427E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.39261552E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.27466284E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.27466284E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.26540943E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.60857960E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.60857960E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.60857273E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.15861923E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.39445431E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.15861923E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.39445431E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     9.75734667E-07    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.23704590E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.23704590E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.01161890E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.47277100E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.47277100E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.47277102E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.80080154E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.80080154E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.80080154E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.80080154E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.60025347E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.60025347E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.60025347E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.60025347E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.45201785E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.45201785E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.46638424E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.67460032E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.43233369E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.43566309E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.15142814E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.15142814E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     5.76329484E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.87509079E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.77939364E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.77344380E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.77344380E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.78309985E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.78309985E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04798024E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91182701E-01    2           5        -5   # BR(h -> b       bb     )
     6.44351212E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28099314E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83637930E-04    2           3        -3   # BR(h -> s       sb     )
     2.10037764E-02    2           4        -4   # BR(h -> c       cb     )
     6.87653666E-02    2          21        21   # BR(h -> g       g      )
     2.37943203E-03    2          22        22   # BR(h -> gam     gam    )
     1.63891100E-03    2          22        23   # BR(h -> Z       gam    )
     2.21790051E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80929033E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48368204E+01   # H decays
#          BR         NDA      ID1       ID2
     3.51962792E-01    2           5        -5   # BR(H -> b       bb     )
     6.04630377E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13782657E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52868012E-04    2           3        -3   # BR(H -> s       sb     )
     7.09287938E-08    2           4        -4   # BR(H -> c       cb     )
     7.10854191E-03    2           6        -6   # BR(H -> t       tb     )
     9.10469576E-07    2          21        21   # BR(H -> g       g      )
     3.11727985E-10    2          22        22   # BR(H -> gam     gam    )
     1.82624375E-09    2          23        22   # BR(H -> Z       gam    )
     1.86368168E-06    2          24       -24   # BR(H -> W+      W-     )
     9.31195218E-07    2          23        23   # BR(H -> Z       Z      )
     7.22716065E-06    2          25        25   # BR(H -> h       h      )
     2.05102907E-24    2          36        36   # BR(H -> A       A      )
    -1.11407110E-21    2          23        36   # BR(H -> Z       A      )
     1.85421109E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61949602E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61949602E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.03076515E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.23241809E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.16747089E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.79936480E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.59100777E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.11783465E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.99917074E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.09267321E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.03479353E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.82406532E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.39525167E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.39525167E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.34321012E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48315408E+01   # A decays
#          BR         NDA      ID1       ID2
     3.52022314E-01    2           5        -5   # BR(A -> b       bb     )
     6.04691594E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13804133E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52934675E-04    2           3        -3   # BR(A -> s       sb     )
     7.13898017E-08    2           4        -4   # BR(A -> c       cb     )
     7.12241383E-03    2           6        -6   # BR(A -> t       tb     )
     1.46347575E-05    2          21        21   # BR(A -> g       g      )
     5.67600647E-08    2          22        22   # BR(A -> gam     gam    )
     1.60858029E-08    2          23        22   # BR(A -> Z       gam    )
     1.85993239E-06    2          23        25   # BR(A -> Z       h      )
     1.86219751E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61964434E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61964434E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.76125858E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.03368327E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.71920968E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.43714066E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.29171466E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.17702917E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.10612536E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.92482800E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.13889067E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.49445284E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.49445284E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48914245E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.64280711E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.04189655E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13626660E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.61139346E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21023447E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48984149E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.59420129E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.86001004E-06    2          24        25   # BR(H+ -> W+      h      )
     2.98218917E-14    2          24        36   # BR(H+ -> W+      A      )
     6.54248589E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.86705049E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.17967111E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62000972E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.29671507E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60089122E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.21814689E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.47683271E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.87238598E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

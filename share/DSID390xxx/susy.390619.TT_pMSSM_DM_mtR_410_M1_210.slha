#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90079717E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.15959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.31900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     4.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04203089E+01   # W+
        25     1.24778379E+02   # h
        35     4.00000394E+03   # H
        36     3.99999536E+03   # A
        37     4.00099414E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01269850E+03   # ~d_L
   2000001     4.01219765E+03   # ~d_R
   1000002     4.01204425E+03   # ~u_L
   2000002     4.01272433E+03   # ~u_R
   1000003     4.01269850E+03   # ~s_L
   2000003     4.01219765E+03   # ~s_R
   1000004     4.01204425E+03   # ~c_L
   2000004     4.01272433E+03   # ~c_R
   1000005     2.02683158E+03   # ~b_1
   2000005     4.01867327E+03   # ~b_2
   1000006     4.23877628E+02   # ~t_1
   2000006     2.03773947E+03   # ~t_2
   1000011     4.00160791E+03   # ~e_L
   2000011     4.00366966E+03   # ~e_R
   1000012     4.00049497E+03   # ~nu_eL
   1000013     4.00160791E+03   # ~mu_L
   2000013     4.00366966E+03   # ~mu_R
   1000014     4.00049497E+03   # ~nu_muL
   1000015     4.00397087E+03   # ~tau_1
   2000015     4.00910231E+03   # ~tau_2
   1000016     4.00310048E+03   # ~nu_tauL
   1000021     1.96781057E+03   # ~g
   1000022     1.93752629E+02   # ~chi_10
   1000023    -2.41782577E+02   # ~chi_20
   1000025     2.59368097E+02   # ~chi_30
   1000035     2.06755836E+03   # ~chi_40
   1000024     2.38453768E+02   # ~chi_1+
   1000037     2.06773361E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.01471059E-01   # N_11
  1  2    -1.50828350E-02   # N_12
  1  3    -4.65651665E-01   # N_13
  1  4    -3.74946899E-01   # N_14
  2  1    -7.36933408E-02   # N_21
  2  2     2.58666742E-02   # N_22
  2  3    -6.99588824E-01   # N_23
  2  4     7.10264517E-01   # N_24
  3  1     5.93474952E-01   # N_31
  3  2     2.51913220E-02   # N_32
  3  3     5.41975401E-01   # N_33
  3  4     5.94487630E-01   # N_34
  4  1    -9.56493868E-04   # N_41
  4  2     9.99234117E-01   # N_42
  4  3    -2.58236571E-03   # N_43
  4  4    -3.90332673E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     3.65701889E-03   # U_11
  1  2     9.99993313E-01   # U_12
  2  1    -9.99993313E-01   # U_21
  2  2     3.65701889E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52137632E-02   # V_11
  1  2    -9.98474557E-01   # V_12
  2  1    -9.98474557E-01   # V_21
  2  2    -5.52137632E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.42245548E-02   # cos(theta_t)
  1  2     9.96446800E-01   # sin(theta_t)
  2  1    -9.96446800E-01   # -sin(theta_t)
  2  2    -8.42245548E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999543E-01   # cos(theta_b)
  1  2    -9.56033363E-04   # sin(theta_b)
  2  1     9.56033363E-04   # -sin(theta_b)
  2  2     9.99999543E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05023724E-01   # cos(theta_tau)
  1  2     7.09183720E-01   # sin(theta_tau)
  2  1    -7.09183720E-01   # -sin(theta_tau)
  2  2    -7.05023724E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00271758E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.00797171E+02  # DRbar Higgs Parameters
         1    -2.31900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44585626E+02   # higgs               
         4     1.62405613E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.00797171E+02  # The gauge couplings
     1     3.60923714E-01   # gprime(Q) DRbar
     2     6.35358932E-01   # g(Q) DRbar
     3     1.03762417E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.00797171E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.68325464E-07   # A_c(Q) DRbar
  3  3     2.15959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.00797171E+02  # The trilinear couplings
  1  1     2.09093143E-07   # A_d(Q) DRbar
  2  2     2.09113461E-07   # A_s(Q) DRbar
  3  3     3.73405136E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.00797171E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.65230139E-08   # A_mu(Q) DRbar
  3  3     4.69946344E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.00797171E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.73949464E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.00797171E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85724999E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.00797171E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03378754E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.00797171E+02  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58109043E+07   # M^2_Hd              
        22    -8.08508687E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     4.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40036788E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.71000519E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.51933796E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     5.85994433E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.45035986E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.46896958E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.85870779E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     7.86502792E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.45675991E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.63005270E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.55049373E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.77957581E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.79205942E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.06747545E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.70773128E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.52648133E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.86325849E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.13474533E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.79633525E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.24125969E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63637994E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65903267E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.78712994E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.54423543E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.30888197E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62614594E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.48356696E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13629056E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     4.39017290E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     5.77942236E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.71630138E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.01112800E-04    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.74859188E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.64988027E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.25027458E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.66565869E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80450195E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.37283187E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59671204E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52122535E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.57908836E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.60455452E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.04005965E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.96961881E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.79877259E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43954233E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.74877441E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.45867354E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.39715872E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.69063550E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80940049E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.35716912E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62876019E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52348583E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51226601E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.40830187E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     7.93490032E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.14092701E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.30376138E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85371415E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.74859188E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.64988027E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.25027458E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.66565869E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80450195E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.37283187E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59671204E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52122535E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.57908836E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.60455452E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.04005965E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.96961881E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.79877259E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43954233E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.74877441E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.45867354E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.39715872E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.69063550E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80940049E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.35716912E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62876019E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52348583E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51226601E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.40830187E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     7.93490032E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.14092701E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.30376138E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85371415E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.10733752E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.97359689E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.31346267E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.73159469E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77141610E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.37334347E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55661394E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06270753E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.43206857E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.42368591E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.51368962E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.94944747E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.10733752E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.97359689E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.31346267E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.73159469E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77141610E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.37334347E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55661394E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06270753E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.43206857E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.42368591E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.51368962E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.94944747E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07881628E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.41436724E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.27668936E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.43726870E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38905589E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46617545E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78502170E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08333220E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.36398784E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.49799566E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.21978982E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41446282E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.16002592E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83595737E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.10845349E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.13800641E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.35331461E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.98643570E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77455641E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.12853477E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53397512E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.10845349E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.13800641E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.35331461E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.98643570E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77455641E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.12853477E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53397512E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44001172E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.02899811E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.12790066E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.50881285E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51114984E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.79095681E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00859608E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.07273945E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33618304E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33618304E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11207356E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11207356E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10348680E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.68917308E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.82214846E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.71929724E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.45035988E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.10087145E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39912644E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.76494595E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.37607277E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.75311275E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.33146888E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18353194E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53486129E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18353194E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53486129E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40054124E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51746858E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51746858E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48192802E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03181121E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03181121E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03181085E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.01983843E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.01983843E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.01983843E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.01983843E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.73280395E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.73280395E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.73280395E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.73280395E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.62990720E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.62990720E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.12532185E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.60407612E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.47810637E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.21998790E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.45110561E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.21998790E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.45110561E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.20048616E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.23229769E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.23229769E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.22589200E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.50563530E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.50563530E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.50562629E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.03155100E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.33783230E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.03155100E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.33783230E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     6.50654074E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.06636742E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.06636742E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.89300906E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.12867716E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.12867716E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.12867733E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.37211420E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.37211420E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.37211420E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.37211420E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.79066787E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.79066787E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.79066787E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.79066787E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.69301678E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.69301678E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69062003E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.93734888E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.39514255E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.58112078E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38751858E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38751858E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.20337866E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.02974635E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.01807501E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.88944814E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.88944814E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.59622342E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.59622342E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.95122720E-03   # h decays
#          BR         NDA      ID1       ID2
     6.07381782E-01    2           5        -5   # BR(h -> b       bb     )
     6.55416857E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32020507E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.92633261E-04    2           3        -3   # BR(h -> s       sb     )
     2.13953991E-02    2           4        -4   # BR(h -> c       cb     )
     6.91128172E-02    2          21        21   # BR(h -> g       g      )
     2.36321356E-03    2          22        22   # BR(h -> gam     gam    )
     1.55144977E-03    2          22        23   # BR(h -> Z       gam    )
     2.06166073E-01    2          24       -24   # BR(h -> W+      W-     )
     2.57629259E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45699601E+01   # H decays
#          BR         NDA      ID1       ID2
     3.49454901E-01    2           5        -5   # BR(H -> b       bb     )
     6.07586578E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14827898E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54104388E-04    2           3        -3   # BR(H -> s       sb     )
     7.12725644E-08    2           4        -4   # BR(H -> c       cb     )
     7.14299480E-03    2           6        -6   # BR(H -> t       tb     )
     8.01234735E-07    2          21        21   # BR(H -> g       g      )
     1.05819757E-10    2          22        22   # BR(H -> gam     gam    )
     1.83767092E-09    2          23        22   # BR(H -> Z       gam    )
     1.81630423E-06    2          24       -24   # BR(H -> W+      W-     )
     9.07523107E-07    2          23        23   # BR(H -> Z       Z      )
     6.72227148E-06    2          25        25   # BR(H -> h       h      )
     4.21416170E-24    2          36        36   # BR(H -> A       A      )
     5.77856193E-20    2          23        36   # BR(H -> Z       A      )
     1.86437525E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.62970521E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.62970521E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.82441305E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.14001968E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68681608E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.13127085E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.82385981E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.57963265E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.64611515E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.87362257E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.88992424E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.76685087E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.45013282E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.45013282E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.45679343E+01   # A decays
#          BR         NDA      ID1       ID2
     3.49493758E-01    2           5        -5   # BR(A -> b       bb     )
     6.07612492E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14836891E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54156460E-04    2           3        -3   # BR(A -> s       sb     )
     7.17346462E-08    2           4        -4   # BR(A -> c       cb     )
     7.15681825E-03    2           6        -6   # BR(A -> t       tb     )
     1.47054573E-05    2          21        21   # BR(A -> g       g      )
     5.11834238E-08    2          22        22   # BR(A -> gam     gam    )
     1.61740076E-08    2          23        22   # BR(A -> Z       gam    )
     1.81262302E-06    2          23        25   # BR(A -> Z       h      )
     1.86647025E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.62979623E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.62979623E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.42670920E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.41791014E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.37727059E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.68596107E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.88013557E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.51996808E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.86550127E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.12005436E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.70244358E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.49984012E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.49984012E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44680873E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.57332109E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.08878321E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15284457E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.56692244E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21962801E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50916702E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.55134705E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.81794201E-06    2          24        25   # BR(H+ -> W+      h      )
     2.40127708E-14    2          24        36   # BR(H+ -> W+      A      )
     5.32322440E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.38644636E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.53563393E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63485898E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.12179573E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60342995E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.62366026E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.03044747E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13068610E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.62959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     8.59990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04182143E+01   # W+
        25     1.25824676E+02   # h
        35     4.00000379E+03   # H
        36     3.99999661E+03   # A
        37     4.00105656E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02608967E+03   # ~d_L
   2000001     4.02273457E+03   # ~d_R
   1000002     4.02544042E+03   # ~u_L
   2000002     4.02334504E+03   # ~u_R
   1000003     4.02608967E+03   # ~s_L
   2000003     4.02273457E+03   # ~s_R
   1000004     4.02544042E+03   # ~c_L
   2000004     4.02334504E+03   # ~c_R
   1000005     2.03535993E+03   # ~b_1
   2000005     4.02561538E+03   # ~b_2
   1000006     8.77419090E+02   # ~t_1
   2000006     2.04985605E+03   # ~t_2
   1000011     4.00451823E+03   # ~e_L
   2000011     4.00364348E+03   # ~e_R
   1000012     4.00340705E+03   # ~nu_eL
   1000013     4.00451823E+03   # ~mu_L
   2000013     4.00364348E+03   # ~mu_R
   1000014     4.00340705E+03   # ~nu_muL
   1000015     4.00535304E+03   # ~tau_1
   2000015     4.00709566E+03   # ~tau_2
   1000016     4.00483780E+03   # ~nu_tauL
   1000021     1.99058511E+03   # ~g
   1000022     1.44878404E+02   # ~chi_10
   1000023    -1.96266808E+02   # ~chi_20
   1000025     2.10850294E+02   # ~chi_30
   1000035     2.06384332E+03   # ~chi_40
   1000024     1.92303810E+02   # ~chi_1+
   1000037     2.06402317E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15363400E-01   # N_11
  1  2    -1.34459061E-02   # N_12
  1  3    -4.64050958E-01   # N_13
  1  4    -3.45916814E-01   # N_14
  2  1    -9.37846497E-02   # N_21
  2  2     2.63661860E-02   # N_22
  2  3    -6.95912324E-01   # N_23
  2  4     7.11488089E-01   # N_24
  3  1     5.71302119E-01   # N_31
  3  2     2.51359814E-02   # N_32
  3  3     5.48051058E-01   # N_33
  3  4     6.10427809E-01   # N_34
  4  1    -9.24893745E-04   # N_41
  4  2     9.99245823E-01   # N_42
  4  3    -1.66809111E-03   # N_43
  4  4    -3.87833438E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36370788E-03   # U_11
  1  2     9.99997206E-01   # U_12
  2  1    -9.99997206E-01   # U_21
  2  2     2.36370788E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48606392E-02   # V_11
  1  2    -9.98494021E-01   # V_12
  2  1    -9.98494021E-01   # V_21
  2  2    -5.48606392E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -1.17619866E-01   # cos(theta_t)
  1  2     9.93058693E-01   # sin(theta_t)
  2  1    -9.93058693E-01   # -sin(theta_t)
  2  2    -1.17619866E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999717E-01   # cos(theta_b)
  1  2    -7.52329662E-04   # sin(theta_b)
  2  1     7.52329662E-04   # -sin(theta_b)
  2  2     9.99999717E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04817993E-01   # cos(theta_tau)
  1  2     7.09388185E-01   # sin(theta_tau)
  2  1    -7.09388185E-01   # -sin(theta_tau)
  2  2    -7.04817993E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00309711E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30686102E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43545465E+02   # higgs               
         4     1.60833633E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30686102E+03  # The gauge couplings
     1     3.62160220E-01   # gprime(Q) DRbar
     2     6.36325865E-01   # g(Q) DRbar
     3     1.02938411E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30686102E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37246050E-06   # A_c(Q) DRbar
  3  3     2.62959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30686102E+03  # The trilinear couplings
  1  1     5.02933838E-07   # A_d(Q) DRbar
  2  2     5.02980970E-07   # A_s(Q) DRbar
  3  3     9.01010696E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30686102E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08905043E-07   # A_mu(Q) DRbar
  3  3     1.10029556E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30686102E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67708014E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30686102E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81627028E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30686102E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04021589E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30686102E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58550992E+07   # M^2_Hd              
        22    -4.23977753E+03   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     8.59989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40472512E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.78183044E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.43599595E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.01553418E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.22250209E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.12248968E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.75345481E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.53063298E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.46216455E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.26136483E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.55065357E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.43014606E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.94143564E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.05731490E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.96661222E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.84071852E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.72468748E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.06621471E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.55987277E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.08547996E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -7.31582840E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.64127271E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.69281140E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77647954E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53932810E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.62795795E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63325017E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.13303316E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13457069E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.72577087E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.48121501E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.81809341E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     5.98750510E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.76142384E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.76428171E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.48869450E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.57253177E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83758258E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.31407693E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.66281326E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51121332E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58697683E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.75608639E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.95957308E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83915327E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.64657767E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43551620E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.76161282E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.50021480E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72128831E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.11337158E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84235484E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.86550035E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.69467870E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51344997E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51926486E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.80720309E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29495189E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.80205551E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.90878760E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85261239E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.76142384E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.76428171E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.48869450E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.57253177E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83758258E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.31407693E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.66281326E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51121332E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58697683E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.75608639E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.95957308E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83915327E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.64657767E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43551620E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.76161282E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.50021480E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72128831E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.11337158E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84235484E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.86550035E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.69467870E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51344997E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51926486E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.80720309E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29495189E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.80205551E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.90878760E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85261239E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13181672E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04299332E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.73933831E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.28424926E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77043275E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.72829649E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55435238E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08187595E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65466912E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.78487991E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25747745E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.62866701E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13181672E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04299332E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.73933831E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.28424926E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77043275E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.72829649E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55435238E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08187595E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65466912E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.78487991E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25747745E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.62866701E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10301279E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52133350E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16523772E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34920278E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38762899E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.43299845E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78201111E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10570169E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.45637173E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76301927E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.10926474E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41022664E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.20513685E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.82732128E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13290041E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17046458E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.25715076E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.60701766E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77343208E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08381074E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53199196E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13290041E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17046458E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.25715076E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.60701766E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77343208E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08381074E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53199196E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46548157E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05851561E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.94562552E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16638866E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50946002E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.80470802E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00545845E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.58869245E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33585442E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33585442E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11196192E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11196192E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10436733E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.69049551E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.12501316E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.80722118E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44197303E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.12404121E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.39400593E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.84753363E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38777476E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.43046042E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.90469336E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18046753E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53132381E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18046753E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53132381E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41150096E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51183634E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51183634E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47895196E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02176270E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02176270E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02176246E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.94659125E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.94659125E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.94659125E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.94659125E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.31553266E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.31553266E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.31553266E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.31553266E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.96412975E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.96412975E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.24052453E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.78005476E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.02265478E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.63220865E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.87487201E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.63220865E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.87487201E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.40685970E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.24420078E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.24420078E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.22922648E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.53781118E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.53781118E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.53780527E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.79434539E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.92241087E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.79434539E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.92241087E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.55746471E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.12908803E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.12908803E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.03658959E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.25693080E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.25693080E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.25693084E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.27761825E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.27761825E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.27761825E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.27761825E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.42585438E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.42585438E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.42585438E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.42585438E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.36283486E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.36283486E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69087212E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.62710436E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.33977230E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.37824389E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.37870096E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.37870096E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.56654957E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08942418E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.03441162E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.02965867E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     2.02965867E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.93423397E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.93423397E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.09366833E-03   # h decays
#          BR         NDA      ID1       ID2
     5.91388629E-01    2           5        -5   # BR(h -> b       bb     )
     6.38025497E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25859349E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78771833E-04    2           3        -3   # BR(h -> s       sb     )
     2.07901899E-02    2           4        -4   # BR(h -> c       cb     )
     6.79776565E-02    2          21        21   # BR(h -> g       g      )
     2.36564051E-03    2          22        22   # BR(h -> gam     gam    )
     1.64316946E-03    2          22        23   # BR(h -> Z       gam    )
     2.23007304E-01    2          24       -24   # BR(h -> W+      W-     )
     2.83202303E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.42150599E+01   # H decays
#          BR         NDA      ID1       ID2
     3.42773847E-01    2           5        -5   # BR(H -> b       bb     )
     6.11563678E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.16234104E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55767687E-04    2           3        -3   # BR(H -> s       sb     )
     7.17499996E-08    2           4        -4   # BR(H -> c       cb     )
     7.19084374E-03    2           6        -6   # BR(H -> t       tb     )
     1.02171356E-06    2          21        21   # BR(H -> g       g      )
     3.44494410E-11    2          22        22   # BR(H -> gam     gam    )
     1.84599525E-09    2          23        22   # BR(H -> Z       gam    )
     2.03552096E-06    2          24       -24   # BR(H -> W+      W-     )
     1.01705543E-06    2          23        23   # BR(H -> Z       Z      )
     7.51471138E-06    2          25        25   # BR(H -> h       h      )
    -4.40922961E-24    2          36        36   # BR(H -> A       A      )
     6.12012574E-20    2          23        36   # BR(H -> Z       A      )
     1.86229456E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.65516399E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.65516399E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.89951931E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.57104191E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.61557439E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.10094981E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.82762267E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.65388861E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.63407704E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.77308647E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.17380037E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.92609401E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.46649184E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.46649184E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.42111241E+01   # A decays
#          BR         NDA      ID1       ID2
     3.42824713E-01    2           5        -5   # BR(A -> b       bb     )
     6.11611906E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16250986E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55829355E-04    2           3        -3   # BR(A -> s       sb     )
     7.22068136E-08    2           4        -4   # BR(A -> c       cb     )
     7.20392542E-03    2           6        -6   # BR(A -> t       tb     )
     1.48022448E-05    2          21        21   # BR(A -> g       g      )
     4.71878082E-08    2          22        22   # BR(A -> gam     gam    )
     1.62745271E-08    2          23        22   # BR(A -> Z       gam    )
     2.03136927E-06    2          23        25   # BR(A -> Z       h      )
     1.86370258E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.65532076E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.65532076E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.51085895E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94703060E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30708296E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67944741E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.58251963E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.51151691E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.84003107E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.29771167E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.78672002E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.65013570E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.65013570E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.40853295E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.46061128E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.13196872E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16811391E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.49478820E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22827598E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52695868E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.48145450E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.03842613E-06    2          24        25   # BR(H+ -> W+      h      )
     3.25517024E-14    2          24        36   # BR(H+ -> W+      A      )
     5.60265190E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.47709729E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.35433205E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66133969E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.19081850E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61227575E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.99681628E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.46037267E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

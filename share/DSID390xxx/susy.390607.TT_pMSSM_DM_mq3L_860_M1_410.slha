#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13058548E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.34900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     8.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04181138E+01   # W+
        25     1.25838739E+02   # h
        35     4.00001033E+03   # H
        36     3.99999722E+03   # A
        37     4.00103213E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02604144E+03   # ~d_L
   2000001     4.02264555E+03   # ~d_R
   1000002     4.02538983E+03   # ~u_L
   2000002     4.02349144E+03   # ~u_R
   1000003     4.02604144E+03   # ~s_L
   2000003     4.02264555E+03   # ~s_R
   1000004     4.02538983E+03   # ~c_L
   2000004     4.02349144E+03   # ~c_R
   1000005     9.25200792E+02   # ~b_1
   2000005     4.02550121E+03   # ~b_2
   1000006     9.07439264E+02   # ~t_1
   2000006     2.01919396E+03   # ~t_2
   1000011     4.00462075E+03   # ~e_L
   2000011     4.00330618E+03   # ~e_R
   1000012     4.00350817E+03   # ~nu_eL
   1000013     4.00462075E+03   # ~mu_L
   2000013     4.00330618E+03   # ~mu_R
   1000014     4.00350817E+03   # ~nu_muL
   1000015     4.00416677E+03   # ~tau_1
   2000015     4.00800079E+03   # ~tau_2
   1000016     4.00492255E+03   # ~nu_tauL
   1000021     1.97937171E+03   # ~g
   1000022     3.97305056E+02   # ~chi_10
   1000023    -4.46476729E+02   # ~chi_20
   1000025     4.61683546E+02   # ~chi_30
   1000035     2.05213035E+03   # ~chi_40
   1000024     4.44251754E+02   # ~chi_1+
   1000037     2.05229498E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.20545264E-01   # N_11
  1  2    -1.72190603E-02   # N_12
  1  3    -4.27105047E-01   # N_13
  1  4    -3.79460477E-01   # N_14
  2  1    -3.86323899E-02   # N_21
  2  2     2.36027246E-02   # N_22
  2  3    -7.04458572E-01   # N_23
  2  4     7.08299774E-01   # N_24
  3  1     5.70273444E-01   # N_31
  3  2     2.83000933E-02   # N_32
  3  3     5.66809949E-01   # N_33
  3  4     5.93897117E-01   # N_34
  4  1    -1.09885306E-03   # N_41
  4  2     9.99172418E-01   # N_42
  4  3    -6.77358598E-03   # N_43
  4  4    -4.00922808E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.58527005E-03   # U_11
  1  2     9.99954060E-01   # U_12
  2  1    -9.99954060E-01   # U_21
  2  2     9.58527005E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.67088674E-02   # V_11
  1  2    -9.98390757E-01   # V_12
  2  1    -9.98390757E-01   # V_21
  2  2    -5.67088674E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.92904982E-01   # cos(theta_t)
  1  2    -1.18910457E-01   # sin(theta_t)
  2  1     1.18910457E-01   # -sin(theta_t)
  2  2     9.92904982E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999015E-01   # cos(theta_b)
  1  2    -1.40356654E-03   # sin(theta_b)
  2  1     1.40356654E-03   # -sin(theta_b)
  2  2     9.99999015E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06120327E-01   # cos(theta_tau)
  1  2     7.08091861E-01   # sin(theta_tau)
  2  1    -7.08091861E-01   # -sin(theta_tau)
  2  2    -7.06120327E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00258821E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30585480E+03  # DRbar Higgs Parameters
         1    -4.34900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43662876E+02   # higgs               
         4     1.61444454E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30585480E+03  # The gauge couplings
     1     3.61897755E-01   # gprime(Q) DRbar
     2     6.35915228E-01   # g(Q) DRbar
     3     1.03004318E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30585480E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37981693E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30585480E+03  # The trilinear couplings
  1  1     5.08775852E-07   # A_d(Q) DRbar
  2  2     5.08822899E-07   # A_s(Q) DRbar
  3  3     9.09710432E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30585480E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.12320797E-07   # A_mu(Q) DRbar
  3  3     1.13466440E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30585480E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.67332839E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30585480E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.86165606E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30585480E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03104480E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30585480E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57028779E+07   # M^2_Hd              
        22    -1.61838481E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     8.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40291709E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.21218606E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.42125397E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.42125397E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.57874603E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.57874603E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.73534341E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.48388320E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.20062858E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.20540827E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.11007994E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17986050E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.84163869E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12684164E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.52300637E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.22346955E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.65139433E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.18602799E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.37580199E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.20107935E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.10734461E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.23485760E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.38227303E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.82755248E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66684570E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.55808606E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81764513E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60248193E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59693103E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64794884E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.16950146E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12776483E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.05470693E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.79766121E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.68933929E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.95982415E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78531565E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.64360698E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26000358E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.58680129E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80381572E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.48967478E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.59553525E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52204514E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60774183E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.69000982E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.13850124E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.77042725E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.71212899E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45314207E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78551813E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.56320145E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.16452726E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.43609118E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.80915054E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.56848019E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.62826055E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52421748E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54106882E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.62200014E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.12217575E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.61651967E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.67693349E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85740249E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78531565E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.64360698E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26000358E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.58680129E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80381572E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.48967478E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.59553525E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52204514E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60774183E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.69000982E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.13850124E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.77042725E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.71212899E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45314207E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78551813E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.56320145E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.16452726E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.43609118E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.80915054E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.56848019E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.62826055E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52421748E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54106882E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.62200014E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.12217575E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.61651967E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.67693349E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85740249E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13990189E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01741166E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.30848519E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.21842831E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78158724E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.19659333E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57822552E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.04063719E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.74827089E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.48804677E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.23684194E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.70922986E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13990189E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01741166E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.30848519E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.21842831E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78158724E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.19659333E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57822552E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.04063719E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.74827089E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.48804677E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.23684194E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.70922986E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.07718130E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.43206720E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.47718831E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34022820E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40611550E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.54018244E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81985203E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06963193E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.47266130E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.97963982E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.11650462E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43828414E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90284205E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88430175E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14095936E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17844888E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03923506E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.38065487E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78574313E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.21697211E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55518042E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14095936E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17844888E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03923506E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.38065487E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78574313E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.21697211E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55518042E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46402066E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06893697E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.42663653E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.97358541E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52810371E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.54739275E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04143487E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     9.48557949E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33590420E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33590420E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11198309E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11198309E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10422543E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.27051582E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.60504728E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.04554529E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.44612404E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.49073184E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.90080020E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.30332077E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.38927824E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.40405122E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.43056822E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.57789292E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18133522E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53246779E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18133522E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53246779E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40679809E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51463498E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51463498E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48103150E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02658598E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02658598E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02658584E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.25932412E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.25932412E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.25932412E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.25932412E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.53108643E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.53108643E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.53108643E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.53108643E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.40973825E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.40973825E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.15056610E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.16647363E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.61689546E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     3.21433743E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     4.14036076E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     3.21433743E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     4.14036076E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.95535629E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     9.26462360E-03    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     9.26462360E-03    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     9.27758182E-03    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.90958357E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.90958357E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.90957694E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.20156338E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.55875010E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.20156338E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.55875010E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.57960079E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.57513986E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.57513986E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.30658329E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     7.14671398E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     7.14671398E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     7.14671404E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.09074517E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.09074517E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.09074517E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.09074517E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.69688547E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.69688547E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.69688547E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.69688547E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.56268924E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.56268924E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.26908901E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.12636787E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.98167916E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.79780817E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.30089672E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.30089672E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.22116325E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.48009223E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.68276890E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.76047549E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.76047549E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.76562741E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.76562741E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05272825E-03   # h decays
#          BR         NDA      ID1       ID2
     5.86727167E-01    2           5        -5   # BR(h -> b       bb     )
     6.44411937E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28120071E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83554833E-04    2           3        -3   # BR(h -> s       sb     )
     2.10021097E-02    2           4        -4   # BR(h -> c       cb     )
     6.88021238E-02    2          21        21   # BR(h -> g       g      )
     2.39221095E-03    2          22        22   # BR(h -> gam     gam    )
     1.66180920E-03    2          22        23   # BR(h -> Z       gam    )
     2.25605895E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86558155E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53387945E+01   # H decays
#          BR         NDA      ID1       ID2
     3.62652120E-01    2           5        -5   # BR(H -> b       bb     )
     5.99146274E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11843611E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50574438E-04    2           3        -3   # BR(H -> s       sb     )
     7.02788245E-08    2           4        -4   # BR(H -> c       cb     )
     7.04340150E-03    2           6        -6   # BR(H -> t       tb     )
     8.13548322E-07    2          21        21   # BR(H -> g       g      )
     3.23559988E-09    2          22        22   # BR(H -> gam     gam    )
     1.81202244E-09    2          23        22   # BR(H -> Z       gam    )
     1.72433479E-06    2          24       -24   # BR(H -> W+      W-     )
     8.61570125E-07    2          23        23   # BR(H -> Z       Z      )
     6.91381340E-06    2          25        25   # BR(H -> h       h      )
     3.32050911E-24    2          36        36   # BR(H -> A       A      )
    -2.39200481E-21    2          23        36   # BR(H -> Z       A      )
     1.85527895E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.55994542E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.55994542E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.38280040E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.96880736E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.53443013E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.43302884E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.29220332E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.68955242E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.33201431E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.22340793E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.73908731E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.62792266E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.53872780E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.53872780E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.35695634E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53375896E+01   # A decays
#          BR         NDA      ID1       ID2
     3.62685585E-01    2           5        -5   # BR(A -> b       bb     )
     5.99161875E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11848960E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50621662E-04    2           3        -3   # BR(A -> s       sb     )
     7.07369634E-08    2           4        -4   # BR(A -> c       cb     )
     7.05728149E-03    2           6        -6   # BR(A -> t       tb     )
     1.45009258E-05    2          21        21   # BR(A -> g       g      )
     6.47631311E-08    2          22        22   # BR(A -> gam     gam    )
     1.59429597E-08    2          23        22   # BR(A -> Z       gam    )
     1.72072152E-06    2          23        25   # BR(A -> Z       h      )
     1.88680097E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.55980093E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.55980093E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.07641383E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.39635388E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.31397854E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.91238459E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.20422519E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.00740166E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.47438624E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.30758544E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.32876988E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.98102698E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.98102698E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.55336747E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.83718512E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.97200754E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11155556E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.73579531E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19623556E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46104123E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.71479024E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.71657747E-06    2          24        25   # BR(H+ -> W+      h      )
     2.81309391E-14    2          24        36   # BR(H+ -> W+      A      )
     5.32304108E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.69264684E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.06503667E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.55650723E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.68806414E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.54778606E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00547001E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.43964972E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.55138672E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

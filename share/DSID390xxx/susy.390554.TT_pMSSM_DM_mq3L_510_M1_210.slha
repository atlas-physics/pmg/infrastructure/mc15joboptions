#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10035575E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04178013E+01   # W+
        25     1.24913101E+02   # h
        35     4.00000537E+03   # H
        36     3.99999590E+03   # A
        37     4.00101155E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01653928E+03   # ~d_L
   2000001     4.01508906E+03   # ~d_R
   1000002     4.01587944E+03   # ~u_L
   2000002     4.01633104E+03   # ~u_R
   1000003     4.01653928E+03   # ~s_L
   2000003     4.01508906E+03   # ~s_R
   1000004     4.01587944E+03   # ~c_L
   2000004     4.01633104E+03   # ~c_R
   1000005     5.51027177E+02   # ~b_1
   2000005     4.02020067E+03   # ~b_2
   1000006     5.36285797E+02   # ~t_1
   2000006     2.02379774E+03   # ~t_2
   1000011     4.00280233E+03   # ~e_L
   2000011     4.00296173E+03   # ~e_R
   1000012     4.00168276E+03   # ~nu_eL
   1000013     4.00280233E+03   # ~mu_L
   2000013     4.00296173E+03   # ~mu_R
   1000014     4.00168276E+03   # ~nu_muL
   1000015     4.00458322E+03   # ~tau_1
   2000015     4.00794737E+03   # ~tau_2
   1000016     4.00394376E+03   # ~nu_tauL
   1000021     1.96406661E+03   # ~g
   1000022     2.02493941E+02   # ~chi_10
   1000023    -2.70785757E+02   # ~chi_20
   1000025     2.79532538E+02   # ~chi_30
   1000035     2.05622407E+03   # ~chi_40
   1000024     2.67836581E+02   # ~chi_1+
   1000037     2.05638843E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99141528E-01   # N_11
  1  2    -1.04277709E-02   # N_12
  1  3    -3.52144995E-01   # N_13
  1  4    -2.59672251E-01   # N_14
  2  1    -6.91355235E-02   # N_21
  2  2     2.55264838E-02   # N_22
  2  3    -7.00625207E-01   # N_23
  2  4     7.09713320E-01   # N_24
  3  1     4.32161861E-01   # N_31
  3  2     2.79950068E-02   # N_32
  3  3     6.20570880E-01   # N_33
  3  4     6.53715678E-01   # N_34
  4  1    -9.58285691E-04   # N_41
  4  2     9.99227672E-01   # N_42
  4  3    -3.16291812E-03   # N_43
  4  4    -3.91553039E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.47826543E-03   # U_11
  1  2     9.99989973E-01   # U_12
  2  1    -9.99989973E-01   # U_21
  2  2     4.47826543E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.53865896E-02   # V_11
  1  2    -9.98464985E-01   # V_12
  2  1    -9.98464985E-01   # V_21
  2  2    -5.53865896E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95956027E-01   # cos(theta_t)
  1  2    -8.98420407E-02   # sin(theta_t)
  2  1     8.98420407E-02   # -sin(theta_t)
  2  2     9.95956027E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999670E-01   # cos(theta_b)
  1  2    -8.12403773E-04   # sin(theta_b)
  2  1     8.12403773E-04   # -sin(theta_b)
  2  2     9.99999670E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05167825E-01   # cos(theta_tau)
  1  2     7.09040435E-01   # sin(theta_tau)
  2  1    -7.09040435E-01   # -sin(theta_tau)
  2  2    -7.05167825E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00275231E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00355747E+03  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44326898E+02   # higgs               
         4     1.62134497E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00355747E+03  # The gauge couplings
     1     3.61121025E-01   # gprime(Q) DRbar
     2     6.36186292E-01   # g(Q) DRbar
     3     1.03613397E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00355747E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.51792023E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00355747E+03  # The trilinear couplings
  1  1     2.76435197E-07   # A_d(Q) DRbar
  2  2     2.76461726E-07   # A_s(Q) DRbar
  3  3     4.93624239E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00355747E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.07692631E-08   # A_mu(Q) DRbar
  3  3     6.13851696E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00355747E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72228662E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00355747E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83916920E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00355747E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03387435E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00355747E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58056196E+07   # M^2_Hd              
        22    -7.94839689E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40412702E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.01013163E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45192154E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45192154E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54807846E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54807846E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.41391487E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.01945541E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.99871805E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.57629678E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.40552976E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17181535E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.80798420E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.21474351E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.05879062E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.39799650E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.46693045E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.09236280E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.28837768E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.92189683E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.51980577E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.29268634E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.76601447E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.54214934E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66534655E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52258091E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76380090E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62669855E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.75903086E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.56986472E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.37633411E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14831780E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.50972261E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.76492024E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.64445055E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     4.64197176E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78204731E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.21756298E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.34165252E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04306932E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77524665E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.31560524E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.53830539E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53058870E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60927062E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.45940490E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.62657592E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02573212E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.78764333E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44885944E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78224717E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83473489E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29123596E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.50363403E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78007707E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.47526822E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57019354E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53279597E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54168208E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16335701E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.85212152E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.67589397E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.26919886E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85622007E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78204731E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.21756298E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.34165252E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04306932E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77524665E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.31560524E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.53830539E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53058870E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60927062E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.45940490E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.62657592E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02573212E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.78764333E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44885944E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78224717E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83473489E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29123596E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.50363403E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78007707E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.47526822E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57019354E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53279597E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54168208E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16335701E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.85212152E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.67589397E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.26919886E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85622007E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13745276E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27729998E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.57639981E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.79930317E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77590437E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.04177436E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56570352E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06460225E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09172889E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.76463276E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86061978E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.00631748E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13745276E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27729998E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.57639981E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.79930317E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77590437E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.04177436E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56570352E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06460225E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09172889E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.76463276E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86061978E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.00631748E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09453731E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92053380E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28532340E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.07505954E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39697633E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.45501947E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80094963E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09553843E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.04029099E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40596196E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53280686E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42292187E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10424982E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85295910E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13852830E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38522070E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13377683E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.40057496E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77910891E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.12122305E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54306289E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13852830E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38522070E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13377683E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.40057496E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77910891E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.12122305E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54306289E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46898831E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25397828E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.93162150E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17314450E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51782678E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.69642179E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02192209E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.41340139E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33454696E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33454696E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11152457E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11152457E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10785694E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.50317168E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.82514508E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     8.12645069E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.71597632E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.25660156E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.01563145E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.12112753E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.37540675E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.13407891E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.83694005E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.53831546E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17529478E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52398621E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17529478E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52398621E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45772489E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49145890E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49145890E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46962700E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.97967942E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.97967942E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.97967903E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.04332451E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.04332451E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.04332451E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.04332451E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.01444282E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.01444282E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01444282E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01444282E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.56491653E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.56491653E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.75958310E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.26361288E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.00308064E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.16014692E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50212867E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.16014692E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50212867E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45596116E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.42352666E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.42352666E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.41156035E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.88739743E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.88739743E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.88738968E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.23575984E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.60247191E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.23575984E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.60247191E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.67179395E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.67179395E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.81951630E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     7.33855593E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     7.33855593E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     7.33855604E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.78062465E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.78062465E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.78062465E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.78062465E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.93536735E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.93536735E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.93536735E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.93536735E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.30530474E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.30530474E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.50216486E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.41109011E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.65021429E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.26411546E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.10517570E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.10517570E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.70310070E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.56810864E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.18025188E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.87977874E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.87977874E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.89099822E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.89099822E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.95146312E-03   # h decays
#          BR         NDA      ID1       ID2
     6.03481988E-01    2           5        -5   # BR(h -> b       bb     )
     6.56096156E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32260375E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93038809E-04    2           3        -3   # BR(h -> s       sb     )
     2.14127077E-02    2           4        -4   # BR(h -> c       cb     )
     6.93263024E-02    2          21        21   # BR(h -> g       g      )
     2.37701466E-03    2          22        22   # BR(h -> gam     gam    )
     1.57010111E-03    2          22        23   # BR(h -> Z       gam    )
     2.09296252E-01    2          24       -24   # BR(h -> W+      W-     )
     2.62007196E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50095808E+01   # H decays
#          BR         NDA      ID1       ID2
     3.51847569E-01    2           5        -5   # BR(H -> b       bb     )
     6.02731116E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13111124E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52073729E-04    2           3        -3   # BR(H -> s       sb     )
     7.07039776E-08    2           4        -4   # BR(H -> c       cb     )
     7.08601059E-03    2           6        -6   # BR(H -> t       tb     )
     7.88730535E-07    2          21        21   # BR(H -> g       g      )
     2.38590052E-10    2          22        22   # BR(H -> gam     gam    )
     1.82154268E-09    2          23        22   # BR(H -> Z       gam    )
     1.82003302E-06    2          24       -24   # BR(H -> W+      W-     )
     9.09386075E-07    2          23        23   # BR(H -> Z       Z      )
     7.04152520E-06    2          25        25   # BR(H -> h       h      )
     8.25024811E-25    2          36        36   # BR(H -> A       A      )
     6.36442964E-20    2          23        36   # BR(H -> Z       A      )
     1.85312016E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.62241408E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.62241408E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.94947387E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03427922E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.05163732E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.82017194E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.83253626E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.01563015E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.42968560E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.94062052E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.31489200E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.09960728E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.02893343E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.02893343E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42050064E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50073915E+01   # A decays
#          BR         NDA      ID1       ID2
     3.51887290E-01    2           5        -5   # BR(A -> b       bb     )
     6.02758323E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13120576E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52126020E-04    2           3        -3   # BR(A -> s       sb     )
     7.11615625E-08    2           4        -4   # BR(A -> c       cb     )
     7.09964286E-03    2           6        -6   # BR(A -> t       tb     )
     1.45879738E-05    2          21        21   # BR(A -> g       g      )
     5.32366553E-08    2          22        22   # BR(A -> gam     gam    )
     1.60377897E-08    2          23        22   # BR(A -> Z       gam    )
     1.81633614E-06    2          23        25   # BR(A -> Z       h      )
     1.85679969E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.62249941E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.62249941E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.69937736E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29067592E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.68580164E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.49018897E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.47630728E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.02562557E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.03706756E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.03037767E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.21417430E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.09554899E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.09554899E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50398632E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.63622670E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02555673E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13048924E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.60718201E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20696262E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48311025E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.58990223E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.81728323E-06    2          24        25   # BR(H+ -> W+      h      )
     2.58397797E-14    2          24        36   # BR(H+ -> W+      A      )
     6.78665458E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.83040280E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.96692632E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.62363506E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.04433871E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59719262E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.25409538E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.10605388E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.11132113E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

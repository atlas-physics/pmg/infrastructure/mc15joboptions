#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10050370E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     2.00485961E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     5.09990000E+02   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194755E+01   # W+
        25     1.24950153E+02   # h
        35     4.00000261E+03   # H
        36     3.99999559E+03   # A
        37     4.00101736E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01667734E+03   # ~d_L
   2000001     4.01533519E+03   # ~d_R
   1000002     4.01602405E+03   # ~u_L
   2000002     4.01589957E+03   # ~u_R
   1000003     4.01667734E+03   # ~s_L
   2000003     4.01533519E+03   # ~s_R
   1000004     4.01602405E+03   # ~c_L
   2000004     4.01589957E+03   # ~c_R
   1000005     2.03035592E+03   # ~b_1
   2000005     4.02069460E+03   # ~b_2
   1000006     5.29143410E+02   # ~t_1
   2000006     2.04178973E+03   # ~t_2
   1000011     4.00247112E+03   # ~e_L
   2000011     4.00366376E+03   # ~e_R
   1000012     4.00135793E+03   # ~nu_eL
   1000013     4.00247112E+03   # ~mu_L
   2000013     4.00366376E+03   # ~mu_R
   1000014     4.00135793E+03   # ~nu_muL
   1000015     4.00452574E+03   # ~tau_1
   2000015     4.00837997E+03   # ~tau_2
   1000016     4.00362017E+03   # ~nu_tauL
   1000021     1.97452216E+03   # ~g
   1000022     1.44859415E+02   # ~chi_10
   1000023    -1.95922015E+02   # ~chi_20
   1000025     2.11301428E+02   # ~chi_30
   1000035     2.06649653E+03   # ~chi_40
   1000024     1.91944310E+02   # ~chi_1+
   1000037     2.06667270E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15258631E-01   # N_11
  1  2    -1.34770392E-02   # N_12
  1  3    -4.64189542E-01   # N_13
  1  4    -3.45976594E-01   # N_14
  2  1    -9.38427543E-02   # N_21
  2  2     2.64234081E-02   # N_22
  2  3    -6.95889700E-01   # N_23
  2  4     7.11500433E-01   # N_24
  3  1     5.71442072E-01   # N_31
  3  2     2.51883451E-02   # N_32
  3  3     5.47962407E-01   # N_33
  3  4     6.10374234E-01   # N_34
  4  1    -9.27464690E-04   # N_41
  4  2     9.99242573E-01   # N_42
  4  3    -1.67165550E-03   # N_43
  4  4    -3.88667620E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36878270E-03   # U_11
  1  2     9.99997194E-01   # U_12
  2  1    -9.99997194E-01   # U_21
  2  2     2.36878270E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49787808E-02   # V_11
  1  2    -9.98487523E-01   # V_12
  2  1    -9.98487523E-01   # V_21
  2  2    -5.49787808E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1    -8.97418793E-02   # cos(theta_t)
  1  2     9.95965057E-01   # sin(theta_t)
  2  1    -9.95965057E-01   # -sin(theta_t)
  2  2    -8.97418793E-02   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999711E-01   # cos(theta_b)
  1  2    -7.60263057E-04   # sin(theta_b)
  2  1     7.60263057E-04   # -sin(theta_b)
  2  2     9.99999711E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04570336E-01   # cos(theta_tau)
  1  2     7.09634160E-01   # sin(theta_tau)
  2  1    -7.09634160E-01   # -sin(theta_tau)
  2  2    -7.04570336E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00291549E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00503703E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44266797E+02   # higgs               
         4     1.61699946E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00503703E+03  # The gauge couplings
     1     3.61319852E-01   # gprime(Q) DRbar
     2     6.35817187E-01   # g(Q) DRbar
     3     1.03520423E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00503703E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.47188726E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00503703E+03  # The trilinear couplings
  1  1     2.74510079E-07   # A_d(Q) DRbar
  2  2     2.74536552E-07   # A_s(Q) DRbar
  3  3     4.90377581E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00503703E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.03931726E-08   # A_mu(Q) DRbar
  3  3     6.10104543E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00503703E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72221266E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00503703E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83731729E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00503703E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03691875E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00503703E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58373690E+07   # M^2_Hd              
        22    -4.53733531E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     2.00485961E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     5.09989996E+02   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40242506E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.56140472E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.13449699E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.83114424E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.92992887E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.03640818E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     5.25054853E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     6.05630341E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.70724392E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.41931745E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.68742656E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.46850293E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.81729537E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.85838594E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     3.08735256E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.70435093E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.49313237E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.91452941E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.18137759E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.60227240E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
    -3.52806101E-02    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     1.63812997E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.68514192E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76734226E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.53169505E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.64724235E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61464989E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.17150477E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13885196E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.77475101E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.54345667E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.16389110E-06    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.16853704E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.75332231E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75773172E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.60240349E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.57098497E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81435045E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33884567E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61636431E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51823649E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.58238902E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.74043257E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.94633462E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.83279801E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.63909858E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.43773034E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.75350690E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.49441043E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.72510129E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.08725926E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81914069E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.91138830E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64827527E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52049203E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.51517857E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.76447179E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29124955E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.78455119E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.88804905E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85321845E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.75332231E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75773172E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.60240349E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.57098497E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81435045E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33884567E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61636431E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51823649E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.58238902E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.74043257E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.94633462E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.83279801E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.63909858E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.43773034E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.75350690E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.49441043E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.72510129E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.08725926E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81914069E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.91138830E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64827527E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52049203E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.51517857E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.76447179E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29124955E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.78455119E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.88804905E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85321845E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.11686749E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04208745E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.72017589E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.28783502E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77060276E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.76839624E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.55474843E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07222016E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65301451E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.79598491E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25902099E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.64584179E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.11686749E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04208745E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.72017589E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.28783502E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77060276E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.76839624E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.55474843E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07222016E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65301451E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.79598491E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25902099E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.64584179E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08959632E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52083105E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.16754235E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.35000711E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38714697E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44187996E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78107263E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09540378E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.45184024E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.76637463E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.10794557E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41210965E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.20341671E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.83112541E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.11797212E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16997645E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.26661807E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.60478164E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77357654E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.10541067E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.53224856E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.11797212E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16997645E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.26661807E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.60478164E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77357654E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.10541067E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.53224856E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45081971E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05772758E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95322233E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.16300765E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.50953030E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.81338707E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.00557042E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.24584654E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33589268E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33589268E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11197472E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11197472E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10426520E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.69548478E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87141670E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.18471286E-03    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     2.44934085E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.14417831E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.40304588E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.85380279E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.38040386E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.08706993E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.20548587E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18152898E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53233800E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18152898E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53233800E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41106457E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51207537E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51207537E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47889757E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02152964E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02152964E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02152927E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.15802462E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.15802462E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.15802462E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.15802462E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.38601058E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.38601058E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.38601058E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.38601058E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.26971373E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.26971373E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.35423964E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.01599808E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.77899411E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.30066333E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.44389462E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.30066333E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.44389462E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.00597712E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.14520829E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.14520829E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.13129456E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.33665903E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.33665903E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.33665017E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.49785573E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     5.83369509E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.49785573E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     5.83369509E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.14201434E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.33732010E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.33732010E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.23873907E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.67289621E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.67289621E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.67289628E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.82257527E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.82257527E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.82257527E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.82257527E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.60750434E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.60750434E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.60750434E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.60750434E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.54209929E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.54209929E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.69663064E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.63441294E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.34327318E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.39655363E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.38655612E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.38655612E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.55758854E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.08629471E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.03068965E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.91279655E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.91279655E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.26076969E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.26076969E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.97736092E-03   # h decays
#          BR         NDA      ID1       ID2
     6.05274282E-01    2           5        -5   # BR(h -> b       bb     )
     6.52060431E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30831548E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.89976995E-04    2           3        -3   # BR(h -> s       sb     )
     2.12783576E-02    2           4        -4   # BR(h -> c       cb     )
     6.87145012E-02    2          21        21   # BR(h -> g       g      )
     2.36277395E-03    2          22        22   # BR(h -> gam     gam    )
     1.56516139E-03    2          22        23   # BR(h -> Z       gam    )
     2.08726815E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61512571E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43377348E+01   # H decays
#          BR         NDA      ID1       ID2
     3.44687905E-01    2           5        -5   # BR(H -> b       bb     )
     6.10182921E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15745902E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55190238E-04    2           3        -3   # BR(H -> s       sb     )
     7.15828027E-08    2           4        -4   # BR(H -> c       cb     )
     7.17408710E-03    2           6        -6   # BR(H -> t       tb     )
     9.15449925E-07    2          21        21   # BR(H -> g       g      )
     2.40053166E-12    2          22        22   # BR(H -> gam     gam    )
     1.84371450E-09    2          23        22   # BR(H -> Z       gam    )
     1.93054762E-06    2          24       -24   # BR(H -> W+      W-     )
     9.64605190E-07    2          23        23   # BR(H -> Z       Z      )
     7.09328130E-06    2          25        25   # BR(H -> h       h      )
     3.14346208E-25    2          36        36   # BR(H -> A       A      )
    -1.62053343E-20    2          23        36   # BR(H -> Z       A      )
     1.86625697E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64840185E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64840185E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.89404738E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.57099487E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.61135622E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09471131E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.80797482E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.64058622E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.63094828E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.74112340E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.14887484E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.08706245E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.93511643E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     4.93511643E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
#
#         PDG            Width
DECAY        36     5.43356568E+01   # A decays
#          BR         NDA      ID1       ID2
     3.44727061E-01    2           5        -5   # BR(A -> b       bb     )
     6.10209988E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15755302E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55242958E-04    2           3        -3   # BR(A -> s       sb     )
     7.20413056E-08    2           4        -4   # BR(A -> c       cb     )
     7.18741303E-03    2           6        -6   # BR(A -> t       tb     )
     1.47683208E-05    2          21        21   # BR(A -> g       g      )
     4.71272091E-08    2          22        22   # BR(A -> gam     gam    )
     1.62407929E-08    2          23        22   # BR(A -> Z       gam    )
     1.92662365E-06    2          23        25   # BR(A -> Z       h      )
     1.86753897E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64850118E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64850118E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.50608423E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94685097E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30367148E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67166097E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.56731984E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.49948000E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.83633131E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.26281423E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.76652094E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.14560871E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.14560871E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42045410E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.49074210E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11842280E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16332441E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.51407192E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22556415E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52137957E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.50011578E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.93344046E-06    2          24        25   # BR(H+ -> W+      h      )
     2.70379571E-14    2          24        36   # BR(H+ -> W+      A      )
     5.58755005E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46943831E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.34907799E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65461406E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.16582730E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60565922E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     9.95214307E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.04869010E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14500115E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03981009E+01   # W+
        25     1.24199504E+02   # h
        35     3.00014987E+03   # H
        36     2.99999977E+03   # A
        37     3.00092643E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03352567E+03   # ~d_L
   2000001     3.02833407E+03   # ~d_R
   1000002     3.03260829E+03   # ~u_L
   2000002     3.02978892E+03   # ~u_R
   1000003     3.03352567E+03   # ~s_L
   2000003     3.02833407E+03   # ~s_R
   1000004     3.03260829E+03   # ~c_L
   2000004     3.02978892E+03   # ~c_R
   1000005     7.90297174E+02   # ~b_1
   2000005     3.02759151E+03   # ~b_2
   1000006     7.88464862E+02   # ~t_1
   2000006     3.01455372E+03   # ~t_2
   1000011     3.00648969E+03   # ~e_L
   2000011     3.00169542E+03   # ~e_R
   1000012     3.00509239E+03   # ~nu_eL
   1000013     3.00648969E+03   # ~mu_L
   2000013     3.00169542E+03   # ~mu_R
   1000014     3.00509239E+03   # ~nu_muL
   1000015     2.98581215E+03   # ~tau_1
   2000015     3.02203097E+03   # ~tau_2
   1000016     3.00500483E+03   # ~nu_tauL
   1000021     2.34125010E+03   # ~g
   1000022     1.50764297E+02   # ~chi_10
   1000023     3.20376244E+02   # ~chi_20
   1000025    -2.99838479E+03   # ~chi_30
   1000035     2.99894494E+03   # ~chi_40
   1000024     3.20538132E+02   # ~chi_1+
   1000037     2.99959355E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888311E-01   # N_11
  1  2    -1.15268140E-03   # N_12
  1  3     1.48274132E-02   # N_13
  1  4    -1.47807213E-03   # N_14
  2  1     1.54887501E-03   # N_21
  2  2     9.99643999E-01   # N_22
  2  3    -2.63448264E-02   # N_23
  2  4     3.92764913E-03   # N_24
  3  1    -9.41821032E-03   # N_31
  3  2     1.58644981E-02   # N_32
  3  3     7.06841822E-01   # N_33
  3  4     7.07131002E-01   # N_34
  4  1    -1.15006023E-02   # N_41
  4  2     2.14211192E-02   # N_42
  4  3     7.06725362E-01   # N_43
  4  4    -7.07070106E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99304955E-01   # U_11
  1  2    -3.72774306E-02   # U_12
  2  1     3.72774306E-02   # U_21
  2  2     9.99304955E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984550E-01   # V_11
  1  2    -5.55878809E-03   # V_12
  2  1     5.55878809E-03   # V_21
  2  2     9.99984550E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98841713E-01   # cos(theta_t)
  1  2    -4.81168616E-02   # sin(theta_t)
  2  1     4.81168616E-02   # -sin(theta_t)
  2  2     9.98841713E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99916060E-01   # cos(theta_b)
  1  2     1.29565796E-02   # sin(theta_b)
  2  1    -1.29565796E-02   # -sin(theta_b)
  2  2     9.99916060E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06922618E-01   # cos(theta_tau)
  1  2     7.07290896E-01   # sin(theta_tau)
  2  1    -7.07290896E-01   # -sin(theta_tau)
  2  2     7.06922618E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01845152E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.45001149E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44330234E+02   # higgs               
         4     7.34659324E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.45001149E+03  # The gauge couplings
     1     3.62059852E-01   # gprime(Q) DRbar
     2     6.38903661E-01   # g(Q) DRbar
     3     1.02712178E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.45001149E+03  # The trilinear couplings
  1  1     1.97961119E-06   # A_u(Q) DRbar
  2  2     1.97964169E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.45001149E+03  # The trilinear couplings
  1  1     5.03129329E-07   # A_d(Q) DRbar
  2  2     5.03227167E-07   # A_s(Q) DRbar
  3  3     1.14821421E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.45001149E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09512153E-07   # A_mu(Q) DRbar
  3  3     1.10833317E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.45001149E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52644944E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.45001149E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12989153E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.45001149E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05664797E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.45001149E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.41077306E+04   # M^2_Hd              
        22    -9.07064174E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41701690E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.42422151E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48234256E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48234256E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51765744E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51765744E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.72762940E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.46537409E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.99988594E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.85357665E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.02708641E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.40067144E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.08437738E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.19352875E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.34291675E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.28384555E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.58415335E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50512366E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.97709810E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.49264630E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.72216955E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.57232753E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.25545552E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.22357275E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.93186583E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.54653469E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.25602802E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.22746218E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.80367653E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29016452E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.90530921E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.20604436E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.05263559E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02461685E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.85107649E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63348850E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.93570341E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.50083813E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.26721980E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.62259240E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.04078034E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18333831E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.58983957E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.75351520E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.02599834E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.01161298E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.41015650E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02969583E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.99321859E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63092693E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.19416021E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.53617542E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26148002E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.66418721E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.04765733E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67274062E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.52490604E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.06828566E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.82524542E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.62348408E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54750828E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02461685E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.85107649E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63348850E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.93570341E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.50083813E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.26721980E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.62259240E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.04078034E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18333831E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.58983957E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.75351520E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.02599834E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.01161298E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.41015650E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02969583E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.99321859E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63092693E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.19416021E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.53617542E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26148002E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.66418721E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.04765733E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67274062E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.52490604E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.06828566E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.82524542E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.62348408E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54750828E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96584573E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79336778E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01181493E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.88992646E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.71803315E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00884808E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.79627448E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55739064E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997642E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35706315E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.33406704E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.46145793E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96584573E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79336778E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01181493E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.88992646E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.71803315E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00884808E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.79627448E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55739064E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997642E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35706315E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.33406704E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.46145793E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79027668E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48497108E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17531820E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33971071E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73089253E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56752798E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14788079E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.11274042E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.25917929E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28422738E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.26662135E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96615108E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.86798833E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99956554E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.74985279E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.01150745E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01363555E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.54179872E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96615108E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.86798833E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99956554E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.74985279E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.01150745E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01363555E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.54179872E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96636816E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.86715785E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99930996E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.67853845E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.89806073E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01396631E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.87577354E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.62762202E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.50691135E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.68608115E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.20105554E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.86972935E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.01163069E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.17465236E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.84410696E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.27292472E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.89972558E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.90305555E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50969444E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.53281312E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.34312305E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.50561700E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.81111148E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.81111148E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.27849222E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.51609789E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.62926046E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.62926046E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.24989923E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.24989923E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.11143794E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.11143794E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.44521225E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.10154937E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.49927353E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.88404275E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.88404275E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.40146912E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.77340094E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.60475986E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.60475986E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27615070E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27615070E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.86796344E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.86796344E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.74740032E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85249556E-01    2           5        -5   # BR(h -> b       bb     )
     5.46379690E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93423054E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.11020740E-04    2           3        -3   # BR(h -> s       sb     )
     1.77404554E-02    2           4        -4   # BR(h -> c       cb     )
     5.71264310E-02    2          21        21   # BR(h -> g       g      )
     1.92882404E-03    2          22        22   # BR(h -> gam     gam    )
     1.22427918E-03    2          22        23   # BR(h -> Z       gam    )
     1.61558402E-01    2          24       -24   # BR(h -> W+      W-     )
     1.99296400E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39824524E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44801938E-01    2           5        -5   # BR(H -> b       bb     )
     1.77849945E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28834904E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72208786E-04    2           3        -3   # BR(H -> s       sb     )
     2.17923494E-07    2           4        -4   # BR(H -> c       cb     )
     2.17392749E-02    2           6        -6   # BR(H -> t       tb     )
     2.97191991E-05    2          21        21   # BR(H -> g       g      )
     1.97455526E-08    2          22        22   # BR(H -> gam     gam    )
     8.35790967E-09    2          23        22   # BR(H -> Z       gam    )
     3.22655508E-05    2          24       -24   # BR(H -> W+      W-     )
     1.61128796E-05    2          23        23   # BR(H -> Z       Z      )
     8.45230193E-05    2          25        25   # BR(H -> h       h      )
    -1.80014552E-23    2          36        36   # BR(H -> A       A      )
     5.86996512E-18    2          23        36   # BR(H -> Z       A      )
     2.31746774E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11943847E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.15484724E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.22124360E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.97325700E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.97877457E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32911626E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83609580E-01    2           5        -5   # BR(A -> b       bb     )
     1.87095282E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.61523283E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.12487964E-04    2           3        -3   # BR(A -> s       sb     )
     2.29272587E-07    2           4        -4   # BR(A -> c       cb     )
     2.28588243E-02    2           6        -6   # BR(A -> t       tb     )
     6.73170548E-05    2          21        21   # BR(A -> g       g      )
     3.13275973E-08    2          22        22   # BR(A -> gam     gam    )
     6.62789451E-08    2          23        22   # BR(A -> Z       gam    )
     3.38158316E-05    2          23        25   # BR(A -> Z       h      )
     2.63203253E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21365714E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.31155663E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.95888131E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01200084E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09898539E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.45798406E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.69083210E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.03349547E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.10741610E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05075973E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.15343307E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.44811609E-05    2          24        25   # BR(H+ -> W+      h      )
     8.88404008E-14    2          24        36   # BR(H+ -> W+      A      )
     2.03719170E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.86898726E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.36996737E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

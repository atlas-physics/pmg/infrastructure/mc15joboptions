#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10888790E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     3.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04003326E+01   # W+
        25     1.26772356E+02   # h
        35     3.00020301E+03   # H
        36     3.00000019E+03   # A
        37     3.00111522E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.01700230E+03   # ~d_L
   2000001     3.01130094E+03   # ~d_R
   1000002     3.01607326E+03   # ~u_L
   2000002     3.01523663E+03   # ~u_R
   1000003     3.01700230E+03   # ~s_L
   2000003     3.01130094E+03   # ~s_R
   1000004     3.01607326E+03   # ~c_L
   2000004     3.01523663E+03   # ~c_R
   1000005     4.52418257E+02   # ~b_1
   2000005     3.01403664E+03   # ~b_2
   1000006     4.47374829E+02   # ~t_1
   2000006     3.01799354E+03   # ~t_2
   1000011     3.00779617E+03   # ~e_L
   2000011     2.99920019E+03   # ~e_R
   1000012     3.00639214E+03   # ~nu_eL
   1000013     3.00779617E+03   # ~mu_L
   2000013     2.99920019E+03   # ~mu_R
   1000014     3.00639214E+03   # ~nu_muL
   1000015     2.98636563E+03   # ~tau_1
   2000015     3.02279738E+03   # ~tau_2
   1000016     3.00713073E+03   # ~nu_tauL
   1000021     2.32055514E+03   # ~g
   1000022     5.08607140E+01   # ~chi_10
   1000023     1.10240997E+02   # ~chi_20
   1000025    -3.01046253E+03   # ~chi_30
   1000035     3.01065052E+03   # ~chi_40
   1000024     1.10391691E+02   # ~chi_1+
   1000037     3.01149924E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99890726E-01   # N_11
  1  2     1.91638218E-03   # N_12
  1  3    -1.46500449E-02   # N_13
  1  4     4.88967007E-04   # N_14
  2  1    -1.53302888E-03   # N_21
  2  2     9.99657558E-01   # N_22
  2  3     2.61194769E-02   # N_23
  2  4    -4.35733062E-04   # N_24
  3  1     1.00446383E-02   # N_31
  3  2    -1.81437249E-02   # N_32
  3  3     7.06791530E-01   # N_33
  3  4     7.07117842E-01   # N_34
  4  1    -1.07373409E-02   # N_41
  4  2     1.87589936E-02   # N_42
  4  3    -7.06787720E-01   # N_43
  4  4     7.07095416E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99319181E-01   # U_11
  1  2     3.68941101E-02   # U_12
  2  1    -3.68941101E-02   # U_21
  2  2     9.99319181E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999810E-01   # V_11
  1  2     6.16739152E-04   # V_12
  2  1     6.16739152E-04   # V_21
  2  2    -9.99999810E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98656962E-01   # cos(theta_t)
  1  2    -5.18099628E-02   # sin(theta_t)
  2  1     5.18099628E-02   # -sin(theta_t)
  2  2     9.98656962E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99706617E-01   # cos(theta_b)
  1  2    -2.42214766E-02   # sin(theta_b)
  2  1     2.42214766E-02   # -sin(theta_b)
  2  2     9.99706617E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06857716E-01   # cos(theta_tau)
  1  2     7.07355759E-01   # sin(theta_tau)
  2  1    -7.07355759E-01   # -sin(theta_tau)
  2  2    -7.06857716E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00218378E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.08887896E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44762492E+02   # higgs               
         4     1.19187324E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.08887896E+03  # The gauge couplings
     1     3.61041269E-01   # gprime(Q) DRbar
     2     6.41103887E-01   # g(Q) DRbar
     3     1.03349869E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.08887896E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.06249214E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.08887896E+03  # The trilinear couplings
  1  1     3.78366676E-07   # A_d(Q) DRbar
  2  2     3.78412963E-07   # A_s(Q) DRbar
  3  3     7.97175409E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.08887896E+03  # The trilinear couplings
  1  1     1.88619875E-07   # A_e(Q) DRbar
  2  2     1.88629346E-07   # A_mu(Q) DRbar
  3  3     1.91347422E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.08887896E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.60148730E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.08887896E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.13138672E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.08887896E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03614004E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.08887896E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -7.65808213E+04   # M^2_Hd              
        22    -9.19017107E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42669424E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.52649812E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48067902E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48067902E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51932098E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51932098E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.67816477E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.06481415E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.57027049E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.32324810E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.25547235E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.22655834E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.17392482E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.37849618E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.22721388E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.84041039E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72140449E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.65065266E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.25827194E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     4.27753735E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.57281573E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.07197981E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.77073862E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.88262598E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.79989785E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
    -8.56092910E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.71774675E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.04683527E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     8.06843463E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.16261258E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.70372148E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.12204304E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.91396380E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.64020618E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.59110131E-10    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.53236978E-10    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.28452997E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.46563973E-12    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.01612420E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.22593647E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56218627E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.66638479E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.00877987E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.12207231E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43781006E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12713842E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.67503933E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.64144484E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.55254899E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.57774543E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.27875977E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.52013215E-09    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.02304489E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69891872E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45590908E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.04577805E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.91119670E-12    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.32824099E-12    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55440805E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.12204304E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.91396380E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.64020618E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.59110131E-10    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.53236978E-10    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.28452997E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.46563973E-12    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.01612420E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.22593647E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56218627E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.66638479E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.00877987E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.12207231E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43781006E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12713842E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.67503933E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.64144484E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.55254899E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.57774543E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.27875977E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.52013215E-09    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.02304489E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69891872E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45590908E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.04577805E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.91119670E-12    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.32824099E-12    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55440805E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.06528179E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.65115589E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00953332E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.02535109E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55430258E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997654E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.34568713E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     4.06528179E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.65115589E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00953332E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.02535109E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55430258E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997654E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.34568713E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.83871674E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42952215E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.18895418E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.38152367E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.77952834E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50965480E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.16200156E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.62103556E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.62916524E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32825009E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.10505658E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.06564022E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.51537398E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.01826439E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.03019821E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     4.06564022E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.51537398E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.01826439E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.03019821E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     4.06698150E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.51457541E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.01801423E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.03052823E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     7.13639243E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.39950082E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.77119653E-08    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.39950082E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.77119653E-08    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.06854735E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.90401488E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.06854735E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.90401488E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.06390320E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.02592401E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.52714395E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54398852E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.24244583E-13    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     5.24244583E-13    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     9.26863055E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     9.86785452E-10    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     9.86785452E-10    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.30552049E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.83702474E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.82958901E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.69450118E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.92742041E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.73808645E-09   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.83837356E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     5.93109831E-03    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     7.36408162E-03    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     5.93109831E-03    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     7.36408162E-03    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     9.67304794E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     6.44509955E-04    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     6.44509955E-04    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     6.42972892E-04    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     1.11394748E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     1.11394748E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     1.11690082E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     1.03108153E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.80118108E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.00574929E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.67680380E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.67680380E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.14608235E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.48363846E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.31609880E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.31609880E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.51999928E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.51999928E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.91764124E-11    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     2.91764124E-11    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.26066340E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.26066340E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.91764124E-11    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     2.91764124E-11    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     4.26066340E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.26066340E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.51750537E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.51750537E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     2.46896313E-10    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     2.46896313E-10    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     2.46896313E-10    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     2.46896313E-10    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.65465335E-10    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.65465335E-10    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.02478274E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.63114945E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.83054733E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.71185911E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.71185911E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.07123710E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.53510484E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.25707179E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.25707179E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.56728740E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.56728740E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.48609182E-11    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     3.48609182E-11    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     5.06276603E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.06276603E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.48609182E-11    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     3.48609182E-11    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     5.06276603E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.06276603E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.33325664E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.33325664E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     2.95307982E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.95307982E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.95307982E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.95307982E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.01802470E-10    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.01802470E-10    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.88690990E-03   # h decays
#          BR         NDA      ID1       ID2
     5.37763499E-01    2           5        -5   # BR(h -> b       bb     )
     6.76790503E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.39577801E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.07114129E-04    2           3        -3   # BR(h -> s       sb     )
     2.20288046E-02    2           4        -4   # BR(h -> c       cb     )
     7.31541449E-02    2          21        21   # BR(h -> g       g      )
     2.56756593E-03    2          22        22   # BR(h -> gam     gam    )
     1.87762650E-03    2          22        23   # BR(h -> Z       gam    )
     2.60678060E-01    2          24       -24   # BR(h -> W+      W-     )
     3.34796368E-02    2          23        23   # BR(h -> Z       Z      )
     2.49197564E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     4.39783642E+01   # H decays
#          BR         NDA      ID1       ID2
     9.14342595E-01    2           5        -5   # BR(H -> b       bb     )
     5.65474196E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.99938163E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.45523387E-04    2           3        -3   # BR(H -> s       sb     )
     6.88394182E-08    2           4        -4   # BR(H -> c       cb     )
     6.86717890E-03    2           6        -6   # BR(H -> t       tb     )
     2.77460899E-06    2          21        21   # BR(H -> g       g      )
     3.29727910E-08    2          22        22   # BR(H -> gam     gam    )
     2.80771469E-09    2          23        22   # BR(H -> Z       gam    )
     8.07579451E-07    2          24       -24   # BR(H -> W+      W-     )
     4.03291865E-07    2          23        23   # BR(H -> Z       Z      )
     4.74305157E-06    2          25        25   # BR(H -> h       h      )
     1.69094983E-25    2          36        36   # BR(H -> A       A      )
     8.46065008E-18    2          23        36   # BR(H -> Z       A      )
     7.78285299E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.50437273E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.90462657E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.34192631E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.03451808E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.34635176E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.30855753E+01   # A decays
#          BR         NDA      ID1       ID2
     9.33276027E-01    2           5        -5   # BR(A -> b       bb     )
     5.77157008E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.04068640E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50638663E-04    2           3        -3   # BR(A -> s       sb     )
     7.07266780E-08    2           4        -4   # BR(A -> c       cb     )
     7.05155697E-03    2           6        -6   # BR(A -> t       tb     )
     2.07661584E-05    2          21        21   # BR(A -> g       g      )
     4.26266238E-08    2          22        22   # BR(A -> gam     gam    )
     2.04537854E-08    2          23        22   # BR(A -> Z       gam    )
     8.20987014E-07    2          23        25   # BR(A -> Z       h      )
     8.01363252E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.60488364E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.02032970E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.40841629E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.64804747E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48652615E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.35200736E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.89233926E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.51375576E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.11207824E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.28790254E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.34143184E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.62348988E-07    2          24        25   # BR(H+ -> W+      h      )
     4.87898668E-14    2          24        36   # BR(H+ -> W+      A      )
     4.47052586E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.05006043E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     9.96374123E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90842940E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.28900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     3.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.30485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04163157E+01   # W+
        25     1.24634480E+02   # h
        35     3.99999959E+03   # H
        36     3.99999508E+03   # A
        37     4.00102244E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01281817E+03   # ~d_L
   2000001     4.01202981E+03   # ~d_R
   1000002     4.01215156E+03   # ~u_L
   2000002     4.01387501E+03   # ~u_R
   1000003     4.01281817E+03   # ~s_L
   2000003     4.01202981E+03   # ~s_R
   1000004     4.01215156E+03   # ~c_L
   2000004     4.01387501E+03   # ~c_R
   1000005     4.00773110E+02   # ~b_1
   2000005     4.01790102E+03   # ~b_2
   1000006     3.91630388E+02   # ~t_1
   2000006     2.32363455E+03   # ~t_2
   1000011     4.00234336E+03   # ~e_L
   2000011     4.00238248E+03   # ~e_R
   1000012     4.00121685E+03   # ~nu_eL
   1000013     4.00234336E+03   # ~mu_L
   2000013     4.00238248E+03   # ~mu_R
   1000014     4.00121685E+03   # ~nu_muL
   1000015     4.00479461E+03   # ~tau_1
   2000015     4.00768015E+03   # ~tau_2
   1000016     4.00380377E+03   # ~nu_tauL
   1000021     1.96130025E+03   # ~g
   1000022     9.46526539E+01   # ~chi_10
   1000023    -1.38560899E+02   # ~chi_20
   1000025     1.53648704E+02   # ~chi_30
   1000035     2.05822600E+03   # ~chi_40
   1000024     1.33651414E+02   # ~chi_1+
   1000037     2.05839049E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.72480681E-01   # N_11
  1  2     1.34903213E-02   # N_12
  1  3     5.24814966E-01   # N_13
  1  4     3.57296601E-01   # N_14
  2  1    -1.34796275E-01   # N_21
  2  2     2.72912279E-02   # N_22
  2  3    -6.85718536E-01   # N_23
  2  4     7.14755372E-01   # N_24
  3  1     6.20566471E-01   # N_31
  3  2     2.41767741E-02   # N_32
  3  3     5.04340190E-01   # N_33
  3  4     5.99961425E-01   # N_34
  4  1    -9.04210423E-04   # N_41
  4  2     9.99244056E-01   # N_42
  4  3    -5.59563497E-04   # N_43
  4  4    -3.88611064E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.95701808E-04   # U_11
  1  2     9.99999683E-01   # U_12
  2  1    -9.99999683E-01   # U_21
  2  2     7.95701808E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49715233E-02   # V_11
  1  2    -9.98487923E-01   # V_12
  2  1    -9.98487923E-01   # V_21
  2  2    -5.49715233E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97853743E-01   # cos(theta_t)
  1  2    -6.54821165E-02   # sin(theta_t)
  2  1     6.54821165E-02   # -sin(theta_t)
  2  2     9.97853743E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999922E-01   # cos(theta_b)
  1  2    -3.94968345E-04   # sin(theta_b)
  2  1     3.94968345E-04   # -sin(theta_b)
  2  2     9.99999922E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.02778379E-01   # cos(theta_tau)
  1  2     7.11408849E-01   # sin(theta_tau)
  2  1    -7.11408849E-01   # -sin(theta_tau)
  2  2    -7.02778379E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00305344E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.08429403E+02  # DRbar Higgs Parameters
         1    -1.28900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44567756E+02   # higgs               
         4     1.61933853E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.08429403E+02  # The gauge couplings
     1     3.60909489E-01   # gprime(Q) DRbar
     2     6.36908816E-01   # g(Q) DRbar
     3     1.03841967E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.08429403E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.87736234E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.08429403E+02  # The trilinear couplings
  1  1     2.15688483E-07   # A_d(Q) DRbar
  2  2     2.15709207E-07   # A_s(Q) DRbar
  3  3     3.85404377E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.08429403E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.66659959E-08   # A_mu(Q) DRbar
  3  3     4.71328424E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.08429403E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72986004E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.08429403E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81567355E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.08429403E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03756854E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.08429403E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58486051E+07   # M^2_Hd              
        22    -9.73633896E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     3.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.30485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40738332E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.49421312E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46883633E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46883633E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53116367E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53116367E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.93938988E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.55802095E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.12891469E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.68740448E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.62565988E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.38864154E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.41158869E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23979422E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.72993276E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.71489184E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.40404846E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.04868194E-06    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.00658855E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.10752358E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.69868593E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.85801125E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.38533309E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.72247427E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     6.86370297E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.72118930E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.36926335E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66318829E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.76712037E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.67800359E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40891365E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.04931048E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.50708741E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.92856131E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.16315434E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.84497745E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.02647887E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.45249286E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
    -3.58520046E-09    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78103846E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.53274695E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.92439796E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.74474982E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77110575E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.27784631E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.52995332E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53183835E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60987872E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.29553344E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.00238445E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.12346055E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.47067392E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44807651E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78124060E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.22109605E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.42994378E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.55503694E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77575128E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.10587604E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.56160116E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53406771E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54165200E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.59940712E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.61563226E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.54097725E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.44284854E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85598046E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78103846E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.53274695E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.92439796E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.74474982E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77110575E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.27784631E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.52995332E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53183835E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60987872E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.29553344E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.00238445E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.12346055E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.47067392E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44807651E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78124060E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.22109605E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.42994378E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.55503694E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77575128E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.10587604E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.56160116E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53406771E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54165200E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.59940712E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.61563226E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.54097725E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.44284854E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85598046E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14047477E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.24517650E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.23639252E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.24211989E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77514569E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.49733525E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56375425E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07048996E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.97161138E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.81600208E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.84678398E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.43161229E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14047477E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.24517650E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.23639252E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.24211989E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77514569E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.49733525E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56375425E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07048996E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.97161138E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.81600208E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.84678398E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.43161229E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09958880E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.33445305E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00929448E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.54610431E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39007831E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41542613E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78689227E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11062836E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.17526380E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.32099436E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.29403511E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42330560E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.21770874E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85352518E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14155721E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.04519531E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.51067026E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.49501497E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77797762E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09911480E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54122772E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14155721E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.04519531E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.51067026E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.49501497E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77797762E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09911480E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54122772E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47598122E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.45249223E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.98372442E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.96956968E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51466787E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77288442E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01600025E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.41001306E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33712330E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33712330E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11238211E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11238211E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10098917E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.85904106E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.85384878E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.76536199E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.06342543E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.37402414E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.99730029E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.45896960E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.91417281E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.29977858E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.76862679E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18453402E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53552592E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18453402E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53552592E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.38235029E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51541942E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51541942E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47240770E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02706907E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02706907E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02706857E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.45760596E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.45760596E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.45760596E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.45760596E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.19204000E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.19204000E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.19204000E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.19204000E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.37562081E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.37562081E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     8.96934497E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.99259612E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.85581590E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.94233091E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.68389126E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.94233091E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.68389126E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.15373424E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.74398303E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.74398303E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.72630306E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.52379254E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.52379254E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.52378441E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.38062385E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.97560311E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.38062385E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.97560311E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.46617597E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.59743573E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.59743573E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.47462046E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.19190235E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.19190235E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.19190249E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     7.50493579E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     7.50493579E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     7.50493579E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     7.50493579E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.50160521E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.50160521E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.50160521E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.50160521E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.40590908E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.40590908E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.85800124E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.48461039E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.18543111E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.12801613E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.92180627E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.92180627E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.43193878E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.87904412E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.37593428E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89660364E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89660364E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.91321171E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.91321171E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.94269625E-03   # h decays
#          BR         NDA      ID1       ID2
     6.08907910E-01    2           5        -5   # BR(h -> b       bb     )
     6.56163695E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.32285542E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.93306029E-04    2           3        -3   # BR(h -> s       sb     )
     2.14217857E-02    2           4        -4   # BR(h -> c       cb     )
     7.05999040E-02    2          21        21   # BR(h -> g       g      )
     2.34568911E-03    2          22        22   # BR(h -> gam     gam    )
     1.53454936E-03    2          22        23   # BR(h -> Z       gam    )
     2.03492905E-01    2          24       -24   # BR(h -> W+      W-     )
     2.53552958E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43725400E+01   # H decays
#          BR         NDA      ID1       ID2
     3.39586114E-01    2           5        -5   # BR(H -> b       bb     )
     6.09791783E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15607605E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55026681E-04    2           3        -3   # BR(H -> s       sb     )
     7.15408754E-08    2           4        -4   # BR(H -> c       cb     )
     7.16988506E-03    2           6        -6   # BR(H -> t       tb     )
     1.02970919E-06    2          21        21   # BR(H -> g       g      )
     2.04244479E-10    2          22        22   # BR(H -> gam     gam    )
     1.84004723E-09    2          23        22   # BR(H -> Z       gam    )
     2.00526754E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00193908E-06    2          23        23   # BR(H -> Z       Z      )
     7.32072589E-06    2          25        25   # BR(H -> h       h      )
    -2.68359084E-24    2          36        36   # BR(H -> A       A      )
     4.39914549E-20    2          23        36   # BR(H -> Z       A      )
     1.87248377E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66776567E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66776567E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.32562709E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.57874808E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.66806194E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.54946341E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.15092243E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.76308177E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.98829474E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.42575009E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.53460618E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.38036565E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.23343770E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.23343770E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.44016760E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43735542E+01   # A decays
#          BR         NDA      ID1       ID2
     3.39605779E-01    2           5        -5   # BR(A -> b       bb     )
     6.09784604E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15604897E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55065030E-04    2           3        -3   # BR(A -> s       sb     )
     7.19910861E-08    2           4        -4   # BR(A -> c       cb     )
     7.18240273E-03    2           6        -6   # BR(A -> t       tb     )
     1.47580283E-05    2          21        21   # BR(A -> g       g      )
     4.09709016E-08    2          22        22   # BR(A -> gam     gam    )
     1.62205475E-08    2          23        22   # BR(A -> Z       gam    )
     2.00111087E-06    2          23        25   # BR(A -> Z       h      )
     1.87542670E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66775675E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66775675E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.90833034E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.19286922E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.32208024E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.05147566E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.67919401E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.46897737E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.23632730E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.29291440E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.96115450E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.28183930E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.28183930E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.42593169E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.41203501E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.11225389E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16114323E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.46369941E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22432828E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51883698E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.45088788E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.00757176E-06    2          24        25   # BR(H+ -> W+      h      )
     2.77568008E-14    2          24        36   # BR(H+ -> W+      A      )
     4.99360418E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.09762452E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.92875513E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67343054E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.26128595E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57681836E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.52285344E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.91420989E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.29748648E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11090043E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.55959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -4.38900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     6.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.05485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04194073E+01   # W+
        25     1.26002814E+02   # h
        35     4.00001000E+03   # H
        36     3.99999692E+03   # A
        37     4.00098996E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02015384E+03   # ~d_L
   2000001     4.01795210E+03   # ~d_R
   1000002     4.01950152E+03   # ~u_L
   2000002     4.01909122E+03   # ~u_R
   1000003     4.02015384E+03   # ~s_L
   2000003     4.01795210E+03   # ~s_R
   1000004     4.01950152E+03   # ~c_L
   2000004     4.01909122E+03   # ~c_R
   1000005     6.70238665E+02   # ~b_1
   2000005     4.02231783E+03   # ~b_2
   1000006     6.52307425E+02   # ~t_1
   2000006     2.07196620E+03   # ~t_2
   1000011     4.00350794E+03   # ~e_L
   2000011     4.00297001E+03   # ~e_R
   1000012     4.00239604E+03   # ~nu_eL
   1000013     4.00350794E+03   # ~mu_L
   2000013     4.00297001E+03   # ~mu_R
   1000014     4.00239604E+03   # ~nu_muL
   1000015     4.00407418E+03   # ~tau_1
   2000015     4.00819108E+03   # ~tau_2
   1000016     4.00432956E+03   # ~nu_tauL
   1000021     1.97019607E+03   # ~g
   1000022     3.99318787E+02   # ~chi_10
   1000023    -4.49659429E+02   # ~chi_20
   1000025     4.64163120E+02   # ~chi_30
   1000035     2.05464094E+03   # ~chi_40
   1000024     4.47562493E+02   # ~chi_1+
   1000037     2.05480517E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.36796876E-01   # N_11
  1  2    -1.64600577E-02   # N_12
  1  3    -4.10313539E-01   # N_13
  1  4    -3.62136514E-01   # N_14
  2  1    -3.84510102E-02   # N_21
  2  2     2.35936858E-02   # N_22
  2  3    -7.04484585E-01   # N_23
  2  4     7.08284072E-01   # N_24
  3  1     5.46160504E-01   # N_31
  3  2     2.88945244E-02   # N_32
  3  3     5.79048426E-01   # N_33
  3  4     6.04629416E-01   # N_34
  4  1    -1.10103773E-03   # N_41
  4  2     9.99168410E-01   # N_42
  4  3    -6.86943802E-03   # N_43
  4  4    -4.01757090E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.72089343E-03   # U_11
  1  2     9.99952751E-01   # U_12
  2  1    -9.99952751E-01   # U_21
  2  2     9.72089343E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.68270127E-02   # V_11
  1  2    -9.98384040E-01   # V_12
  2  1    -9.98384040E-01   # V_21
  2  2    -5.68270127E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95070817E-01   # cos(theta_t)
  1  2    -9.91668753E-02   # sin(theta_t)
  2  1     9.91668753E-02   # -sin(theta_t)
  2  2     9.95070817E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999030E-01   # cos(theta_b)
  1  2    -1.39283849E-03   # sin(theta_b)
  2  1     1.39283849E-03   # -sin(theta_b)
  2  2     9.99999030E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06043321E-01   # cos(theta_tau)
  1  2     7.08168644E-01   # sin(theta_tau)
  2  1    -7.08168644E-01   # -sin(theta_tau)
  2  2    -7.06043321E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00261105E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10900429E+03  # DRbar Higgs Parameters
         1    -4.38900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44037963E+02   # higgs               
         4     1.62771324E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10900429E+03  # The gauge couplings
     1     3.61347424E-01   # gprime(Q) DRbar
     2     6.35747297E-01   # g(Q) DRbar
     3     1.03385874E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10900429E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.57780711E-07   # A_c(Q) DRbar
  3  3     2.55959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10900429E+03  # The trilinear couplings
  1  1     3.52557466E-07   # A_d(Q) DRbar
  2  2     3.52590491E-07   # A_s(Q) DRbar
  3  3     6.31514685E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10900429E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.84959483E-08   # A_mu(Q) DRbar
  3  3     7.92879673E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10900429E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.71442171E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10900429E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.87735247E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10900429E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02950399E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10900429E+03  # The soft SUSY breaking masses at the scale Q
         1     4.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56890664E+07   # M^2_Hd              
        22    -2.03403663E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     6.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.05485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40211974E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.54695290E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44157237E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44157237E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55842763E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55842763E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.56882421E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.58832702E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.47912269E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.02890598E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.90364431E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22927457E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.48637377E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11227730E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.71429146E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.20322894E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.27640641E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.66489617E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.16641054E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.43312039E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.11473355E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.33020472E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.71448817E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.80163357E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.21536735E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66960374E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.53366335E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80854472E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.60972422E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.60645441E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.62916491E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.18817691E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13176066E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.15813256E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.95118468E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.95449014E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.78628260E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78480243E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.72747883E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.26430239E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.48927429E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77658571E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.49962792E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54109106E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53033873E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60967660E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.81548249E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.01442244E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.61434130E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.69427188E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45621581E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78500176E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.62089355E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.11777936E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.72803628E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78191405E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.60939516E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57377616E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53252129E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54304396E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.94763215E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.08949434E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.20885462E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.62791103E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85822609E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78480243E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.72747883E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.26430239E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.48927429E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77658571E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.49962792E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54109106E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53033873E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60967660E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.81548249E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.01442244E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.61434130E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.69427188E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45621581E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78500176E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.62089355E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.11777936E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.72803628E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78191405E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.60939516E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57377616E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53252129E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54304396E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.94763215E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.08949434E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.20885462E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.62791103E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85822609E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13046255E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06254104E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.51559245E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.76198102E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78173709E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     9.47589130E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57856102E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03417413E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.01709882E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.47363485E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.96815811E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.72367494E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13046255E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06254104E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.51559245E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.76198102E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78173709E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     9.47589130E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57856102E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03417413E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.01709882E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.47363485E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.96815811E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.72367494E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06923988E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51820637E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.48198146E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.25288145E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40603417E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.54972999E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81970686E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06204377E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.57939036E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.98147710E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.00717970E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43916403E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.90034673E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.88608353E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13153280E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.21959397E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.03475932E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.96397409E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78588023E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.23625075E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55541828E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13153280E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.21959397E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.03475932E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.96397409E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78588023E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.23625075E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55541828E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45483125E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.10602153E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.38404209E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.59486114E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52814715E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.55470729E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.04149043E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.00928872E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33576044E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33576044E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11193560E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11193560E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10460792E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.14161012E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.79426013E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.66397196E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.46368253E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48230746E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.30604541E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.80944351E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.35620024E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.79312437E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.27799436E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18062385E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53124447E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18062385E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53124447E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41410334E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51009843E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51009843E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47765525E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01696070E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01696070E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01696050E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.61658315E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.61658315E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.61658315E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.61658315E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     5.38861481E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     5.38861481E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     5.38861481E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     5.38861481E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.06046034E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.06046034E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.91542680E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.96176224E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.94033969E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.37730088E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     5.64250402E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.37730088E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     5.64250402E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.41315855E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.26603109E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.26603109E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.26729963E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.59876639E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.59876639E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.59875575E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.03296155E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.33976575E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.03296155E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.33976575E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.20566949E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     3.07136524E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.07136524E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.81769440E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.13917382E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.13917382E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.13917389E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     6.89954211E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     6.89954211E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     6.89954211E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     6.89954211E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.29982379E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.29982379E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.29982379E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.29982379E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.17409980E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.17409980E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.14039875E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.55259796E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.31059330E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.59796603E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.30318120E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.30318120E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.47878474E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.12127561E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.27144891E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.86133257E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.86133257E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.86812820E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.86812820E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.06978893E-03   # h decays
#          BR         NDA      ID1       ID2
     5.83737864E-01    2           5        -5   # BR(h -> b       bb     )
     6.42555101E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27462050E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82037767E-04    2           3        -3   # BR(h -> s       sb     )
     2.09360183E-02    2           4        -4   # BR(h -> c       cb     )
     6.85366546E-02    2          21        21   # BR(h -> g       g      )
     2.39862607E-03    2          22        22   # BR(h -> gam     gam    )
     1.67884531E-03    2          22        23   # BR(h -> Z       gam    )
     2.28630738E-01    2          24       -24   # BR(h -> W+      W-     )
     2.91162441E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.57081431E+01   # H decays
#          BR         NDA      ID1       ID2
     3.66493494E-01    2           5        -5   # BR(H -> b       bb     )
     5.95173833E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.10439052E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48913091E-04    2           3        -3   # BR(H -> s       sb     )
     6.98135033E-08    2           4        -4   # BR(H -> c       cb     )
     6.99676662E-03    2           6        -6   # BR(H -> t       tb     )
     6.41866070E-07    2          21        21   # BR(H -> g       g      )
     2.78009282E-09    2          22        22   # BR(H -> gam     gam    )
     1.80037693E-09    2          23        22   # BR(H -> Z       gam    )
     1.72451564E-06    2          24       -24   # BR(H -> W+      W-     )
     8.61660557E-07    2          23        23   # BR(H -> Z       Z      )
     6.87877278E-06    2          25        25   # BR(H -> h       h      )
    -3.85931878E-24    2          36        36   # BR(H -> A       A      )
    -1.92264815E-20    2          23        36   # BR(H -> Z       A      )
     1.84893791E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54545671E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.54545671E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.25826795E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.90693637E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.43745282E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.52109792E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.16572946E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.45293364E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.21820931E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.15986911E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.89017321E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     7.07337627E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     8.41930005E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     8.41930005E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.43240513E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.57073670E+01   # A decays
#          BR         NDA      ID1       ID2
     3.66524195E-01    2           5        -5   # BR(A -> b       bb     )
     5.95184682E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.10442722E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48958057E-04    2           3        -3   # BR(A -> s       sb     )
     7.02674172E-08    2           4        -4   # BR(A -> c       cb     )
     7.01043583E-03    2           6        -6   # BR(A -> t       tb     )
     1.44046712E-05    2          21        21   # BR(A -> g       g      )
     6.45934609E-08    2          22        22   # BR(A -> gam     gam    )
     1.58406995E-08    2          23        22   # BR(A -> Z       gam    )
     1.72087531E-06    2          23        25   # BR(A -> Z       h      )
     1.88103367E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.54529326E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.54529326E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.97101567E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     6.32132941E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23256333E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.01142142E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.79687441E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.75301474E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.34362954E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.22864728E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.51808606E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     8.75937536E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     8.75937536E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.59373091E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.90494669E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.92885203E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.09629683E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.77916270E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18759278E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.44326025E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.75661075E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.71567795E-06    2          24        25   # BR(H+ -> W+      h      )
     2.27182848E-14    2          24        36   # BR(H+ -> W+      A      )
     5.51296144E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.81713166E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.81000151E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.54111057E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.19471930E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.53263502E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.03885242E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.64060968E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.71397286E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

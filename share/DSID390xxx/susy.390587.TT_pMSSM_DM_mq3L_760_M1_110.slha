#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13069959E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04148190E+01   # W+
        25     1.25503964E+02   # h
        35     4.00000129E+03   # H
        36     3.99999659E+03   # A
        37     4.00106359E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02606149E+03   # ~d_L
   2000001     4.02261692E+03   # ~d_R
   1000002     4.02540454E+03   # ~u_L
   2000002     4.02368522E+03   # ~u_R
   1000003     4.02606149E+03   # ~s_L
   2000003     4.02261692E+03   # ~s_R
   1000004     4.02540454E+03   # ~c_L
   2000004     4.02368522E+03   # ~c_R
   1000005     8.33132972E+02   # ~b_1
   2000005     4.02538001E+03   # ~b_2
   1000006     8.22123888E+02   # ~t_1
   2000006     2.27843359E+03   # ~t_2
   1000011     4.00476332E+03   # ~e_L
   2000011     4.00319202E+03   # ~e_R
   1000012     4.00364350E+03   # ~nu_eL
   1000013     4.00476332E+03   # ~mu_L
   2000013     4.00319202E+03   # ~mu_R
   1000014     4.00364350E+03   # ~nu_muL
   1000015     4.00556139E+03   # ~tau_1
   2000015     4.00669794E+03   # ~tau_2
   1000016     4.00507800E+03   # ~nu_tauL
   1000021     1.98157988E+03   # ~g
   1000022     9.33132561E+01   # ~chi_10
   1000023    -1.37081657E+02   # ~chi_20
   1000025     1.52708405E+02   # ~chi_30
   1000035     2.05248287E+03   # ~chi_40
   1000024     1.31972488E+02   # ~chi_1+
   1000037     2.05264811E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61971288E-01   # N_11
  1  2     1.37927838E-02   # N_12
  1  3     5.33807747E-01   # N_13
  1  4     3.66413434E-01   # N_14
  2  1    -1.35835123E-01   # N_21
  2  2     2.72327057E-02   # N_22
  2  3    -6.85335508E-01   # N_23
  2  4     7.14928277E-01   # N_24
  3  1     6.33204362E-01   # N_31
  3  2     2.38609676E-02   # N_32
  3  3     4.95342570E-01   # N_33
  3  4     5.94237855E-01   # N_34
  4  1    -9.00681964E-04   # N_41
  4  2     9.99249115E-01   # N_42
  4  3    -5.18897318E-04   # N_43
  4  4    -3.87314657E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.38132284E-04   # U_11
  1  2     9.99999728E-01   # U_12
  2  1    -9.99999728E-01   # U_21
  2  2     7.38132284E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.47878935E-02   # V_11
  1  2    -9.98498015E-01   # V_12
  2  1    -9.98498015E-01   # V_21
  2  2    -5.47878935E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96296666E-01   # cos(theta_t)
  1  2    -8.59822849E-02   # sin(theta_t)
  2  1     8.59822849E-02   # -sin(theta_t)
  2  2     9.96296666E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999922E-01   # cos(theta_b)
  1  2    -3.94968345E-04   # sin(theta_b)
  2  1     3.94968345E-04   # -sin(theta_b)
  2  2     9.99999922E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03342543E-01   # cos(theta_tau)
  1  2     7.10851087E-01   # sin(theta_tau)
  2  1    -7.10851087E-01   # -sin(theta_tau)
  2  2    -7.03342543E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00322507E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30699588E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43590155E+02   # higgs               
         4     1.60883913E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30699588E+03  # The gauge couplings
     1     3.62130112E-01   # gprime(Q) DRbar
     2     6.37372203E-01   # g(Q) DRbar
     3     1.03009368E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30699588E+03  # The trilinear couplings
  1  1     1.38323360E-06   # A_u(Q) DRbar
  2  2     1.38324670E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30699588E+03  # The trilinear couplings
  1  1     5.07072459E-07   # A_d(Q) DRbar
  2  2     5.07119850E-07   # A_s(Q) DRbar
  3  3     9.07479182E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30699588E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08003005E-07   # A_mu(Q) DRbar
  3  3     1.09107797E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30699588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66551894E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30699588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.79279564E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30699588E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04200168E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30699588E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58733699E+07   # M^2_Hd              
        22    -1.98877586E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40949767E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.74322353E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44993962E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44993962E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55006038E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55006038E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.18375632E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.19135975E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.48156408E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.31590947E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.01116669E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.37998340E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.51670497E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.22450409E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.37859709E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     9.93231319E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.36429291E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.31740540E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.05452261E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.34154559E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.21802499E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.95287178E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.21623191E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.58644549E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.69960976E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.62259063E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.90913541E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66646775E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.79853081E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69968979E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.41429715E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.85356780E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55604820E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.53074048E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15247665E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.66585922E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.85062757E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.14261100E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.10592456E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.79041534E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48896705E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.98381161E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80424976E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81204217E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.24286524E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61182362E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51943640E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61356413E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.23029310E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02547998E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.22728281E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.48779816E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44398736E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.79061920E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.18501482E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.44902420E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.05370186E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81666920E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.51680067E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64344075E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52163518E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54534809E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.42993122E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.67614134E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.81242167E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.48989507E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85490026E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.79041534E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48896705E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.98381161E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80424976E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81204217E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.24286524E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61182362E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51943640E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61356413E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.23029310E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02547998E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.22728281E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.48779816E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44398736E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.79061920E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.18501482E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.44902420E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.05370186E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81666920E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.51680067E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64344075E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52163518E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54534809E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.42993122E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.67614134E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.81242167E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.48989507E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85490026E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.16249714E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97962590E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.27342479E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.50894068E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77500107E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.56400785E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56340246E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08495085E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.81038840E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84418755E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00518843E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.41598315E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.16249714E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97962590E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.27342479E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.50894068E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77500107E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.56400785E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56340246E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08495085E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.81038840E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84418755E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00518843E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.41598315E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11921024E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27736820E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00042348E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60081325E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39182925E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.39575851E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79037110E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12699631E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.12110965E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.32946491E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35541421E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42047503E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22236660E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84781796E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16355133E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01942787E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.56190329E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75340938E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77787987E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06352769E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54109702E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.16355133E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01942787E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.56190329E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75340938E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77787987E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06352769E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54109702E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49755161E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.22408237E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.03257779E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20586038E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51478898E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.75591298E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01629967E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.37820453E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33719431E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33719431E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11240581E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11240581E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10079975E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.67880482E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.63904648E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.51408180E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.23561226E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.72909049E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.15032221E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.21990583E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.13378639E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.40039058E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.77782349E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18466818E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53634370E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18466818E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53634370E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37482186E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.52084055E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.52084055E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47747596E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03929136E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03929136E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03929109E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.93149142E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.93149142E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.93149142E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.93149142E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.77166222E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.77166222E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.77166222E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.77166222E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.47903022E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.47903022E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.24101946E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04614392E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.46422433E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     5.14381344E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.65270711E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.14381344E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.65270711E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     6.21516317E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.51031681E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.51031681E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.49538879E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.05515878E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.05515878E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.05515454E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.05060632E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.84748128E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.05060632E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.84748128E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.00230750E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.79891716E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.79891716E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.66989325E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.59518787E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.59518787E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.59518796E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.49442292E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.49442292E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.49442292E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.49442292E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.83142725E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.83142725E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.83142725E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.83142725E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.73046847E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.73046847E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.67762485E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.42415531E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.79594496E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.49575115E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.05794247E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.05794247E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.19650054E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.47796485E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.81600409E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.78174550E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78174550E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.79507372E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.79507372E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05285738E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96314544E-01    2           5        -5   # BR(h -> b       bb     )
     6.42836495E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27563819E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82624205E-04    2           3        -3   # BR(h -> s       sb     )
     2.09564395E-02    2           4        -4   # BR(h -> c       cb     )
     6.85853151E-02    2          21        21   # BR(h -> g       g      )
     2.35939231E-03    2          22        22   # BR(h -> gam     gam    )
     1.61331897E-03    2          22        23   # BR(h -> Z       gam    )
     2.17680944E-01    2          24       -24   # BR(h -> W+      W-     )
     2.74962084E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.43107259E+01   # H decays
#          BR         NDA      ID1       ID2
     3.36757147E-01    2           5        -5   # BR(H -> b       bb     )
     6.10485976E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15853054E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.55316991E-04    2           3        -3   # BR(H -> s       sb     )
     7.16272367E-08    2           4        -4   # BR(H -> c       cb     )
     7.17854029E-03    2           6        -6   # BR(H -> t       tb     )
     1.11068492E-06    2          21        21   # BR(H -> g       g      )
     3.01799462E-10    2          22        22   # BR(H -> gam     gam    )
     1.84025045E-09    2          23        22   # BR(H -> Z       gam    )
     2.10421504E-06    2          24       -24   # BR(H -> W+      W-     )
     1.05137846E-06    2          23        23   # BR(H -> Z       Z      )
     7.70438992E-06    2          25        25   # BR(H -> h       h      )
     2.85062856E-24    2          36        36   # BR(H -> A       A      )
     3.57936447E-20    2          23        36   # BR(H -> Z       A      )
     1.86216949E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.67668305E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.67668305E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.36172372E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60503812E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68786968E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48504513E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.96745246E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.96016589E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.05133971E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.45025024E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.40428658E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.06713187E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.16161864E-06    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.16161864E-06    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.33784253E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.43101860E+01   # A decays
#          BR         NDA      ID1       ID2
     3.36786459E-01    2           5        -5   # BR(A -> b       bb     )
     6.10496321E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15856543E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.55362721E-04    2           3        -3   # BR(A -> s       sb     )
     7.20751079E-08    2           4        -4   # BR(A -> c       cb     )
     7.19078541E-03    2           6        -6   # BR(A -> t       tb     )
     1.47752455E-05    2          21        21   # BR(A -> g       g      )
     4.07109145E-08    2          22        22   # BR(A -> gam     gam    )
     1.62352412E-08    2          23        22   # BR(A -> Z       gam    )
     2.09982697E-06    2          23        25   # BR(A -> Z       h      )
     1.86531434E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.67672096E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.67672096E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93648736E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.22573509E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33501743E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.97380865E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.57545883E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.64203910E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.31254248E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.33445534E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.83441990E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.20537064E-06    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.20537064E-06    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.41819270E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.36348080E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.12104718E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16425232E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.43262474E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22608805E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.52245741E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.42066524E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.10721990E-06    2          24        25   # BR(H+ -> W+      h      )
     3.35891570E-14    2          24        36   # BR(H+ -> W+      A      )
     4.85421808E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.29413304E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.08039234E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.68289024E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.63535634E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58391575E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.26730834E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.19863586E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.43379277E-06    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

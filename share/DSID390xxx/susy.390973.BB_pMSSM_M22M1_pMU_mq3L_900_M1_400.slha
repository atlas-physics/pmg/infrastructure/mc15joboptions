#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419705E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.99900000E+02   # M_1(MX)             
         2     7.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04052626E+01   # W+
        25     1.24857609E+02   # h
        35     3.00011112E+03   # H
        36     2.99999992E+03   # A
        37     3.00091308E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04043627E+03   # ~d_L
   2000001     3.03536285E+03   # ~d_R
   1000002     3.03953405E+03   # ~u_L
   2000002     3.03636597E+03   # ~u_R
   1000003     3.04043627E+03   # ~s_L
   2000003     3.03536285E+03   # ~s_R
   1000004     3.03953405E+03   # ~c_L
   2000004     3.03636597E+03   # ~c_R
   1000005     1.00532871E+03   # ~b_1
   2000005     3.03431610E+03   # ~b_2
   1000006     1.00136170E+03   # ~t_1
   2000006     3.01738201E+03   # ~t_2
   1000011     3.00622986E+03   # ~e_L
   2000011     3.00214470E+03   # ~e_R
   1000012     3.00484747E+03   # ~nu_eL
   1000013     3.00622986E+03   # ~mu_L
   2000013     3.00214470E+03   # ~mu_R
   1000014     3.00484747E+03   # ~nu_muL
   1000015     2.98547488E+03   # ~tau_1
   2000015     3.02201458E+03   # ~tau_2
   1000016     3.00458165E+03   # ~nu_tauL
   1000021     2.35014996E+03   # ~g
   1000022     4.01820469E+02   # ~chi_10
   1000023     8.35014678E+02   # ~chi_20
   1000025    -2.99518641E+03   # ~chi_30
   1000035     2.99623749E+03   # ~chi_40
   1000024     8.35177083E+02   # ~chi_1+
   1000037     2.99670089E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99881904E-01   # N_11
  1  2    -6.79696294E-04   # N_12
  1  3     1.51048072E-02   # N_13
  1  4    -2.74961257E-03   # N_14
  2  1     1.12942948E-03   # N_21
  2  2     9.99563407E-01   # N_22
  2  3    -2.81832395E-02   # N_23
  2  4     8.79911704E-03   # N_24
  3  1    -8.72473051E-03   # N_31
  3  2     1.37159848E-02   # N_32
  3  3     7.06886506E-01   # N_33
  3  4     7.07140169E-01   # N_34
  4  1    -1.26008342E-02   # N_41
  4  2     2.61611321E-02   # N_42
  4  3     7.06603862E-01   # N_43
  4  4    -7.07013293E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99204922E-01   # U_11
  1  2    -3.98688384E-02   # U_12
  2  1     3.98688384E-02   # U_21
  2  2     9.99204922E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99922511E-01   # V_11
  1  2    -1.24487808E-02   # V_12
  2  1     1.24487808E-02   # V_21
  2  2     9.99922511E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98479104E-01   # cos(theta_t)
  1  2    -5.51314690E-02   # sin(theta_t)
  2  1     5.51314690E-02   # -sin(theta_t)
  2  2     9.98479104E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99906871E-01   # cos(theta_b)
  1  2     1.36473194E-02   # sin(theta_b)
  2  1    -1.36473194E-02   # -sin(theta_b)
  2  2     9.99906871E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06967559E-01   # cos(theta_tau)
  1  2     7.07245976E-01   # sin(theta_tau)
  2  1    -7.07245976E-01   # -sin(theta_tau)
  2  2     7.06967559E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01779430E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64197047E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44004096E+02   # higgs               
         4     7.54021398E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64197047E+03  # The gauge couplings
     1     3.62476886E-01   # gprime(Q) DRbar
     2     6.36866235E-01   # g(Q) DRbar
     3     1.02431897E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64197047E+03  # The trilinear couplings
  1  1     2.58166482E-06   # A_u(Q) DRbar
  2  2     2.58169896E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64197047E+03  # The trilinear couplings
  1  1     7.17358988E-07   # A_d(Q) DRbar
  2  2     7.17477122E-07   # A_s(Q) DRbar
  3  3     1.53473512E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64197047E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.42086125E-07   # A_mu(Q) DRbar
  3  3     1.43736433E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64197047E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51099253E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64197047E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16201781E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64197047E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07912744E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64197047E+03  # The soft SUSY breaking masses at the scale Q
         1     3.99900000E+02   # M_1(Q)              
         2     7.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.62055081E+04   # M^2_Hd              
        22    -9.04012160E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40767969E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.38806368E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47677592E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47677592E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52322408E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52322408E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.73528377E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.07618339E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.92381661E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11276181E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.75775058E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     4.26044817E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     8.56832801E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.75568774E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.99265181E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69494321E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59776669E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12525889E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.14258658E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.07695801E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.92304199E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.19145961E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.86855664E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.36435673E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.33638938E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.24150294E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.05339720E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.30262363E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.89792690E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.19020356E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.01248933E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.56861256E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.14212380E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52579344E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.48794093E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     9.22474435E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.05228033E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     4.95553880E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.36050332E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14506974E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56675643E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.77451553E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02999254E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.04327525E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43324148E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.57393980E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.22776606E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.52385989E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.19765164E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.38372584E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.04664998E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.28687720E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.36720537E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64988157E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44669342E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.03592550E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.78848876E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.52467130E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55533007E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.56861256E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.14212380E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52579344E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.48794093E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     9.22474435E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.05228033E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     4.95553880E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.36050332E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14506974E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56675643E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.77451553E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02999254E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.04327525E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43324148E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.57393980E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.22776606E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.52385989E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.19765164E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.38372584E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.04664998E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.28687720E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.36720537E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64988157E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44669342E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.03592550E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.78848876E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.52467130E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55533007E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.47385442E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.08810928E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.97472749E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.43772466E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.54538883E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.93716272E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.44666259E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.51338367E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998870E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.12612013E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.69239610E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.54512170E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.47385442E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.08810928E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.97472749E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.43772466E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.54538883E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.93716272E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.44666259E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.51338367E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998870E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.12612013E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.69239610E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.54512170E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.51811049E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.75199025E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.08567095E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.16233881E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.46644668E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.84017681E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.05624912E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.62228188E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.78803269E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.10304872E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.84311193E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.47388562E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.09277093E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.96523772E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.02001212E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.26977287E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.94199115E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.17157289E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.47388562E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.09277093E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.96523772E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.02001212E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.26977287E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.94199115E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.17157289E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47382008E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.09268783E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.96494354E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.74849450E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.19281197E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.94234824E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.02269392E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.71615443E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.99741327E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.83375947E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.04224194E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.46752680E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.37832274E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.32758704E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.18299345E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.85488323E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.46677845E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.85346392E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.41465361E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.01474803E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.62326737E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.76377257E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.15488260E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.15488260E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.07633499E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.61573132E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.56623382E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.56623382E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27755129E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27755129E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.55017231E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.55017231E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.93356588E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.86423862E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.60507726E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.23368259E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.23368259E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71148881E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.27818275E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52673904E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.52673904E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30829748E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30829748E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.31582493E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.31582493E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.77146069E-03   # h decays
#          BR         NDA      ID1       ID2
     6.72148668E-01    2           5        -5   # BR(h -> b       bb     )
     5.46369129E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93416825E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.10585785E-04    2           3        -3   # BR(h -> s       sb     )
     1.77262276E-02    2           4        -4   # BR(h -> c       cb     )
     5.74869296E-02    2          21        21   # BR(h -> g       g      )
     1.96331819E-03    2          22        22   # BR(h -> gam     gam    )
     1.29323858E-03    2          22        23   # BR(h -> Z       gam    )
     1.72593191E-01    2          24       -24   # BR(h -> W+      W-     )
     2.15475108E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40710191E+01   # H decays
#          BR         NDA      ID1       ID2
     7.39022870E-01    2           5        -5   # BR(H -> b       bb     )
     1.76728345E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.24869194E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.67340216E-04    2           3        -3   # BR(H -> s       sb     )
     2.16492730E-07    2           4        -4   # BR(H -> c       cb     )
     2.15965408E-02    2           6        -6   # BR(H -> t       tb     )
     2.79089350E-05    2          21        21   # BR(H -> g       g      )
     3.61756584E-08    2          22        22   # BR(H -> gam     gam    )
     8.33741910E-09    2          23        22   # BR(H -> Z       gam    )
     3.02245495E-05    2          24       -24   # BR(H -> W+      W-     )
     1.50936486E-05    2          23        23   # BR(H -> Z       Z      )
     7.93720585E-05    2          25        25   # BR(H -> h       h      )
    -5.34003022E-23    2          36        36   # BR(H -> A       A      )
     1.46436400E-18    2          23        36   # BR(H -> Z       A      )
     1.59295660E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.03705380E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.94669850E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.00003278E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.80102745E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.56535860E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32751915E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83402581E-01    2           5        -5   # BR(A -> b       bb     )
     1.87320382E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62319181E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.13465486E-04    2           3        -3   # BR(A -> s       sb     )
     2.29548430E-07    2           4        -4   # BR(A -> c       cb     )
     2.28863263E-02    2           6        -6   # BR(A -> t       tb     )
     6.73980414E-05    2          21        21   # BR(A -> g       g      )
     6.46063917E-08    2          22        22   # BR(A -> gam     gam    )
     6.64406210E-08    2          23        22   # BR(A -> Z       gam    )
     3.19152790E-05    2          23        25   # BR(A -> Z       h      )
     2.60484352E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22818106E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29928751E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.88303011E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03591885E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10154446E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.40122175E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.49013443E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.04987356E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.98947325E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02649509E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.16259642E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.09620441E-05    2          24        25   # BR(H+ -> W+      h      )
     8.06481438E-14    2          24        36   # BR(H+ -> W+      A      )
     1.82227583E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.16644658E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.87209449E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90942191E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.85485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04196476E+01   # W+
        25     1.25526622E+02   # h
        35     4.00000462E+03   # H
        36     3.99999544E+03   # A
        37     4.00099952E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01295414E+03   # ~d_L
   2000001     4.01226281E+03   # ~d_R
   1000002     4.01229810E+03   # ~u_L
   2000002     4.01348498E+03   # ~u_R
   1000003     4.01295414E+03   # ~s_L
   2000003     4.01226281E+03   # ~s_R
   1000004     4.01229810E+03   # ~c_L
   2000004     4.01348498E+03   # ~c_R
   1000005     4.76521141E+02   # ~b_1
   2000005     4.01826860E+03   # ~b_2
   1000006     4.52225345E+02   # ~t_1
   2000006     1.87505906E+03   # ~t_2
   1000011     4.00203112E+03   # ~e_L
   2000011     4.00297036E+03   # ~e_R
   1000012     4.00091632E+03   # ~nu_eL
   1000013     4.00203112E+03   # ~mu_L
   2000013     4.00297036E+03   # ~mu_R
   1000014     4.00091632E+03   # ~nu_muL
   1000015     4.00423518E+03   # ~tau_1
   2000015     4.00846600E+03   # ~tau_2
   1000016     4.00348955E+03   # ~nu_tauL
   1000021     1.95666609E+03   # ~g
   1000022     2.02746999E+02   # ~chi_10
   1000023    -2.70472160E+02   # ~chi_20
   1000025     2.79484782E+02   # ~chi_30
   1000035     2.05752948E+03   # ~chi_40
   1000024     2.67654109E+02   # ~chi_1+
   1000037     2.05769353E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99165246E-01   # N_11
  1  2    -1.04311247E-02   # N_12
  1  3    -3.52102888E-01   # N_13
  1  4    -2.59647084E-01   # N_14
  2  1    -6.91204020E-02   # N_21
  2  2     2.55370955E-02   # N_22
  2  3    -7.00625475E-01   # N_23
  2  4     7.09714147E-01   # N_24
  3  1     4.32114929E-01   # N_31
  3  2     2.80068259E-02   # N_32
  3  3     6.20594463E-01   # N_33
  3  4     6.53723809E-01   # N_34
  4  1    -9.58469363E-04   # N_41
  4  2     9.99227034E-01   # N_42
  4  3    -3.16421642E-03   # N_43
  4  4    -3.91714562E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.48011105E-03   # U_11
  1  2     9.99989964E-01   # U_12
  2  1    -9.99989964E-01   # U_21
  2  2     4.48011105E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.54094861E-02   # V_11
  1  2    -9.98463714E-01   # V_12
  2  1    -9.98463714E-01   # V_21
  2  2    -5.54094861E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94508737E-01   # cos(theta_t)
  1  2    -1.04653581E-01   # sin(theta_t)
  2  1     1.04653581E-01   # -sin(theta_t)
  2  2     9.94508737E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999668E-01   # cos(theta_b)
  1  2    -8.14861884E-04   # sin(theta_b)
  2  1     8.14861884E-04   # -sin(theta_b)
  2  2     9.99999668E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05095909E-01   # cos(theta_tau)
  1  2     7.09111951E-01   # sin(theta_tau)
  2  1    -7.09111951E-01   # -sin(theta_tau)
  2  2    -7.05095909E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00292764E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.09421906E+02  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44489263E+02   # higgs               
         4     1.62397709E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.09421906E+02  # The gauge couplings
     1     3.60802040E-01   # gprime(Q) DRbar
     2     6.36027443E-01   # g(Q) DRbar
     3     1.03838816E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.09421906E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.90684535E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.09421906E+02  # The trilinear couplings
  1  1     2.16580589E-07   # A_d(Q) DRbar
  2  2     2.16601573E-07   # A_s(Q) DRbar
  3  3     3.87798967E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.09421906E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.79734427E-08   # A_mu(Q) DRbar
  3  3     4.84571010E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.09421906E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75999716E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.09421906E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85403131E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.09421906E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03354859E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.09421906E+02  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57992794E+07   # M^2_Hd              
        22    -7.55110509E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.85485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40334521E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.24453674E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44163951E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44163951E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55836049E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55836049E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.74809473E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.54968371E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.33583315E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.70421786E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     3.54406136E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.21902855E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.32853117E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.08660736E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.39076994E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.13978026E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.58496972E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.26355833E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.55315422E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.96297454E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.51233473E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.78857283E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.49005390E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.92090385E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66134888E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52433105E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77078322E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63220806E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.77237248E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.58366945E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.40182373E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14544684E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.63146680E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.96420900E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.67497245E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.26815191E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77570553E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.21912913E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.34913505E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04427865E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.77474903E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.33096783E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.53729606E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53069553E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60427079E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.46191472E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.62683492E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02604111E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.78486793E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44857730E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77589680E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83739626E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29387688E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.50201780E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77959389E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.48531583E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.56923803E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53291210E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53687922E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16402727E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.85288886E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.67673584E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.26209483E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85614455E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77570553E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.21912913E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.34913505E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04427865E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.77474903E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.33096783E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.53729606E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53069553E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60427079E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.46191472E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.62683492E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02604111E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.78486793E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44857730E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77589680E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83739626E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29387688E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.50201780E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77959389E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.48531583E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.56923803E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53291210E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53687922E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16402727E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.85288886E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.67673584E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.26209483E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85614455E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13151889E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27721331E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.52657977E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.79912871E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77593904E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.04593123E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56577753E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06094145E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09212850E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.76269523E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86023954E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.00374037E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13151889E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27721331E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.52657977E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.79912871E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77593904E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.04593123E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56577753E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06094145E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09212850E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.76269523E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86023954E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.00374037E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08974292E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92029461E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.29048154E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.07648234E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39658930E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46243736E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80017597E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09196863E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.03839934E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.41145652E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53263402E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42329630E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10812252E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85371244E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13260954E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38522599E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13486334E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.39916087E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77913094E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.12757202E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54310263E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13260954E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38522599E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13486334E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.39916087E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77913094E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.12757202E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54310263E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46346698E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25371249E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.93218921E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17139728E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51758827E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.70794944E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02144268E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.26526690E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33456782E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33456782E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11153166E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11153166E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10780103E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.68130601E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.85938004E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.11557015E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.74312169E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10232365E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.84206429E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.97211873E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.24407719E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.97114112E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.69878161E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.52069113E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17535412E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52391048E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17535412E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52391048E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45881424E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49044326E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49044326E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.46837807E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.97731887E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.97731887E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.97731842E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.54729091E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.54729091E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.54729091E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.54729091E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.49098073E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.49098073E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.49098073E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.49098073E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.05726524E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.05726524E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.66793435E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.94048440E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.57989166E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.14826775E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.48657890E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.14826775E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.48657890E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.44090221E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.38712543E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.38712543E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.37497194E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.81420420E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.81420420E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.81419539E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.46780220E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.90318258E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.46780220E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.90318258E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     4.35977088E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     4.35977088E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     3.40814346E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     8.71314944E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     8.71314944E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     8.71314958E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.92977597E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.92977597E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.92977597E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.92977597E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.43253435E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.43253435E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.43253435E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.43253435E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.76410563E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.76410563E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.68038811E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.32572258E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.56036172E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20818784E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.95612188E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.95612188E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.60427585E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.50127777E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.09515414E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89749086E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89749086E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.74884408E-06    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.74884408E-06    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.90398040E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.90398040E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02714825E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94425480E-01    2           5        -5   # BR(h -> b       bb     )
     6.46980545E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29030712E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85718923E-04    2           3        -3   # BR(h -> s       sb     )
     2.10932956E-02    2           4        -4   # BR(h -> c       cb     )
     6.78437513E-02    2          21        21   # BR(h -> g       g      )
     2.39311299E-03    2          22        22   # BR(h -> gam     gam    )
     1.62715014E-03    2          22        23   # BR(h -> Z       gam    )
     2.19455006E-01    2          24       -24   # BR(h -> W+      W-     )
     2.77493995E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.53058713E+01   # H decays
#          BR         NDA      ID1       ID2
     3.55484644E-01    2           5        -5   # BR(H -> b       bb     )
     5.99501882E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.11969345E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50723205E-04    2           3        -3   # BR(H -> s       sb     )
     7.03301084E-08    2           4        -4   # BR(H -> c       cb     )
     7.04854110E-03    2           6        -6   # BR(H -> t       tb     )
     6.46786828E-07    2          21        21   # BR(H -> g       g      )
     5.10869540E-10    2          22        22   # BR(H -> gam     gam    )
     1.81132208E-09    2          23        22   # BR(H -> Z       gam    )
     1.90327537E-06    2          24       -24   # BR(H -> W+      W-     )
     9.50978523E-07    2          23        23   # BR(H -> Z       Z      )
     7.23060013E-06    2          25        25   # BR(H -> h       h      )
    -1.47254894E-24    2          36        36   # BR(H -> A       A      )
    -2.11347671E-20    2          23        36   # BR(H -> Z       A      )
     1.84486997E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61232504E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61232504E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.93832177E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.02880623E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.04551603E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.80485256E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.82308460E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.00218203E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.37545423E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.89133822E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.27539653E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.58869577E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.04502576E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.12630575E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.12630575E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42703142E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.53046466E+01   # A decays
#          BR         NDA      ID1       ID2
     3.55518371E-01    2           5        -5   # BR(A -> b       bb     )
     5.99518508E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11975057E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50770851E-04    2           3        -3   # BR(A -> s       sb     )
     7.07790714E-08    2           4        -4   # BR(A -> c       cb     )
     7.06148251E-03    2           6        -6   # BR(A -> t       tb     )
     1.45095660E-05    2          21        21   # BR(A -> g       g      )
     5.29547175E-08    2          22        22   # BR(A -> gam     gam    )
     1.59567169E-08    2          23        22   # BR(A -> Z       gam    )
     1.89932164E-06    2          23        25   # BR(A -> Z       h      )
     1.84845454E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61238215E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61238215E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.68966043E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.28377337E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.63505126E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.47117507E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.46871083E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.01274999E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.03109244E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.98055016E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.17634226E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.22124048E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.22124048E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.53382935E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.69466975E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.99304388E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11899349E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.64458554E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20045052E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46971275E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.62602613E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.90026108E-06    2          24        25   # BR(H+ -> W+      h      )
     2.42685783E-14    2          24        36   # BR(H+ -> W+      A      )
     6.74920391E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.81039080E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.95583750E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61347265E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.01786309E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.58723247E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.24631351E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.35858297E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.33619019E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

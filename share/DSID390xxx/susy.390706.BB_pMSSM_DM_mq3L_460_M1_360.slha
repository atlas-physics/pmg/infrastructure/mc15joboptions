#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90919642E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.94900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.85485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04205421E+01   # W+
        25     1.25515153E+02   # h
        35     4.00000634E+03   # H
        36     3.99999600E+03   # A
        37     4.00097714E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01293777E+03   # ~d_L
   2000001     4.01223878E+03   # ~d_R
   1000002     4.01228248E+03   # ~u_L
   2000002     4.01346530E+03   # ~u_R
   1000003     4.01293777E+03   # ~s_L
   2000003     4.01223878E+03   # ~s_R
   1000004     4.01228248E+03   # ~c_L
   2000004     4.01346530E+03   # ~c_R
   1000005     4.81612060E+02   # ~b_1
   2000005     4.01837533E+03   # ~b_2
   1000006     4.57247049E+02   # ~t_1
   2000006     1.87524021E+03   # ~t_2
   1000011     4.00202453E+03   # ~e_L
   2000011     4.00287349E+03   # ~e_R
   1000012     4.00091110E+03   # ~nu_eL
   1000013     4.00202453E+03   # ~mu_L
   2000013     4.00287349E+03   # ~mu_R
   1000014     4.00091110E+03   # ~nu_muL
   1000015     4.00383051E+03   # ~tau_1
   2000015     4.00873058E+03   # ~tau_2
   1000016     4.00347356E+03   # ~nu_tauL
   1000021     1.95665049E+03   # ~g
   1000022     3.51152011E+02   # ~chi_10
   1000023    -4.04631726E+02   # ~chi_20
   1000025     4.17943858E+02   # ~chi_30
   1000035     2.05749596E+03   # ~chi_40
   1000024     4.02591135E+02   # ~chi_1+
   1000037     2.05765977E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.57924419E-01   # N_11
  1  2    -1.47589420E-02   # N_12
  1  3    -3.89761488E-01   # N_13
  1  4    -3.34415680E-01   # N_14
  2  1    -4.32373667E-02   # N_21
  2  2     2.40742237E-02   # N_22
  2  3    -7.03975521E-01   # N_23
  2  4     7.08498009E-01   # N_24
  3  1     5.11952237E-01   # N_31
  3  2     2.88388657E-02   # N_32
  3  3     5.93691226E-01   # N_33
  3  4     6.20164458E-01   # N_34
  4  1    -1.06202449E-03   # N_41
  4  2     9.99185131E-01   # N_42
  4  3    -5.93101759E-03   # N_43
  4  4    -3.99095218E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.39364914E-03   # U_11
  1  2     9.99964773E-01   # U_12
  2  1    -9.99964773E-01   # U_21
  2  2     8.39364914E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.64514316E-02   # V_11
  1  2    -9.98405346E-01   # V_12
  2  1    -9.98405346E-01   # V_21
  2  2    -5.64514316E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94478755E-01   # cos(theta_t)
  1  2    -1.04938105E-01   # sin(theta_t)
  2  1     1.04938105E-01   # -sin(theta_t)
  2  2     9.94478755E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999224E-01   # cos(theta_b)
  1  2    -1.24579268E-03   # sin(theta_b)
  2  1     1.24579268E-03   # -sin(theta_b)
  2  2     9.99999224E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05813657E-01   # cos(theta_tau)
  1  2     7.08397545E-01   # sin(theta_tau)
  2  1    -7.08397545E-01   # -sin(theta_tau)
  2  2    -7.05813657E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00266150E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.09196423E+02  # DRbar Higgs Parameters
         1    -3.94900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44546359E+02   # higgs               
         4     1.63292941E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.09196423E+02  # The gauge couplings
     1     3.60718726E-01   # gprime(Q) DRbar
     2     6.35569584E-01   # g(Q) DRbar
     3     1.03839211E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.09196423E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.90145362E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.09196423E+02  # The trilinear couplings
  1  1     2.17304395E-07   # A_d(Q) DRbar
  2  2     2.17325278E-07   # A_s(Q) DRbar
  3  3     3.88549480E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.09196423E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.87764240E-08   # A_mu(Q) DRbar
  3  3     4.92661879E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.09196423E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.75674418E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.09196423E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88385791E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.09196423E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02870204E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.09196423E+02  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57124516E+07   # M^2_Hd              
        22    -1.63260334E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.85485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40128644E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.22596803E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44126089E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44126089E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55873911E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55873911E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.82619050E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.19082824E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.90330678E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05051896E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.46724040E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.07817950E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.62792356E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.29378853E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.61253475E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     9.30509605E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.44805370E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.69485430E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.85709200E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
#
#         PDG            Width
DECAY   2000005     1.66299568E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52174165E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.79811004E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62040060E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.20168303E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61201565E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.38178066E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13643288E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.77453164E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.43158173E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     4.07744775E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.09317144E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77520327E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.87623620E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.16668764E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.35314736E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.76870228E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.46731867E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.52528026E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.53262392E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60321444E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.02352223E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.01696647E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.42386701E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.41985299E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45424377E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77539590E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.70221075E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.92285401E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.96239802E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.77391125E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.20879864E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.55780592E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53483061E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53654418E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.04918933E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.65187344E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.71291695E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.91399197E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85768662E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77520327E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.87623620E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.16668764E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.35314736E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.76870228E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.46731867E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.52528026E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.53262392E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60321444E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.02352223E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.01696647E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.42386701E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.41985299E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45424377E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77539590E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.70221075E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.92285401E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.96239802E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.77391125E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.20879864E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.55780592E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53483061E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53654418E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.04918933E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.65187344E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.71291695E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.91399197E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85768662E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.12195807E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.13068100E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.09214944E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.14066402E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77992464E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11174204E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57461569E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.03713522E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.37288318E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.86314600E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.60847915E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.21208114E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.12195807E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.13068100E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.09214944E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.14066402E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77992464E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11174204E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57461569E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.03713522E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.37288318E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.86314600E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.60847915E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.21208114E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.06825748E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.64591949E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.45553322E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.13855774E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40308868E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.53238734E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81364204E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.06430581E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72809893E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05863131E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.85860960E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43473963E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95624601E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87706411E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.12304451E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.27566361E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19177441E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.44947073E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78378239E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.21475141E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55154167E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.12304451E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.27566361E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19177441E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.44947073E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78378239E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.21475141E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55154167E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.44864683E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.15598530E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.07997340E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.12587958E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52488939E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.60474950E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03526267E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.24141157E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33545040E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33545040E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11183226E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11183226E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10543469E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.63912276E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.87151740E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.02874436E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.75235008E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.04914247E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.16946426E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.90816242E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.71814497E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     5.91538238E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.16281105E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.00871915E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17817483E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52771225E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17817483E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52771225E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43434636E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49999701E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49999701E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47021075E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99614662E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99614662E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99614632E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18569942E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18569942E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18569942E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18569942E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.95233485E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.95233485E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.95233485E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.95233485E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.71091631E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.71091631E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.83810988E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.08668966E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     4.10254387E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.23335403E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.33804009E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.23335403E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.33804009E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.54029687E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.10665722E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.10665722E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.10477059E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.29165288E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.29165288E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.29163634E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.92642326E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     7.68489564E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.92642326E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     7.68489564E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.80522684E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.76074839E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.76074839E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.58785300E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.51908485E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.51908485E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.51908491E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     4.10652245E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     4.10652245E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     4.10652245E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     4.10652245E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.36882778E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.36882778E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.36882778E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.36882778E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.28190320E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.28190320E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.63816279E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.68050653E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.91149869E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.66901090E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.90273456E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.90273456E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.14186891E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.09867491E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.08931115E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.90290923E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.90290923E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.54812232E-06    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.54812232E-06    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.90926517E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.90926517E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.00423161E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92479739E-01    2           5        -5   # BR(h -> b       bb     )
     6.50554526E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30295949E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.88411548E-04    2           3        -3   # BR(h -> s       sb     )
     2.12124601E-02    2           4        -4   # BR(h -> c       cb     )
     6.82148629E-02    2          21        21   # BR(h -> g       g      )
     2.40649339E-03    2          22        22   # BR(h -> gam     gam    )
     1.63485712E-03    2          22        23   # BR(h -> Z       gam    )
     2.20408771E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78686568E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.60721634E+01   # H decays
#          BR         NDA      ID1       ID2
     3.67216018E-01    2           5        -5   # BR(H -> b       bb     )
     5.91309396E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.09072680E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.47296939E-04    2           3        -3   # BR(H -> s       sb     )
     6.93616167E-08    2           4        -4   # BR(H -> c       cb     )
     6.95147810E-03    2           6        -6   # BR(H -> t       tb     )
     3.10365052E-07    2          21        21   # BR(H -> g       g      )
     3.90687401E-09    2          22        22   # BR(H -> gam     gam    )
     1.78879862E-09    2          23        22   # BR(H -> Z       gam    )
     1.73894003E-06    2          24       -24   # BR(H -> W+      W-     )
     8.68867810E-07    2          23        23   # BR(H -> Z       Z      )
     6.78251447E-06    2          25        25   # BR(H -> h       h      )
     5.08745949E-24    2          36        36   # BR(H -> A       A      )
     7.42672098E-20    2          23        36   # BR(H -> Z       A      )
     1.83617727E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.54892403E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.54892403E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.13369532E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.64520288E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.32136033E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.61966825E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.09853883E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.25393532E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.10244115E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.03849679E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.24158024E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.76051138E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     8.46712295E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     7.59594265E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     7.59594265E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.44797588E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.60719100E+01   # A decays
#          BR         NDA      ID1       ID2
     3.67243468E-01    2           5        -5   # BR(A -> b       bb     )
     5.91315043E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09074513E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.47339444E-04    2           3        -3   # BR(A -> s       sb     )
     6.98105703E-08    2           4        -4   # BR(A -> c       cb     )
     6.96485714E-03    2           6        -6   # BR(A -> t       tb     )
     1.43110227E-05    2          21        21   # BR(A -> g       g      )
     6.15748235E-08    2          22        22   # BR(A -> gam     gam    )
     1.57408230E-08    2          23        22   # BR(A -> Z       gam    )
     1.73529661E-06    2          23        25   # BR(A -> Z       h      )
     1.85771297E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.54882289E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.54882289E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.85402313E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.20091145E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.12083897E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.15927653E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.07448838E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.45689442E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.21948581E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.36125368E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.71750807E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     7.93894384E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     7.93894384E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.62668493E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.91061256E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.89410941E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08401269E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.78278885E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18063405E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.42894391E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.75974689E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.73116262E-06    2          24        25   # BR(H+ -> W+      h      )
     2.12633847E-14    2          24        36   # BR(H+ -> W+      A      )
     5.84379913E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.70194542E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.51750009E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.54554828E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.70283459E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.53494086E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.09431960E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.48660136E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.55266493E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11087423E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.60900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.20485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04157663E+01   # W+
        25     1.24091212E+02   # h
        35     4.00000601E+03   # H
        36     3.99999636E+03   # A
        37     4.00102300E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02013203E+03   # ~d_L
   2000001     4.01790710E+03   # ~d_R
   1000002     4.01946890E+03   # ~u_L
   2000002     4.01921382E+03   # ~u_R
   1000003     4.02013203E+03   # ~s_L
   2000003     4.01790710E+03   # ~s_R
   1000004     4.01946890E+03   # ~c_L
   2000004     4.01921382E+03   # ~c_R
   1000005     6.22247506E+02   # ~b_1
   2000005     4.02213371E+03   # ~b_2
   1000006     6.14564291E+02   # ~t_1
   2000006     2.22287632E+03   # ~t_2
   1000011     4.00360557E+03   # ~e_L
   2000011     4.00290809E+03   # ~e_R
   1000012     4.00248171E+03   # ~nu_eL
   1000013     4.00360557E+03   # ~mu_L
   2000013     4.00290809E+03   # ~mu_R
   1000014     4.00248171E+03   # ~nu_muL
   1000015     4.00485395E+03   # ~tau_1
   2000015     4.00748555E+03   # ~tau_2
   1000016     4.00442722E+03   # ~nu_tauL
   1000021     1.97171234E+03   # ~g
   1000022     2.02257190E+02   # ~chi_10
   1000023    -2.71085670E+02   # ~chi_20
   1000025     2.79570732E+02   # ~chi_30
   1000035     2.05492909E+03   # ~chi_40
   1000024     2.68027721E+02   # ~chi_1+
   1000037     2.05509356E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.99116018E-01   # N_11
  1  2    -1.04246527E-02   # N_12
  1  3    -3.52190248E-01   # N_13
  1  4    -2.59699330E-01   # N_14
  2  1    -6.91516173E-02   # N_21
  2  2     2.55162709E-02   # N_22
  2  3    -7.00624750E-01   # N_23
  2  4     7.09712571E-01   # N_24
  3  1     4.32212357E-01   # N_31
  3  2     2.79835926E-02   # N_32
  3  3     6.20545722E-01   # N_33
  3  4     6.53706665E-01   # N_34
  4  1    -9.58130276E-04   # N_41
  4  2     9.99228285E-01   # N_42
  4  3    -3.16166865E-03   # N_43
  4  4    -3.91397593E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.47648912E-03   # U_11
  1  2     9.99989980E-01   # U_12
  2  1    -9.99989980E-01   # U_21
  2  2     4.47648912E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.53645531E-02   # V_11
  1  2    -9.98466207E-01   # V_12
  2  1    -9.98466207E-01   # V_21
  2  2    -5.53645531E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.97245398E-01   # cos(theta_t)
  1  2    -7.41728803E-02   # sin(theta_t)
  2  1     7.41728803E-02   # -sin(theta_t)
  2  2     9.97245398E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999672E-01   # cos(theta_b)
  1  2    -8.09938203E-04   # sin(theta_b)
  2  1     8.09938203E-04   # -sin(theta_b)
  2  2     9.99999672E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05239566E-01   # cos(theta_tau)
  1  2     7.08969079E-01   # sin(theta_tau)
  2  1    -7.08969079E-01   # -sin(theta_tau)
  2  2    -7.05239566E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00266381E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.10874227E+03  # DRbar Higgs Parameters
         1    -2.60900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44166348E+02   # higgs               
         4     1.61905802E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.10874227E+03  # The gauge couplings
     1     3.61442882E-01   # gprime(Q) DRbar
     2     6.36350538E-01   # g(Q) DRbar
     3     1.03386173E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.10874227E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     9.49402332E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.10874227E+03  # The trilinear couplings
  1  1     3.50236190E-07   # A_d(Q) DRbar
  2  2     3.50269543E-07   # A_s(Q) DRbar
  3  3     6.23567684E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.10874227E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.63929262E-08   # A_mu(Q) DRbar
  3  3     7.71709697E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.10874227E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68238105E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.10874227E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82421521E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.10874227E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03417292E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.10874227E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58117659E+07   # M^2_Hd              
        22    -8.75664358E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.20485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40494118E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.74365296E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.46299753E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.46299753E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.53700247E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.53700247E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.27766207E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     8.59866028E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.24237548E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.70668958E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.19106891E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.20797705E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.08971736E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.30217008E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.14449137E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.57558132E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.60262948E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.64693225E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.24325170E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.13835802E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.84700430E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.59091715E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.95501256E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.63017039E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.32157999E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.70932371E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66739192E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.52252528E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.75843624E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62274326E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.74935248E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55921059E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.35779594E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15038615E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.39360501E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.56748358E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.61348475E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.61532588E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78650520E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.21837673E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.33569900E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.04301073E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78086079E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.30619428E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54953565E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52890672E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61240061E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.46218562E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.62952258E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.02666270E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.79390927E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44848537E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78671259E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.83513580E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29003776E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.50798086E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78568298E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.46911270E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58140692E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53110694E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54457521E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.16414284E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.86016372E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.67846026E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.28576724E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85611502E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78650520E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.21837673E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.33569900E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.04301073E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78086079E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.30619428E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54953565E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52890672E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61240061E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.46218562E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.62952258E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.02666270E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.79390927E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44848537E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78671259E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.83513580E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29003776E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.50798086E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78568298E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.46911270E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58140692E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53110694E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54457521E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.16414284E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.86016372E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.67846026E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.28576724E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85611502E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14347998E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27737426E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.62646244E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.79953546E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77587148E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.03768476E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56563429E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06827448E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.09129499E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.76671136E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86103288E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.00907260E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14347998E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27737426E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.62646244E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.79953546E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77587148E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.03768476E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56563429E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06827448E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.09129499E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.76671136E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86103288E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.00907260E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09925902E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.92082909E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28005843E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.07383834E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39734460E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44750411E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80168623E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09920166E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.04216823E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40026129E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.53297128E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42256896E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10015876E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85224952E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14454130E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.38520475E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13275056E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.40201636E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77908887E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11497667E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54302746E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14454130E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.38520475E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13275056E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.40201636E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77908887E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11497667E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54302746E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47459136E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.25424030E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.93111693E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.17492715E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51807714E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.68453249E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02242543E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.56621480E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33452675E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33452675E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11151769E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11151769E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10791112E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.30817832E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.78470618E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.68455279E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.43712463E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.21878722E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.29579465E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.52927751E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.32333478E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.97469272E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.58743082E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17512065E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52391211E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17512065E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52391211E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45744308E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49213621E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49213621E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47052362E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98135141E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98135141E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98135107E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.48069440E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.48069440E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.48069440E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.48069440E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.16023297E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.16023297E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.16023297E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.16023297E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.08172985E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.08172985E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.88053127E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.46025252E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.31985219E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.16094457E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.50332977E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.16094457E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.50332977E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.45784974E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.42722888E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.42722888E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.41561243E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.89482523E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.89482523E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.89481860E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.03091836E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.33697652E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.03091836E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.33697652E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.06419864E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.06419864E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.30721763E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     6.12448040E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     6.12448040E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     6.12448048E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.61783346E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.61783346E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.61783346E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.61783346E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.39273445E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.39273445E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.39273445E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.39273445E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.80608783E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.80608783E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.30704757E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.51090815E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.75543084E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.32957456E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.27983612E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.27983612E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.81794969E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.64596132E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.27901170E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.85877640E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.85877640E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.87609678E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.87609678E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.85207211E-03   # h decays
#          BR         NDA      ID1       ID2
     6.15845643E-01    2           5        -5   # BR(h -> b       bb     )
     6.68561999E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.36677136E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.03061731E-04    2           3        -3   # BR(h -> s       sb     )
     2.18488361E-02    2           4        -4   # BR(h -> c       cb     )
     7.03689043E-02    2          21        21   # BR(h -> g       g      )
     2.36564006E-03    2          22        22   # BR(h -> gam     gam    )
     1.49491728E-03    2          22        23   # BR(h -> Z       gam    )
     1.96254237E-01    2          24       -24   # BR(h -> W+      W-     )
     2.42258834E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.47500787E+01   # H decays
#          BR         NDA      ID1       ID2
     3.48732433E-01    2           5        -5   # BR(H -> b       bb     )
     6.05588065E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14121272E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53268554E-04    2           3        -3   # BR(H -> s       sb     )
     7.10365959E-08    2           4        -4   # BR(H -> c       cb     )
     7.11934588E-03    2           6        -6   # BR(H -> t       tb     )
     8.82798314E-07    2          21        21   # BR(H -> g       g      )
     1.63756323E-10    2          22        22   # BR(H -> gam     gam    )
     1.82992570E-09    2          23        22   # BR(H -> Z       gam    )
     1.78213768E-06    2          24       -24   # BR(H -> W+      W-     )
     8.90451421E-07    2          23        23   # BR(H -> Z       Z      )
     6.93725657E-06    2          25        25   # BR(H -> h       h      )
    -9.95405434E-25    2          36        36   # BR(H -> A       A      )
     6.67889477E-20    2          23        36   # BR(H -> Z       A      )
     1.86025557E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63149251E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63149251E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.95948694E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03915172E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.05714889E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.83377247E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.84084537E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.02785713E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.47844392E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.98493196E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.35047152E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     1.63619372E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.86310117E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.86310117E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.41136536E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.47466436E+01   # A decays
#          BR         NDA      ID1       ID2
     3.48779941E-01    2           5        -5   # BR(A -> b       bb     )
     6.05629217E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14135654E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53326874E-04    2           3        -3   # BR(A -> s       sb     )
     7.15004987E-08    2           4        -4   # BR(A -> c       cb     )
     7.13345783E-03    2           6        -6   # BR(A -> t       tb     )
     1.46574529E-05    2          21        21   # BR(A -> g       g      )
     5.34870324E-08    2          22        22   # BR(A -> gam     gam    )
     1.61084658E-08    2          23        22   # BR(A -> Z       gam    )
     1.77862654E-06    2          23        25   # BR(A -> Z       h      )
     1.86404058E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63161534E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63161534E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.70811174E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29683699E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.73153620E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.50710333E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48298785E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.03730281E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.04244933E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.07530489E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.24813066E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.90512602E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.90512602E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.47779255E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.58613139E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.05438713E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14068296E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.57512103E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21273713E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49499026E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.55894924E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.77960238E-06    2          24        25   # BR(H+ -> W+      h      )
     2.73975735E-14    2          24        36   # BR(H+ -> W+      A      )
     6.82003721E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.84963104E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.97688172E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63279647E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.06840598E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60617297E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.26110099E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     8.66840376E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.75607011E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

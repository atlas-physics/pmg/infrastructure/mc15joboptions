#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15500897E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03975688E+01   # W+
        25     1.23985987E+02   # h
        35     3.00011725E+03   # H
        36     2.99999986E+03   # A
        37     3.00090762E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03727813E+03   # ~d_L
   2000001     3.03211321E+03   # ~d_R
   1000002     3.03636215E+03   # ~u_L
   2000002     3.03334709E+03   # ~u_R
   1000003     3.03727813E+03   # ~s_L
   2000003     3.03211321E+03   # ~s_R
   1000004     3.03636215E+03   # ~c_L
   2000004     3.03334709E+03   # ~c_R
   1000005     8.95531707E+02   # ~b_1
   2000005     3.03124043E+03   # ~b_2
   1000006     8.93946978E+02   # ~t_1
   2000006     3.01791439E+03   # ~t_2
   1000011     3.00639672E+03   # ~e_L
   2000011     3.00192294E+03   # ~e_R
   1000012     3.00499934E+03   # ~nu_eL
   1000013     3.00639672E+03   # ~mu_L
   2000013     3.00192294E+03   # ~mu_R
   1000014     3.00499934E+03   # ~nu_muL
   1000015     2.98575857E+03   # ~tau_1
   2000015     3.02196245E+03   # ~tau_2
   1000016     3.00482697E+03   # ~nu_tauL
   1000021     2.34597987E+03   # ~g
   1000022     1.50587154E+02   # ~chi_10
   1000023     3.20598384E+02   # ~chi_20
   1000025    -2.99687422E+03   # ~chi_30
   1000035     2.99744300E+03   # ~chi_40
   1000024     3.20760398E+02   # ~chi_1+
   1000037     2.99809245E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888301E-01   # N_11
  1  2    -1.15223063E-03   # N_12
  1  3     1.48281008E-02   # N_13
  1  4    -1.47814063E-03   # N_14
  2  1     1.54826982E-03   # N_21
  2  2     9.99644310E-01   # N_22
  2  3    -2.63333116E-02   # N_23
  2  4     3.92593553E-03   # N_24
  3  1    -9.41866570E-03   # N_31
  3  2     1.58575627E-02   # N_32
  3  3     7.06841988E-01   # N_33
  3  4     7.07130986E-01   # N_34
  4  1    -1.15011611E-02   # N_41
  4  2     2.14117603E-02   # N_42
  4  3     7.06725611E-01   # N_43
  4  4    -7.07070132E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99305562E-01   # U_11
  1  2    -3.72611514E-02   # U_12
  2  1     3.72611514E-02   # U_21
  2  2     9.99305562E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984563E-01   # V_11
  1  2    -5.55636384E-03   # V_12
  2  1     5.55636384E-03   # V_21
  2  2     9.99984563E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98805935E-01   # cos(theta_t)
  1  2    -4.88539068E-02   # sin(theta_t)
  2  1     4.88539068E-02   # -sin(theta_t)
  2  2     9.98805935E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99912633E-01   # cos(theta_b)
  1  2     1.32184102E-02   # sin(theta_b)
  2  1    -1.32184102E-02   # -sin(theta_b)
  2  2     9.99912633E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06927425E-01   # cos(theta_tau)
  1  2     7.07286092E-01   # sin(theta_tau)
  2  1    -7.07286092E-01   # -sin(theta_tau)
  2  2     7.06927425E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01808029E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.55008969E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44191103E+02   # higgs               
         4     7.59291131E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.55008969E+03  # The gauge couplings
     1     3.62283590E-01   # gprime(Q) DRbar
     2     6.38988109E-01   # g(Q) DRbar
     3     1.02562106E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.55008969E+03  # The trilinear couplings
  1  1     2.26874883E-06   # A_u(Q) DRbar
  2  2     2.26878328E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.55008969E+03  # The trilinear couplings
  1  1     5.79355274E-07   # A_d(Q) DRbar
  2  2     5.79466878E-07   # A_s(Q) DRbar
  3  3     1.31785797E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.55008969E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.26493739E-07   # A_mu(Q) DRbar
  3  3     1.28029000E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.55008969E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51026959E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.55008969E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13585929E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.55008969E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05711034E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.55008969E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.29532747E+04   # M^2_Hd              
        22    -9.04801898E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41741501E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.93967320E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48209106E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48209106E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51790894E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51790894E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     8.35898156E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.39677358E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.08828964E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.77203301E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.00998813E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.52496630E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.20238500E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.43073244E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.82503289E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.32781190E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.56249247E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.49543897E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.95134442E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.15819676E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.57170802E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.48554675E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.35728245E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.20826968E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.95628727E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.11323504E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.75369172E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.71924280E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.14367903E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29637942E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.88128368E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.18237699E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.01275231E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02114556E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.86938024E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63698927E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.55138491E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.63248265E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27421979E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.48003771E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.03009634E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17422596E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59738277E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.76836481E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.41744521E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.36199533E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40261323E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02620550E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.01184696E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63442433E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.18311757E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.27698717E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26847618E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.51841705E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.03697635E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66414889E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54745600E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.07276966E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.50959269E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.62555762E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54525327E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02114556E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.86938024E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63698927E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.55138491E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.63248265E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27421979E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.48003771E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.03009634E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17422596E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59738277E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.76836481E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.41744521E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.36199533E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40261323E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02620550E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.01184696E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63442433E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.18311757E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.27698717E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26847618E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.51841705E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.03697635E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66414889E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54745600E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.07276966E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.50959269E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.62555762E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54525327E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96703952E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.80236930E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01151358E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.36202257E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.41431434E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00824920E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.60122488E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55945392E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997643E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35512478E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.00729341E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.18283666E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96703952E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.80236930E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01151358E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.36202257E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.41431434E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00824920E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.60122488E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55945392E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997643E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35512478E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.00729341E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.18283666E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79186275E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48724387E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17455866E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33819747E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73246858E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56987619E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14708034E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.25809030E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41408640E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28263300E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.43255393E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96734166E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.87699593E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99926669E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.03143894E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.05552606E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01303361E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.00478634E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96734166E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.87699593E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99926669E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.03143894E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.05552606E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01303361E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.00478634E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96744358E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.87616995E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99900968E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.86254732E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.78292260E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01336080E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.24271823E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.64525720E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.25627961E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.68885685E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.13338849E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.02513317E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.21035618E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.23817542E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.03651419E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.49217222E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.91089593E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.89502616E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.51049738E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.28138030E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.38264590E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.63327910E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.00254024E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.00254024E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.56803645E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.59109869E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.59654649E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.59654649E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.25004884E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.25004884E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.23535201E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.23535201E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.19429404E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.37182448E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.57076652E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.07937103E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.07937103E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.44535368E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.91941245E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.57073801E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.57073801E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27684275E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27684275E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.85312924E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.85312924E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.69231692E-03   # h decays
#          BR         NDA      ID1       ID2
     6.86383264E-01    2           5        -5   # BR(h -> b       bb     )
     5.51759433E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.95328353E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.15209572E-04    2           3        -3   # BR(h -> s       sb     )
     1.79238901E-02    2           4        -4   # BR(h -> c       cb     )
     5.74514201E-02    2          21        21   # BR(h -> g       g      )
     1.93808593E-03    2          22        22   # BR(h -> gam     gam    )
     1.21464324E-03    2          22        23   # BR(h -> Z       gam    )
     1.59679272E-01    2          24       -24   # BR(h -> W+      W-     )
     1.96229441E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40117615E+01   # H decays
#          BR         NDA      ID1       ID2
     7.46619923E-01    2           5        -5   # BR(H -> b       bb     )
     1.77476065E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.27512954E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.70586545E-04    2           3        -3   # BR(H -> s       sb     )
     2.17433456E-07    2           4        -4   # BR(H -> c       cb     )
     2.16903852E-02    2           6        -6   # BR(H -> t       tb     )
     2.69151143E-05    2          21        21   # BR(H -> g       g      )
     1.07589685E-08    2          22        22   # BR(H -> gam     gam    )
     8.35077904E-09    2          23        22   # BR(H -> Z       gam    )
     3.11485021E-05    2          24       -24   # BR(H -> W+      W-     )
     1.55550428E-05    2          23        23   # BR(H -> Z       Z      )
     8.26114120E-05    2          25        25   # BR(H -> h       h      )
    -2.89510717E-23    2          36        36   # BR(H -> A       A      )
     1.88710827E-18    2          23        36   # BR(H -> Z       A      )
     2.31031488E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11727898E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.15128346E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.20333957E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.83596149E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.78573611E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33384842E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84382272E-01    2           5        -5   # BR(A -> b       bb     )
     1.86431521E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.59176387E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.09605487E-04    2           3        -3   # BR(A -> s       sb     )
     2.28459192E-07    2           4        -4   # BR(A -> c       cb     )
     2.27777276E-02    2           6        -6   # BR(A -> t       tb     )
     6.70782304E-05    2          21        21   # BR(A -> g       g      )
     3.12322381E-08    2          22        22   # BR(A -> gam     gam    )
     6.60377479E-08    2          23        22   # BR(A -> Z       gam    )
     3.25991264E-05    2          23        25   # BR(A -> Z       h      )
     2.62028311E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.20953178E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30570244E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.92755734E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.02389154E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10616299E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.42942367E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.58984955E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.07943210E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.04807492E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03855134E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.19480247E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.25328208E-05    2          24        25   # BR(H+ -> W+      h      )
     7.92130595E-14    2          24        36   # BR(H+ -> W+      A      )
     2.01275332E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.80659925E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.24608347E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10036483E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.86900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04171856E+01   # W+
        25     1.24915762E+02   # h
        35     4.00000261E+03   # H
        36     3.99999561E+03   # A
        37     4.00102573E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01654645E+03   # ~d_L
   2000001     4.01509721E+03   # ~d_R
   1000002     4.01588593E+03   # ~u_L
   2000002     4.01633219E+03   # ~u_R
   1000003     4.01654645E+03   # ~s_L
   2000003     4.01509721E+03   # ~s_R
   1000004     4.01588593E+03   # ~c_L
   2000004     4.01633219E+03   # ~c_R
   1000005     5.49573899E+02   # ~b_1
   2000005     4.02015149E+03   # ~b_2
   1000006     5.34847950E+02   # ~t_1
   2000006     2.02377352E+03   # ~t_2
   1000011     4.00280247E+03   # ~e_L
   2000011     4.00298811E+03   # ~e_R
   1000012     4.00168175E+03   # ~nu_eL
   1000013     4.00280247E+03   # ~mu_L
   2000013     4.00298811E+03   # ~mu_R
   1000014     4.00168175E+03   # ~nu_muL
   1000015     4.00479732E+03   # ~tau_1
   2000015     4.00777755E+03   # ~tau_2
   1000016     4.00394784E+03   # ~nu_tauL
   1000021     1.96407229E+03   # ~g
   1000022     1.46840229E+02   # ~chi_10
   1000023    -1.96496082E+02   # ~chi_20
   1000025     2.10068557E+02   # ~chi_30
   1000035     2.05625837E+03   # ~chi_40
   1000024     1.92825059E+02   # ~chi_1+
   1000037     2.05642290E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.19583553E-01   # N_11
  1  2    -1.33207655E-02   # N_12
  1  3    -4.59834275E-01   # N_13
  1  4    -3.41552041E-01   # N_14
  2  1    -9.35502698E-02   # N_21
  2  2     2.64442642E-02   # N_22
  2  3    -6.95962631E-01   # N_23
  2  4     7.11466840E-01   # N_24
  3  1     5.65270099E-01   # N_31
  3  2     2.53314179E-02   # N_32
  3  3     5.51530224E-01   # N_33
  3  4     6.12896766E-01   # N_34
  4  1    -9.28449863E-04   # N_41
  4  2     9.99240500E-01   # N_42
  4  3    -1.69345362E-03   # N_43
  4  4    -3.89190549E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.39963906E-03   # U_11
  1  2     9.99997121E-01   # U_12
  2  1    -9.99997121E-01   # U_21
  2  2     2.39963906E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.50528781E-02   # V_11
  1  2    -9.98483440E-01   # V_12
  2  1    -9.98483440E-01   # V_21
  2  2    -5.50528781E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95968408E-01   # cos(theta_t)
  1  2    -8.97046836E-02   # sin(theta_t)
  2  1     8.97046836E-02   # -sin(theta_t)
  2  2     9.95968408E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999833E-01   # cos(theta_b)
  1  2    -5.77927307E-04   # sin(theta_b)
  2  1     5.77927307E-04   # -sin(theta_b)
  2  2     9.99999833E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04336056E-01   # cos(theta_tau)
  1  2     7.09866692E-01   # sin(theta_tau)
  2  1    -7.09866692E-01   # -sin(theta_tau)
  2  2    -7.04336056E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00295167E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00364831E+03  # DRbar Higgs Parameters
         1    -1.86900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44294953E+02   # higgs               
         4     1.61719202E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00364831E+03  # The gauge couplings
     1     3.61186654E-01   # gprime(Q) DRbar
     2     6.36559563E-01   # g(Q) DRbar
     3     1.03613230E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00364831E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.52304490E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00364831E+03  # The trilinear couplings
  1  1     2.76316986E-07   # A_d(Q) DRbar
  2  2     2.76343713E-07   # A_s(Q) DRbar
  3  3     4.93336833E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00364831E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     6.02270713E-08   # A_mu(Q) DRbar
  3  3     6.08381453E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00364831E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72397163E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00364831E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82434341E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00364831E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03670499E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00364831E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58382351E+07   # M^2_Hd              
        22    -4.63257043E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40580216E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.01619160E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45203188E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45203188E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54796812E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54796812E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.15137677E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.28100428E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.22236899E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.25862086E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.23800587E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18128199E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.56980748E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.23431068E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.85207015E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.41963562E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.45175896E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.08286129E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.26924569E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.65643143E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.77758401E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.63618067E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.53807062E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.70481647E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66438456E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.65189010E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.73638107E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.50950427E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.30579132E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.55054394E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.48655272E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15352210E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.82797938E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.67562104E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.89806879E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.00358045E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78238414E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.75117941E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.66644360E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.52155604E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78003243E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.27805631E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.54786209E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52913847E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60952131E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.71406442E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.82970003E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.76224922E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.61724526E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44753867E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78258335E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47176315E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69613657E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.79081061E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78476200E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.00283776E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.57957273E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.53135192E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54176899E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.69014020E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.26008627E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.59777042E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.82556736E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85586074E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78238414E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.75117941E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.66644360E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.52155604E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78003243E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.27805631E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.54786209E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52913847E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60952131E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.71406442E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.82970003E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.76224922E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.61724526E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44753867E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78258335E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47176315E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69613657E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.79081061E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78476200E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.00283776E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.57957273E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.53135192E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54176899E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.69014020E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.26008627E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.59777042E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.82556736E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85586074E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14187018E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.04499096E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.62589257E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.11620168E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77535575E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.88651188E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56434836E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07032578E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.72335486E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.74105351E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.18922992E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.68810595E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14187018E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.04499096E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.62589257E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.11620168E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77535575E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.88651188E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56434836E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07032578E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.72335486E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.74105351E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.18922992E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.68810595E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10091240E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52994248E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.15248491E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.32176639E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39454223E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.42565601E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79593481E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10633459E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.46734964E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73662240E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.07125104E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42097059E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.17840108E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84892638E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14294612E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17115940E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.22994704E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.45372578E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77836784E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09638384E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54183687E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14294612E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17115940E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.22994704E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.45372578E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77836784E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09638384E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54183687E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47574974E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05962363E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92234899E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.02958658E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51580287E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.74280994E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01811035E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.20120450E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33602024E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33602024E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11201736E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11201736E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10392479E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.52746917E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.81783711E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     8.58451631E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.70960896E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.29178716E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.28599099E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.17788701E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.80476020E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.16510536E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.76614533E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.18571046E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18130399E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53167596E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18130399E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53167596E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40881850E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50848674E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50848674E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47407826E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01359154E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01359154E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01359116E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.28057077E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.28057077E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.28057077E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.28057077E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.09352545E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.09352545E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.09352545E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.09352545E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.47527229E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.47527229E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.80068457E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.28867437E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.26225764E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.80513375E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.00936617E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.80513375E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.00936617E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.51733496E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.29112396E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.29112396E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.27378636E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.63193084E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.63193084E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.63192122E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.40607904E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.41659943E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.40607904E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.41659943E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.09321983E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.01186718E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.01186718E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     9.16101094E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.02219271E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.02219271E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.02219276E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.82292573E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.82292573E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.82292573E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.82292573E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.27429240E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.27429240E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.27429240E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.27429240E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.20947675E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.20947675E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.52641964E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.58812223E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.45651595E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.17529785E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.14076234E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.14076234E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.45969956E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.81101870E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.69159001E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.87639272E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.87639272E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.88757081E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.88757081E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.96380121E-03   # h decays
#          BR         NDA      ID1       ID2
     6.04606324E-01    2           5        -5   # BR(h -> b       bb     )
     6.54119984E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.31560791E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.91551193E-04    2           3        -3   # BR(h -> s       sb     )
     2.13464182E-02    2           4        -4   # BR(h -> c       cb     )
     6.91255873E-02    2          21        21   # BR(h -> g       g      )
     2.36887504E-03    2          22        22   # BR(h -> gam     gam    )
     1.56555299E-03    2          22        23   # BR(h -> Z       gam    )
     2.08724253E-01    2          24       -24   # BR(h -> W+      W-     )
     2.61278786E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.46792486E+01   # H decays
#          BR         NDA      ID1       ID2
     3.45197529E-01    2           5        -5   # BR(H -> b       bb     )
     6.06371838E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14398395E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.53596370E-04    2           3        -3   # BR(H -> s       sb     )
     7.11367412E-08    2           4        -4   # BR(H -> c       cb     )
     7.12938245E-03    2           6        -6   # BR(H -> t       tb     )
     9.46078630E-07    2          21        21   # BR(H -> g       g      )
     7.48647800E-12    2          22        22   # BR(H -> gam     gam    )
     1.83084688E-09    2          23        22   # BR(H -> Z       gam    )
     1.93815922E-06    2          24       -24   # BR(H -> W+      W-     )
     9.68408221E-07    2          23        23   # BR(H -> Z       Z      )
     7.32772326E-06    2          25        25   # BR(H -> h       h      )
     5.12930762E-24    2          36        36   # BR(H -> A       A      )
    -6.79687469E-21    2          23        36   # BR(H -> Z       A      )
     1.85935169E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.64984104E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.64984104E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.84862661E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.55605382E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.58287946E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.11207952E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.23556186E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.57292783E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.59229898E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.74682982E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.22619001E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.52383205E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.11889286E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.11889286E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.41388844E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.46773329E+01   # A decays
#          BR         NDA      ID1       ID2
     3.45235496E-01    2           5        -5   # BR(A -> b       bb     )
     6.06396817E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14407058E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.53647958E-04    2           3        -3   # BR(A -> s       sb     )
     7.15911232E-08    2           4        -4   # BR(A -> c       cb     )
     7.14249925E-03    2           6        -6   # BR(A -> t       tb     )
     1.46760342E-05    2          21        21   # BR(A -> g       g      )
     4.69560440E-08    2          22        22   # BR(A -> gam     gam    )
     1.61328720E-08    2          23        22   # BR(A -> Z       gam    )
     1.93421646E-06    2          23        25   # BR(A -> Z       h      )
     1.86059533E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.64993590E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.64993590E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.46836894E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.92838024E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.28113583E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.69100686E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.90845576E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.43837686E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.79108782E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.27508632E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.83051752E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     5.28555182E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     5.28555182E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.46242449E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.51366044E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.07142477E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.14670705E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.52873965E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21614979E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.50201119E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.51388495E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.93827045E-06    2          24        25   # BR(H+ -> W+      h      )
     2.79434223E-14    2          24        36   # BR(H+ -> W+      A      )
     5.60997626E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.43147512E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.25977927E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.65365321E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.02653224E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60515481E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00795495E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     5.13380191E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.02973034E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

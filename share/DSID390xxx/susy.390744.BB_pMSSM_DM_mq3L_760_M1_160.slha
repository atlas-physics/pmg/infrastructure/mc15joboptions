#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13069589E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     7.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.26485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04149855E+01   # W+
        25     1.25494103E+02   # h
        35     4.00000382E+03   # H
        36     3.99999673E+03   # A
        37     4.00105351E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02605498E+03   # ~d_L
   2000001     4.02261383E+03   # ~d_R
   1000002     4.02539883E+03   # ~u_L
   2000002     4.02368357E+03   # ~u_R
   1000003     4.02605498E+03   # ~s_L
   2000003     4.02261383E+03   # ~s_R
   1000004     4.02539883E+03   # ~c_L
   2000004     4.02368357E+03   # ~c_R
   1000005     8.33508968E+02   # ~b_1
   2000005     4.02539923E+03   # ~b_2
   1000006     8.22471347E+02   # ~t_1
   2000006     2.27842482E+03   # ~t_2
   1000011     4.00475829E+03   # ~e_L
   2000011     4.00317964E+03   # ~e_R
   1000012     4.00363980E+03   # ~nu_eL
   1000013     4.00475829E+03   # ~mu_L
   2000013     4.00317964E+03   # ~mu_R
   1000014     4.00363980E+03   # ~nu_muL
   1000015     4.00528866E+03   # ~tau_1
   2000015     4.00694347E+03   # ~tau_2
   1000016     4.00507103E+03   # ~nu_tauL
   1000021     1.98157784E+03   # ~g
   1000022     1.46070805E+02   # ~chi_10
   1000023    -1.96000561E+02   # ~chi_20
   1000025     2.09463751E+02   # ~chi_30
   1000035     2.05243558E+03   # ~chi_40
   1000024     1.92193366E+02   # ~chi_1+
   1000037     2.05260061E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15343111E-01   # N_11
  1  2    -1.34635797E-02   # N_12
  1  3    -4.64075926E-01   # N_13
  1  4    -3.45930454E-01   # N_14
  2  1    -9.37882387E-02   # N_21
  2  2     2.64000024E-02   # N_22
  2  3    -6.95904444E-01   # N_23
  2  4     7.11494070E-01   # N_24
  3  1     5.71330483E-01   # N_31
  3  2     2.51673104E-02   # N_32
  3  3     5.48039917E-01   # N_33
  3  4     6.10409973E-01   # N_34
  4  1    -9.26105143E-04   # N_41
  4  2     9.99243903E-01   # N_42
  4  3    -1.67019776E-03   # N_43
  4  4    -3.88326459E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36670804E-03   # U_11
  1  2     9.99997199E-01   # U_12
  2  1    -9.99997199E-01   # U_21
  2  2     2.36670804E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49304827E-02   # V_11
  1  2    -9.98490181E-01   # V_12
  2  1    -9.98490181E-01   # V_21
  2  2    -5.49304827E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96288802E-01   # cos(theta_t)
  1  2    -8.60733583E-02   # sin(theta_t)
  2  1     8.60733583E-02   # -sin(theta_t)
  2  2     9.96288802E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999831E-01   # cos(theta_b)
  1  2    -5.81377650E-04   # sin(theta_b)
  2  1     5.81377650E-04   # -sin(theta_b)
  2  2     9.99999831E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04617200E-01   # cos(theta_tau)
  1  2     7.09587628E-01   # sin(theta_tau)
  2  1    -7.09587628E-01   # -sin(theta_tau)
  2  2    -7.04617200E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00307580E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30695894E+03  # DRbar Higgs Parameters
         1    -1.85900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43625031E+02   # higgs               
         4     1.61065357E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30695894E+03  # The gauge couplings
     1     3.62057377E-01   # gprime(Q) DRbar
     2     6.36930351E-01   # g(Q) DRbar
     3     1.03009399E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30695894E+03  # The trilinear couplings
  1  1     1.38237795E-06   # A_u(Q) DRbar
  2  2     1.38239110E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30695894E+03  # The trilinear couplings
  1  1     5.06974200E-07   # A_d(Q) DRbar
  2  2     5.07021821E-07   # A_s(Q) DRbar
  3  3     9.07657390E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30695894E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.08761766E-07   # A_mu(Q) DRbar
  3  3     1.09873128E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30695894E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.66403379E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30695894E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80396645E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30695894E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03957565E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30695894E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58551902E+07   # M^2_Hd              
        22    -3.84570941E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     7.59899996E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.26485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40753681E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.74116768E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44985235E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44985235E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55014765E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55014765E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.10913888E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.10461595E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.42491250E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.44662562E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.02384593E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.37509853E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.44964445E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20172177E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.57051055E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     9.56061512E-08    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.35444586E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.05327581E-08    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     7.07985765E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.34866003E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.25372855E-02    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.95979766E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.14217365E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.06687307E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.87742436E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.11824289E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.89374597E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66723820E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.66581610E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.74750574E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.51414880E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.29649168E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.57407441E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.46804419E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14828733E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.62108719E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.42870193E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.48570914E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     8.47089296E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.79003898E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.73413485E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.47978457E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.54822696E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.80642377E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.25257256E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.60064626E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52115201E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61337997E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.69264930E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.87666608E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.80854911E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.62955002E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44500323E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.79024330E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.45969849E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.69801146E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.00451504E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81113609E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.75119357E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.63232160E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52334497E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54529328E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.63575027E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.27253593E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.71929581E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.85913640E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85517693E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.79003898E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.73413485E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.47978457E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.54822696E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.80642377E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.25257256E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.60064626E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52115201E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61337997E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.69264930E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.87666608E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.80854911E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.62955002E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44500323E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.79024330E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.45969849E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.69801146E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.00451504E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81113609E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.75119357E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.63232160E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52334497E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54529328E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.63575027E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.27253593E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.71929581E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.85913640E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85517693E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15779455E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03363284E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.68909990E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.23263755E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77525370E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70680505E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56410353E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08044040E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.65408468E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.78570912E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25805355E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.67715915E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15779455E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03363284E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.68909990E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.23263755E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77525370E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70680505E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56410353E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08044040E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.65408468E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.78570912E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25805355E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.67715915E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11448690E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.50695015E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.14613574E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34428012E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39535001E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41272387E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79753375E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11772218E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44411035E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73335620E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.09971236E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41934682E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.17845114E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84564973E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15884800E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16031892E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23433794E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.56577322E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77829789E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.07229707E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54173951E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15884800E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16031892E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23433794E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.56577322E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77829789E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.07229707E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54173951E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49124609E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05022340E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92745601E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.13256346E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51595887E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.72827456E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01845937E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.46760820E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33600462E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33600462E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11201211E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11201211E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10396653E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.65844220E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.64597234E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     3.52033566E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.20074401E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.51188789E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.06453240E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.45316621E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.10658951E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.17516342E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.19617361E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18093008E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53163040E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18093008E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53163040E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40710903E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51084487E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51084487E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47665832E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01921717E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01921717E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01921693E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.74405101E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.74405101E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.74405101E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.74405101E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.24801913E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.24801913E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.24801913E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.24801913E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.32087805E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.32087805E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.61024901E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.81888522E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     6.21421210E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.69833026E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     9.95757451E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.69833026E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     9.95757451E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.41078185E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.26113461E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.26113461E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.24416763E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.57361945E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.57361945E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.57361326E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.31267143E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.29671147E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.31267143E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.29671147E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.02401327E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     9.85091728E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.85091728E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.90347888E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.96893785E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.96893785E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.96893788E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.89798175E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.89798175E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.89798175E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.89798175E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.29931048E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.29931048E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.29931048E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.29931048E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.23342049E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.23342049E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.65719385E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.74987923E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.95261071E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.47238545E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.02230647E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.02230647E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.65628905E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.24105260E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.07963359E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.78503830E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.78503830E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.79841609E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.79841609E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.04248296E-03   # h decays
#          BR         NDA      ID1       ID2
     5.95580653E-01    2           5        -5   # BR(h -> b       bb     )
     6.44397056E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.28116300E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.83803697E-04    2           3        -3   # BR(h -> s       sb     )
     2.10088954E-02    2           4        -4   # BR(h -> c       cb     )
     6.87417800E-02    2          21        21   # BR(h -> g       g      )
     2.36620778E-03    2          22        22   # BR(h -> gam     gam    )
     1.61605964E-03    2          22        23   # BR(h -> Z       gam    )
     2.18001619E-01    2          24       -24   # BR(h -> W+      W-     )
     2.75331599E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44599285E+01   # H decays
#          BR         NDA      ID1       ID2
     3.41769407E-01    2           5        -5   # BR(H -> b       bb     )
     6.08813918E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15261855E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54617684E-04    2           3        -3   # BR(H -> s       sb     )
     7.14267821E-08    2           4        -4   # BR(H -> c       cb     )
     7.15845061E-03    2           6        -6   # BR(H -> t       tb     )
     1.03723378E-06    2          21        21   # BR(H -> g       g      )
     3.14785502E-11    2          22        22   # BR(H -> gam     gam    )
     1.83635460E-09    2          23        22   # BR(H -> Z       gam    )
     2.01448666E-06    2          24       -24   # BR(H -> W+      W-     )
     1.00654537E-06    2          23        23   # BR(H -> Z       Z      )
     7.52242414E-06    2          25        25   # BR(H -> h       h      )
     4.97797460E-24    2          36        36   # BR(H -> A       A      )
     4.32609655E-22    2          23        36   # BR(H -> Z       A      )
     1.85852516E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66104656E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66104656E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.88727908E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56570820E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60891672E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09179552E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.80703409E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.66677412E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62736096E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.79460357E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.19903815E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     6.54678853E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     2.22676887E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.22676887E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.34230523E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44563925E+01   # A decays
#          BR         NDA      ID1       ID2
     3.41817443E-01    2           5        -5   # BR(A -> b       bb     )
     6.08857260E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15277011E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54677120E-04    2           3        -3   # BR(A -> s       sb     )
     7.18816003E-08    2           4        -4   # BR(A -> c       cb     )
     7.17147955E-03    2           6        -6   # BR(A -> t       tb     )
     1.47355762E-05    2          21        21   # BR(A -> g       g      )
     4.70003235E-08    2          22        22   # BR(A -> gam     gam    )
     1.61921217E-08    2          23        22   # BR(A -> Z       gam    )
     2.01039360E-06    2          23        25   # BR(A -> Z       h      )
     1.85989629E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66119176E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66119176E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.50046442E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.94036133E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30149800E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.66769406E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.56725138E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.52344455E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.83256256E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.33320807E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.79846866E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.29626400E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.29626400E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43918823E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.45618889E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09740429E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15589277E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.49195788E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22135260E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51271506E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.47829210E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.01507883E-06    2          24        25   # BR(H+ -> W+      h      )
     3.18874903E-14    2          24        36   # BR(H+ -> W+      A      )
     5.57177011E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46532582E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.33744493E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66530721E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.20753578E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61615821E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00225598E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.57206028E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.44644947E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

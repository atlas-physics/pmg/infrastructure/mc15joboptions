#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.10037208E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     5.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     2.00485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04170210E+01   # W+
        25     1.24923978E+02   # h
        35     3.99999971E+03   # H
        36     3.99999538E+03   # A
        37     4.00103879E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01655270E+03   # ~d_L
   2000001     4.01510356E+03   # ~d_R
   1000002     4.01589135E+03   # ~u_L
   2000002     4.01633308E+03   # ~u_R
   1000003     4.01655270E+03   # ~s_L
   2000003     4.01510356E+03   # ~s_R
   1000004     4.01589135E+03   # ~c_L
   2000004     4.01633308E+03   # ~c_R
   1000005     5.48747521E+02   # ~b_1
   2000005     4.02011049E+03   # ~b_2
   1000006     5.34047121E+02   # ~t_1
   2000006     2.02377611E+03   # ~t_2
   1000011     4.00280327E+03   # ~e_L
   2000011     4.00300841E+03   # ~e_R
   1000012     4.00168120E+03   # ~nu_eL
   1000013     4.00280327E+03   # ~mu_L
   2000013     4.00300841E+03   # ~mu_R
   1000014     4.00168120E+03   # ~nu_muL
   1000015     4.00493543E+03   # ~tau_1
   2000015     4.00767513E+03   # ~tau_2
   1000016     4.00395123E+03   # ~nu_tauL
   1000021     1.96407676E+03   # ~g
   1000022     9.36129053E+01   # ~chi_10
   1000023    -1.36694076E+02   # ~chi_20
   1000025     1.52678610E+02   # ~chi_30
   1000035     2.05630844E+03   # ~chi_40
   1000024     1.31715436E+02   # ~chi_1+
   1000037     2.05647319E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61900066E-01   # N_11
  1  2     1.38232568E-02   # N_12
  1  3     5.33895899E-01   # N_13
  1  4     3.66431953E-01   # N_14
  2  1    -1.35878521E-01   # N_21
  2  2     2.72923331E-02   # N_22
  2  3    -6.85306151E-01   # N_23
  2  4     7.14945897E-01   # N_24
  3  1     6.33280745E-01   # N_31
  3  2     2.39114029E-02   # N_32
  3  3     4.95288176E-01   # N_33
  3  4     5.94199769E-01   # N_34
  4  1    -9.02929826E-04   # N_41
  4  2     9.99245861E-01   # N_42
  4  3    -5.20003680E-04   # N_43
  4  4    -3.88152467E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.39726294E-04   # U_11
  1  2     9.99999726E-01   # U_12
  2  1    -9.99999726E-01   # U_21
  2  2     7.39726294E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49065674E-02   # V_11
  1  2    -9.98491497E-01   # V_12
  2  1    -9.98491497E-01   # V_21
  2  2    -5.49065674E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95978495E-01   # cos(theta_t)
  1  2    -8.95926197E-02   # sin(theta_t)
  2  1     8.95926197E-02   # -sin(theta_t)
  2  2     9.95978495E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999923E-01   # cos(theta_b)
  1  2    -3.92428330E-04   # sin(theta_b)
  2  1     3.92428330E-04   # -sin(theta_b)
  2  2     9.99999923E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.02906701E-01   # cos(theta_tau)
  1  2     7.11282061E-01   # sin(theta_tau)
  2  1    -7.11282061E-01   # -sin(theta_tau)
  2  2    -7.02906701E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00312073E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.00372080E+03  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44259584E+02   # higgs               
         4     1.61395949E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.00372080E+03  # The gauge couplings
     1     3.61259983E-01   # gprime(Q) DRbar
     2     6.37006357E-01   # g(Q) DRbar
     3     1.03613074E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.00372080E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     7.52855263E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.00372080E+03  # The trilinear couplings
  1  1     2.76388729E-07   # A_d(Q) DRbar
  2  2     2.76415343E-07   # A_s(Q) DRbar
  3  3     4.93240832E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.00372080E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     5.97941587E-08   # A_mu(Q) DRbar
  3  3     6.04014944E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.00372080E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.72561107E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.00372080E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.81260400E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.00372080E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03916632E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.00372080E+03  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58567123E+07   # M^2_Hd              
        22    -2.74636557E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     5.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     2.00485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40778350E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.01956191E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.45211905E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.45211905E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.54788095E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.54788095E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.26334426E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.30119731E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.35112600E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.17051057E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.17716612E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18680377E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.69373409E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.25925929E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.61171816E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.43231503E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.44215970E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.07732527E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.25839549E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.76067162E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.18689329E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.26896078E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.87602829E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.76681176E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66356815E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.78819579E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.68815746E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40580523E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.86502492E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.53181804E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.55695595E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15789822E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.75991362E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.95162569E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.31562956E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.64839777E-08    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78276448E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48259724E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.25257095E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80116084E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78567366E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.26795676E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55908635E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52741621E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60970911E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.21497450E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02147394E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.21767252E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.47597647E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44652031E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78296326E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.17915443E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45205834E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.01446500E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.79031834E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.56225874E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.59073693E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52963545E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.54183075E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.38865619E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66527410E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.78644555E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.45766540E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85558364E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78276448E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48259724E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.25257095E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80116084E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78567366E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.26795676E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55908635E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52741621E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60970911E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.21497450E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02147394E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.21767252E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.47597647E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44652031E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78296326E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.17915443E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45205834E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.01446500E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.79031834E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.56225874E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.59073693E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52963545E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.54183075E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.38865619E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66527410E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.78644555E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.45766540E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85558364E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14658471E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97312537E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.26878202E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.51238987E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77510508E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.60716838E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56364997E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07484051E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80928268E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84539708E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00617319E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.42597541E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14658471E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97312537E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.26878202E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.51238987E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77510508E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.60716838E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56364997E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07484051E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80928268E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84539708E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00617319E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.42597541E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10533189E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27831718E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00525004E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60195469E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39047151E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41062760E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78766886E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11601679E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11652691E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33482477E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35313025E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42262149E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22096933E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85214195E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14766206E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01921816E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.57479867E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.75012495E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77795082E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.08730200E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54119753E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14766206E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01921816E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.57479867E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.75012495E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77795082E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.08730200E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54119753E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48209137E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.21853520E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.04225222E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.20083344E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51461777E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.77101499E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01592135E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.13885694E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33730857E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33730857E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11244394E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11244394E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10049499E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.54991387E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.81152863E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     8.75071007E-08    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.70389101E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.32340185E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.51234261E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.25466123E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.56511634E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.19027281E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.05914380E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.78210919E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18546868E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53693876E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18546868E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53693876E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37344296E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51974561E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51974561E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47540943E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03614224E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03614224E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03614180E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.78875274E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.78875274E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.78875274E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.78875274E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     9.29586549E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     9.29586549E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     9.29586549E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     9.29586549E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.05289289E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.05289289E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.38723312E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.69417891E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.18011984E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.90064638E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.33635714E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.90064638E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.33635714E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.89874921E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.43750513E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.43750513E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.42311554E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.90746375E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.90746375E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.90745735E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.63016126E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     8.59667050E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.63016126E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     8.59667050E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.48168066E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.96927978E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.96927978E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.83415170E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.93512927E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.93512927E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.93512943E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.79485795E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.79485795E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.79485795E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.79485795E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.93157049E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.93157049E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.93157049E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.93157049E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.82922796E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.82922796E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.54890766E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.23552964E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.31750942E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.18066204E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.17309109E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.17309109E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.11287483E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.01993111E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.44239436E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.87339039E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.87339039E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.88453363E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.88453363E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     3.97466054E-03   # h decays
#          BR         NDA      ID1       ID2
     6.05434789E-01    2           5        -5   # BR(h -> b       bb     )
     6.52419900E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30958918E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.90266842E-04    2           3        -3   # BR(h -> s       sb     )
     2.12892200E-02    2           4        -4   # BR(h -> c       cb     )
     6.89594280E-02    2          21        21   # BR(h -> g       g      )
     2.36144222E-03    2          22        22   # BR(h -> gam     gam    )
     1.56241706E-03    2          22        23   # BR(h -> Z       gam    )
     2.08346148E-01    2          24       -24   # BR(h -> W+      W-     )
     2.60833399E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44224268E+01   # H decays
#          BR         NDA      ID1       ID2
     3.39414233E-01    2           5        -5   # BR(H -> b       bb     )
     6.09232789E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15409958E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54792899E-04    2           3        -3   # BR(H -> s       sb     )
     7.14772199E-08    2           4        -4   # BR(H -> c       cb     )
     7.16350545E-03    2           6        -6   # BR(H -> t       tb     )
     1.09895735E-06    2          21        21   # BR(H -> g       g      )
     2.86151193E-10    2          22        22   # BR(H -> gam     gam    )
     1.83820222E-09    2          23        22   # BR(H -> Z       gam    )
     2.04097926E-06    2          24       -24   # BR(H -> W+      W-     )
     1.01978261E-06    2          23        23   # BR(H -> Z       Z      )
     7.56540465E-06    2          25        25   # BR(H -> h       h      )
    -9.32530165E-25    2          36        36   # BR(H -> A       A      )
    -3.32852766E-22    2          23        36   # BR(H -> Z       A      )
     1.86655596E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66884884E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66884884E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.35509314E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.60354909E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.68358862E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.48073664E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.93765194E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.93743179E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04710290E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.41507207E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.38203056E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.49425617E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.88508842E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.88508842E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.41189221E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44229654E+01   # A decays
#          BR         NDA      ID1       ID2
     3.39436898E-01    2           5        -5   # BR(A -> b       bb     )
     6.09231020E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15409163E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54833470E-04    2           3        -3   # BR(A -> s       sb     )
     7.19257293E-08    2           4        -4   # BR(A -> c       cb     )
     7.17588222E-03    2           6        -6   # BR(A -> t       tb     )
     1.47446289E-05    2          21        21   # BR(A -> g       g      )
     4.06702621E-08    2          22        22   # BR(A -> gam     gam    )
     1.62078110E-08    2          23        22   # BR(A -> Z       gam    )
     2.03673909E-06    2          23        25   # BR(A -> Z       h      )
     1.86963941E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66885373E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66885373E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.93072871E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.22377949E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.33159035E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.96826803E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.55772222E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.62273811E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30764413E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.29472302E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.81612327E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.94360127E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.94360127E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43010229E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.40766875E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.10758433E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15949219E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.46090501E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22339230E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51691138E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.44810684E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.04362570E-06    2          24        25   # BR(H+ -> W+      h      )
     2.99713267E-14    2          24        36   # BR(H+ -> W+      A      )
     4.84093112E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.26786838E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07276902E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.67477709E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.59184452E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57617758E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.22547607E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.96727536E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.51930347E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

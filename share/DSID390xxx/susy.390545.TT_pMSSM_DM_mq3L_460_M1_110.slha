#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.90964362E+03
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.25959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.26900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     4.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.85485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04188792E+01   # W+
        25     1.25546145E+02   # h
        35     3.99999890E+03   # H
        36     3.99999486E+03   # A
        37     4.00102769E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.01296943E+03   # ~d_L
   2000001     4.01228145E+03   # ~d_R
   1000002     4.01231185E+03   # ~u_L
   2000002     4.01348758E+03   # ~u_R
   1000003     4.01296943E+03   # ~s_L
   2000003     4.01228145E+03   # ~s_R
   1000004     4.01231185E+03   # ~c_L
   2000004     4.01348758E+03   # ~c_R
   1000005     4.73621284E+02   # ~b_1
   2000005     4.01814793E+03   # ~b_2
   1000006     4.49293486E+02   # ~t_1
   2000006     1.87501747E+03   # ~t_2
   1000011     4.00202917E+03   # ~e_L
   2000011     4.00302402E+03   # ~e_R
   1000012     4.00091184E+03   # ~nu_eL
   1000013     4.00202917E+03   # ~mu_L
   2000013     4.00302402E+03   # ~mu_R
   1000014     4.00091184E+03   # ~nu_muL
   1000015     4.00450727E+03   # ~tau_1
   2000015     4.00828039E+03   # ~tau_2
   1000016     4.00349440E+03   # ~nu_tauL
   1000021     1.95668098E+03   # ~g
   1000022     9.37086090E+01   # ~chi_10
   1000023    -1.36501784E+02   # ~chi_20
   1000025     1.52743535E+02   # ~chi_30
   1000035     2.05761450E+03   # ~chi_40
   1000024     1.31633341E+02   # ~chi_1+
   1000037     2.05777894E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.61920278E-01   # N_11
  1  2     1.38291196E-02   # N_12
  1  3     5.33866338E-01   # N_13
  1  4     3.66432776E-01   # N_14
  2  1    -1.35849452E-01   # N_21
  2  2     2.73037145E-02   # N_22
  2  3    -6.85310250E-01   # N_23
  2  4     7.14947057E-01   # N_24
  3  1     6.33262664E-01   # N_31
  3  2     2.39210436E-02   # N_32
  3  3     4.95314369E-01   # N_33
  3  4     5.94196818E-01   # N_34
  4  1    -9.03104086E-04   # N_41
  4  2     9.99245238E-01   # N_42
  4  3    -5.20215253E-04   # N_43
  4  4    -3.88312694E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     7.40031350E-04   # U_11
  1  2     9.99999726E-01   # U_12
  2  1    -9.99999726E-01   # U_21
  2  2     7.40031350E-04   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.49292791E-02   # V_11
  1  2    -9.98490247E-01   # V_12
  2  1    -9.98490247E-01   # V_21
  2  2    -5.49292791E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.94538924E-01   # cos(theta_t)
  1  2    -1.04366320E-01   # sin(theta_t)
  2  1     1.04366320E-01   # -sin(theta_t)
  2  2     9.94538924E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999923E-01   # cos(theta_b)
  1  2    -3.92428330E-04   # sin(theta_b)
  2  1     3.92428330E-04   # -sin(theta_b)
  2  2     9.99999923E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.02758803E-01   # cos(theta_tau)
  1  2     7.11428187E-01   # sin(theta_tau)
  2  1    -7.11428187E-01   # -sin(theta_tau)
  2  2    -7.02758803E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00322333E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.09643620E+02  # DRbar Higgs Parameters
         1    -1.26900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44422138E+02   # higgs               
         4     1.61565006E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  9.09643620E+02  # The gauge couplings
     1     3.60940882E-01   # gprime(Q) DRbar
     2     6.36846828E-01   # g(Q) DRbar
     3     1.03838275E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.09643620E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     5.91649348E-07   # A_c(Q) DRbar
  3  3     2.25959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  9.09643620E+02  # The trilinear couplings
  1  1     2.16513697E-07   # A_d(Q) DRbar
  2  2     2.16534597E-07   # A_s(Q) DRbar
  3  3     3.87466042E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  9.09643620E+02  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     4.71206231E-08   # A_mu(Q) DRbar
  3  3     4.75964070E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.09643620E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.76339468E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.09643620E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.82417995E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.09643620E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03883703E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.09643620E+02  # The soft SUSY breaking masses at the scale Q
         1     1.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58505577E+07   # M^2_Hd              
        22    -2.35532641E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     4.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.85485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40699704E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.25507612E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.44186202E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.44186202E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.55813798E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.55813798E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.40625398E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.39512682E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.17719185E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.06415843E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.36352290E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.23549592E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.14739697E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13097676E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.53560763E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.17874809E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.55758068E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.24570960E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.51868441E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.17059736E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.66904837E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.73423264E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.08408858E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.65126304E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65928731E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.79025802E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.69142203E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.40767010E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.88285894E-08    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.53838388E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.59244695E-08    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.15651121E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.79566132E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.97923710E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.35712991E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.43841303E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77642730E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.48351948E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.35132584E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.80311099E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.78515153E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.28317059E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.55802824E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.52753020E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60471557E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.21681949E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.02157479E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.21869478E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.47350582E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44623258E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77661754E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.18138929E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.45616137E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.01606085E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.78981075E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.58988500E-08    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.58973233E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52975862E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53703709E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.39357678E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.66557114E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.78918646E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.45134426E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85550673E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77642730E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.48351948E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.35132584E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.80311099E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.78515153E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.28317059E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.55802824E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.52753020E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60471557E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.21681949E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.02157479E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.21869478E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.47350582E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44623258E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77661754E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.18138929E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.45616137E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.01606085E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.78981075E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.58988500E-08    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.58973233E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52975862E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53703709E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.39357678E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.66557114E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.78918646E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.45134426E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85550673E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14062729E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.97237640E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.26640430E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.51243946E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77513473E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.61854689E-07    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56371402E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07118043E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80959019E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.84462361E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.00594303E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.42368527E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14062729E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.97237640E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.26640430E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.51243946E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77513473E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.61854689E-07    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56371402E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07118043E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80959019E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.84462361E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.00594303E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.42368527E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10034578E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.27895728E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.00992220E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.60201601E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.38979533E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41923570E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.78631559E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.11261765E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.11489210E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.33909225E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.35211081E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42327526E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.22357261E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85345534E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14172029E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01926314E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.57574147E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.74849847E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77796749E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09356084E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54122650E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14172029E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01926314E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.57574147E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.74849847E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77796749E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09356084E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54122650E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47654329E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.21694619E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.04201407E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.19823817E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51437488E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.78253458E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01543308E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.06384896E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33734609E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33734609E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11245646E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11245646E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10039488E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     6.72886910E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.84607364E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.14599454E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.73140056E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.16725066E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.47536775E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.10200329E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.45335326E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     6.02613703E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.92217970E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.77899916E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18571046E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53709776E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18571046E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53709776E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37336791E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51927271E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51927271E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47451502E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.03484164E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.03484164E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.03484114E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.58047250E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.58047250E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.58047250E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.58047250E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     8.60159631E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     8.60159631E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     8.60159631E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     8.60159631E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.54517528E-06    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.54517528E-06    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.57058455E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     7.71750921E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.70707674E-04    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     4.78893187E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     6.19129594E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     4.78893187E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     6.19129594E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     5.76097093E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.40427745E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.40427745E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.39018065E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     2.84008156E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     2.84008156E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     2.84007445E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     7.02134107E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.10295945E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     7.02134107E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.10295945E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.82874550E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.08476094E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.08476094E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.94610239E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.16568190E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.16568190E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.16568209E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     8.91613085E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     8.91613085E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     8.91613085E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     8.91613085E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     2.97199410E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     2.97199410E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     2.97199410E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     2.97199410E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     2.86963859E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     2.86963859E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     6.72794208E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.03180800E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.23563343E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.12670395E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     6.02216172E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.02216172E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.92450172E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.94068429E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.37743387E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.89124386E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.89124386E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.82800408E-06    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     1.82800408E-06    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.89771737E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.89771737E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05177174E-03   # h decays
#          BR         NDA      ID1       ID2
     5.96269413E-01    2           5        -5   # BR(h -> b       bb     )
     6.43224921E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27701138E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82883861E-04    2           3        -3   # BR(h -> s       sb     )
     2.09677255E-02    2           4        -4   # BR(h -> c       cb     )
     6.74779327E-02    2          21        21   # BR(h -> g       g      )
     2.37763066E-03    2          22        22   # BR(h -> gam     gam    )
     1.62000807E-03    2          22        23   # BR(h -> Z       gam    )
     2.18606805E-01    2          24       -24   # BR(h -> W+      W-     )
     2.76474084E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.45618008E+01   # H decays
#          BR         NDA      ID1       ID2
     3.41544659E-01    2           5        -5   # BR(H -> b       bb     )
     6.07676364E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14859644E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54141976E-04    2           3        -3   # BR(H -> s       sb     )
     7.12975458E-08    2           4        -4   # BR(H -> c       cb     )
     7.14549835E-03    2           6        -6   # BR(H -> t       tb     )
     1.14544708E-06    2          21        21   # BR(H -> g       g      )
     4.34176245E-10    2          22        22   # BR(H -> gam     gam    )
     1.83359007E-09    2          23        22   # BR(H -> Z       gam    )
     2.09354107E-06    2          24       -24   # BR(H -> W+      W-     )
     1.04604542E-06    2          23        23   # BR(H -> Z       Z      )
     7.71000625E-06    2          25        25   # BR(H -> h       h      )
    -4.46029445E-24    2          36        36   # BR(H -> A       A      )
     3.21447558E-20    2          23        36   # BR(H -> Z       A      )
     1.86342928E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66313716E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66313716E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     3.34589262E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.59648701E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.67894127E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.47694757E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.93101816E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.91963425E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     2.04136888E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.38973904E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     4.36707126E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.70457178E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.78386171E-06    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.98824839E-05    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.98824839E-05    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.42241595E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.45626923E+01   # A decays
#          BR         NDA      ID1       ID2
     3.41565206E-01    2           5        -5   # BR(A -> b       bb     )
     6.07670792E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14857505E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54180852E-04    2           3        -3   # BR(A -> s       sb     )
     7.17415302E-08    2           4        -4   # BR(A -> c       cb     )
     7.15750505E-03    2           6        -6   # BR(A -> t       tb     )
     1.47068709E-05    2          21        21   # BR(A -> g       g      )
     4.05736315E-08    2          22        22   # BR(A -> gam     gam    )
     1.61715362E-08    2          23        22   # BR(A -> Z       gam    )
     2.08911774E-06    2          23        25   # BR(A -> Z       h      )
     1.86648571E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66313178E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66313178E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.92265958E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.21497684E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.32792836E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     1.96320680E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.55643486E-04    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.60665471E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     2.30116891E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.26659532E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     3.80373303E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.06592397E-05    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.06592397E-05    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.44404951E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.44197986E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09192028E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15395376E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.48286410E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22025511E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51045716E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.46935435E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.09616246E-06    2          24        25   # BR(H+ -> W+      h      )
     2.84085074E-14    2          24        36   # BR(H+ -> W+      A      )
     4.82782971E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.24250229E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.06171525E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66902505E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     9.55776088E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.57080185E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.19795196E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     2.07540485E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     3.66688676E-05    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

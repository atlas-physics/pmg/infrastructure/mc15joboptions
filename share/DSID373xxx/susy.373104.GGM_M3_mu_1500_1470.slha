#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.46500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.47000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05414136E+01   # W+
        25     1.26000000E+02   # h
        35     2.00401147E+03   # H
        36     2.00000000E+03   # A
        37     2.00150999E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98868327E+03   # ~d_L
   2000001     4.98857377E+03   # ~d_R
   1000002     4.98843844E+03   # ~u_L
   2000002     4.98849628E+03   # ~u_R
   1000003     4.98868327E+03   # ~s_L
   2000003     4.98857377E+03   # ~s_R
   1000004     4.98843844E+03   # ~c_L
   2000004     4.98849628E+03   # ~c_R
   1000005     4.98802334E+03   # ~b_1
   2000005     4.98923511E+03   # ~b_2
   1000006     5.10588799E+03   # ~t_1
   2000006     5.39843946E+03   # ~t_2
   1000011     5.00008217E+03   # ~e_L
   2000011     5.00007611E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008217E+03   # ~mu_L
   2000013     5.00007611E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99968728E+03   # ~tau_1
   2000015     5.00047161E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.61385874E+03   # ~g
   1000022     1.51302773E+03   # ~chi_10
   1000023    -1.56294712E+03   # ~chi_20
   1000025     1.60028716E+03   # ~chi_30
   1000035     3.12733185E+03   # ~chi_40
   1000024     1.55907994E+03   # ~chi_1+
   1000037     3.12732897E+03   # ~chi_2+
   1000039     1.94572727E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.26065374E-01   # N_11
  1  2    -3.30464423E-02   # N_12
  1  3     4.87234384E-01   # N_13
  1  4    -4.84086418E-01   # N_14
  2  1     2.80039138E-03   # N_21
  2  2    -3.31089576E-03   # N_22
  2  3    -7.07014423E-01   # N_23
  2  4    -7.07185832E-01   # N_24
  3  1     6.87618566E-01   # N_31
  3  2     3.69295296E-02   # N_32
  3  3    -5.11531551E-01   # N_33
  3  4     5.13957575E-01   # N_34
  4  1     1.39199888E-03   # N_41
  4  2    -9.98765829E-01   # N_42
  4  3    -3.26914783E-02   # N_43
  4  4     3.73650788E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.61956427E-02   # U_11
  1  2     9.98932411E-01   # U_12
  2  1     9.98932411E-01   # U_21
  2  2     4.61956427E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.28053713E-02   # V_11
  1  2     9.98604823E-01   # V_12
  2  1     9.98604823E-01   # V_21
  2  2     5.28053713E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99929560E-01   # cos(theta_t)
  1  2     1.18690791E-02   # sin(theta_t)
  2  1    -1.18690791E-02   # -sin(theta_t)
  2  2     9.99929560E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.74402109E-01   # cos(theta_b)
  1  2     7.38364270E-01   # sin(theta_b)
  2  1    -7.38364270E-01   # -sin(theta_b)
  2  2     6.74402109E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04368114E-01   # cos(theta_tau)
  1  2     7.09834882E-01   # sin(theta_tau)
  2  1    -7.09834882E-01   # -sin(theta_tau)
  2  2     7.04368114E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202386E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.47000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52213218E+02   # vev(Q)              
         4     3.32944410E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52756463E-01   # gprime(Q) DRbar
     2     6.27008075E-01   # g(Q) DRbar
     3     1.08507291E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02511211E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71688761E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79779769E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.46500000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     6.25495224E+05   # M^2_Hd              
        22    -7.46920097E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36974084E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.73505217E-08   # gluino decays
#          BR         NDA      ID1       ID2
     7.22472233E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.09701686E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.16720248E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.91883455E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.19665299E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     9.24596955E-09    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.84260172E-07    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.55358033E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.11655513E-09    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     7.21483455E-07    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.19665299E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     9.24596955E-09    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.84260172E-07    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.55358033E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.11655513E-09    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.21483455E-07    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.94838815E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     5.91794097E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.05127099E-08    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.23549440E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.23549440E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.23549440E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.23549440E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.00938299E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.75789438E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.98954334E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.40012201E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.53342691E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     7.54238770E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.01883152E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.12395282E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.94227713E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.58883532E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.69260089E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.55781391E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.84072111E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.86910320E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.20414223E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.77076359E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.25111879E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.19925886E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -1.41190511E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.10970682E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     5.81812264E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     6.90604004E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.44424520E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.21097892E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.35110567E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.02918275E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.22932343E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01318957E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.48236216E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.55250514E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.55590544E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.29973488E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.18844510E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.59370877E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.54617704E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.30701757E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.12277223E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.17623422E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.30514600E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.16777740E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.16055129E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.81066116E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.01410910E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.71254712E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.02944048E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.06665295E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.17648665E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.11795739E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.10804617E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.85424496E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.49122632E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60277631E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.30520516E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.93018374E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.78612160E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.57248678E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.01771285E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.84143204E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.03429865E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.06707331E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.11169337E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.45747661E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.00870558E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.77795584E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.99636838E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89764478E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.30514600E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.16777740E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.16055129E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.81066116E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.01410910E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.71254712E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.02944048E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.06665295E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.17648665E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.11795739E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.10804617E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.85424496E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.49122632E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60277631E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.30520516E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.93018374E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.78612160E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.57248678E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.01771285E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.84143204E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.03429865E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.06707331E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.11169337E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.45747661E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.00870558E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.77795584E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.99636838E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89764478E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.68536488E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.47006753E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78528144E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.05352795E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.68795645E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.53285511E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.38616244E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.02004456E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.33166317E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     7.82449490E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.66824978E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     8.80152940E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.68536488E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.47006753E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78528144E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.05352795E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.68795645E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.53285511E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.38616244E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.02004456E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.33166317E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     7.82449490E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.66824978E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     8.80152940E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.35762861E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.84296684E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64824083E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.56537333E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51447860E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.77835971E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.03474936E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     4.02959396E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.35563274E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.69760066E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.54365297E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.64244009E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54846937E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.15400732E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.10279222E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.68550539E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17133169E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.41527093E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.18214721E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69579629E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.30911541E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.38142461E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.68550539E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17133169E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.41527093E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.18214721E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69579629E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.30911541E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.38142461E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.68812188E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17019157E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.41389337E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.17515644E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69317233E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.27762628E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.37620280E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.29562059E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.07249603E-06    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33604630E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33604630E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11201670E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11201670E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10386329E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.87766549E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53263449E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.09680311E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46236973E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.37208504E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53610762E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.33723132E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.32820354E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.82706526E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.01758754E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.87222273E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10189728E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.57453586E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.77818429E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.20299960E-12    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     3.42445449E-08    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.44488387E-07    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.18515716E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53462106E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18515716E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53462106E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40653712E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50491860E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50491860E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47147428E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99958520E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99958520E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99958520E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.02365333E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.02365333E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.02365333E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.02365333E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     6.74551151E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     6.74551151E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     6.74551151E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     6.74551151E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.41722369E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.41722369E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.96521234E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.27959541E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.88905153E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     6.19050059E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.54732761E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.60454270E-08    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27561444E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.51708885E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.27561444E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.51708885E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.55547915E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.64266000E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.64266000E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     5.75297349E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.42113118E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.42113118E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.42113118E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.28953964E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.96465324E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.28953964E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.96465324E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.54740063E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.77097725E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.77097725E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.67133538E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.35220772E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.35220772E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.35220772E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.34820957E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.34820957E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.34820957E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.34820957E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.49402561E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.49402561E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.49402561E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.49402561E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.45275073E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.45275073E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.87767255E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.13837794E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51888137E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.07589007E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46483690E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46483690E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.12338945E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.10009111E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.40584020E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.59847512E-11    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.77711873E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     1.92436904E-14    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.25476576E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     5.99914480E-16    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21976772E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02022099E-01    2           5        -5   # BR(h -> b       bb     )
     6.22180577E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20224799E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65953092E-04    2           3        -3   # BR(h -> s       sb     )
     2.01021168E-02    2           4        -4   # BR(h -> c       cb     )
     6.63860932E-02    2          21        21   # BR(h -> g       g      )
     2.30612105E-03    2          22        22   # BR(h -> gam     gam    )
     1.62611215E-03    2          22        23   # BR(h -> Z       gam    )
     2.16581647E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80715749E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78101565E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46975021E-03    2           5        -5   # BR(H -> b       bb     )
     2.46427432E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71212717E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547047E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668236E-05    2           4        -4   # BR(H -> c       cb     )
     9.96065667E-01    2           6        -6   # BR(H -> t       tb     )
     7.97764384E-04    2          21        21   # BR(H -> g       g      )
     2.71499355E-06    2          22        22   # BR(H -> gam     gam    )
     1.16012396E-06    2          23        22   # BR(H -> Z       gam    )
     3.34738292E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66912654E-04    2          23        23   # BR(H -> Z       Z      )
     9.02811774E-04    2          25        25   # BR(H -> h       h      )
     7.27747666E-24    2          36        36   # BR(H -> A       A      )
     2.96449955E-11    2          23        36   # BR(H -> Z       A      )
     6.81066927E-12    2          24       -37   # BR(H -> W+      H-     )
     6.81066927E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381847E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47266187E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897990E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62267502E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677522E-06    2           3        -3   # BR(A -> s       sb     )
     9.96177585E-06    2           4        -4   # BR(A -> c       cb     )
     9.96997094E-01    2           6        -6   # BR(A -> t       tb     )
     9.43676691E-04    2          21        21   # BR(A -> g       g      )
     3.15126598E-06    2          22        22   # BR(A -> gam     gam    )
     1.35270931E-06    2          23        22   # BR(A -> Z       gam    )
     3.26204300E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472240E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36130756E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237638E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81145084E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50124821E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45694387E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731676E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402255E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34322396E-04    2          24        25   # BR(H+ -> W+      h      )
     2.75680523E-13    2          24        36   # BR(H+ -> W+      A      )

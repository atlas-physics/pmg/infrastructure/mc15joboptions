#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376826E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416461E+03   # H
        36     2.00000000E+03   # A
        37     2.00209624E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.96252906E+03   # ~d_L
   2000001     4.96241863E+03   # ~d_R
   1000002     4.96228232E+03   # ~u_L
   2000002     4.96234100E+03   # ~u_R
   1000003     4.96252906E+03   # ~s_L
   2000003     4.96241863E+03   # ~s_R
   1000004     4.96228232E+03   # ~c_L
   2000004     4.96234100E+03   # ~c_R
   1000005     4.96239170E+03   # ~b_1
   2000005     4.96255747E+03   # ~b_2
   1000006     5.09437018E+03   # ~t_1
   2000006     5.39449314E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     1.88525309E+03   # ~g
   1000022     1.46530885E+02   # ~chi_10
   1000023    -1.62956609E+02   # ~chi_20
   1000025     2.94799003E+02   # ~chi_30
   1000035     3.12909431E+03   # ~chi_40
   1000024     1.60744847E+02   # ~chi_1+
   1000037     3.12909391E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08740546E-01   # N_11
  1  2    -2.47371084E-02   # N_12
  1  3     6.79331278E-01   # N_13
  1  4    -6.65264133E-01   # N_14
  2  1     2.00315724E-02   # N_21
  2  2    -4.81337771E-03   # N_22
  2  3    -7.04243521E-01   # N_23
  2  4    -7.09659517E-01   # N_24
  3  1     9.50935244E-01   # N_31
  3  2     8.56931792E-03   # N_32
  3  3    -2.05717162E-01   # N_33
  3  4     2.30931109E-01   # N_34
  4  1     4.15245676E-04   # N_41
  4  2    -9.99645674E-01   # N_42
  4  3    -1.51831370E-02   # N_43
  4  4     2.18592377E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14649314E-02   # U_11
  1  2     9.99769602E-01   # U_12
  2  1     9.99769602E-01   # U_21
  2  2     2.14649314E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09100082E-02   # V_11
  1  2     9.99522172E-01   # V_12
  2  1     9.99522172E-01   # V_21
  2  2     3.09100082E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99724957E-01   # cos(theta_t)
  1  2    -2.34522995E-02   # sin(theta_t)
  2  1     2.34522995E-02   # -sin(theta_t)
  2  2     9.99724957E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08578830E-01   # cos(theta_b)
  1  2     9.12723036E-01   # sin(theta_b)
  2  1    -9.12723036E-01   # -sin(theta_b)
  2  2     4.08578830E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76623469E-01   # cos(theta_tau)
  1  2     7.36329193E-01   # sin(theta_tau)
  2  1    -7.36329193E-01   # -sin(theta_tau)
  2  2     6.76623469E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90210805E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51748530E+02   # vev(Q)              
         4     3.87919764E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146829E-01   # gprime(Q) DRbar
     2     6.29570112E-01   # g(Q) DRbar
     3     1.08213341E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02722379E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72398160E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79972657E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04059270E+06   # M^2_Hd              
        22    -5.46399558E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111774E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.71468826E-03   # gluino decays
#          BR         NDA      ID1       ID2
     7.49459487E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     8.46808808E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     7.70208879E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.00998459E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.69870855E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.14650397E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.40497458E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.35130909E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.23170085E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.23252974E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     8.69870855E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.14650397E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.40497458E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.35130909E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.23170085E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.23252974E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.05624479E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.14533459E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.42834141E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.81284204E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.06737783E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.02339408E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.07627785E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.07627785E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.07627785E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.07627785E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36521752E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36521752E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.00420269E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.06561363E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.22714102E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.66174405E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.52376572E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.29942947E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.03309325E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.58656438E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.29668415E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.63554394E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.81752931E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.44573323E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.02909046E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.66664843E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.82401315E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.04486041E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.65807557E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.60980685E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.70181676E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     8.27396429E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.34051106E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     4.16106053E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.30272511E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.92298780E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.54453153E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.57395903E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -2.61384517E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -4.18833471E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -5.24563616E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.46951125E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.48446606E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.02447367E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.59037775E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.27545607E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.95291096E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.61565860E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.92341395E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.50387996E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.12943284E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.61890863E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.00754265E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.05461573E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.23102019E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.68825383E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.46144959E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.99755571E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.02080821E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.96568421E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.08958828E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.68744520E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.28686985E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.48138965E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.12948050E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.01030399E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.47184851E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.51872150E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.23253307E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.14161825E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.46566697E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.99810360E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.94225793E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.29164808E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.43532894E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.21927417E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.54998365E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86510174E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.12943284E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.61890863E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.00754265E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.05461573E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.23102019E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.68825383E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.46144959E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.99755571E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.02080821E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.96568421E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.08958828E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.68744520E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.28686985E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.48138965E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.12948050E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.01030399E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.47184851E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.51872150E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.23253307E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.14161825E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.46566697E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.99810360E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.94225793E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.29164808E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.43532894E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.21927417E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.54998365E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86510174E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762257E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54459993E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89054222E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04818706E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563250E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45680480E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497459E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513805E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57738997E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03007504E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03823029E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42305229E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762257E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54459993E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89054222E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04818706E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563250E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45680480E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497459E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513805E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57738997E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03007504E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03823029E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42305229E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957264E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08481302E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84741454E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56338555E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26705801E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289539E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53593051E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825940E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319166E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61428692E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12659316E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05218176E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49093861E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85068681E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98399993E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747372E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74575436E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80485549E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92070642E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774189E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33893300E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178207E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747372E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74575436E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80485549E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92070642E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774189E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33893300E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178207E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068809E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74261425E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80279141E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91850986E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477105E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48071869E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584769E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23843421E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87422580E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36072993E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36072993E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024385E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024385E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03766502E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36660154E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99044877E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247500E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94615573E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26239243E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99429170E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31582710E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33901188E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91638195E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29740026E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34251473E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02290556E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82557726E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.02203045E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03274663E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96190146E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.35190970E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.26470153E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.50704502E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65752244E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18369466E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01663433E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27775076E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65369023E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27775076E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65369023E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83739792E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77309773E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77309773E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52635921E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53254379E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53254379E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53254379E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.36009867E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.36009867E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.36009867E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.36009867E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86699719E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86699719E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86699719E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86699719E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36111081E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36111081E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63816252E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83665563E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19032929E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99742087E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99742087E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79604547E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.16886607E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.84010428E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47823023E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.38244029E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36663712E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03762182E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93142494E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02331258E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94061602E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94061602E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71036741E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76481735E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63747965E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36386574E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01120356E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77915058E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22360995E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48869030E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19376240E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33265002E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33265002E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44985954E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77841009E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80591125E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81044045E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61876260E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21682493E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01605967E-01    2           5        -5   # BR(h -> b       bb     )
     6.22630429E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20384027E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66289768E-04    2           3        -3   # BR(h -> s       sb     )
     2.01159186E-02    2           4        -4   # BR(h -> c       cb     )
     6.64321469E-02    2          21        21   # BR(h -> g       g      )
     2.32831872E-03    2          22        22   # BR(h -> gam     gam    )
     1.62703390E-03    2          22        23   # BR(h -> Z       gam    )
     2.16849734E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80911642E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01485679E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38928267E-03    2           5        -5   # BR(H -> b       bb     )
     2.32089640E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20523286E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05055811E-06    2           3        -3   # BR(H -> s       sb     )
     9.48135443E-06    2           4        -4   # BR(H -> c       cb     )
     9.38140179E-01    2           6        -6   # BR(H -> t       tb     )
     7.51324367E-04    2          21        21   # BR(H -> g       g      )
     2.62938761E-06    2          22        22   # BR(H -> gam     gam    )
     1.09155211E-06    2          23        22   # BR(H -> Z       gam    )
     3.17733065E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58433155E-04    2          23        23   # BR(H -> Z       Z      )
     8.52351192E-04    2          25        25   # BR(H -> h       h      )
     8.36385975E-24    2          36        36   # BR(H -> A       A      )
     3.36663698E-11    2          23        36   # BR(H -> Z       A      )
     2.47941612E-12    2          24       -37   # BR(H -> W+      H-     )
     2.47941612E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71003796E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52164729E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36037374E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64294036E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94711139E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53626441E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87729950E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05932939E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39224192E-03    2           5        -5   # BR(A -> b       bb     )
     2.29747712E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12241157E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082270E-06    2           3        -3   # BR(A -> s       sb     )
     9.38382152E-06    2           4        -4   # BR(A -> c       cb     )
     9.39154116E-01    2           6        -6   # BR(A -> t       tb     )
     8.88927213E-04    2          21        21   # BR(A -> g       g      )
     2.69762222E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304489E-06    2          23        22   # BR(A -> Z       gam    )
     3.09635440E-04    2          23        25   # BR(A -> Z       h      )
     5.85773409E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28931996E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65392394E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84466826E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65669728E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36524514E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93378081E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97902324E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23399302E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630231E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29502622E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42035269E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13691774E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02355022E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40803400E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17331143E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33743788E-12    2          24        36   # BR(H+ -> W+      A      )
     5.57989143E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23541852E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29278403E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

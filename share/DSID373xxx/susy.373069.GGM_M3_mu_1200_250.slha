#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375320E+01   # W+
        25     1.26000000E+02   # h
        35     2.00415888E+03   # H
        36     2.00000000E+03   # A
        37     2.00208528E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.01048251E+03   # ~d_L
   2000001     5.01037294E+03   # ~d_R
   1000002     5.01023764E+03   # ~u_L
   2000002     5.01029578E+03   # ~u_R
   1000003     5.01048251E+03   # ~s_L
   2000003     5.01037294E+03   # ~s_R
   1000004     5.01023764E+03   # ~c_L
   2000004     5.01029578E+03   # ~c_R
   1000005     5.01031233E+03   # ~b_1
   2000005     5.01054458E+03   # ~b_2
   1000006     5.14040729E+03   # ~t_1
   2000006     5.43766118E+03   # ~t_2
   1000011     5.00008258E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008258E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001287E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     1.33583436E+03   # ~g
   1000022     2.38919085E+02   # ~chi_10
   1000023    -2.70397228E+02   # ~chi_20
   1000025     3.33136926E+02   # ~chi_30
   1000035     3.12901938E+03   # ~chi_40
   1000024     2.68154034E+02   # ~chi_1+
   1000037     3.12901895E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56290878E-01   # N_11
  1  2    -2.22778141E-02   # N_12
  1  3     5.94557140E-01   # N_13
  1  4    -5.80125818E-01   # N_14
  2  1     1.53224815E-02   # N_21
  2  2    -4.61392933E-03   # N_22
  2  3    -7.05672806E-01   # N_23
  2  4    -7.08357131E-01   # N_24
  3  1     8.30846251E-01   # N_31
  3  2     1.55221616E-02   # N_32
  3  3    -3.85061782E-01   # N_33
  3  4     4.01473528E-01   # N_34
  4  1     4.33052446E-04   # N_41
  4  2    -9.99620665E-01   # N_42
  4  3    -1.59725591E-02   # N_43
  4  4     2.24324913E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25809660E-02   # U_11
  1  2     9.99745017E-01   # U_12
  2  1     9.99745017E-01   # U_21
  2  2     2.25809660E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17201421E-02   # V_11
  1  2     9.99496790E-01   # V_12
  2  1     9.99496790E-01   # V_21
  2  2     3.17201421E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99882450E-01   # cos(theta_t)
  1  2    -1.53325204E-02   # sin(theta_t)
  2  1     1.53325204E-02   # -sin(theta_t)
  2  2     9.99882450E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13903702E-01   # cos(theta_b)
  1  2     8.57847880E-01   # sin(theta_b)
  2  1    -8.57847880E-01   # -sin(theta_b)
  2  2     5.13903702E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89410914E-01   # cos(theta_tau)
  1  2     7.24370480E-01   # sin(theta_tau)
  2  1    -7.24370480E-01   # -sin(theta_tau)
  2  2     6.89410914E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90202712E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51790476E+02   # vev(Q)              
         4     3.84692144E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53061961E-01   # gprime(Q) DRbar
     2     6.28966933E-01   # g(Q) DRbar
     3     1.08870429E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02627909E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72265491E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79958496E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99443236E+06   # M^2_Hd              
        22    -5.55122416E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37847901E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.22209398E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.49071190E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.14996469E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     6.23930271E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.51988684E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.24640011E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.55560314E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.39542576E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     9.85642475E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.87135430E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.93047233E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.24640011E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.55560314E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.39542576E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     9.85642475E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.87135430E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.93047233E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.38921002E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.30247965E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.47394833E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.30484574E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.79208396E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     7.66173648E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.85711323E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.85711323E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.85711323E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.85711323E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35409117E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35409117E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.32897848E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.33681105E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.10697044E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.85231593E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.34128209E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.29081245E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.66747774E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.90895302E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.33191078E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.19677942E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.11771409E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.36159563E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.51056495E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.86374072E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -7.69338362E-06    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.89814759E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.52925101E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.92655345E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.64894496E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.14405281E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.99156795E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.39001816E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.14261044E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.48619392E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     9.94174071E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     8.45548462E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.44794024E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -5.59863045E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.91470482E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.04730785E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.31023368E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.02331005E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.36249569E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.20448589E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.24033763E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.52789058E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.49541511E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.77952973E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.46418681E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.15051212E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.34546230E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.24033613E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.88203979E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.54678344E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.76358743E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.10633428E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.34992351E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.39887939E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.05997463E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.10738199E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.17791558E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.54926783E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.46421905E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.22887549E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.30148126E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.50071933E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.88343507E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.83899140E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.76737198E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.10679643E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.27053010E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.61954120E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.74263958E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.04022038E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.22304150E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88337495E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.46418681E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.15051212E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.34546230E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.24033613E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.88203979E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.54678344E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.76358743E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.10633428E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.34992351E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.39887939E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.05997463E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.10738199E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.17791558E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.54926783E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.46421905E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.22887549E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.30148126E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.50071933E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.88343507E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.83899140E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.76737198E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.10679643E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.27053010E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.61954120E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.74263958E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.04022038E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.22304150E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88337495E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80275386E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87657010E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10949687E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61587929E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59513852E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11805297E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409618E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125635E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10384611E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35177656E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89380142E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99382271E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80275386E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87657010E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10949687E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61587929E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59513852E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11805297E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409618E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125635E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10384611E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35177656E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89380142E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99382271E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63122742E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88509812E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46991794E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15157394E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31199196E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95802274E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591847E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36736239E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64274909E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64561667E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92684366E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00419033E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599316E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67466291E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89410553E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80261453E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.82113295E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21881683E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41439658E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734533E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40458114E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088016E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80261453E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.82113295E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21881683E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41439658E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734533E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40458114E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088016E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80581641E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81220781E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21742597E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41278254E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438135E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54378274E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496008E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.51958806E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64877074E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33997073E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33997073E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332457E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332457E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09304453E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41056530E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06989848E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33263406E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02058699E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85215143E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07668350E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52706363E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.55038955E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25297842E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51959561E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29878135E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00957794E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08119903E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     4.02542683E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00640206E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92458121E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.90167363E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77332873E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.19116309E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15685388E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14209517E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69698556E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20450440E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55908163E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20450440E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55908163E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25908837E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55807064E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55807064E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48794491E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10391570E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10391570E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10391570E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18534075E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18534075E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18534075E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18534075E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.95113649E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.95113649E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.95113649E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.95113649E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.19009332E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.19009332E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.44030423E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99749872E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.75744605E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.31260036E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.25745734E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41060255E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39876034E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01538303E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20393167E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01739504E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01739504E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33836648E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26922835E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02357548E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.68911696E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20919646E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02339566E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83580840E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74403525E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22069192E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54421934E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54421934E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42036017E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67460361E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.77974043E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06211502E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44079444E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21686576E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01616307E-01    2           5        -5   # BR(h -> b       bb     )
     6.22609355E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20376568E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66274197E-04    2           3        -3   # BR(h -> s       sb     )
     2.01159419E-02    2           4        -4   # BR(h -> c       cb     )
     6.64310457E-02    2          21        21   # BR(h -> g       g      )
     2.31885453E-03    2          22        22   # BR(h -> gam     gam    )
     1.62700845E-03    2          22        23   # BR(h -> Z       gam    )
     2.16852363E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80908931E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01144532E+01   # H decays
#          BR         NDA      ID1       ID2
     1.39031839E-03    2           5        -5   # BR(H -> b       bb     )
     2.32288872E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21227643E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05146056E-06    2           3        -3   # BR(H -> s       sb     )
     9.48916503E-06    2           4        -4   # BR(H -> c       cb     )
     9.38912856E-01    2           6        -6   # BR(H -> t       tb     )
     7.51957981E-04    2          21        21   # BR(H -> g       g      )
     2.63842542E-06    2          22        22   # BR(H -> gam     gam    )
     1.09244151E-06    2          23        22   # BR(H -> Z       gam    )
     3.15673822E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57406334E-04    2          23        23   # BR(H -> Z       Z      )
     8.53048119E-04    2          25        25   # BR(H -> h       h      )
     8.27978170E-24    2          36        36   # BR(H -> A       A      )
     3.34637615E-11    2          23        36   # BR(H -> Z       A      )
     2.51305237E-12    2          24       -37   # BR(H -> W+      H-     )
     2.51305237E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23250182E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15123527E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93543019E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.01898491E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96704944E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.32825231E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54832252E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05621555E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39315460E-03    2           5        -5   # BR(A -> b       bb     )
     2.29924082E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12864690E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07164474E-06    2           3        -3   # BR(A -> s       sb     )
     9.39102520E-06    2           4        -4   # BR(A -> c       cb     )
     9.39875077E-01    2           6        -6   # BR(A -> t       tb     )
     8.89609616E-04    2          21        21   # BR(A -> g       g      )
     2.66247064E-06    2          22        22   # BR(A -> gam     gam    )
     1.27397436E-06    2          23        22   # BR(A -> Z       gam    )
     3.07605884E-04    2          23        25   # BR(A -> Z       h      )
     6.12166914E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74236349E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47863642E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11516680E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99266972E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.14072335E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45947830E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97574335E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23546254E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34822509E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30182397E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42128546E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14113123E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02438977E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41574768E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.15263965E-04    2          24        25   # BR(H+ -> W+      h      )
     1.30391887E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79029558E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30723414E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98280916E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373701E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416682E+03   # H
        36     2.00000000E+03   # A
        37     2.00209057E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.96252893E+03   # ~d_L
   2000001     4.96241862E+03   # ~d_R
   1000002     4.96228243E+03   # ~u_L
   2000002     4.96234099E+03   # ~u_R
   1000003     4.96252893E+03   # ~s_L
   2000003     4.96241862E+03   # ~s_R
   1000004     4.96228243E+03   # ~c_L
   2000004     4.96234099E+03   # ~c_R
   1000005     4.96237534E+03   # ~b_1
   2000005     4.96257369E+03   # ~b_2
   1000006     5.09418379E+03   # ~t_1
   2000006     5.39435822E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984134E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984134E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984134E+03   # ~nu_tauL
   1000021     1.88525250E+03   # ~g
   1000022     1.91310283E+02   # ~chi_10
   1000023    -2.16758692E+02   # ~chi_20
   1000025     2.96412111E+02   # ~chi_30
   1000035     3.12904236E+03   # ~chi_40
   1000024     2.14529140E+02   # ~chi_1+
   1000037     3.12904195E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69201003E-01   # N_11
  1  2    -2.32976460E-02   # N_12
  1  3     6.31893825E-01   # N_13
  1  4    -6.16455864E-01   # N_14
  2  1     1.80285721E-02   # N_21
  2  2    -4.70680180E-03   # N_22
  2  3    -7.05102508E-01   # N_23
  2  4    -7.08860543E-01   # N_24
  3  1     8.82907250E-01   # N_31
  3  2     1.29539118E-02   # N_32
  3  3    -3.21400314E-01   # N_33
  3  4     3.42065524E-01   # N_34
  4  1     4.21121183E-04   # N_41
  4  2    -9.99633564E-01   # N_42
  4  3    -1.55719584E-02   # N_43
  4  4     2.21376353E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20146629E-02   # U_11
  1  2     9.99757648E-01   # U_12
  2  1     9.99757648E-01   # U_21
  2  2     2.20146629E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13034921E-02   # V_11
  1  2     9.99509926E-01   # V_12
  2  1     9.99509926E-01   # V_21
  2  2     3.13034921E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99750600E-01   # cos(theta_t)
  1  2    -2.23324383E-02   # sin(theta_t)
  2  1     2.23324383E-02   # -sin(theta_t)
  2  2     9.99750600E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71093526E-01   # cos(theta_b)
  1  2     8.82083267E-01   # sin(theta_b)
  2  1    -8.82083267E-01   # -sin(theta_b)
  2  2     4.71093526E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84679930E-01   # cos(theta_tau)
  1  2     7.28843875E-01   # sin(theta_tau)
  2  1    -7.28843875E-01   # -sin(theta_tau)
  2  2     6.84679930E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90210263E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51768801E+02   # vev(Q)              
         4     3.85965564E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099798E-01   # gprime(Q) DRbar
     2     6.29225241E-01   # g(Q) DRbar
     3     1.08213351E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02713320E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72367567E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79966401E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02018242E+06   # M^2_Hd              
        22    -5.47957355E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961843E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.60989380E-03   # gluino decays
#          BR         NDA      ID1       ID2
     6.65365754E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     8.63980964E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.81929083E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.05053662E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.93522043E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.37236893E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.66882474E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.71481273E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.59506896E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.00653707E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.93522043E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.37236893E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.66882474E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.71481273E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.59506896E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.00653707E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.09212756E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.13655903E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.72056166E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.54915160E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.04284945E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.71057452E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.47582247E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.47582247E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.47582247E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.47582247E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35502127E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35502127E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.00208379E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.09525382E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.22308903E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.23632337E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.52294045E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.11740263E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.03088884E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.59111105E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.35459526E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.51595921E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.81468339E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.57612132E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.02581218E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.55336671E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.86144392E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.04323140E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.73067268E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.61365526E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.49516379E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     7.49779640E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.00161206E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     3.50968817E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.52036358E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.64623195E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.69280166E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.66597173E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.36002007E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.40365816E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.73043353E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.25979512E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.26527956E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.41648245E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.13469927E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.48413650E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.73927119E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.30246448E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.49547785E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.87031480E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.12922333E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.66547522E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.56928561E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.78666047E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.22785385E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.72717834E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.45513041E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.99844075E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.02081327E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.14527033E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.68955774E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.03938756E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.37978081E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.48136522E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.12927121E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.08790275E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.75218323E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.03497255E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.22937896E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.54255741E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.45934562E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.99898701E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.94225909E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.97902368E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.39479878E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.05070675E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.79168541E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86509513E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.12922333E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.66547522E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.56928561E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.78666047E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.22785385E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.72717834E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.45513041E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.99844075E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.02081327E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.14527033E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.68955774E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.03938756E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.37978081E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.48136522E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.12927121E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.08790275E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.75218323E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.03497255E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.22937896E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.54255741E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.45934562E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.99898701E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.94225909E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.97902368E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.39479878E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.05070675E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.79168541E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86509513E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543512E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03131361E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04684644E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80161556E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484144E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77864727E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342831E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528946E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20855966E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25801094E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78818167E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60422105E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543512E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03131361E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04684644E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80161556E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484144E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77864727E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342831E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528946E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20855966E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25801094E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78818167E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60422105E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238526E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35956242E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64039777E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72952734E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460160E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259426E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107552E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677921E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832657E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14126646E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06879552E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45984173E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197236E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84913924E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604658E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528914E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74860604E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53665870E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62268411E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698335E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37058513E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022943E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528914E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74860604E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53665870E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62268411E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698335E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37058513E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022943E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849802E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74203790E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53490297E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62083009E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401613E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51125218E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430256E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26972512E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76358492E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387803E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387803E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462683E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462683E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08271393E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38825002E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03050055E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48710330E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98334212E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91779954E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03603798E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91780784E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94109006E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96027923E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90592983E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86237740E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01627572E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94983282E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.28750662E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02638301E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93156116E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20558334E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65394291E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59054850E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49079613E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56157109E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66134432E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22028286E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57942404E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22028286E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57942404E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13482763E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60413490E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60413490E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093214E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19561662E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19561662E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19561662E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.05982927E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.05982927E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.05982927E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.05982927E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.01994328E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.01994328E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01994328E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01994328E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95856293E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95856293E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.59994504E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.42555916E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77871801E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77871801E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.10602788E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.93462597E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.53231308E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38828647E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03640351E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97360504E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80764997E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97929553E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97929553E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49007149E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49601348E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70816245E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00726159E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60658370E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91124120E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.88004838E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81943362E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37734991E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93486354E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93486354E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43543185E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72652166E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78948970E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93284682E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85741218E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21697599E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01616458E-01    2           5        -5   # BR(h -> b       bb     )
     6.22607118E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20375776E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66272324E-04    2           3        -3   # BR(h -> s       sb     )
     2.01152126E-02    2           4        -4   # BR(h -> c       cb     )
     6.64298448E-02    2          21        21   # BR(h -> g       g      )
     2.32224547E-03    2          22        22   # BR(h -> gam     gam    )
     1.62695761E-03    2          22        23   # BR(h -> Z       gam    )
     2.16851764E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80901580E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01434350E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38923081E-03    2           5        -5   # BR(H -> b       bb     )
     2.32119741E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20629702E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05069421E-06    2           3        -3   # BR(H -> s       sb     )
     9.48256041E-06    2           4        -4   # BR(H -> c       cb     )
     9.38259563E-01    2           6        -6   # BR(H -> t       tb     )
     7.51419537E-04    2          21        21   # BR(H -> g       g      )
     2.63476974E-06    2          22        22   # BR(H -> gam     gam    )
     1.09160656E-06    2          23        22   # BR(H -> Z       gam    )
     3.17618793E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58376164E-04    2          23        23   # BR(H -> Z       Z      )
     8.52441488E-04    2          25        25   # BR(H -> h       h      )
     8.46856632E-24    2          36        36   # BR(H -> A       A      )
     3.37598655E-11    2          23        36   # BR(H -> Z       A      )
     2.52732183E-12    2          24       -37   # BR(H -> W+      H-     )
     2.52732183E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49634407E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87503025E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75224081E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52443212E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45518585E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45505556E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11343540E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891467E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39215415E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771186E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12324147E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093211E-06    2           3        -3   # BR(A -> s       sb     )
     9.38478031E-06    2           4        -4   # BR(A -> c       cb     )
     9.39250074E-01    2           6        -6   # BR(A -> t       tb     )
     8.89018039E-04    2          21        21   # BR(A -> g       g      )
     2.67561981E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307581E-06    2          23        22   # BR(A -> Z       gam    )
     3.09515077E-04    2          23        25   # BR(A -> Z       h      )
     5.99362420E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30122281E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12614712E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74022088E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19908920E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48793177E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69809892E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853893E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23376966E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658128E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601249E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42020861E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13753048E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367231E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40915271E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17211277E-04    2          24        25   # BR(H+ -> W+      h      )
     1.31961088E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28626402E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26678235E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55301759E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

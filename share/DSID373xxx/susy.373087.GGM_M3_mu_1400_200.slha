#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373707E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416039E+03   # H
        36     2.00000000E+03   # A
        37     2.00209149E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.99643668E+03   # ~d_L
   2000001     4.99632684E+03   # ~d_R
   1000002     4.99619123E+03   # ~u_L
   2000002     4.99624954E+03   # ~u_R
   1000003     4.99643668E+03   # ~s_L
   2000003     4.99632684E+03   # ~s_R
   1000004     4.99619123E+03   # ~c_L
   2000004     4.99624954E+03   # ~c_R
   1000005     4.99628375E+03   # ~b_1
   2000005     4.99648124E+03   # ~b_2
   1000006     5.12708305E+03   # ~t_1
   2000006     5.42520210E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     1.52201164E+03   # ~g
   1000022     1.91301220E+02   # ~chi_10
   1000023    -2.16746466E+02   # ~chi_20
   1000025     2.96409001E+02   # ~chi_30
   1000035     3.12904228E+03   # ~chi_40
   1000024     2.14516972E+02   # ~chi_1+
   1000037     3.12904187E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69156254E-01   # N_11
  1  2    -2.32979282E-02   # N_12
  1  3     6.31910285E-01   # N_13
  1  4    -6.16473039E-01   # N_14
  2  1     1.80288219E-02   # N_21
  2  2    -4.70676605E-03   # N_22
  2  3    -7.05102407E-01   # N_23
  2  4    -7.08860637E-01   # N_24
  3  1     8.82931024E-01   # N_31
  3  2     1.29525196E-02   # N_32
  3  3    -3.21368183E-01   # N_33
  3  4     3.42034398E-01   # N_34
  4  1     4.21109562E-04   # N_41
  4  2    -9.99633575E-01   # N_42
  4  3    -1.55716820E-02   # N_43
  4  4     2.21373047E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20142724E-02   # U_11
  1  2     9.99757657E-01   # U_12
  2  1     9.99757657E-01   # U_21
  2  2     2.20142724E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13030248E-02   # V_11
  1  2     9.99509940E-01   # V_12
  2  1     9.99509940E-01   # V_21
  2  2     3.13030248E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99829551E-01   # cos(theta_t)
  1  2    -1.84626365E-02   # sin(theta_t)
  2  1     1.84626365E-02   # -sin(theta_t)
  2  2     9.99829551E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71071816E-01   # cos(theta_b)
  1  2     8.82094861E-01   # sin(theta_b)
  2  1    -8.82094861E-01   # -sin(theta_b)
  2  2     4.71071816E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84680588E-01   # cos(theta_tau)
  1  2     7.28843256E-01   # sin(theta_tau)
  2  1    -7.28843256E-01   # -sin(theta_tau)
  2  2     6.84680588E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90206351E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51765848E+02   # vev(Q)              
         4     3.86332170E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099777E-01   # gprime(Q) DRbar
     2     6.29225115E-01   # g(Q) DRbar
     3     1.08619186E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02671569E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72335249E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79968574E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02026724E+06   # M^2_Hd              
        22    -5.51884676E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961785E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.66366790E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.11668003E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.44908984E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.03975831E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     1.22698598E-02    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.09279120E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.34117807E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.82522425E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     6.18001327E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.54337034E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.06187937E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.09279120E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.34117807E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.82522425E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     6.18001327E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.54337034E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.06187937E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.25331881E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.21856472E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.87976769E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.50489245E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.93984448E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.43099883E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.93095387E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.93095387E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.93095387E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.93095387E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36082703E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36082703E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.23137190E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.53051011E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.14301333E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.99219208E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.39401801E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.54817941E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.77332187E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.81330219E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.86343809E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.34497711E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     4.02857948E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.12834133E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.75105513E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.36328382E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.22064735E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.94286852E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -2.43845167E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.83017253E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.62017266E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.72084611E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     6.31949250E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.21481491E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.84556660E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.82739612E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.33106279E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.29847951E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -8.28574605E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -1.53292943E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -1.66366302E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.16127002E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.40520314E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.44684889E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.24864170E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.22239748E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.51762391E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.93768976E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.05114208E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.29693953E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.36194013E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.33223990E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.42681351E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.53403207E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.98057024E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.57052079E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.96060738E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.07563773E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.24936495E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.03767542E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.53115183E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.66071502E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.11002308E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.53000781E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.36197704E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.89126734E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.23065157E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.85057923E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.98198468E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.76773861E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.96451199E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.07612419E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.17012330E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.68895699E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.96771616E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     9.48611314E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.05940631E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87820961E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.36194013E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.33223990E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.42681351E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.53403207E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.98057024E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.57052079E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.96060738E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.07563773E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.24936495E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.03767542E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.53115183E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.66071502E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.11002308E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.53000781E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.36197704E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.89126734E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.23065157E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.85057923E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.98198468E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.76773861E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.96451199E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.07612419E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.17012330E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.68895699E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.96771616E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     9.48611314E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.05940631E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87820961E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543423E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03046259E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04698133E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80170086E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484148E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77840905E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342830E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528917E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20813901E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25810260E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78860223E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60385698E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543423E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03046259E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04698133E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80170086E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484148E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77840905E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342830E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528917E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20813901E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25810260E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78860223E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60385698E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238511E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35930679E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64053379E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72977632E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460379E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259285E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107985E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677929E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832579E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14103962E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06882153E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.46007579E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46196987E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84972352E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604153E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528826E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74761261E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53668117E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62278378E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698333E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37054467E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022950E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528826E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74761261E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53668117E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62278378E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698333E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37054467E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022950E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849722E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74104544E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53492537E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62092960E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401604E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51124014E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430248E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26647374E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76435052E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34388078E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34388078E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462775E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462775E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08270650E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38819523E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03047151E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48716860E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98331377E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91685938E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03605667E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91802088E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94124875E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96059967E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90608013E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86069336E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01629210E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94975595E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.28396925E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02611703E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93182883E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20541381E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65010944E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59191766E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49128701E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56307359E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66182294E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22029333E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57943767E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22029333E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57943767E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13473992E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60416615E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60416615E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093948E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19567912E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19567912E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19567912E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.06119090E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.06119090E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.06119090E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.06119090E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.02039716E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.02039716E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.02039716E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.02039716E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95878547E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95878547E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.62353292E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.38868694E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.78056166E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.78056166E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.05112407E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.91705127E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.50907236E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38823169E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03667229E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97357595E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80737963E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97926706E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97926706E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49016920E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49626150E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70847519E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00765338E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60678044E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91125616E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.87921848E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81859725E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37579411E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93501460E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93501460E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43547159E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72664502E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78954482E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93283890E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85706424E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21685964E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01606648E-01    2           5        -5   # BR(h -> b       bb     )
     6.22617024E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20379283E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66279845E-04    2           3        -3   # BR(h -> s       sb     )
     2.01158731E-02    2           4        -4   # BR(h -> c       cb     )
     6.64311344E-02    2          21        21   # BR(h -> g       g      )
     2.32231549E-03    2          22        22   # BR(h -> gam     gam    )
     1.62700202E-03    2          22        23   # BR(h -> Z       gam    )
     2.16857732E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80909335E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01428176E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38941134E-03    2           5        -5   # BR(H -> b       bb     )
     2.32123783E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20643992E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05071308E-06    2           3        -3   # BR(H -> s       sb     )
     9.48256929E-06    2           4        -4   # BR(H -> c       cb     )
     9.38260275E-01    2           6        -6   # BR(H -> t       tb     )
     7.51431153E-04    2          21        21   # BR(H -> g       g      )
     2.63479667E-06    2          22        22   # BR(H -> gam     gam    )
     1.09162411E-06    2          23        22   # BR(H -> Z       gam    )
     3.16495913E-04    2          24       -24   # BR(H -> W+      W-     )
     1.57816253E-04    2          23        23   # BR(H -> Z       Z      )
     8.52463097E-04    2          25        25   # BR(H -> h       h      )
     8.43016988E-24    2          36        36   # BR(H -> A       A      )
     3.35007404E-11    2          23        36   # BR(H -> Z       A      )
     2.48294990E-12    2          24       -37   # BR(H -> W+      H-     )
     2.48294990E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49645921E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87431120E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75248821E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52388207E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45494962E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45753702E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11373444E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891102E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39230994E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771393E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12324879E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093308E-06    2           3        -3   # BR(A -> s       sb     )
     9.38478876E-06    2           4        -4   # BR(A -> c       cb     )
     9.39250919E-01    2           6        -6   # BR(A -> t       tb     )
     8.89018839E-04    2          21        21   # BR(A -> g       g      )
     2.67562909E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307715E-06    2          23        22   # BR(A -> Z       gam    )
     3.08419367E-04    2          23        25   # BR(A -> Z       h      )
     5.99345651E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30095544E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12619656E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.73999613E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19832106E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48843399E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69818483E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853713E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23409047E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658342E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29602006E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42041392E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13753485E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367318E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40916276E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16088621E-04    2          24        25   # BR(H+ -> W+      h      )
     1.32251759E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28602118E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26674549E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55327243E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

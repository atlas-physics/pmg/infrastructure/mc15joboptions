#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373703E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416886E+03   # H
        36     2.00000000E+03   # A
        37     2.00209008E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.94269549E+03   # ~d_L
   2000001     4.94258490E+03   # ~d_R
   1000002     4.94244837E+03   # ~u_L
   2000002     4.94250708E+03   # ~u_R
   1000003     4.94269549E+03   # ~s_L
   2000003     4.94258490E+03   # ~s_R
   1000004     4.94244837E+03   # ~c_L
   2000004     4.94250708E+03   # ~c_R
   1000005     4.94254151E+03   # ~b_1
   2000005     4.94274036E+03   # ~b_2
   1000006     5.07489315E+03   # ~t_1
   2000006     5.37623154E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984134E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984134E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984134E+03   # ~nu_tauL
   1000021     2.06307953E+03   # ~g
   1000022     1.91312846E+02   # ~chi_10
   1000023    -2.16762150E+02   # ~chi_20
   1000025     2.96412991E+02   # ~chi_30
   1000035     3.12904240E+03   # ~chi_40
   1000024     2.14532581E+02   # ~chi_1+
   1000037     3.12904199E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69213658E-01   # N_11
  1  2    -2.32975672E-02   # N_12
  1  3     6.31889169E-01   # N_13
  1  4    -6.16451007E-01   # N_14
  2  1     1.80285015E-02   # N_21
  2  2    -4.70681211E-03   # N_22
  2  3    -7.05102536E-01   # N_23
  2  4    -7.08860517E-01   # N_24
  3  1     8.82900526E-01   # N_31
  3  2     1.29543061E-02   # N_32
  3  3    -3.21409400E-01   # N_33
  3  4     3.42074326E-01   # N_34
  4  1     4.21124486E-04   # N_41
  4  2    -9.99633560E-01   # N_42
  4  3    -1.55720372E-02   # N_43
  4  4     2.21377297E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20147743E-02   # U_11
  1  2     9.99757645E-01   # U_12
  2  1     9.99757645E-01   # U_21
  2  2     2.20147743E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13036255E-02   # V_11
  1  2     9.99509921E-01   # V_12
  2  1     9.99509921E-01   # V_21
  2  2     3.13036255E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99707115E-01   # cos(theta_t)
  1  2    -2.42009136E-02   # sin(theta_t)
  2  1     2.42009136E-02   # -sin(theta_t)
  2  2     9.99707115E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71093701E-01   # cos(theta_b)
  1  2     8.82083173E-01   # sin(theta_b)
  2  1    -8.82083173E-01   # -sin(theta_b)
  2  2     4.71093701E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84679707E-01   # cos(theta_tau)
  1  2     7.28844084E-01   # sin(theta_tau)
  2  1    -7.28844084E-01   # -sin(theta_tau)
  2  2     6.84679707E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90211255E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51769634E+02   # vev(Q)              
         4     3.85786742E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099807E-01   # gprime(Q) DRbar
     2     6.29225311E-01   # g(Q) DRbar
     3     1.08044589E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02725126E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72368769E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79965790E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02016010E+06   # M^2_Hd              
        22    -5.45357183E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961873E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.37308090E-03   # gluino decays
#          BR         NDA      ID1       ID2
     5.39627486E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     7.00155136E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.47050479E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     9.83962947E-03    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.88843567E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.40331125E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.61572638E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.57668980E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.64639859E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.98774887E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.88843567E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.40331125E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.61572638E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.57668980E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.64639859E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.98774887E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.04471807E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.11687781E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.66660493E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.56353438E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.07224710E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.76939832E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.33929900E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.33929900E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.33929900E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.33929900E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35454273E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35454273E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.87332183E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.44480152E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.27227114E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.38371605E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.60229333E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.52604996E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.18944616E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.45389732E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.66986902E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.62581090E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.69358982E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.84115309E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.05598846E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.67037912E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.11779871E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.10293666E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.24955263E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.48447790E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.98127071E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     9.24509688E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.23315987E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     4.32094777E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.34828210E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.23320706E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.94678751E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.92422344E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.71080167E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.99586047E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -3.43453310E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.32636927E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.17703128E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.42810373E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.12066126E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -3.86655228E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.85938553E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.50481919E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.73620600E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.63424804E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.99929556E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.88379432E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.66269227E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.95240495E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.38807491E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.82993295E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.77554388E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.94839868E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.89335369E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.21611519E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.79396606E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.28899214E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.55537111E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44930983E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.99934898E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.15265506E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.09431821E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.15597421E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.38967125E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.05075811E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.77995941E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.94898462E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.81520721E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.17122635E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.67807049E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.11842749E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.27161647E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85639820E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.99929556E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.88379432E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.66269227E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.95240495E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.38807491E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.82993295E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.77554388E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.94839868E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.89335369E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.21611519E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.79396606E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.28899214E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.55537111E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44930983E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.99934898E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.15265506E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.09431821E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.15597421E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.38967125E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.05075811E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.77995941E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.94898462E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.81520721E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.17122635E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.67807049E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.11842749E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.27161647E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85639820E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543558E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03155398E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04680787E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80159134E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484147E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77871545E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342840E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528958E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20867862E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25798503E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78806273E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60432444E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543558E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03155398E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04680787E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80159134E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484147E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77871545E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342840E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528958E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20867862E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25798503E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78806273E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60432444E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238541E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35963477E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64035934E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72945709E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460091E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259480E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107415E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677913E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832694E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14133049E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06878813E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45977508E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197326E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84897075E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604839E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528960E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74888673E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53665234E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62265581E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698340E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37059673E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022950E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528960E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74888673E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53665234E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62265581E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698340E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37059673E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022950E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849846E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74231832E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53489664E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62080184E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401620E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51125569E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430267E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.27064583E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76336782E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387725E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387725E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462657E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462657E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08271603E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38826614E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03050876E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48708486E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98335017E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91806557E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03603252E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91774571E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94104491E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96018982E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90588887E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86285706E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01627093E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94985691E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     9.28850730E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02645828E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93148536E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20563643E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65502823E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59001017E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49065721E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56114545E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66120777E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22028011E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57942042E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22028011E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57942042E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13485260E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60412657E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60412657E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093056E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19559992E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19559992E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19559992E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.05944863E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.05944863E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.05944863E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.05944863E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.01981640E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.01981640E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.01981640E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.01981640E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95850943E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95850943E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.59328770E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.43608680E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77819162E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77819162E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.12170458E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.93964405E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.53895176E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38830258E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03632756E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97361327E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80772644E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97930361E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97930361E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49004367E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49594515E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70807382E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00715729E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60652595E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91123679E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.88028254E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81967529E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37778942E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93482238E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93482238E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43542006E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72648573E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78947661E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93284613E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85751044E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21704933E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01622775E-01    2           5        -5   # BR(h -> b       bb     )
     6.22598134E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20372596E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66265570E-04    2           3        -3   # BR(h -> s       sb     )
     2.01148361E-02    2           4        -4   # BR(h -> c       cb     )
     6.64291520E-02    2          21        21   # BR(h -> g       g      )
     2.32220063E-03    2          22        22   # BR(h -> gam     gam    )
     1.62692945E-03    2          22        23   # BR(h -> Z       gam    )
     2.16847986E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80896693E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01435982E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38916684E-03    2           5        -5   # BR(H -> b       bb     )
     2.32118725E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20626111E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05068944E-06    2           3        -3   # BR(H -> s       sb     )
     9.48255827E-06    2           4        -4   # BR(H -> c       cb     )
     9.38259404E-01    2           6        -6   # BR(H -> t       tb     )
     7.51413379E-04    2          21        21   # BR(H -> g       g      )
     2.63474762E-06    2          22        22   # BR(H -> gam     gam    )
     1.09160209E-06    2          23        22   # BR(H -> Z       gam    )
     3.17903991E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58518375E-04    2          23        23   # BR(H -> Z       Z      )
     8.52435700E-04    2          25        25   # BR(H -> h       h      )
     8.52059121E-24    2          36        36   # BR(H -> A       A      )
     3.38424380E-11    2          23        36   # BR(H -> Z       A      )
     2.54274519E-12    2          24       -37   # BR(H -> W+      H-     )
     2.54274519E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49631965E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87524801E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75217377E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52459564E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45525313E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45436393E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11335217E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891551E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39209673E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771139E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12323979E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093189E-06    2           3        -3   # BR(A -> s       sb     )
     9.38477836E-06    2           4        -4   # BR(A -> c       cb     )
     9.39249879E-01    2           6        -6   # BR(A -> t       tb     )
     8.89017854E-04    2          21        21   # BR(A -> g       g      )
     2.67561730E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307561E-06    2          23        22   # BR(A -> Z       gam    )
     3.09793240E-04    2          23        25   # BR(A -> Z       h      )
     5.99367244E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30129847E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12613330E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74028444E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19930646E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48778975E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69807463E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853875E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23362244E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658081E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601083E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42011439E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13752962E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367214E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40915018E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17496202E-04    2          24        25   # BR(H+ -> W+      h      )
     1.31806601E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28633272E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26679296E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55294558E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

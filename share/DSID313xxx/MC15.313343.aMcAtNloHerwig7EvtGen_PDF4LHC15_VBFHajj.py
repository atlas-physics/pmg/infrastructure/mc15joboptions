from MadGraphControl.MadGraphUtils import *
import os
import fileinput
import re

nevents=3*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
evgenConfig.minevents=5000
mode=0
gridpack_mode = True
gridpack_dir='madevent/'
#qCut=25
#---------------------------------------------------------------------------
# Process type based on runNumber:
#---------------------------------------------------------------------------
if runArgs.runNumber==313343:
    process1='generate p p > h a j j [QCD] @0'
    evgenConfig.keywords = ['Higgs','photon','bottom','VBFHiggs']
    description='MadGraph_aMC@NLO h->jj 0,2jets@NLO'
    ptgmin=10
    name = runArgs.jobConfig[0]
    thisDSID = int(name.split(".")[1])
    runName = str(thisDSID)
    gridpack='MG5_aMCatNLO.313343'
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

if hasattr(runArgs, 'inputGenConfFile'):
    if not gridpack in runArgs.inputGenConfFile:
        raise RuntimeError("Please use the correct gridpack for this DSID.")

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
set complex_mass_scheme
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+process1+"""
output -f
""")
fcard.close()

#----------------------------------------------------------------------------
# Run Number
#----------------------------------------------------------------------------
if not hasattr(runArgs,'runNumber'):
    raise RunTimeError("No run number found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = { 'lhe_version' : '3.0',
           'parton_shower' :'HERWIGPP',
           'pdlabel'       :"'lhapdf'",
           'lhaid'         : "90400",
           'ickkw'         : 0,
           'ptj'           : "15.0",
           'etaj'          : "-1.0",
           'jetradius'     : "0.4",
           'ptgmin'        : ptgmin,
           'epsgamma'      :'0.1', #'0.1',
           'R0gamma'       :'0.1',#'0.1',
           'xn'            :'2', #'2',
           'isoEM'         :'True',
           'etagamma'      :'3.0',
           'bwcutoff'      :'15',
           'maxjetflavor'  : 5,
           'reweight_scale':'True',
           'event_norm' : 'sum',
           'reweight_PDF'  :'True',
           'dynamical_scale_choice' : '-1', #default value# @1
           'store_rwgt_info':'True'
}


                              

process_dir = new_process(grid_pack=gridpack_dir)


if not hasattr(runArgs, 'inputGenConfFile'):
    with open("PROCNLO_loop_sm-no_b_mass_0/Cards/FKS_params.dat","rw") as file:
        for line in file:
            print line

    print "editing FKS file"

    for line in fileinput.input("PROCNLO_loop_sm-no_b_mass_0/Cards/FKS_params.dat", inplace=2):
        line = re.sub('1.0d-5', '-1d0', line.rstrip())
        print(line)

    print line

else:
    print "FKS file already corrected"

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', 
               xqcut=15,
               nevts=nevents,
               rand_seed=randomSeed,
               beamEnergy=beamEnergy,
               extras=extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
njobs=1
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    mode=2
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

nJobs=int(njobs)
madspin_card_loc=None

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,random_seed=runArgs.randomSeed,required_accuracy=0.001,nevents=nevents)
#--------------------------------------------------------------------------------------------------------------------
# Shower
#-----------------------------------------------------------------------------------------------------------------
name="hajj"
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
outputDS=arrange_output(run_name=runName, proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)
evgenConfig.generators  += ["aMcAtNlo","Herwig7"]
evgenConfig.description = 'MadGraph_higgs_NLO_Herwigpp'
evgenConfig.keywords+=['jets']
evgenConfig.inputconfcheck = runName
runArgs.inputGeneratorFile=outputDS
evgenConfig.tune =  "PDF4LHC15_nlo_mc_pdfas" 


print "runArgs.inputGeneratorFile",runArgs.inputGeneratorFile
lhe_filename=runArgs.inputGeneratorFile
print "lhe_file print test",lhe_filename


from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME
from Herwig7_i.Herwig7ConfigLHEF import Hw7ConfigLHEF

genSeq += Herwig7()

Herwig7Config = Hw7ConfigLHEF(genSeq,runArgs)
#HERWIGPP_qCut=qCut 
HERWIGPP_nJetMax=1 


# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_mc_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO") 


# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

import os
if "HERWIG7VER" in os.environ:
   version = os.getenv("HERWIG7VER")
   verh7 = version.split(".")[1]
else:
   verh7 = 0

if int(verh7 == 0):
#   Herwig7Config.add_commands("""
# insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
#set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV   
    Herwig7Config.add_commands("""
 do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
 do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
 do /Herwig/Particles/h0:PrintDecayModes
 do /Herwig/Particles/Z0:PrintDecayModes # print out decays modes and branching ratios to the terminal/log.generate
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 
 """)

else:
#   Herwig7Config.add_commands("""
   Herwig7Config.add_commands("""
## ------------------
## Hard process setup
 do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
 do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
 do /Herwig/Particles/h0:PrintDecayModes
 do /Herwig/Particles/Z0:PrintDecayModes # print out decays modes and branching ratios to the terminal/log.generate
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 """)


# run Herwig7
Herwig7Config.run()

include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
#--------------------------------------------------------------------------------------------------------------------
# Mjj filtering:
#--------------------------------------------------------------------------------------------------------------------




#--------------------------------------------------------------------------------------------------------------------
# Sample information:
#--------------------------------------------------------------------------------------------------------------------

"""evgenConfig.generators = ["aMcAtNlo", "Herwig7"]
evgenConfig.contact = ['Han CUI <hacui@cern.ch>']
evgenConfig.description = description
runArgs.inputGeneratorFile=name+'._00001.events.tar.gz'
evgenConfig.minevents=minevents"""



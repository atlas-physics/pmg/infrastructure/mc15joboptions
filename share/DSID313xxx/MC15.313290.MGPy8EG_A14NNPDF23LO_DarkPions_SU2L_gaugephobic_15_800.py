dmmodel = 'Gaugephobic_SU2L'
dpionmass = 800.0
fermioneta = 0.15
ndark = 4

include("MC15JobOptions/MadGraphControl_DarkPions_Pythia8EvtGen_A14_NNPDF23LO.py")

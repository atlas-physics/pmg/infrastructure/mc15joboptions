from MadGraphControl.MadGraphUtils import *
import os
import fileinput
import re

nevents=3*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
#nevents=5000
evgenConfig.minevents=5000
gridpack_mode=False
mode=0
gridpack_dir=None #'madevent/'

#---------------------------------------------------------------------------
# Process type based on runNumber:
#---------------------------------------------------------------------------
if runArgs.runNumber==313344:
    process1='generate p p > h a j j'
    evgenConfig.keywords = ['Higgs','photon','bottom','VBFHiggs']
    description='MadGraph@LO h->jj 0,2jets@LO'
    name = runArgs.jobConfig[0]
    ptgmin=10
    thisDSID = int(name.split(".")[1])
    runName = str(thisDSID)
    gridpack='313344'

else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
set complex_mass_scheme
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
"""+process1+"""
output -f
""")
fcard.close()

#----------------------------------------------------------------------------
# Run Number
#----------------------------------------------------------------------------
if not hasattr(runArgs,'runNumber'):
    raise RunTimeError("No run number found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
parton_shower='HERWIGPP'
extras = { 'lhe_version' : '3.0',
           'pdlabel'       :"'lhapdf'",
           'lhaid'         : "90400",
           'ickkw'         : 0,
           'ptj'           : "15.0",
           'etaj'          : "-1.0",
           'epsgamma'      :'0.1', #'0.1',
           'R0gamma'       :'0.1',#'0.1',
           'xn'            :'2', #'2',
           'isoEM'         :'True',
           'bwcutoff'      :'15',
           'maxjetflavor'  : 5,
           'event_norm' : 'sum',
}
                              

process_dir = new_process(grid_pack="madevent/")


build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
njobs=1
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    mode=2
    print opts
nJobs=int(njobs)
madspin_card_loc=None
name="hajj"
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,random_seed=runArgs.randomSeed,required_accuracy=0.001,nevents=nevents)


#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
outputDS=arrange_output(run_name=runName, proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)
 
evgenConfig.generators  += ["MadGraph","Herwig7"]
evgenConfig.description = 'MadGraph_higgs_LO_Herwigpp'
evgenConfig.keywords+=['jets']
runArgs.inputGeneratorFile=outputDS

print "runArgs.inputGeneratorFile",runArgs.inputGeneratorFile
lhe_filename=runArgs.inputGeneratorFile
print "lhe_file print test",lhe_filename


from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME
from Herwig7_i.Herwig7ConfigLHEF import Hw7ConfigLHEF

genSeq += Herwig7()

Herwig7Config = Hw7ConfigLHEF(genSeq,runArgs)

# configure Herwig7
#Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_mc_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="LO") 


# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")


import os
if "HERWIG7VER" in os.environ:
   version = os.getenv("HERWIG7VER")
   verh7 = version.split(".")[1]
else:
   verh7 = 0

if int(verh7 == 0):
    Herwig7Config.add_commands("""
 do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
 do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
 do /Herwig/Particles/h0:PrintDecayModes
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 
 """)

else:
   Herwig7Config.add_commands("""
## ------------------
## Hard process setup
 do /Herwig/Particles/h0:SelectDecayModes h0->Z0,Z0;
 do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
 do /Herwig/Particles/h0:PrintDecayModes
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 """)


# run Herwig7
Herwig7Config.run()

# add MET Filter
include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV

#--------------------------------------------------------------------------------------------------------------------
# Sample information:
#--------------------------------------------------------------------------------------------------------------------

"""evgenConfig.generators = ["MadGraph", "Herwig7"]
evgenConfig.contact = ['Han CUI <hacui@cern.ch>']
evgenConfig.description = description
runArgs.inputGeneratorFile=name+'._00001.events.tar.gz'
evgenConfig.minevents=minevents"""



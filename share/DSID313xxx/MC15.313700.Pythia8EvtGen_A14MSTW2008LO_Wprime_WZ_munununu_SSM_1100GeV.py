# Wprime resonance mass (in GeV)
WprimeMass = 1100

include('/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/nonStandard/Pythia8/Pythia8_A14_MSTW2008LO_EvtGen_Common.py')


genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",  # create W' bosons
			    "Wprime:coup2WZ = 1.",
                            "34:onMode = off",                     # switch off all W' decays
                            "34:onIfAny = 23,24",                  # switch on W'->WZ decays
			    "23:onMode = off",			#switch off all Z decays
                            "23:onIfAny = 14",			# switch on Z-> nunu
			    "24:onMode = off",			#switch off all W decays
			    "24:onIfAny = 13,14",		#switch on W->munu 
                            "34:m0 = "+str(WprimeMass)]

# EVGEN configuration
evgenConfig.description = 'Pythia 8 Wprime decaying to munu'
evgenConfig.keywords    = [ 'BSM', 'Wprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak', 'lepton', 'neutrino' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Wprime>WZ>munununu"

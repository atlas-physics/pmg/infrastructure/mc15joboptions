## Pythia8 W->mu+HNL & HNL->mu el nu_el

evgenConfig.description = "W->mu+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W"]
evgenConfig.contact = ["Dominique Anderson Trischuk, d.trischuk@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from EvgenProdTools.EvgenProdToolsConf import TestHepMC
genSeq += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm

# id:all = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0 isResonance mayDecay doExternalDecays isVisible doForceWidth
genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 2.5 0.0 0.0 0.0 1.0 0 1 0 1 0", # 2.5 GeV & 1.0 mm
							"50:isResonance = false",
							# id:addChannel = onMode bRatio meMode product1 product2 ...
							"50:addChannel = 1 0.50 23 -13  11 -12",# HNL decay in mu+ el- nu_el
							"50:addChannel = 1 0.50 23  13 -11 12",# HNL decay in mu- el+ nu_el
							"50:mayDecay = on",
							"WeakSingleBoson:ffbar2W = on", # create W+ and W- bosons
							"24:onMode = off", # switch off all W decays
							"24:addchannel = 1 1.0 103 -13 50",# W decay in mu+ HNL
							"ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
							"ParticleDecays:tau0Max = 600.0"]


#--------------------------------------------------------------
# Powheg VBF_H setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_VBF_H_Common.py')

# Set Powheg variables, overriding defaults
# Note: width_H will be overwritten in case of CPS.
PowhegConfig.mass_H  = 1000.
PowhegConfig.width_H = 0.00407

PowhegConfig.complexpolescheme = 1 # use CPS

# Increase number of events requested to compensate for potential Pythia losses
PowhegConfig.nEvents *= 2.5

PowhegConfig.PDF = range(90400,90433) + range(260000,260101) + [11068] + [25200] + [13165]
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 1.0, 2.0]

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
#genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 12 12',
                             '23:onIfMatch = 14 14',
                             '23:onIfMatch = 16 16' ]

include('MC15JobOptions/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ZZ->4nu mh=1000 GeV CPS"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "ZZ" ]
evgenConfig.contact     = ['bcarlson@cern.ch']
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.inputconfcheck = "VBFH125_NNPDF30"

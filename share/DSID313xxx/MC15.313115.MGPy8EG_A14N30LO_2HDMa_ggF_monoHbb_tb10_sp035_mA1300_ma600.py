THDMparams = {}
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['tanbeta'] = 1 # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427. 
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0.35 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['MXd'] = 10. # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.THDMparams['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.
THDMparams['mh2'] = 1300 # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = 1300 # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mhc'] = 1300 # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh4'] = 600 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['MB'] = 4.7 # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.

initialGluons = True # Determines initial state to generate from. True --> top loop induced production (gluon Fusion). False --> b b~ -initiated production (b-anti-b annihilation). See Sections 5.4 and 6.5 of arXiv:1701.07427 for further details

decayChannel="monoHbb"

reweight = True # Determines whether to store alternative weights for tanb and sinp
Reweight = {}
Reweight['tanbeta'] = "scan:[1, 5, 25, 50]"
Reweight['sinp'] = "scan:[0.1, 0.71, 1.0]"

# Event multiplier
if THDMparams['mh4'] == 100 and THDMparams['mh3'] == 250 and initialGluons == False:
    multiplier=15
elif (THDMparams['mh4'] == 150 and THDMparams['mh3'] == 300) or (THDMparams['mh4'] == 100 and (THDMparams['mh3'] in [250,900,1200,1500,1800])):
    multiplier=10
else:    
    multiplier=4
    
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30LO_Pseudoscalar_2HDMa_monoHbb.py")

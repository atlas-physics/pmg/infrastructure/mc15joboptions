# mass of the seesaw heavy leptons
mL = 1100.0

# filters
enableFilters = True
lightLeptons = True

# load configuration
include('common/MadGraphControl_MGPy8EG_N30LO_A14N23LO_TypeIIISeesaw.py')

# metadata
evgenConfig.description = 'MadGraph5+Pythia8 Type III Seesaw Model, Mass: 1100 GeV, Ve=Vu=Vt=0.055'
evgenConfig.contact = ['revital.kopeliansky@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic', 'seeSaw']

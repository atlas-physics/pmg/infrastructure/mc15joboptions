# Wprime resonance mass (in GeV)
WprimeMass = 800
include('/cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/nonStandard/Pythia8/Pythia8_A14_MSTW2008LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",  # create W' bosons
                            "34:onMode = off",                     # switch off all W' decays
                            "34:onIfAny = 13,14",                  # switch on W'->munu decays
                            "34:m0 = "+str(WprimeMass)]

# EVGEN configuration
evgenConfig.description = 'Pythia 8 Wprime decaying to munu'
evgenConfig.keywords    = [ 'BSM', 'Wprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak', 'lepton', 'neutrino' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Wprime>munu"

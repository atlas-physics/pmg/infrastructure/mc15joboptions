#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.05375698E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05407649E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414989E+03   # H
        36     2.00000000E+03   # A
        37     2.00179212E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013266E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989264E+03   # ~u_L
   2000002     4.99994939E+03   # ~u_R
   1000003     5.00013266E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989264E+03   # ~c_L
   2000004     4.99994939E+03   # ~c_R
   1000005     4.99965203E+03   # ~b_1
   2000005     5.00050736E+03   # ~b_2
   1000006     4.99157697E+03   # ~t_1
   2000006     5.01287343E+03   # ~t_2
   1000011     5.00008205E+03   # ~e_L
   2000011     5.00007592E+03   # ~e_R
   1000012     4.99984203E+03   # ~nu_eL
   1000013     5.00008205E+03   # ~mu_L
   2000013     5.00007592E+03   # ~mu_R
   1000014     4.99984203E+03   # ~nu_muL
   1000015     4.99979957E+03   # ~tau_1
   2000015     5.00035901E+03   # ~tau_2
   1000016     4.99984203E+03   # ~nu_tauL
   1000021     1.80000000E+03   # ~g
   1000022     1.04324675E+03   # ~chi_10
   1000023    -1.05238202E+03   # ~chi_20
   1000025     1.06128778E+03   # ~chi_30
   1000035     3.00160447E+03   # ~chi_40
   1000024     1.05135802E+03   # ~chi_1+
   1000037     3.00160419E+03   # ~chi_2+
   1000039     4.62714837E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.06099727E-01   # N_11
  1  2    -5.78873567E-03   # N_12
  1  3    -5.71407746E-01   # N_13
  1  4    -5.53265578E-01   # N_14
  2  1     2.06774730E-02   # N_21
  2  2    -1.91089687E-02   # N_22
  2  3     7.06666274E-01   # N_23
  2  4    -7.06986610E-01   # N_24
  3  1     7.95119707E-01   # N_31
  3  2     5.49190951E-03   # N_32
  3  3     4.17197140E-01   # N_33
  3  4     4.40114799E-01   # N_34
  4  1     4.63148506E-04   # N_41
  4  2    -9.99785565E-01   # N_42
  4  3    -7.90642180E-03   # N_43
  4  4     1.91336668E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.11730113E-02   # U_11
  1  2     9.99937580E-01   # U_12
  2  1     9.99937580E-01   # U_21
  2  2     1.11730113E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.70586584E-02   # V_11
  1  2    -9.99633847E-01   # V_12
  2  1     9.99633847E-01   # V_21
  2  2     2.70586584E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08047873E-01   # cos(theta_t)
  1  2    -7.06164435E-01   # sin(theta_t)
  2  1     7.06164435E-01   # -sin(theta_t)
  2  2     7.08047873E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.61241833E-01   # cos(theta_b)
  1  2     7.50172806E-01   # sin(theta_b)
  2  1    -7.50172806E-01   # -sin(theta_b)
  2  2    -6.61241833E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03216226E-01   # cos(theta_tau)
  1  2     7.10976047E-01   # sin(theta_tau)
  2  1    -7.10976047E-01   # -sin(theta_tau)
  2  2    -7.03216226E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90200288E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51851457E+02   # vev(Q)              
         4     4.36016340E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52808644E-01   # gprime(Q) DRbar
     2     6.27339096E-01   # g(Q) DRbar
     3     1.08213258E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02705110E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72704019E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79779024E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.05375698E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.80000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.82008413E+06   # M^2_Hd              
        22    -6.47473709E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37121514E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.75687529E-04   # gluino decays
#          BR         NDA      ID1       ID2
     2.48898388E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.95091866E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.49524682E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.84716466E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.19030792E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.80338944E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.27388948E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.55860911E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.01112927E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.84716466E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.19030792E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.80338944E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.27388948E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.55860911E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.01112927E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     4.03048806E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.24548547E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.84009772E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.10638316E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.44397711E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.01810332E-01    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     2.19295200E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.19295200E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.19295200E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.19295200E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34258528E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.34258528E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.08439274E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     5.08937871E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.03283117E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.41125752E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.34942787E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.02861846E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.69504305E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.28403966E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.20916921E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.90853904E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.07267525E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.93712499E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.12850250E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.08071328E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.25286728E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.32390809E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.41429761E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.28642460E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.46920863E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.46063959E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.46781344E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.27254125E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.94039254E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.21882059E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.48316016E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.51620790E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.95103220E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.29913572E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.69734790E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.43158967E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.40014500E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     7.99855657E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.22415052E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     9.03832991E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.80073663E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.85070952E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.48659459E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.13662310E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.97228534E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.92524988E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.08216140E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.71507637E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.99295640E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.94229679E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.49490621E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.53406334E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.22419226E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.10837476E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.10025091E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.59705624E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.48828240E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.93803282E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.97747602E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.92576602E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00944829E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44292385E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.16277532E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.62205234E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.16444651E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87929860E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.22415052E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.03832991E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.80073663E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.85070952E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.48659459E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.13662310E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.97228534E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.92524988E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.08216140E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.71507637E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.99295640E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.94229679E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.49490621E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.53406334E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.22419226E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.10837476E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.10025091E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.59705624E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.48828240E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.93803282E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.97747602E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.92576602E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00944829E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44292385E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.16277532E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.62205234E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.16444651E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87929860E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.96884734E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.77202190E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.36948015E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.23195596E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.69485020E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.50378971E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.39415091E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.26090597E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.68095597E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.27731963E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.31476575E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.61225119E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.96884734E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.77202190E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.36948015E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.23195596E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.69485020E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.50378971E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.39415091E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.26090597E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.68095597E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.27731963E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.31476575E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.61225119E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.61401982E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.04796418E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.64844782E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.40304088E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.51423599E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.14316563E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.03096670E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     6.42707922E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.62454514E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.90456473E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.36013152E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.44788973E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.54021315E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.07562708E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.08297480E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.96866139E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.24812125E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.68964350E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.17290071E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.69740759E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.81985910E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.39037007E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.96866139E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.24812125E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.68964350E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.17290071E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.69740759E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.81985910E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.39037007E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.97159861E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.24095699E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.68401969E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.17174138E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.69474139E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.86959160E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.38504160E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.32354829E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.45355230E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.52701128E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04390981E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17567723E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17468102E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.33365431E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     9.59069658E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53091068E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49618325E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46039906E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.79396646E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53311022E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.36208783E-08    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.02348902E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.18331857E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.49250399E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.32417744E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.96179178E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.64673213E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02610457E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.91172336E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.39549064E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.80685535E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18878228E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80046196E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.12611904E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.12329748E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.24899701E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.23991990E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.23991990E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.23991990E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.32109450E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.32109450E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.40364862E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.40364862E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.17940691E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.17940691E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.91264754E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.13434318E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.72905997E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.17633706E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.30193916E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.75051857E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     2.31460450E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.61455718E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     2.31160475E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.26304285E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     5.67474726E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     5.67184562E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.92309037E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.03554190E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.03554190E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.03554190E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.39985351E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.10731533E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.02597000E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.09575127E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.09595583E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.09085439E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.50684898E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.41701382E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.41701382E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.41701382E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.39589546E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.39589546E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26453574E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26453574E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.65293606E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.65293606E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.65030432E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.65030432E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.98508326E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.98508326E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     9.59095293E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.35790022E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.70693278E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     8.05910464E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46412239E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46412239E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.76670333E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.16200224E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.98578552E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.27557865E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.03435629E-08    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.96527962E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.90980047E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.06291218E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07859381E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17093897E-01    2           5        -5   # BR(h -> b       bb     )
     6.38591236E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26037779E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78994499E-04    2           3        -3   # BR(h -> s       sb     )
     2.06650089E-02    2           4        -4   # BR(h -> c       cb     )
     6.71000639E-02    2          21        21   # BR(h -> g       g      )
     2.30163482E-03    2          22        22   # BR(h -> gam     gam    )
     1.54015796E-03    2          22        23   # BR(h -> Z       gam    )
     2.01073382E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56617002E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78127378E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48054068E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428323E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71215866E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11546385E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666723E-05    2           4        -4   # BR(H -> c       cb     )
     9.96054518E-01    2           6        -6   # BR(H -> t       tb     )
     7.97622162E-04    2          21        21   # BR(H -> g       g      )
     2.74679029E-06    2          22        22   # BR(H -> gam     gam    )
     1.15988084E-06    2          23        22   # BR(H -> Z       gam    )
     3.34147002E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66617852E-04    2          23        23   # BR(H -> Z       Z      )
     9.04165884E-04    2          25        25   # BR(H -> h       h      )
     8.69743654E-24    2          36        36   # BR(H -> A       A      )
     3.51212218E-11    2          23        36   # BR(H -> Z       A      )
     5.06643191E-12    2          24       -37   # BR(H -> W+      H-     )
     5.06643191E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82385660E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48335580E-03    2           5        -5   # BR(A -> b       bb     )
     2.43895558E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62258904E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676388E-06    2           3        -3   # BR(A -> s       sb     )
     9.96167652E-06    2           4        -4   # BR(A -> c       cb     )
     9.96987154E-01    2           6        -6   # BR(A -> t       tb     )
     9.43667282E-04    2          21        21   # BR(A -> g       g      )
     2.97320706E-06    2          22        22   # BR(A -> gam     gam    )
     1.35247724E-06    2          23        22   # BR(A -> Z       gam    )
     3.25641038E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74524200E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38513825E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238187E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147024E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51649998E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45685178E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729835E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402708E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33844692E-04    2          24        25   # BR(H+ -> W+      h      )
     6.49014576E-13    2          24        36   # BR(H+ -> W+      A      )

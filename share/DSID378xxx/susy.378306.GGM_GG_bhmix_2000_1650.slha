#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.65426550E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05418584E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414555E+03   # H
        36     2.00000000E+03   # A
        37     2.00174215E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013258E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989273E+03   # ~u_L
   2000002     4.99994937E+03   # ~u_R
   1000003     5.00013258E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989273E+03   # ~c_L
   2000004     4.99994937E+03   # ~c_R
   1000005     4.99940991E+03   # ~b_1
   2000005     5.00074935E+03   # ~b_2
   1000006     4.98547011E+03   # ~t_1
   2000006     5.01895121E+03   # ~t_2
   1000011     5.00008195E+03   # ~e_L
   2000011     5.00007595E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008195E+03   # ~mu_L
   2000013     5.00007595E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99963980E+03   # ~tau_1
   2000015     5.00051869E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.00000000E+03   # ~g
   1000022     1.64327474E+03   # ~chi_10
   1000023    -1.65186336E+03   # ~chi_20
   1000025     1.66138702E+03   # ~chi_30
   1000035     3.00146710E+03   # ~chi_40
   1000024     1.65111153E+03   # ~chi_1+
   1000037     3.00146682E+03   # ~chi_2+
   1000039     1.68207099E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.01208446E-01   # N_11
  1  2    -8.65507086E-03   # N_12
  1  3    -5.70749740E-01   # N_13
  1  4    -5.59212149E-01   # N_14
  2  1     1.31772342E-02   # N_21
  2  2    -1.66428202E-02   # N_22
  2  3     7.06867648E-01   # N_23
  2  4    -7.07027231E-01   # N_24
  3  1     7.98983347E-01   # N_31
  3  2     7.55379518E-03   # N_32
  3  3     4.17815031E-01   # N_33
  3  4     4.32433984E-01   # N_34
  4  1     6.12673812E-04   # N_41
  4  2    -9.99795502E-01   # N_42
  4  3    -3.66905286E-03   # N_43
  4  4     1.98775305E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.17673135E-03   # U_11
  1  2     9.99986601E-01   # U_12
  2  1     9.99986601E-01   # U_21
  2  2     5.17673135E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.81129665E-02   # V_11
  1  2    -9.99604752E-01   # V_12
  2  1     9.99604752E-01   # V_21
  2  2     2.81129665E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07704318E-01   # cos(theta_t)
  1  2    -7.06508739E-01   # sin(theta_t)
  2  1     7.06508739E-01   # -sin(theta_t)
  2  2     7.07704318E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.78202487E-01   # cos(theta_b)
  1  2     7.34875082E-01   # sin(theta_b)
  2  1    -7.34875082E-01   # -sin(theta_b)
  2  2    -6.78202487E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04688188E-01   # cos(theta_tau)
  1  2     7.09517130E-01   # sin(theta_tau)
  2  1    -7.09517130E-01   # -sin(theta_tau)
  2  2    -7.04688188E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90198778E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51963624E+02   # vev(Q)              
         4     4.64511754E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52728805E-01   # gprime(Q) DRbar
     2     6.26831421E-01   # g(Q) DRbar
     3     1.08044324E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02706004E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72927408E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79655601E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.65426550E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.22628363E+03   # M^2_Hd              
        22    -7.95573696E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36895020E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     1.83207990E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     2.76051704E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     9.55864208E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     6.66398888E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.19374355E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     8.77867299E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.17690111E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     9.14530780E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.06337469E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     6.66398888E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.19374355E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     8.77867299E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.17690111E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     9.14530780E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.06337469E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.03560947E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     3.51773044E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     8.76717891E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.17559386E-03    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.18473430E-05    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.12489312E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.12489312E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.12489312E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.12489312E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.45796973E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     1.45796973E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.81358606E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.96812400E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.76221422E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.90719043E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.47546376E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.74244933E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.94589113E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.41986671E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.95483034E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.49961422E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.01705747E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.67733621E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.22537687E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.02307130E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.44545914E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.47509258E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.25209155E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.02216863E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.29843621E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.18914644E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.60523629E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.19617026E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.21666148E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.25909697E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.30508082E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.43523409E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.59360645E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.07602017E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.79273617E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.32527499E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.59236773E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.07950847E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.09894904E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.75149348E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.43241540E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.76261871E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.68890667E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.12771546E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.37669564E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.86679113E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.94533726E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56993173E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.52281548E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.75791338E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.41617028E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.56714018E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.09900264E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.05437701E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.60618911E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.44060493E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69108185E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.82399662E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.38329010E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.86731413E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.88223317E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.05650568E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.94380085E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.12610178E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.17468998E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.88815447E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.09894904E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.75149348E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.43241540E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.76261871E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.68890667E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.12771546E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.37669564E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.86679113E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.94533726E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56993173E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.52281548E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.75791338E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.41617028E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.56714018E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.09900264E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.05437701E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.60618911E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.44060493E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69108185E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.82399662E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.38329010E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.86731413E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.88223317E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.05650568E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.94380085E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.12610178E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.17468998E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.88815447E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.89030437E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.84621310E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     4.56924710E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.11827343E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.76347710E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.87666809E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.53288357E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.96267647E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.62693042E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.73793933E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.37132971E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.93698034E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.89030437E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.84621310E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     4.56924710E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.11827343E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.76347710E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.87666809E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.53288357E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.96267647E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.62693042E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.73793933E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.37132971E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.93698034E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.42612126E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.87719705E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.81881258E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.20927414E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.63488279E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.56054858E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.27326661E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     5.23936730E-09    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.43450636E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.75419152E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.05937370E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.26955702E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.65161521E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     7.26209848E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.30678041E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.89011272E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.47626839E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.10573210E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.04557793E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.76704309E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.48378425E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.52816262E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.89011272E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.47626839E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.10573210E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.04557793E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.76704309E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.48378425E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.52816262E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.89266175E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.47056146E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.10299530E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.04465656E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.76460475E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.72905693E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.52328897E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.96260678E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.26081627E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.55064175E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.03273661E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.18355357E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.18247931E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.24507133E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.57932615E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53567017E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48749208E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46542478E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.93515021E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.51789794E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.15826188E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.69179534E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.02968136E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51033293E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.45998572E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.23626195E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     2.37034891E-07    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02052865E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.95091009E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.40491151E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.81923211E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.16985260E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.81195593E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.15522234E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.15201429E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.15268313E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.29854144E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.29854144E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.29854144E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.81021499E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.81021499E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.27007175E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.27007175E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.15360003E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.15360003E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.66602423E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.71628023E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.92220514E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.22728119E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.02084863E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     8.33335172E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.11646233E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.70730476E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.11504798E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.51192748E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.86049305E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.85904151E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.49063263E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.93541139E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.93541139E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.93541139E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.59934857E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.66085794E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.10855648E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.64568245E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.06457229E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.06390165E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.56891785E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.12603447E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.12603447E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.12603447E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.77474816E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.77474816E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.61814921E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.61814921E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.91576824E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.91576824E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.91264343E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.91264343E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.11807470E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.11807470E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.57947347E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.10151674E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.07869021E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.29451043E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46898721E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46898721E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.22061556E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.71263547E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.88491720E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.19201444E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     1.63868654E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.25121639E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.03198233E-14    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.57441285E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07628122E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16908613E-01    2           5        -5   # BR(h -> b       bb     )
     6.38950647E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26164997E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79264126E-04    2           3        -3   # BR(h -> s       sb     )
     2.06767746E-02    2           4        -4   # BR(h -> c       cb     )
     6.71372008E-02    2          21        21   # BR(h -> g       g      )
     2.30417399E-03    2          22        22   # BR(h -> gam     gam    )
     1.54109198E-03    2          22        23   # BR(h -> Z       gam    )
     2.01155393E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56762590E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78125217E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48387636E-03    2           5        -5   # BR(H -> b       bb     )
     2.46429695E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71220719E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547045E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666657E-05    2           4        -4   # BR(H -> c       cb     )
     9.96053740E-01    2           6        -6   # BR(H -> t       tb     )
     7.97598019E-04    2          21        21   # BR(H -> g       g      )
     2.73584493E-06    2          22        22   # BR(H -> gam     gam    )
     1.16020538E-06    2          23        22   # BR(H -> Z       gam    )
     3.33687608E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66388823E-04    2          23        23   # BR(H -> Z       Z      )
     9.02329837E-04    2          25        25   # BR(H -> h       h      )
     8.62509821E-24    2          36        36   # BR(H -> A       A      )
     3.49390094E-11    2          23        36   # BR(H -> Z       A      )
     5.57598203E-12    2          24       -37   # BR(H -> W+      H-     )
     5.57598203E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82386774E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48665057E-03    2           5        -5   # BR(A -> b       bb     )
     2.43894848E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62256392E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676057E-06    2           3        -3   # BR(A -> s       sb     )
     9.96164750E-06    2           4        -4   # BR(A -> c       cb     )
     9.96984249E-01    2           6        -6   # BR(A -> t       tb     )
     9.43664532E-04    2          21        21   # BR(A -> g       g      )
     3.03486297E-06    2          22        22   # BR(A -> gam     gam    )
     1.35284178E-06    2          23        22   # BR(A -> Z       gam    )
     3.25192745E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74517486E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.39245767E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49236433E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81140825E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52118444E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45683182E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08729438E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403180E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33366764E-04    2          24        25   # BR(H+ -> W+      h      )
     5.63460686E-13    2          24        36   # BR(H+ -> W+      A      )

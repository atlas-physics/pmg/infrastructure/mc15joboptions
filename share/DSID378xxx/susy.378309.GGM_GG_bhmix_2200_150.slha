#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.36958918E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05368952E+01   # W+
        25     1.25000000E+02   # h
        35     2.00415734E+03   # H
        36     2.00000000E+03   # A
        37     2.00201823E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013333E+03   # ~d_L
   2000001     5.00002532E+03   # ~d_R
   1000002     4.99989198E+03   # ~u_L
   2000002     4.99994936E+03   # ~u_R
   1000003     5.00013333E+03   # ~s_L
   2000003     5.00002532E+03   # ~s_R
   1000004     4.99989198E+03   # ~c_L
   2000004     4.99994936E+03   # ~c_R
   1000005     4.99999892E+03   # ~b_1
   2000005     5.00016118E+03   # ~b_2
   1000006     5.00071450E+03   # ~t_1
   2000006     5.00375735E+03   # ~t_2
   1000011     5.00008270E+03   # ~e_L
   2000011     5.00007595E+03   # ~e_R
   1000012     4.99984134E+03   # ~nu_eL
   1000013     5.00008270E+03   # ~mu_L
   2000013     5.00007595E+03   # ~mu_R
   1000014     4.99984134E+03   # ~nu_muL
   1000015     5.00003952E+03   # ~tau_1
   2000015     5.00011976E+03   # ~tau_2
   1000016     4.99984134E+03   # ~nu_tauL
   1000021     2.20000000E+03   # ~g
   1000022     1.49120446E+02   # ~chi_10
   1000023    -1.56740347E+02   # ~chi_20
   1000025     2.42579432E+02   # ~chi_30
   1000035     3.00199939E+03   # ~chi_40
   1000024     1.51829673E+02   # ~chi_1+
   1000037     3.00199904E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.15264448E-02   # N_11
  1  2    -5.02701686E-03   # N_12
  1  3    -7.14404986E-01   # N_13
  1  4    -6.93702498E-01   # N_14
  2  1     1.10071510E-01   # N_21
  2  2    -2.44713829E-02   # N_22
  2  3     6.99502572E-01   # N_23
  2  4    -7.05678089E-01   # N_24
  3  1     9.89700464E-01   # N_31
  3  2     3.59612396E-03   # N_32
  3  3    -1.17235996E-02   # N_33
  3  4     1.42627546E-01   # N_34
  4  1     4.05507686E-04   # N_41
  4  2    -9.99681423E-01   # N_42
  4  3    -1.35729529E-02   # N_43
  4  4     2.12758572E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.91876970E-02   # U_11
  1  2     9.99815899E-01   # U_12
  2  1     9.99815899E-01   # U_21
  2  2     1.91876970E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.00860891E-02   # V_11
  1  2    -9.99547311E-01   # V_12
  2  1     9.99547311E-01   # V_21
  2  2     3.00860891E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13740225E-01   # cos(theta_t)
  1  2    -7.00410516E-01   # sin(theta_t)
  2  1     7.00410516E-01   # -sin(theta_t)
  2  2     7.13740225E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -4.08835544E-01   # cos(theta_b)
  1  2     9.12608075E-01   # sin(theta_b)
  2  1    -9.12608075E-01   # -sin(theta_b)
  2  2    -4.08835544E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -6.76714974E-01   # cos(theta_tau)
  1  2     7.36245098E-01   # sin(theta_tau)
  2  1    -7.36245098E-01   # -sin(theta_tau)
  2  2    -6.76714974E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90210943E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51672063E+02   # vev(Q)              
         4     3.99793037E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53144503E-01   # gprime(Q) DRbar
     2     6.29521107E-01   # g(Q) DRbar
     3     1.07892594E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02777545E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72576969E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79990502E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.36958918E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04103770E+06   # M^2_Hd              
        22    -5.41355087E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38093653E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.41513389E-03   # gluino decays
#          BR         NDA      ID1       ID2
     5.75611681E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.90921371E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.34250965E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     5.71389938E-05    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.00006056E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.83702041E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.66172226E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.82432866E-04    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.00477280E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.71389938E-05    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.00006056E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.83702041E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.66172226E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.82432866E-04    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.00477280E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.35693160E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.65527365E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.83700742E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.11763286E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.14772562E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     3.09625698E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     3.48474385E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.48474385E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.48474385E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.48474385E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.31670856E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.31670856E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.95792445E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.12810684E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.13818931E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.48615590E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.32485608E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.32001982E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.64441548E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     5.66814128E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.01289332E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.20709739E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.27900797E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.20531317E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.30334303E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.10949808E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.59967385E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.89356355E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.77315643E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.54148758E-04    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
    -2.55490061E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.15951288E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.46106452E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     3.01007121E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.93633887E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     6.21660674E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.67349112E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
    -2.53925904E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.14191381E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.39285639E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.86949804E-03    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -6.65597090E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.17232644E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     1.03415039E+00    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.96837833E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.40459622E-05    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.45369714E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.41260662E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.96475942E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.74023257E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.92859622E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.77464314E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.83280963E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.83862229E-04    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.99693423E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.64269143E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.92036767E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.42389526E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.96842695E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.71303772E-05    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.95107009E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.15745005E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.96648441E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     7.07835241E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.93390166E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.77525668E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.75366970E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.26426472E-04    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.82820163E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.47435692E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.02437207E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.84947183E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.96837833E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.40459622E-05    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.45369714E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.41260662E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.96475942E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.74023257E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.92859622E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.77464314E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.83280963E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.83862229E-04    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.99693423E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.64269143E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.92036767E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.42389526E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.96842695E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.71303772E-05    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.95107009E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.15745005E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.96648441E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     7.07835241E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.93390166E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.77525668E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.75366970E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.26426472E-04    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.82820163E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.47435692E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.02437207E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.84947183E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.03797896E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.38937243E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.99731501E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.01635086E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.65071586E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.76856388E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.30527368E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46956200E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.40121278E-03    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.21483585E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.79450361E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.75683020E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.03797896E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.38937243E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.99731501E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.01635086E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.65071586E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.76856388E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.30527368E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46956200E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.40121278E-03    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.21483585E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.79450361E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.75683020E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.73319353E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.59056058E-03    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     5.23481181E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.82509300E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.35081692E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     9.35700487E-05    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.70358427E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.31638586E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.78435600E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.67719390E-03    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.74846504E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.16911712E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.56617323E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.58328949E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.13462016E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.03780358E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.05804230E-03    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     4.81354156E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.96479739E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.65286600E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.17240431E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.30189673E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.03780358E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.05804230E-03    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     4.81354156E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.96479739E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.65286600E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.17240431E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.30189673E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.04101971E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.05586574E-03    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.80845083E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.96271945E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.65006037E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.22852851E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.29629173E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.16454247E-10   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.11949071E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     4.55454532E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     1.14814983E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.51818310E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.50673497E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.52896069E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.94913487E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.80527276E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.72354913E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.73418296E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.62641002E-03    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.80827201E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     7.14436968E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     7.17164770E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     7.15701180E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     7.06806224E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     8.25500269E-04    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     9.48887006E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     2.41279786E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.75853544E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     2.29414210E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     2.94732246E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.46397709E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.21799524E-05    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     4.20011537E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.55253048E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.09779911E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.42080930E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.64541899E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.41358221E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     3.24181902E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.23863811E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.24141689E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.47198816E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.47198816E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.47198816E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.25086345E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.25086345E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.85422356E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.85422356E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.41695483E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.41695483E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.41368427E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.41368427E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.57016281E-03    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.57016281E-03    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.49123700E-02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.06826290E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.96586843E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.96586843E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     2.07218947E-08    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.60341963E-09    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     5.05332565E-11    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     2.94917024E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.69760478E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.63587413E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.31863850E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.76001521E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.76001521E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.90991661E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.91876532E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.25308255E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     1.69650044E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.76463583E-03    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     6.89417117E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.47937088E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.80787065E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.47686643E-04    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     7.16287946E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     7.16287946E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.27347931E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.21457560E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.84259602E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     2.40086540E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.17647038E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.08169745E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17291939E-01    2           5        -5   # BR(h -> b       bb     )
     6.38125965E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25873090E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78645222E-04    2           3        -3   # BR(h -> s       sb     )
     2.06490009E-02    2           4        -4   # BR(h -> c       cb     )
     6.70486407E-02    2          21        21   # BR(h -> g       g      )
     2.27850440E-03    2          22        22   # BR(h -> gam     gam    )
     1.53877455E-03    2          22        23   # BR(h -> Z       gam    )
     2.01033839E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56421863E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.00916981E+01   # H decays
#          BR         NDA      ID1       ID2
     1.39265677E-03    2           5        -5   # BR(H -> b       bb     )
     2.32417972E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21684059E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05204483E-06    2           3        -3   # BR(H -> s       sb     )
     9.49477789E-06    2           4        -4   # BR(H -> c       cb     )
     9.39468185E-01    2           6        -6   # BR(H -> t       tb     )
     7.52322305E-04    2          21        21   # BR(H -> g       g      )
     2.50780345E-06    2          22        22   # BR(H -> gam     gam    )
     1.09288318E-06    2          23        22   # BR(H -> Z       gam    )
     3.18220113E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58675982E-04    2          23        23   # BR(H -> Z       Z      )
     8.53910100E-04    2          25        25   # BR(H -> h       h      )
     8.38005731E-24    2          36        36   # BR(H -> A       A      )
     3.34203553E-11    2          23        36   # BR(H -> Z       A      )
     2.93750046E-12    2          24       -37   # BR(H -> W+      H-     )
     2.93750046E-12    2         -24        37   # BR(H -> W-      H+     )
     8.42635454E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13584746E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.00111659E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.13567201E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.06389092E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.16563198E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.25464170E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05320841E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39574100E-03    2           5        -5   # BR(A -> b       bb     )
     2.30094667E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.13467769E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07243981E-06    2           3        -3   # BR(A -> s       sb     )
     9.39799257E-06    2           4        -4   # BR(A -> c       cb     )
     9.40572386E-01    2           6        -6   # BR(A -> t       tb     )
     8.90269633E-04    2          21        21   # BR(A -> g       g      )
     3.07014546E-06    2          22        22   # BR(A -> gam     gam    )
     1.27471721E-06    2          23        22   # BR(A -> Z       gam    )
     3.10200791E-04    2          23        25   # BR(A -> Z       h      )
     5.28252777E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.24988148E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.70346954E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.76901242E-04    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.45835572E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     2.67656205E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.95021587E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97308522E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.24035144E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34971744E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30709995E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42440833E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14442185E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02504546E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.42179031E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17867840E-04    2          24        25   # BR(H+ -> W+      h      )
     1.10811208E-12    2          24        36   # BR(H+ -> W+      A      )
     4.25428342E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     6.78246215E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.61510674E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

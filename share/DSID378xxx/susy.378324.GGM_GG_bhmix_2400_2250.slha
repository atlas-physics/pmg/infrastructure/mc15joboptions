#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.25516232E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -2.25000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05426287E+01   # W+
        25     1.25000000E+02   # h
        35     2.00414280E+03   # H
        36     2.00000000E+03   # A
        37     2.00170765E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013257E+03   # ~d_L
   2000001     5.00002533E+03   # ~d_R
   1000002     4.99989276E+03   # ~u_L
   2000002     4.99994934E+03   # ~u_R
   1000003     5.00013257E+03   # ~s_L
   2000003     5.00002533E+03   # ~s_R
   1000004     4.99989276E+03   # ~c_L
   2000004     4.99994934E+03   # ~c_R
   1000005     4.99916616E+03   # ~b_1
   2000005     5.00099303E+03   # ~b_2
   1000006     4.97934693E+03   # ~t_1
   2000006     5.02503197E+03   # ~t_2
   1000011     5.00008190E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984210E+03   # ~nu_eL
   1000013     5.00008190E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984210E+03   # ~nu_muL
   1000015     4.99948011E+03   # ~tau_1
   2000015     5.00067835E+03   # ~tau_2
   1000016     4.99984210E+03   # ~nu_tauL
   1000021     2.40000000E+03   # ~g
   1000022     2.24344528E+03   # ~chi_10
   1000023    -2.25156316E+03   # ~chi_20
   1000025     2.26181858E+03   # ~chi_30
   1000035     3.00146162E+03   # ~chi_40
   1000024     2.25082207E+03   # ~chi_1+
   1000037     3.00146127E+03   # ~chi_2+
   1000039     4.11107394E-07   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.82542866E-01   # N_11
  1  2    -1.60265357E-02   # N_12
  1  3    -5.78800356E-01   # N_13
  1  4    -5.70418361E-01   # N_14
  2  1     9.67029133E-03   # N_21
  2  2    -1.47416587E-02   # N_22
  2  3     7.06947775E-01   # N_23
  2  4    -7.07045976E-01   # N_24
  3  1     8.12741706E-01   # N_31
  3  2     1.30182935E-02   # N_32
  3  3     4.06445313E-01   # N_33
  3  4     4.17233330E-01   # N_34
  4  1     1.10216465E-03   # N_41
  4  2    -9.99678127E-01   # N_42
  4  3     4.14714093E-03   # N_43
  4  4     2.50045749E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.89176375E-03   # U_11
  1  2     9.99982643E-01   # U_12
  2  1     9.99982643E-01   # U_21
  2  2    -5.89176375E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     3.53721386E-02   # V_11
  1  2    -9.99374210E-01   # V_12
  2  1     9.99374210E-01   # V_21
  2  2     3.53721386E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07544261E-01   # cos(theta_t)
  1  2    -7.06669031E-01   # sin(theta_t)
  2  1     7.06669031E-01   # -sin(theta_t)
  2  2     7.07544261E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.86040052E-01   # cos(theta_b)
  1  2     7.27563775E-01   # sin(theta_b)
  2  1    -7.27563775E-01   # -sin(theta_b)
  2  2    -6.86040052E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05361228E-01   # cos(theta_tau)
  1  2     7.08848036E-01   # sin(theta_tau)
  2  1    -7.08848036E-01   # -sin(theta_tau)
  2  2    -7.05361228E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90196905E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -2.25000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52078627E+02   # vev(Q)              
         4     4.98462166E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52673456E-01   # gprime(Q) DRbar
     2     6.26479778E-01   # g(Q) DRbar
     3     1.07753924E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02724001E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73262889E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79540448E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.25516232E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -2.61374288E+06   # M^2_Hd              
        22    -1.00637197E+07   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36738197E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     3.49948289E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.69726763E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.31434509E-01    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.44049891E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.84487845E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.35397320E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.68238457E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     2.17705539E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     8.36143723E-03    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.44049891E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.84487845E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.35397320E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.68238457E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     2.17705539E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     8.36143723E-03    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.62720443E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.11808404E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.29260994E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.02840960E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     3.02840960E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.02840960E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     3.02840960E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.34172022E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.98663683E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.24143971E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.36634423E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.79941312E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.20454822E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     3.58580133E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.48158166E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     2.50772489E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.17781607E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.69169225E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.37464940E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.41977319E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.77330079E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.82711374E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.57356546E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.92223712E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.75177748E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.64854072E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.04196169E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.90619727E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.13275126E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.82764735E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.23546203E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     1.95996855E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.30729974E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.37686922E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.91215911E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.07713650E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.22908339E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.17083073E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.09254843E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.82089604E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.74332505E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.09886775E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.79342461E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.24391931E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.64605335E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.48619173E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.70155539E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.65184085E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.39096559E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.81910643E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.68523316E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.20524458E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.59234161E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.82096867E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.03687533E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.80258897E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.27367895E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.24792285E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     4.56705939E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     8.49750822E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.70212542E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60139028E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.58707689E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.84886350E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.92478724E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.26596505E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89487143E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.82089604E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.74332505E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.09886775E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.79342461E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.24391931E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.64605335E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.48619173E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.70155539E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.65184085E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.39096559E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.81910643E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.68523316E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.20524458E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.59234161E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.82096867E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.03687533E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.80258897E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.27367895E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.24792285E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     4.56705939E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     8.49750822E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.70212542E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60139028E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.58707689E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.84886350E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.92478724E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.26596505E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89487143E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.78934620E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.34316479E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.84514932E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     9.80251114E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.85804938E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.08899363E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.72668962E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56982825E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.41221328E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.36851500E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.58684203E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.83468612E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.78934620E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.34316479E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.84514932E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     9.80251114E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.85804938E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.08899363E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.72668962E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56982825E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.41221328E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.36851500E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.58684203E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.83468612E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.17892492E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.56030100E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.96410766E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.96713293E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.81901787E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     6.81948655E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.64476459E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     9.76467491E-10    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.18636638E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.45538448E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     8.52700166E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.02601591E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.83345044E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     2.92084606E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.67370134E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.78914678E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.28153087E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.81225341E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.74750061E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.86491146E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.11336624E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.71923947E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.78914678E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.28153087E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.81225341E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.74750061E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.86491146E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.11336624E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.71923947E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.79118586E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.27767248E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.81092948E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.74111017E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.86281852E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.84356243E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.71505666E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.48769656E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.31666616E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     2.52763995E-05    2     1000039        37   # BR(~chi_1+ -> ~G       H+)
#           BR         NDA      ID1       ID2       ID3
     3.58070802E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     2.99786596E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.19357530E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.19235281E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.03578532E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     1.75831520E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.54087176E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49251710E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47410722E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.99658627E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.49284529E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.40916966E-10    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.83417446E-14    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     3.76025159E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.51994327E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.71096306E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
     3.34472269E-05    2     1000039        35   # BR(~chi_10 -> ~G        H)
     8.50761313E-04    2     1000039        36   # BR(~chi_10 -> ~G        A)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.78048939E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.11340021E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.04137295E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.05120707E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
     1.95114077E-05    2     1000039        35   # BR(~chi_20 -> ~G        H)
     8.29533774E-07    2     1000039        36   # BR(~chi_20 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     1.41234413E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.82898011E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.14824938E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.82079786E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.17807252E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.17446766E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.04669276E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.34453016E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.34453016E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.34453016E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.55741354E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.55741354E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.51913800E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.51913800E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.37601245E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.37601245E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.23422149E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.39647851E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     2.42663942E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.43301820E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.70179395E-05    2     1000039        25   # BR(~chi_30 -> ~G        h)
     5.63539266E-09    2     1000039        35   # BR(~chi_30 -> ~G        H)
     1.74470384E-07    2     1000039        36   # BR(~chi_30 -> ~G        A)
#           BR         NDA      ID1       ID2       ID3
     6.07147797E-06    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     8.22864665E-06    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     5.64776964E-06    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     8.21882745E-06    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.43955128E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.19061903E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.18953918E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.92323788E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.59691402E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.59691402E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.59691402E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     4.98203474E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.45174993E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     4.39555860E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.43362137E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     5.78772383E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.47382517E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.47302225E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.22690137E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     2.94348365E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     2.94348365E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     2.94348365E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     2.38355155E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     2.38355155E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     2.19853440E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     2.19853440E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     7.94508791E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     7.94508791E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     7.94142309E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     7.94142309E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     6.99934842E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     6.99934842E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     1.75835543E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.87101969E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.65197070E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.00000475E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.48176781E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.48176781E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.36761467E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.38432643E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.22197110E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.26061784E-10    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.14547549E-10    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.49663845E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.87757114E-15    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.35268086E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07335256E-03   # h decays
#          BR         NDA      ID1       ID2
     6.16656848E-01    2           5        -5   # BR(h -> b       bb     )
     6.39406464E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26326339E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.79606076E-04    2           3        -3   # BR(h -> s       sb     )
     2.06916928E-02    2           4        -4   # BR(h -> c       cb     )
     6.71840911E-02    2          21        21   # BR(h -> g       g      )
     2.30640631E-03    2          22        22   # BR(h -> gam     gam    )
     1.54224234E-03    2          22        23   # BR(h -> Z       gam    )
     2.01277421E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56947199E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78122989E+01   # H decays
#          BR         NDA      ID1       ID2
     1.48836531E-03    2           5        -5   # BR(H -> b       bb     )
     2.46431428E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71226844E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547855E-06    2           3        -3   # BR(H -> s       sb     )
     1.00666568E-05    2           4        -4   # BR(H -> c       cb     )
     9.96052786E-01    2           6        -6   # BR(H -> t       tb     )
     7.97561998E-04    2          21        21   # BR(H -> g       g      )
     2.73232426E-06    2          22        22   # BR(H -> gam     gam    )
     1.16043609E-06    2          23        22   # BR(H -> Z       gam    )
     3.33119187E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66105418E-04    2          23        23   # BR(H -> Z       Z      )
     8.99683994E-04    2          25        25   # BR(H -> h       h      )
     8.55686030E-24    2          36        36   # BR(H -> A       A      )
     3.48240814E-11    2          23        36   # BR(H -> Z       A      )
     5.95412180E-12    2          24       -37   # BR(H -> W+      H-     )
     5.95412180E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82388266E+01   # A decays
#          BR         NDA      ID1       ID2
     1.49108580E-03    2           5        -5   # BR(A -> b       bb     )
     2.43893896E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62253027E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13675613E-06    2           3        -3   # BR(A -> s       sb     )
     9.96160862E-06    2           4        -4   # BR(A -> c       cb     )
     9.96980358E-01    2           6        -6   # BR(A -> t       tb     )
     9.43660850E-04    2          21        21   # BR(A -> g       g      )
     3.05052223E-06    2          22        22   # BR(A -> gam     gam    )
     1.35309610E-06    2          23        22   # BR(A -> Z       gam    )
     3.24637020E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74513858E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.40222199E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49234552E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81134175E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.52743364E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45680337E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08728872E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99403755E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.32784163E-04    2          24        25   # BR(H+ -> W+      h      )
     5.09848480E-13    2          24        36   # BR(H+ -> W+      A      )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5a                    |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5a /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.15378564E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23    -1.15000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05409827E+01   # W+
        25     1.25000000E+02   # h
        35     2.00413858E+03   # H
        36     2.00000000E+03   # A
        37     2.00178746E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00013264E+03   # ~d_L
   2000001     5.00002531E+03   # ~d_R
   1000002     4.99989267E+03   # ~u_L
   2000002     4.99994939E+03   # ~u_R
   1000003     5.00013264E+03   # ~s_L
   2000003     5.00002531E+03   # ~s_R
   1000004     4.99989267E+03   # ~c_L
   2000004     4.99994939E+03   # ~c_R
   1000005     4.99961221E+03   # ~b_1
   2000005     5.00054714E+03   # ~b_2
   1000006     4.99056741E+03   # ~t_1
   2000006     5.01386962E+03   # ~t_2
   1000011     5.00008203E+03   # ~e_L
   2000011     5.00007592E+03   # ~e_R
   1000012     4.99984205E+03   # ~nu_eL
   1000013     5.00008203E+03   # ~mu_L
   2000013     5.00007592E+03   # ~mu_R
   1000014     4.99984205E+03   # ~nu_muL
   1000015     4.99977293E+03   # ~tau_1
   2000015     5.00038562E+03   # ~tau_2
   1000016     4.99984205E+03   # ~nu_tauL
   1000021     1.20000000E+03   # ~g
   1000022     1.14322364E+03   # ~chi_10
   1000023    -1.15226791E+03   # ~chi_20
   1000025     1.16125483E+03   # ~chi_30
   1000035     3.00157509E+03   # ~chi_40
   1000024     1.15131540E+03   # ~chi_1+
   1000037     3.00157481E+03   # ~chi_2+
   1000039     5.99356112E-08   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     6.07040155E-01   # N_11
  1  2    -6.13927101E-03   # N_12
  1  3    -5.70140308E-01   # N_13
  1  4    -5.53538245E-01   # N_14
  2  1     1.88856137E-02   # N_21
  2  2    -1.86478163E-02   # N_22
  2  3     7.06718416E-01   # N_23
  2  4    -7.06996940E-01   # N_24
  3  1     7.94446571E-01   # N_31
  3  2     5.73758467E-03   # N_32
  3  3     4.18850997E-01   # N_33
  3  4     4.39756261E-01   # N_34
  4  1     4.79345258E-04   # N_41
  4  2    -9.99790802E-01   # N_42
  4  3    -7.27683857E-03   # N_43
  4  4     1.91094068E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.02822802E-02   # U_11
  1  2     9.99947136E-01   # U_12
  2  1     9.99947136E-01   # U_21
  2  2     1.02822802E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     2.70246193E-02   # V_11
  1  2    -9.99634768E-01   # V_12
  2  1     9.99634768E-01   # V_21
  2  2     2.70246193E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.07966495E-01   # cos(theta_t)
  1  2    -7.06246021E-01   # sin(theta_t)
  2  1     7.06246021E-01   # -sin(theta_t)
  2  2     7.07966495E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1    -6.65281598E-01   # cos(theta_b)
  1  2     7.46592523E-01   # sin(theta_b)
  2  1    -7.46592523E-01   # -sin(theta_b)
  2  2    -6.65281598E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.03571320E-01   # cos(theta_tau)
  1  2     7.10624653E-01   # sin(theta_tau)
  2  1    -7.10624653E-01   # -sin(theta_tau)
  2  2    -7.03571320E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90191073E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1    -1.15000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51862423E+02   # vev(Q)              
         4     4.37295952E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52792777E-01   # gprime(Q) DRbar
     2     6.27238260E-01   # g(Q) DRbar
     3     1.08870452E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02601547E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72508914E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79763314E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.15378564E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.57251894E+06   # M^2_Hd              
        22    -6.72757248E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37076509E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.0E-05   # gluino decays
#          BR         NDA      ID1       ID2
     4.10932051E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.03077933E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
     8.46536370E-02    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000039        21   # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     3.71369966E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.31603634E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     9.29999395E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.22776342E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.41228630E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.22561640E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     3.71369966E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.31603634E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     9.29999395E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.22776342E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.41228630E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     3.22561640E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.99852902E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.56836924E-06    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.42087093E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     9.79150103E-07    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     9.79150103E-07    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     9.79150103E-07    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     9.79150103E-07    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.38601062E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.54862136E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.21832542E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.61477294E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.22780287E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.17508323E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.45214830E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.67632459E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     3.47988867E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.05543602E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.68864755E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.64723866E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.03948049E-02    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.76688713E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.07517744E-02    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.67271327E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     2.70720366E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.90249407E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.22574503E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.05221381E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.28975622E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.08585830E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.58365480E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.46693094E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.79108401E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.32222763E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.65668335E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.72480585E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.52567644E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.27269886E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.05624728E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.21698175E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.53136834E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.78198212E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52927394E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.60048954E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.05999351E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     9.78939076E-05    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.11917823E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.05716408E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.38808801E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.47464154E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.42478998E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.51682642E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.19420748E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60071068E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.53139541E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.65851541E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.29917835E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.37190090E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.06153696E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.41721678E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.12388925E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.05760822E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.31661643E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.80041249E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.67193639E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.48630828E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.08095464E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89709606E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.53136834E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.78198212E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52927394E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.60048954E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.05999351E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     9.78939076E-05    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.11917823E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.05716408E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.38808801E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.47464154E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.42478998E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.51682642E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.19420748E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60071068E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.53139541E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.65851541E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.29917835E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.37190090E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.06153696E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.41721678E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.12388925E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.05760822E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.31661643E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.80041249E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.67193639E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.48630828E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.08095464E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89709606E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.95760459E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.68058255E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.82076882E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.21290404E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.70426309E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.25456546E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.41313797E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.21910146E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.69314868E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.56831138E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.30328196E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.04895608E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.95760459E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.68058255E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.82076882E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.21290404E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.70426309E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.25456546E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.41313797E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.21910146E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.69314868E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.56831138E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.30328196E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.04895608E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58767652E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.03509379E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.63980632E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.36646420E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.53061977E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.33824888E-04    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.06384380E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     3.86952760E-08    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.59767810E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.89552144E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.29353757E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.41599575E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.55423209E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.01955741E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.11111977E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.95741787E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.17886454E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.08223243E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.15218189E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.70692982E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     8.66633621E-04    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.40925328E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.95741787E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.17886454E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.08223243E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.15218189E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.70692982E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     8.66633621E-04    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.40925328E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.96030076E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.17187340E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.07728309E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.15105983E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.70429367E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     1.83971500E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.40398473E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.29005629E-08   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.38945704E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.53043883E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.04473110E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17681967E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17581769E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     9.33247005E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.59283753E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53144483E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.49122611E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46105010E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     9.84759429E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53151944E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.06061422E-09    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.28637588E-13    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.16684091E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     1.49970524E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.33345385E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.0E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.34037909E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.16371557E-06    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.01930287E-02    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     3.90297697E-04    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.39731004E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.80924649E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18620287E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.80271621E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.13174534E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     4.12886385E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.23519250E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.25125420E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.25125420E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.25125420E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     9.69178748E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     9.69178748E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.23059604E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.23059604E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.04154431E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.04154431E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
#
#         PDG            Width
DECAY   1000025     1.0E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.88770109E-01    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     9.96256053E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.56659811E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.12819477E-04    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.26117671E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.45976239E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.93493796E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.34661240E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.93243100E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.73494053E-06    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     4.78348890E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     4.78104005E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.15038937E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     8.63758524E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     8.63758524E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     8.63758524E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.58209935E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     3.34334902E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.18701504E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     3.33112965E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     7.63525789E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     7.62986611E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.95698146E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.52473197E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.52473197E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.52473197E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.44182063E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.44182063E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.30638700E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.30638700E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.80601924E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.80601924E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.80330620E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.80330620E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.11741531E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.11741531E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     8.59307671E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.32492930E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.21846830E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.88296598E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46454025E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46454025E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.04397682E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.10981578E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.21633221E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.17917856E-09    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.88023065E-09    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.31133413E-13    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.19239225E-13    2     1000039        35   # BR(~chi_40 -> ~G        H)
     8.97172657E-15    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.07912014E-03   # h decays
#          BR         NDA      ID1       ID2
     6.17150070E-01    2           5        -5   # BR(h -> b       bb     )
     6.38491271E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26002395E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78919764E-04    2           3        -3   # BR(h -> s       sb     )
     2.06625976E-02    2           4        -4   # BR(h -> c       cb     )
     6.70922348E-02    2          21        21   # BR(h -> g       g      )
     2.30162559E-03    2          22        22   # BR(h -> gam     gam    )
     1.53997013E-03    2          22        23   # BR(h -> Z       gam    )
     2.01041063E-01    2          24       -24   # BR(h -> W+      W-     )
     2.56583901E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78112997E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47957850E-03    2           5        -5   # BR(H -> b       bb     )
     2.46439346E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71254838E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11551489E-06    2           3        -3   # BR(H -> s       sb     )
     1.00667292E-05    2           4        -4   # BR(H -> c       cb     )
     9.96059838E-01    2           6        -6   # BR(H -> t       tb     )
     7.97626885E-04    2          21        21   # BR(H -> g       g      )
     2.74318548E-06    2          22        22   # BR(H -> gam     gam    )
     1.15999048E-06    2          23        22   # BR(H -> Z       gam    )
     3.31357547E-04    2          24       -24   # BR(H -> W+      W-     )
     1.65226932E-04    2          23        23   # BR(H -> Z       Z      )
     9.03975582E-04    2          25        25   # BR(H -> h       h      )
     8.63683505E-24    2          36        36   # BR(H -> A       A      )
     3.46467826E-11    2          23        36   # BR(H -> Z       A      )
     4.99558820E-12    2          24       -37   # BR(H -> W+      H-     )
     4.99558820E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82384236E+01   # A decays
#          BR         NDA      ID1       ID2
     1.48234020E-03    2           5        -5   # BR(A -> b       bb     )
     2.43896466E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62262114E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13676811E-06    2           3        -3   # BR(A -> s       sb     )
     9.96171361E-06    2           4        -4   # BR(A -> c       cb     )
     9.96990865E-01    2           6        -6   # BR(A -> t       tb     )
     9.43670795E-04    2          21        21   # BR(A -> g       g      )
     2.99695403E-06    2          22        22   # BR(A -> gam     gam    )
     1.35255566E-06    2          23        22   # BR(A -> Z       gam    )
     3.22917032E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74521866E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.38292738E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49239160E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81150466E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.51508499E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45687481E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08730294E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99405503E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.31050546E-04    2          24        25   # BR(H+ -> W+      h      )
     6.40625566E-13    2          24        36   # BR(H+ -> W+      A      )

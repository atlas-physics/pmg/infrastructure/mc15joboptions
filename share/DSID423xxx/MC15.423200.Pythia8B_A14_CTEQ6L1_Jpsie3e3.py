
evgenConfig.description = "Inclusive pp->J/psi(e3e3) production with Photos"
evgenConfig.keywords = ["egamma","charmonium","2electron","inclusive"]
evgenConfig.process  = "Jpsi -> ee"
evgenConfig.contact  = [ "ocariz@in2p3.fr" ]
evgenConfig.minevents = 500

# Pythia8B config with Photos++  
include('MC15JobOptions/nonStandard/Pythia8B_A14_CTEQ6L1_Common.py')
include('MC15JobOptions/nonStandard/Pythia8B_Photospp.py')
include("MC15JobOptions/Pythia8B_Charmonium_Common.py")

# genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 4.'] not used in MC12 egamma J/Psi jobOptions

# Close all J/psi decays apart from J/psi->ee
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:1:onMode = on']
genSeq.Pythia8B.SignalPDGCodes = [443,-11,11]

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 11
genSeq.Pythia8B.TriggerStatePtCut = [3.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2]

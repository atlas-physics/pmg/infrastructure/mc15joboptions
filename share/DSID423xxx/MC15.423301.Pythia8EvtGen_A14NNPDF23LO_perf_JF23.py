evgenConfig.description = "Pythia8 e/gamma performance sample (jets, gamma+jet, W/Z, ttbar), with EM jet pT > 23 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "QCD"]
evgenConfig.process  = ""
evgenConfig.generators =  ["Pythia8"]
evgenConfig.contact  = [ "ocariz@in2p3.fr" ]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "WeakSingleBoson:all = on",
                            "Top:gg2ttbar = on",
                            "Top:qqbar2ttbar = on",
                            "PhaseSpace:pTHatMin = 21",		# PtHat and mHat thresholds interpolated from DC14 numbers
                            "PhaseSpace:mHatMin = 42"]		# to be used for performance studies only

include("MC15JobOptions/JetFilter.py")

filtSeq.JetFilter.JetThreshold = 23000.

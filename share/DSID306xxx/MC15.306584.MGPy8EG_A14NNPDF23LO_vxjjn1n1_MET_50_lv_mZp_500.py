model="LightVector"
mDM1 = 5.
mDM2 = 530.
mZp = 500.
mHD = 125.
widthZp = 2.346527e+00
widthN2 = 1.281024e-01
filteff = 7.496252e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

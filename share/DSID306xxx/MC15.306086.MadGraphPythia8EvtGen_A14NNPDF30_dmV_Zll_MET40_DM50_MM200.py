Mchi =  50.
Mxi =  200.
gxichi = 1.0 
gxiU = 0.25 
Wxi = 10.1422986111
evgenConfig.description = "Wimp pair monoZ with dmV"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kenji Hamano <kenji.hamano@cern.ch>"]
include("MC15JobOptions/MadGraphControl_Pythia8EvtGen_A14NNPDF30_dmV_Zll_MET40.py")

model="LightVector"
mDM1 = 5.
mDM2 = 210.
mZp = 180.
mHD = 125.
widthZp = 7.161968e-01
widthN2 = 8.173004e-01
filteff = 5.347594e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

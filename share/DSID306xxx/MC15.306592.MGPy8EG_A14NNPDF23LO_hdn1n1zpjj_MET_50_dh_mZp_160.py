model="darkHiggs"
mDM1 = 5.
mDM2 = 5.
mZp = 160.
mHD = 125.
widthZp = 6.366192e-01
widthhd = 8.122615e-02
filteff = 7.485030e-01

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

model="InelasticVectorEFT"
mDM1 = 40.
mDM2 = 160.
mZp = 80.
mHD = 125.
widthZp = 3.183053e-01
widthN2 = 1.889196e-03
filteff = 6.150062e-01

evgenConfig.description = "Mono Z' sample - model Light Vector w/ Inelastic EFT"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

model="LightVector"
mDM1 = 5.
mDM2 = 330.
mZp = 300.
mHD = 125.
widthZp = 1.193662e+00
widthN2 = 5.233764e-01
filteff = 6.605020e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Kevin Bauer <kevin.thomas.bauer@cern.ch>"]

include("MC15JobOptions/MadGraphControl_MGPy8EG_mono_zp.py")

include("MC15JobOptions/MadGraphControl_HVT.py")
evgenConfig.description = "heavyVectorTriplet Spin-1 Wgamma 4 TeV resonance in fully leptonic decay model"
evgenConfig.keywords = ["exotic", "Wgamma", "heavyVectorTriplet", "leptonic", "spin1"]

# JO for Pythia 8 jet jet JZ0 slice with muon filter

evgenConfig.description = "Dijet truth jet slice JZ0, R04, with the A14 NNPDF23 LO tune with muon filter"
evgenConfig.keywords = ["QCD", "jets", "1muon", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:hardccbar = on"]
genSeq.Pythia8.Commands += ["HardQCD:hardbbbar = on"]
genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = 1."]

include("MC15JobOptions/JetFilter_JZ0R04.py")
include("MC15JobOptions/LowPtMuonFilter.py")

evgenConfig.minevents = 100

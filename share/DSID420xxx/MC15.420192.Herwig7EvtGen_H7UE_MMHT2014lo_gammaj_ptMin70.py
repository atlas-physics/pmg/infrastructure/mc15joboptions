## Job options file for Herwig 7 for combined photon+jet and QCD 2->2 production
include("MC15JobOptions/Herwig7_701_H7UE_MMHT2014lo68cl_EvtGen_Common.py")

evgenConfig.description = "Photon+jet production with MMHT2014lo68cl PDF and H7UE tune."
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.keywords    = ["photon", "jets", "QCD"]
evgenConfig.contact     = [ "daniel.rauch@desy.de" ]

## see http://herwig.hepforge.org/tutorials/hardprocess/builtin.html for list of builtin MEs
cmds = """
## hard process setup
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[1] /Herwig/MatrixElements/MEGammaJet
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
"""
genSeq.Herwig7.Commands += cmds.splitlines()

## http://herwig.hepforge.org/tutorials/hardprocess/matchbox.html#cuts
## - I guess JetKtCut is fine, since process is LO anyway (no IR safe parton-level cut needed)
## - Is it fine to use the PhotonKtCut?
##   - This comes with a default photon eta range of [-3, +3] -> is this fine? Study bias?
## - Assuming no photon isolation is needed/wanted
genSeq.Herwig7.Commands += """
## cut on photon pT
set /Herwig/Cuts/PhotonKtCut:MinKT 35.0*GeV

## increase pT of jet cut to match photon cut
set /Herwig/Cuts/JetKtCut:MinKT 35.0*GeV
""".splitlines()

include("MC15JobOptions/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.Ptmin = 70000.
filtSeq.DirectPhotonFilter.Ptmax = 140000.

#--------------------------------------------------------------
# Powheg W setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_W_Common.py')
PowhegConfig.idvecbos   = -24 # W-
PowhegConfig.beam_1_type = 1  # p
PowhegConfig.beam_2_type = 1  # p
PowhegConfig.vdecaymode = 3  # taunu

# Configure Powheg setup
PowhegConfig.withdamp   = 1
PowhegConfig.ptsqmin    = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 3.0 # increase number of generated events by factor 3
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

# force leptonic tau decays
genSeq.Pythia8.Commands += [ '15:onMode = off' ]
genSeq.Pythia8.Commands += [ '15:onIfAny = 11 12' ]

include("MC15JobOptions/LeptonFilter.py")
filtSeq.LeptonFilter.Ptcut = 10000.
filtSeq.LeptonFilter.Etacut = 2.7

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 pp-> Wminus->taunu production with electron filter and AZNLO CT10 tune'
evgenConfig.contact = ["Andrzej Olszewski <Andrzej.Olszewski@ifj.edu.pl>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'W', 'drellYan', 'tau', 'neutrino' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.minevents = 1000

# JO for Pythia 8 jet jet JZ2 slice with eta < 3.0

evgenConfig.description = "Dijet truth jet slice JZ2, R04, with the A14 NNPDF23 LO tune, eta* > 3.0"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 15."]

include("MC15JobOptions/JetFilter_JZ2R04.py")
include("MC15JobOptions/JetFilterFragment_MaxEta_3p0.py")

evgenConfig.minevents = 200

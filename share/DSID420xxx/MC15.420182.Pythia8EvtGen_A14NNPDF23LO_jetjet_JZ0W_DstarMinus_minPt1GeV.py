# JO for Pythia 8 jet jet JZ0W slice with Dstar+ filter

evgenConfig.description = "Dijet truth jet slice JZ0W, with the A14 NNPDF23 LO tune and DstarPlusFilter"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.process = "pp to jetjet"

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_JZ0W.py")
include("MC15JobOptions/DstarMinusFilter_minPt1GeV.py")

genSeq.EvtInclusiveDecay.userDecayFile = "DstarM2D0PiM_D02Kpi.DEC"
evgenConfig.auxfiles += [ 'DstarM2D0PiM_D02Kpi.DEC']

evgenConfig.minevents = 5000

include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "gamma + 1,2,3 jets with 70<pT_y<140"
evgenConfig.keywords = ["SM", "photon"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "heberth.torres@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "SinglePhotonPt70_140"

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{PPerp2(p[2])};
  INTEGRATION_ERROR=0.99;

  % tags for process setup
  NJET:=2; QCUT:=10;
}(run)

(processes){
  Process 93 93 -> 22 93 93{NJET};
  Order (*,1);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  End process;
}(processes)

(selector){
  PT  22  70  140
  IsolationCut  22  0.3  2  0.025;
}(selector)
"""

genSeq.Sherpa_i.NCores = 48

include("MC15JobOptions/DirectPhotonFilter.py")

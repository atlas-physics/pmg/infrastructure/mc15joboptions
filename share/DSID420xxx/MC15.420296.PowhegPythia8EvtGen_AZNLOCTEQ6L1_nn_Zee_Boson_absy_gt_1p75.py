#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.beam_1_type = 2   # n
PowhegConfig.beam_2_type = 2   # n
PowhegConfig.vdecaymode = 1    # ee

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 10 # increase number of generated events by 10
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("ParentChildFilter")

filtSeq.ParentChildFilter.PDGParent = [23]  # Select Z
filtSeq.ParentChildFilter.PDGChild = [-11, 11]   # Select e+ or e- in Z decay
filtSeq.ParentChildFilter.RapidityMinParent = 1.75  # min abs y 1.75

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 nn -> Z->ee, abd Z_y max 1.75 production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Andrzej Olszewski <andrzej.olszewski@ifj.edu.pl>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2electron' ]
evgenConfig.minevents = 1000

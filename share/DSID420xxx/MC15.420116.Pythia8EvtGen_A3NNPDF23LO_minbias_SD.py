evgenConfig.description = "Single diffractive, with the A3 NNPDF23LO tune and EvtGen"

evgenConfig.keywords = ["QCD", "minBias", "SM"]

# Note: The tune used here (A3 NNPDF23LO) is not the standard one for high pT physics.  It is an option for pileup.  For standard high pT physics samples for MC15/run2 please see the A14 NNPDF23LO tune

include("MC15JobOptions/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

evgenConfig.minevents = 1000


evgenConfig.description = "Pythia6 Perugia2012 tune with CTEQ6L1 mu+p w/ virtual photons for photo-production"
evgenConfig.tune = "Perugia2012"
evgenConfig.keywords=["dijet","coherent"]
include("MC15JobOptions/Pythia_Perugia2012_Common.py")

genSeq.Pythia.PythiaCommand += ["pyinit use_PYINIT CMS gamma/mu+ p+ 5020"]
genSeq.Pythia.PythiaCommand += ["pysubs msel 1",
                                "pysubs ckin 3 10.",
                                "pysubs ckin 61 0.00001", #min x for photon, beam1
                                "pysubs ckin 62 0.05", #max x for photon, beam1
                                "pysubs ckin 63 0.00001", #min x for photon, beam2
                                "pysubs ckin 64 0.05", #max x for photon, beam2
                                "pysubs ckin 66 0.001", #max virtuality on photon, beam1
                                "pysubs ckin 68 0.001", #max virtuality on phton, beam2
                                "pysubs ckin 77 15", #minimum of photon-hadron invariant mass
                                "pypars mstp 14 10", #production mode, mix of direct/VMD/GVMD
                                "pypars mstp 55 5", #PDF set for photons, re-set to default
                                "pypars mstp 56 1"] #PDF library for photons, re-set to default


genSeq.Pythia.PythiaCommand += ["pysubs ckin 3 15."]
include("MC15JobOptions/JetFilter_JZ2R04.py")

#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.beam_1_type = 2   # n
PowhegConfig.beam_2_type = 2   # n
PowhegConfig.vdecaymode = 2    # mumu

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 10 # increase number of generated events by 10
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("ParentChildFilter")

filtSeq.ParentChildFilter.PDGParent = [23]  # Select Z
filtSeq.ParentChildFilter.PDGChild = [-13, 13]   # Select mumu in Z decay
filtSeq.ParentChildFilter.PtMinParent = 25000.  # min Pt 25 GeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 nn -> Z->mumu, Z_pt min 25 GeV production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["Andrzej Olszewski <andrzej.olszewski@ifj.edu.pl>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2muon' ]
evgenConfig.minevents = 1000

evgenConfig.description = "Herwig7 multijets with pdf NNPDF30nlo, slice JZ3 with mu filter"
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.keywords+=['QCD', 'jets', 'muon']
evgenConfig.contact = ['jdickinson@lbl.gov']
evgenConfig.minevents = 100

include("MC15JobOptions/Herwig7EvtGen_H7UE_NNPDF30nlo_jetjet.py");
include("MC15JobOptions/JetFilter_JZ3.py")

include("MC15JobOptions/LowPtMuonFilter.py")

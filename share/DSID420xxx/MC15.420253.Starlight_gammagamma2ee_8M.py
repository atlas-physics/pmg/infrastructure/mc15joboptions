evgenConfig.description = "Starlight gamma + gamma UPC collisions at 5020 GeV to continuum -> 2 e, m > 8 GeV, pT(e)>1.8GeV, |eta(e)|<2.6"
evgenConfig.keywords = ["2photon","2lepton"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["mateusz.dyndal@cern.ch"]

# TODO: Sort out proper param setting based on runArgs.ecmEnergy
if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)

include("MC15JobOptions/Starlight_Common.py")

genSeq.Starlight.Initialize = \
    ["beam1Z 82", "beam1A 208", #Z,A of projectile
     "beam2Z 82", "beam2A 208", #Z,A of target
     # TODO: Calculate this from runArgs.ecmEnergy
     "beam1Gamma 2705.", #2672.89",   #Gamma of the colliding ion1, for sqrt(nn)=5.02 TeV
     "beam2Gamma 2705.", #2672.89",   #Gamma of the colliding ion2, for sqrt(nn)=5.02 TeV
     "maxW 200.", #Max value of w
     "minW 8.", #Min value of w
     "nmbWBins 400", #Bins n w
     "maxRapidity 9.", #max y
     "nmbRapidityBins 1000", #Bins n y
     "accCutPt 1", #Cut in pT? 0 = (no, 1 = yes)
     "minPt 1.8", #Minimum pT in GeV
     "maxPt 100.0", #Maximum pT in GeV
     "accCutEta 1", #Cut in pseudorapidity? (0 = no, 1 = yes)
     "minEta -2.6", #Minimum pseudorapidity
     "maxEta 2.6", #Maximum pseudorapidity
     "productionMode 1", #(1=2-phot,2=vmeson(narrow),3=vmeson(wide))
     "nmbEventsTot 1", #Number of events
     "prodParticleId 11", #Channel of interest
     "beamBreakupMode 5", #Controls the nuclear breakup
     "interferenceEnabled 0", #Interference (0 = off, 1 = on)
     "interferenceStrength 1.", #% of intefernce (0.0 - 0.1)
     "coherentProduction 1", #Coherent=1,Incoherent=0
     "incoherentFactor 1.", #percentage of incoherence
     "maxPtInterference 0.24", #Maximum pt considered, when interference is turned on
     "nmbPtBinsInterference 120", #Number of pt bins when interference is turned on
     "xsecMethod 0", #Set to 0 to use old method for calculating gamma-gamma luminosity
     "nThreads 1", #Number of threads used for calculating luminosity (when using the new method)
     "pythFullRec 0" #Write full pythia information to output (vertex, parents, daughter etc)
    ]

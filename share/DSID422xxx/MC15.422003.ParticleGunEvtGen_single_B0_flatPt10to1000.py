evgenConfig.description = "Single B0 with flat E in [10-1000] GeV"
evgenConfig.keywords = ["singleParticle", "B0"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 511
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[10000, 1000000], eta=[-1.0, 1.0], mass=5.2795300e+03)

evgenConfig.auxfiles += [ 'inclusiveP8DsDPlus.pdt' ]

include("MC15JobOptions/EvtGen_Fragment.py")
# genSeq.EvtInclusiveDecay.isfHerwig=True
genSeq.EvtInclusiveDecay.blackList=[]
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"


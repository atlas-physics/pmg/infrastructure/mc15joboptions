evgenConfig.description = "Single K0s with flat phi, eta in [-3.0, 3.0], and flat pT [1,10] GeV"
evgenConfig.keywords = ["singleParticle", "K0S"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (310)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[1000,10000], eta=[-3.0, 3.0])


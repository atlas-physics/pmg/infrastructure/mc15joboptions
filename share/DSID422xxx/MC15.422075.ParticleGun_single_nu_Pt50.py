evgenConfig.description = "Single neutrinos with fixed eta and E: purely for pile-up/lumi testing"
evgenConfig.keywords = ["singleParticle", "neutrino"]
 
include("MC15JobOptions/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 12
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=50000, eta=0.0)

evgenConfig.description = "Single muons with flat phi, eta in [-4.1, 4.1], and flat pT [1,150] GeV"
evgenConfig.keywords = ["singleParticle", "muon"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = (-13, 13)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=[1000,150000], eta=[-4.1, 4.1])

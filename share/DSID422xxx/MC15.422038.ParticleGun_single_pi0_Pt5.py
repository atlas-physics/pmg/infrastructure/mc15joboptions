evgenConfig.description = "Single Pi0 with flat eta-phi and fixed pT of 5 GeV"
evgenConfig.keywords = ["singleParticle", "pi0"]

include("MC15JobOptions/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 111
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=5000, eta=[-4.2, 4.2])


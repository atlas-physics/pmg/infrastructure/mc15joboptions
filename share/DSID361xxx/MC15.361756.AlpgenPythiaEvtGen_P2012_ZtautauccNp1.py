#Alpgen Pythia JobOption
evgenConfig.description = "ALPGEN+Pythia Z(->tautau)+jets process with PythiaPerugia2012C tune with c-jet filter"
evgenConfig.keywords = ["SM", "Z", "tau", "jets"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "AlpgenPythia_P2012_ZtautauccNp1"

if runArgs.trfSubstepName == 'generate' :
    include('MC15JobOptions/AlpgenPythia_Perugia2012_Common.py')
    include('MC15JobOptions/Pythia_Tauola.py')
    include('MC15JobOptions/Pythia_Photos.py')

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Alpgen_EvtGen.py')



include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa+OpenLoops WWZ+0,1,2j@LO with 2l4v decays."
evgenConfig.keywords = ["SM", "triboson", "2lepton", "neutrino", "jets"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_CT10_WWZ"
evgenConfig.generators += ["Sherpa"]

evgenConfig.process="""
(run){
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  LJET:=0; NJET:=2; QCUT:=30;
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;

  HARD_DECAYS On;
  STABLE[24]=0
  WIDTH[24]=0
  STABLE[23]=0
  WIDTH[23]=0
  HDH_ONLY_DECAY {24,12,-11}|{24,14,-13}|{24,16,-15}|{-24,-12,11}|{-24,-14,13}|{-24,-16,15}|{23,12,-12}|{23,14,-14}|{23,16,-16};
}(run);

(processes){
  Process 901 901 -> 23 24 -24 901{NJET};
  Order_EW 3; 
  CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes);
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0", "WIDTH[23]=0" ]

genSeq.Sherpa_i.CrossSectionScaleFactor = WlvBRfac**2 * ZvvBRfac

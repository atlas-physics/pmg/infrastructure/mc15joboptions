## MC@NLO+HERWIG+JIMMY config -- just include a standard HERWIG+JIMMY tune and enable MC@NLO mode
evgenConfig.description = 'MC@NLO VV->leptons using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['electroweak', 'diboson', 'lepton']
evgenConfig.contact = ['alexander.oh@cern.ch']
evgenConfig.inputfilecheck = 'WW_e_mu_0p6_0_0_13TeV'
evgenConfig.generators += ["McAtNlo"]

if runArgs.trfSubstepName == 'generate' :

  include('MC15JobOptions/nonStandard/Herwig_AUET2_CT10_Common.py')

  genSeq.Herwig.HerwigCommand += ['iproc mcatnlo']

  include('MC15JobOptions/nonStandard/Herwig_Tauola.py')
  include('MC15JobOptions/nonStandard/Herwig_Photos.py')

## Run EvtGen in afterburner mode
include("MC15JobOptions/nonStandard/Herwig_EvtGen.py")

if runArgs.trfSubstepName == 'afterburn':
  genSeq.EvtInclusiveDecay.isfHerwig=True


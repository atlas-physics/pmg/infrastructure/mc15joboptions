#Alpgen Pythia JobOption
evgenConfig.description = "ALPGEN+Pythia Z(->mumu)+jets process with PythiaPerugia2012C tune"
evgenConfig.keywords = ["SM", "Z", "muon", "jets"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "AlpgenPythia_P2012_ZmumuNp2"

if runArgs.trfSubstepName == 'generate' :
   include('MC15JobOptions/AlpgenPythia_Perugia2012_Common.py')
   include('MC15JobOptions/Pythia_Tauola.py')
   include('MC15JobOptions/Pythia_Photos.py')

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Alpgen_EvtGen.py')


evgenConfig.description = "High-pT inelastic minimum bias events for pile-up, with the A3 NNPDF23LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]

evgenConfig.saveJets = True

include("MC15JobOptions/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += \
    ["SoftQCD:inelastic = on"]

include("MC15JobOptions/JetFilter_MinbiasHigh.py")

evgenConfig.minevents = 1000

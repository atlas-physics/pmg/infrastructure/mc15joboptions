# Pythia8 minimum bias A14 inclusive

evgenConfig.description = "Incluisve minimum bias, with the A14 NNPDF23LO tune (NO EvtGen)"
evgenConfig.keywords = ["QCD", "minBias", "SM"]

include ("MC15JobOptions/nonStandard//Pythia8_A14_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]


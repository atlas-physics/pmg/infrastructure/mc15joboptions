#Alpgen Pythia JobOption
evgenConfig.description = "ALPGEN+Pythia W(->munu)+cc process with PythiaPerugia2012C tune"
evgenConfig.keywords = ["SM", "W", "muon", "jets"]
evgenConfig.minevents = 5000
evgenConfig.inputfilecheck = "AlpgenPythia_P2012_WmunuccNp3"

if runArgs.trfSubstepName == 'generate' :
   include('MC15JobOptions/AlpgenPythia_Perugia2012_Common.py')
   include('MC15JobOptions/Pythia_Tauola.py')
   include('MC15JobOptions/Pythia_Photos.py')

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Alpgen_EvtGen.py')


include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "llvvjj-EW with high mjj filter"
evgenConfig.keywords = ["SM", "diboson", "2lepton", "VBS"]
evgenConfig.contact  = ["chris.g@cern.ch", "atlas-generators-sherpa@cern.ch", "jvannieu@cern.ch"]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "Sherpa_CT10_llvvjj_EW6"

evgenConfig.process="""
(run){
  EW_SCHEME=3;
  ACTIVE[25]=1;
  MASS[25]=126.0;
  WIDTH[25]=0.00418;
  MASSIVE[5]=1; 
  MASSIVE[11]=1;
  MASSIVE[13]=1;
  MASSIVE[15]=1;
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;

  %scales, tags for scale variations
  FSCF:=1.0; RSCF:=1.0; QSCF:=1.0;
  EXCLUSIVE_CLUSTER_MODE=1;
  #SCALES=STRICT_METS{FSCF*MU_F2}{RSCF*MU_R2}{QSCF*MU_Q2};
  #CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  # simplified setup as long as only 2->6 taken into account:
  SCALES=VAR{FSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSCF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSCF*Abs2(p[2]+p[3]+p[4]+p[5])};
  
  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)

(processes){
  # V V -> e- antinu_e e+ nu_e
  Process 93 93 -> 11 -12 -11 12 93 93;
  Order_EW 6;
  End process;

  # V V -> e- antinu_e mu+ nu_mu
  Process 93 93 -> 11 -12 -13 14 93 93;
  Order_EW 6;
  End process;

  # V V -> e- antinu_e tau+ nu_tau
  Process 93 93 -> 11 -12 -15 16 93 93;
  Order_EW 6;
  End process;

  # V V -> mu- antinu_mu e+ nu_e
  Process 93 93 -> 13 -14 -11 12 93 93;
  Order_EW 6;
  End process;

  # V V -> mu- antinu_mu mu+ nu_mu
  Process 93 93 -> 13 -14 -13 14 93 93;
  Order_EW 6;
  End process;

  # V V -> mu- antinu_mu tau+ nu_tau
  Process 93 93 -> 13 -14 -15 16 93 93;
  Order_EW 6;
  End process;

  # V V -> tau- antinu_tau e+ nu_e
  Process 93 93 -> 15 -16 -11 12 93 93;
  Order_EW 6;
  End process;

  # V V -> tau- antinu_tau mu+ nu_mu
  Process 93 93 -> 15 -16 -13 14 93 93;
  Order_EW 6;
  End process;

  # V V -> tau- antinu_tau tau+ nu_tau
  Process 93 93 -> 15 -16 -15 16 93 93;
  Order_EW 6;
  End process;

  # V V -> e- e+  nu_mu antinu_mu
  Process 93 93 -> 11 -11 14 -14 93 93;
  Order_EW 6;
  End process;

  # V V -> e- e+  nu_tau antinu_tau
  Process 93 93 -> 11 -11 16 -16 93 93;
  Order_EW 6;
  End process;

  # V V -> mu- mu+  nu_e antinu_e
  Process 93 93 -> 13 -13 12 -12 93 93;
  Order_EW 6;
  End process;

  # V V -> mu- mu+  nu_tau antinu_tau
  Process 93 93 -> 13 -13 16 -16 93 93;
  Order_EW 6;
  End process;

  # V V -> tau- tau+  nu_e antinu_e
  Process 93 93 -> 15 -15 12 -12 93 93;
  Order_EW 6;
  End process;

  # V V -> tau- tau+  nu_mu antinu_mu
  Process 93 93 -> 15 -15 14 -14 93 93;
  Order_EW 6;
  End process;

}(processes)

(selector){
  Mass 11 -11 0.1 E_CMS
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -5.0,5.0:-5.0,5.0 [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

#--------------------------------------------------------------
# VBF Mjj Interval filter
#--------------------------------------------------------------

include("MC15JobOptions/VBFMjjIntervalFilter.py")
filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0
filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 15.*GeV
filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 15.*GeV
filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
filtSeq.VBFMjjIntervalFilter.NoJetProbability  = 1000.0*0.0000252
filtSeq.VBFMjjIntervalFilter.OneJetProbability = 1000.0*0.0000252
filtSeq.VBFMjjIntervalFilter.LowMjjProbability = 1000.0*0.0000252
filtSeq.VBFMjjIntervalFilter.HighMjjProbability = 1.0
filtSeq.VBFMjjIntervalFilter.LowMjj = 100.*GeV
filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = False
filtSeq.VBFMjjIntervalFilter.HighMjj = 2500.*GeV
filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = True
filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = True

# Pythia8 minimum bias SD with A2

evgenConfig.description = "SD Minbias with Pythia8 A2"
evgenConfig.keywords = ["minBias"]
evgenConfig.generators = ["Pythia8"]

include ("MC15JobOptions/nonStandard/Pythia8_A2_MSTW2008LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]


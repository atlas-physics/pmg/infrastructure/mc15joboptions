# Pythia8 minimum bias CD with A3

evgenConfig.description = "Central-diffractive events, with the A3 MSTW2008LO tune. Default PomFlux4"
evgenConfig.keywords = ["QCD", "minBias","diffraction","CD"]
evgenConfig.generators = ["Pythia8"]

# include("../Pythia8_A3_NNPDF23LO_Common.py")
include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:centralDiffractive = on"]
genSeq.Pythia8.Commands += ["SigmaTotal:zeroAXB = off"]

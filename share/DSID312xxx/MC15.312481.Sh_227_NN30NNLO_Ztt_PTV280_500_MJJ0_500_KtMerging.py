include("Sherpa_i/2.2.7_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2@NLO + 3,4j@LO with 280 GeV < pTV < 500 GeV, 0 < Mjj < 500 GeV."
evgenConfig.keywords = ["SM", "Z", "lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch", "bjr@sas.upenn.edu"]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "Ztt_PTV280_500_MJJ0_500_KtMerging"

genSeq.Sherpa_i.RunCard = """
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=2,3,4; QCUT:=20.;

  % Fastjet/antikt merging criterion.
  JET_CRITERION FASTJET[A:kt,R:0.4,y:5];

  % Handle hadronic tau decays.
  SOFT_SPIN_CORRELATIONS=1
  DECAYPATH=. 

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops
}(run)

(processes){
  Process 93 93 -> 15 -15 93 93{NJET};

  % This uses a different selector for 0- and 1- parton matrix elements.
  % 0-parton matrix elements might be forbidden, but... hopefully it works.
  Selector_File *|(sel_0_1){|}(sel_0_1) {2,3}

  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

% This selector imposes the ptv and mjj requirements.
(selector){
  Mass 15 -15 40.0 E_CMS
  "PT" 15,-15  280,500
  FastjetSelector Mass(p[4]+p[5])<500. antikt 2 20. 0. 0.4
}(selector)

% This selector only imposes the ptv requirement.
(sel_0_1){
  Mass 15 -15 40.0 E_CMS
  "PT" 15,-15  280,500
}(sel_0_1)

"""

genSeq.Sherpa_i.OpenLoopsLibs = ["ppll", "ppllj", "pplljj"]
genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.ExtraFiles = ['Decaydata.db']

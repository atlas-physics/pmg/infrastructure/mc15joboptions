evgenConfig.description = "SuperChic3 MC gamma + gamma pp collisions at 13000 GeV to 2 gamma ALP mediated"
evgenConfig.keywords = ["2photon"]
#evgenConfig.weighting = 0
evgenConfig.contact = ["denis-patrick.odagiu@cern.ch"]

# TODO: Sort out proper param setting based on runArgs.ecmEnergy
#if int(runArgs.ecmEnergy) != 5020:
#    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
#    sys.exit(1)
evgenConfig.inputfilecheck = 'ALP_1000_13TeV'

include("MC15JobOptions/HepMCReadFromFile_Common.py")
del testSeq.TestHepMC

evgenConfig.tune = "none"


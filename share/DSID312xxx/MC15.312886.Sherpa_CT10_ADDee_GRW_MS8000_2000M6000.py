include("MC15JobOptions/Sherpa_CT10_Common.py")
#include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "ADD Graviton to ll"
evgenConfig.keywords = ["exotic", "graviton", "BSM"]
evgenConfig.contact = ["shellesu@cern.ch"]


evgenConfig.process="""
(processes){
  #
  # jet jet -> e+ e-
  #
  Process 93 93 -> 11 -11 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 2000.0 6000.
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3           ! number of extra dimensions
  M_S   = 8000       ! string scale (GeV)
  M_CUT = 8000        ! invariant mass cut. Ultra violet cut-off. 
  KK_CONVENTION = 5   

  MASS[39] = 100.
  MASS[40] = 100.

}(model)

#(me){
#  ME_SIGNAL_GENERATOR = Amegic
#}(me)



 
"""




#genSeq.Sherpa_i.Parameters += ["METS_SCALE_MUFMODE=0"]
#evgenConfig.minevents = 5000


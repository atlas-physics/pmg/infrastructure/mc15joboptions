mH = 125
mfd2 = 25
mfd1 = 18
mZd = 6000
nGamma = 2
avgtau = 600
decayMode = 'normal'
include("MC15JobOptions/MadGraphControl_A14N23LO_FRVZdisplaced_wh.py")
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

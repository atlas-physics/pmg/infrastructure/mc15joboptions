## Pythia8 W->el+HNL & HNL->el mu nu_mu

evgenConfig.description = "W->el+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W"]
evgenConfig.contact = ["Dominique Anderson Trischuk, d.trischuk@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from EvgenProdTools.EvgenProdToolsConf import TestHepMC
genSeq += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm

# id:all = name antiName spinType chargeType colType m0 mWidth mMin mMax tau0 isResonance mayDecay doExternalDecays isVisible doForceWidth
genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 3.0 0.0 0.0 0.0 100.0 0 1 0 1 0", # 3.0 GeV & 100.0 mm
							"50:isResonance = false",
							# id:addChannel = onMode bRatio meMode product1 product2 ...
							"50:addChannel = 1 0.50 23 -11  13 -14", # HNL decay in el+ mu- nu_mu
							"50:addChannel = 1 0.50 23  11 -13 14", # HNL decay in el- mu+ nu_mu
							"50:mayDecay = on",
							"WeakSingleBoson:ffbar2W = on", # create W+ and W- bosons
							"24:onMode = off", # switch off all W decays
							"24:addchannel = 1 1.0 103 -11 50",# W decay in el+ HNL
							"ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
							"ParticleDecays:tau0Max = 600.0"]


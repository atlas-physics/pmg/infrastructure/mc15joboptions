#-----------------------------------------------------------------
# http://bbgen.web.cern.ch/bbgen/Kwee/6.5TeV/index_6500GeV_BG.html
#-----------------------------------------------------------------

include( "MC15JobOptions/BeamHaloGenerator_Common.py")

# Default settings in common JO
genSeq.BeamHaloGeneratorAlg.inputType="FLUKA-RB"
genSeq.BeamHaloGeneratorAlg.interfacePlane = -22600.0

# The probability of the event being flipped about the x-y plane.  B2
# is incoming negative z in the ATLAS coordinate system.  In the BDSIM
# simulations, z is always positive.  So flip probability should be 1,
# which 
genSeq.BeamHaloGeneratorAlg.flipProbability = 1.0

# The generator settings determine if the event is accepted.
#   * If the allowedPdgId list is not given all particles are accepted.
#   * Limits are in the form of (lower limit, upper limit)
#   * If a limit is not given it is disabled.
#   * If a limit value is -1 then it is disabled.
#   * All limits are checked against |value|
#   * r = sqrt(x^2 + y^2)
genSeq.BeamHaloGeneratorAlg.generatorSettings = []


evgenConfig.inputfilecheck = 'group.phys-gener.BeamHaloGenerator.312564.bdsim_beamgas_20MeV'
evgenConfig.description = "Non collision beam background generator"
evgenConfig.keywords = [ "monojet" ]
evgenConfig.minevents = 5000
evgenConfig.contact = ["stuart.derek.walker@cern.ch"]

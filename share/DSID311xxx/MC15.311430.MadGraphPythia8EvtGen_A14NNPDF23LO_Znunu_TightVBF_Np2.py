evgenConfig.description = "MG Z -> nu nu + 2j@LO  with Truth MET>100 MJJ>800,DPHI<2.5 "
evgenConfig.keywords = ["SM", "Z", "jets", "LO" ]
evgenConfig.contact  = [ "schae@cern.ch"]

include("MC15JobOptions/MadGraphControl_Zjets_LO_Pythia8_VBF.py")
evgenConfig.maxeventsstrategy='IGNORE'
evgenConfig.minevents=1000

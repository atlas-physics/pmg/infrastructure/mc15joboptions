evgenConfig.description = "MG W -> tau nu + 0,1,2,3j@LO with 280<HT<500 ptll>100 MJJ>800,DPHI<2.5, B veto C veto "
evgenConfig.keywords = ["SM", "W", "jets", "LO" ]
evgenConfig.contact  = [ "schae@cern.ch"]

import os
os.environ["LHAPATH"]=os.environ['LHAPATH'].split(':')[0]+":/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/"
os.environ["LHAPDF_DATA_PATH"]=os.environ["LHAPATH"]
ihtmin=280
ihtmax=500
HTrange='midHT'
include('MC15JobOptions/MadGraphControl_Wjets_LO_Pythia8_25ns_VBF.py')
evgenConfig.maxeventsstrategy='IGNORE'
evgenConfig.minevents=1000

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
include("MC15JobOptions/CHadronPt4Eta3_Filter.py")
filtSeq += HeavyFlavorBHadronFilter
filtSeq += HeavyFlavorCHadronPt4Eta3_Filter
filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter) and QCDTruthJetFilter and VBFMjjIntervalFilter"

evgenConfig.description = "Excited quark with Pythia8, A14 tune and NNPDF23LO PDF, m=8500 GeV"
evgenConfig.process = "qg->q*->qg"
evgenConfig.keywords = ["exotic","excitedQuark","jets"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = ["Nishu Nishu <nishu.nishu@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
        "ExcitedFermion:dg2dStar = on",
        "ExcitedFermion:ug2uStar = on",
        "4000001:m0 = 8500", # d* mass
        "4000002:m0 = 8500", # u* mass
        "ExcitedFermion:Lambda =  8500",
        "ExcitedFermion:coupF = 1.0", # SU(2) coupling
        "ExcitedFermion:coupFprime = 1.0", # U(1) coupling
        "ExcitedFermion:coupFcol = 1.0", # SU(3) coupling
        "4000001:onMode = off",     # turn all off
        "4000002:onMode = off",     # turn all off
        "4000001:0:onMode = on",     # turn on d* -> d g
        "4000002:0:onMode = on"]     # turn on u* -> u g


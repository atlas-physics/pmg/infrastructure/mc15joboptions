## Pythia8 W->mu+HNL & HNL -> mu mu nu_mu

evgenConfig.description = "W->mu+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W"]
evgenConfig.contact = ["Jyoti Prakash Biswal, jyoti.prakash.biswal@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

from EvgenProdTools.EvgenProdToolsConf import TestHepMC
genSeq += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm

genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 5.0 0.0 0.0 0.0 100.0 0 1 0 1 0", # 5.0 GeV & 100.0 mm
                            "50:isResonance = false",
                            "50:addChannel = 1 0.50 23 -13  13 -14",# HNL decay in mu+ mu- nu_mu
                            "50:addChannel = 1 0.50 23  13 -13 14",# HNL decay in mu- mu+ nu_mu
                            "50:mayDecay = on",
                            "WeakSingleBoson:ffbar2W = on", # create W+ and W- bosons
                            "24:onMode = off", # switch off all W decays
			    "24:addchannel = 1 1.0 103 -13 50",# W decay in mu+ HNL
			    "ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
			    "ParticleDecays:tau0Max = 600.0"]
	


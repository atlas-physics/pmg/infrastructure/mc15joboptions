#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.33700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.30000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.34000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05411894E+01   # W+
        25     1.26000000E+02   # h
        35     2.00401615E+03   # H
        36     2.00000000E+03   # A
        37     2.00150727E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.00370329E+03   # ~d_L
   2000001     5.00359399E+03   # ~d_R
   1000002     5.00345891E+03   # ~u_L
   2000002     5.00351666E+03   # ~u_R
   1000003     5.00370329E+03   # ~s_L
   2000003     5.00359399E+03   # ~s_R
   1000004     5.00345891E+03   # ~c_L
   2000004     5.00351666E+03   # ~c_R
   1000005     5.00309757E+03   # ~b_1
   2000005     5.00420113E+03   # ~b_2
   1000006     5.12250634E+03   # ~t_1
   2000006     5.41554947E+03   # ~t_2
   1000011     5.00008219E+03   # ~e_L
   2000011     5.00007610E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008219E+03   # ~mu_L
   2000013     5.00007610E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99972198E+03   # ~tau_1
   2000015     5.00043691E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     1.42933978E+03   # ~g
   1000022     1.37681434E+03   # ~chi_10
   1000023    -1.42614174E+03   # ~chi_20
   1000025     1.46405367E+03   # ~chi_30
   1000035     3.12792154E+03   # ~chi_40
   1000024     1.42258233E+03   # ~chi_1+
   1000037     3.12791931E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.23889617E-01   # N_11
  1  2    -3.05591988E-02   # N_12
  1  3     4.89074968E-01   # N_13
  1  4    -4.85649703E-01   # N_14
  2  1     3.06890810E-03   # N_21
  2  2    -3.41015386E-03   # N_22
  2  3    -7.07000597E-01   # N_23
  2  4    -7.07198069E-01   # N_24
  3  1     6.89907975E-01   # N_31
  3  2     3.37871104E-02   # N_32
  3  3    -5.09968642E-01   # N_33
  3  4     5.12657196E-01   # N_34
  4  1     1.17927603E-03   # N_41
  4  2    -9.98955924E-01   # N_42
  4  3    -2.97962347E-02   # N_43
  4  4     3.46100609E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.21091888E-02   # U_11
  1  2     9.99113015E-01   # U_12
  2  1     9.99113015E-01   # U_21
  2  2     4.21091888E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -4.89177475E-02   # V_11
  1  2     9.98802810E-01   # V_12
  2  1     9.98802810E-01   # V_21
  2  2     4.89177475E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99946008E-01   # cos(theta_t)
  1  2     1.03913948E-02   # sin(theta_t)
  2  1    -1.03913948E-02   # -sin(theta_t)
  2  2     9.99946008E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.71176858E-01   # cos(theta_b)
  1  2     7.41297258E-01   # sin(theta_b)
  2  1    -7.41297258E-01   # -sin(theta_b)
  2  2     6.71176858E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04088665E-01   # cos(theta_tau)
  1  2     7.10112070E-01   # sin(theta_tau)
  2  1    -7.10112070E-01   # -sin(theta_tau)
  2  2     7.04088665E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201273E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.34000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52177903E+02   # vev(Q)              
         4     3.40002307E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52772155E-01   # gprime(Q) DRbar
     2     6.27107518E-01   # g(Q) DRbar
     3     1.08739692E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02503623E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71757289E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79795407E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.33700000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.30000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.03554449E+06   # M^2_Hd              
        22    -7.14723444E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37018490E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.87035800E-06   # gluino decays
#          BR         NDA      ID1       ID2
     2.61539743E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.33023637E-07    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     4.91253339E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.46658324E-17    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.48261377E-06    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.91144200E-17    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     4.91253339E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.46658324E-17    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.48261377E-06    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.91144200E-17    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.25083556E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     7.48661454E-13    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.48661454E-13    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.48661454E-13    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.48661454E-13    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     3.14138940E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.76424516E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.94297510E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.34652212E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.44929498E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.74434109E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.85836803E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.15443608E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     5.02296554E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     5.82192906E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.83809730E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.54529793E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.79399587E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.82693710E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.33614285E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.76138514E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.07530519E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.21936019E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -8.28777996E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.56012683E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     4.29875958E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.19647482E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.55523153E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.15924143E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.26616447E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.07411683E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.17194785E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     9.92397006E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.36338578E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.59100943E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.68140613E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.22182032E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.17664117E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.59381303E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.50501195E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.31216012E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.03558769E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.18470591E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.41228294E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.41081262E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.31875158E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.74518868E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.91115285E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.17061218E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.82310932E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.09853515E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.28484820E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.08501274E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.70275356E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.85349152E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.41082846E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60614563E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.41233397E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.85266319E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.05149511E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.98960199E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.91417450E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.34954368E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.82761251E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.09894501E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.21740234E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.37118988E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.53864693E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.77477277E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.21073446E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89853936E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.41228294E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.41081262E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.31875158E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.74518868E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.91115285E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.17061218E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.82310932E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.09853515E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.28484820E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.08501274E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.70275356E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.85349152E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.41082846E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60614563E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.41233397E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.85266319E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.05149511E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.98960199E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.91417450E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.34954368E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.82761251E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.09894501E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.21740234E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.37118988E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.53864693E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.77477277E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.21073446E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89853936E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.70267761E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.76938863E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.73154110E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.07672087E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.67194893E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.16786365E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.35269539E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.09316033E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.29393282E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.40130616E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.70596708E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.09393586E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.70267761E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.76938863E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.73154110E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.07672087E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.67194893E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.16786365E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.35269539E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.09316033E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.29393282E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.40130616E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.70596708E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.09393586E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.40323393E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.87723031E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.68443950E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.62046405E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.48562129E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.43556510E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.97614761E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.49664788E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.40110415E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.72771289E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.71108905E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.69744270E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.52062077E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.30389420E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.04620866E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.70278162E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.18450146E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.61123907E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.59344790E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.67849262E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     2.92527132E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.34824730E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.70278162E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.18450146E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.61123907E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.59344790E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.67849262E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     2.92527132E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.34824730E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.70549437E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.18331378E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.60962351E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.58583410E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.67580694E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     3.92365829E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.34289832E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.28867626E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.57558828E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.25016070E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.25016070E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.08338815E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.08338815E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07534345E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.86839886E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53157240E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.10560955E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46262648E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.36217790E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53792486E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.87956865E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.06422637E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02586190E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.86370843E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10429678E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.51356072E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.98125203E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.51250973E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.50998089E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.09275344E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.15955661E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.50144316E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.15955661E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.50144316E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.37341007E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.42900982E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.42900982E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.39573410E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.84790345E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.84790345E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.84790345E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.39171550E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.39171550E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.39171550E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.39171550E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     4.63905193E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     4.63905193E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     4.63905193E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     4.63905193E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36244548E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36244548E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.08882904E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.03172542E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     8.91427409E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     1.41531140E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.26665721E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     3.63432748E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.21098994E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.44424299E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.21098994E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.44424299E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.00291147E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.53328859E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.53328859E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     4.91745968E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.05332658E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.05332658E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.05332658E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.31145363E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.99297226E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.31145363E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.99297226E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.58311646E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.83539038E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.83539038E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.73718326E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.36505368E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.36505368E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.36505368E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.29966185E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.29966185E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.29966185E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.29966185E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.33220007E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.33220007E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.33220007E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.33220007E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.29291924E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.29291924E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     4.88245733E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.26165062E-08    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     4.88245733E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.26165062E-08    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     9.30468333E-09    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.86841830E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.43993957E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51599336E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.60354294E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46409595E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46409595E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13232829E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.38058881E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.39554828E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.12861287E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.74999919E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     6.55907865E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.04264826E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.70270557E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21907691E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01950193E-01    2           5        -5   # BR(h -> b       bb     )
     6.22280382E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20260126E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66027865E-04    2           3        -3   # BR(h -> s       sb     )
     2.01054382E-02    2           4        -4   # BR(h -> c       cb     )
     6.63966488E-02    2          21        21   # BR(h -> g       g      )
     2.30672465E-03    2          22        22   # BR(h -> gam     gam    )
     1.62636540E-03    2          22        23   # BR(h -> Z       gam    )
     2.16624132E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80761713E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78101490E+01   # H decays
#          BR         NDA      ID1       ID2
     1.47084125E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428424E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71216225E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547463E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668124E-05    2           4        -4   # BR(H -> c       cb     )
     9.96064688E-01    2           6        -6   # BR(H -> t       tb     )
     7.97768397E-04    2          21        21   # BR(H -> g       g      )
     2.71362977E-06    2          22        22   # BR(H -> gam     gam    )
     1.16006029E-06    2          23        22   # BR(H -> Z       gam    )
     3.34402084E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66745001E-04    2          23        23   # BR(H -> Z       Z      )
     9.03199271E-04    2          25        25   # BR(H -> h       h      )
     7.32708805E-24    2          36        36   # BR(H -> A       A      )
     2.98181597E-11    2          23        36   # BR(H -> Z       A      )
     6.91199172E-12    2          24       -37   # BR(H -> W+      H-     )
     6.91199172E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82382136E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47373734E-03    2           5        -5   # BR(A -> b       bb     )
     2.43897805E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62266849E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677436E-06    2           3        -3   # BR(A -> s       sb     )
     9.96176831E-06    2           4        -4   # BR(A -> c       cb     )
     9.96996340E-01    2           6        -6   # BR(A -> t       tb     )
     9.43675977E-04    2          21        21   # BR(A -> g       g      )
     3.16134749E-06    2          22        22   # BR(A -> gam     gam    )
     1.35263274E-06    2          23        22   # BR(A -> Z       gam    )
     3.25874036E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472220E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.36368651E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49237312E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81143933E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.50277075E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45693775E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731554E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402592E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.33982815E-04    2          24        25   # BR(H+ -> W+      h      )
     2.73206817E-13    2          24        36   # BR(H+ -> W+      A      )

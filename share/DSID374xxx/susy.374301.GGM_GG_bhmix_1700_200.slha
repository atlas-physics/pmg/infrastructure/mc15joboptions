#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05373705E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416554E+03   # H
        36     2.00000000E+03   # A
        37     2.00209080E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.97172798E+03   # ~d_L
   2000001     4.97161779E+03   # ~d_R
   1000002     4.97148176E+03   # ~u_L
   2000002     4.97154026E+03   # ~u_R
   1000003     4.97172798E+03   # ~s_L
   2000003     4.97161779E+03   # ~s_R
   1000004     4.97148176E+03   # ~c_L
   2000004     4.97154026E+03   # ~c_R
   1000005     4.97157456E+03   # ~b_1
   2000005     4.97177269E+03   # ~b_2
   1000006     5.10312231E+03   # ~t_1
   2000006     5.40274998E+03   # ~t_2
   1000011     5.00008266E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984135E+03   # ~nu_eL
   1000013     5.00008266E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984135E+03   # ~nu_muL
   1000015     5.00002622E+03   # ~tau_1
   2000015     5.00013306E+03   # ~tau_2
   1000016     4.99984135E+03   # ~nu_tauL
   1000021     1.79544888E+03   # ~g
   1000022     1.91308566E+02   # ~chi_10
   1000023    -2.16756374E+02   # ~chi_20
   1000025     2.96411521E+02   # ~chi_30
   1000035     3.12904235E+03   # ~chi_40
   1000024     2.14526834E+02   # ~chi_1+
   1000037     3.12904194E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.69192518E-01   # N_11
  1  2    -2.32976993E-02   # N_12
  1  3     6.31896946E-01   # N_13
  1  4    -6.16459121E-01   # N_14
  2  1     1.80286193E-02   # N_21
  2  2    -4.70679497E-03   # N_22
  2  3    -7.05102489E-01   # N_23
  2  4    -7.08860561E-01   # N_24
  3  1     8.82911758E-01   # N_31
  3  2     1.29536477E-02   # N_32
  3  3    -3.21394222E-01   # N_33
  3  4     3.42059622E-01   # N_34
  4  1     4.21118971E-04   # N_41
  4  2    -9.99633566E-01   # N_42
  4  3    -1.55719058E-02   # N_43
  4  4     2.21375724E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.20145887E-02   # U_11
  1  2     9.99757650E-01   # U_12
  2  1     9.99757650E-01   # U_21
  2  2     2.20145887E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.13034032E-02   # V_11
  1  2     9.99509928E-01   # V_12
  2  1     9.99509928E-01   # V_21
  2  2     3.13034032E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99771367E-01   # cos(theta_t)
  1  2    -2.13825566E-02   # sin(theta_t)
  2  1     2.13825566E-02   # -sin(theta_t)
  2  2     9.99771367E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.71091067E-01   # cos(theta_b)
  1  2     8.82084580E-01   # sin(theta_b)
  2  1    -8.82084580E-01   # -sin(theta_b)
  2  2     4.71091067E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.84680054E-01   # cos(theta_tau)
  1  2     7.28843758E-01   # sin(theta_tau)
  2  1    -7.28843758E-01   # -sin(theta_tau)
  2  2     6.84680054E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90209545E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51768240E+02   # vev(Q)              
         4     3.86056037E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53099792E-01   # gprime(Q) DRbar
     2     6.29225215E-01   # g(Q) DRbar
     3     1.08305245E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02705406E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72363650E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79966816E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.58000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.02019817E+06   # M^2_Hd              
        22    -5.49104574E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37961830E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.97493368E-03   # gluino decays
#          BR         NDA      ID1       ID2
     7.46656439E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     9.69697212E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.04264304E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.96490225E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     3.35976314E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.70087334E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     5.80244274E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.57416215E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.01787734E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.96490225E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     3.35976314E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.70087334E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     5.80244274E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.57416215E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.01787734E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.12235260E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.15056657E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.75315109E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.54040940E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.02392877E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     6.66636537E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.56214943E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.56214943E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.56214943E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.56214943E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35577351E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35577351E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.06301181E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     8.93867265E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.20097872E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.16952603E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.48727414E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.94536015E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.95962992E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.65262610E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.21624357E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.46777108E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.87173559E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.45467459E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.01202085E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     4.50071365E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.71800785E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.01594156E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.44093910E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.67262225E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.26578725E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     6.72144589E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.98474607E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     3.14837822E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.60411327E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.40492064E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.58734184E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.55877897E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.20976870E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.15388994E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.42883915E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.23162363E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.30491747E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     4.41748534E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.15261093E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -3.05550800E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.68177058E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.20679868E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.38022037E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.98207503E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.19087158E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     3.57064334E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.52872466E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.71470841E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.15787075E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.68256674E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.31517670E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02029343E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.08132350E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.11458414E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.64435331E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.93132136E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.30328958E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.49524498E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.19091666E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.05978334E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.60365248E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.98244852E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.15936464E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.32191536E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.31930424E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02082257E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.00258135E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.89607080E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.27259239E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.02149187E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.58342200E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.86884737E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.19087158E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.57064334E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.52872466E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.71470841E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.15787075E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.68256674E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.31517670E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02029343E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.08132350E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.11458414E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.64435331E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.93132136E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.30328958E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.49524498E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.19091666E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.05978334E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.60365248E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.98244852E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.15936464E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.32191536E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.31930424E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02082257E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.00258135E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.89607080E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.27259239E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.02149187E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.58342200E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.86884737E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80543492E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.03115224E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.04687198E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.80163174E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59484144E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.77860203E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19342831E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46528937E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.20847989E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.25802826E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.78826142E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.60415173E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80543492E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.03115224E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.04687198E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.80163174E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59484144E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.77860203E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19342831E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46528937E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.20847989E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.25802826E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.78826142E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.60415173E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63238521E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.35951395E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.64042376E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.72957456E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.29460201E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.92259404E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.59107634E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36677924E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64832640E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.14122344E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.06880048E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.45988610E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.46197189E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.84925052E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.92604563E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80528895E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     5.74841764E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.53666293E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.62270300E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59698335E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.37057744E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19022944E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80528895E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     5.74841764E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.53666293E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.62270300E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59698335E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.37057744E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19022944E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80849785E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     5.74184968E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53490719E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.62084896E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59401612E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.51124992E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18430255E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.26910808E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     2.76373041E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34387855E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34387855E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11462700E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11462700E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.08271252E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.38823956E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.03049504E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.48711566E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.98333674E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.91762122E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.03604155E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.91784878E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.94112024E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.96033972E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.90595800E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     9.86205736E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.01627886E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.94981772E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.02633254E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.93161195E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.20555031E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     6.65321446E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.59084917E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.49088950E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.56185764E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     7.66143688E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22028480E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.57942657E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22028480E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.57942657E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.13481092E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.60414069E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.60414069E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50093339E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.19562819E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.19562819E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.19562819E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.06008111E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.06008111E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.06008111E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.06008111E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.02002723E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.02002723E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.02002723E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.02002723E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.95858989E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.95858989E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.60440754E-03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.41853281E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.77906933E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.77906933E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     6.09556454E-07    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.93127657E-07    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     2.52788321E-09    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.38827602E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.03645453E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.97359951E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.80759871E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.97929012E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.97929012E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.49009005E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.49606018E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.70822181E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.00733453E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.60662159E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.91124409E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     4.87989111E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     4.81927413E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     9.37705510E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.93489183E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.93489183E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.43543944E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.72654526E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.78949962E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.93284611E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     2.85734597E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21696456E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01615689E-01    2           5        -5   # BR(h -> b       bb     )
     6.22607469E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20375901E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66272606E-04    2           3        -3   # BR(h -> s       sb     )
     2.01152865E-02    2           4        -4   # BR(h -> c       cb     )
     6.64298394E-02    2          21        21   # BR(h -> g       g      )
     2.32225362E-03    2          22        22   # BR(h -> gam     gam    )
     1.62696194E-03    2          22        23   # BR(h -> Z       gam    )
     2.16852340E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80902341E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01433205E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38928079E-03    2           5        -5   # BR(H -> b       bb     )
     2.32120478E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20632308E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05069766E-06    2           3        -3   # BR(H -> s       sb     )
     9.48256192E-06    2           4        -4   # BR(H -> c       cb     )
     9.38259679E-01    2           6        -6   # BR(H -> t       tb     )
     7.51422505E-04    2          21        21   # BR(H -> g       g      )
     2.63477868E-06    2          22        22   # BR(H -> gam     gam    )
     1.09160987E-06    2          23        22   # BR(H -> Z       gam    )
     3.17412515E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58273306E-04    2          23        23   # BR(H -> Z       Z      )
     8.52445520E-04    2          25        25   # BR(H -> h       h      )
     8.46088234E-24    2          36        36   # BR(H -> A       A      )
     3.37081487E-11    2          23        36   # BR(H -> Z       A      )
     2.51815387E-12    2          24       -37   # BR(H -> W+      H-     )
     2.51815387E-12    2         -24        37   # BR(H -> W-      H+     )
     7.49636413E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.87489045E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     7.75228677E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     4.52432601E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.45514090E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     7.45552352E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.11349164E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05891406E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39219954E-03    2           5        -5   # BR(A -> b       bb     )
     2.29771221E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12324269E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07093227E-06    2           3        -3   # BR(A -> s       sb     )
     9.38478171E-06    2           4        -4   # BR(A -> c       cb     )
     9.39250215E-01    2           6        -6   # BR(A -> t       tb     )
     8.89018172E-04    2          21        21   # BR(A -> g       g      )
     2.67562152E-06    2          22        22   # BR(A -> gam     gam    )
     1.27307613E-06    2          23        22   # BR(A -> Z       gam    )
     3.09313822E-04    2          23        25   # BR(A -> Z       h      )
     5.99359221E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.30117201E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.12615632E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.74017818E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.19894341E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.48802692E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.69811513E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97853874E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23384633E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34658166E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29601383E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42025768E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13753123E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02367246E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40915457E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17005096E-04    2          24        25   # BR(H+ -> W+      h      )
     1.32033748E-12    2          24        36   # BR(H+ -> W+      A      )
     1.28621793E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.26677534E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.55306575E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

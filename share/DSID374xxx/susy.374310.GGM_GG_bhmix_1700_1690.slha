#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.68100000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.69000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05417508E+01   # W+
        25     1.26000000E+02   # h
        35     2.00400328E+03   # H
        36     2.00000000E+03   # A
        37     2.00151425E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.97172755E+03   # ~d_L
   2000001     4.97161783E+03   # ~d_R
   1000002     4.97148220E+03   # ~u_L
   2000002     4.97154014E+03   # ~u_R
   1000003     4.97172755E+03   # ~s_L
   2000003     4.97161783E+03   # ~s_R
   1000004     4.97148220E+03   # ~c_L
   2000004     4.97154014E+03   # ~c_R
   1000005     4.97097619E+03   # ~b_1
   2000005     4.97237056E+03   # ~b_2
   1000006     5.08559243E+03   # ~t_1
   2000006     5.37647694E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007613E+03   # ~e_R
   1000012     4.99984170E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007613E+03   # ~mu_R
   1000014     4.99984170E+03   # ~nu_muL
   1000015     4.99962855E+03   # ~tau_1
   2000015     5.00053033E+03   # ~tau_2
   1000016     4.99984170E+03   # ~nu_tauL
   1000021     1.79543032E+03   # ~g
   1000022     1.74293278E+03   # ~chi_10
   1000023    -1.79403562E+03   # ~chi_20
   1000025     1.83022539E+03   # ~chi_30
   1000035     3.12623602E+03   # ~chi_40
   1000024     1.78950202E+03   # ~chi_1+
   1000037     3.12623133E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.30014681E-01   # N_11
  1  2    -3.83496855E-02   # N_12
  1  3     4.83887940E-01   # N_13
  1  4    -4.81103240E-01   # N_14
  2  1     2.44002312E-03   # N_21
  2  2    -3.15601245E-03   # N_22
  2  3    -7.07031949E-01   # N_23
  2  4    -7.07170354E-01   # N_24
  3  1     6.83424437E-01   # N_31
  3  2     4.37659571E-02   # N_32
  3  3    -5.14242387E-01   # N_33
  3  4     5.16304510E-01   # N_34
  4  1     1.91043720E-03   # N_41
  4  2    -9.98300497E-01   # N_42
  4  3    -3.88979662E-02   # N_43
  4  4     4.33522347E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.49505967E-02   # U_11
  1  2     9.98489075E-01   # U_12
  2  1     9.98489075E-01   # U_21
  2  2     5.49505967E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -6.12489232E-02   # V_11
  1  2     9.98122522E-01   # V_12
  2  1     9.98122522E-01   # V_21
  2  2     6.12489232E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99872742E-01   # cos(theta_t)
  1  2     1.59530500E-02   # sin(theta_t)
  2  1    -1.59530500E-02   # -sin(theta_t)
  2  2     9.99872742E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.78715383E-01   # cos(theta_b)
  1  2     7.34401409E-01   # sin(theta_b)
  2  1    -7.34401409E-01   # -sin(theta_b)
  2  2     6.78715383E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04740882E-01   # cos(theta_tau)
  1  2     7.09464791E-01   # sin(theta_tau)
  2  1    -7.09464791E-01   # -sin(theta_tau)
  2  2     7.04740882E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90201555E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.69000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52268241E+02   # vev(Q)              
         4     3.21307354E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52732928E-01   # gprime(Q) DRbar
     2     6.26858916E-01   # g(Q) DRbar
     3     1.08305277E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02500727E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.71564904E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79755355E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.68100000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.70000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21    -1.53391565E+05   # M^2_Hd              
        22    -8.09130771E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36907488E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     2.14817110E-05   # gluino decays
#          BR         NDA      ID1       ID2
     8.97450434E-05    2     1000022        21   # BR(~g -> ~chi_10 g)
     3.82683752E-09    2     1000023        21   # BR(~g -> ~chi_20 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     1.87139295E-07    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     1.14261339E-19    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.48281867E-07    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     8.74407357E-20    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.87139295E-07    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     1.14261339E-19    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.48281867E-07    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     8.74407357E-20    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.62204142E-07    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.30271303E-13    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.30271303E-13    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.30271303E-13    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.30271303E-13    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     2.84561141E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     4.63383307E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.81654889E-02    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     5.34633300E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.65129491E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     9.41880522E-05    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.23663865E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     7.13603180E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.52781114E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     4.92833577E-03    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.49746961E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.45896759E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     8.74949483E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.84328832E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.00128477E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.75206337E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.01714793E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.23706581E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.63783980E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.94120226E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.07489626E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     1.24467937E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.31747525E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.19540529E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.25703119E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.89593265E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.30655501E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     1.01388775E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.64647693E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     8.52916997E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.40790363E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.37663483E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.95078420E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.50342179E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.59631189E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.26152799E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     3.23402384E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     8.20574279E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.18861016E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     6.41107142E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.72417208E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.89517997E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.13458553E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     4.87541632E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.27146172E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.02915226E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.05636555E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     2.10645805E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.31432484E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.79356117E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.88257662E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.60999507E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.18868016E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.02697174E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.47324737E-07    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.56831340E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.13954557E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.92446074E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.27698106E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.02957637E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.99626601E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.42482019E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.96014886E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.61901289E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.77254536E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.89956090E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.18861016E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.41107142E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.72417208E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.89517997E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.13458553E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     4.87541632E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.27146172E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.02915226E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.05636555E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.10645805E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.31432484E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.79356117E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.88257662E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.60999507E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.18868016E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.02697174E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.47324737E-07    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.56831340E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.13954557E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.92446074E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.27698106E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.02957637E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.99626601E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.42482019E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.96014886E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.61901289E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.77254536E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.89956090E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.65384359E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.88321469E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.77724857E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.01331916E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71691010E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.38112498E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.44762024E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.88514191E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.40035922E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     5.93430023E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.59956365E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.77785298E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.65384359E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.88321469E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.77724857E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.01331916E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71691010E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.38112498E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.44762024E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.88514191E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.40035922E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     5.93430023E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.59956365E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.77785298E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.27464218E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.77072153E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.55084844E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.46318253E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.56881260E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.55303937E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.14562099E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.58110974E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.27202221E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.63273244E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25015892E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.53958858E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.60291451E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.62689185E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.21388742E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.65404915E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14628158E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.14651231E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.41379685E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72795854E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.19997317E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.44226581E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.65404915E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14628158E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.14651231E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.41379685E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72795854E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.19997317E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.44226581E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.65648801E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14522920E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.14545973E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.40790851E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.72545407E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.11195408E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.43729178E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.47458335E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     7.13116016E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.09809379E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.09809379E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.03269907E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.03269907E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.02529827E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.79980511E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53518190E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.07981254E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46176525E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.39183515E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53131504E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     9.00969256E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.77987295E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00111641E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.88962254E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.09261052E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.87970676E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.85263950E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.81332881E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.17031910E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.37016850E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.11846624E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.44830650E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.11846624E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.44830650E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33240890E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.30797890E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.30797890E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.27743027E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.60640633E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.60640633E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.60640633E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.74799675E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.74799675E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.74799675E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.74799675E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.24933233E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.24933233E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.24933233E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.24933233E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.97534333E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.97534333E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.01375730E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.17048909E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.29485248E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     4.47062279E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.99556298E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.17406735E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.29364606E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.53515539E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.29364606E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.53515539E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     4.37594650E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.65886690E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.65886690E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.98270311E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     7.51521427E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     7.51521427E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     7.51521427E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.92549426E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.49333308E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.92549426E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.49333308E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.12203763E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.69485996E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.69485996E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.60676608E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.13732221E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.13732221E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.13732221E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.25613211E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.25613211E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.25613211E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.25613211E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.18710117E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.18710117E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.18710117E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.18710117E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.14772146E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.14772146E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
     5.82508452E-08    3     1000021        -2         2   # BR(~chi_30 -> ~g      ub      u)
     1.45086628E-08    3     1000021        -1         1   # BR(~chi_30 -> ~g      db      d)
     5.82508452E-08    3     1000021        -4         4   # BR(~chi_30 -> ~g      cb      c)
     1.45086628E-08    3     1000021        -3         3   # BR(~chi_30 -> ~g      sb      s)
     1.06449274E-08    3     1000021        -5         5   # BR(~chi_30 -> ~g      bb      b)
#
#         PDG            Width
DECAY   1000035     3.79977688E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39201544E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52268094E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.02944630E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46674380E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46674380E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10465830E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     7.24462061E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.42441698E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.15370057E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.85485796E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     9.28152967E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.73560715E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.81095496E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22073892E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02124105E-01    2           5        -5   # BR(h -> b       bb     )
     6.22035869E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20173579E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65844741E-04    2           3        -3   # BR(h -> s       sb     )
     2.00975137E-02    2           4        -4   # BR(h -> c       cb     )
     6.63713622E-02    2          21        21   # BR(h -> g       g      )
     2.30528517E-03    2          22        22   # BR(h -> gam     gam    )
     1.62575721E-03    2          22        23   # BR(h -> Z       gam    )
     2.16521256E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80651157E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78098204E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46820010E-03    2           5        -5   # BR(H -> b       bb     )
     2.46428890E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71217871E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11547773E-06    2           3        -3   # BR(H -> s       sb     )
     1.00668526E-05    2           4        -4   # BR(H -> c       cb     )
     9.96068317E-01    2           6        -6   # BR(H -> t       tb     )
     7.97759309E-04    2          21        21   # BR(H -> g       g      )
     2.71664093E-06    2          22        22   # BR(H -> gam     gam    )
     1.16023126E-06    2          23        22   # BR(H -> Z       gam    )
     3.34484247E-04    2          24       -24   # BR(H -> W+      W-     )
     1.66785988E-04    2          23        23   # BR(H -> Z       Z      )
     9.02094397E-04    2          25        25   # BR(H -> h       h      )
     7.23037343E-24    2          36        36   # BR(H -> A       A      )
     2.93441281E-11    2          23        36   # BR(H -> Z       A      )
     6.64294765E-12    2          24       -37   # BR(H -> W+      H-     )
     6.64294765E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82381157E+01   # A decays
#          BR         NDA      ID1       ID2
     1.47111782E-03    2           5        -5   # BR(A -> b       bb     )
     2.43898430E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62269058E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13677727E-06    2           3        -3   # BR(A -> s       sb     )
     9.96179382E-06    2           4        -4   # BR(A -> c       cb     )
     9.96998893E-01    2           6        -6   # BR(A -> t       tb     )
     9.43678394E-04    2          21        21   # BR(A -> g       g      )
     3.13976456E-06    2          22        22   # BR(A -> gam     gam    )
     1.35282539E-06    2          23        22   # BR(A -> Z       gam    )
     3.25958471E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74472051E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.35789259E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49238295E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81147405E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.49906261E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45695668E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08731931E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99402508E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.34072159E-04    2          24        25   # BR(H+ -> W+      h      )
     2.79590205E-13    2          24        36   # BR(H+ -> W+      A      )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376823E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416759E+03   # H
        36     2.00000000E+03   # A
        37     2.00209593E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.92095337E+03   # ~d_L
   2000001     4.92084237E+03   # ~d_R
   1000002     4.92070535E+03   # ~u_L
   2000002     4.92076433E+03   # ~u_R
   1000003     4.92095337E+03   # ~s_L
   2000003     4.92084237E+03   # ~s_R
   1000004     4.92070535E+03   # ~c_L
   2000004     4.92076433E+03   # ~c_R
   1000005     4.92081529E+03   # ~b_1
   2000005     4.92098193E+03   # ~b_2
   1000006     5.05391317E+03   # ~t_1
   2000006     5.35647467E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     2.23872787E+03   # ~g
   1000022     1.46534606E+02   # ~chi_10
   1000023    -1.62960906E+02   # ~chi_20
   1000025     2.94799553E+02   # ~chi_30
   1000035     3.12909436E+03   # ~chi_40
   1000024     1.60749118E+02   # ~chi_1+
   1000037     3.12909396E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08749845E-01   # N_11
  1  2    -2.47371997E-02   # N_12
  1  3     6.79329185E-01   # N_13
  1  4    -6.65261950E-01   # N_14
  2  1     2.00314807E-02   # N_21
  2  2    -4.81339651E-03   # N_22
  2  3    -7.04243584E-01   # N_23
  2  4    -7.09659457E-01   # N_24
  3  1     9.50932227E-01   # N_31
  3  2     8.56962203E-03   # N_32
  3  3    -2.05723845E-01   # N_33
  3  4     2.30937569E-01   # N_34
  4  1     4.15250868E-04   # N_41
  4  2    -9.99645669E-01   # N_42
  4  3    -1.51832536E-02   # N_43
  4  4     2.18593832E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14650962E-02   # U_11
  1  2     9.99769598E-01   # U_12
  2  1     9.99769598E-01   # U_21
  2  2     2.14650962E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09102139E-02   # V_11
  1  2     9.99522165E-01   # V_12
  2  1     9.99522165E-01   # V_21
  2  2     3.09102139E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99631288E-01   # cos(theta_t)
  1  2    -2.71530487E-02   # sin(theta_t)
  2  1     2.71530487E-02   # -sin(theta_t)
  2  2     9.99631288E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08575785E-01   # cos(theta_b)
  1  2     9.12724399E-01   # sin(theta_b)
  2  1    -9.12724399E-01   # -sin(theta_b)
  2  2     4.08575785E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76623003E-01   # cos(theta_tau)
  1  2     7.36329622E-01   # sin(theta_tau)
  2  1    -7.36329622E-01   # -sin(theta_tau)
  2  2     6.76623003E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90212288E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51749914E+02   # vev(Q)              
         4     3.87652727E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146845E-01   # gprime(Q) DRbar
     2     6.29570207E-01   # g(Q) DRbar
     3     1.07892608E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02741942E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72396089E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79971639E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04057095E+06   # M^2_Hd              
        22    -5.40818392E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111817E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.20241100E-03   # gluino decays
#          BR         NDA      ID1       ID2
     5.07483926E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.72522572E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     5.13831683E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.38249004E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.19787711E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.37128494E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.26585730E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.32130173E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.22079740E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     8.38249004E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.19787711E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.37128494E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.26585730E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.32130173E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.22079740E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.02294159E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.11279686E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.39388912E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.83991325E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.11063954E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.09798102E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     5.87112504E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.87112504E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.87112504E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.87112504E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36204607E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36204607E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.73806142E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.15548746E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.33339101E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.83205833E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.69437022E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.28170129E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.37403358E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.29005399E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.81997389E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.89539886E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.56648182E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.00560818E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.09348219E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.86896474E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.23962722E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.17188674E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.50249739E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.33451932E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.47113158E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.22977257E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.23538925E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     6.16326316E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     9.28521622E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.49971615E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.78499095E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.79810898E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -4.27926116E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -6.91471214E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -8.58737460E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.77927827E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.32111551E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.68773954E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.09877153E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -1.95571761E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.20644971E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.06174728E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.43142616E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     4.98782470E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.86131215E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.08858038E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.14021569E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.45666831E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.58437427E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.91045677E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.16809319E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.88718639E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.75788613E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     5.64659168E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.37596694E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.32997910E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.66401482E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.41029854E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.86137066E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.80148336E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.32350499E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.85019465E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.58604237E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.21313837E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.17275133E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.88782265E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.68019308E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.47694722E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.21468313E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.39413286E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.58416319E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.84575508E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.86131215E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.08858038E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.14021569E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.45666831E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.58437427E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.91045677E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.16809319E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.88718639E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.75788613E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.64659168E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.37596694E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.32997910E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.66401482E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.41029854E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.86137066E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.80148336E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.32350499E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.85019465E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.58604237E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.21313837E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.17275133E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.88782265E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.68019308E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.47694722E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.21468313E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.39413286E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.58416319E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.84575508E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762323E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54470617E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89047780E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04817622E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563253E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45690367E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497468E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513827E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57796608E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03003769E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03817271E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42321265E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762323E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54470617E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89047780E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04817622E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563253E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45690367E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497468E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513827E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57796608E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03003769E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03817271E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42321265E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957279E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08516894E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84735270E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56335455E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26705649E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289474E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53592750E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825932E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319227E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61457966E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12658137E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05214734E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49094037E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85041828E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98400347E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747438E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74590106E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80484780E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92069148E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774194E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33895077E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178212E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747438E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74590106E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80484780E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92069148E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774194E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33895077E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178212E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068871E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74276082E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80278375E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91849495E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477113E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48072312E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584782E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23941976E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87421988E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36072789E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36072789E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024317E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024317E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03767045E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36662650E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99046155E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247397E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94616822E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26252986E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99428224E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31573550E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33894555E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91629484E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29733715E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34274341E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02289802E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82560781E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03258783E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96205780E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.35436525E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.26613536E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.50562117E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65732733E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18408007E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01718395E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27774573E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65368364E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27774573E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65368364E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83789253E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77308260E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77308260E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52636204E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53251348E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53251348E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53251348E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.35983016E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.35983016E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.35983016E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.35983016E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86610215E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86610215E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86610215E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86610215E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36116864E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36116864E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63788966E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83661805E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19028735E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99746325E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99746325E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79600343E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.16492232E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.84034335E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47893388E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.38319784E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36666208E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03758826E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93143763E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02335769E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94062860E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94062860E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71034836E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76474165E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63739271E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36370178E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01111859E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77914335E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22372243E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48877866E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19397581E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33258641E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33258641E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44984111E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77835346E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80589506E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81044045E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61885484E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21690905E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01612643E-01    2           5        -5   # BR(h -> b       bb     )
     6.22620765E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20380607E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66282492E-04    2           3        -3   # BR(h -> s       sb     )
     2.01154774E-02    2           4        -4   # BR(h -> c       cb     )
     6.64318568E-02    2          21        21   # BR(h -> g       g      )
     2.32826234E-03    2          22        22   # BR(h -> gam     gam    )
     1.62700162E-03    2          22        23   # BR(h -> Z       gam    )
     2.16845416E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80906036E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01488108E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38917501E-03    2           5        -5   # BR(H -> b       bb     )
     2.32088120E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20517911E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05055097E-06    2           3        -3   # BR(H -> s       sb     )
     9.48135119E-06    2           4        -4   # BR(H -> c       cb     )
     9.38139936E-01    2           6        -6   # BR(H -> t       tb     )
     7.51311786E-04    2          21        21   # BR(H -> g       g      )
     2.62933827E-06    2          22        22   # BR(H -> gam     gam    )
     1.09154528E-06    2          23        22   # BR(H -> Z       gam    )
     3.18159494E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58645789E-04    2          23        23   # BR(H -> Z       Z      )
     8.52342587E-04    2          25        25   # BR(H -> h       h      )
     8.43485257E-24    2          36        36   # BR(H -> A       A      )
     3.37867702E-11    2          23        36   # BR(H -> Z       A      )
     2.49918033E-12    2          24       -37   # BR(H -> W+      H-     )
     2.49918033E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71001796E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52179960E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36028193E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64303660E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94745929E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53619271E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87724297E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05933069E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39214399E-03    2           5        -5   # BR(A -> b       bb     )
     2.29747638E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12240896E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082236E-06    2           3        -3   # BR(A -> s       sb     )
     9.38381850E-06    2           4        -4   # BR(A -> c       cb     )
     9.39153814E-01    2           6        -6   # BR(A -> t       tb     )
     8.88926927E-04    2          21        21   # BR(A -> g       g      )
     2.69761807E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304439E-06    2          23        22   # BR(A -> Z       gam    )
     3.10051386E-04    2          23        25   # BR(A -> Z       h      )
     5.85780973E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28937944E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65390931E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84513665E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65761014E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36513665E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93376968E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97902383E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23377727E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630160E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29502371E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42021461E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13691629E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02354993E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40802999E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17757332E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33644913E-12    2          24        36   # BR(H+ -> W+      A      )
     5.58022541E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23543438E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29274806E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

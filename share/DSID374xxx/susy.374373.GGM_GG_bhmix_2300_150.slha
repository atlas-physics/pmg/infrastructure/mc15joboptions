#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.30000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05376822E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416797E+03   # H
        36     2.00000000E+03   # A
        37     2.00209585E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.90936659E+03   # ~d_L
   2000001     4.90925543E+03   # ~d_R
   1000002     4.90911821E+03   # ~u_L
   2000002     4.90917728E+03   # ~u_R
   1000003     4.90936659E+03   # ~s_L
   2000003     4.90925543E+03   # ~s_R
   1000004     4.90911821E+03   # ~c_L
   2000004     4.90917728E+03   # ~c_R
   1000005     4.90922831E+03   # ~b_1
   2000005     4.90939519E+03   # ~b_2
   1000006     5.04262832E+03   # ~t_1
   2000006     5.34586265E+03   # ~t_2
   1000011     5.00008277E+03   # ~e_L
   2000011     5.00007600E+03   # ~e_R
   1000012     4.99984123E+03   # ~nu_eL
   1000013     5.00008277E+03   # ~mu_L
   2000013     5.00007600E+03   # ~mu_R
   1000014     4.99984123E+03   # ~nu_muL
   1000015     5.00003957E+03   # ~tau_1
   2000015     5.00011983E+03   # ~tau_2
   1000016     4.99984123E+03   # ~nu_tauL
   1000021     2.32579401E+03   # ~g
   1000022     1.46535042E+02   # ~chi_10
   1000023    -1.62961410E+02   # ~chi_20
   1000025     2.94799618E+02   # ~chi_30
   1000035     3.12909437E+03   # ~chi_40
   1000024     1.60749619E+02   # ~chi_1+
   1000037     3.12909397E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     3.08750937E-01   # N_11
  1  2    -2.47372108E-02   # N_12
  1  3     6.79328940E-01   # N_13
  1  4    -6.65261693E-01   # N_14
  2  1     2.00314701E-02   # N_21
  2  2    -4.81339880E-03   # N_22
  2  3    -7.04243592E-01   # N_23
  2  4    -7.09659450E-01   # N_24
  3  1     9.50931873E-01   # N_31
  3  2     8.56965788E-03   # N_32
  3  3    -2.05724630E-01   # N_33
  3  4     2.30938327E-01   # N_34
  4  1     4.15251488E-04   # N_41
  4  2    -9.99645669E-01   # N_42
  4  3    -1.51832675E-02   # N_43
  4  4     2.18594006E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.14651159E-02   # U_11
  1  2     9.99769598E-01   # U_12
  2  1     9.99769598E-01   # U_21
  2  2     2.14651159E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.09102386E-02   # V_11
  1  2     9.99522164E-01   # V_12
  2  1     9.99522164E-01   # V_21
  2  2     3.09102386E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99606300E-01   # cos(theta_t)
  1  2    -2.80578866E-02   # sin(theta_t)
  2  1     2.80578866E-02   # -sin(theta_t)
  2  2     9.99606300E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08571584E-01   # cos(theta_b)
  1  2     9.12726279E-01   # sin(theta_b)
  2  1    -9.12726279E-01   # -sin(theta_b)
  2  2     4.08571584E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.76622938E-01   # cos(theta_tau)
  1  2     7.36329681E-01   # sin(theta_tau)
  2  1    -7.36329681E-01   # -sin(theta_tau)
  2  2     6.76622938E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90212344E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51750077E+02   # vev(Q)              
         4     3.87587299E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53146849E-01   # gprime(Q) DRbar
     2     6.29570228E-01   # g(Q) DRbar
     3     1.07821956E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02744236E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72391237E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79971521E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.65000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.30000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.04057133E+06   # M^2_Hd              
        22    -5.39187820E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38111826E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.97400018E-03   # gluino decays
#          BR         NDA      ID1       ID2
     4.65961275E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.25397517E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     4.69124951E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     8.33128035E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.21533003E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.36589945E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.25201944E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.35174816E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.21892297E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     8.33128035E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.21533003E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.36589945E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.25201944E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.35174816E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.21892297E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.01773070E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.10918091E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.38836767E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.84474841E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.11784725E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     4.10548212E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     5.83802056E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     5.83802056E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     5.83802056E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     5.83802056E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.36208912E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.36208912E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.66639026E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.18239395E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.36512435E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.88232261E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.74516611E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.59272240E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.47555974E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.20108810E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     3.98344375E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.97661592E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.49830869E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.02328483E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11217806E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     3.92896822E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.31975598E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.20876508E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.66649429E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.25446178E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.68699941E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.35244637E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.35719635E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     6.77110650E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     8.32014826E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.73518482E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.27908429E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.28674517E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -4.92726494E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -7.99113468E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -9.88757706E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.90123134E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.27324794E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.86967943E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.98265031E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -2.13363345E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.27400020E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.18403358E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.56673938E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     4.84686085E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.78940073E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.12884926E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.18239204E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.58450764E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.69570031E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.98111089E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.39072493E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.85240126E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.68743396E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     5.86424652E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.46753455E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.53540635E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.78350608E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.38757011E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78946174E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.05302234E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.59432041E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.95559488E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.69741703E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.55384596E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.39552138E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.85306586E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.60998117E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.53661578E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.46571143E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.45044961E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.91439228E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.83952421E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78940073E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.12884926E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.18239204E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.58450764E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.69570031E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.98111089E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.39072493E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.85240126E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.68743396E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.86424652E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.46753455E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.53540635E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.78350608E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.38757011E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78946174E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     7.05302234E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.59432041E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.95559488E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.69741703E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.55384596E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.39552138E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.85306586E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.60998117E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.53661578E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.46571143E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.45044961E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.91439228E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.83952421E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80762338E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.54471861E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.89047017E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.04817492E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59563254E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.45691553E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19497471E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46513833E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.57803373E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.03003337E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.03816595E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.42323178E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80762338E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.54471861E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.89047017E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.04817492E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59563254E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.45691553E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19497471E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46513833E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.57803373E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.03003337E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.03816595E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.42323178E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.62957286E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     6.08521080E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.84734549E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.56335098E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.26705629E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.89289470E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.53592710E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36825929E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.65319240E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     4.61461389E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.12657998E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.05214317E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.49094062E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.85038584E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.98400398E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80747453E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.74591827E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.80484692E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92068970E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59774196E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.33895291E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19178215E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80747453E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.74591827E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.80484692E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92068970E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59774196E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.33895291E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19178215E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.81068886E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.74277801E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.80278288E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.91849318E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59477116E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.48072369E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18584785E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.23953660E-07   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.87421810E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.36072765E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.36072765E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.12024309E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.12024309E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03767109E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.36662963E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.99046306E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.71247386E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.94616972E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.26254602E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.99428106E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     5.31572449E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     5.33893765E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     4.91628484E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     5.29733000E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     4.34277050E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.02289708E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     3.82561182E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.03256928E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.96207606E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     5.35465963E-04    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     8.26630401E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.50531852E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     3.65730432E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     7.18412486E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     2.01724822E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.27774532E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.65368309E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.27774532E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.65368309E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83795180E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.77308132E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.77308132E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.52636284E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.53251090E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.53251090E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.53251090E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.35979843E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.35979843E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.35979843E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.35979843E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.86599640E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.86599640E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.86599640E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.86599640E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36117488E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36117488E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.63785784E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.83661341E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.19028246E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.99746827E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.99746827E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.79599837E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     4.16446533E-05    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.84037129E-09    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     9.47901602E-10    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     7.38329316E-12    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.36666522E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.03758426E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93143911E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.02336297E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.94063011E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.94063011E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.71034599E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.76473572E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.63738240E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.36368981E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     5.01110803E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.77914245E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     2.22373550E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.48879162E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     4.19400077E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.33257917E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.33257917E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.44983878E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.77834643E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.80589593E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     3.81043970E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.61886559E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21691954E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01613294E-01    2           5        -5   # BR(h -> b       bb     )
     6.22619320E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20380095E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66281408E-04    2           3        -3   # BR(h -> s       sb     )
     2.01154258E-02    2           4        -4   # BR(h -> c       cb     )
     6.64320224E-02    2          21        21   # BR(h -> g       g      )
     2.32825351E-03    2          22        22   # BR(h -> gam     gam    )
     1.62699758E-03    2          22        23   # BR(h -> Z       gam    )
     2.16844879E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80905337E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01488239E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38914748E-03    2           5        -5   # BR(H -> b       bb     )
     2.32088071E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.20517738E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05055072E-06    2           3        -3   # BR(H -> s       sb     )
     9.48135124E-06    2           4        -4   # BR(H -> c       cb     )
     9.38139950E-01    2           6        -6   # BR(H -> t       tb     )
     7.51308433E-04    2          21        21   # BR(H -> g       g      )
     2.62932262E-06    2          22        22   # BR(H -> gam     gam    )
     1.09154494E-06    2          23        22   # BR(H -> Z       gam    )
     3.18175700E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58653870E-04    2          23        23   # BR(H -> Z       Z      )
     8.52342068E-04    2          25        25   # BR(H -> h       h      )
     8.50667357E-24    2          36        36   # BR(H -> A       A      )
     3.38021620E-11    2          23        36   # BR(H -> Z       A      )
     2.50195448E-12    2          24       -37   # BR(H -> W+      H-     )
     2.50195448E-12    2         -24        37   # BR(H -> W-      H+     )
     7.71002365E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     5.52182606E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.36027472E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.64305142E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     6.94750252E-03    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53618659E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.87723810E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05933065E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39211697E-03    2           5        -5   # BR(A -> b       bb     )
     2.29747640E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12240904E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07082237E-06    2           3        -3   # BR(A -> s       sb     )
     9.38381860E-06    2           4        -4   # BR(A -> c       cb     )
     9.39153824E-01    2           6        -6   # BR(A -> t       tb     )
     8.88926937E-04    2          21        21   # BR(A -> g       g      )
     2.69761771E-06    2          22        22   # BR(A -> gam     gam    )
     1.27304437E-06    2          23        22   # BR(A -> Z       gam    )
     3.10067105E-04    2          23        25   # BR(A -> Z       h      )
     5.85781909E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.28938650E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.65390788E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.84519213E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     9.65771786E-05    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.36512410E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.93376849E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97902363E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23372653E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34630162E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29502380E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42018214E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13691638E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02354995E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40802982E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17773417E-04    2          24        25   # BR(H+ -> W+      h      )
     1.33619377E-12    2          24        36   # BR(H+ -> W+      A      )
     5.58026493E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.23543634E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     5.29274411E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

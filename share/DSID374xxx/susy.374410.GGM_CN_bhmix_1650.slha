#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.64200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.65000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05416752E+01   # W+
        25     1.26000000E+02   # h
        35     2.00396202E+03   # H
        36     2.00000000E+03   # A
        37     2.00152668E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.38578352E+03   # ~d_L
   2000001     4.38566092E+03   # ~d_R
   1000002     4.38550939E+03   # ~u_L
   2000002     4.38557413E+03   # ~u_R
   1000003     4.38578352E+03   # ~s_L
   2000003     4.38566092E+03   # ~s_R
   1000004     4.38550939E+03   # ~c_L
   2000004     4.38557413E+03   # ~c_R
   1000005     4.38496478E+03   # ~b_1
   2000005     4.38648110E+03   # ~b_2
   1000006     4.51481757E+03   # ~t_1
   2000006     4.83923474E+03   # ~t_2
   1000011     5.00008216E+03   # ~e_L
   2000011     5.00007612E+03   # ~e_R
   1000012     4.99984171E+03   # ~nu_eL
   1000013     5.00008216E+03   # ~mu_L
   2000013     5.00007612E+03   # ~mu_R
   1000014     4.99984171E+03   # ~nu_muL
   1000015     4.99963923E+03   # ~tau_1
   2000015     5.00051965E+03   # ~tau_2
   1000016     4.99984171E+03   # ~nu_tauL
   1000021     4.51224089E+03   # ~g
   1000022     1.70113075E+03   # ~chi_10
   1000023    -1.75166959E+03   # ~chi_20
   1000025     1.78837722E+03   # ~chi_30
   1000035     3.12643923E+03   # ~chi_40
   1000024     1.74727544E+03   # ~chi_1+
   1000037     3.12643496E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.26864831E-01   # N_11
  1  2    -3.73966748E-02   # N_12
  1  3     4.86322169E-01   # N_13
  1  4    -4.83487078E-01   # N_14
  2  1     2.49841480E-03   # N_21
  2  2    -3.18319574E-03   # N_22
  2  3    -7.07029175E-01   # N_23
  2  4    -7.07172801E-01   # N_24
  3  1     6.86773652E-01   # N_31
  3  2     4.22027068E-02   # N_32
  3  3    -5.12041307E-01   # N_33
  3  4     5.14173690E-01   # N_34
  4  1     1.79629356E-03   # N_41
  4  2    -9.98403870E-01   # N_42
  4  3    -3.76057726E-02   # N_43
  4  4     4.20985948E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.31284027E-02   # U_11
  1  2     9.98587689E-01   # U_12
  2  1     9.98587689E-01   # U_21
  2  2     5.31284027E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.94815168E-02   # V_11
  1  2     9.98229407E-01   # V_12
  2  1     9.98229407E-01   # V_21
  2  2     5.94815168E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99682108E-01   # cos(theta_t)
  1  2    -2.52127536E-02   # sin(theta_t)
  2  1     2.52127536E-02   # -sin(theta_t)
  2  2     9.99682108E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.77920059E-01   # cos(theta_b)
  1  2     7.35135629E-01   # sin(theta_b)
  2  1    -7.35135629E-01   # -sin(theta_b)
  2  2     6.77920059E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04679806E-01   # cos(theta_tau)
  1  2     7.09525455E-01   # sin(theta_tau)
  2  1    -7.09525455E-01   # -sin(theta_tau)
  2  2     7.04679806E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90161053E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.65000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52247162E+02   # vev(Q)              
         4     3.01587586E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.52738467E-01   # gprime(Q) DRbar
     2     6.26894141E-01   # g(Q) DRbar
     3     1.06610938E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02342680E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.70687748E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79767997E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.64200000E+03   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     5.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.36689009E+04   # M^2_Hd              
        22    -7.14490404E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.36923196E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.73199361E+00   # gluino decays
#          BR         NDA      ID1       ID2
     4.98371671E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.98371671E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99324718E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99324718E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.00503879E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.00503879E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.99999927E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.99999927E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.98371671E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.98371671E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99324718E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99324718E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.00503879E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.00503879E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.99999927E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.99999927E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     4.91894626E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     4.91894626E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.05495840E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.05495840E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     1.24182915E-03    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     5.78934333E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.74309781E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.87866821E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.22254023E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     8.22947713E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
    -6.15351965E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.62627498E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
    -1.35256739E-02    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
    -1.52118689E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     1.13344114E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     1.47388003E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.23651148E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.42439780E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.52161966E-04    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     4.46558392E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -5.04021014E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     4.07958813E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
    -2.14446363E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     3.21860948E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     8.08636465E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     9.36944033E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     2.38792034E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.38456851E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.61096570E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.60424418E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     7.14952610E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     7.33349404E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.44706111E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     3.37669610E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.36131043E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.11508610E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.44012896E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.98215278E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     7.60231221E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.41421349E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     1.27147448E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     9.23036441E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.80327059E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.65723106E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.19144682E-01    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.54513165E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.38500708E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     6.63280251E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     5.37597434E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     6.21801520E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.62395232E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.11547450E-06    2     1000035         2   # BR(~u_R -> ~chi_40 u)
#
#         PDG            Width
DECAY   1000001     1.27064098E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.84835056E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.23210525E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.82043787E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.19883569E-01    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.22559056E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.39574576E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
#
#         PDG            Width
DECAY   2000001     1.65825215E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.37596996E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.21801636E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.62395671E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.11554827E-06    2     1000035         1   # BR(~d_R -> ~chi_40 d)
#
#         PDG            Width
DECAY   1000004     1.27147448E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.23036441E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.80327059E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.65723106E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.19144682E-01    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.54513165E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.38500708E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     6.63280251E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.37597434E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     6.21801520E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.62395232E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.11547450E-06    2     1000035         4   # BR(~c_R -> ~chi_40 c)
#
#         PDG            Width
DECAY   1000003     1.27064098E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.84835056E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.23210525E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.82043787E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.19883569E-01    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.22559056E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.39574576E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
#
#         PDG            Width
DECAY   2000003     1.65825215E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.37596996E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.21801636E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.62395671E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.11554827E-06    2     1000035         3   # BR(~s_R -> ~chi_40 s)
#
#         PDG            Width
DECAY   1000011     2.65980402E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     7.93416201E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.78670019E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.02682014E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.71158540E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.19723823E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.43618801E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.91052153E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.35238829E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     6.22423517E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.64753396E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.55066579E-06    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.65980402E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     7.93416201E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.78670019E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.02682014E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.71158540E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.19723823E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.43618801E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.91052153E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.35238829E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     6.22423517E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.64753396E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.55066579E-06    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.29034249E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.76673657E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.57265570E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.50052802E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.55840409E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     4.38739441E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.12431443E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.57028800E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.28776429E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.62753789E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.30633112E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.57727478E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.59242353E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04151395E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.19241596E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.65999745E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.14478637E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19084085E-05    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.62140674E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.72192285E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.00701953E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.43096082E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.65999745E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.14478637E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19084085E-05    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.62140674E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.72192285E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.00701953E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.43096082E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.66247043E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.14372306E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.18973477E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.61525659E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.71939466E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.93002291E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.42593742E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.40708146E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     6.62830796E-02    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.11491628E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.11491628E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.03830610E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.03830610E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.03072446E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.82364341E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.53456951E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.09173101E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.46186523E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.37926205E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.53248260E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     8.95734923E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     1.65974672E-09    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.98111737E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.90826844E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.10614189E-02    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77902330E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.19231786E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     4.05531963E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     2.03396084E-03    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     5.03006813E-02    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.12291578E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.45405804E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.12291578E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.45405804E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.33535929E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.32107022E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.32107022E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.28992430E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.63252148E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.63252148E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.63252148E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.38859073E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.38859073E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.38859073E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.38859073E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.12953032E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.12953032E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.12953032E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.12953032E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.14473128E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.14473128E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     2.07852607E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.91232043E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.91909286E-06    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     3.88529466E-02    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     3.44316252E-02    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.00502605E-03    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.16910094E-05    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.33677330E-05    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.16910094E-05    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.33677330E-05    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     3.67653289E-05    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.29795768E-06    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.29795768E-06    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     5.88365657E-06    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.59453819E-06    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.59453819E-06    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.59453819E-06    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.99008130E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.57695016E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.99008130E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.57695016E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.20291915E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.88576244E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.88576244E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.79676529E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.17544204E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.17544204E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.17544204E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.26628767E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.26628767E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.26628767E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.26628767E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.22094989E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.22094989E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.22094989E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.22094989E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.18198340E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.18198340E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     3.82362362E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.70206379E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.52203201E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.33159439E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.46627552E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.46627552E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.11731427E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     7.85436456E-04    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.41212508E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.14215025E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     6.81409305E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     8.82005387E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     1.61949606E-09    2     1000039        35   # BR(~chi_40 -> ~G        H)
     3.45160536E-11    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22538680E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02540654E-01    2           5        -5   # BR(h -> b       bb     )
     6.21276498E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.19904795E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65277102E-04    2           3        -3   # BR(h -> s       sb     )
     2.00764959E-02    2           4        -4   # BR(h -> c       cb     )
     6.63237493E-02    2          21        21   # BR(h -> g       g      )
     2.30257392E-03    2          22        22   # BR(h -> gam     gam    )
     1.62395931E-03    2          22        23   # BR(h -> Z       gam    )
     2.16285487E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80342493E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78035609E+01   # H decays
#          BR         NDA      ID1       ID2
     1.46046484E-03    2           5        -5   # BR(H -> b       bb     )
     2.46477992E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.71391466E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11570432E-06    2           3        -3   # BR(H -> s       sb     )
     1.00671234E-05    2           4        -4   # BR(H -> c       cb     )
     9.96093967E-01    2           6        -6   # BR(H -> t       tb     )
     7.97621915E-04    2          21        21   # BR(H -> g       g      )
     2.71545964E-06    2          22        22   # BR(H -> gam     gam    )
     1.16041241E-06    2          23        22   # BR(H -> Z       gam    )
     3.22309134E-04    2          24       -24   # BR(H -> W+      W-     )
     1.60715013E-04    2          23        23   # BR(H -> Z       Z      )
     9.02513505E-04    2          25        25   # BR(H -> h       h      )
     6.91830127E-24    2          36        36   # BR(H -> A       A      )
     2.78675983E-11    2          23        36   # BR(H -> Z       A      )
     5.95782115E-12    2          24       -37   # BR(H -> W+      H-     )
     5.95782115E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82373562E+01   # A decays
#          BR         NDA      ID1       ID2
     1.46318013E-03    2           5        -5   # BR(A -> b       bb     )
     2.43903275E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62286185E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13679985E-06    2           3        -3   # BR(A -> s       sb     )
     9.96199169E-06    2           4        -4   # BR(A -> c       cb     )
     9.97018697E-01    2           6        -6   # BR(A -> t       tb     )
     9.43697138E-04    2          21        21   # BR(A -> g       g      )
     3.14158087E-06    2          22        22   # BR(A -> gam     gam    )
     1.35282678E-06    2          23        22   # BR(A -> Z       gam    )
     3.14067449E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74465520E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.34073967E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49244189E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81168245E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48808455E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45708115E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08734411E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99414701E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.21890436E-04    2          24        25   # BR(H+ -> W+      h      )
     2.91259242E-13    2          24        36   # BR(H+ -> W+      A      )

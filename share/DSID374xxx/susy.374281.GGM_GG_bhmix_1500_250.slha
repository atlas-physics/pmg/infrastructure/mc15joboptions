#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375320E+01   # W+
        25     1.26000000E+02   # h
        35     2.00416601E+03   # H
        36     2.00000000E+03   # A
        37     2.00208394E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.98868361E+03   # ~d_L
   2000001     4.98857373E+03   # ~d_R
   1000002     4.98843806E+03   # ~u_L
   2000002     4.98849636E+03   # ~u_R
   1000003     4.98868361E+03   # ~s_L
   2000003     4.98857373E+03   # ~s_R
   1000004     4.98843806E+03   # ~c_L
   2000004     4.98849636E+03   # ~c_R
   1000005     4.98851295E+03   # ~b_1
   2000005     4.98874586E+03   # ~b_2
   1000006     5.11933437E+03   # ~t_1
   2000006     5.41796610E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001287E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     1.61387438E+03   # ~g
   1000022     2.38930157E+02   # ~chi_10
   1000023    -2.70414087E+02   # ~chi_20
   1000025     3.33142650E+02   # ~chi_30
   1000035     3.12901947E+03   # ~chi_40
   1000024     2.68170825E+02   # ~chi_1+
   1000037     3.12901904E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56361207E-01   # N_11
  1  2    -2.22769218E-02   # N_12
  1  3     5.94524372E-01   # N_13
  1  4    -5.80091990E-01   # N_14
  2  1     1.53222220E-02   # N_21
  2  2    -4.61396452E-03   # N_22
  2  3    -7.05672886E-01   # N_23
  2  4    -7.08357056E-01   # N_24
  3  1     8.30799163E-01   # N_31
  3  2     1.55243453E-02   # N_32
  3  3    -3.85112212E-01   # N_33
  3  4     4.01522515E-01   # N_34
  4  1     4.33066123E-04   # N_41
  4  2    -9.99620650E-01   # N_42
  4  3    -1.59728975E-02   # N_43
  4  4     2.24328823E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25814442E-02   # U_11
  1  2     9.99745007E-01   # U_12
  2  1     9.99745007E-01   # U_21
  2  2     2.25814442E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17206948E-02   # V_11
  1  2     9.99496772E-01   # V_12
  2  1     9.99496772E-01   # V_21
  2  2     3.17206948E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99832048E-01   # cos(theta_t)
  1  2    -1.83269144E-02   # sin(theta_t)
  2  1     1.83269144E-02   # -sin(theta_t)
  2  2     9.99832048E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13931692E-01   # cos(theta_b)
  1  2     8.57831112E-01   # sin(theta_b)
  2  1    -8.57831112E-01   # -sin(theta_b)
  2  2     5.13931692E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89410337E-01   # cos(theta_tau)
  1  2     7.24371029E-01   # sin(theta_tau)
  2  1    -7.24371029E-01   # -sin(theta_tau)
  2  2     6.89410337E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90207124E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51793729E+02   # vev(Q)              
         4     3.84342441E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53061979E-01   # gprime(Q) DRbar
     2     6.28967068E-01   # g(Q) DRbar
     3     1.08507312E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02673980E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72311852E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79956101E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     1.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99432098E+06   # M^2_Hd              
        22    -5.53059503E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37847961E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     9.98584947E-04   # gluino decays
#          BR         NDA      ID1       ID2
     9.00523318E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.30567712E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.83961918E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.93567881E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.55033366E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.21107507E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     8.91322879E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     3.86332007E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.86453377E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.93567881E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.55033366E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.21107507E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     8.91322879E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     3.86332007E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.86453377E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     3.07398629E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.18522110E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.28506829E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.34994342E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.94270728E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.20862942E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.24174535E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     7.24174535E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     7.24174535E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     7.24174535E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34916831E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.34916831E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     3.17514171E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     7.64588670E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.15778044E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.05747141E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.42291839E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     1.53006472E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     4.83051948E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.76858536E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     4.68893709E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.29535165E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.97503316E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     7.63709392E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     9.84126175E-02    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.00254254E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -1.38339838E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.96378347E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -2.76417249E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.78556552E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -3.41000348E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     4.73601794E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     7.51407978E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     2.09193848E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.92884907E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.03023580E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.13038624E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     9.77983738E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -3.40992451E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -8.82261919E-02    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -6.85457648E-03    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.08356758E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     3.21090587E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.74250462E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.44380660E-05    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     9.71058502E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.38496411E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.75645374E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     4.78544429E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.51010795E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     2.30708353E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     5.47323520E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.49182345E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.37973936E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.03249975E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     1.64320385E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.06448170E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     9.05938553E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     2.19545590E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.48954417E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.12835642E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.30760795E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.35111604E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.52017192E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     2.30712349E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.30567522E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.56942704E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.59396748E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.03396409E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.32767477E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.06845629E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     9.05988307E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.11649579E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.86284742E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.92617632E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.57764862E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.69082183E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.87556577E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     2.30708353E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.47323520E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.49182345E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.37973936E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.03249975E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     1.64320385E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.06448170E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     9.05938553E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     2.19545590E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.48954417E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.12835642E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.30760795E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.35111604E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.52017192E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     2.30712349E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.30567522E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.56942704E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.59396748E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.03396409E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.32767477E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.06845629E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     9.05988307E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.11649579E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.86284742E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.92617632E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.57764862E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.69082183E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.87556577E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80275481E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87820239E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10939473E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61571582E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59513846E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11834997E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409619E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125659E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10462966E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35169519E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89301795E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99426400E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80275481E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87820239E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10939473E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61571582E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59513846E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11834997E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409619E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125659E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10462966E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35169519E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89301795E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99426400E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63122760E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88556947E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46977228E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15110833E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31199006E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95802763E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591473E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36736230E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64274981E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64604896E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92657193E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00375154E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599541E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67402250E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89411011E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80261547E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.82293224E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21879541E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41421627E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734534E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40462920E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088008E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80261547E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.82293224E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21879541E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41421627E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734534E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40462920E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088008E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80581726E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81400529E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21740461E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41260247E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438145E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54379923E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496016E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.52092085E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64685282E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33996819E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33996819E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332372E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332372E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09305150E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41062867E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06993242E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33249411E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02062013E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85388533E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07666406E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52681106E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.55019650E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25250174E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51941371E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29907479E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00955925E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08129814E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00700591E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92398946E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.90046263E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77478538E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.19011397E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15621423E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14158530E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69635894E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20449353E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55906749E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20449353E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55906749E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25917595E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55803821E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55803821E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48793480E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10385081E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10385081E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10385081E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18454891E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18454891E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18454891E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18454891E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94849702E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94849702E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94849702E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94849702E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.18768249E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.18768249E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.43717666E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99749320E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.76122862E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.32965391E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.26054666E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41066590E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39828770E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01541723E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20438320E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01742827E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01742827E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33819435E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26886589E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02311647E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.68868297E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20896237E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02337840E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83737926E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74579617E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22095964E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54403689E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54403689E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42031478E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67446281E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.77967479E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06213129E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44139180E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21699157E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01627343E-01    2           5        -5   # BR(h -> b       bb     )
     6.22598982E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20372896E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66266313E-04    2           3        -3   # BR(h -> s       sb     )
     2.01152229E-02    2           4        -4   # BR(h -> c       cb     )
     6.64292092E-02    2          21        21   # BR(h -> g       g      )
     2.31878303E-03    2          22        22   # BR(h -> gam     gam    )
     1.62696048E-03    2          22        23   # BR(h -> Z       gam    )
     2.16845890E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80900546E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01151459E+01   # H decays
#          BR         NDA      ID1       ID2
     1.39013624E-03    2           5        -5   # BR(H -> b       bb     )
     2.32284314E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21211530E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05143930E-06    2           3        -3   # BR(H -> s       sb     )
     9.48915525E-06    2           4        -4   # BR(H -> c       cb     )
     9.38912074E-01    2           6        -6   # BR(H -> t       tb     )
     7.51949461E-04    2          21        21   # BR(H -> g       g      )
     2.63841634E-06    2          22        22   # BR(H -> gam     gam    )
     1.09242196E-06    2          23        22   # BR(H -> Z       gam    )
     3.16939135E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58037268E-04    2          23        23   # BR(H -> Z       Z      )
     8.53023859E-04    2          25        25   # BR(H -> h       h      )
     8.37454312E-24    2          36        36   # BR(H -> A       A      )
     3.37509730E-11    2          23        36   # BR(H -> Z       A      )
     2.56474533E-12    2          24       -37   # BR(H -> W+      H-     )
     2.56474533E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23233786E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15131352E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93518184E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.01962724E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96747564E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.32547842E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54782075E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05621951E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39300029E-03    2           5        -5   # BR(A -> b       bb     )
     2.29923858E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12863898E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07164370E-06    2           3        -3   # BR(A -> s       sb     )
     9.39101605E-06    2           4        -4   # BR(A -> c       cb     )
     9.39874161E-01    2           6        -6   # BR(A -> t       tb     )
     8.89608749E-04    2          21        21   # BR(A -> g       g      )
     2.66246115E-06    2          22        22   # BR(A -> gam     gam    )
     1.27397311E-06    2          23        22   # BR(A -> Z       gam    )
     3.08840572E-04    2          23        25   # BR(A -> Z       h      )
     6.12186144E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74267327E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47858469E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11542848E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99412340E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.13484232E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45931710E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97574468E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23513783E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34822274E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30181564E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42107766E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14112654E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02438884E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41573696E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.16528953E-04    2          24        25   # BR(H+ -> W+      h      )
     1.29973508E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79074196E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30727885E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98234300E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

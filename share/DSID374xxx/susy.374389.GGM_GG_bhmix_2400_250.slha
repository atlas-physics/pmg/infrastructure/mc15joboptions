#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05375314E+01   # W+
        25     1.26000000E+02   # h
        35     2.00417616E+03   # H
        36     2.00000000E+03   # A
        37     2.00207956E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.89730148E+03   # ~d_L
   2000001     4.89719034E+03   # ~d_R
   1000002     4.89705311E+03   # ~u_L
   2000002     4.89711208E+03   # ~u_R
   1000003     4.89730148E+03   # ~s_L
   2000003     4.89719034E+03   # ~s_R
   1000004     4.89705311E+03   # ~c_L
   2000004     4.89711208E+03   # ~c_R
   1000005     4.89712886E+03   # ~b_1
   2000005     4.89736445E+03   # ~b_2
   1000006     5.03044848E+03   # ~t_1
   2000006     5.33441866E+03   # ~t_2
   1000011     5.00008259E+03   # ~e_L
   2000011     5.00007599E+03   # ~e_R
   1000012     4.99984142E+03   # ~nu_eL
   1000013     5.00008259E+03   # ~mu_L
   2000013     5.00007599E+03   # ~mu_R
   1000014     4.99984142E+03   # ~nu_muL
   1000015     5.00001288E+03   # ~tau_1
   2000015     5.00014633E+03   # ~tau_2
   1000016     4.99984142E+03   # ~nu_tauL
   1000021     2.41237971E+03   # ~g
   1000022     2.38942174E+02   # ~chi_10
   1000023    -2.70432396E+02   # ~chi_20
   1000025     3.33148874E+02   # ~chi_30
   1000035     3.12901966E+03   # ~chi_40
   1000024     2.68189060E+02   # ~chi_1+
   1000037     3.12901923E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.56437586E-01   # N_11
  1  2    -2.22759597E-02   # N_12
  1  3     5.94488779E-01   # N_13
  1  4    -5.80055244E-01   # N_14
  2  1     1.53219420E-02   # N_21
  2  2    -4.61400430E-03   # N_22
  2  3    -7.05672973E-01   # N_23
  2  4    -7.08356975E-01   # N_24
  3  1     8.30748014E-01   # N_31
  3  2     1.55267222E-02   # N_32
  3  3    -3.85166979E-01   # N_33
  3  4     4.01575716E-01   # N_34
  4  1     4.33081156E-04   # N_41
  4  2    -9.99620635E-01   # N_42
  4  3    -1.59732703E-02   # N_43
  4  4     2.24333144E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.25819711E-02   # U_11
  1  2     9.99744995E-01   # U_12
  2  1     9.99744995E-01   # U_21
  2  2     2.25819711E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.17213056E-02   # V_11
  1  2     9.99496753E-01   # V_12
  2  1     9.99496753E-01   # V_21
  2  2     3.17213056E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99643048E-01   # cos(theta_t)
  1  2    -2.67165976E-02   # sin(theta_t)
  2  1     2.67165976E-02   # -sin(theta_t)
  2  2     9.99643048E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13925806E-01   # cos(theta_b)
  1  2     8.57834638E-01   # sin(theta_b)
  2  1    -8.57834638E-01   # -sin(theta_b)
  2  2     5.13925806E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.89409545E-01   # cos(theta_tau)
  1  2     7.24371782E-01   # sin(theta_tau)
  2  1    -7.24371782E-01   # -sin(theta_tau)
  2  2     6.89409545E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.90211155E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51797266E+02   # vev(Q)              
         4     3.83340951E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53062036E-01   # gprime(Q) DRbar
     2     6.28967434E-01   # g(Q) DRbar
     3     1.07754468E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.02724003E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.72307036E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79953499E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.87000000E+02   # M_1                 
         2     3.00000000E+03   # M_2                 
         3     2.40000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.99419597E+06   # M^2_Hd              
        22    -5.40990607E+06   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     5.00000000E+03   # M_q1L               
        42     5.00000000E+03   # M_q2L               
        43     5.00000000E+03   # M_q3L               
        44     5.00000000E+03   # M_uR                
        45     5.00000000E+03   # M_cR                
        46     5.00000000E+03   # M_tR                
        47     5.00000000E+03   # M_dR                
        48     5.00000000E+03   # M_sR                
        49     5.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.37848124E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.05137977E-02   # gluino decays
#          BR         NDA      ID1       ID2
     3.43824964E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.97200429E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.45366378E-03    2     1000025        21   # BR(~g -> ~chi_30 g)
#          BR         NDA      ID1       ID2
0.0E+00    2     1000039    21 # BR(~g -> ~G      g)
#           BR         NDA      ID1       ID2       ID3
     2.58990717E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     2.68746642E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.95944687E-03    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.86364035E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.07103370E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.77453445E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     2.58990717E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     2.68746642E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.95944687E-03    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.86364035E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.07103370E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.77453445E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     2.72628089E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.09204565E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     5.02851877E-03    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.40735504E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.09651059E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.60036843E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     6.53706297E-05    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     6.53706297E-05    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     6.53706297E-05    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     6.53706297E-05    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.35121806E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.35121806E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000006     2.58811085E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.09758798E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.39102161E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.96821139E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.79991073E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     3.35263346E-04    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     5.58371139E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     6.11613355E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
     6.49790713E-03    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     1.79570991E-02    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.42127120E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.80259309E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12568087E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.63683831E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
    -2.63792822E-05    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     2.24442878E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -5.30149795E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     5.17856563E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
    -6.22069026E-05    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.26352681E-04    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.99112277E-04    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     5.54294102E-04    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.19515107E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.23775445E-03    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.90995314E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.72997785E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
    -1.25820952E-02    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
    -2.53081375E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
    -2.52735640E-02    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.26520851E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     2.75879373E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.18762590E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.01957099E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.83866475E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.91693096E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.63077925E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.85165539E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.48331626E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     1.71543427E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     7.18946428E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.27232969E-07    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.12451856E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.81215445E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.15775157E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     7.62365811E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.81582307E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.61518985E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     1.97731274E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.49739217E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.38897299E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.25796696E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.36322165E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.71549818E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.71478523E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.99992456E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.09260746E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.81397107E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.09352718E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     7.62860617E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.81651482E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53810658E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.19110327E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.93115238E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.15225147E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.11790620E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.83282450E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.71543427E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.18946428E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.27232969E-07    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.12451856E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.81215445E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.15775157E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     7.62365811E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.81582307E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.61518985E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.97731274E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.49739217E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.38897299E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.25796696E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.36322165E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.71549818E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.71478523E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.99992456E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.09260746E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.81397107E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.09352718E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     7.62860617E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.81651482E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53810658E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.19110327E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.93115238E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.15225147E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.11790620E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.83282450E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.80275732E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     5.87997300E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.10928253E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.61553781E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.59513863E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     7.11867849E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.19409666E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.46125737E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.10548073E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35160738E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.89216696E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.99474854E-08    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     2.80275732E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     5.87997300E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.10928253E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.61553781E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.59513863E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     7.11867849E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.19409666E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.46125737E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.10548073E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35160738E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.89216696E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.99474854E-08    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.63122872E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.88608166E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.46961243E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.15060314E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.31198774E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.95803348E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.62591015E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     1.36736173E-04    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     2.64275171E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.64651763E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.92627357E-04    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.00327305E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.44599879E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.67330693E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89411693E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.80261799E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.82488461E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.21877244E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.41401991E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.59734559E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     1.40468257E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.19088044E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.80261799E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.82488461E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.21877244E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.41401991E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.59734559E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     1.40468257E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.19088044E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     2.80581968E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.81595570E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.21738171E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41240638E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.59438178E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     2.54381767E-03    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.18496071E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.52237326E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.64476117E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.33996543E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33996543E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11332279E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11332279E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09305908E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.41070235E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.06996909E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.33234212E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.02065621E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.85576912E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     2.07664132E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     4.52651683E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     4.54998418E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     3.25199763E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     4.51923571E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.29939955E-02    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     1.00953765E-05    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08143701E-10    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022    1.0E-05    # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.00766193E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.92334604E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.89920287E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.77637110E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.18817131E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.15551878E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.14103023E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.69567575E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20448283E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55905339E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20448283E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55905339E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.25927204E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55800565E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55800565E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48792643E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.10378545E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.10378545E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.10378545E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18369002E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18369002E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18369002E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18369002E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94563404E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94563404E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94563404E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94563404E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     8.18507439E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     8.18507439E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.43378829E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99748719E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.76534549E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     7.34821616E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.26391877E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.41073957E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.39777401E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.01545418E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.20487346E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.01746445E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.01746445E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.33800602E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.26848007E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.02261620E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.68825114E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.20868706E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.02335774E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     6.83907884E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     7.74774792E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.22124970E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     4.54385832E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     4.54385832E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.42026148E-06    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     7.67430097E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     4.77961978E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.06211761E-10    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.44203834E-13    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21728633E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01652334E-01    2           5        -5   # BR(h -> b       bb     )
     6.22562959E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20360146E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.66239230E-04    2           3        -3   # BR(h -> s       sb     )
     2.01137084E-02    2           4        -4   # BR(h -> c       cb     )
     6.64267761E-02    2          21        21   # BR(h -> g       g      )
     2.31860009E-03    2          22        22   # BR(h -> gam     gam    )
     1.62684726E-03    2          22        23   # BR(h -> Z       gam    )
     2.16830748E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80880908E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01158352E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38973287E-03    2           5        -5   # BR(H -> b       bb     )
     2.32280245E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21197143E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05142003E-06    2           3        -3   # BR(H -> s       sb     )
     9.48914781E-06    2           4        -4   # BR(H -> c       cb     )
     9.38911601E-01    2           6        -6   # BR(H -> t       tb     )
     7.51921036E-04    2          21        21   # BR(H -> g       g      )
     2.63831067E-06    2          22        22   # BR(H -> gam     gam    )
     1.09240298E-06    2          23        22   # BR(H -> Z       gam    )
     3.18098681E-04    2          24       -24   # BR(H -> W+      W-     )
     1.58615464E-04    2          23        23   # BR(H -> Z       Z      )
     8.52999015E-04    2          25        25   # BR(H -> h       h      )
     8.55950027E-24    2          36        36   # BR(H -> A       A      )
     3.41634848E-11    2          23        36   # BR(H -> Z       A      )
     2.65543566E-12    2          24       -37   # BR(H -> W+      H-     )
     2.65543566E-12    2         -24        37   # BR(H -> W-      H+     )
     7.23221552E-05    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.15141032E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.93493098E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.02039577E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.96794364E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.32249799E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.54728515E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05622211E+01   # A decays
#          BR         NDA      ID1       ID2
     1.39262440E-03    2           5        -5   # BR(A -> b       bb     )
     2.29923711E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12863376E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07164301E-06    2           3        -3   # BR(A -> s       sb     )
     9.39101001E-06    2           4        -4   # BR(A -> c       cb     )
     9.39873557E-01    2           6        -6   # BR(A -> t       tb     )
     8.89608177E-04    2          21        21   # BR(A -> g       g      )
     2.66245188E-06    2          22        22   # BR(A -> gam     gam    )
     1.27397210E-06    2          23        22   # BR(A -> Z       gam    )
     3.09970877E-04    2          23        25   # BR(A -> Z       h      )
     6.12207681E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.74301097E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.47853058E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.11571349E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.99570458E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.12846018E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.45914269E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97573891E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.23431597E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34822101E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30180953E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.42055168E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14112428E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02438839E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41572735E-01    2           6        -5   # BR(H+ -> t       bb     )
     3.17685952E-04    2          24        25   # BR(H+ -> W+      h      )
     1.28613680E-12    2          24        36   # BR(H+ -> W+      A      )
     1.79122733E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.30732859E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.98183766E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980604E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03971444E+01   # W+
        25     1.25372274E+02   # h
        35     3.00008620E+03   # H
        36     2.99999991E+03   # A
        37     3.00109575E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03525969E+03   # ~d_L
   2000001     3.02998514E+03   # ~d_R
   1000002     3.03434592E+03   # ~u_L
   2000002     3.03192357E+03   # ~u_R
   1000003     3.03525969E+03   # ~s_L
   2000003     3.02998514E+03   # ~s_R
   1000004     3.03434592E+03   # ~c_L
   2000004     3.03192357E+03   # ~c_R
   1000005     8.43283615E+02   # ~b_1
   2000005     3.02866803E+03   # ~b_2
   1000006     8.40861273E+02   # ~t_1
   2000006     3.02281872E+03   # ~t_2
   1000011     3.00673935E+03   # ~e_L
   2000011     3.00120883E+03   # ~e_R
   1000012     3.00534563E+03   # ~nu_eL
   1000013     3.00673935E+03   # ~mu_L
   2000013     3.00120883E+03   # ~mu_R
   1000014     3.00534563E+03   # ~nu_muL
   1000015     2.98611277E+03   # ~tau_1
   2000015     3.02195196E+03   # ~tau_2
   1000016     3.00540816E+03   # ~nu_tauL
   1000021     2.34353356E+03   # ~g
   1000022     1.51311472E+02   # ~chi_10
   1000023     3.22550535E+02   # ~chi_20
   1000025    -2.99921483E+03   # ~chi_30
   1000035     2.99922095E+03   # ~chi_40
   1000024     3.22712473E+02   # ~chi_1+
   1000037     3.00015257E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891853E-01   # N_11
  1  2     3.81804180E-04   # N_12
  1  3    -1.47015762E-02   # N_13
  1  4     3.31790938E-07   # N_14
  2  1     1.66903088E-06   # N_21
  2  2     9.99659132E-01   # N_22
  2  3     2.60749810E-02   # N_23
  2  4     1.30942096E-03   # N_24
  3  1     1.03988263E-02   # N_31
  3  2    -1.93613756E-02   # N_32
  3  3     7.06763947E-01   # N_33
  3  4     7.07108001E-01   # N_34
  4  1    -1.03993522E-02   # N_41
  4  2     1.75102994E-02   # N_42
  4  3    -7.06815875E-01   # N_43
  4  4     7.07104349E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99320329E-01   # U_11
  1  2     3.68630034E-02   # U_12
  2  1    -3.68630034E-02   # U_21
  2  2     9.99320329E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99998289E-01   # V_11
  1  2    -1.84991916E-03   # V_12
  2  1    -1.84991916E-03   # V_21
  2  2    -9.99998289E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98558934E-01   # cos(theta_t)
  1  2    -5.36661470E-02   # sin(theta_t)
  2  1     5.36661470E-02   # -sin(theta_t)
  2  2     9.98558934E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99713862E-01   # cos(theta_b)
  1  2    -2.39205795E-02   # sin(theta_b)
  2  1     2.39205795E-02   # -sin(theta_b)
  2  2     9.99713862E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06924238E-01   # cos(theta_tau)
  1  2     7.07289277E-01   # sin(theta_tau)
  2  1    -7.07289277E-01   # -sin(theta_tau)
  2  2    -7.06924238E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00149207E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49806044E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44069132E+02   # higgs               
         4     1.05905303E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49806044E+03  # The gauge couplings
     1     3.62146725E-01   # gprime(Q) DRbar
     2     6.38803636E-01   # g(Q) DRbar
     3     1.02637371E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49806044E+03  # The trilinear couplings
  1  1     2.13207084E-06   # A_u(Q) DRbar
  2  2     2.13210398E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49806044E+03  # The trilinear couplings
  1  1     7.47169732E-07   # A_d(Q) DRbar
  2  2     7.47260723E-07   # A_s(Q) DRbar
  3  3     1.55494456E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49806044E+03  # The trilinear couplings
  1  1     3.43952101E-07   # A_e(Q) DRbar
  2  2     3.43969280E-07   # A_mu(Q) DRbar
  3  3     3.48851202E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49806044E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53076130E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49806044E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.90526228E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49806044E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02875044E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49806044E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.14099615E+04   # M^2_Hd              
        22    -9.06910213E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41661546E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.19444781E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47994510E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47994510E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52005490E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52005490E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.51867268E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.46348059E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.04904650E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.80460544E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16650664E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.65096041E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.75953535E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.55084543E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     8.99050943E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.93656955E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69198376E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61832843E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.17881279E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.31866769E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.63240075E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.52228931E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.31447061E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.49036112E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.02387048E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.04698376E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.36184102E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.30184771E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.54702216E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.36119658E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.40396023E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.05670924E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.53864283E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.01992061E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.95761251E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63376194E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.10572528E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.47908041E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.26966816E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.66424715E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.03699321E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18233093E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59218209E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.36382363E-13    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.26978305E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.26734900E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40781773E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02496005E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.90725982E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63311495E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.90138505E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.08615152E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26394732E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.78258791E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.04386146E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66720613E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53656949E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.24334745E-13    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.34840111E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.34773037E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54634300E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.01992061E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.95761251E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63376194E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.10572528E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.47908041E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.26966816E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.66424715E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.03699321E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18233093E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59218209E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.36382363E-13    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.26978305E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.26734900E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40781773E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02496005E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.90725982E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63311495E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.90138505E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.08615152E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26394732E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.78258791E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.04386146E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66720613E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53656949E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.24334745E-13    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.34840111E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.34773037E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54634300E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96425432E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.85580139E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00621544E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.39506242E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.03614941E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00820424E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.60266752E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55783300E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.73616474E-12    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.91823806E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.90667792E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96425432E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.85580139E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00621544E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.39506242E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.03614941E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00820424E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.60266752E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55783300E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.73616474E-12    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.91823806E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.90667792E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.78869120E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.49187075E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17059131E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33753794E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73143367E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.57298828E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14343368E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.18147287E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.00470426E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28324301E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.16403885E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96451367E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.82403839E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00454911E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.26068444E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79473239E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01304699E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.51097259E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96451367E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.82403839E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00454911E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.26068444E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79473239E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01304699E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.51097259E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96493472E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.82320159E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00429547E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.32704543E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.85166793E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01337722E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.09412745E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.41683622E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.22440392E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.39140672E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.54395343E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.53148992E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.20192693E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.97428392E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.04211979E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.42802252E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.30726797E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.23547313E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     2.76452687E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.18518031E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.01474808E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.80094972E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.06989551E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.06989551E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.57719861E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.33727659E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.35285298E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.35285298E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.89617487E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.89617487E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.26651326E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.26651326E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.31047982E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.00171731E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.38350461E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.98749203E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.98749203E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.26543608E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.34208168E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.22113287E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.22113287E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.80476964E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.80476964E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.98097068E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     3.98097068E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.67438332E-03   # h decays
#          BR         NDA      ID1       ID2
     5.58414707E-01    2           5        -5   # BR(h -> b       bb     )
     7.07814875E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.50566767E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.31522943E-04    2           3        -3   # BR(h -> s       sb     )
     2.30955397E-02    2           4        -4   # BR(h -> c       cb     )
     7.54274647E-02    2          21        21   # BR(h -> g       g      )
     2.59259499E-03    2          22        22   # BR(h -> gam     gam    )
     1.75775849E-03    2          22        23   # BR(h -> Z       gam    )
     2.37310790E-01    2          24       -24   # BR(h -> W+      W-     )
     2.98375679E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.72513763E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00695022E-01    2           5        -5   # BR(H -> b       bb     )
     6.67564036E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.36034691E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.89851299E-04    2           3        -3   # BR(H -> s       sb     )
     8.12454863E-08    2           4        -4   # BR(H -> c       cb     )
     8.10475779E-03    2           6        -6   # BR(H -> t       tb     )
     8.52976161E-06    2          21        21   # BR(H -> g       g      )
     5.02505103E-08    2          22        22   # BR(H -> gam     gam    )
     3.31679200E-09    2          23        22   # BR(H -> Z       gam    )
     7.56758466E-07    2          24       -24   # BR(H -> W+      W-     )
     3.77912538E-07    2          23        23   # BR(H -> Z       Z      )
     5.45830601E-06    2          25        25   # BR(H -> h       h      )
     7.50935171E-25    2          36        36   # BR(H -> A       A      )
     1.76974599E-19    2          23        36   # BR(H -> Z       A      )
     8.67004787E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.14855464E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.33538144E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.69369380E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.22859073E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.36765373E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.64259878E+01   # A decays
#          BR         NDA      ID1       ID2
     9.21124670E-01    2           5        -5   # BR(A -> b       bb     )
     6.82675773E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.41377502E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.96461695E-04    2           3        -3   # BR(A -> s       sb     )
     8.36572883E-08    2           4        -4   # BR(A -> c       cb     )
     8.34075841E-03    2           6        -6   # BR(A -> t       tb     )
     2.45627357E-05    2          21        21   # BR(A -> g       g      )
     6.23673626E-08    2          22        22   # BR(A -> gam     gam    )
     2.41799627E-08    2          23        22   # BR(A -> Z       gam    )
     7.70963944E-07    2          23        25   # BR(A -> Z       h      )
     9.20373333E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.28608912E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.60195956E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.80220561E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.97798606E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46883970E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.25347160E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21107503E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.40056252E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.29939199E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.67326717E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24186380E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.07180015E-07    2          24        25   # BR(H+ -> W+      h      )
     5.22690412E-14    2          24        36   # BR(H+ -> W+      A      )
     5.09381909E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     5.69360421E-15    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07891466E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

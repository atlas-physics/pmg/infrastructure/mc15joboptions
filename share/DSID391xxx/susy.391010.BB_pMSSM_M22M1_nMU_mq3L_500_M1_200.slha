#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12203482E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04003789E+01   # W+
        25     1.26119152E+02   # h
        35     3.00018426E+03   # H
        36     3.00000000E+03   # A
        37     3.00110120E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02343157E+03   # ~d_L
   2000001     3.01805193E+03   # ~d_R
   1000002     3.02251600E+03   # ~u_L
   2000002     3.02125082E+03   # ~u_R
   1000003     3.02343157E+03   # ~s_L
   2000003     3.01805193E+03   # ~s_R
   1000004     3.02251600E+03   # ~c_L
   2000004     3.02125082E+03   # ~c_R
   1000005     5.76498480E+02   # ~b_1
   2000005     3.01924489E+03   # ~b_2
   1000006     5.72540984E+02   # ~t_1
   2000006     3.01996188E+03   # ~t_2
   1000011     3.00723804E+03   # ~e_L
   2000011     2.99991903E+03   # ~e_R
   1000012     3.00584791E+03   # ~nu_eL
   1000013     3.00723804E+03   # ~mu_L
   2000013     2.99991903E+03   # ~mu_R
   1000014     3.00584791E+03   # ~nu_muL
   1000015     2.98629362E+03   # ~tau_1
   2000015     3.02227153E+03   # ~tau_2
   1000016     3.00633664E+03   # ~nu_tauL
   1000021     2.32897257E+03   # ~g
   1000022     2.02404320E+02   # ~chi_10
   1000023     4.25750630E+02   # ~chi_20
   1000025    -3.00655017E+03   # ~chi_30
   1000035     3.00662078E+03   # ~chi_40
   1000024     4.25911886E+02   # ~chi_1+
   1000037     3.00751939E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891605E-01   # N_11
  1  2     1.90912133E-04   # N_12
  1  3    -1.47201088E-02   # N_13
  1  4    -2.45769552E-04   # N_14
  2  1     1.95978138E-04   # N_21
  2  2     9.99653221E-01   # N_22
  2  3     2.62404419E-02   # N_23
  2  4     2.19988350E-03   # N_24
  3  1    -1.02351397E-02   # N_31
  3  2     1.69997986E-02   # N_32
  3  3    -7.06827005E-01   # N_33
  3  4     7.07108078E-01   # N_34
  4  1     1.05821529E-02   # N_41
  4  2    -2.01099251E-02   # N_42
  4  3     7.06746307E-01   # N_43
  4  4     7.07102020E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99311526E-01   # U_11
  1  2     3.71008560E-02   # U_12
  2  1    -3.71008560E-02   # U_21
  2  2     9.99311526E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995167E-01   # V_11
  1  2    -3.10911073E-03   # V_12
  2  1    -3.10911073E-03   # V_21
  2  2    -9.99995167E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98640072E-01   # cos(theta_t)
  1  2    -5.21345048E-02   # sin(theta_t)
  2  1     5.21345048E-02   # -sin(theta_t)
  2  2     9.98640072E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99721827E-01   # cos(theta_b)
  1  2    -2.35853476E-02   # sin(theta_b)
  2  1     2.35853476E-02   # -sin(theta_b)
  2  2     9.99721827E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06919718E-01   # cos(theta_tau)
  1  2     7.07293795E-01   # sin(theta_tau)
  2  1    -7.07293795E-01   # -sin(theta_tau)
  2  2    -7.06919718E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00147047E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.22034819E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44484740E+02   # higgs               
         4     1.15255966E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.22034819E+03  # The gauge couplings
     1     3.61462004E-01   # gprime(Q) DRbar
     2     6.37869550E-01   # g(Q) DRbar
     3     1.03095842E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.22034819E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37935478E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.22034819E+03  # The trilinear couplings
  1  1     4.91367957E-07   # A_d(Q) DRbar
  2  2     4.91426437E-07   # A_s(Q) DRbar
  3  3     1.01551338E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.22034819E+03  # The trilinear couplings
  1  1     2.28079765E-07   # A_e(Q) DRbar
  2  2     2.28091038E-07   # A_mu(Q) DRbar
  3  3     2.31280036E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.22034819E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.57741456E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.22034819E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.98628256E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.22034819E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02298067E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.22034819E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -6.16651218E+04   # M^2_Hd              
        22    -9.14438861E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41233331E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.21161879E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48047908E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48047908E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51952092E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51952092E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.02890034E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.75822302E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.52417770E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.22344894E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.34300250E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.66224352E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.35430218E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.54051834E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.87439606E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72269867E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.63741582E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.22291861E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.80554649E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.17581468E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.82418532E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.66946650E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.89693919E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.86587412E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.21846719E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.89017332E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.11524942E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.23216883E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.73023070E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.09956564E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.60542839E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96064350E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.92669319E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60900568E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.06991439E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.26293734E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.21987964E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.82168962E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.11184763E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.20641922E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56531434E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.83870520E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.88240231E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.99340623E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43468556E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96576756E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.90030499E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60812293E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     8.91617564E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.20591448E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.21419065E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.10210514E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.11868255E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68666979E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.46003984E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.66351075E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.33046755E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.51761013E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55399599E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96064350E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.92669319E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60900568E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.06991439E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.26293734E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.21987964E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.82168962E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.11184763E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.20641922E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56531434E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.83870520E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.88240231E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.99340623E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43468556E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96576756E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.90030499E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60812293E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     8.91617564E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.20591448E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.21419065E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.10210514E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.11868255E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68666979E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.46003984E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.66351075E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.33046755E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.51761013E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55399599E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89007848E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.96069524E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00318509E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.21173813E-12    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.05003494E-11    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00074538E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54503324E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.72219519E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.89007848E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.96069524E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00318509E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.21173813E-12    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.05003494E-11    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00074538E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54503324E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.72219519E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.74451112E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52005114E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16151797E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31843089E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.68898807E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60126222E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13441323E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     5.84071595E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     4.65828445E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26416551E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     5.40486779E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89029741E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.94203824E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00018081E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.00561537E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.89029741E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.94203824E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00018081E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.00561537E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89130899E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.94109926E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99992851E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.00596157E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.91368994E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.87480119E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.45174989E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.62891830E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     1.47136504E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.47136504E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     5.93892930E-12    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     5.93892930E-12    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     9.55397561E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.76811675E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.84387842E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.62154113E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.95968624E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.01808819E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99929916E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.00839722E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.90180823E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.73764664E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.35332395E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.60541402E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.60541402E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.35125228E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.42734057E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33999644E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33999644E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.14973210E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.14973210E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     1.60523280E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     1.60523280E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     1.60523280E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     1.60523280E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.12187918E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.12187918E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     6.96892209E-12    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     6.96892209E-12    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     6.96892209E-12    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     6.96892209E-12    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     6.44403881E-13    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     6.44403881E-13    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     9.80123596E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.20713955E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.42382538E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.66396470E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.66396470E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.01321405E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.55332586E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31573581E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31573581E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.22304285E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.22304285E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.77056498E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.77056498E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.77056498E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.77056498E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     9.46418035E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     9.46418035E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.11797903E-11    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.11797903E-11    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.11797903E-11    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.11797903E-11    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.51131889E-12    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.51131889E-12    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.76580373E-03   # h decays
#          BR         NDA      ID1       ID2
     5.45202176E-01    2           5        -5   # BR(h -> b       bb     )
     6.94749820E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.45938241E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.21102099E-04    2           3        -3   # BR(h -> s       sb     )
     2.26428703E-02    2           4        -4   # BR(h -> c       cb     )
     7.46790413E-02    2          21        21   # BR(h -> g       g      )
     2.59474983E-03    2          22        22   # BR(h -> gam     gam    )
     1.83163073E-03    2          22        23   # BR(h -> Z       gam    )
     2.50889425E-01    2          24       -24   # BR(h -> W+      W-     )
     3.19180841E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.14483428E+01   # H decays
#          BR         NDA      ID1       ID2
     9.09676609E-01    2           5        -5   # BR(H -> b       bb     )
     5.99987617E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12141284E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.60508997E-04    2           3        -3   # BR(H -> s       sb     )
     7.30201917E-08    2           4        -4   # BR(H -> c       cb     )
     7.28423720E-03    2           6        -6   # BR(H -> t       tb     )
     4.15014769E-06    2          21        21   # BR(H -> g       g      )
     3.97013413E-08    2          22        22   # BR(H -> gam     gam    )
     2.98484128E-09    2          23        22   # BR(H -> Z       gam    )
     6.75008874E-07    2          24       -24   # BR(H -> W+      W-     )
     3.37088301E-07    2          23        23   # BR(H -> Z       Z      )
     4.88392340E-06    2          25        25   # BR(H -> h       h      )
     3.53861877E-25    2          36        36   # BR(H -> A       A      )
     5.57741275E-18    2          23        36   # BR(H -> Z       A      )
     7.49659231E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.70039421E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.74711769E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.37215060E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.11537244E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.26591130E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.05768669E+01   # A decays
#          BR         NDA      ID1       ID2
     9.29206491E-01    2           5        -5   # BR(A -> b       bb     )
     6.12840320E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.16685390E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.66134653E-04    2           3        -3   # BR(A -> s       sb     )
     7.50994267E-08    2           4        -4   # BR(A -> c       cb     )
     7.48752664E-03    2           6        -6   # BR(A -> t       tb     )
     2.20500489E-05    2          21        21   # BR(A -> g       g      )
     5.80854580E-08    2          22        22   # BR(A -> gam     gam    )
     2.17185359E-08    2          23        22   # BR(A -> Z       gam    )
     6.86774410E-07    2          23        25   # BR(A -> Z       h      )
     8.19010435E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.83689865E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.09341818E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.49516814E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.41723160E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48099014E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.63164224E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.99121133E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.47832531E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.17018348E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.40744369E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.31023577E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.31964345E-07    2          24        25   # BR(H+ -> W+      h      )
     4.82329092E-14    2          24        36   # BR(H+ -> W+      A      )
     4.52048332E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.62970042E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.02652840E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

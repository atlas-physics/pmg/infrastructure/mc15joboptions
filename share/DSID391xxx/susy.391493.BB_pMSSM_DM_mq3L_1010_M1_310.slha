#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14032060E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.46990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04166019E+01   # W+
        25     1.25545382E+02   # h
        35     4.00000866E+03   # H
        36     3.99999716E+03   # A
        37     4.00106603E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02860917E+03   # ~d_L
   2000001     4.02468650E+03   # ~d_R
   1000002     4.02795681E+03   # ~u_L
   2000002     4.02542332E+03   # ~u_R
   1000003     4.02860917E+03   # ~s_L
   2000003     4.02468650E+03   # ~s_R
   1000004     4.02795681E+03   # ~c_L
   2000004     4.02542332E+03   # ~c_R
   1000005     1.07414625E+03   # ~b_1
   2000005     4.02686371E+03   # ~b_2
   1000006     1.05581958E+03   # ~t_1
   2000006     1.97857306E+03   # ~t_2
   1000011     4.00512658E+03   # ~e_L
   2000011     4.00347707E+03   # ~e_R
   1000012     4.00401226E+03   # ~nu_eL
   1000013     4.00512658E+03   # ~mu_L
   2000013     4.00347707E+03   # ~mu_R
   1000014     4.00401226E+03   # ~nu_muL
   1000015     4.00454353E+03   # ~tau_1
   2000015     4.00764164E+03   # ~tau_2
   1000016     4.00520523E+03   # ~nu_tauL
   1000021     1.98412104E+03   # ~g
   1000022     2.99213596E+02   # ~chi_10
   1000023    -3.58354040E+02   # ~chi_20
   1000025     3.70021989E+02   # ~chi_30
   1000035     2.05141632E+03   # ~chi_40
   1000024     3.55798971E+02   # ~chi_1+
   1000037     2.05158115E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.64103163E-01   # N_11
  1  2    -1.37441357E-02   # N_12
  1  3    -3.86334192E-01   # N_13
  1  4    -3.22308413E-01   # N_14
  2  1    -4.96421199E-02   # N_21
  2  2     2.44908307E-02   # N_22
  2  3    -7.03236073E-01   # N_23
  2  4     7.08798197E-01   # N_24
  3  1     5.00859603E-01   # N_31
  3  2     2.81745682E-02   # N_32
  3  3     5.96808890E-01   # N_33
  3  4     6.26230789E-01   # N_34
  4  1    -1.02018273E-03   # N_41
  4  2     9.99208433E-01   # N_42
  4  3    -4.90570990E-03   # N_43
  4  4    -3.94639276E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     6.94333402E-03   # U_11
  1  2     9.99975895E-01   # U_12
  2  1    -9.99975895E-01   # U_21
  2  2     6.94333402E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.58215743E-02   # V_11
  1  2    -9.98440760E-01   # V_12
  2  1    -9.98440760E-01   # V_21
  2  2    -5.58215743E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90749334E-01   # cos(theta_t)
  1  2    -1.35704669E-01   # sin(theta_t)
  2  1     1.35704669E-01   # -sin(theta_t)
  2  2     9.90749334E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999362E-01   # cos(theta_b)
  1  2    -1.12960152E-03   # sin(theta_b)
  2  1     1.12960152E-03   # -sin(theta_b)
  2  2     9.99999362E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05899460E-01   # cos(theta_tau)
  1  2     7.08312044E-01   # sin(theta_tau)
  2  1    -7.08312044E-01   # -sin(theta_tau)
  2  2    -7.05899460E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00273446E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40320601E+03  # DRbar Higgs Parameters
         1    -3.46990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43497170E+02   # higgs               
         4     1.60755046E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40320601E+03  # The gauge couplings
     1     3.62186744E-01   # gprime(Q) DRbar
     2     6.36216032E-01   # g(Q) DRbar
     3     1.02833822E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40320601E+03  # The trilinear couplings
  1  1     1.60580681E-06   # A_u(Q) DRbar
  2  2     1.60582207E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40320601E+03  # The trilinear couplings
  1  1     5.91606780E-07   # A_d(Q) DRbar
  2  2     5.91661385E-07   # A_s(Q) DRbar
  3  3     1.05699639E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40320601E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.29061510E-07   # A_mu(Q) DRbar
  3  3     1.30388151E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40320601E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65073125E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40320601E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83469605E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40320601E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03443323E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40320601E+03  # The soft SUSY breaking masses at the scale Q
         1     3.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57753360E+07   # M^2_Hd              
        22    -7.45593771E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40431076E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.31136551E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40252758E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40252758E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59747242E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59747242E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.28572755E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.29796590E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.30959138E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.70277751E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.05783452E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.11723422E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.62645047E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.20823921E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.70615502E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.37046493E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.55378211E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.12711707E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.20713613E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.32584227E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.49088782E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.92475063E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.57270095E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.90116606E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66126823E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54652195E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81076873E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.63608066E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     8.79845975E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64444477E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.74150855E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13038492E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.26178159E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.69030343E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.82906594E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.61060917E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78245948E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.95779475E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.96119852E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.30967216E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82668982E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.38249469E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64121072E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51495666E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60448024E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.14310781E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.36109188E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.38412985E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.21680158E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44591482E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78266170E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.73995154E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.93023588E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.85733700E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83176038E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     8.32798713E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67354117E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51713669E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53716607E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.08092897E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.55105689E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.61116109E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.39048248E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85544030E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78245948E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.95779475E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.96119852E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.30967216E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82668982E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.38249469E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64121072E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51495666E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60448024E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.14310781E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.36109188E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.38412985E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.21680158E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44591482E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78266170E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.73995154E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.93023588E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.85733700E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83176038E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     8.32798713E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67354117E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51713669E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53716607E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.08092897E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.55105689E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.61116109E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.39048248E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85544030E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15010280E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.15747834E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.15817276E-06    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.93958734E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77787688E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     4.85830822E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57012863E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06321462E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.47797446E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.45601782E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.49745963E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.73224040E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15010280E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.15747834E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.15817276E-06    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.93958734E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77787688E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     4.85830822E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57012863E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06321462E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.47797446E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.45601782E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.49745963E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.73224040E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.09745950E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.69793710E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.40342531E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.10291886E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40096035E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.48661621E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80917954E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09302805E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.78193688E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.13601624E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.81280206E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42786449E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.00702009E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.86309293E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15115548E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.29358006E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.40092423E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.32049719E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78153649E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.13821945E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54744229E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15115548E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.29358006E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.40092423E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.32049719E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78153649E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.13821945E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54744229E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47781618E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.17243508E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.26973008E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.00954026E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52209512E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.61793115E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03002536E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.99720811E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33505333E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33505333E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11169854E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11169854E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10649625E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.69507113E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.41923220E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.16580860E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.24115164E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.48073119E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.51715467E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.27338018E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.72539721E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.39833254E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.87169619E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.03026654E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17723015E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52720720E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17723015E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52720720E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.43674183E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50293428E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50293428E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47680485E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.00357892E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.00357892E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.00357877E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.26910285E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.26910285E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.26910285E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.26910285E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.56368355E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.56368355E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.56368355E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.56368355E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     5.25434529E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     5.25434529E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     9.15912822E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.58232101E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     2.43739043E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     9.62253795E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.24413112E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     9.62253795E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.24413112E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.19926772E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.82021623E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.82021623E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.81481683E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.72090479E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.72090479E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.72089698E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.98818807E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.57934482E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.98818807E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.57934482E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.63359355E-04    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     5.91675120E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     5.91675120E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     5.15725316E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.18278076E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.18278076E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.18278077E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.81101620E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.81101620E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.81101620E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.81101620E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     6.03666375E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     6.03666375E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     6.03666375E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     6.03666375E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.59321248E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.59321248E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.69377477E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.48824922E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.29288091E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.53981619E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.26477183E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.26477183E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.97540872E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.15845267E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.31923469E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.66278290E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.66278290E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.66790240E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.66790240E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.02772651E-03   # h decays
#          BR         NDA      ID1       ID2
     5.92817697E-01    2           5        -5   # BR(h -> b       bb     )
     6.46934646E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29014381E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.85670655E-04    2           3        -3   # BR(h -> s       sb     )
     2.10928084E-02    2           4        -4   # BR(h -> c       cb     )
     6.88943236E-02    2          21        21   # BR(h -> g       g      )
     2.38158337E-03    2          22        22   # BR(h -> gam     gam    )
     1.62943782E-03    2          22        23   # BR(h -> Z       gam    )
     2.19966162E-01    2          24       -24   # BR(h -> W+      W-     )
     2.78098386E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48899475E+01   # H decays
#          BR         NDA      ID1       ID2
     3.54127650E-01    2           5        -5   # BR(H -> b       bb     )
     6.04045283E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13575782E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52623311E-04    2           3        -3   # BR(H -> s       sb     )
     7.08576234E-08    2           4        -4   # BR(H -> c       cb     )
     7.10140916E-03    2           6        -6   # BR(H -> t       tb     )
     7.65692140E-07    2          21        21   # BR(H -> g       g      )
     1.70549794E-09    2          22        22   # BR(H -> gam     gam    )
     1.82513769E-09    2          23        22   # BR(H -> Z       gam    )
     1.81459676E-06    2          24       -24   # BR(H -> W+      W-     )
     9.06669761E-07    2          23        23   # BR(H -> Z       Z      )
     7.13307182E-06    2          25        25   # BR(H -> h       h      )
     1.30865920E-25    2          36        36   # BR(H -> A       A      )
     4.70477001E-20    2          23        36   # BR(H -> Z       A      )
     1.85497698E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.60548642E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.60548642E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.18629748E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.82237231E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.32649950E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.68552293E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.21088288E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.33998281E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.11877415E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.17039771E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.59685317E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.83884326E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.44382190E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     3.64223267E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     3.64223267E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.27289954E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48846597E+01   # A decays
#          BR         NDA      ID1       ID2
     3.54187399E-01    2           5        -5   # BR(A -> b       bb     )
     6.04106387E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13597218E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52689887E-04    2           3        -3   # BR(A -> s       sb     )
     7.13207119E-08    2           4        -4   # BR(A -> c       cb     )
     7.11552087E-03    2           6        -6   # BR(A -> t       tb     )
     1.46205933E-05    2          21        21   # BR(A -> g       g      )
     5.96799684E-08    2          22        22   # BR(A -> gam     gam    )
     1.60702965E-08    2          23        22   # BR(A -> Z       gam    )
     1.81095651E-06    2          23        25   # BR(A -> Z       h      )
     1.86876203E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.60559320E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.60559320E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.89247185E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     8.62649214E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.11324792E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.27347914E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     9.88523986E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.46588009E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.24280745E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.75547853E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.87881893E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     3.92322251E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     3.92322251E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.49718430E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.68195966E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.03309469E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13315448E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.63645108E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20847047E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48621237E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.61855200E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.81016763E-06    2          24        25   # BR(H+ -> W+      h      )
     3.33978206E-14    2          24        36   # BR(H+ -> W+      A      )
     6.13415273E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.41269955E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.50872572E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.60522017E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.80631977E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59076069E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.14793226E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.02655236E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     7.57082675E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14032898E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -1.85990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     1.00990000E+03   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.96485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04152590E+01   # W+
        25     1.25553572E+02   # h
        35     4.00000421E+03   # H
        36     3.99999689E+03   # A
        37     4.00107865E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02862134E+03   # ~d_L
   2000001     4.02469330E+03   # ~d_R
   1000002     4.02796785E+03   # ~u_L
   2000002     4.02543719E+03   # ~u_R
   1000003     4.02862134E+03   # ~s_L
   2000003     4.02469330E+03   # ~s_R
   1000004     4.02796785E+03   # ~c_L
   2000004     4.02543719E+03   # ~c_R
   1000005     1.07304132E+03   # ~b_1
   2000005     4.02683776E+03   # ~b_2
   1000006     1.05481124E+03   # ~t_1
   2000006     1.97835268E+03   # ~t_2
   1000011     4.00514225E+03   # ~e_L
   2000011     4.00351468E+03   # ~e_R
   1000012     4.00402590E+03   # ~nu_eL
   1000013     4.00514225E+03   # ~mu_L
   2000013     4.00351468E+03   # ~mu_R
   1000014     4.00402590E+03   # ~nu_muL
   1000015     4.00528421E+03   # ~tau_1
   2000015     4.00698781E+03   # ~tau_2
   1000016     4.00523002E+03   # ~nu_tauL
   1000021     1.98412642E+03   # ~g
   1000022     1.45755396E+02   # ~chi_10
   1000023    -1.96257596E+02   # ~chi_20
   1000025     2.09818581E+02   # ~chi_30
   1000035     2.05148086E+03   # ~chi_40
   1000024     1.92411028E+02   # ~chi_1+
   1000037     2.05164604E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.15758361E-01   # N_11
  1  2    -1.34361403E-02   # N_12
  1  3    -4.63654822E-01   # N_13
  1  4    -3.45516964E-01   # N_14
  2  1    -9.37540735E-02   # N_21
  2  2     2.63765358E-02   # N_22
  2  3    -6.95917848E-01   # N_23
  2  4     7.11486332E-01   # N_24
  3  1     5.70743037E-01   # N_31
  3  2     2.51568341E-02   # N_32
  3  3     5.48379216E-01   # N_33
  3  4     6.10655186E-01   # N_34
  4  1    -9.25234836E-04   # N_41
  4  2     9.99245156E-01   # N_42
  4  3    -1.67057520E-03   # N_43
  4  4    -3.88003985E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     2.36722888E-03   # U_11
  1  2     9.99997198E-01   # U_12
  2  1    -9.99997198E-01   # U_21
  2  2     2.36722888E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.48848042E-02   # V_11
  1  2    -9.98492693E-01   # V_12
  2  1    -9.98492693E-01   # V_21
  2  2    -5.48848042E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.90801675E-01   # cos(theta_t)
  1  2    -1.35321989E-01   # sin(theta_t)
  2  1     1.35321989E-01   # -sin(theta_t)
  2  2     9.90801675E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999821E-01   # cos(theta_b)
  1  2    -5.98330985E-04   # sin(theta_b)
  2  1     5.98330985E-04   # -sin(theta_b)
  2  2     9.99999821E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.04736914E-01   # cos(theta_tau)
  1  2     7.09468732E-01   # sin(theta_tau)
  2  1    -7.09468732E-01   # -sin(theta_tau)
  2  2    -7.04736914E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00310101E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.40328983E+03  # DRbar Higgs Parameters
         1    -1.85990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43427463E+02   # higgs               
         4     1.60575375E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.40328983E+03  # The gauge couplings
     1     3.62311414E-01   # gprime(Q) DRbar
     2     6.36911670E-01   # g(Q) DRbar
     3     1.02833882E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.40328983E+03  # The trilinear couplings
  1  1     1.60696365E-06   # A_u(Q) DRbar
  2  2     1.60697904E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.40328983E+03  # The trilinear couplings
  1  1     5.89951140E-07   # A_d(Q) DRbar
  2  2     5.90006005E-07   # A_s(Q) DRbar
  3  3     1.05486990E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.40328983E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.26554626E-07   # A_mu(Q) DRbar
  3  3     1.27860456E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.40328983E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.65353865E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.40328983E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.80188899E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.40328983E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04047991E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.40328983E+03  # The soft SUSY breaking masses at the scale Q
         1     1.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58603142E+07   # M^2_Hd              
        22     1.18617786E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     1.00990000E+03   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.96485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40744226E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.31798828E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40298276E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40298276E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59701724E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59701724E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.54336865E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.90156483E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.39492895E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.58731318E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.02760138E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14317090E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.85237577E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.25202939E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.67489892E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.42827434E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.51209924E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.09975423E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.15511534E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.57847670E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.89209718E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.70460048E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.03323800E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.93700643E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65931460E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.67796742E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.76275908E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.52689018E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.33677501E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60524655E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.54800073E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.14118657E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     3.51628337E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.35128407E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     7.30209996E-05    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     6.25626231E-07    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78310056E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.74753551E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     6.46433544E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.55432027E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83586256E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.26788724E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.65947448E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51211521E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60533743E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.72223190E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.90705430E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.81739298E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.64620688E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44113019E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78330087E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.47585910E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.70448754E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.02885431E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.84059968E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     9.80023187E-07    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.69128843E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51430946E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53740546E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.71491805E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.28072613E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.74333888E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     6.90476566E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85413664E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78310056E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.74753551E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     6.46433544E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.55432027E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83586256E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.26788724E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.65947448E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51211521E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60533743E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.72223190E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.90705430E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.81739298E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.64620688E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44113019E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78330087E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.47585910E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.70448754E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.02885431E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.84059968E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     9.80023187E-07    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.69128843E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51430946E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53740546E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.71491805E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.28072613E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.74333888E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     6.90476566E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85413664E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.16077749E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03547513E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.69750038E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.22306193E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77496151E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.70408665E-06    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56350262E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.08354361E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.66091709E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     8.77917056E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.25128653E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.67200808E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.16077749E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03547513E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.69750038E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.22306193E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77496151E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.70408665E-06    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56350262E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.08354361E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.66091709E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     8.77917056E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.25128653E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.67200808E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.11767338E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.50974059E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.14624717E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.34208093E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39523881E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.41011795E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.79730315E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.12070299E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.44857797E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.73250378E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.09799298E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41841491E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.17990244E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84377352E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.16182616E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.16194722E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.23105718E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.55893762E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77801468E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.06438691E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54118990E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.16182616E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.16194722E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.23105718E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.55893762E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77801468E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.06438691E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54118990E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.49413458E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05175852E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.92465637E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.12661577E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51564486E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.72841680E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.01784681E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.91014305E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33594159E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33594159E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11199104E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11199104E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10413474E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.73832126E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.39925987E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.20436838E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.22496307E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.57467312E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.79529556E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     8.40909446E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.49568892E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.48181408E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     8.64953182E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.13972005E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18056386E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53132939E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18056386E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53132939E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.40857500E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.51115431E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.51115431E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47749572E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.02017822E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.02017822E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.02017802E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.72416431E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.72416431E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.72416431E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.72416431E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.24139022E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.24139022E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.24139022E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.24139022E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.39734645E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.39734645E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.01694976E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.69775816E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     5.86734740E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     7.79899441E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.00892310E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     7.79899441E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.00892310E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     9.55997927E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     2.29191755E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     2.29191755E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.27523317E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     4.63551305E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     4.63551305E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     4.63550783E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     3.24845449E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     4.21389733E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     3.24845449E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     4.21389733E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.03326795E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     9.66380389E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     9.66380389E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     8.74787467E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.93163311E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.93163311E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.93163314E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     3.83409664E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     3.83409664E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     3.83409664E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     3.83409664E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.27801576E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.27801576E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.27801576E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.27801576E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.21418527E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.21418527E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.73717959E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.20639712E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.70708131E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.94608976E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     8.35925281E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     8.35925281E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     9.11252222E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.86679210E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.67973382E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.65370732E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.65370732E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.65878796E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.65878796E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.05161314E-03   # h decays
#          BR         NDA      ID1       ID2
     5.94918778E-01    2           5        -5   # BR(h -> b       bb     )
     6.43256817E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27712397E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.82902486E-04    2           3        -3   # BR(h -> s       sb     )
     2.09695474E-02    2           4        -4   # BR(h -> c       cb     )
     6.85081534E-02    2          21        21   # BR(h -> g       g      )
     2.36672774E-03    2          22        22   # BR(h -> gam     gam    )
     1.62092594E-03    2          22        23   # BR(h -> Z       gam    )
     2.18905690E-01    2          24       -24   # BR(h -> W+      W-     )
     2.76738809E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.44592982E+01   # H decays
#          BR         NDA      ID1       ID2
     3.41391072E-01    2           5        -5   # BR(H -> b       bb     )
     6.08821008E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.15264362E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54620645E-04    2           3        -3   # BR(H -> s       sb     )
     7.14283340E-08    2           4        -4   # BR(H -> c       cb     )
     7.15860616E-03    2           6        -6   # BR(H -> t       tb     )
     1.10441276E-06    2          21        21   # BR(H -> g       g      )
     7.79655814E-11    2          22        22   # BR(H -> gam     gam    )
     1.83632800E-09    2          23        22   # BR(H -> Z       gam    )
     2.02857153E-06    2          24       -24   # BR(H -> W+      W-     )
     1.01358294E-06    2          23        23   # BR(H -> Z       Z      )
     7.63859252E-06    2          25        25   # BR(H -> h       h      )
    -3.83119152E-24    2          36        36   # BR(H -> A       A      )
     6.57252099E-20    2          23        36   # BR(H -> Z       A      )
     1.85541935E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.66216364E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.66216364E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.88454678E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.56400364E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.60727106E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.09479577E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.84860937E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.66320347E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62456485E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.80038303E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.20837225E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     9.17873793E-05    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     3.83939432E-08    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.31197515E-04    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.31197515E-04    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.24648571E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.44549686E+01   # A decays
#          BR         NDA      ID1       ID2
     3.41444047E-01    2           5        -5   # BR(A -> b       bb     )
     6.08873205E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.15282648E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.54683789E-04    2           3        -3   # BR(A -> s       sb     )
     7.18834823E-08    2           4        -4   # BR(A -> c       cb     )
     7.17166732E-03    2           6        -6   # BR(A -> t       tb     )
     1.47359613E-05    2          21        21   # BR(A -> g       g      )
     4.69911780E-08    2          22        22   # BR(A -> gam     gam    )
     1.61933163E-08    2          23        22   # BR(A -> Z       gam    )
     2.02447335E-06    2          23        25   # BR(A -> Z       h      )
     1.85683751E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.66233295E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.66233295E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.49818334E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.93831548E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.30029705E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.67128346E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     3.60025682E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.51916097E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.82925986E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.33887347E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.80807296E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.48396354E-04    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.48396354E-04    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.43779148E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.44756847E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.09900878E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.15646008E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.48644081E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.22167303E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.51337429E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.47292679E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.02969765E-06    2          24        25   # BR(H+ -> W+      h      )
     3.58477185E-14    2          24        36   # BR(H+ -> W+      A      )
     5.57963443E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.46615368E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.33153054E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.66685810E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     7.20146980E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.61771195E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.00440678E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.92791739E-05    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     4.67835156E-04    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

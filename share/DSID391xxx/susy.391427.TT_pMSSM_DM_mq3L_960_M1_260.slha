#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13003554E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.03990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04183753E+01   # W+
        25     1.26082545E+02   # h
        35     4.00000736E+03   # H
        36     3.99999670E+03   # A
        37     4.00105780E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02591339E+03   # ~d_L
   2000001     4.02257210E+03   # ~d_R
   1000002     4.02526357E+03   # ~u_L
   2000002     4.02331093E+03   # ~u_R
   1000003     4.02591339E+03   # ~s_L
   2000003     4.02257210E+03   # ~s_R
   1000004     4.02526357E+03   # ~c_L
   2000004     4.02331093E+03   # ~c_R
   1000005     1.01483220E+03   # ~b_1
   2000005     4.02541350E+03   # ~b_2
   1000006     9.87461978E+02   # ~t_1
   2000006     1.80231710E+03   # ~t_2
   1000011     4.00454142E+03   # ~e_L
   2000011     4.00348152E+03   # ~e_R
   1000012     4.00343025E+03   # ~nu_eL
   1000013     4.00454142E+03   # ~mu_L
   2000013     4.00348152E+03   # ~mu_R
   1000014     4.00343025E+03   # ~nu_muL
   1000015     4.00480788E+03   # ~tau_1
   2000015     4.00752108E+03   # ~tau_2
   1000016     4.00486702E+03   # ~nu_tauL
   1000021     1.97719343E+03   # ~g
   1000022     2.50527741E+02   # ~chi_10
   1000023    -3.14811059E+02   # ~chi_20
   1000025     3.25078007E+02   # ~chi_30
   1000035     2.05216025E+03   # ~chi_40
   1000024     3.12104657E+02   # ~chi_1+
   1000037     2.05232484E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.83688378E-01   # N_11
  1  2    -1.20188770E-02   # N_12
  1  3    -3.66677640E-01   # N_13
  1  4    -2.90685235E-01   # N_14
  2  1    -5.77601580E-02   # N_21
  2  2     2.49626801E-02   # N_22
  2  3    -7.02220045E-01   # N_23
  2  4     7.09173912E-01   # N_24
  3  1     4.64497194E-01   # N_31
  3  2     2.80905637E-02   # N_32
  3  3     6.10260869E-01   # N_33
  3  4     6.41120074E-01   # N_34
  4  1    -9.85965453E-04   # N_41
  4  2     9.99221363E-01   # N_42
  4  3    -4.02346381E-03   # N_43
  4  4    -3.92365669E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     5.69544391E-03   # U_11
  1  2     9.99983781E-01   # U_12
  2  1    -9.99983781E-01   # U_21
  2  2     5.69544391E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.55006967E-02   # V_11
  1  2    -9.98458648E-01   # V_12
  2  1    -9.98458648E-01   # V_21
  2  2    -5.55006967E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85750061E-01   # cos(theta_t)
  1  2    -1.68216578E-01   # sin(theta_t)
  2  1     1.68216578E-01   # -sin(theta_t)
  2  2     9.85750061E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999515E-01   # cos(theta_b)
  1  2    -9.84885661E-04   # sin(theta_b)
  2  1     9.84885661E-04   # -sin(theta_b)
  2  2     9.99999515E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05667179E-01   # cos(theta_tau)
  1  2     7.08543458E-01   # sin(theta_tau)
  2  1    -7.08543458E-01   # -sin(theta_tau)
  2  2    -7.05667179E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00293072E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30035539E+03  # DRbar Higgs Parameters
         1    -3.03990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43562315E+02   # higgs               
         4     1.60843996E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30035539E+03  # The gauge couplings
     1     3.61968575E-01   # gprime(Q) DRbar
     2     6.36241094E-01   # g(Q) DRbar
     3     1.03007639E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30035539E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36794382E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30035539E+03  # The trilinear couplings
  1  1     5.01948629E-07   # A_d(Q) DRbar
  2  2     5.01995652E-07   # A_s(Q) DRbar
  3  3     8.99511766E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30035539E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09925241E-07   # A_mu(Q) DRbar
  3  3     1.11055380E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30035539E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68803279E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30035539E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.84033709E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30035539E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03609377E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30035539E+03  # The soft SUSY breaking masses at the scale Q
         1     2.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57980003E+07   # M^2_Hd              
        22    -4.33121730E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40434383E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.69557990E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.36930279E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.36930279E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.61822323E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.61822323E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     1.24739823E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     1.24739823E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.24825449E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.17757633E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.24192492E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.92841741E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.11190004E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13132899E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.88462309E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.11073757E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.02844148E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.15483573E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.63335597E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.28423987E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.42552440E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.30049529E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.35120224E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.90426240E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.71132216E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.90332132E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65855232E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54313830E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.81259995E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.65411030E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     6.10142227E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65627664E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.20419104E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12889859E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.79360029E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.29755892E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     2.15680169E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.70231157E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77820872E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.10950079E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.70345702E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.17061915E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.82821110E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.34841608E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64421666E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51443726E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60142475E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.34709932E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.84926914E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.19500740E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.00119518E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44393976E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77840377E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.81195395E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.11974398E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.12209672E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83317035E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     5.63259937E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.67639328E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51662593E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53400241E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.13432533E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.82544655E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.11822793E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.82929991E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85490256E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77820872E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.10950079E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.70345702E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.17061915E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.82821110E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.34841608E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64421666E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51443726E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60142475E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.34709932E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.84926914E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.19500740E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.00119518E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44393976E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77840377E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.81195395E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.11974398E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.12209672E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83317035E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     5.63259937E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.67639328E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51662593E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53400241E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.13432533E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.82544655E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.11822793E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.82929991E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85490256E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14919626E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.22340397E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.15467354E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.31880162E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77666032E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     3.28163032E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56741192E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.06833828E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.81822926E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.32496754E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.14851573E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     5.33172973E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14919626E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.22340397E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.15467354E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.31880162E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77666032E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     3.28163032E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56741192E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.06833828E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.81822926E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.32496754E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.14851573E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     5.33172973E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10184599E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.81924926E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.35525244E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.94498419E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39887852E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.46985354E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80486320E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.09919502E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.92659683E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.25195332E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.66238601E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42418710E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.06048374E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.85558635E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15025758E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.34536104E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.68985693E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.81665114E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78009306E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.11430797E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54483913E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15025758E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.34536104E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.68985693E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.81665114E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78009306E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.11430797E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54483913E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.47903533E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.21866505E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.53072394E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.55141002E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51955851E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.66117592E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02521061E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.77204119E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33474639E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33474639E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11159431E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11159431E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10731860E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.95877740E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.50123112E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.83572144E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.31555069E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.07836812E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.15661604E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.88773320E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.68947668E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.99163068E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.45954555E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.43582079E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17597458E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52541790E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17597458E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52541790E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.44773973E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49788164E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49788164E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47438969E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.99337274E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.99337274E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.99337253E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.29591601E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.29591601E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.29591601E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.29591601E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.65306185E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.65306185E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.65306185E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.65306185E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.66459057E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.66459057E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.90711859E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.04203322E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.34732856E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.10299226E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.42757527E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.10299226E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.42757527E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.38172136E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.24805391E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.24805391E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.23872027E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.55629533E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.55629533E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.55628826E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     5.25654881E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     6.81882872E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     5.25654881E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     6.81882872E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.00180389E-05    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     1.56377820E-04    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     1.56377820E-04    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.30279748E-04    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     3.12591695E-04    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     3.12591695E-04    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     3.12591698E-04    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.71852291E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.71852291E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.71852291E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.71852291E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.90615775E-03    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.90615775E-03    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.90615775E-03    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.90615775E-03    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.73952175E-03    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.73952175E-03    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.95759818E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.27447452E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.87770918E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.67287697E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.87450063E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.87450063E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.51125201E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.17268811E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.14714391E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.70651398E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.70651398E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.84294092E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.84294092E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.70260212E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.70260212E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.10603767E-03   # h decays
#          BR         NDA      ID1       ID2
     5.84916186E-01    2           5        -5   # BR(h -> b       bb     )
     6.37367869E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25625450E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.78086035E-04    2           3        -3   # BR(h -> s       sb     )
     2.07617562E-02    2           4        -4   # BR(h -> c       cb     )
     6.80807359E-02    2          21        21   # BR(h -> g       g      )
     2.38188298E-03    2          22        22   # BR(h -> gam     gam    )
     1.67557817E-03    2          22        23   # BR(h -> Z       gam    )
     2.28600912E-01    2          24       -24   # BR(h -> W+      W-     )
     2.91424501E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.50170031E+01   # H decays
#          BR         NDA      ID1       ID2
     3.53404121E-01    2           5        -5   # BR(H -> b       bb     )
     6.02649993E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13082441E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52039784E-04    2           3        -3   # BR(H -> s       sb     )
     7.06995077E-08    2           4        -4   # BR(H -> c       cb     )
     7.08556265E-03    2           6        -6   # BR(H -> t       tb     )
     6.23917211E-07    2          21        21   # BR(H -> g       g      )
     1.02107896E-09    2          22        22   # BR(H -> gam     gam    )
     1.82029129E-09    2          23        22   # BR(H -> Z       gam    )
     1.91493578E-06    2          24       -24   # BR(H -> W+      W-     )
     9.56804613E-07    2          23        23   # BR(H -> Z       Z      )
     7.42861451E-06    2          25        25   # BR(H -> h       h      )
     3.98449723E-24    2          36        36   # BR(H -> A       A      )
    -2.05608162E-20    2          23        36   # BR(H -> Z       A      )
     1.84630356E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.61461905E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.61461905E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.05028990E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.21741096E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.18336503E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.77208239E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.53973501E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.14956045E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.01407496E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.06521094E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.98109202E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     5.83620605E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     9.82712355E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     2.47060710E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     2.47060710E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.28452146E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.50123137E+01   # A decays
#          BR         NDA      ID1       ID2
     3.53459984E-01    2           5        -5   # BR(A -> b       bb     )
     6.02704512E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13101550E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52103505E-04    2           3        -3   # BR(A -> s       sb     )
     7.11552077E-08    2           4        -4   # BR(A -> c       cb     )
     7.09900886E-03    2           6        -6   # BR(A -> t       tb     )
     1.45866674E-05    2          21        21   # BR(A -> g       g      )
     5.64859724E-08    2          22        22   # BR(A -> gam     gam    )
     1.60379572E-08    2          23        22   # BR(A -> Z       gam    )
     1.91102588E-06    2          23        25   # BR(A -> Z       h      )
     1.85414114E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.61475083E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.61475083E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.77738618E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.03164221E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     9.84892139E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.40501160E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.24904632E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.20788822E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.12283074E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.90333249E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.08065950E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     2.77114384E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     2.77114384E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.50555209E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.66264075E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.02391271E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12990795E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.62408699E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.20663157E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.48242916E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.60638003E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.91171500E-06    2          24        25   # BR(H+ -> W+      h      )
     3.21517840E-14    2          24        36   # BR(H+ -> W+      A      )
     6.48377019E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.87013625E-06    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.21166890E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.61562394E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.36067232E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.59650099E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.20731618E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     3.04218807E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     5.26545574E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

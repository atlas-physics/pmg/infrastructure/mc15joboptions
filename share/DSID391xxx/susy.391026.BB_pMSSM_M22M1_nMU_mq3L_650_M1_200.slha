#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13937743E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03995275E+01   # W+
        25     1.25605181E+02   # h
        35     3.00012750E+03   # H
        36     2.99999992E+03   # A
        37     3.00109635E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03110468E+03   # ~d_L
   2000001     3.02582075E+03   # ~d_R
   1000002     3.03019161E+03   # ~u_L
   2000002     3.02818270E+03   # ~u_R
   1000003     3.03110468E+03   # ~s_L
   2000003     3.02582075E+03   # ~s_R
   1000004     3.03019161E+03   # ~c_L
   2000004     3.02818270E+03   # ~c_R
   1000005     7.39775589E+02   # ~b_1
   2000005     3.02536566E+03   # ~b_2
   1000006     7.36951521E+02   # ~t_1
   2000006     3.02185781E+03   # ~t_2
   1000011     3.00688011E+03   # ~e_L
   2000011     3.00077098E+03   # ~e_R
   1000012     3.00548938E+03   # ~nu_eL
   1000013     3.00688011E+03   # ~mu_L
   2000013     3.00077098E+03   # ~mu_R
   1000014     3.00548938E+03   # ~nu_muL
   1000015     2.98619351E+03   # ~tau_1
   2000015     3.02201602E+03   # ~tau_2
   1000016     3.00569778E+03   # ~nu_tauL
   1000021     2.33844695E+03   # ~g
   1000022     2.01934380E+02   # ~chi_10
   1000023     4.26415092E+02   # ~chi_20
   1000025    -3.00180562E+03   # ~chi_30
   1000035     3.00188678E+03   # ~chi_40
   1000024     4.26577565E+02   # ~chi_1+
   1000037     3.00278156E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891582E-01   # N_11
  1  2     1.90773546E-04   # N_12
  1  3    -1.47216463E-02   # N_13
  1  4    -2.45795245E-04   # N_14
  2  1     1.95835993E-04   # N_21
  2  2     9.99653796E-01   # N_22
  2  3     2.62186701E-02   # N_23
  2  4     2.19805926E-03   # N_24
  3  1    -1.02362084E-02   # N_31
  3  2     1.69856924E-02   # N_32
  3  3    -7.06827333E-01   # N_33
  3  4     7.07108073E-01   # N_34
  4  1     1.05832587E-02   # N_41
  4  2    -2.00932411E-02   # N_42
  4  3     7.06746755E-01   # N_43
  4  4     7.07102030E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99312668E-01   # U_11
  1  2     3.70700954E-02   # U_12
  2  1    -3.70700954E-02   # U_21
  2  2     9.99312668E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995175E-01   # V_11
  1  2    -3.10653646E-03   # V_12
  2  1    -3.10653646E-03   # V_21
  2  2    -9.99995175E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98598081E-01   # cos(theta_t)
  1  2    -5.29327179E-02   # sin(theta_t)
  2  1     5.29327179E-02   # -sin(theta_t)
  2  2     9.98598081E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99720523E-01   # cos(theta_b)
  1  2    -2.36405561E-02   # sin(theta_b)
  2  1     2.36405561E-02   # -sin(theta_b)
  2  2     9.99720523E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06929091E-01   # cos(theta_tau)
  1  2     7.07284427E-01   # sin(theta_tau)
  2  1    -7.07284427E-01   # -sin(theta_tau)
  2  2    -7.06929091E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00134075E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39377431E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44210267E+02   # higgs               
         4     1.09206927E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39377431E+03  # The gauge couplings
     1     3.61905862E-01   # gprime(Q) DRbar
     2     6.38056270E-01   # g(Q) DRbar
     3     1.02799225E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39377431E+03  # The trilinear couplings
  1  1     1.83811611E-06   # A_u(Q) DRbar
  2  2     1.83814421E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39377431E+03  # The trilinear couplings
  1  1     6.50216212E-07   # A_d(Q) DRbar
  2  2     6.50293913E-07   # A_s(Q) DRbar
  3  3     1.34291145E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39377431E+03  # The trilinear couplings
  1  1     2.96551990E-07   # A_e(Q) DRbar
  2  2     2.96566697E-07   # A_mu(Q) DRbar
  3  3     3.00725806E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39377431E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54778654E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39377431E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.92125283E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39377431E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02386516E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39377431E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.79123100E+04   # M^2_Hd              
        22    -9.09495279E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41319776E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.64063899E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48025256E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48025256E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51974744E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51974744E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     3.88055307E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.13223006E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.71350078E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.07327622E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18831133E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.51884206E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.79138667E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.61405843E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.25770493E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.91010223E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70703072E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62582572E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19612591E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.63372197E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.65815679E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.83321328E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.90097104E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.54387808E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.97669189E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.52112260E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.15477038E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.09325560E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.01173507E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.32665612E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.51191549E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.06784961E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.55620948E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.95852288E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.96007483E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61503434E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.54818895E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.22483837E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.23193985E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.23240675E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.09342467E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18986780E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57946957E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.88280152E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.90107079E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.27028248E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42053025E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96361841E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.93359011E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.61415144E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.59968398E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.52569195E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.22624328E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.86849308E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.10026690E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67439930E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.49894825E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.67557244E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.40253118E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.48939902E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55010513E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.95852288E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.96007483E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61503434E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.54818895E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.22483837E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.23193985E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.23240675E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.09342467E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18986780E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57946957E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.88280152E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.90107079E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.27028248E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42053025E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96361841E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.93359011E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.61415144E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.59968398E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.52569195E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.22624328E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.86849308E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.10026690E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67439930E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.49894825E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.67557244E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.40253118E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.48939902E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55010513E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89215738E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.97903971E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00257161E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.44717476E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.84034418E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99952434E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.38141964E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54934354E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.71623068E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.89215738E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.97903971E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00257161E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.44717476E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.84034418E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99952434E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.38141964E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54934354E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.71623068E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.74764170E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52464379E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.15998460E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31537161E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69198828E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.60599632E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13279713E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.44288826E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.93229966E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26094109E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.17154121E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89236887E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.96038775E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99957227E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     9.74880988E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.22239207E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00438893E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.95785278E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89236887E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.96038775E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99957227E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     9.74880988E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.22239207E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00438893E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.95785278E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89299082E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.95949953E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99931810E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.08797952E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.36749812E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00473007E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.85151519E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.93156236E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.48083683E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.40933517E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.60002159E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10324184E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.97345436E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.91303483E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.81913661E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.20074561E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.03743659E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99929614E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.03859208E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.50619700E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.01077678E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.47285299E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.80272233E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.80272233E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.75023392E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.52294348E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32695274E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32695274E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.93688081E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.93688081E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.09542578E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.09542578E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     4.09542578E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.09542578E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.11770369E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.11770369E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.40656928E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.56034278E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.51109838E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.86531335E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.86531335E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.05658194E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.70122719E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.30117248E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.30117248E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.01043380E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.01043380E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.14524313E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.14524313E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     5.14524313E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.14524313E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.71954860E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.71954860E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.69706731E-03   # h decays
#          BR         NDA      ID1       ID2
     5.53741959E-01    2           5        -5   # BR(h -> b       bb     )
     7.04739352E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.49476920E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.29020362E-04    2           3        -3   # BR(h -> s       sb     )
     2.29881544E-02    2           4        -4   # BR(h -> c       cb     )
     7.53233154E-02    2          21        21   # BR(h -> g       g      )
     2.59729403E-03    2          22        22   # BR(h -> gam     gam    )
     1.78347181E-03    2          22        23   # BR(h -> Z       gam    )
     2.41791651E-01    2          24       -24   # BR(h -> W+      W-     )
     3.05217216E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.87326146E+01   # H decays
#          BR         NDA      ID1       ID2
     9.04024157E-01    2           5        -5   # BR(H -> b       bb     )
     6.42043546E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.27011255E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.78769986E-04    2           3        -3   # BR(H -> s       sb     )
     7.81346522E-08    2           4        -4   # BR(H -> c       cb     )
     7.79443452E-03    2           6        -6   # BR(H -> t       tb     )
     6.57880947E-06    2          21        21   # BR(H -> g       g      )
     4.66316958E-08    2          22        22   # BR(H -> gam     gam    )
     3.19367612E-09    2          23        22   # BR(H -> Z       gam    )
     6.89399926E-07    2          24       -24   # BR(H -> W+      W-     )
     3.44274908E-07    2          23        23   # BR(H -> Z       Z      )
     5.19294971E-06    2          25        25   # BR(H -> h       h      )
    -1.86102052E-25    2          36        36   # BR(H -> A       A      )
     1.02431253E-18    2          23        36   # BR(H -> Z       A      )
     8.00526070E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.96140097E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.00137595E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.53651635E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.19590803E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.32972595E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.78880334E+01   # A decays
#          BR         NDA      ID1       ID2
     9.24185043E-01    2           5        -5   # BR(A -> b       bb     )
     6.56332283E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.32063087E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.85021659E-04    2           3        -3   # BR(A -> s       sb     )
     8.04290722E-08    2           4        -4   # BR(A -> c       cb     )
     8.01890037E-03    2           6        -6   # BR(A -> t       tb     )
     2.36148944E-05    2          21        21   # BR(A -> g       g      )
     6.21812926E-08    2          22        22   # BR(A -> gam     gam    )
     2.32564420E-08    2          23        22   # BR(A -> Z       gam    )
     7.02045735E-07    2          23        25   # BR(A -> Z       h      )
     8.75541305E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.11053644E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.37596153E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.67017465E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.13737116E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47348739E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.01256858E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.12589760E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.43030771E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.24933538E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.57028460E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.26798035E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.44007656E-07    2          24        25   # BR(H+ -> W+      h      )
     5.03900143E-14    2          24        36   # BR(H+ -> W+      A      )
     4.82257108E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.73586431E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06283487E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

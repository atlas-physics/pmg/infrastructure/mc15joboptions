#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13015809E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.59990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.89900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.09900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.88485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04184860E+01   # W+
        25     1.25984186E+02   # h
        35     4.00000846E+03   # H
        36     3.99999698E+03   # A
        37     4.00104387E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02593660E+03   # ~d_L
   2000001     4.02257900E+03   # ~d_R
   1000002     4.02528616E+03   # ~u_L
   2000002     4.02335868E+03   # ~u_R
   1000003     4.02593660E+03   # ~s_L
   2000003     4.02257900E+03   # ~s_R
   1000004     4.02528616E+03   # ~c_L
   2000004     4.02335868E+03   # ~c_R
   1000005     9.69581669E+02   # ~b_1
   2000005     4.02544414E+03   # ~b_2
   1000006     9.47269595E+02   # ~t_1
   2000006     1.90109928E+03   # ~t_2
   1000011     4.00456424E+03   # ~e_L
   2000011     4.00339792E+03   # ~e_R
   1000012     4.00345277E+03   # ~nu_eL
   1000013     4.00456424E+03   # ~mu_L
   2000013     4.00339792E+03   # ~mu_R
   1000014     4.00345277E+03   # ~nu_muL
   1000015     4.00439567E+03   # ~tau_1
   2000015     4.00784485E+03   # ~tau_2
   1000016     4.00488011E+03   # ~nu_tauL
   1000021     1.97812874E+03   # ~g
   1000022     3.48188098E+02   # ~chi_10
   1000023    -4.01236387E+02   # ~chi_20
   1000025     4.14997211E+02   # ~chi_30
   1000035     2.05212646E+03   # ~chi_40
   1000024     3.98910904E+02   # ~chi_1+
   1000037     2.05229059E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.40093380E-01   # N_11
  1  2    -1.56170079E-02   # N_12
  1  3    -4.09808810E-01   # N_13
  1  4    -3.55043604E-01   # N_14
  2  1    -4.35047986E-02   # N_21
  2  2     2.40500728E-02   # N_22
  2  3    -7.03936807E-01   # N_23
  2  4     7.08520923E-01   # N_24
  3  1     5.40693376E-01   # N_31
  3  2     2.81538855E-02   # N_32
  3  3     5.80082748E-01   # N_33
  3  4     6.08573773E-01   # N_34
  4  1    -1.05743508E-03   # N_41
  4  2     9.99192205E-01   # N_42
  4  3    -5.80652969E-03   # N_43
  4  4    -3.97505251E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     8.21747788E-03   # U_11
  1  2     9.99966236E-01   # U_12
  2  1    -9.99966236E-01   # U_21
  2  2     8.21747788E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.62262582E-02   # V_11
  1  2    -9.98418053E-01   # V_12
  2  1    -9.98418053E-01   # V_21
  2  2    -5.62262582E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.89896708E-01   # cos(theta_t)
  1  2    -1.41790365E-01   # sin(theta_t)
  2  1     1.41790365E-01   # -sin(theta_t)
  2  2     9.89896708E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999202E-01   # cos(theta_b)
  1  2    -1.26332868E-03   # sin(theta_b)
  2  1     1.26332868E-03   # -sin(theta_b)
  2  2     9.99999202E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06001164E-01   # cos(theta_tau)
  1  2     7.08210672E-01   # sin(theta_tau)
  2  1    -7.08210672E-01   # -sin(theta_tau)
  2  2    -7.06001164E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00272596E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30158093E+03  # DRbar Higgs Parameters
         1    -3.89900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43620729E+02   # higgs               
         4     1.61137461E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30158093E+03  # The gauge couplings
     1     3.61915612E-01   # gprime(Q) DRbar
     2     6.35998022E-01   # g(Q) DRbar
     3     1.03008552E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30158093E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.37046589E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30158093E+03  # The trilinear couplings
  1  1     5.04399186E-07   # A_d(Q) DRbar
  2  2     5.04445845E-07   # A_s(Q) DRbar
  3  3     9.02730348E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30158093E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.11182281E-07   # A_mu(Q) DRbar
  3  3     1.12320321E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30158093E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68141024E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30158093E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85635016E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30158093E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03281958E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30158093E+03  # The soft SUSY breaking masses at the scale Q
         1     3.59990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.57391700E+07   # M^2_Hd              
        22    -1.12513259E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.09899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.88485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40326616E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.94937107E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.40035097E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.40035097E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.59964903E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.59964903E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.73815690E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.19704289E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.21584492E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     3.48288287E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.10422932E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16498378E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.44768347E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.09451680E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.41439202E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.14576684E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.65993577E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.26284220E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.45073084E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.02635809E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.80621730E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.07730872E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.46106981E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.86554042E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66308910E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.55619130E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.82110601E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.62165330E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.20120739E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.65932725E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.38135208E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.12653645E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.63810843E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     2.21767489E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     3.70439865E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     3.97554583E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.78172275E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.78362027E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.14439309E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.46610411E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81474954E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.43388618E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.61737183E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51864229E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60448177E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.89313806E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.03907089E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.60281622E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.44463642E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44936516E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.78192160E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.64316654E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     8.96593273E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.75081876E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.81994998E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.16073993E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.64988992E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.52082086E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53754753E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.01544848E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.71020701E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.18062087E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.98229314E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85637783E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.78172275E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.78362027E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.14439309E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.46610411E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81474954E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.43388618E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.61737183E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51864229E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60448177E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.89313806E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.03907089E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.60281622E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.44463642E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44936516E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.78192160E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.64316654E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     8.96593273E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.75081876E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.81994998E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.16073993E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.64988992E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.52082086E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53754753E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.01544848E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.71020701E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.18062087E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.98229314E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85637783E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.14305821E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.07977360E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.50696553E-07    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.65836668E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77967161E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.78671450E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.57403694E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.05111724E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     7.07087089E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.88676765E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.91025525E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     6.18221834E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.14305821E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.07977360E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.50696553E-07    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.65836668E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77967161E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.78671450E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.57403694E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.05111724E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     7.07087089E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.88676765E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.91025525E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     6.18221834E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.08643619E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.55199777E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.44778963E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.23418762E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.40333636E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.51597134E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.81410215E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.08048019E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.60802689E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.05442261E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.98456578E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.43281777E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.95973364E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.87317394E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.14411898E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.22979227E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.19600995E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.91756063E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.78356112E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.17533040E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.55117715E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.14411898E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.22979227E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.19600995E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.91756063E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.78356112E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.17533040E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.55117715E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.46931451E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.11492739E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.08430430E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.55166961E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.52482600E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.59032160E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.03520445E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.28015821E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33551695E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33551695E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11185394E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11185394E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10525821E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.11423426E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.55964408E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     5.20923808E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.38611345E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.75653207E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.70787535E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.56526292E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.83644526E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.67109985E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.24325333E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.79897690E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17955649E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.53013614E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17955649E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.53013614E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42038445E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.50914063E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.50914063E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47891534E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.01566743E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.01566743E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.01566727E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.13538632E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.13538632E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.13538632E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.13538632E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.11796065E-08    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.11796065E-08    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.11796065E-08    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.11796065E-08    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.21747841E-09    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.21747841E-09    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.62932130E-06   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.29292283E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     3.68937337E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     6.12791882E-02    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     7.90895488E-02    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     6.12791882E-02    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     7.90895488E-02    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     7.57935150E-02    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.78193810E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.78193810E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     1.78071727E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     3.64138656E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     3.64138656E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     3.64137748E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     6.97370860E-03    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     9.04666188E-03    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     6.97370860E-03    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     9.04666188E-03    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.35389526E-03    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     2.07487514E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     2.07487514E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     1.88438779E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     4.14765246E-03    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     4.14765246E-03    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     4.14765250E-03    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     5.16814724E-02    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     5.16814724E-02    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     5.16814724E-02    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     5.16814724E-02    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     1.72269797E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     1.72269797E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     1.72269797E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     1.72269797E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     1.62265343E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     1.62265343E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     5.11277180E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     6.84260645E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.99716061E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.07634433E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.56027399E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.56027399E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.05450960E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.73026706E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.86582818E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.73668280E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.73668280E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.73687128E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.73687128E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07970794E-03   # h decays
#          BR         NDA      ID1       ID2
     5.85179286E-01    2           5        -5   # BR(h -> b       bb     )
     6.40927284E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.26885889E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.80830303E-04    2           3        -3   # BR(h -> s       sb     )
     2.08826282E-02    2           4        -4   # BR(h -> c       cb     )
     6.84497876E-02    2          21        21   # BR(h -> g       g      )
     2.38901441E-03    2          22        22   # BR(h -> gam     gam    )
     1.67198712E-03    2          22        23   # BR(h -> Z       gam    )
     2.27647658E-01    2          24       -24   # BR(h -> W+      W-     )
     2.89791936E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.52798763E+01   # H decays
#          BR         NDA      ID1       ID2
     3.59838221E-01    2           5        -5   # BR(H -> b       bb     )
     5.99784492E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.12069269E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.50841369E-04    2           3        -3   # BR(H -> s       sb     )
     7.03575715E-08    2           4        -4   # BR(H -> c       cb     )
     7.05129355E-03    2           6        -6   # BR(H -> t       tb     )
     5.07835733E-07    2          21        21   # BR(H -> g       g      )
     3.27816456E-09    2          22        22   # BR(H -> gam     gam    )
     1.81313546E-09    2          23        22   # BR(H -> Z       gam    )
     1.79735686E-06    2          24       -24   # BR(H -> W+      W-     )
     8.98055887E-07    2          23        23   # BR(H -> Z       Z      )
     7.11464823E-06    2          25        25   # BR(H -> h       h      )
     1.61146796E-24    2          36        36   # BR(H -> A       A      )
     5.80822974E-20    2          23        36   # BR(H -> Z       A      )
     1.84924242E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.57863462E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.57863462E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.30986509E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     5.76080271E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.45513676E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.53719539E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.58897538E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.54902592E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.23915305E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.17593989E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     5.11330457E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     8.71058246E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     1.32123314E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     5.55899516E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     5.55899516E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.32385860E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.52746807E+01   # A decays
#          BR         NDA      ID1       ID2
     3.59897745E-01    2           5        -5   # BR(A -> b       bb     )
     5.99843755E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.12090056E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.50906886E-04    2           3        -3   # BR(A -> s       sb     )
     7.08174665E-08    2           4        -4   # BR(A -> c       cb     )
     7.06531312E-03    2           6        -6   # BR(A -> t       tb     )
     1.45174299E-05    2          21        21   # BR(A -> g       g      )
     6.20720462E-08    2          22        22   # BR(A -> gam     gam    )
     1.59621390E-08    2          23        22   # BR(A -> Z       gam    )
     1.79371000E-06    2          23        25   # BR(A -> Z       h      )
     1.87060873E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.57868065E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.57868065E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.00284466E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.34392553E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.23251175E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.06761968E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     7.05060745E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.76337766E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.37717475E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     7.51671057E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     5.55184033E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     6.03028783E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     6.03028783E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.54147610E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.78277093E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.98484034E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.11609292E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.70097026E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.19880562E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.46632867E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.68095458E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.79120754E-06    2          24        25   # BR(H+ -> W+      h      )
     2.98611337E-14    2          24        36   # BR(H+ -> W+      A      )
     5.67051706E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.56110830E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.82470321E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.57686140E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     5.30869618E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.56575929E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.06602246E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     4.49946484E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     1.16231025E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12854035E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.94990000E+02   # M_1(MX)             
         2     5.89990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     5.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04035391E+01   # W+
        25     1.24484196E+02   # h
        35     3.00020548E+03   # H
        36     2.99999954E+03   # A
        37     3.00095353E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02647275E+03   # ~d_L
   2000001     3.02145134E+03   # ~d_R
   1000002     3.02555776E+03   # ~u_L
   2000002     3.02326202E+03   # ~u_R
   1000003     3.02647275E+03   # ~s_L
   2000003     3.02145134E+03   # ~s_R
   1000004     3.02555776E+03   # ~c_L
   2000004     3.02326202E+03   # ~c_R
   1000005     6.29870366E+02   # ~b_1
   2000005     3.02086834E+03   # ~b_2
   1000006     6.27342429E+02   # ~t_1
   2000006     3.00591868E+03   # ~t_2
   1000011     3.00641660E+03   # ~e_L
   2000011     3.00129794E+03   # ~e_R
   1000012     3.00502588E+03   # ~nu_eL
   1000013     3.00641660E+03   # ~mu_L
   2000013     3.00129794E+03   # ~mu_R
   1000014     3.00502588E+03   # ~nu_muL
   1000015     2.98567311E+03   # ~tau_1
   2000015     3.02210843E+03   # ~tau_2
   1000016     3.00507469E+03   # ~nu_tauL
   1000021     2.33272427E+03   # ~g
   1000022     2.97602596E+02   # ~chi_10
   1000023     6.18719041E+02   # ~chi_20
   1000025    -3.00122060E+03   # ~chi_30
   1000035     3.00203309E+03   # ~chi_40
   1000024     6.18878146E+02   # ~chi_1+
   1000037     3.00258892E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885347E-01   # N_11
  1  2    -7.79453236E-04   # N_12
  1  3     1.49603197E-02   # N_13
  1  4    -2.20756438E-03   # N_14
  2  1     1.20119265E-03   # N_21
  2  2     9.99606628E-01   # N_22
  2  3    -2.72214821E-02   # N_23
  2  4     6.64362717E-03   # N_24
  3  1    -9.00362155E-03   # N_31
  3  2     1.45606150E-02   # N_32
  3  3     7.06869201E-01   # N_33
  3  4     7.07137084E-01   # N_34
  4  1    -1.21154442E-02   # N_41
  4  2     2.39576851E-02   # N_42
  4  3     7.06661950E-01   # N_43
  4  4    -7.07041819E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99258211E-01   # U_11
  1  2    -3.85100950E-02   # U_12
  2  1     3.85100950E-02   # U_21
  2  2     9.99258211E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99955819E-01   # V_11
  1  2    -9.39997003E-03   # V_12
  2  1     9.39997003E-03   # V_21
  2  2     9.99955819E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98881421E-01   # cos(theta_t)
  1  2    -4.72853760E-02   # sin(theta_t)
  2  1     4.72853760E-02   # -sin(theta_t)
  2  2     9.98881421E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99918476E-01   # cos(theta_b)
  1  2     1.27687648E-02   # sin(theta_b)
  2  1    -1.27687648E-02   # -sin(theta_b)
  2  2     9.99918476E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06939161E-01   # cos(theta_tau)
  1  2     7.07274362E-01   # sin(theta_tau)
  2  1    -7.07274362E-01   # -sin(theta_tau)
  2  2     7.06939161E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01917932E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.28540349E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44593787E+02   # higgs               
         4     6.73478752E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.28540349E+03  # The gauge couplings
     1     3.61657749E-01   # gprime(Q) DRbar
     2     6.37218332E-01   # g(Q) DRbar
     3     1.02980256E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.28540349E+03  # The trilinear couplings
  1  1     1.53914411E-06   # A_u(Q) DRbar
  2  2     1.53916668E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.28540349E+03  # The trilinear couplings
  1  1     4.09414807E-07   # A_d(Q) DRbar
  2  2     4.09488737E-07   # A_s(Q) DRbar
  3  3     9.02172758E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.28540349E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     8.43720012E-08   # A_mu(Q) DRbar
  3  3     8.53571323E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.28540349E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.55340120E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.28540349E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14294394E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.28540349E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06853964E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.28540349E+03  # The soft SUSY breaking masses at the scale Q
         1     2.94990000E+02   # M_1(Q)              
         2     5.89990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.82912302E+04   # M^2_Hd              
        22    -9.11082585E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     5.49899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40931228E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.03957639E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48253303E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48253303E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51746697E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51746697E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.30614924E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.18828709E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.11712906E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.04418581E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.17932644E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.39629736E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.82529697E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.22966643E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.62644980E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51431344E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.00141610E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.15164203E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.46865839E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.31341606E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.25080836E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.83681868E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.22480719E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.69619474E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.35579516E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     6.69170659E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27126983E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.98011836E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.29228670E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.17695379E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.80680189E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.95352167E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57339896E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.55455403E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.27372382E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.14742748E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     8.17074873E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.21963795E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18765333E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55991648E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.11194294E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.22214898E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     5.42282177E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44008132E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.81206770E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.04973263E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57132311E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.46922605E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.59122486E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.14174647E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.47816668E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.22643111E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68305176E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.43125679E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.99892070E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.75522191E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.29646443E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55687370E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.80680189E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.95352167E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57339896E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.55455403E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.27372382E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.14742748E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     8.17074873E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.21963795E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18765333E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55991648E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.11194294E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.22214898E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     5.42282177E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44008132E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.81206770E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.04973263E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57132311E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.46922605E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.59122486E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.14174647E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.47816668E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.22643111E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68305176E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.43125679E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.99892070E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.75522191E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.29646443E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55687370E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.72047568E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.02777919E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99494130E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.47688416E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.08631300E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97727944E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.26882790E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53102860E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998650E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.34954132E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.19661971E-13    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.72047568E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.02777919E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99494130E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.47688416E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.08631300E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97727944E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.26882790E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53102860E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998650E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.34954132E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.19661971E-13    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.65264696E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.60477362E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13485200E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.26037437E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59681548E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.69020741E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10643283E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.09282866E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.03171227E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.20306307E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.02588907E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.72065034E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03289590E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98501582E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.08596050E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.22926665E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.98208826E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.51529453E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.72065034E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03289590E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98501582E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.08596050E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.22926665E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.98208826E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.51529453E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.72105315E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03280153E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98474976E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     8.29355947E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.26951165E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.98244733E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.36792509E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.26684230E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.84415916E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.77384746E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.26807612E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.07760167E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.76572906E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.10722235E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.59605169E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.07631751E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.12026008E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.21226766E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.47877323E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.86328906E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.39954274E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.92121766E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.57120590E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.57120590E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     7.95551857E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.84482044E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.66626973E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.66626973E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.28546680E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.28546680E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.36088628E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.36088628E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.78289947E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.80163115E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.83788769E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.63493117E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.63493117E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.46122464E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.25651771E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.63852249E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.63852249E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31186150E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31186150E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.13080881E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.13080881E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.13080881E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.13080881E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     8.58430733E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     8.58430733E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.82319007E-03   # h decays
#          BR         NDA      ID1       ID2
     6.83830928E-01    2           5        -5   # BR(h -> b       bb     )
     5.39186081E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.90875387E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.05424458E-04    2           3        -3   # BR(h -> s       sb     )
     1.74938727E-02    2           4        -4   # BR(h -> c       cb     )
     5.67429433E-02    2          21        21   # BR(h -> g       g      )
     1.91477805E-03    2          22        22   # BR(h -> gam     gam    )
     1.23690736E-03    2          22        23   # BR(h -> Z       gam    )
     1.63928512E-01    2          24       -24   # BR(h -> W+      W-     )
     2.03371507E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39030268E+01   # H decays
#          BR         NDA      ID1       ID2
     7.42409359E-01    2           5        -5   # BR(H -> b       bb     )
     1.78869156E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.32438593E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.76632199E-04    2           3        -3   # BR(H -> s       sb     )
     2.19235503E-07    2           4        -4   # BR(H -> c       cb     )
     2.18701652E-02    2           6        -6   # BR(H -> t       tb     )
     3.29955062E-05    2          21        21   # BR(H -> g       g      )
     6.60139668E-08    2          22        22   # BR(H -> gam     gam    )
     8.39346228E-09    2          23        22   # BR(H -> Z       gam    )
     3.45743011E-05    2          24       -24   # BR(H -> W+      W-     )
     1.72658420E-05    2          23        23   # BR(H -> Z       Z      )
     8.75380941E-05    2          25        25   # BR(H -> h       h      )
    -5.31938560E-23    2          36        36   # BR(H -> A       A      )
     2.86385311E-17    2          23        36   # BR(H -> Z       A      )
     1.99758816E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08836448E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.96278998E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.70230652E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.14903526E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.29420645E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31988789E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82075375E-01    2           5        -5   # BR(A -> b       bb     )
     1.88403398E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.66148463E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.18168653E-04    2           3        -3   # BR(A -> s       sb     )
     2.30875597E-07    2           4        -4   # BR(A -> c       cb     )
     2.30186469E-02    2           6        -6   # BR(A -> t       tb     )
     6.77877234E-05    2          21        21   # BR(A -> g       g      )
     4.41015180E-08    2          22        22   # BR(A -> gam     gam    )
     6.68049260E-08    2          23        22   # BR(A -> Z       gam    )
     3.62787216E-05    2          23        25   # BR(A -> Z       h      )
     2.66267887E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22916908E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32788355E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.00374762E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.93419687E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08813240E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.50398138E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.85346742E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.96403633E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.20298731E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07042180E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.09121715E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.82771927E-05    2          24        25   # BR(H+ -> W+      h      )
     1.04656827E-13    2          24        36   # BR(H+ -> W+      A      )
     1.99762330E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.80418906E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.53313490E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

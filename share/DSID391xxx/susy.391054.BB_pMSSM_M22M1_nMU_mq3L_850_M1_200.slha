#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15954577E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03984778E+01   # W+
        25     1.25087628E+02   # h
        35     3.00005090E+03   # H
        36     2.99999994E+03   # A
        37     3.00109119E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03878394E+03   # ~d_L
   2000001     3.03358330E+03   # ~d_R
   1000002     3.03787365E+03   # ~u_L
   2000002     3.03518070E+03   # ~u_R
   1000003     3.03878394E+03   # ~s_L
   2000003     3.03358330E+03   # ~s_R
   1000004     3.03787365E+03   # ~c_L
   2000004     3.03518070E+03   # ~c_R
   1000005     9.46928697E+02   # ~b_1
   2000005     3.03160076E+03   # ~b_2
   1000006     9.44701946E+02   # ~t_1
   2000006     3.02372415E+03   # ~t_2
   1000011     3.00655725E+03   # ~e_L
   2000011     3.00155391E+03   # ~e_R
   1000012     3.00516622E+03   # ~nu_eL
   1000013     3.00655725E+03   # ~mu_L
   2000013     3.00155391E+03   # ~mu_R
   1000014     3.00516622E+03   # ~nu_muL
   1000015     2.98607765E+03   # ~tau_1
   2000015     3.02178504E+03   # ~tau_2
   1000016     3.00510819E+03   # ~nu_tauL
   1000021     2.34803946E+03   # ~g
   1000022     2.01454483E+02   # ~chi_10
   1000023     4.27022925E+02   # ~chi_20
   1000025    -2.99695426E+03   # ~chi_30
   1000035     2.99705181E+03   # ~chi_40
   1000024     4.27185833E+02   # ~chi_1+
   1000037     2.99793899E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891547E-01   # N_11
  1  2     1.90647907E-04   # N_12
  1  3    -1.47240507E-02   # N_13
  1  4    -2.45835425E-04   # N_14
  2  1     1.95707142E-04   # N_21
  2  2     9.99654364E-01   # N_22
  2  3     2.61971396E-02   # N_23
  2  4     2.19625523E-03   # N_24
  3  1    -1.02378799E-02   # N_31
  3  2     1.69717425E-02   # N_32
  3  3    -7.06827649E-01   # N_33
  3  4     7.07108068E-01   # N_34
  4  1     1.05849877E-02   # N_41
  4  2    -2.00767421E-02   # N_42
  4  3     7.06747187E-01   # N_43
  4  4     7.07102040E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99313796E-01   # U_11
  1  2     3.70396762E-02   # U_12
  2  1    -3.70396762E-02   # U_21
  2  2     9.99313796E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995183E-01   # V_11
  1  2    -3.10399076E-03   # V_12
  2  1    -3.10399076E-03   # V_21
  2  2    -9.99995183E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98510225E-01   # cos(theta_t)
  1  2    -5.45649207E-02   # sin(theta_t)
  2  1     5.45649207E-02   # -sin(theta_t)
  2  2     9.98510225E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99712562E-01   # cos(theta_b)
  1  2    -2.39748489E-02   # sin(theta_b)
  2  1     2.39748489E-02   # -sin(theta_b)
  2  2     9.99712562E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06938953E-01   # cos(theta_tau)
  1  2     7.07274570E-01   # sin(theta_tau)
  2  1    -7.07274570E-01   # -sin(theta_tau)
  2  2    -7.06938953E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00153288E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.59545774E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43944778E+02   # higgs               
         4     1.03649807E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.59545774E+03  # The gauge couplings
     1     3.62358716E-01   # gprime(Q) DRbar
     2     6.38225796E-01   # g(Q) DRbar
     3     1.02494882E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.59545774E+03  # The trilinear couplings
  1  1     2.42323542E-06   # A_u(Q) DRbar
  2  2     2.42327242E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.59545774E+03  # The trilinear couplings
  1  1     8.49453452E-07   # A_d(Q) DRbar
  2  2     8.49555917E-07   # A_s(Q) DRbar
  3  3     1.75376864E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.59545774E+03  # The trilinear couplings
  1  1     3.78926460E-07   # A_e(Q) DRbar
  2  2     3.78945261E-07   # A_mu(Q) DRbar
  3  3     3.84264635E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.59545774E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51478335E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.59545774E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.84186210E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.59545774E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02465341E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.59545774E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.85151921E+04   # M^2_Hd              
        22    -9.04599041E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41399504E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.69106969E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47945342E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47945342E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52054658E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52054658E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.33800310E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.65341212E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.06339236E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.77126643E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14099196E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.76625567E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.03948269E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.11302891E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.18618069E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.97661593E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67852862E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60746948E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.15042171E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.16534604E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.81769282E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.49452979E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.32370092E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.38371278E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.08002667E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.60240852E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     4.57630814E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.48388707E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.60239441E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.46061917E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.16574015E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.02479005E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.48910187E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.95209949E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.99764655E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.62202040E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.13987090E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.51672552E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.24591643E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.55318381E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.07208591E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16968239E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59527940E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.93377351E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.21995822E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.29762816E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40472029E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.95715699E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.97105176E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.62113814E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.17860431E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     7.02896409E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.24021156E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.79438514E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.07893477E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65787046E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.54373016E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.69002354E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.20158902E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.40467905E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54562690E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.95209949E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.99764655E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.62202040E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.13987090E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.51672552E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.24591643E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.55318381E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.07208591E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16968239E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59527940E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.93377351E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.21995822E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.29762816E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40472029E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.95715699E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.97105176E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.62113814E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.17860431E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     7.02896409E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.24021156E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.79438514E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.07893477E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65787046E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.54373016E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.69002354E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.20158902E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.40467905E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54562690E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89415440E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.99822397E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00192983E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.58455907E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.46717778E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.99824745E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.81309211E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55370295E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999961E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.71080813E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.92172701E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.01611475E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.89415440E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.99822397E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00192983E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.58455907E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.46717778E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.99824745E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.81309211E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55370295E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999961E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.71080813E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.92172701E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.01611475E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.75073338E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.52944226E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.15838240E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31217534E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.69499631E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.61093489E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13110232E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.40324091E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.21855206E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.25755992E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.40689220E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89435881E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97957550E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99893539E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.83487759E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.18835777E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00310695E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.39050929E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89435881E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97957550E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99893539E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.83487759E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.18835777E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00310695E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.39050929E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89461374E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97872608E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99867641E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.76665942E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.09999861E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00343711E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.37596685E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.94868643E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.86282231E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.35064118E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.52911396E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.35218558E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.35272983E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.03839170E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.18580415E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.62548933E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.05607322E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99930491E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.95086909E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.88690098E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.07744308E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.69313657E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.16801304E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.16801304E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.04403803E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.69283235E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.29388821E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.29388821E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.63934137E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.63934137E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.84823090E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.84823090E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.78791284E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01978775E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.67223123E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.23880098E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.23880098E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.13188318E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.96005958E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.26548119E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.26548119E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.71437801E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.71437801E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.82143265E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.82143265E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.63330664E-03   # h decays
#          BR         NDA      ID1       ID2
     5.62559546E-01    2           5        -5   # BR(h -> b       bb     )
     7.14199666E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.52828370E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.36557723E-04    2           3        -3   # BR(h -> s       sb     )
     2.33139572E-02    2           4        -4   # BR(h -> c       cb     )
     7.58103513E-02    2          21        21   # BR(h -> g       g      )
     2.59718135E-03    2          22        22   # BR(h -> gam     gam    )
     1.73325675E-03    2          22        23   # BR(h -> Z       gam    )
     2.32652158E-01    2          24       -24   # BR(h -> W+      W-     )
     2.91241968E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.57818448E+01   # H decays
#          BR         NDA      ID1       ID2
     8.97416664E-01    2           5        -5   # BR(H -> b       bb     )
     6.94972146E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.45725543E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.01752165E-04    2           3        -3   # BR(H -> s       sb     )
     8.45826880E-08    2           4        -4   # BR(H -> c       cb     )
     8.43766285E-03    2           6        -6   # BR(H -> t       tb     )
     1.08723514E-05    2          21        21   # BR(H -> g       g      )
     5.53899057E-08    2          22        22   # BR(H -> gam     gam    )
     3.45303761E-09    2          23        22   # BR(H -> Z       gam    )
     7.99226675E-07    2          24       -24   # BR(H -> W+      W-     )
     3.99120497E-07    2          23        23   # BR(H -> Z       Z      )
     5.78543875E-06    2          25        25   # BR(H -> h       h      )
    -2.62139988E-24    2          36        36   # BR(H -> A       A      )
     8.83919775E-20    2          23        36   # BR(H -> Z       A      )
     8.64744153E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.29032165E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.32236776E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.74380199E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24634579E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.25908500E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.49846731E+01   # A decays
#          BR         NDA      ID1       ID2
     9.17895872E-01    2           5        -5   # BR(A -> b       bb     )
     7.10800972E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.51321887E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.08675464E-04    2           3        -3   # BR(A -> s       sb     )
     8.71038407E-08    2           4        -4   # BR(A -> c       cb     )
     8.68438490E-03    2           6        -6   # BR(A -> t       tb     )
     2.55746825E-05    2          21        21   # BR(A -> g       g      )
     6.73127504E-08    2          22        22   # BR(A -> gam     gam    )
     2.51819265E-08    2          23        22   # BR(A -> Z       gam    )
     8.14378857E-07    2          23        25   # BR(A -> Z       h      )
     9.46502932E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.45372521E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.73062918E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.88977025E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.83152827E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46442563E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.49249682E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.29558852E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.37231249E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.34905867E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.77544748E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.21718268E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.44865843E-07    2          24        25   # BR(H+ -> W+      h      )
     5.31392585E-14    2          24        36   # BR(H+ -> W+      A      )
     5.20399402E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.87049964E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08412269E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

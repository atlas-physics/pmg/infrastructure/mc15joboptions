#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13937838E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.48990000E+02   # M_1(MX)             
         2     6.97990000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04036583E+01   # W+
        25     1.25505637E+02   # h
        35     3.00013546E+03   # H
        36     2.99999992E+03   # A
        37     3.00109366E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03092271E+03   # ~d_L
   2000001     3.02580901E+03   # ~d_R
   1000002     3.03001370E+03   # ~u_L
   2000002     3.02819090E+03   # ~u_R
   1000003     3.03092271E+03   # ~s_L
   2000003     3.02580901E+03   # ~s_R
   1000004     3.03001370E+03   # ~c_L
   2000004     3.02819090E+03   # ~c_R
   1000005     7.44918191E+02   # ~b_1
   2000005     3.02543054E+03   # ~b_2
   1000006     7.41966895E+02   # ~t_1
   2000006     3.02206043E+03   # ~t_2
   1000011     3.00670977E+03   # ~e_L
   2000011     3.00072152E+03   # ~e_R
   1000012     3.00532431E+03   # ~nu_eL
   1000013     3.00670977E+03   # ~mu_L
   2000013     3.00072152E+03   # ~mu_R
   1000014     3.00532431E+03   # ~nu_muL
   1000015     2.98620578E+03   # ~tau_1
   2000015     3.02180137E+03   # ~tau_2
   1000016     3.00553869E+03   # ~nu_tauL
   1000021     2.33844355E+03   # ~g
   1000022     3.52299443E+02   # ~chi_10
   1000023     7.31607175E+02   # ~chi_20
   1000025    -3.00174872E+03   # ~chi_30
   1000035     3.00209200E+03   # ~chi_40
   1000024     7.31766618E+02   # ~chi_1+
   1000037     3.00287675E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889575E-01   # N_11
  1  2    -5.51507362E-05   # N_12
  1  3    -1.48275317E-02   # N_13
  1  4    -9.89209664E-04   # N_14
  2  1     4.60959752E-04   # N_21
  2  2     9.99621951E-01   # N_22
  2  3     2.70334481E-02   # N_23
  2  4     4.99353708E-03   # N_24
  3  1    -9.78167836E-03   # N_31
  3  2     1.55881696E-02   # N_32
  3  3    -7.06857022E-01   # N_33
  3  4     7.07117018E-01   # N_34
  4  1     1.11778155E-02   # N_41
  4  2    -2.26486454E-02   # N_42
  4  3     7.06684149E-01   # N_43
  4  4     7.07078219E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99269071E-01   # U_11
  1  2     3.82272738E-02   # U_12
  2  1    -3.82272738E-02   # U_21
  2  2     9.99269071E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99975078E-01   # V_11
  1  2    -7.05992050E-03   # V_12
  2  1    -7.05992050E-03   # V_21
  2  2    -9.99975078E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98598254E-01   # cos(theta_t)
  1  2    -5.29294541E-02   # sin(theta_t)
  2  1     5.29294541E-02   # -sin(theta_t)
  2  2     9.98598254E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99729469E-01   # cos(theta_b)
  1  2    -2.32591662E-02   # sin(theta_b)
  2  1     2.32591662E-02   # -sin(theta_b)
  2  2     9.99729469E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06947952E-01   # cos(theta_tau)
  1  2     7.07265575E-01   # sin(theta_tau)
  2  1    -7.07265575E-01   # -sin(theta_tau)
  2  2    -7.06947952E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00113126E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39378384E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44225367E+02   # higgs               
         4     1.10362009E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39378384E+03  # The gauge couplings
     1     3.61904341E-01   # gprime(Q) DRbar
     2     6.36813846E-01   # g(Q) DRbar
     3     1.02797967E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39378384E+03  # The trilinear couplings
  1  1     1.84521723E-06   # A_u(Q) DRbar
  2  2     1.84524394E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39378384E+03  # The trilinear couplings
  1  1     6.64241325E-07   # A_d(Q) DRbar
  2  2     6.64316820E-07   # A_s(Q) DRbar
  3  3     1.33794693E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39378384E+03  # The trilinear couplings
  1  1     2.84944377E-07   # A_e(Q) DRbar
  2  2     2.84958221E-07   # A_mu(Q) DRbar
  3  3     2.88827074E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39378384E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.54671621E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39378384E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.85765616E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39378384E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01206079E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39378384E+03  # The soft SUSY breaking masses at the scale Q
         1     3.48990000E+02   # M_1(Q)              
         2     6.97990000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.94517394E+04   # M^2_Hd              
        22    -9.09242520E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40751844E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.61697826E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48013286E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48013286E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51986714E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51986714E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.63142494E-02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.16398126E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.36018739E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.18382981E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.44610246E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.14585906E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.32845134E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.34714876E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.92224256E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.71141566E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62356449E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.18805802E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.28582470E-02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.43183411E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.68165886E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.47295676E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.96393424E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     7.97700498E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.13358851E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.02780685E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.58318983E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.40909594E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.35830699E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.04166891E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.51659425E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.69015714E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.07725610E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.54748446E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.32495261E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.89091895E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.09645092E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.46771287E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.29529157E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17843417E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55675653E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.02228616E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.42929386E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     6.91173662E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44324304E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.69544261E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.08150676E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.54631186E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.26963112E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.47220619E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.09081703E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.01961806E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.30205335E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67127535E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.42576016E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.59088330E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.28651709E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.63362825E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55742387E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.69015714E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.07725610E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.54748446E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.32495261E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.89091895E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.09645092E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.46771287E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.29529157E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17843417E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55675653E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.02228616E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.42929386E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     6.91173662E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44324304E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.69544261E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.08150676E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.54631186E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.26963112E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.47220619E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.09081703E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.01961806E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.30205335E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67127535E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.42576016E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.59088330E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.28651709E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.63362825E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55742387E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59789922E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05862585E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98301199E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.68643967E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.43367337E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95836208E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.39660640E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52062367E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999807E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93306939E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.59789922E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05862585E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98301199E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.68643967E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.43367337E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95836208E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.39660640E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52062367E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999807E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93306939E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.58301172E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.67828979E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10921831E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.21249190E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53344584E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76102583E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.08160158E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.72412150E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     8.04248619E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15710098E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.39459252E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59791077E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.05851834E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97815828E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     8.52168387E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.30987445E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96332335E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     8.90401635E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59791077E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.05851834E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97815828E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     8.52168387E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.30987445E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96332335E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     8.90401635E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59855453E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.05840868E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97789142E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     9.57110096E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.48891008E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96369835E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.52158254E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.30606331E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.36572453E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.38144830E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.66733963E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.10880029E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.82260457E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.90714737E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.66828955E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.11401817E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.18703569E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.37280725E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.62719275E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.38538549E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.10816644E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.93860747E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.65289693E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.65289693E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.76704665E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.89659982E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.36473342E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.36473342E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.78965867E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.78965867E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.73370001E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     3.73370001E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.73370001E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     3.73370001E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     7.10234812E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     7.10234812E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.29246081E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.57679372E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.88873319E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.71188095E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.71188095E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.16239129E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.24627115E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.33527386E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.33527386E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.85778485E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.85778485E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     8.76362896E-12    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     8.76362896E-12    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.76362896E-12    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     8.76362896E-12    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.81417722E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.81417722E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.66680163E-03   # h decays
#          BR         NDA      ID1       ID2
     5.53456905E-01    2           5        -5   # BR(h -> b       bb     )
     7.09932343E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.51315710E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.33002360E-04    2           3        -3   # BR(h -> s       sb     )
     2.31631156E-02    2           4        -4   # BR(h -> c       cb     )
     7.57804801E-02    2          21        21   # BR(h -> g       g      )
     2.61023763E-03    2          22        22   # BR(h -> gam     gam    )
     1.78272257E-03    2          22        23   # BR(h -> Z       gam    )
     2.41031576E-01    2          24       -24   # BR(h -> W+      W-     )
     3.03974116E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.84885051E+01   # H decays
#          BR         NDA      ID1       ID2
     9.03722150E-01    2           5        -5   # BR(H -> b       bb     )
     6.46117493E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.28451706E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.80538762E-04    2           3        -3   # BR(H -> s       sb     )
     7.86238131E-08    2           4        -4   # BR(H -> c       cb     )
     7.84323193E-03    2           6        -6   # BR(H -> t       tb     )
     6.68900353E-06    2          21        21   # BR(H -> g       g      )
     6.07864572E-08    2          22        22   # BR(H -> gam     gam    )
     3.21888730E-09    2          23        22   # BR(H -> Z       gam    )
     6.41944294E-07    2          24       -24   # BR(H -> W+      W-     )
     3.20576494E-07    2          23        23   # BR(H -> Z       Z      )
     5.22559873E-06    2          25        25   # BR(H -> h       h      )
     7.81845279E-25    2          36        36   # BR(H -> A       A      )
     1.31086362E-18    2          23        36   # BR(H -> Z       A      )
     6.53270268E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.83871058E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.26380164E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.30336837E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.20472682E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.21547649E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.76532235E+01   # A decays
#          BR         NDA      ID1       ID2
     9.23776516E-01    2           5        -5   # BR(A -> b       bb     )
     6.60425248E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.33510259E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.86799087E-04    2           3        -3   # BR(A -> s       sb     )
     8.09306372E-08    2           4        -4   # BR(A -> c       cb     )
     8.06890716E-03    2           6        -6   # BR(A -> t       tb     )
     2.37621596E-05    2          21        21   # BR(A -> g       g      )
     6.24885081E-08    2          22        22   # BR(A -> gam     gam    )
     2.34181341E-08    2          23        22   # BR(A -> Z       gam    )
     6.53651113E-07    2          23        25   # BR(A -> Z       h      )
     8.44476835E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.09768339E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.21837529E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.59868693E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.12894573E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47332700E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.02483229E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13023374E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.42928124E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.25188376E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.57552746E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.26712646E-01    2           6        -5   # BR(H+ -> t       bb     )
     5.97111949E-07    2          24        25   # BR(H+ -> W+      h      )
     4.98772298E-14    2          24        36   # BR(H+ -> W+      A      )
     4.51686075E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.67263822E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.06208969E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

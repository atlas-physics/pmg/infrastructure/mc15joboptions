#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13003846E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -2.59900000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59900000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04180184E+01   # W+
        25     1.26085948E+02   # h
        35     4.00000632E+03   # H
        36     3.99999662E+03   # A
        37     4.00106163E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02591658E+03   # ~d_L
   2000001     4.02257462E+03   # ~d_R
   1000002     4.02526649E+03   # ~u_L
   2000002     4.02331611E+03   # ~u_R
   1000003     4.02591658E+03   # ~s_L
   2000003     4.02257462E+03   # ~s_R
   1000004     4.02526649E+03   # ~c_L
   2000004     4.02331611E+03   # ~c_R
   1000005     1.01447284E+03   # ~b_1
   2000005     4.02540131E+03   # ~b_2
   1000006     9.87132577E+02   # ~t_1
   2000006     1.80222398E+03   # ~t_2
   1000011     4.00454586E+03   # ~e_L
   2000011     4.00349549E+03   # ~e_R
   1000012     4.00343420E+03   # ~nu_eL
   1000013     4.00454586E+03   # ~mu_L
   2000013     4.00349549E+03   # ~mu_R
   1000014     4.00343420E+03   # ~nu_muL
   1000015     4.00501257E+03   # ~tau_1
   2000015     4.00734538E+03   # ~tau_2
   1000016     4.00487448E+03   # ~nu_tauL
   1000021     1.97719509E+03   # ~g
   1000022     2.01329102E+02   # ~chi_10
   1000023    -2.70465044E+02   # ~chi_20
   1000025     2.79352725E+02   # ~chi_30
   1000035     2.05217587E+03   # ~chi_40
   1000024     2.67421455E+02   # ~chi_1+
   1000037     2.05234056E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     8.96968004E-01   # N_11
  1  2    -1.05275681E-02   # N_12
  1  3    -3.55299233E-01   # N_13
  1  4    -2.62868838E-01   # N_14
  2  1    -6.92285393E-02   # N_21
  2  2     2.54651477E-02   # N_22
  2  3    -7.00612168E-01   # N_23
  2  4     7.09719329E-01   # N_24
  3  1     4.36640468E-01   # N_31
  3  2     2.78485830E-02   # N_32
  3  3     6.18785281E-01   # N_33
  3  4     6.52437227E-01   # N_34
  4  1    -9.54744607E-04   # N_41
  4  2     9.99232282E-01   # N_42
  4  3    -3.13394376E-03   # N_43
  4  4    -3.90399010E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     4.43722767E-03   # U_11
  1  2     9.99990155E-01   # U_12
  2  1    -9.99990155E-01   # U_21
  2  2     4.43722767E-03   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.52231476E-02   # V_11
  1  2    -9.98474038E-01   # V_12
  2  1    -9.98474038E-01   # V_21
  2  2    -5.52231476E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85771561E-01   # cos(theta_t)
  1  2    -1.68090540E-01   # sin(theta_t)
  2  1     1.68090540E-01   # -sin(theta_t)
  2  2     9.85771561E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99999648E-01   # cos(theta_b)
  1  2    -8.39047005E-04   # sin(theta_b)
  2  1     8.39047005E-04   # -sin(theta_b)
  2  2     9.99999648E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.05402262E-01   # cos(theta_tau)
  1  2     7.08807201E-01   # sin(theta_tau)
  2  1    -7.08807201E-01   # -sin(theta_tau)
  2  2    -7.05402262E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00301997E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30038463E+03  # DRbar Higgs Parameters
         1    -2.59900000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43542238E+02   # higgs               
         4     1.60781552E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30038463E+03  # The gauge couplings
     1     3.62000071E-01   # gprime(Q) DRbar
     2     6.36414876E-01   # g(Q) DRbar
     3     1.03007644E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30038463E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36815014E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30038463E+03  # The trilinear couplings
  1  1     5.01377568E-07   # A_d(Q) DRbar
  2  2     5.01424389E-07   # A_s(Q) DRbar
  3  3     8.98873504E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30038463E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.09291063E-07   # A_mu(Q) DRbar
  3  3     1.10416134E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30038463E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68884045E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30038463E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.83040604E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30038463E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03774802E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30038463E+03  # The soft SUSY breaking masses at the scale Q
         1     2.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.58226579E+07   # M^2_Hd              
        22    -1.82923297E+04   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59899997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40512672E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.69801695E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.36929487E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.36929487E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.61790463E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.61790463E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     1.28004930E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     1.28004930E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     1.32805455E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     5.70740837E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.27038054E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.05736083E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.10151779E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13973488E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     4.64170101E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12351727E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.36621477E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.17442807E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.61969387E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.27431239E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.40725682E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.37753468E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.23469480E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.84061913E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.77894353E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.91457425E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.65795554E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.54654952E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.80020197E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.65516589E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.87969494E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.64396919E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.61691875E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.13220169E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.10282446E-05    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.24305653E-05    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.53737223E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     2.61927210E-06    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77837721E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     2.22547089E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.32619419E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.06559079E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.83051157E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.31363912E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.64878263E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51371371E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.60170892E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     4.49023653E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.66466849E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.05942829E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.81410641E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.44236857E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77857172E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.85179854E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.29728952E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.62779722E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.83536021E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.43082287E-06    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.68080412E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51590619E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53408370E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.17182145E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.95400195E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.76479601E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     7.34213822E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85447442E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77837721E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.22547089E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.32619419E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.06559079E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.83051157E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.31363912E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.64878263E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51371371E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.60170892E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     4.49023653E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.66466849E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.05942829E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.81410641E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.44236857E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77857172E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.85179854E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.29728952E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.62779722E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.83536021E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.43082287E-06    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.68080412E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51590619E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53408370E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.17182145E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.95400195E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.76479601E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     7.34213822E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85447442E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.15225734E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.27143695E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     9.81686179E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.86695204E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.77561689E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.99748575E-05    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.56506952E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.07501173E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.05287153E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     4.77741842E-03    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.89934930E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     4.98419139E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.15225734E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.27143695E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     9.81686179E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.86695204E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.77561689E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.99748575E-05    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.56506952E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.07501173E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.05287153E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     4.77741842E-03    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.89934930E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     4.98419139E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.10815019E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.91008243E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.28426961E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.18409396E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.39712112E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.44749489E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.80121060E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.10741437E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.02765942E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.40513929E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.55081644E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.42102667E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.10854235E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.84912932E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.15331713E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.37999339E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.13108324E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.46315947E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.77885097E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.09195991E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.54260926E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.15331713E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.37999339E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.13108324E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.46315947E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.77885097E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.09195991E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.54260926E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.48369695E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.24957431E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.92968657E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.23038113E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.51751085E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.69233142E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02134672E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     3.74688812E-04   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33451244E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33451244E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11151290E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11151290E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10794932E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.97051132E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.49594252E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.84893234E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.31140912E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     8.10277555E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     9.33090529E-03    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.91888993E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.92910026E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     8.01413797E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     3.11629072E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.23885097E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.17491777E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52399173E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17491777E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52399173E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.45590476E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.49427148E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.49427148E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.47276529E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.98629631E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.98629631E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.98629607E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.24627889E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.24627889E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.24627889E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.24627889E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.08209437E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.08209437E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.08209437E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.08209437E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.90545340E-08    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.90545340E-08    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.17728090E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.81857054E-03    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     7.15834709E-03    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     1.14908680E-01    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.48831959E-01    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.14908680E-01    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.48831959E-01    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.44571926E-01    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     3.39498507E-02    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     3.39498507E-02    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     3.38337299E-02    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     6.83044344E-02    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     6.83044344E-02    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     6.83043859E-02    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     1.20079579E-04    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     1.55763685E-04    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     1.20079579E-04    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     1.55763685E-04    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     3.57192183E-05    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     3.57192183E-05    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     2.76973185E-05    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     7.13997674E-05    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     7.13997674E-05    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     7.13997681E-05    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.76307074E-03    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.76307074E-03    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.76307074E-03    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.76307074E-03    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     5.87685424E-04    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     5.87685424E-04    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     5.87685424E-04    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     5.87685424E-04    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     5.27573693E-04    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     5.27573693E-04    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.96941998E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.53947922E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     4.72342249E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.92431835E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.89894161E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.89894161E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.97170707E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.35455394E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.14208813E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.70410182E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.70410182E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.90504132E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.90504132E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.70023844E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.70023844E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.11279309E-03   # h decays
#          BR         NDA      ID1       ID2
     5.85477982E-01    2           5        -5   # BR(h -> b       bb     )
     6.36360870E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.25268962E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.77327933E-04    2           3        -3   # BR(h -> s       sb     )
     2.07281029E-02    2           4        -4   # BR(h -> c       cb     )
     6.79765098E-02    2          21        21   # BR(h -> g       g      )
     2.37791190E-03    2          22        22   # BR(h -> gam     gam    )
     1.67330124E-03    2          22        23   # BR(h -> Z       gam    )
     2.28320804E-01    2          24       -24   # BR(h -> W+      W-     )
     2.91067047E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.48552198E+01   # H decays
#          BR         NDA      ID1       ID2
     3.49681105E-01    2           5        -5   # BR(H -> b       bb     )
     6.04427165E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.13710806E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.52783041E-04    2           3        -3   # BR(H -> s       sb     )
     7.09105321E-08    2           4        -4   # BR(H -> c       cb     )
     7.10671167E-03    2           6        -6   # BR(H -> t       tb     )
     7.56534211E-07    2          21        21   # BR(H -> g       g      )
     3.22997933E-10    2          22        22   # BR(H -> gam     gam    )
     1.82486656E-09    2          23        22   # BR(H -> Z       gam    )
     1.96923157E-06    2          24       -24   # BR(H -> W+      W-     )
     9.83933682E-07    2          23        23   # BR(H -> Z       Z      )
     7.56216605E-06    2          25        25   # BR(H -> h       h      )
     4.38380468E-24    2          36        36   # BR(H -> A       A      )
    -1.86226206E-20    2          23        36   # BR(H -> Z       A      )
     1.84745892E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.63185789E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.63185789E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     1.98233263E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.03681605E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.07506504E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.81110651E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.79038243E-02    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     2.06624888E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     9.62510475E-03    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     7.98268275E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     6.31819003E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     3.68254150E-04    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     4.08922396E-05    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.37065625E-03    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.37065625E-03    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.27759798E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.48514229E+01   # A decays
#          BR         NDA      ID1       ID2
     3.49731089E-01    2           5        -5   # BR(A -> b       bb     )
     6.04472359E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.13726617E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.52842975E-04    2           3        -3   # BR(A -> s       sb     )
     7.13639197E-08    2           4        -4   # BR(A -> c       cb     )
     7.11983163E-03    2           6        -6   # BR(A -> t       tb     )
     1.46294533E-05    2          21        21   # BR(A -> g       g      )
     5.32316382E-08    2          22        22   # BR(A -> gam     gam    )
     1.60840005E-08    2          23        22   # BR(A -> Z       gam    )
     1.96518039E-06    2          23        25   # BR(A -> Z       h      )
     1.85126266E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.63199288E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.63199288E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     1.72701166E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.29383728E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.87755141E-03    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.48054997E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.44111568E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     2.07252363E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.05972962E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     8.08037508E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     6.21118700E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.53619203E-03    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.53619203E-03    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.48488785E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.59467642E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.04661350E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.13793440E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.58058984E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.21117855E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.49178376E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.56419595E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.96753013E-06    2          24        25   # BR(H+ -> W+      h      )
     3.28717416E-14    2          24        36   # BR(H+ -> W+      A      )
     6.77454438E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.90150054E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     2.00954430E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.63421228E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     4.14470514E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.60749741E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     1.25496233E-01    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.92970196E-04    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.90704486E-03    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

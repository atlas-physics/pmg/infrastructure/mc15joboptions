#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13002730E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     5.09990000E+02   # M_1(MX)             
         2     2.00000000E+03   # M_2(MX)             
         3     1.80000000E+03   # M_3(MX)             
        11     2.64959179E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -5.21990000E+02   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     4.00000000E+03   # mA(pole)            
        31     4.00000000E+03   # meL(MX)             
        32     4.00000000E+03   # mmuL(MX)            
        33     4.00000000E+03   # mtauL(MX)           
        34     4.00000000E+03   # meR(MX)             
        35     4.00000000E+03   # mmuR(MX)            
        36     4.00000000E+03   # mtauR(MX)           
        41     4.00000000E+03   # mqL1(MX)            
        42     4.00000000E+03   # mqL2(MX)            
        43     9.59990000E+02   # mqL3(MX)            
        44     4.00000000E+03   # muR(MX)             
        45     4.00000000E+03   # mcR(MX)             
        46     1.78485961E+03   # mtR(MX)             
        47     4.00000000E+03   # mdR(MX)             
        48     4.00000000E+03   # msR(MX)             
        49     4.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04197259E+01   # W+
        25     1.26072421E+02   # h
        35     4.00000652E+03   # H
        36     3.99999722E+03   # A
        37     4.00103621E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     4.02590053E+03   # ~d_L
   2000001     4.02255612E+03   # ~d_R
   1000002     4.02525153E+03   # ~u_L
   2000002     4.02327275E+03   # ~u_R
   1000003     4.02590053E+03   # ~s_L
   2000003     4.02255612E+03   # ~s_R
   1000004     4.02525153E+03   # ~c_L
   2000004     4.02327275E+03   # ~c_R
   1000005     1.01771515E+03   # ~b_1
   2000005     4.02545680E+03   # ~b_2
   1000006     9.90270910E+02   # ~t_1
   2000006     1.80322499E+03   # ~t_2
   1000011     4.00451516E+03   # ~e_L
   2000011     4.00337466E+03   # ~e_R
   1000012     4.00340565E+03   # ~nu_eL
   1000013     4.00451516E+03   # ~mu_L
   2000013     4.00337466E+03   # ~mu_R
   1000014     4.00340565E+03   # ~nu_muL
   1000015     4.00376817E+03   # ~tau_1
   2000015     4.00837045E+03   # ~tau_2
   1000016     4.00482340E+03   # ~nu_tauL
   1000021     1.97718813E+03   # ~g
   1000022     4.93612288E+02   # ~chi_10
   1000023    -5.33934070E+02   # ~chi_20
   1000025     5.53995784E+02   # ~chi_30
   1000035     2.05210226E+03   # ~chi_40
   1000024     5.31887417E+02   # ~chi_1+
   1000037     2.05226651E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.58985015E-01   # N_11
  1  2    -2.13299055E-02   # N_12
  1  3    -4.78060980E-01   # N_13
  1  4    -4.41525177E-01   # N_14
  2  1    -3.16377858E-02   # N_21
  2  2     2.27714603E-02   # N_22
  2  3    -7.05136058E-01   # N_23
  2  4     7.07999754E-01   # N_24
  3  1     6.50337893E-01   # N_31
  3  2     2.78329976E-02   # N_32
  3  3     5.23607468E-01   # N_33
  3  4     5.49655500E-01   # N_34
  4  1    -1.19237850E-03   # N_41
  4  2     9.99125528E-01   # N_42
  4  3    -8.72120957E-03   # N_43
  4  4    -4.08741821E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.23397971E-02   # U_11
  1  2     9.99923862E-01   # U_12
  2  1    -9.99923862E-01   # U_21
  2  2     1.23397971E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     5.78129384E-02   # V_11
  1  2    -9.98327433E-01   # V_12
  2  1    -9.98327433E-01   # V_21
  2  2    -5.78129384E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.85642203E-01   # cos(theta_t)
  1  2    -1.68847409E-01   # sin(theta_t)
  2  1     1.68847409E-01   # -sin(theta_t)
  2  2     9.85642203E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99998519E-01   # cos(theta_b)
  1  2    -1.72104556E-03   # sin(theta_b)
  2  1     1.72104556E-03   # -sin(theta_b)
  2  2     9.99998519E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06302733E-01   # cos(theta_tau)
  1  2     7.07909916E-01   # sin(theta_tau)
  2  1    -7.07909916E-01   # -sin(theta_tau)
  2  2    -7.06302733E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00252061E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.30027302E+03  # DRbar Higgs Parameters
         1    -5.21990000E+02   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43644764E+02   # higgs               
         4     1.61267310E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.30027302E+03  # The gauge couplings
     1     3.61858560E-01   # gprime(Q) DRbar
     2     6.35644038E-01   # g(Q) DRbar
     3     1.03007346E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.30027302E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.36765075E-06   # A_c(Q) DRbar
  3  3     2.64959179E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.30027302E+03  # The trilinear couplings
  1  1     5.05539137E-07   # A_d(Q) DRbar
  2  2     5.05585554E-07   # A_s(Q) DRbar
  3  3     9.03258182E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.30027302E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.13011475E-07   # A_mu(Q) DRbar
  3  3     1.14166482E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.30027302E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.68443809E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.30027302E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.88898419E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.30027302E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02868045E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.30027302E+03  # The soft SUSY breaking masses at the scale Q
         1     5.09990000E+02   # M_1(Q)              
         2     2.00000000E+03   # M_2(Q)              
         3     1.80000000E+03   # M_3(Q)              
        21     1.56199850E+07   # M^2_Hd              
        22    -2.24135658E+05   # M^2_Hu              
        31     4.00000000E+03   # M_eL                
        32     4.00000000E+03   # M_muL               
        33     4.00000000E+03   # M_tauL              
        34     4.00000000E+03   # M_eR                
        35     4.00000000E+03   # M_muR               
        36     4.00000000E+03   # M_tauR              
        41     4.00000000E+03   # M_q1L               
        42     4.00000000E+03   # M_q2L               
        43     9.59989997E+02   # M_q3L               
        44     4.00000000E+03   # M_uR                
        45     4.00000000E+03   # M_cR                
        46     1.78485961E+03   # M_tR                
        47     4.00000000E+03   # M_dR                
        48     4.00000000E+03   # M_sR                
        49     4.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40165040E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.67433216E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.37024593E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.37024593E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.62080022E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.62080022E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     8.95385246E-04    2     2000006        -6   # BR(~g -> ~t_2  tb)
     8.95385246E-04    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     7.47977960E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.92302083E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.03379438E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.85242372E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.19076107E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.07170605E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.16616175E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.03674119E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     6.83200817E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.01582586E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.73080982E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.35659055E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.56021559E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     8.11317339E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.53136036E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.26933366E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.94054234E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     8.82587636E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.66082287E+02   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.60249717E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.84117502E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.56540903E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.63964921E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     3.68910731E-02    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.24574783E-06    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     9.11599379E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.99122549E-04    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.13503838E-04    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.86188941E-04    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     1.20350438E-05    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.77750204E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.28216557E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.38256181E-05    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.93312301E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.81992185E-02    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     5.66609513E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.62773525E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     8.51717705E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.59912965E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.14038030E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.42901464E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.28780947E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     4.39304103E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     9.45663768E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     1.77770023E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.32890362E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     7.01692371E-05    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     7.72034211E-04    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.82562073E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.58153184E-05    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.66121609E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     8.51934709E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     1.53334501E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.18620679E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.41520933E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.96374553E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.14488803E-08    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.85835884E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     1.77750204E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.28216557E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.38256181E-05    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.93312301E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.81992185E-02    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     5.66609513E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.62773525E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     8.51717705E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.59912965E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.14038030E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.42901464E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.28780947E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     4.39304103E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     9.45663768E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     1.77770023E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.32890362E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     7.01692371E-05    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     7.72034211E-04    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.82562073E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.58153184E-05    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.66121609E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     8.51934709E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     1.53334501E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.18620679E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.41520933E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.96374553E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.14488803E-08    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.85835884E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.13146308E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.40958678E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.12382899E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.83493267E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.78602110E-01    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     1.51048582E-04    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     5.58790408E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.01596676E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.78017318E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.99087443E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.20982795E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.99533286E-07    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.13146308E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.40958678E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.12382899E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.83493267E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.78602110E-01    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     1.51048582E-04    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     5.58790408E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.01596676E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.78017318E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.99087443E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.20982795E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.99533286E-07    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     3.05585885E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.07822841E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.51948353E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.66518758E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     1.41236182E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     5.59495106E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.83277872E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     3.04539645E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.08228118E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.86661556E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     2.49247749E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.45035778E-01    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.79330506E-02    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.90889148E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.13251820E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.02406717E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     8.24167995E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.79312889E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.79078564E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.31341172E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.56445850E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.13251820E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.02406717E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     8.24167995E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.79312889E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.79078564E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.31341172E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.56445850E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.45137812E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.29806913E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.48310285E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.25993078E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53513107E-01    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     9.46817872E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.05476797E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.26503392E-05   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.33724285E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.33724285E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11242804E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11242804E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.10065824E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.90057155E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.52728785E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     2.68890824E-04    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.33357335E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.96803734E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.79228215E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.76404334E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     4.96754791E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     7.87258824E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.45580309E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.14993860E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     1.18768445E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.54078821E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.18768445E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.54078821E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.35944640E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.53421415E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.53421415E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48887481E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.06570405E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.06570405E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.06570394E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.14956510E-07    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.14956510E-07    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.14956510E-07    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.14956510E-07    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.04985577E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.04985577E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.04985577E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.04985577E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.85522893E-10    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.85522893E-10    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.00657477E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.09835294E-02    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.61217722E-02    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#           BR         NDA      ID1       ID2       ID3
     2.37876788E-03    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     3.03251753E-03    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     2.37876788E-03    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     3.03251753E-03    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     2.90448198E-03    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     6.54704411E-04    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     6.54704411E-04    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     6.61500561E-04    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     1.41148347E-03    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     1.41148347E-03    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     1.41147107E-03    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.11351456E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.74193033E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.11351456E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.74193033E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     1.61436012E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.28965285E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.28965285E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.01509192E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.25732790E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.25732790E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.25732791E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.17643118E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.17643118E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.17643118E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.17643118E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     3.92138552E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     3.92138552E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     3.92138552E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     3.92138552E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     3.79838363E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     3.79838363E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.89898379E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.12015106E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.61165914E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.44664665E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     7.76505257E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.76505257E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.92637055E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.32868404E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.59747479E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.71780713E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.71780713E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.19565827E-05    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     9.19565827E-05    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     1.71362553E-01    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.71362553E-01    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
#
#         PDG            Width
DECAY        25     4.07439164E-03   # h decays
#          BR         NDA      ID1       ID2
     5.82067291E-01    2           5        -5   # BR(h -> b       bb     )
     6.42161437E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.27322396E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.81690305E-04    2           3        -3   # BR(h -> s       sb     )
     2.09216699E-02    2           4        -4   # BR(h -> c       cb     )
     6.85852413E-02    2          21        21   # BR(h -> g       g      )
     2.40038904E-03    2          22        22   # BR(h -> gam     gam    )
     1.68718279E-03    2          22        23   # BR(h -> Z       gam    )
     2.30080646E-01    2          24       -24   # BR(h -> W+      W-     )
     2.93324234E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.59085495E+01   # H decays
#          BR         NDA      ID1       ID2
     3.69848133E-01    2           5        -5   # BR(H -> b       bb     )
     5.93039947E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.09684561E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.48020687E-04    2           3        -3   # BR(H -> s       sb     )
     6.95606882E-08    2           4        -4   # BR(H -> c       cb     )
     6.97142921E-03    2           6        -6   # BR(H -> t       tb     )
     3.10464067E-07    2          21        21   # BR(H -> g       g      )
     1.00120260E-08    2          22        22   # BR(H -> gam     gam    )
     1.79472999E-09    2          23        22   # BR(H -> Z       gam    )
     1.67273595E-06    2          24       -24   # BR(H -> W+      W-     )
     8.35788660E-07    2          23        23   # BR(H -> Z       Z      )
     6.79125445E-06    2          25        25   # BR(H -> h       h      )
    -1.96797875E-24    2          36        36   # BR(H -> A       A      )
    -7.41232259E-21    2          23        36   # BR(H -> Z       A      )
     1.85262399E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.50746605E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.50746605E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     2.51505230E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.82948904E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.66944161E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     2.07302695E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.54776587E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.14281441E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     1.62384305E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     8.21440907E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     3.76978363E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
     2.35830504E-03    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     7.39149025E-04    2     2000006  -2000006   # BR(H -> ~t_2    ~t_2*  )
     1.24756000E-02    2     1000006  -2000006   # BR(H -> ~t_1    ~t_2*  )
     1.24756000E-02    2     2000006  -1000006   # BR(H -> ~t_2    ~t_1*  )
     1.34255661E-07    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     5.59050446E+01   # A decays
#          BR         NDA      ID1       ID2
     3.69897235E-01    2           5        -5   # BR(A -> b       bb     )
     5.93080181E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.09698622E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.48077768E-04    2           3        -3   # BR(A -> s       sb     )
     7.00189595E-08    2           4        -4   # BR(A -> c       cb     )
     6.98564771E-03    2           6        -6   # BR(A -> t       tb     )
     1.43537365E-05    2          21        21   # BR(A -> g       g      )
     6.89406296E-08    2          22        22   # BR(A -> gam     gam    )
     1.57855630E-08    2          23        22   # BR(A -> Z       gam    )
     1.66928558E-06    2          23        25   # BR(A -> Z       h      )
     1.91051135E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.50719478E-01    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     1.50719478E-01    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     2.22608181E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     5.04228597E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.46537450E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.43038211E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.27462808E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     3.74627067E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     1.77606710E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     6.82656411E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     4.54618008E-02    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
     1.40188090E-02    2     1000006  -2000006   # BR(A -> ~t_1    ~t_2*  )
     1.40188090E-02    2    -1000006   2000006   # BR(A -> ~t_1*   ~t_2   )
#
#         PDG            Width
DECAY        37     5.61785985E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.96530500E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.90345561E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.08731727E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.81779200E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.18250397E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.43279094E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.79401324E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.66303369E-06    2          24        25   # BR(H+ -> W+      h      )
     2.83605121E-14    2          24        36   # BR(H+ -> W+      A      )
     4.34292555E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.34332461E-05    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.74497618E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.50220640E-01    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     6.87210437E-02    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     1.49657352E-01    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     8.29784074E-02    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.21398917E-03    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )
     2.67343899E-02    2     2000006  -1000005   # BR(H+ -> ~t_2    ~b_1*  )

include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa mumugamma + 0,1j@NLO + 2,3j@LO with 70<pT_y<140."
evgenConfig.keywords = ["SM", "2muon", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "mumugamma_pty_70_140"

genSeq.Sherpa_i.RunCard = """
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=3; LJET:=3,4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;

  % additional library for selector fix
  SHERPA_LDADD = SelectorsFixed
}(run)

(processes){
  Process 93 93 -> 22 13 -13 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PTNLOFixed  22  70  140
  IsolationCut  22  0.1  2  0.10
  DeltaRNLOFixed  22  13  0.1 1000.0
  DeltaRNLOFixed  22  -13  0.1 1000.0
  Mass  13  -13  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=redlib1=5=redlib2=5=write_parameters=1" ]

genSeq.Sherpa_i.NCores = 96
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplla", "ppllaj" ]
genSeq.Sherpa_i.ExtraFiles = [ "libSelectorsFixed.so" ]


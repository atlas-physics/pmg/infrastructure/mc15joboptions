include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> e e + 0,1,2j@NLO + 3,4j@LO with 70 GeV < max(HT, pTV) < 140 GeV and mll>100 GeV and b-jet filter."
evgenConfig.keywords = ["SM", "Z", "2electron", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 100
evgenConfig.inputconfcheck = "Zee_MAXHTPTV70_140_Mll100"

Sherpa_iRunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  %settings for MAX(HT,PTV) slicing
  SHERPA_LDADD=SherpaFastjetMAXHTPTV
  HTMIN:=70
  HTMAX:=140
}(run)

(processes){
  Process 93 93 -> 11 -11 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1; 
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 11 -11 100.0 E_CMS
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter

Sherpa_iOpenLoopsLibs = [ "ppll", "ppllj", "pplljj" ]
Sherpa_iNCores = 96
Sherpa_iExtraFiles = [ "libSherpaFastjetMAXHTPTV.so" ]



from MadGraphControl.MadGraphUtils import *

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_ZbbjjaQCD'
evgenConfig.keywords+=['Z', 'VBF', 'bottom', 'photon']
evgenConfig.inputfilecheck = 'ZbbjjaQCD'

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")


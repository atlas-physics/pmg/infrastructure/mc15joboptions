include("MC15JobOptions/MadGraphControl_monoHiggs_scalar.py")

evgenConfig.description = "Simplified Model of scalar mediator for\
MonoHiggs(h->ZZ*->4l) with mDM="+str(mDM)+"GeV and mScalar="+str(mS)+"GeV"
evgenConfig.keywords = [ "Higgs", "ZZ", "4lepton", "scalar", \
                        "simplifiedModel", "BSMHiggs"]
evgenConfig.contact = ['Xiangyang Ju <xiangyang.ju@cern.ch>']

genSeq.Pythia8.Commands += [
    '25:oneChannel = on 1.0 100 23 23 ',
    '25:addChannel = on 0.0E0 100 5 -5',
    '23:onMode = off',
    '23:mMin = 2.0',
    '23:onIfMatch = 11 11',
    '23:onIfMatch = 13 13'
]

if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()

MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 4000.
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 4

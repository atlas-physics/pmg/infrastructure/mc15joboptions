include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

## To modify Higgs BR
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Particles/h0:Width_generator NULL
set /Herwig/Particles/h0:Mass_generator NULL

set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio 0.5770
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio 0.0632
set /Herwig/Particles/h0/h0->mu-,mu+;:BranchingRatio 0.000219
set /Herwig/Particles/h0/h0->c,cbar;:BranchingRatio 0.0291
set /Herwig/Particles/h0/h0->t,tbar;:BranchingRatio 0.00000
set /Herwig/Particles/h0/h0->g,g;:BranchingRatio 0.0857
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio  0.00228
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio 0.2150
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio 0.0264
decaymode h0->s,sbar; 0.000246 1 /Herwig/Decays/Hff
set /Herwig/Particles/h0/h0->s,sbar;:OnOff On
set /Herwig/Particles/h0/h0->s,sbar;:BranchingRatio 0.000246
decaymode h0->Z0,gamma; 0.00154 1 /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff On
set /Herwig/Particles/h0/h0->Z0,gamma;:BranchingRatio  0.00154
"""

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["aMcAtNlo", "Herwigpp"]
evgenConfig.description = 'MG5_aMC@NLO_Herwigpp_ttH'
evgenConfig.keywords +=  [ 'SM', 'Higgs']

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0


# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
genSeq.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

evgenConfig.description = "PYTHIA8+EVTGEN, ttH, top->any, H->Zllgam"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "2lepton", "photon", "mH125" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:doForceWidth = true',
                             '25:onMode = off',
                             '25:onIfMatch = 22 23', # Higgs decay
                             'HiggsSM:gg2Httbar = on',
                             'HiggsSM:qqbar2Httbar = on'
                             '24:onMode = off',
                             '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13 15'   
                             ]

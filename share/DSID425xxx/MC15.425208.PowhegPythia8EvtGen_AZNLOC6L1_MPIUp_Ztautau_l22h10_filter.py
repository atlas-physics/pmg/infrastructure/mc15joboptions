#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
#PowhegConfig.vdecaymode = 3   # tautau
if hasattr(PowhegConfig, "vdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.vdecaymode = 3  # tautau
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode = "z > tau+ tau-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.nEvents   *= 27 # needed for a generator filter
PowhegConfig.running_width = 1
# both options are set here to allow this to run in PowhegControl-00-02-04 and PowhegControl-00-03-XY
PowhegConfig.mass_low = 40.0 # Lower the Z mass cut-off
PowhegConfig.masswindow_low = 40.0 # Lower the Z mass cut-off

# Generate events
if hasattr(PowhegConfig, "generate"):
	# Use PowhegControl-00-02-05 and later syntax
	PowhegConfig.generate()
else:
	# Use Powheg-00-02-04 and earlier syntax
	PowhegConfig.generateRunCard()
	PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_MPIUp_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

# Set Tau Filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep22had10filter = TauFilter("lep22had10filter")
  filtSeq += lep22had10filter

filtSeq.lep22had10filter.UseNewOptions = True
filtSeq.lep22had10filter.Ntaus = 2
filtSeq.lep22had10filter.Nleptaus = 1
filtSeq.lep22had10filter.Nhadtaus = 1
filtSeq.lep22had10filter.EtaMaxlep = 2.7
filtSeq.lep22had10filter.EtaMaxhad = 2.7
filtSeq.lep22had10filter.Ptcutlep = 22000.0 #MeV
filtSeq.lep22had10filter.Ptcutlep_lead = 22000.0 #MeV
filtSeq.lep22had10filter.Ptcuthad = 10000.0 #MeV
filtSeq.lep22had10filter.Ptcuthad_lead = 10000.0 #MeV
filtSeq.lep22had10filter.filterEventNumber = 0 # keep all EventNumber events

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->tautau production with l22h10 filter and AZNLO CT10 tune'
evgenConfig.contact = ["Pier-Olivier DeViveiros <pier.deviveiros@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2tau' ]
evgenConfig.minevents = 1000

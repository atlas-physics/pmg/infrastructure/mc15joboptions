# displaced VLQ from lhe with mass VLQ 300 GeV and c*tau 5000 mm

from EvgenJobTransforms import EvgenConfig;
EvgenConfig.notesthepmcGenerators+=["MadGraph"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [
                              "ParticleDecays:limitTau0 = on",
                              "ParticleDecays:tau0Max = 1000000000.",
                              "Check:epTolErr = 100.",
                              "Next:numberShowEvent = 10"
                           ]

evgenConfig.description = "Displaced VLQ (mVLQ = 300 GeV, c*tau = 5000 mm)"
evgenConfig.process = "pp -> vlq vlq~ , vlq -> z j -> mu+ mu- j, vlq~ -> z j"
evgenConfig.keywords = ["exotic","BSM","longLived","2muon"]
evgenConfig.contact = ["Nathan Bernard, nathan.rogers.bernard@cern.ch"]
evgenConfig.inputfilecheck = '302061.pp_tptp_zjmumuj'

# JO for Pythia 8 jet jet JZ5W slice

evgenConfig.description = "Dijet truth jet slice JZ5W, with the A14 NNPDF23 LO Var2Down tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

evgenConfig.contact = ["marco.vanadia@cern.ch"]
evgenConfig.process = "QCD dijet"
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Var2Down_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 350."]

include("MC15JobOptions/JetFilter_JZ5W.py")
evgenConfig.minevents = 500

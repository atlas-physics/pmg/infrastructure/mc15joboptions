LQmass = 1800

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",
                            "LeptoQuark:qqbar2LQLQbar = on",
                            "LeptoQuark:kCoup = 0.01",                            
                            "42:0:products = 2 11",
                            "42:m0 = "+ str(LQmass),
                            "42:mMin ="+ str(LQmass-200),
                            "42:mMax ="+ str(LQmass+200)]
# EVGEN configuration
evgenConfig.description = 'Pythia 8 Pair produced scalar leptoquarks (M = "+str(LQmass)+" GeV) to di-electron jet channel, A14 tune and NNPDF23LO PDF'
evgenConfig.contact = ["Ilias Panagoulias <ilias.panagoulias@cern.ch>"]
evgenConfig.keywords    = [ 'exotic', 'BSM', 'leptoquark', 'scalar', '2electron', '2jet' ]
evgenConfig.process = "pp>LQLQ>eueu"


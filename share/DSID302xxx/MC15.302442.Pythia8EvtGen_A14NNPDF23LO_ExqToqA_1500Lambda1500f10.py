######################################################################
# Excited quarks to gamm+jet with Pythia 8 
######################################################################

evgenConfig.description = "PYTHIA 8 q* -> gamma+jet, q* mass = lambda = 1500 GeV"
evgenConfig.keywords = ["exotic","excitedQuark","photon","jets"]
evgenConfig.contact = ["leonardo.carminati@cern.ch","miguel.villaplana@cern.ch"]
evgenConfig.process = "q* -> gamma+jet"

#Excited Quark Mass (in GeV)
M_ExQ = 1500.0

#Mass Scale parameter (Lambda, in GeV)
M_Lam = M_ExQ

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
                           "ExcitedFermion:dg2dStar = on",             #switch on dg -> d*
                           "ExcitedFermion:ug2uStar = on",             #switch on ug -> u*
                           "ExcitedFermion:Lambda = "+str(M_Lam),      # Compositness scale
                           "4000001:m0="+str(M_ExQ),                   # d* mass 
                           "4000002:m0="+str(M_ExQ),                   # u* mass 
                           "4000001:onMode = off",                     # switch off all d* decays                           
                           "4000001:onIfAny = 22",                     # switch on d*->gamma+X decays
                           "4000002:onMode = off",                     # switch off all u* decays                           
                           "4000002:onIfAny = 22",                     # switch on u*->gamma+X decays

                           "ExcitedFermion:coupF = 1.",                #coupling strength of SU(2)
                           "ExcitedFermion:coupFprime = 1.",           #coupling strength of U(1)
                           "ExcitedFermion:coupFcol = 1."              #coupling strength of SU(3)
                          ]


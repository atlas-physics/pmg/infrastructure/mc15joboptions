# based on the JobOptions MC15.429313

evgenConfig.description = "Herwig7 BuiltinME Z + jets with MMHT2014 LO PDF and H7UE tune"
evgenConfig.generators  = ["Herwig7"] 
evgenConfig.keywords    = ["SM", "Z", "jets"]
evgenConfig.contact     = ['paolo.francavilla@cern.ch', 'Shu.Li@cern.ch', "daniel.rauch@desy.de"]
evgenConfig.minevents   = 500

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()
Herwig7Config.add_commands("""
## Z+jet
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEZJet
""")

# run Herwig7
Herwig7Config.run()

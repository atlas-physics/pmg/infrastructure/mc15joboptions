evgenConfig.description = "Low-pT Pythia8 inelastic minimum bias events for pileup, with the A3 NNPDF23LO tune and EvtGen, using low-pT jet and photon filters."
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch" ]

evgenConfig.saveJets = True

#include("MC15JobOptions/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# This is what goes inside the line above...
include("MC15JobOptions/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
    "PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    "SigmaDiffractive:PomFlux = 4",
    "SigmaDiffractive:PomFluxEpsilon = 0.07",
    "SigmaDiffractive:PomFluxAlphaPrime = 0.25",
    "ColourReconnection:range = 1.8",
    "SigmaElastic:rho = 0.0",
    "SigmaDiffractive:mode = 0",
    "SigmaTotal:sigmaXB = 6.416",
    "SigmaTotal:sigmaAX = 6.416",
    "SigmaTotal:sigmaXX = 8.798",
    "SigmaTotal:sigmaAXB = 0.",
    "SigmaDiffractive:OwnbMinDD = 5."
	]

rel = os.popen("echo $AtlasVersion").read()
print "Atlas release " + rel


evgenConfig.tune = "A3 NNPDF23LO"

include("MC15JobOptions/Pythia8_EvtGen.py")
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

genSeq.Pythia8.Commands += \
    [
    "SoftQCD:inelastic = on",
    ]

include("MC15JobOptions/JetFilter_MinbiasLow.py")

include("MC15JobOptions/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = 8000. 
filtSeq.DirectPhotonFilter.Etacut = 4.5

filtSeq.Expression = 'not DirectPhotonFilter and QCDTruthJetFilter'

# include ("GeneratorFilters/FindJets.py")
# CreateJets(prefiltSeq,filtSeq,runArgs.ecmEnergy, 0.6)
# 
# from AthenaCommon.SystemOfUnits import GeV
# filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

evgenConfig.minevents = 1000


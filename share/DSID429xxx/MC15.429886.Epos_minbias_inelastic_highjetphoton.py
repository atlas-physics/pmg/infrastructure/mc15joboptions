evgenConfig.description = "High-pT EPOS inelastic minimum bias events for pileup, using high-pT jet and photon filters."
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch" ]

evgenConfig.saveJets = True

include("MC15JobOptions/Epos_Base_Fragment.py")

include("MC15JobOptions/JetFilter_MinbiasHigh.py")

include("MC15JobOptions/DirectPhotonFilter.py")
filtSeq.DirectPhotonFilter.NPhotons = 1
filtSeq.DirectPhotonFilter.Ptmin = 8000. 
filtSeq.DirectPhotonFilter.Etacut = 4.5

filtSeq.Expression = 'DirectPhotonFilter or QCDTruthJetFilter'

#include ("GeneratorFilters/FindJets.py")
#CreateJets(prefiltSeq,filtSeq,runArgs.ecmEnergy, 0.6)

#from AthenaCommon.SystemOfUnits import GeV
#filtSeq.QCDTruthJetFilter.MaxPt = 35.*GeV

# This will remove the TestHepMC temporarily, so that it will not crash comparing 900 GeV vs. 900.002 GeV
del testSeq.TestHepMC

evgenConfig.minevents = 1000

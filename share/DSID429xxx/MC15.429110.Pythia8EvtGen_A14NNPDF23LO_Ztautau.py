# JO for test of Pythia internal PDF info

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Pythia 8 Z->tautau production to test PDF info'
evgenConfig.keywords    = [ 'SM', 'Z', 'tau', 'lepton']
evgenConfig.contact     = [ 'jmonk@cern.ch' ]

include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays]
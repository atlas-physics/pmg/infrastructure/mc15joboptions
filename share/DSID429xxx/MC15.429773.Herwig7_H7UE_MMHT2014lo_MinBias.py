# based on MC15.361223 and MC15.429307

evgenConfig.description = "Herwig7 BuiltinME Minimum bias events with MMHT2014lo68cl PDF and H7UE tune"
evgenConfig.generators  = ["Herwig7"] 
evgenConfig.keywords    = ["QCD", "minBias"]
evgenConfig.contact     = ['paolo.francavilla@cern.ch', 'Shu.Li@cern.ch', "daniel.rauch@desy.de"]
evgenConfig.minevents   = 500

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")
Herwig7Config.tune_commands()
Herwig7Config.add_commands("""
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Generators/EventGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts 
# The default MinBias cuts of the Herwig authors are X1Min == X2Min == 0.055, but to be consistent with previous productions it is set to 0.01 here.
set /Herwig/Cuts/MinBiasCuts:X1Min 0.01
set /Herwig/Cuts/MinBiasCuts:X2Min 0.01

# These settings have no effect on the MinBiasCuts, it is set here just to avoid confusion in the log file.
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/PhotonKtCut:MinKT 0.0*GeV
""")
# run Herwig7
Herwig7Config.run()

# based on the JobOptions MC15.361594 and MC15.429305
# using LHE files from mc15_13TeV.361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv.evgen.TXT.e4711

# Provide config information
evgenConfig.generators    += ["Powheg", "Herwig7", "EvtGen"] 
evgenConfig.tune           = "H7-UE-MMHT"
evgenConfig.description    = "PowhegBox/Herwig7 Diboson WZ->qqvv production"
evgenConfig.keywords       = ['electroweak', 'diboson', 'WZ', 'neutrino', '2jet']
evgenConfig.contact        = ['james.robinson@cern.ch', 'christian.johnson@cern.ch','carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'daniel.rauch@desy.de']
evgenConfig.minevents      = 5000
evgenConfig.inputfilecheck = "TXT"

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# run Herwig7
Herwig7Config.run()

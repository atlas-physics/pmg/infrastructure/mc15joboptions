include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "nu nu gamma production with up to one jet in ME+PS and pT_gamma>140 GeV with aTGC H4_Z 0.000005"
evgenConfig.keywords = [ "electroweak", "photon", "neutrino", "SM" ]
evgenConfig.contact  = [ "Evgeny.Soldatov@cern.ch" ]
evgenConfig.inputconfcheck = "nunugamma_pty_140_E_CMS_h4Z_plus0000005"
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(model){ 
  MODEL = SM+AGC 
  H4_Z = 0.000005 
  UNITARIZATION_SCALE=2000000 
  UNITARIZATION_N=4 
}(model) 

(processes){
  Process 93 93 ->  91 91 22 93{1}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  End process;
}(processes)

(selector){
  PT 22  140 E_CMS
  DeltaR 22 90 0.1 1000
  IsolationCut  22  0.3  2  0.025
}(selector)
"""

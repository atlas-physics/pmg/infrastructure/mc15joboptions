include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Electroweak opposite-sign llvvjj + 0j@LO"
evgenConfig.keywords = ["SM", "diboson", "2lepton", "jets", "VBS"]
evgenConfig.contact  = ["chris.g@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "NNPDF30NNLO_llvvjj_EW6_5gamma"

Sherpa_iRunCard="""
(run){
  ACTIVE[25]=1;
  MASS[25]=126.0;
  WIDTH[25]=0.02044   #  Gamma' = xi^4 * Gamma = 5 * 0.004088
  MASSIVE[5]=1; 
  PARTICLE_CONTAINER 901 lminus 11  12  13;
  PARTICLE_CONTAINER 902 lplus -11 -12 -13;

  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE=VAR{Abs2(p[2]+p[3]+p[4]+p[5])};
  % simplified setup as long as only 2->6 taken into account:
  %SCALES=VAR{FSF*Abs2(p[2]+p[3]+p[4]+p[5])}{RSF*Abs2(p[2]+p[3]+p[4]+p[5])}{QSF*Abs2(p[2]+p[3]+p[4]+p[5])};

  %tags for process setup
  NJET:=0; QCUT:=20.;

  EXCLUSIVE_CLUSTER_MODE=1
  SOFT_SPIN_CORRELATIONS=1

  % load new model library
  SHERPA_LDADD SherpaSMModifiedHVVCouplings;

  % model parameters
  MODEL SMModifiedHVVCouplings;
  # gVV' = xi^2 gVV = 2.236 * gVV
  LAMBDA_HZZ 2.236;
  LAMBDA_HWW 2.236;

  % improve integration performance
  PSI_ITMIN=25000;
  CDXS_VSOPT=5;
  INTEGRATION_ERROR 0.05;
}(run)

(processes){
  Process 93 93 -> 901 91 902 91 93 93 93{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 0.25 E_CMS
  Mass 13 -13 0.4614 E_CMS
  Mass 15 -15 3.804 E_CMS
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  NJetFinder 2 15. 0. 0.4 -1;
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
Sherpa_iNCores = 96
Sherpa_iExtraFiles = [ "libSherpaSMModifiedHVVCouplings.so" ]


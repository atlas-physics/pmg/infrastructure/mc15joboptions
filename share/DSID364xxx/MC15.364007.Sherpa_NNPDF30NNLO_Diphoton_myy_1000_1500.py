include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa gamma gamma + 0,1j@NLO + 2,3j@LO with 1000<m_yy<1500."
evgenConfig.keywords = ["SM", "diphoton", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Sherpa_NNPDF30NNLO_Diphoton_myy_1000_1500"

evgenConfig.process="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  ALPHAQED_DEFAULT_SCALE=0.0

  % tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=10;

  % me generator settings
  ME_SIGNAL_GENERATOR Amegic Comix LOOPGEN Internal;
  LOOPGEN:=OpenLoops;
}(run)

(processes){
  Process 21 21 -> 22 22
  ME_Generator Internal;
  Loop_Generator gg_yy
  Scales VAR{Abs2(p[2]+p[3])};
  End process;

  Process 93 93 -> 22 22 93{NJET};
  Order (*,2);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]));
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  PSI_ItMin 50000 {3}
  Integration_Error 0.99 {3}
  PSI_ItMin 100000 {4,5}
  Integration_Error 0.99 {4,5}
  End process;
}(processes)

(selector){
  "PT" 22 40,E_CMS:39,E_CMS [PT_UP]
  RapidityNLO  22  -2.7  2.7
  IsolationCut  22  0.1  2  0.10;
  Mass 22 22  1000  1500
  DeltaRNLO 22 22 0.2 1000.0;
}(selector)
"""

# switch to CutTools reduction within OpenLoops
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=redlib1=5=redlib2=5=write_parameters=1" ]

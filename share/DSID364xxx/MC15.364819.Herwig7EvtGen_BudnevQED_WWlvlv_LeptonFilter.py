evgenConfig.description = "gammagamma->WW with Budnev parameterization, leptonic decays, central lepton filter pt>15 GeV, "
evgenConfig.keywords = ["2lepton", "diboson", "SM", "WW", "electroweak", "exclusive"]
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch"]
evgenConfig.generators  = ["Herwig7", "EvtGen"]


# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

Herwig7Config.pdf_gammagamma_cmds()

include("MC15JobOptions/Herwig71_QED_EvtGen_Common.py")

commands = """
do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;

# Selected the hard process
cd /Herwig/MatrixElements
insert SubProcess:MatrixElements 0 MEgg2WW
"""

Herwig7Config.add_commands(commands)
del commands

Herwig7Config.run()

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2

evgenConfig.minevents = 5000

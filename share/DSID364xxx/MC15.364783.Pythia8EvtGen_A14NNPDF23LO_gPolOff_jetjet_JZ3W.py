# JO for Pythia 8 jet jet JZ3W slice

evgenConfig.description = "Dijet truth jet slice JZ3W, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:pTHatMin = 50.",
                            "TimeShower:phiPolAsym = off",
                            "TimeShower:phiPolAsymHard = off"]

include("MC15JobOptions/JetFilter_JZ3W.py")
evgenConfig.minevents = 100

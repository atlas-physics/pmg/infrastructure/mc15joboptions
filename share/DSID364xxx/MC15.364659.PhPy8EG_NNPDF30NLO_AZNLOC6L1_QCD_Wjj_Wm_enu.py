#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, QCD Wjj->e-,nu with born suppression"
evgenConfig.keywords    = [ "SM", "VBF", "QCD" ]
evgenConfig.contact     = [ "Christian Johnson <cjohnson@cern.ch>" ]
evgenConfig.process     = "QCD Wjj"
evgenConfig.inputfilecheck = 'QCD_Wjj_Wm_enu'
evgenConfig.minevents   = 5000

evgenConfig.description = 'Herwig7 NLO dijets with MMHT2014 and angular ordered shower, slice JZ 11'
evgenConfig.keywords+=['SM','QCD','jets']
evgenConfig.contact = ['javier.llorente.merino@cern.ch']
evgenConfig.minevents = 100

include("MC15JobOptions/Herwig7EvtGen_Matchbox_MadGraph_H7UEMMHT2014_multijet_withGridpack.py")

include("MC15JobOptions/JetFilter_JZ11.py")

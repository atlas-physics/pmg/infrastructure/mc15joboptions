include('MC15JobOptions/MadGraphControl_Vjets_NLO_FxFx_VBFMjjIntervalFilter.py')
evgenConfig.minevents = 200

## Example configuration for VBFMjjIntervalFilter setting up defaults

include("MC15JobOptions/AntiKt4TruthJets_pileup.py")
include("MC15JobOptions/AntiKt6TruthJets_pileup.py")
include("MC15JobOptions/JetFilter_Fragment.py")

if not hasattr( filtSeq, "VBFMjjIntervalFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
    filtSeq += VBFMjjIntervalFilter()
    pass

VBFMjjIntervalFilter = filtSeq.VBFMjjIntervalFilter
VBFMjjIntervalFilter.RapidityAcceptance = 5.0
VBFMjjIntervalFilter.MinSecondJetPT = 15000.0
VBFMjjIntervalFilter.MinOverlapPT = 15000.0
VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
VBFMjjIntervalFilter.NoJetProbability = 0.0002
VBFMjjIntervalFilter.OneJetProbability = 0.001
VBFMjjIntervalFilter.LowMjjProbability = 0.005
VBFMjjIntervalFilter.HighMjjProbability = 1.0
VBFMjjIntervalFilter.LowMjj = 100000.0
VBFMjjIntervalFilter.HighMjj = 800000.0
VBFMjjIntervalFilter.ElectronJetOverlapRemoval = True
#VBFMjjIntervalFilter.PhotonJetOverlapRemoval = False
#VBFMjjIntervalFilter.TauJetOverlapRemoval = False

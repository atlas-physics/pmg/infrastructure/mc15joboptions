include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'Shower lvlv FxFx events'
evgenConfig.contact = ['Dominik Duda <dominik.duda@cern.ch>']
evgenConfig.keywords+=['WW']
evgenConfig.inputfilecheck = 'lvlv_1jNLO'
evgenConfig.minevents = 5000

genSeq.Pythia8.Commands += [ 'JetMatching:merge         = on',
                             'JetMatching:scheme        = 1',
                             'JetMatching:setMad        = off',
                             'JetMatching:qCut          = 20',
                             'JetMatching:coneRadius    = 1.0',
                             'JetMatching:etaJetMax     = 1000.0',
                             'JetMatching:doFxFx        = on',
                             'JetMatching:qCutME        = 10.0',
                             'JetMatching:nJetMax       = 1' ]

genSeq.Pythia8.UserHook = 'JetMatchingMadgraph'
genSeq.Pythia8.FxFxXS = True


# Pythia8 minimum bias ND with A3

evgenConfig.description = "ND Minbias with Pythia8 A3"
evgenConfig.keywords = ["minBias","diffraction","ND"]
evgenConfig.generators = ["Pythia8"]

include("MC15JobOptions/nonStandard/Pythia8_A3_ALT_NNPDF23LO_Common.py")


genSeq.Pythia8.Commands += ["SoftQCD:nonDiffractive = on"]

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
if  "ForwardProtonFilter" not in filtSeq:
    filtSeq += ForwardProtonFilter()

filtSeq.ForwardProtonFilter.xi_min = 0.00
filtSeq.ForwardProtonFilter.xi_max = 0.20
filtSeq.ForwardProtonFilter.beam_energy = 6500.*GeV
filtSeq.ForwardProtonFilter.pt_min = 0.0*GeV
filtSeq.ForwardProtonFilter.pt_max = 1.5*GeV


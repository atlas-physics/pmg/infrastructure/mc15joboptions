include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa+OpenLoops gg->W+(lnu)W-(qq) + 0,1j."
evgenConfig.keywords = ["SM", "diboson", "1lepton" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "ggWmlvWpqq"  # general gg->W+W- input

Sherpa_iRunCard="""
(run){
  % scales, tags for scale variations
  SCALES STRICT_METS{MU_F2}{MU_R2}{MU_Q2};
  ## for mu_F=mu_R=mu_Q = mVV/2
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  % tags for process setup
  NJET:=1; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  INTEGRATION_ERROR=0.1
  EXCLUSIVE_CLUSTER_MODE 1;
  AMEGIC_ALLOW_MAPPING=0
  INTEGRATOR=Rambo

  MASS[25]=125.
  WIDTH[25]=0.004088

  % decay setup
  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,12,-11]=2
  HDH_STATUS[24,14,-13]=2
  HDH_STATUS[24,16,-15]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2
  HARD_SPIN_CORRELATIONS=0
}(run)

(processes){
  Process 93 93 -> 24 -24 93{NJET}
  CKKW sqr(QCUT/E_CMS);
  Order (2,2) {2}
  Order (3,2) {3}
  Enable_MHV 10 # initialises external process
  Loop_Generator LOOPGEN;
  End process;
}(processes)

(selector){
  PT 23 1.0 E_CMS
  PT 24 1.0 E_CMS
  PT -24 1.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "WIDTH[24]=0", "OL_PARAMETERS=preset=3=write_parameters=1" ]

Sherpa_iOpenLoopsLibs = [ "ppvv2", "ppvvj2" ]
Sherpa_iNCores = 96

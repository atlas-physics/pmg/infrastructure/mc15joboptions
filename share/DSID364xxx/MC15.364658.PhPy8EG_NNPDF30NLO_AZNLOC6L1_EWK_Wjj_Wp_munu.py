#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('MC15JobOptions/nonStandard/Pythia8_Photospp.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, EWK Wjj->e-,nu"
evgenConfig.keywords    = [ "SM", "VBF", "electroweak" ]
evgenConfig.contact     = [ "Christian Johnson <cjohnson@cern.ch>" ]
evgenConfig.process     = "EWK Wjj"
evgenConfig.inputfilecheck = 'EWK_Wjj_Wp_munu'
evgenConfig.minevents   = 5000

include("Sherpa_i/2.2.8_NNPDF30NNLO.py")

evgenConfig.description = "Sherpa mumugamma + 2,3j@LO, with 100 < m_lly < 175 GeV, pT_j1 > 35 GeV, DeltaY_jj > 2"
evgenConfig.keywords = ["SM", "2muon", "photon", "2jet" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "heberth.torres@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "mumugamma_LO_Mlly100_175_DYJJ2"

genSeq.Sherpa_i.RunCard = """
(run){
  % tags for process setup
  NJET:=1; QCUT:=20;

  % Fastjet/antikt merging criterion.
  JET_CRITERION FASTJET[A:kt,R:0.4,y:5];

  % me generator settings
  ME_SIGNAL_GENERATOR Comix;
}(run)

(processes){
  Process 93 93 -> 22 13 -13 93 93 93{NJET}
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  Cut_Core 1;
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process
}(processes)

(selector){
  PT 22  8  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaR  22  90  0.1 1000.0
  Mass  13  -13  10.0  E_CMS
  "m" 90,90,22 100,175
  FastjetSelector DY(p[5],p[6])>2.&&PPerp(p[5])>35. antikt 2 20. 0. 0.4
}(selector)
"""

genSeq.Sherpa_i.NCores = 24

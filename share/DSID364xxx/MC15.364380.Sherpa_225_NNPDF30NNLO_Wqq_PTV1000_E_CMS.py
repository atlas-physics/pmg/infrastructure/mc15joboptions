include("MC15JobOptions/Sherpa_2.2.5_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa W+/W- -> qq + 1,2,3,4j@LO with 1000 GeV < pTV < E_CMS."
evgenConfig.keywords = ["SM", "W", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Wqq_PTV1000_E_CMS"

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; QCUT:=20.;

  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2
}(run)

(processes){
  Process 93 93 -> 24 93 93{NJET};
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.05 {5,6,7,8};
  End process;

  Process 93 93 -> -24 93 93{NJET};
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.05 {5,6,7,8};
  End process;
}(processes)

(selector){
  PT 24 1000.0 E_CMS
  PT -24 1000.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0" ]
genSeq.Sherpa_i.NCores = 96



evgenConfig.description = 'MadGraph multijets with pdf NNPDF30nlo and CKKW merging, HT slice 7'
evgenConfig.keywords+=['QCD', 'jets']
evgenConfig.contact = ['jdickinson@lbl.gov']

include("MC15JobOptions/MadGraphPythia8EvtGen_NNPDF30nlo_multijet_CKKW_withGridpack.py")

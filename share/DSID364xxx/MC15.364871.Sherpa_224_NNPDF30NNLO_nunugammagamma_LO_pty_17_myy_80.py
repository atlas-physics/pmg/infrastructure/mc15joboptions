include("MC15JobOptions/Sherpa_2.2.4_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa nunugammagamma + 0,1,2j@LO with pT_y>17 and m_yy>80"
evgenConfig.keywords = ["SM", "neutrino", "2photon", "LO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "heberth.torres@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "Sherpa_224_NNPDF30NNLO_nunugammagamma_LO_pty_17_myy_80"

genSeq.Sherpa_i.RunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % tags for process setup
  NJET:=2; LJET:=0; QCUT:=20;
}(run)

(processes){
  Process 93 93 -> 22 22 91 91 93{NJET}
  Order (*,4); CKKW sqr(QCUT/E_CMS)
  PSI_ItMin 20000 {4}
  PSI_ItMin 20000 {5}
  Integration_Error 0.99 {5}
  PSI_ItMin 50000 {6}
  Integration_Error 0.99 {6}
  End process
}(processes)

(selector){
  "PT"  22  17,E_CMS:17,E_CMS [PT_UP]
  IsolationCut  22  0.1  2  0.10
  Mass  22   22  80.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 48

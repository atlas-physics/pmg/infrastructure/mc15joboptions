evgenConfig.description = "Herwig7 multijets with pdf NNPDF30nlo, slice JZ1"
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.keywords+=['QCD', 'jets']
evgenConfig.contact = ['jdickinson@lbl.gov']
evgenConfig.minevents = 5000

name = runArgs.jobConfig[0]
name_info = name.split("_JZ")[1].split(".py")
slice = int(name_info[0])

minkT = {0:0,1:0,2:15,3:50,4:150,5:350,6:600,7:950,8:1500,9:2200,10:2800,11:3500,12:4200}

# initialize Herwig7 generator configuration for built-in matrix elements
include("MC15JobOptions/Herwig7_BuiltinME.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")

command = """
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias

set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
create Herwig::MPIXSecReweighter MPIXSecReweighter
insert /Herwig/Generators/LHCGenerator:EventHandler:PostSubProcessHandlers 0 MPIXSecReweighter
 
## The cuts are set by creating a new Cuts objects.
## This is done due to an issue in 2.7.1, once it will be fixed in a new version, we can go back to the usual setup (as in DSID 187503 in MC12).
cd /Herwig/Cuts
## create ThePEG::Cuts MinBiasCuts
set MinBiasCuts:ScaleMin 2.0*GeV
set MinBiasCuts:X1Min 0.01
set MinBiasCuts:X2Min 0.01
set MinBiasCuts:MHatMin 0.0*GeV
set  /Herwig/Generators/LHCGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
set /Herwig/Cuts/JetKtCut:MinKT """+str(minkT[slice])+"""*GeV

set /Herwig/Generators/LHCGenerator:EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
read snippets/DipoleShowerFiveFlavours.in
"""
print command

Herwig7Config.add_commands(command)

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")
#Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")

# run Herwig7
Herwig7Config.run()

include("MC15JobOptions/JetFilter_JZ1.py")

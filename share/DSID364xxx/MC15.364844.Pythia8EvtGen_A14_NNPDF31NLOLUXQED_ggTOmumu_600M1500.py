include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += [
    "PDF:useHard = on",
    "PDF:pHardSet = LHAPDF6:NNPDF31_nlo_as_0118_luxqed",
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2mumu= on", # gg->mumu
    "PhaseSpace:mHatMin = 600.", # lower invariant mass
    "PhaseSpace:mHatMax = 1500." # upper invariant mass
]

include('MC15JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 5000.
MultiLeptonFilter.NLeptons = 2

evgenConfig.description = "gammagamma -> mumu production with NNPDF31NLOLUXQED, 600<M<1500GeV"
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]
evgenConfig.keywords = ["SM", "drellYan", "electroweak", "2muon"]
evgenConfig.generators += ["Pythia8"]

include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa W+/W- -> tau nu + 0,1,2j@NLO + 3,4j@LO with 140 GeV < max(HT, ptV) < 280 GeV with b-jet filter."
evgenConfig.keywords = ["SM", "W", "tau", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 100
evgenConfig.inputconfcheck = "Wtaunu_MAXHTPTV140_280"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=3; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  SOFT_SPIN_CORRELATIONS=1

  %settings for MAX(HT,PTV) slicing
  SHERPA_LDADD=SherpaFastjetMAXHTPTV
  HTMIN:=140
  HTMAX:=280
}(run)

(processes){
  Process 93 93 -> 15 -16 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;

  Process 93 93 -> -15 16 93 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Cut_Core 1;
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 15 -16 2.0 E_CMS
  Mass -15 16 2.0 E_CMS
  FastjetMAXHTPTV  HTMIN  HTMAX  antikt  20.0  0.0  0.4
}(selector)
"""

# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter


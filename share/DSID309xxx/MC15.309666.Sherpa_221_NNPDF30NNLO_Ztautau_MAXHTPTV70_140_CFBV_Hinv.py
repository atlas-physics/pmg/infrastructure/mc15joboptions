evgenConfig.description = "Sherpa Z -> tau tau + 0,1,2j@NLO + 3,4j@LO with 70 GeV < max(HT, pTV) < 140 GeV, MJJ>800, DPHI<2.5"
evgenConfig.keywords = ["SM", "Z", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch","mdshapiro@lbl.gov","bcarlson@cern.ch" ]
evgenConfig.minevents = 500
 
if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
## Disable TestHepMC for the time being, cf.  
## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())
   
   #include("MC15JobOptions/AntiKt4TruthJets.py")
   include("MC15JobOptions/VBFMjjIntervalFilter.py")

   filtSeq.VBFMjjIntervalFilter.RapidityAcceptance = 5.0 
   filtSeq.VBFMjjIntervalFilter.MinSecondJetPT = 20.*GeV
   filtSeq.VBFMjjIntervalFilter.MinOverlapPT = 20.*GeV
   filtSeq.VBFMjjIntervalFilter.TruthJetContainerName = "AntiKt4TruthJets"
   filtSeq.VBFMjjIntervalFilter.LowMjj = 800.*GeV
   filtSeq.VBFMjjIntervalFilter.HighMjj = 14000.*GeV
   
   filtSeq.VBFMjjIntervalFilter.TruncateAtLowMjj = True
   filtSeq.VBFMjjIntervalFilter.TruncateAtHighMjj = False
   filtSeq.VBFMjjIntervalFilter.PhotonJetOverlapRemoval = False
   filtSeq.VBFMjjIntervalFilter.ElectronJetOverlapRemoval = False
   filtSeq.VBFMjjIntervalFilter.TauJetOverlapRemoval = False
   filtSeq.VBFMjjIntervalFilter.ApplyWeighting = False;   
   filtSeq.VBFMjjIntervalFilter.ApplyDphi = True; 
   #filtSeq += VBFMjjIntervalFilter

 

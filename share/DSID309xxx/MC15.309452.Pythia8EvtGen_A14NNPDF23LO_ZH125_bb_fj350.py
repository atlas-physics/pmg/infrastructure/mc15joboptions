evgenConfig.description = "PYTHIA8+EVTGEN, ZH, Z->any, H->bb"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125", "ZHiggs", "inclusive" ]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]

#Higgs mass (in GeV)
H_Mass = 125.0

#Higgs width (in GeV)
H_Width = 0.00407

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands += [ 'PhaseSpace:minWidthBreitWigners = 0.001',
                             '25:m0 = '+str(H_Mass),
                             '25:mWidth = '+str(H_Width),
                             '25:doForceWidth = true',
                             '25:onMode = off',
                             '25:onIfMatch = 5 -5',
                             'HiggsSM:ffbar2HZ = on',
                             '23:onMode = off',
                             '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                             ]

#--------------------------------------------------------------
# Filter based on jet pT
#--------------------------------------------------------------
include("MC15JobOptions/JetFilterAkt10.py")

filtSeq.QCDTruthJetFilter.MinPt = 350.*GeV
filtSeq.QCDTruthJetFilter.MaxPt = 14000.*GeV

#-----------------------------------------------------------------
# http://bbgen.web.cern.ch/bbgen/Kwee/6.5TeV/index_6500GeV_BH.html
#-----------------------------------------------------------------

include( "MC15JobOptions/BeamHaloGenerator_Common.py")

# Default settings in common JO
genSeq.BeamHaloGeneratorAlg.inputType="FLUKA-RB"  
genSeq.BeamHaloGeneratorAlg.interfacePlane = -22600.0

# The probability of the event being flipped about the x-y plane.
genSeq.BeamHaloGeneratorAlg.flipProbability = 1.0

# The generator settings determine if the event is accepted.
#   * If the allowedPdgId list is not given all particles are accepted.
#   * Limits are in the form of (lower limit, upper limit)
#   * If a limit is not given it is disabled.
#   * If a limit value is -1 then it is disabled.
#   * All limits are checked against |value|
#   * r = sqrt(x^2 + y^2)
genSeq.BeamHaloGeneratorAlg.generatorSettings = []    

#
evgenConfig.inputfilecheck = 'group.phys-gener.BeamHaloGenerator.309683.beam_halo_6500GeV_80cm_IR1B1_20GeV'
evgenConfig.description = "Non collision beam background generator" 
evgenConfig.keywords = [ "monojet","longLived" ] 
evgenConfig.minevents = 5000
evgenConfig.contact = [ "olariu@cern.ch" ]

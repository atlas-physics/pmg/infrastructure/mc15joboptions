# mass of the seesaw heavy leptons
mL = 500.0

# filters
enableFilters = True
lightLeptons = False

# load configuration
include('MC15JobOptions/MadGraphControl_MGPy8EG_N30LO_A14N23LO_TypeIIISeesaw.py')

# metadata
evgenConfig.description = 'MadGraph5+Pythia8 Type III Seesaw Model, Mass: 500 GeV, Ve=Vu=Vt=0.055, dilepton + hadronic tau'
evgenConfig.contact = ['tadej.novak@cern.ch']
evgenConfig.keywords = ['BSM', 'exotic', 'seeSaw', '2lepton', '2jet']

## Pythia8 W->mu+HNL
 
evgenConfig.description = "W->e+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W", "exotic", "BSM"]
evgenConfig.contact = ["Deywis Moreno, dmoreno@cern.ch"]
evgenConfig.process = "W -> HNL e-+"
 
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
 
from EvgenProdTools.EvgenProdToolsConf import TestHepMC
genSeq += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm
 
genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 50.0 0.0 0.0 0.0 1.0 0 1 0 1 0",
                            "50:isResonance = false",
                            "50:addChannel = 1 1.00 23  11 -13 14",#decay in e- mu+ nu_mu/e+ mu- nu_mu
                            "50:mayDecay = on",
                            "WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
             "24:addchannel = 1 1. 103 -11 50",
             "ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
             "ParticleDecays:tau0Max = 600.0"]
 
 
## import generator filters for electron and muons
from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
 
 
## define muon filter
filtSeq += MultiMuonFilter("MuonFilter")
filtSeq.MuonFilter.Ptcut = 5000.
filtSeq.MuonFilter.Etacut = 2.8
filtSeq.MuonFilter.NMuons = 1
 
 
## separate filters for the leading and subleading electron
filtSeq += MultiElectronFilter("LeadingElectron")
filtSeq.LeadingElectron.Ptcut = 15000.
filtSeq.LeadingElectron.Etacut = 2.6
filtSeq.LeadingElectron.NElectrons = 1
 
 
filtSeq += MultiElectronFilter("SubleadingElectron")
filtSeq.SubleadingElectron.Ptcut = 5000.
filtSeq.SubleadingElectron.Etacut = 2.6
filtSeq.SubleadingElectron.NElectrons = 2

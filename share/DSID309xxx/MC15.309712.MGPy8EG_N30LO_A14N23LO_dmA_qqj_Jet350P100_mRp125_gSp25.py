model       = 'dmA'
mR          = 125
mDM         = 10000
gSM         = 0.25
gDM         = 1.00
widthR      = 3.102928
xptj        = 100
filteff     = 0.01000
jetminpt    = 350
quark_decays= ['u', 'd', 's', 'c']

include("MC15JobOptions/MadGraphControl_MGPy8EG_DM_dijetjet_flavfilt.py")

evgenConfig.description = "Zprime sample - mR125 - model dmA"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Karol Krizka <kkrizka@cern.ch>"]
evgenConfig.minevents = 1000


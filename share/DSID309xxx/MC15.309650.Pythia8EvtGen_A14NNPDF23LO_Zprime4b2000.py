include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z' bosons
                            "Zprime:gmZmode = 3",                  # Z',Z,g without interference
                            "Zprime:universality = off",
                            "Zprime:at = 0.0",
                            "Zprime:vt = 0.0",
                            "Zprime:vb = -0.693",
                            "Zprime:ab = -1.0",
                            "Zprime:vd = 0.0"  ,                           
                            "Zprime:ad = 0.0",
                            "Zprime:au = 0.0",
                            "Zprime:vu = 0.0",
                            "Zprime:as = 0.0",
                            "Zprime:vs = 0.0",
                            "Zprime:ac = 0.0",
                            "Zprime:vc = 0.0",
                            "Zprime:ae = 0.0",
                            "Zprime:ve = 0.0",
                            "Zprime:anue = 0.0",
                            "Zprime:vnue = 0.0",
                            "Zprime:amu = 0.0",
                            "Zprime:vmu = 0.0",
                            "Zprime:anumu = 0.0",
                            "Zprime:vnumu = 0.0",
                            "Zprime:atau = 0.0",
                            "Zprime:vtau = 0.0",
                            "Zprime:anutau = 0.0",
                            "Zprime:vnutau = 0.0",
                            "32:onMode = off",                     # switch off all Z decays
                            "32:onIfAny  = 5 -5",                    # switch on Z->bb~ decays
                            "32:m0 = 2000",
                            "PhaseSpace:mHatMin=200"]
                            #"PhaseSpace:mHatMax=-1",
                            #"32:doForceWidth=on",
                            #"32:mWidth=40.0"]                        # set Z' width

# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime couples to b quark'
evgenConfig.contact = ["Ablet Imin <yiming.abulaiti@cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'resonance']
evgenConfig.generators += [ 'Pythia8' ]

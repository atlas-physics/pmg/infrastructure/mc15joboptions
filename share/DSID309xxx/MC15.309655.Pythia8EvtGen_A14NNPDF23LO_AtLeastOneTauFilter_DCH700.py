m_dch = 700.0

evgenConfig.description = "Doubly charged higgs ("+str(m_dch)+") in lepton mode."
evgenConfig.process = "DCH -> same sign 2lepton"
evgenConfig.keywords = ["BSM", "chargedHiggs" ,"2lepton"]
evgenConfig.contact = ["Simon Erland Arnling Baath <simon.erland.arnling.baath@cern.ch>"]
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += [
"9900041:m0 = " + str(m_dch), # H++_L mass [GeV]
"LeftRightSymmmetry:ffbar2HLHL=on", #HL pair production
#set the VEV (vacuum expectation value) value
"LeftRightSymmmetry:vL=0.0",
# set all couplings to leptons to 0.02
"LeftRightSymmmetry:coupHee=0.02",
"LeftRightSymmmetry:coupHmue=0.02",
"LeftRightSymmmetry:coupHmumu=0.02",
"LeftRightSymmmetry:coupHtaue=0.02",
"LeftRightSymmmetry:coupHtaumu=0.02",
"LeftRightSymmmetry:coupHtautau=0.02"]

from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter #Removes generation of all pure light-lepton events
filtSeq += ParentChildFilter("ParentChildFilter")
filtSeq.ParentChildFilter.PDGParent = [ 9900041, -9900041]  #Set DCH as valid parents
filtSeq.ParentChildFilter.PDGChild = [15, -15] #Require at least one Tau-lepton as a child to a DCH in the event

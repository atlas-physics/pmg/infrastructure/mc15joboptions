include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z' bosons
                            "Zprime:gmZmode = 3",                  # Z',Z,g without interference
                            "32:onMode = off",                     # switch off all Z decays
                            "32:onIfAny  = 11",                    # switch on Z->ee decays
                            "32:m0 = 2000",                        # set Z' pole mass
                            "PhaseSpace:mHatMin = 1999.99",         # set lower invariant mass
                            "PhaseSpace:mHatMax = 2000.01"]       # set upper invariant mass


# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime decaying to two electrons'
evgenConfig.contact = ["Tetiana Hryn'ova <thrynova@mail.cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'resonance',
'electroweak', '2electron' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Zprime>ee"

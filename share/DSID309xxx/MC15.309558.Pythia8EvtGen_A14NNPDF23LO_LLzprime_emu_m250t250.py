evgenConfig.description = "Long-lived Z' -> e + mu (ct = 250 mm, m = 250 GeV)"
evgenConfig.keywords = ["BSM","exotic","longLived"]
evgenConfig.contact = ["siinn.che@cern.ch"]
evgenConfig.process = "Long-lived Z' -> e + mu"

# Specify A14NNPDF23LO PDF
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
#include("MC15JobOptions/Pythia8/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# Z' Parameters
ZprimeMass = 250.
ZprimeLifetime = 250.


genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"]		# Allow long-lived particles to decay
genSeq.Pythia8.Commands += ["32:name = Zprime"]       			    # set Z' name
genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]	# create Z' bosons
genSeq.Pythia8.Commands += ["Zprime:gmZmode = 3"]			        # Z',Z,g with interference
genSeq.Pythia8.Commands += ["32:onMode = off"]				        # Turn off all Z' decays
genSeq.Pythia8.Commands += ["32:mWidth = 6.9903"]
genSeq.Pythia8.Commands += ["32:doForceWidth = 1"]
genSeq.Pythia8.Commands += ["32:oneChannel = 1 0.5 0 11 -13"]	    # Switch on Z'->e-mu+
genSeq.Pythia8.Commands += ["32:addChannel = 1 0.5 0 -11 13"]	    # Switch on Z'->-e+mu
genSeq.Pythia8.Commands += ["32:m0 = "+str(ZprimeMass)] 		    # Z' mass
genSeq.Pythia8.Commands += ["32:tau0 = "+str(ZprimeLifetime)]       # Set Z' ctau

# Increase Maximum failure events
genSeq.Pythia8.MaxFailures = 1000000

# Turn off checks for displaced vertices. Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000				#In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

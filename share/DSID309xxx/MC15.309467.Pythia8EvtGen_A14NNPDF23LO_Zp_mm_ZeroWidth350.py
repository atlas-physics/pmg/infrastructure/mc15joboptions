include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z' bosons
                            "Zprime:gmZmode = 3",                  # Z',Z,g without interference
                            "32:onMode = off",                     # switch off all Z decays
                            "32:onIfAny  = 13",                    # switch on Z->mm decays
                            "32:m0 = 350",                        # set Z' pole mass
                            "PhaseSpace:mHatMin = 349.99",         # set lower invariant mass
                            "PhaseSpace:mHatMax = 350.01"]       # set upper invariant mass


# EVGEN configuration
evgenConfig.description = 'Pythia 8 Zprime decaying to two muons'
evgenConfig.contact = ["Tetiana Hryn'ova <thrynova@mail.cern.ch>"]
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'resonance',
'electroweak', '2muon' ]
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.process = "pp>Zprime>mm"

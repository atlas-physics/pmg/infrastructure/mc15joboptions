#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+HerwigPP Wt production (antitop), HT filtered, with CT10 for ME and CTEQ6L1 for PS, UEEE5 tune and with EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', 'inclusive']
evgenConfig.contact     = [ 'christian.ohm@cern.ch']
evgenConfig.generators += [ 'Powheg' ]

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_Wt_DR_Common.py')

  PowhegConfig.topdecaymode = 11111 # inclusive W-from-top decays
  PowhegConfig.wdecaymode = 11111 # inclusive W decays
  PowhegConfig.ttype  = -1 # anti-top
  PowhegConfig.nEvents *= 25
  PowhegConfig.PDF     = 10800
  PowhegConfig.mu_F    = 1.00000000000000000000
  PowhegConfig.mu_R    = 1.00000000000000000000
  PowhegConfig.hdamp   = -1
  PowhegConfig.hfact   = -1
  PowhegConfig.generate()

#--------------------------------------------------------------
# Showering with HerwigPP, UE-EE-5 tune
#--------------------------------------------------------------
include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_EvtGen_Common.py")

# Configure the HT filter
include('MC15JobOptions/HTFilter.py')
filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
filtSeq.HTFilter.MinHT = 500.*GeV # Min HT to keep event
filtSeq.HTFilter.MaxHT = 20000.*GeV # Max HT to keep event
filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

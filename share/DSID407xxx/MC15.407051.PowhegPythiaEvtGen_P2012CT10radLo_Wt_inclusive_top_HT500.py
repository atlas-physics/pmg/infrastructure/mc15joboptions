#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 Wt production (top), HT filtered, scale*2.0, with CT10 and Perugia2012 radLo tune, EvtGen'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt', 'inclusive', 'systematic']
evgenConfig.contact     = [ 'christian.ohm@cern.ch']
evgenConfig.generators += [ 'Powheg' ]

#--------------------------------------------------------------
# Powheg Wt setup
#--------------------------------------------------------------

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_Wt_DR_Common.py')

  PowhegConfig.topdecaymode = 11111 # inclusive W-from-top decays
  PowhegConfig.wdecaymode = 11111 # inclusive W decays
  PowhegConfig.nEvents *= 25
  PowhegConfig.PDF     = 10800
  PowhegConfig.mu_F    = 2.0
  PowhegConfig.mu_R    = 2.0
  PowhegConfig.hdamp   = -1
  PowhegConfig.hfact   = -1
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012radLo_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')

  # Configure the HT filter
  include('MC15JobOptions/HTFilter.py')
  filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
  filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
  filtSeq.HTFilter.MinHT = 500.*GeV # Min HT to keep event
  filtSeq.HTFilter.MaxHT = 20000.*GeV # Max HT to keep event
  filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
  filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
  filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT
  
#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')




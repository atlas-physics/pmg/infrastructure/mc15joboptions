include("MC15JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa Z/gamma* -> e e + 4j@LO with 0 GeV < ptV < 70 GeV with b-jet filter."
evgenConfig.keywords = ["SM", "Z", "2electron", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Sherpa_CT10_Zee_Pt0_70"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=Internal;
}(run)

(processes){
  Process 93 93 -> 11 -11 93{4};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Scales LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  Integration_Error 0.05 {3,4,5,6,7,8};
  End process;
}(processes)

(selector){
  Mass 11 -11 40.0 E_CMS
  "PT" 11,-11  0,70
}(selector)
"""


# Set up HF filters
include("MC15JobOptions/BHadronFilter.py")
filtSeq += HeavyFlavorBHadronFilter


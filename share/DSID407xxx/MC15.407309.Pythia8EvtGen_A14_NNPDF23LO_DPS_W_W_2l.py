##############################################################
# JO  fragment for DPS W->lnu + W->lnu
##############################################################
evgenConfig.description = "pp -> W + W (DPS) decaying leptonically "
evgenConfig.keywords = ["electroweak","MPI", "W"]
evgenConfig.minevents = 10000

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 11", # switch on W->lv decays
                            "24:onIfAny = 13",
                            "24:onIfAny = 15"
                            ] 
genSeq.Pythia8.Commands += ['SecondHard:generate = on'] # create DPS W
genSeq.Pythia8.Commands += ['SecondHard:SingleW = on']




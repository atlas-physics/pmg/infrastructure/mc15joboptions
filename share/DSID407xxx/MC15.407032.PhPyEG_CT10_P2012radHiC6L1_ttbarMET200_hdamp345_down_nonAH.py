#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg hdamp equal 2*top mass, scale=0.5, Perugia 2012 radHi tune, at least one lepton filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton', 'systematic']
evgenConfig.contact     = [ 'teng.jian.khoo@cern.ch' ]

if runArgs.trfSubstepName == 'generate' :

  include('PowhegControl/PowhegControl_tt_Common.py')
  PowhegConfig.topdecaymode = 22222
  PowhegConfig.hdamp        = 345.0
  PowhegConfig.mu_F         = 0.5000
  PowhegConfig.mu_R         = 0.5000
  # compensate filter efficiency
  PowhegConfig.nEvents     *= 150.
  PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
  include('MC15JobOptions/PowhegPythia_Perugia2012radHi_Common.py')
  include('MC15JobOptions/Pythia_Tauola.py')
  include('MC15JobOptions/Pythia_Photos.py')
  
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
  include('MC15JobOptions/TTbarWToLeptonFilter.py')
  filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
  filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

  include('MC15JobOptions/MissingEtFilter.py')
  filtSeq.MissingEtFilter.METCut = 200*GeV

#  Run EvtGen as afterburner
include('MC15JobOptions/Pythia_Powheg_EvtGen.py')


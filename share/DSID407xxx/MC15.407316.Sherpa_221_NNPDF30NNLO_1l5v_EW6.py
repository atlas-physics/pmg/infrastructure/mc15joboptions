include("MC15JobOptions/Sherpa_2.2.1_NNPDF30NNLO_Common.py")

evgenConfig.description = "1 charged lepton + 5 neutrinos with O,1j@LO."
evgenConfig.keywords = ["SM", "triboson", "multilepton", "jets", ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "chris.g@cern.ch" ]
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "Sherpa_221_NNPDF30NNLO_1l5v"

evgenConfig.process="""
(run){
  FSF:=1; RSF:=1; QSF:=1;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[0]+p[1])};

  NJET:=1; LJET=0; QCUT:=30;

  EXCLUSIVE_CLUSTER_MODE=1

  PARTICLE_CONTAINER 901 lightflavs 1 -1 2 -2 3 -3 4 -4 21;
}(run);

(processes){
  Process 901 901 -> 90 91 91 91 91 91 901{NJET};
  Order (*,6); CKKW sqr(QCUT/E_CMS);
  End process;
}(processes);

(selector){
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP]
  Mass 11 -11 4.0 E_CMS
  Mass 13 -13 4.0 E_CMS
  Mass 15 -15 4.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]


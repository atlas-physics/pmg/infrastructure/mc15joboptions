#Based on 412116 and 412068
#EVGEN configuration

# Provide config information 
evgenConfig.generators  += ["aMcAtNlo", "Herwig7"]
evgenConfig.tune         = "H7.1-Default"
evgenConfig.description  = 'MG5_aMC@NLO+Herwig7+EvtGen, H7p1 default tune, ME NNPDF 3.0 NLO, with single lepton filter from DSID 412121 LHE files, and C filter and B veto'
evgenConfig.keywords     = ['SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact      = ['nedaa.asbah@cern.ch','aknue@cern.ch','tpelzer@cern.ch']
evgenConfig.minevents   = 5000
#evgenConfig.inputFilesPerJob = 25

#--------------------------------------------------------------
# l+jets filter
# This is a filter on the input LHE files, applied before any other algorithm is processed
#--------------------------------------------------------------
include('MC15JobOptions/LHEFilter.py')
include('MC15JobOptions/LHEFilter_NLeptons.py')

nleptonFilter = LHEFilter_NLeptons()
nleptonFilter.NumLeptons = 1
nleptonFilter.Ptcut = 0.
lheFilters.addFilter(nleptonFilter)

lheFilters.run_filters()

#--------------------------------------------------------------
# Herwig7.1.3 showering
#--------------------------------------------------------------

# initialize Herwig7 generator configuration for showering of LHE files                                                                                               
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen                                                                                                                                                                                                                
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7                                                                                                                                                                                                          
Herwig7Config.run()

#--------------------------------------------------------------
# B filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusBFilter.py')

#--------------------------------------------------------------
# C filter
#--------------------------------------------------------------
include('MC15JobOptions/TTbarPlusCFilter.py')

# Combine the filters
filtSeq.Expression = "(TTbarPlusCFilter) and (not TTbarPlusBFilter)"

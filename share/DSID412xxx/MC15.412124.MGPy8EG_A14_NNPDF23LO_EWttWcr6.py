from MadGraphControl.MadGraphUtils import *
import math

#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------
nevents=5000
minevents=200

#mode=0
# gridpack_dir='madevent/'
# gridpack_mode=True
gridpack_dir=None
gridpack_mode=False
safefactor=1.1

DSID = runArgs.runNumber
if DSID == 412123:
    modelExtension = "-SMlimit"#"-SMlimit"

elif DSID == 412124:
    modelExtension="-SMlimit_CR_6"
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


defs = """
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define uc = u c
define uc~ = u~ c~
define ds = d s
define ds~ = d~ s~
"""

mcprod = defs+"""
generate p p > t t~ w j QED=3 QCD=1 NP=1
"""

mcprod_maddec = defs+"""
define w+child = l+ vl uc ds~
define w-child = l- vl~ ds uc~
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
"""


#---------------------------------------------------------------------------------------------------
# generating process cards
#---------------------------------------------------------------------------------------------------

fcard = open('proc_card_mg5.dat','w')

#---------------------------------------------------------------------------------------------------
# generating pp to ttW 
#---------------------------------------------------------------------------------------------------

fcard.write("""
import model SMEFTsim_A_general_MwScheme_UFO"""+modelExtension+"""
set gauge unitary
"""+mcprod+"""
output -f
""")
fcard.close()
#---------------------------------------------------------------------------------------------------
# Decay with MadSpin
#---------------------------------------------------------------------------------------------------

madspin_card_loc='madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 100  # number of PS to estimate the maximum for each event
set Nevents_for_max_weigth 100
set BW_cut 15
set seed %i
%s
launch
"""%(runArgs.randomSeed, mcprod_maddec))
mscard.close()


#---------------------------------------------------------------------------------------------------
# require beam energy to be set as argument
#---------------------------------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#---------------------------------------------------------------------------------------------------
# creating run_card.dat for ttW
#---------------------------------------------------------------------------------------------------

# MadGraph default LO run_card.dat and set parameters

extras = {'lhe_version'    :'3.0',
          'pdlabel'        : "'nn23lo1'",
          'lhaid'          : 230000,
          'parton_shower'  :'PYTHIA8',
          'event_norm'     : 'average',
          'cut_decays'     : 'False',
          'drll'           : '0.4',
          'bwcutoff'       : '15.0',
          'pta'            : '10.',
		  'ptl'            : '10.',
          'etal'           : '2.5',
          'drjj'           : '0.4',
          'draa'           : '0.4',
          'etaj'           : '-5',
          'draj'           : '0.4',
          'drjl'           : '0.4',
          'dral'           : '0.4',
          'etaa'           : '2.5',
          'ptj'            : '20.',
          'ptj1min'        : '0.',
          'ptj1max'        : '-1.0',
          'mmjj'           : '0.0',
          'mmjjmax'        : '-1'
          }

process_dir = new_process()

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=int(runArgs.maxEvents*safefactor),rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)
print_cards()

param_card_loc='param_'+modelExtension[1:]+'.dat'
paramcard = subprocess.Popen(['get_files','-data',param_card_loc])
paramcard.wait()
if not os.access(param_card_loc,os.R_OK):
    raise RuntimeError("ERROR: Could not get %s"%param_card_loc)

runName='run_01'

generate(run_card_loc='run_card.dat',
        param_card_loc=param_card_loc,
        madspin_card_loc=madspin_card_loc,
        mode=0,
        grid_pack=gridpack_mode,
        gridpack_dir=gridpack_dir,
        njobs=1,
        run_name=runName,
        proc_dir=process_dir)

#---------------------------------------------------------------------------------------------------
# Multi-core capability
#---------------------------------------------------------------------------------------------------
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
#### Showering with Pythia 8
#---------------------------------------------------------------------------------------------------
evgenConfig.description = "ttW production in the SMEFT model."
evgenConfig.keywords = ["ttW", "SM","top"]
evgenConfig.process = "generate p p > t t~ w j NP=1"
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.minevents = minevents
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
evgenConfig.contact = ["Laura Barranco Navarro <lbarranc@cern.ch>"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->bb and Z->vv decays
Herwig7Config.add_commands("""
# force H->bb decays
do /Herwig/Particles/h0:SelectDecayModes h0->c,cbar;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/Z0:SelectDecayModes Z0->nu_e,nu_ebar; Z0->nu_mu,nu_mubar; Z0->nu_tau,nu_taubar;
do /Herwig/Particles/Z0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Herwig7 H+Z-jet->vvccbar production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.process     = "gg->ZH, H->bb, Z->vv"
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'spyridon.argyropoulos@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7-MMHT2014LO"
evgenConfig.minevents   = 5000
evgenConfig.inputFilesPerJob = 1
#evgenConfig.inputfilecheck = "TXT"

include("MC15JobOptions/Sherpa_2.2.2_NNPDF30NNLO_Common.py")

evgenConfig.description = "Sherpa+OpenLoops gg->(h->WW/ZZ->)llvv + 0,1j, cf. arXiv:1309.0500. QSF025"
evgenConfig.keywords = ["SM", "diboson", "2lepton", "neutrino" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 5000
evgenConfig.inputconfcheck = "ggllvvIntOnlyHiggs"

Sherpa_iRunCard="""
(run){
  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=0.25;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  ## for mu_F=mu_R=mu_Q = mVV/2
  CORE_SCALE VAR{Abs2(p[2]+p[3]+p[4]+p[5])/4.0}

  % tags for process setup
  NJET:=1; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  INTEGRATION_ERROR=0.1
  EXCLUSIVE_CLUSTER_MODE 1;
  AMEGIC_ALLOW_MAPPING=0
  SHERPA_LDADD=Proc_fsrchannels4 Proc_fsrchannels5

  MASS[25]=125.
  WIDTH[25]=0.004088
}(run)

(processes){
  Process 93 93 -> 11 -11 12 -12 93{NJET}
  CKKW sqr(QCUT/E_CMS);
  Order (2,4) {4}; Order (3,4) {5};
  Integrator fsrchannels4 {4}; Integrator fsrchannels5 {5};
  Enable_MHV 10 # initialises external process
  Loop_Generator LOOPGEN;
  End process;

  Process 93 93 -> 13 -13 14 -14 93{NJET}
  CKKW sqr(QCUT/E_CMS);
  Order (2,4) {4}; Order (3,4) {5};
  Integrator fsrchannels4 {4}; Integrator fsrchannels5 {5};
  Enable_MHV 10 # initialises external process
  Loop_Generator LOOPGEN;
  End process;

  Process 93 93 -> 15 -15 16 -16 93{NJET}
  CKKW sqr(QCUT/E_CMS);
  Order (2,4) {4}; Order (3,4) {5};
  Integrator fsrchannels4 {4}; Integrator fsrchannels5 {5};
  Enable_MHV 10 # initialises external process
  Loop_Generator LOOPGEN;
  End process;
}(processes)

(selector){
  Mass 11 -11 2.0 E_CMS
  Mass 12 -12 2.0 E_CMS
  Mass 13 -13 2.0 E_CMS
  Mass 14 -14 2.0 E_CMS
  Mass 15 -15 2.0 E_CMS
  Mass 16 -16 2.0 E_CMS
}(selector)

"""

genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5", "OL_PARAMETERS=preset=3=write_parameters=1=approx=onlyh" ]
Sherpa_iOpenLoopsLibs = [ "ppllll2_onlyh", "ppllllj2_onlyh" ]
Sherpa_iNCores = 96
Sherpa_iExtraFiles = [ "libProc_fsrchannels4.so", "libProc_fsrchannels5.so" ]

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
MultiLeptonFilter=MultiLeptonFilter(name="MultiLeptonFilter")
MultiLeptonFilter.Ptcut = 7000
MultiLeptonFilter.NLeptons = 2
MultiLeptonFilter.Etacut = 3.0

from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
LeptonFilter=LeptonFilter(name="LeptonFilter")
LeptonFilter.Ptcut = 15000.
LeptonFilter.Etacut =  3.0

from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
DiLeptonMassFilterVeto=DiLeptonMassFilter(name="DiLeptonMassFilterVeto")
DiLeptonMassFilterVeto.MinPt = 10000.
DiLeptonMassFilterVeto.MaxEta = 3.0
DiLeptonMassFilterVeto.MinMass = 45000.
DiLeptonMassFilterVeto.MaxMass = 10E12
DiLeptonMassFilterVeto.MinDilepPt = 0.
DiLeptonMassFilterVeto.AllowSameCharge = False
DiLeptonMassFilterVeto.AllowElecMu = True

filtSeq += MultiLeptonFilter
filtSeq += LeptonFilter
filtSeq += DiLeptonMassFilterVeto

filtSeq.Expression = "(MultiLeptonFilter) and (LeptonFilter) and (DiLeptonMassFilterVeto)"

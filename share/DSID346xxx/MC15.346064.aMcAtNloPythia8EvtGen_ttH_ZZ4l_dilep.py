include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off', #decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13',
                             '23:onIfMatch = 15 15']

evgenConfig.process        = 'ttH dilep H->ZZ->llll'
evgenConfig.description    = 'aMcAtNloPythia8 ttH, H to ZZ to 4l, dilep'
evgenConfig.keywords       = [ 'SM', 'Higgs', 'SMHiggs', 'ZZ', 'mH125', 'ttHiggs','ttbar' ]
evgenConfig.inputfilecheck = "TXT"
evgenConfig.contact        = ['Antonio.Salvucci <antonio.salvucci@cern.ch>']

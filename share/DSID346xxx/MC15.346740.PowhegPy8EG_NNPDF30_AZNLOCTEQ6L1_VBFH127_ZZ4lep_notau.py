
evgenConfig.process     = "VBFH H-> all"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->all mh=127 GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs" ]
evgenConfig.contact     = [ 'roberto.di.nardo@cern.ch', 'sabidi@cern.ch' ]
evgenConfig.minevents   = 10000
evgenConfig.inputFilesPerJob = 2

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]


#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 11 11',
                             '23:onIfMatch = 13 13' ]


#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']



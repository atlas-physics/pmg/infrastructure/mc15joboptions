#--------------------------------------------------------------
# Herwig showering
#--------------------------------------------------------------
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

# only consider H->gamma gamma decays
Herwig7Config.add_commands("""
    # force H->gamma gamma decays
    do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;
    # print out Higgs decays modes and branching ratios to the terminal/log.generate
    do /Herwig/Particles/h0:PrintDecayModes
    """)

# run Herwig7
Herwig7Config.run()
     
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process          = "ttH,  H->gamma gamma"
evgenConfig.description      = 'POWHEG+Herwig7 ttH production with H7UE tune'
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.generators       += [ 'Powheg', 'Herwig7' ]
evgenConfig.contact          = [ 'ana.cueto@cern.ch' ]
evgenConfig.tune             = "H7UE"
evgenConfig.inputfilecheck   = 'TXT'

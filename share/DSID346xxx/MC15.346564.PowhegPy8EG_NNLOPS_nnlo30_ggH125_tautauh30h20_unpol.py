
evgenConfig.process     = "gg->H -> tautau -> hh unpolarized"
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN H+jet production with NNLOPS"
evgenConfig.keywords    = [ "Higgs", "1jet", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 500
evgenConfig.inputfilecheck = "TXT"

#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15',
                             '15:onMode = on', # decay of taus
                             '15:offIfAny = 11 13',
                             'TauDecays:externalMode = 0',
                             'TauDecays:mode = 5' ]

# Set tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  taufilter = TauFilter("taufilter")
  filtSeq += taufilter

filtSeq.taufilter.UseNewOptions = True
filtSeq.taufilter.Ntaus = 2
filtSeq.taufilter.Nleptaus = 0
filtSeq.taufilter.Nhadtaus = 2
filtSeq.taufilter.EtaMaxlep = 2.6
filtSeq.taufilter.EtaMaxhad = 2.6
filtSeq.taufilter.Ptcutlep = 7000.0 #MeV
filtSeq.taufilter.Ptcutlep_lead = 7000.0 #MeV
filtSeq.taufilter.Ptcuthad = 20000.0 #MeV
filtSeq.taufilter.Ptcuthad_lead = 30000.0 #MeV

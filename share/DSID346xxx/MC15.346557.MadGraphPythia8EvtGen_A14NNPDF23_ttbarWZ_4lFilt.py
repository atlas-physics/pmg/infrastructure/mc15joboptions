include('MC15JobOptions/MadGraphControl_ttVV_LO.py')
			                 			                 
include("MC15JobOptions/MultiLeptonFilter.py")
filtSeq.MultiLeptonFilter.Ptcut    = 8000.
filtSeq.MultiLeptonFilter.Etacut   = 3.
filtSeq.MultiLeptonFilter.NLeptons = 3

include("MC15JobOptions/FourLeptonMassFilter.py")
filtSeq.FourLeptonMassFilter.MinPt           = 4000.
filtSeq.FourLeptonMassFilter.MaxEta          = 4.
filtSeq.FourLeptonMassFilter.MinMass1        = 40000.
filtSeq.FourLeptonMassFilter.MaxMass1        = 14000000.
filtSeq.FourLeptonMassFilter.MinMass2        = 8000.
filtSeq.FourLeptonMassFilter.MaxMass2        = 14000000.
filtSeq.FourLeptonMassFilter.AllowElecMu     = False
filtSeq.FourLeptonMassFilter.AllowSameCharge = False


evgenConfig.description = "MG ttWZ sample with 4l filter"
evgenConfig.contact     = [ 'syed.haider.abidi@cern.ch' ]
evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.description = 'MG5_aMC@NLO_Pythia8 ggF+2jets (125 GeV) Higgs production in the Higgs Characterization model decaying to gamma gamma'

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")
include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")

evgenConfig.keywords+=['gluonFusionHiggs']
#evgenConfig.minevents=1000
evgenConfig.inputfilecheck = 'ggF2j_CPMIX'
evgenConfig.contact = ['maria.moreno.llacer@cern.ch']

genSeq.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
]

#--------------------------------------------------------------
# Herwig 7 showering setup                                                                                                    
#-------------------------------------------------------------- 
include("MC15JobOptions/Herwig7_LHEF.py")

# fix default settings

Herwig7Config.commands += """
## fix for initial-state (backward evolution) splitting
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
"""
Herwig7Config.me_pdf_commands(order="NLO", name="CT10")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
evgenConfig.tune = "H7-UE-MMHT"

include("MC15JobOptions/Herwig7_EvtGen.py")

# -----------------------------------------------------------------------
# dantrim
# option 1 : force W's to go leptonically
# option 2 : force W's to go leptonically and add EMuTauFilter
# option 3 : force WWbb and add EMuTauFilter (no force of W decays)
# option 4 : force W's to go leptonically and add XtoVVDecayFilterExtended
# option 5 : force WWbb and add XtoVVDecayFilterExtended
# -----------------------------------------------------------------------

Herwig7Config.commands += """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-; h0->b,bbar;
do /Herwig/Particles/h0:PrintDecayModes
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio  0.5
do /Herwig/Particles/W-:SelectDecayModes W-->nu_mubar,mu-; W-->nu_ebar,e-; W-->nu_taubar,tau-;
do /Herwig/Particles/W+:SelectDecayModes W+->nu_mu,mu+; W+->nu_e,e+; W+->nu_tau,tau+;
do /Herwig/Particles/W-:PrintDecayModes
do /Herwig/Particles/W+:PrintDecayModes
"""


#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["aMcAtNlo", "Herwig7"]
evgenConfig.description    = "M diHiggs production, decay to WWbb (leplep), with MG5_aMC@NLO, inclusive of box diagram FF."
evgenConfig.keywords       = ["SM", "SMHiggs", "nonResonant", "bbbar", "WW"]
evgenConfig.contact        = ['Arnaud Ferrari <Arnaud.Ferrari@cern.ch>']
evgenConfig.inputfilecheck = 'hh_NLO_EFT_FF_HERWIGPP_CT10' 
evgenConfig.minevents      = 5000

#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("hWWFilter", PDGParent = [25], PDGChild = [24])
include("MC15JobOptions/MultiElecMuTauFilter.py")
filtSeq.MultiElecMuTauFilter.MinPt = 7000. # 7 GeV, in accordance with 2015 to 2018 dilepton triggers?
filtSeq.MultiElecMuTauFilter.NLeptons = 2
filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
filtSeq.Expression = "hbbFilter and hWWFilter and MultiElecMuTauFilter"

Herwig7Config.run()

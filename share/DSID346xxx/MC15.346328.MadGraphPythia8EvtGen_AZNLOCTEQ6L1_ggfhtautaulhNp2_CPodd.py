from MadGraphControl.MadGraphUtils import *
    
#Some setup variables.
#nevents=1000
#mode=1
#nJobs=20
#cluster_type="pbs"
#cluster_queue="medium"

from os import environ,path
environ['ATHENA_PROC_NUMBER'] = '8'

#Need extra events to avoid Pythia8 problems.
safefactor=70.
nevents = 2000*safefactor
if runArgs.maxEvents > 0:
    nevents = runArgs.maxEvents*safefactor

######################################################################
# Configure event generation.
######################################################################

runName='run_01'     
gridpack_dir="madevent/"
gridpack_mode=True

#Set input file for Pythia8.
runArgs.inputGeneratorFile = runName+'._00001.events.tar.gz'

#Make process card.
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-heft
define p = g d d~ u u~ s s~ c c~ b b~                                   
define j = g d d~ u u~ s s~ c c~ b b~                                 
generate    p p > x0     / t a [QCD] @0
add process p p > x0 j   / t a [QCD] @1
add process p p > x0 j j / t a [QCD] @2
output ppx0_ttll_NLOQCD_FxFx""")
fcard.close()


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Define process directory.
process_dir = new_process(grid_pack=gridpack_dir)

#Fetch default run_card.dat and set parameters
ptj = 15
maxjetflavor = 5
extras = {'lhe_version'   :'3.0',
          'parton_shower' :'PYTHIA8',
          'reweight_scale':'True',
          'ickkw'         :'3',
          'jetradius'     :'1.0',
          'ptj'           :str(ptj),
          'maxjetflavor'  :str(maxjetflavor),
          'pdlabel'       : "'lhapdf'",
          'lhaid'         : '90900',
          'reweight_PDF'  : 'True',
          'PDF_set_min'   : '90901',
          'PDF_set_max'   : '90930',
          'muR_ref_fixed' : '125.0',
          'muF1_ref_fixed': '125.0',
          'muF2_ref_fixed': '125.0',
          'QES_ref_fixed' : '125.0'}


#Set couplings here.
parameters={'frblock': {
        'kAza':   0.0, 
        'kAgg':   -0.6666, 
        'kAaa':   0.0, 
        'cosa':   0.0, 
        'kHdwR':  0.0, 
        'kHaa':   0.0, 
        'kAll':   0.0, 
        'kHll':   0.0, 
        'kAzz':   0.0, 
        'kSM':    1.0, 
        'kHdwI':  0.0, 
        'kHdz':   0.0, 
        'kAww':   0.0, 
        'kHgg':   1.0, 
        'kHda':   0.0, 
        'kHza':   0.0, 
        'kHww':   0.0, 
        'kHzz':   0.0, 
        'Lambda': 1000.0
        }}

#Create parame card
build_param_card(param_card_old=path.join(process_dir,'Cards/param_card.dat'),param_card_new='param_card_new.dat',
                 masses={'25': '1.250000e+02'},params=parameters)

#Create run card.
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()


#Run the event generation!
generate(run_card_loc='run_card.dat',
         param_card_loc='param_card_new.dat',
         mode=0,
         #njobs=nJobs,
         proc_dir=process_dir,
         run_name=runName,
         grid_pack=gridpack_mode,
         gridpack_dir=gridpack_dir,
         nevents=nevents,
         random_seed=runArgs.randomSeed)


##arrange output
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runArgs.inputGeneratorFile)

######################################################################
# End of event generation, start configuring parton shower here.
######################################################################


#### Shower 
environ['ATHENA_PROC_NUMBER'] = '1'

include("MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

testSeq.TestHepMC.MaxVtxDisp = 100000000

# Matching settings                                                                                                                              
qCut = 40
genSeq.Pythia8.Commands += [
    "JetMatching:setMad           = off",
    "JetMatching:merge            = on",
    "JetMatching:scheme           = 1",
    "JetMatching:coneRadius       = 1.0",                # Default.
    "JetMatching:etaJetMax        = 4.5",
    "JetMatching:nJetMax          = 2",                  # *** Must match highest Born-level jet multiplicity. ***
    "JetMatching:qCut             = "+str(qCut),         # Merging scale for FxFx.
    "JetMatching:doFxFx           = on",
    "JetMatching:qCutME           = "+str(ptj),          # Should match ptj.
    "25:onMode                    = off",                # decay of Higgs
    "25:onIfMatch                 = 15 15"]


# Pythia8_i settings                                                                                                     
genSeq.Pythia8.UserHook = 'JetMatchingMadgraph'
genSeq.Pythia8.FxFxXS = True


# Set up tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  lep15had20filter = TauFilter("lep15had20filter")
  filtSeq += lep15had20filter

filtSeq.lep15had20filter.UseNewOptions = True
filtSeq.lep15had20filter.Ntaus = 2
filtSeq.lep15had20filter.Nleptaus = 1
filtSeq.lep15had20filter.Nhadtaus = 1
filtSeq.lep15had20filter.EtaMaxlep = 2.6
filtSeq.lep15had20filter.EtaMaxhad = 2.6
filtSeq.lep15had20filter.Ptcutlep = 15000.0 #MeV
filtSeq.lep15had20filter.Ptcutlep_lead = 15000.0 #MeV
filtSeq.lep15had20filter.Ptcuthad = 20000.0 #MeV
filtSeq.lep15had20filter.Ptcuthad_lead = 20000.0 #MeV

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.minevents = 2000
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = "ggF+012jets (FxFx merging) 125 GeV Higgs production in the Higgs Characterization model decaying to tautaulh CPodd."
evgenConfig.keywords = ['BSM','Higgs','mH125','BSMHiggs','dijet','resonance']

evgenConfig.contact = ['Alena Loesle <alena.loesle@cern.ch>']
evgenConfig.inputfilecheck = runName
evgenConfig.inputconfcheck = "gfhtautaullNp2_CPodd"


evgenConfig.process     = "qq->ZH, Z->ll, H->tautau"
evgenConfig.description = "POWHEG+MiNLO+PYTHIA8, H+Z+jet, Z->ll, H->tautau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "ZHiggs", "2tau", "mH125" ]
evgenConfig.contact     = [ 'Xin.Chen@cern.ch' ]
evgenConfig.minevents   = 500
#evgenConfig.inputfilecheck = "TXT"
evgenConfig.inputFilesPerJob = 20

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]

# Set tau filters
if not hasattr(filtSeq, "TauFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import TauFilter
  taufilter = TauFilter("taufilter")
  filtSeq += taufilter

filtSeq.taufilter.UseNewOptions = True
filtSeq.taufilter.Ntaus = 2
filtSeq.taufilter.Nleptaus = 0
filtSeq.taufilter.Nhadtaus = 1
filtSeq.taufilter.EtaMaxlep = 2.6
filtSeq.taufilter.EtaMaxhad = 2.6
filtSeq.taufilter.Ptcutlep = 4000.0 #MeV
filtSeq.taufilter.Ptcutlep_lead = 4000.0 #MeV
filtSeq.taufilter.Ptcuthad = 20000.0 #MeV
filtSeq.taufilter.Ptcuthad_lead = 20000.0 #MeV

from MadGraphControl.MadGraphUtils import *

nevents=2200

mode=0



### DSID lists (extensions can include systematics samples)
test=[346904]

fcard = open('proc_card_mg5.dat','w')
if runArgs.runNumber in test:
    fcard.write("""
    import model loop_sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define wpm = w+ w-
    generate p p > wpm wpm z [QCD]
    output -f""")
    fcard.close()
else: 
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)



beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'2.0',
           'pdlabel'      :"'lhapdf'",
           'lhaid'        : 260000,
           'parton_shower':'PYTHIA8',
           'bwcutoff'     :'1000'}

process_dir = new_process()
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


print_cards()
    
runName='run_01'     


#process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,proc_dir=process_dir,run_name=runName)

#stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=1)

evgenConfig.generators = ["aMcAtNlo"]
# ############################
# # Shower JOs will go here
# #### Shower

                    
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = 'MadGraph5_aMC@NLO_WWZ_4l2v'
evgenConfig.keywords+=['triboson']
evgenConfig.contact = ['Ada Farilla <ada.farilla@roma3.infn.it>']
evgenConfig.inputfilecheck = runName
evgenConfig.minEvents = 2000
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# WWZ->lvlvll at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16',
                             '23:onMode = off',#decay of Z
                             '23:mMin = 2.0',
                             '23:onMode = off',
                             '23:onIfAny = 11 13 15']



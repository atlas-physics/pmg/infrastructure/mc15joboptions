include('MC15JobOptions/MadGraphControl_tWH_NLO.py')

genSeq.Pythia8.Commands += [
        '25:onMode = off', # switch OFF all Higgs decay channels
        '25:onIfMatch = 23 23',
        '23:onMode = off',
        '23:mMin = 2.0',
        '23:onIfAny = 11 13 15'
        ]

evgenConfig.description = "aMcAtNlo tWH H4l decay"
evgenConfig.keywords = [ 'Higgs', 'SMHiggs', 'tHiggs' ]
evgenConfig.contact = [ 'syed.haider.abidi@cern.ch' ]
evgenConfig.generators  = ['aMcAtNlo', 'Pythia8', 'EvtGen']

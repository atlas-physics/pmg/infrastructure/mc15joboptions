#--------------------------------------------------------------
# POWHEG+MINLO+Herwig7 W- +H+jet->W-W-W+ -> lvlvqq  production
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.tune_commands()

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# W-H, with W- -> lv 
# H->WW, with  W- ->lv,  W+ ->qq 
# The two leptons from the two W- have the same sign
Herwig7Config.add_commands("""
# force H->WW decays
do /Herwig/Particles/h0:SelectDecayModes h0->W+,W-;
# print out decays modes and branching ratios
do /Herwig/Particles/h0:PrintDecayModes
set /Herwig/Particles/W-:Synchronized Not_synchronized
do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;
do /Herwig/Particles/W-:PrintDecayModes
set /Herwig/Particles/W+:Synchronized Not_synchronized
do /Herwig/Particles/W+:SelectDecayModes W+->u,dbar; W+->sbar,u; W+->c,dbar; W+->c,sbar; W+->bbar,c;
do /Herwig/Particles/W+:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Herwig7 H+W+jet-> W-W-W+ ->lvlvqq + jet production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "WHiggs" ]
evgenConfig.contact     = [ 'ada.farilla@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Herwig7' ]
evgenConfig.tune        = "H7-MMHT2014LO"
evgenConfig.minevents   = 2000
evgenConfig.inputFilesPerJob = 40
evgenConfig.process = "W-H, W- ->lv, H->W-W+, W- ->lv, W+ ->qq"

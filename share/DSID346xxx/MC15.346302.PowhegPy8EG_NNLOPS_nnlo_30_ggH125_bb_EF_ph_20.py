#--------------------------------------------------------------
# Pythia8 showering
# note: Main31 is set in Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py
#--------------------------------------------------------------
#  include('MC15JobOptions/Pythia8_LHEF.py')
include('MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']

else:
    genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 5 -5' ]


#--------------------------------------------------------------
# Photon Filter with pt > 20 GeV
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
filtSeq +=PhotonFilter()
filtSeq.PhotonFilter.Ptcut = 20.*GeV
filtSeq.PhotonFilter.Etacut = 2.45 #(offline 2.37)
filtSeq.PhotonFilter.NPhotons = 1 #(at least one pass requirement)


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->bb, Photon fiter 20GeV"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" ]
evgenConfig.contact     = [ 'boliu@cern.ch' ]
evgenConfig.minevents = 10000
evgenConfig.inputFilesPerJob = 105

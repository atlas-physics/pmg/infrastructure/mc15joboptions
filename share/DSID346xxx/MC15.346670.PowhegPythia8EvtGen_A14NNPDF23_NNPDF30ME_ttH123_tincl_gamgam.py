#--------------------------------------------------------------
# Powheg+Pythia8 ttH setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ttH_Common.py')
PowhegConfig.topdecaymode = 22222 #ttbar we use 22222 which turns on all decay modes
PowhegConfig.hdamp        = 352.5

PowhegConfig.mass_H  = 123.
PowhegConfig.width_H = 0.00407

PowhegConfig.runningscales = 1 ## dynamic scale
PowhegConfig.storeinfo_rwgt = 1
PowhegConfig.PDF = range(260000, 260101) + range(90400, 90433) + [11068] + [25200] + [13165] + [25300] # PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Pythia8 decay to diphoton
#--------------------------------------------------------------
genSeq.Pythia8.Commands  += [ '25:onMode = off', '25:onIfMatch = 22 22' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = 'Powheg+Pythia8 ttH inclusive in top decay production NNPDF30ME, H->yy mH=123GeV'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'antonio.salvucci@cern.ch','ana.cueto@cern.ch' ]
evgenConfig.generators     = [ 'Powheg', 'Pythia8', 'EvtGen']
evgenConfig.inputconfcheck = "ttH123_tincl_NNPDF30"

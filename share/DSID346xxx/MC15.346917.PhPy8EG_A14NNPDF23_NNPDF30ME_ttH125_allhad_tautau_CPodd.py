#--------------------------------------------------------------
# JO to be used with this input TXT sample:
# mc15_13TeV.346305.Powheg_NNPDF30ME_ttH125_allhad_LHE.evgen.TXT.e7020
#--------------------------------------------------------------

evgenConfig.process        = "ttH allhad H->tautau CP odd"
evgenConfig.description    = 'POWHEG+Pythia8.230 ttH (allhad) production with A14 NNPDF2.3 tune'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'tpelzer@cern.ch','antonio.salvucci@cern.ch','Ki.Lie@cern.ch' ]
evgenConfig.minevents      = 10000
evgenConfig.inputFilesPerJob = 2
evgenConfig.generators     = [ 'Powheg', 'Pythia8', 'EvtGen' ]

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF 2.3 tune
#--------------------------------------------------------------
include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Inclusive Higgs decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'HiggsH1:parity = 2',
                             '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 15 15' ]

#--------------------------------------------------------------
# TTbarWToLeptonFilter
# all had ttbar decay - in case input lhe files are inclusive
#--------------------------------------------------------------
include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = 0 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

#--------------------------------------------------------------
# JO to be used with this input TXT sample:
# mc15_13TeV.346305.Powheg_NNPDF30ME_ttH125_allhad_LHE.evgen.TXT.e7020
#--------------------------------------------------------------

evgenConfig.process        = "ttH allhad H->all"
evgenConfig.description    = 'POWHEG+Herwig7.04 ttH (allhad) production with H7UE tune'
evgenConfig.keywords       = [ 'SM', 'top', 'Higgs' ]
evgenConfig.contact        = [ 'tpelzer@cern.ch','antonio.salvucci@cern.ch' ]
evgenConfig.minevents      = 10000
evgenConfig.inputFilesPerJob = 2
evgenConfig.generators     = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune = "H7UE"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("MC15JobOptions/Herwig7_EvtGen.py")

#HW7 settings and Higgs BRs - inclusive Higgs decay
Herwig7Config.add_commands("""
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED 
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio      0.5770
set /Herwig/Particles/h0/h0->c,cbar;:BranchingRatio      0.0291
set /Herwig/Particles/h0/h0->t,tbar;:BranchingRatio      0.00000
set /Herwig/Particles/h0/h0->mu-,mu+;:BranchingRatio     0.000219
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio   0.0632
set /Herwig/Particles/h0/h0->g,g;:BranchingRatio         0.0857
set /Herwig/Particles/h0/h0->gamma,gamma;:BranchingRatio 0.00228
set /Herwig/Particles/h0/h0->Z0,Z0;:BranchingRatio       0.0264
set /Herwig/Particles/h0/h0->W+,W-;:BranchingRatio       0.2150
decaymode h0->Z0,gamma; 0.00154 1 /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->Z0,gamma;:OnOff On
set /Herwig/Particles/h0/h0->Z0,gamma;:BranchingRatio    0.00154
decaymode h0->s,sbar; 0.000246 1 /Herwig/Decays/Hff
set /Herwig/Particles/h0/h0->s,sbar;:OnOff On
set /Herwig/Particles/h0/h0->s,sbar;:BranchingRatio      0.000246
""")

# run Herwig7
Herwig7Config.run()

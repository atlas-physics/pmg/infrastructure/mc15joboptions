MXd =  155.
MY1 =  300.
gVXd =  1.0 #vector coupling to DM 
gAXd = 0. #axial coupling to DM 
gAd11 = 0. #axial couplings to quarks
gAu11 = 0.
gAd22 = 0.
gAu22 = 0.
gAd33 = 0.
gAu33 = 0.
gAl11 = 0.
gAl22 = 0.
gAl33 = 0.

gVd11 = 0.25 #vector couplings to quarks 
gVu11 = 0.25
gVd22 = 0.25
gVu22 = 0.25
gVd33 = 0.25
gVu33 = 0.25
gVl11 = 0.0
gVl22 = 0.0
gVl33 = 0.0
gnu11 = 0.0
gnu22 = 0.0
gnu33= 0.0
WY1 = 7.46038743315

LHE_EventMultiplier = 1.5 #For Filter, Multiplier on maxEvents    
include("MC15JobOptions/MadGraphControl_aMcAtNloPythia8EG_NN30NLO_A14N23LO_DMsimp_monoZll_MET40.py")

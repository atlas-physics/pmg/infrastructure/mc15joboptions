evgenConfig.description = "Quantum black holes (n = 6, M_th = 2.5 TeV) decaying to electron+quark."
evgenConfig.process = "QBH -> e + q"
evgenConfig.keywords = ["BSM", "exotic", "blackhole", "extraDimensions", "ADD"]
evgenConfig.generators += ["QBH"]
evgenConfig.contact = ["Doug Gingrich <gingrich@ualberta.ca>"]
evgenConfig.inputfilecheck = "QBH_ejet"

include("MC15JobOptions/nonStandard/Pythia8_A14_CTEQ6L1_EvtGen_Common.py" )
include("MC15JobOptions/Pythia8_LHEF.py")

MXd =  5.
MY1 =  1.
gVXd = 0. #vector coupling to DM 
gAXd = 1.0 #axial coupling to DM 
gAd11 = 0.25 #axial couplings to quarks
gAu11 = 0.25
gAd22 = 0.25
gAu22 = 0.25
gAd33 = 0.25
gAu33 = 0.25
gAl11 = 0.0
gAl22 = 0.0
gAl33 = 0.0

gVd11 = 0. #vector couplings to quarks 
gVu11 = 0.
gVd22 = 0.
gVu22 = 0.
gVd33 = 0.
gVu33 = 0.
gVl11 = 0.
gVl22 = 0.
gVl33 = 0.
gnu11 = -0.0
gnu22 = -0.0
gnu33= -0.0
WY1 = 0.014653055954

LHE_EventMultiplier = 4 #For Filter, Multiplier on maxEvents 
include("MC15JobOptions/MadGraphControl_aMcAtNloPythia8EG_NN30NLO_A14N23LO_DMsimp_monoZll_MET40.py")

evgenConfig.description = "MadGraph5+Pythia8+EvtGen monoptop non-resonant model (W->lv) with mDM=1 GeV and mMed = 5000 GeV"
evgenConfig.keywords = ["exotic", "monotop", "nonResonant", "leptonic"]
evgenConfig.contact  = ["renjie.wang@cern.ch", "daniele.madaffari@cern.ch", "cescobar@cern.ch"]
evgenConfig.minevents   = 1000

include("MC15JobOptions/MadGraphControl_MonotopDMF_nonres_Wlv.py")

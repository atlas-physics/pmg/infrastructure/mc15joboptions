######################################################################
# Graviton to g g decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 2500. GeV, kappaMG = 1.084"
evgenConfig.keywords = ["exotic", "BSM", "graviton", "RandallSundrum"]
evgenConfig.contact = [" Nishu.Nishu@cern.ch "]
evgenConfig.process = "RS Graviton -> g g"

include( "MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.Commands +=[
"ExtraDimensionsG*:gg2G* = on",
"ExtraDimensionsG*:ffbar2G* = on",
"5100039:m0 = 2500.",
"5100039:onMode = off",
"5100039:onIfAny = 21",
"ExtraDimensionsG*:kappaMG = 1.084",
"5100039:mMin = 50.",
"PhaseSpace:mHatMin = 200.",
]

genSeq.Pythia8.UserModes += ["GravFlat:EnergyMode = 13"]

evgenConfig.description = "MadGraph5+Pythia8 for LRSM WR and NR (Keung-Senjanovic process)"
evgenConfig.keywords = ["exotic","Wprime"]
evgenConfig.contact = ["Deepak Kar <deepak.kar@cern.ch>"]
evgenConfig.process = "pp > WR > l NR, NR > l WR, WR > j j"
include("MC15JobOptions/MadGraphControl_LRSM.py")
evgenConfig.minevents = 1000
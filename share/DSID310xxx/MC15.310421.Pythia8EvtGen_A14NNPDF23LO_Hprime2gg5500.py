evgenConfig.description = "Massive SU3 singlet scalar decaying to gg"
evgenConfig.keywords = ["exotic",  "BSM"]
evgenConfig.process = "H' -> g g"
evgenConfig.contact = ["bnachman@cern.ch"]

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["Higgs:useBSM = on"]   # Turn on BSM Higgses
genSeq.Pythia8.Commands += ["HiggsBSM:gg2H2 = on"] # Turn on gg->H' production
genSeq.Pythia8.Commands += ["35:onMode = off"]     # Turn off all H' decays
genSeq.Pythia8.Commands += ["35:onIfAll = 21 21"]  # Turn back on H' -> gg
genSeq.Pythia8.Commands += ["35:m0 = 5500"]         # Set H' mass
genSeq.Pythia8.Commands += ["35:mWidth = 0.1"]
genSeq.Pythia8.Commands += ["35:doForceWidth = on"]

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13973023E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.99900000E+02   # M_1(MX)             
         2     5.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04031191E+01   # W+
        25     1.24191872E+02   # h
        35     3.00017517E+03   # H
        36     2.99999967E+03   # A
        37     3.00093570E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03126945E+03   # ~d_L
   2000001     3.02623026E+03   # ~d_R
   1000002     3.03035619E+03   # ~u_L
   2000002     3.02778034E+03   # ~u_R
   1000003     3.03126945E+03   # ~s_L
   2000003     3.02623026E+03   # ~s_R
   1000004     3.03035619E+03   # ~c_L
   2000004     3.02778034E+03   # ~c_R
   1000005     7.40897282E+02   # ~b_1
   2000005     3.02552419E+03   # ~b_2
   1000006     7.38820606E+02   # ~t_1
   2000006     3.01191339E+03   # ~t_2
   1000011     3.00635856E+03   # ~e_L
   2000011     3.00157238E+03   # ~e_R
   1000012     3.00496774E+03   # ~nu_eL
   1000013     3.00635856E+03   # ~mu_L
   2000013     3.00157238E+03   # ~mu_R
   1000014     3.00496774E+03   # ~nu_muL
   1000015     2.98563560E+03   # ~tau_1
   2000015     3.02206582E+03   # ~tau_2
   1000016     3.00491849E+03   # ~nu_tauL
   1000021     2.33863981E+03   # ~g
   1000022     3.02123144E+02   # ~chi_10
   1000023     6.29858875E+02   # ~chi_20
   1000025    -2.99932757E+03   # ~chi_30
   1000035     3.00015626E+03   # ~chi_40
   1000024     6.30021087E+02   # ~chi_1+
   1000037     3.00070848E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99885200E-01   # N_11
  1  2    -7.72596772E-04   # N_12
  1  3     1.49667937E-02   # N_13
  1  4    -2.23270817E-03   # N_14
  2  1     1.19526343E-03   # N_21
  2  2     9.99605327E-01   # N_22
  2  3    -2.72463910E-02   # N_23
  2  4     6.73767032E-03   # N_24
  3  1    -8.99057030E-03   # N_31
  3  2     1.45116787E-02   # N_32
  3  3     7.06870230E-01   # N_33
  3  4     7.07137227E-01   # N_34
  4  1    -1.21378662E-02   # N_41
  4  2     2.40417336E-02   # N_42
  4  3     7.06659824E-01   # N_43
  4  4    -7.07040706E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99256857E-01   # U_11
  1  2    -3.85452230E-02   # U_12
  2  1     3.85452230E-02   # U_21
  2  2     9.99256857E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99954560E-01   # V_11
  1  2    -9.53298425E-03   # V_12
  2  1     9.53298425E-03   # V_21
  2  2     9.99954560E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98856799E-01   # cos(theta_t)
  1  2    -4.78026682E-02   # sin(theta_t)
  2  1     4.78026682E-02   # -sin(theta_t)
  2  2     9.98856799E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99915894E-01   # cos(theta_b)
  1  2     1.29693842E-02   # sin(theta_b)
  2  1    -1.29693842E-02   # -sin(theta_b)
  2  2     9.99915894E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06945573E-01   # cos(theta_tau)
  1  2     7.07267953E-01   # sin(theta_tau)
  2  1    -7.07267953E-01   # -sin(theta_tau)
  2  2     7.06945573E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01879055E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39730227E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44414233E+02   # higgs               
         4     7.09179601E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39730227E+03  # The gauge couplings
     1     3.61936751E-01   # gprime(Q) DRbar
     2     6.37296468E-01   # g(Q) DRbar
     3     1.02794040E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39730227E+03  # The trilinear couplings
  1  1     1.84025930E-06   # A_u(Q) DRbar
  2  2     1.84028585E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39730227E+03  # The trilinear couplings
  1  1     4.92762632E-07   # A_d(Q) DRbar
  2  2     4.92850496E-07   # A_s(Q) DRbar
  3  3     1.08055026E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39730227E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.01600941E-07   # A_mu(Q) DRbar
  3  3     1.02795707E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39730227E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53460524E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39730227E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14817682E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39730227E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06958823E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39730227E+03  # The soft SUSY breaking masses at the scale Q
         1     2.99900000E+02   # M_1(Q)              
         2     5.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.65105553E+04   # M^2_Hd              
        22    -9.08220460E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40967636E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.62862120E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48237241E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48237241E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51762759E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51762759E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.33069971E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.17023200E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.82976800E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03020352E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.28313560E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.49952886E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.03279716E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.35204675E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.27381331E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.60155471E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50639047E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.97916041E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.22663279E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.43076790E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.56923210E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.23627679E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.86041195E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.94808103E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.02506901E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.78647105E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.92529873E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27586048E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.96659688E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.27097781E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.14139297E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.79524564E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.98067936E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.57473177E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.36219825E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.72373605E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.15009497E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.38103239E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.21536581E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17821641E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56762889E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.09656965E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.29134285E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.10550971E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43236887E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.80050062E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.07638026E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.57266150E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.30652795E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.78385503E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.14441095E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.39248789E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.22216054E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67441595E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45398375E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.95640150E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.34955233E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.31451244E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55460099E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.79524564E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.98067936E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.57473177E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.36219825E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.72373605E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.15009497E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.38103239E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.21536581E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17821641E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56762889E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.09656965E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.29134285E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.10550971E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43236887E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.80050062E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.07638026E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.57266150E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.30652795E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.78385503E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.14441095E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.39248789E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.22216054E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67441595E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45398375E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.95640150E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.34955233E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.31451244E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55460099E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.71083730E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.03141980E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99371817E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.32018853E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.63524711E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.97486187E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.37149492E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.53260509E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998667E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.33277290E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.84465525E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.33840114E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.71083730E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.03141980E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99371817E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.32018853E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.63524711E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.97486187E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.37149492E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.53260509E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998667E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.33277290E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.84465525E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.33840114E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.64845565E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.61376912E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.13184422E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25438667E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.59280833E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.69939271E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.10334161E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.08822792E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.22397577E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.19691138E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.23072966E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.71100166E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.03650300E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98383014E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.77082255E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.20536816E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.97966681E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     4.77147289E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.71100166E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.03650300E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98383014E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.77082255E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.20536816E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.97966681E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     4.77147289E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.71126403E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.03641205E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98356131E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.73996590E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.13996501E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.98002158E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.02178417E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.33149416E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.62864291E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.78543615E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.21513430E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.77181208E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.91371634E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.15726681E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.73825388E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.25392608E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.17228489E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.27119198E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.47288080E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.64608183E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.43669951E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.04807619E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.71372706E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.71372706E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.13171907E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.86903723E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.64121549E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.64121549E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.29057589E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.29057589E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.97137689E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.97137689E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.56746917E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.95637264E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.85968929E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.77909963E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.77909963E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.50310646E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.41192308E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.61185438E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.61185438E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.31710833E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.31710833E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     7.02107597E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     7.02107597E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.75471899E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85982663E-01    2           5        -5   # BR(h -> b       bb     )
     5.45578719E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.93139532E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.10422459E-04    2           3        -3   # BR(h -> s       sb     )
     1.77122667E-02    2           4        -4   # BR(h -> c       cb     )
     5.70601757E-02    2          21        21   # BR(h -> g       g      )
     1.92440317E-03    2          22        22   # BR(h -> gam     gam    )
     1.22176561E-03    2          22        23   # BR(h -> Z       gam    )
     1.61057617E-01    2          24       -24   # BR(h -> W+      W-     )
     1.98796751E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39385152E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44125688E-01    2           5        -5   # BR(H -> b       bb     )
     1.78412010E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30822233E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.74648355E-04    2           3        -3   # BR(H -> s       sb     )
     2.18641550E-07    2           4        -4   # BR(H -> c       cb     )
     2.18109096E-02    2           6        -6   # BR(H -> t       tb     )
     3.08951962E-05    2          21        21   # BR(H -> g       g      )
     4.97993415E-08    2          22        22   # BR(H -> gam     gam    )
     8.38375549E-09    2          23        22   # BR(H -> Z       gam    )
     3.33459536E-05    2          24       -24   # BR(H -> W+      W-     )
     1.66524241E-05    2          23        23   # BR(H -> Z       Z      )
     8.56526807E-05    2          25        25   # BR(H -> h       h      )
     6.92558738E-24    2          36        36   # BR(H -> A       A      )
     1.29932205E-17    2          23        36   # BR(H -> Z       A      )
     1.97316417E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.08431326E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     9.84116412E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.65532641E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.03417115E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.14243750E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32491147E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82909913E-01    2           5        -5   # BR(A -> b       bb     )
     1.87689050E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63622700E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.15066490E-04    2           3        -3   # BR(A -> s       sb     )
     2.30000210E-07    2           4        -4   # BR(A -> c       cb     )
     2.29313694E-02    2           6        -6   # BR(A -> t       tb     )
     6.75306960E-05    2          21        21   # BR(A -> g       g      )
     4.47902915E-08    2          22        22   # BR(A -> gam     gam    )
     6.65468107E-08    2          23        22   # BR(A -> Z       gam    )
     3.49480424E-05    2          23        25   # BR(A -> Z       h      )
     2.64831146E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22500916E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32073778E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.96608727E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00619068E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09574086E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.47218511E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.74104355E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.01273047E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.13692224E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05683009E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13488743E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.60900666E-05    2          24        25   # BR(H+ -> W+      h      )
     9.39628823E-14    2          24        36   # BR(H+ -> W+      A      )
     1.96720570E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.72945316E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.41943909E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

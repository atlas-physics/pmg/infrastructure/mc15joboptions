#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15501003E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04035229E+01   # W+
        25     1.23822265E+02   # h
        35     3.00012867E+03   # H
        36     2.99999987E+03   # A
        37     3.00090935E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03715980E+03   # ~d_L
   2000001     3.03211705E+03   # ~d_R
   1000002     3.03624977E+03   # ~u_L
   2000002     3.03332770E+03   # ~u_R
   1000003     3.03715980E+03   # ~s_L
   2000003     3.03211705E+03   # ~s_R
   1000004     3.03624977E+03   # ~c_L
   2000004     3.03332770E+03   # ~c_R
   1000005     9.00577621E+02   # ~b_1
   2000005     3.03120974E+03   # ~b_2
   1000006     8.98915889E+02   # ~t_1
   2000006     3.01748948E+03   # ~t_2
   1000011     3.00626120E+03   # ~e_L
   2000011     3.00192584E+03   # ~e_R
   1000012     3.00487170E+03   # ~nu_eL
   1000013     3.00626120E+03   # ~mu_L
   2000013     3.00192584E+03   # ~mu_R
   1000014     3.00487170E+03   # ~nu_muL
   1000015     2.98553754E+03   # ~tau_1
   2000015     3.02201965E+03   # ~tau_2
   1000016     3.00469022E+03   # ~nu_tauL
   1000021     2.34597647E+03   # ~g
   1000022     3.51903111E+02   # ~chi_10
   1000023     7.32825024E+02   # ~chi_20
   1000025    -2.99679843E+03   # ~chi_30
   1000035     2.99773536E+03   # ~chi_40
   1000024     7.32988063E+02   # ~chi_1+
   1000037     2.99825394E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99883624E-01   # N_11
  1  2    -7.19277310E-04   # N_12
  1  3     1.50339792E-02   # N_13
  1  4    -2.48992978E-03   # N_14
  2  1     1.15445209E-03   # N_21
  2  2     9.99586330E-01   # N_22
  2  3    -2.76744733E-02   # N_23
  2  4     7.74339898E-03   # N_24
  3  1    -8.85735931E-03   # N_31
  3  2     1.41028948E-02   # N_32
  3  3     7.06878615E-01   # N_33
  3  4     7.07138798E-01   # N_34
  4  1    -1.23673791E-02   # N_41
  4  2     2.50551483E-02   # N_42
  4  3     7.06633375E-01   # N_43
  4  4    -7.07027977E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99233352E-01   # U_11
  1  2    -3.91498237E-02   # U_12
  2  1     3.91498237E-02   # U_21
  2  2     9.99233352E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99939987E-01   # V_11
  1  2    -1.09555111E-02   # V_12
  2  1     1.09555111E-02   # V_21
  2  2     9.99939987E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98805992E-01   # cos(theta_t)
  1  2    -4.88527414E-02   # sin(theta_t)
  2  1     4.88527414E-02   # -sin(theta_t)
  2  2     9.98805992E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99910464E-01   # cos(theta_b)
  1  2     1.33814791E-02   # sin(theta_b)
  2  1    -1.33814791E-02   # -sin(theta_b)
  2  2     9.99910464E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06958550E-01   # cos(theta_tau)
  1  2     7.07254981E-01   # sin(theta_tau)
  2  1    -7.07254981E-01   # -sin(theta_tau)
  2  2     7.06958550E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01826943E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.55010026E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44203523E+02   # higgs               
         4     7.47709814E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.55010026E+03  # The gauge couplings
     1     3.62284026E-01   # gprime(Q) DRbar
     2     6.37087501E-01   # g(Q) DRbar
     3     1.02560655E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.55010026E+03  # The trilinear couplings
  1  1     2.28216428E-06   # A_u(Q) DRbar
  2  2     2.28219681E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.55010026E+03  # The trilinear couplings
  1  1     6.26155482E-07   # A_d(Q) DRbar
  2  2     6.26262249E-07   # A_s(Q) DRbar
  3  3     1.34840471E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.55010026E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.27307634E-07   # A_mu(Q) DRbar
  3  3     1.28806677E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.55010026E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50961584E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.55010026E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.16211608E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.55010026E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.07426519E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.55010026E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.57382076E+04   # M^2_Hd              
        22    -9.04673734E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40873434E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.91270094E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48200676E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48200676E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51799324E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51799324E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.35467157E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     9.00252222E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.09974778E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.00407050E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.42811508E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.81763856E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.67437695E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.40616573E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.34388571E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.56987537E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.49173079E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.93946399E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.32195708E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.83617319E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.16382681E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.21420985E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.86979885E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.66622443E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.79940538E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.72726121E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     4.92369103E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.28343532E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.94503240E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.24848459E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.09879514E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.68794011E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.06395656E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55324533E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.07015528E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.60205639E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.10716069E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.99276034E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.27895325E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16018057E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57002676E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.91155917E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.38171502E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.55365632E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42997109E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.69324681E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.15382233E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55124573E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.48786779E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.00443505E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.10149114E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.99725492E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.28571955E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66050971E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45892695E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     5.42847772E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.23135541E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.12880695E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55410670E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.68794011E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.06395656E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55324533E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.07015528E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.60205639E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.10716069E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.99276034E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.27895325E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16018057E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57002676E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.91155917E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.38171502E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.55365632E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42997109E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.69324681E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.15382233E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55124573E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.48786779E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.00443505E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.10149114E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.99725492E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.28571955E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66050971E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45892695E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     5.42847772E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.23135541E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.12880695E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55410670E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59919551E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.05787727E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98483357E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.09505396E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.51391387E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95728882E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.92567520E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52454093E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998786E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.21199891E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     9.39814114E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.22420991E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59919551E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.05787727E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98483357E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.09505396E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.51391387E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95728882E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.92567520E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52454093E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998786E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.21199891E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     9.39814114E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.22420991E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58754109E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.67872662E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.11013023E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.21114314E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53374876E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76561670E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.08116866E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.38528191E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.53884760E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15276526E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.56962262E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59929157E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06269667E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97519943E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.55562968E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.81120556E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96210377E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.56630258E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59929157E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06269667E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97519943E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.55562968E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.81120556E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96210377E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.56630258E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59935854E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06260914E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97491894E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.39791846E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     7.41973054E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96245901E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.28053754E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.94483629E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.25852252E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.82006726E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.11435966E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.30299759E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.18617208E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.25412055E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.99920739E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.59679308E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.74065997E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.57726336E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.44227366E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.27133457E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.53778381E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.43851732E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.97656500E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.97656500E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.16408603E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.75419552E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.59470098E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.59470098E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.30293842E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.30293842E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.41469296E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.41469296E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.19740578E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     7.96095656E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.74272200E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.04411876E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.04411876E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.61437684E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.87576447E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.56112284E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.56112284E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.33012049E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.33012049E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.28233565E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.28233565E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.66447142E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88284793E-01    2           5        -5   # BR(h -> b       bb     )
     5.54360274E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.96249715E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.17275251E-04    2           3        -3   # BR(h -> s       sb     )
     1.80117311E-02    2           4        -4   # BR(h -> c       cb     )
     5.75673472E-02    2          21        21   # BR(h -> g       g      )
     1.93826899E-03    2          22        22   # BR(h -> gam     gam    )
     1.20388358E-03    2          22        23   # BR(h -> Z       gam    )
     1.57612776E-01    2          24       -24   # BR(h -> W+      W-     )
     1.93316470E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39762011E+01   # H decays
#          BR         NDA      ID1       ID2
     7.46992067E-01    2           5        -5   # BR(H -> b       bb     )
     1.77928270E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.29111843E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.72549590E-04    2           3        -3   # BR(H -> s       sb     )
     2.18003823E-07    2           4        -4   # BR(H -> c       cb     )
     2.17472848E-02    2           6        -6   # BR(H -> t       tb     )
     2.68102456E-05    2          21        21   # BR(H -> g       g      )
     3.38868462E-08    2          22        22   # BR(H -> gam     gam    )
     8.37813246E-09    2          23        22   # BR(H -> Z       gam    )
     3.17614923E-05    2          24       -24   # BR(H -> W+      W-     )
     1.58611702E-05    2          23        23   # BR(H -> Z       Z      )
     8.28244664E-05    2          25        25   # BR(H -> h       h      )
    -2.09732901E-23    2          36        36   # BR(H -> A       A      )
     2.84565489E-18    2          23        36   # BR(H -> Z       A      )
     1.79785266E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.06448829E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.96793948E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.35982427E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.83302436E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.87794949E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33170236E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84040630E-01    2           5        -5   # BR(A -> b       bb     )
     1.86731959E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60238662E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.10910182E-04    2           3        -3   # BR(A -> s       sb     )
     2.28827358E-07    2           4        -4   # BR(A -> c       cb     )
     2.28144344E-02    2           6        -6   # BR(A -> t       tb     )
     6.71863279E-05    2          21        21   # BR(A -> g       g      )
     5.34856766E-08    2          22        22   # BR(A -> gam     gam    )
     6.62120677E-08    2          23        22   # BR(A -> Z       gam    )
     3.32098273E-05    2          23        25   # BR(A -> Z       h      )
     2.62149871E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22212314E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30750097E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.89871339E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.02424286E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10669730E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.42859177E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.58690813E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.08285167E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.04634593E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03819563E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.19803725E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.32451238E-05    2          24        25   # BR(H+ -> W+      h      )
     7.99382818E-14    2          24        36   # BR(H+ -> W+      A      )
     1.89153834E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.40634819E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.23411823E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

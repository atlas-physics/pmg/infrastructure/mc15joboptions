#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.11565880E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04006816E+01   # W+
        25     1.26326076E+02   # h
        35     3.00020205E+03   # H
        36     3.00000004E+03   # A
        37     3.00110355E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02029864E+03   # ~d_L
   2000001     3.01487659E+03   # ~d_R
   1000002     3.01938211E+03   # ~u_L
   2000002     3.01843649E+03   # ~u_R
   1000003     3.02029864E+03   # ~s_L
   2000003     3.01487659E+03   # ~s_R
   1000004     3.01938211E+03   # ~c_L
   2000004     3.01843649E+03   # ~c_R
   1000005     5.18479951E+02   # ~b_1
   2000005     3.01678047E+03   # ~b_2
   1000006     5.13962273E+02   # ~t_1
   2000006     3.01918053E+03   # ~t_2
   1000011     3.00739343E+03   # ~e_L
   2000011     2.99955298E+03   # ~e_R
   1000012     3.00600362E+03   # ~nu_eL
   1000013     3.00739343E+03   # ~mu_L
   2000013     2.99955298E+03   # ~mu_R
   1000014     3.00600362E+03   # ~nu_muL
   1000015     2.98633022E+03   # ~tau_1
   2000015     3.02238394E+03   # ~tau_2
   1000016     3.00661097E+03   # ~nu_tauL
   1000021     2.32504778E+03   # ~g
   1000022     2.02593504E+02   # ~chi_10
   1000023     4.25443739E+02   # ~chi_20
   1000025    -3.00843749E+03   # ~chi_30
   1000035     3.00850492E+03   # ~chi_40
   1000024     4.25603507E+02   # ~chi_1+
   1000037     3.00940433E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891611E-01   # N_11
  1  2     1.90971593E-04   # N_12
  1  3    -1.47196805E-02   # N_13
  1  4    -2.45762396E-04   # N_14
  2  1     1.96039126E-04   # N_21
  2  2     9.99652984E-01   # N_22
  2  3     2.62493718E-02   # N_23
  2  4     2.20063173E-03   # N_24
  3  1    -1.02348421E-02   # N_31
  3  2     1.70055845E-02   # N_32
  3  3    -7.06826868E-01   # N_33
  3  4     7.07108080E-01   # N_34
  4  1     1.05818449E-02   # N_41
  4  2    -2.01167681E-02   # N_42
  4  3     7.06746121E-01   # N_43
  4  4     7.07102016E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99311058E-01   # U_11
  1  2     3.71134728E-02   # U_12
  2  1    -3.71134728E-02   # U_21
  2  2     9.99311058E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995163E-01   # V_11
  1  2    -3.11016659E-03   # V_12
  2  1    -3.11016659E-03   # V_21
  2  2    -9.99995163E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98649816E-01   # cos(theta_t)
  1  2    -5.19475216E-02   # sin(theta_t)
  2  1     5.19475216E-02   # -sin(theta_t)
  2  2     9.98649816E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99721327E-01   # cos(theta_b)
  1  2    -2.36065318E-02   # sin(theta_b)
  2  1     2.36065318E-02   # -sin(theta_b)
  2  2     9.99721327E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06916010E-01   # cos(theta_tau)
  1  2     7.07297501E-01   # sin(theta_tau)
  2  1    -7.07297501E-01   # -sin(theta_tau)
  2  2    -7.06916010E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00161869E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.15658802E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44598706E+02   # higgs               
         4     1.17836277E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.15658802E+03  # The gauge couplings
     1     3.61283158E-01   # gprime(Q) DRbar
     2     6.37789468E-01   # g(Q) DRbar
     3     1.03215115E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.15658802E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     1.22269197E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.15658802E+03  # The trilinear couplings
  1  1     4.36610845E-07   # A_d(Q) DRbar
  2  2     4.36662823E-07   # A_s(Q) DRbar
  3  3     9.02652334E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.15658802E+03  # The trilinear couplings
  1  1     2.03922436E-07   # A_e(Q) DRbar
  2  2     2.03932498E-07   # A_mu(Q) DRbar
  3  3     2.06778831E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.15658802E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.58877700E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.15658802E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.00966215E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.15658802E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.02259819E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.15658802E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -6.83595250E+04   # M^2_Hd              
        22    -9.16474009E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41196540E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.37073557E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48052625E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48052625E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51947375E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51947375E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     4.77427452E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     7.52052686E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.24794731E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.23565839E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.28323347E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.62809180E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.28559143E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.49527713E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.86542757E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.72671815E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.64094629E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.23052143E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.98621235E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.92916355E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.07083645E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.71471577E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.86848266E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.10931615E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.46796817E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.26425079E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -8.60966047E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.20171854E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.79339725E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.11018774E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.62185896E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96146641E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.91323096E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60655246E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.43187467E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.47181489E-09    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.21497262E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.72883311E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.11934255E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.21328558E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55956769E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     5.82099460E-09    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.86232008E-10    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.34916042E-10    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44043224E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.96660082E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.88688249E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60567003E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.48652603E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.05024614E-09    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.20928723E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.91932047E-08    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.12617352E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.69162143E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44438730E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.65872187E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.07198628E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.12258435E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55556125E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96146641E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.91323096E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60655246E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.43187467E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.47181489E-09    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.21497262E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.72883311E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.11934255E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.21328558E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55956769E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     5.82099460E-09    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.86232008E-10    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.34916042E-10    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44043224E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.96660082E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.88688249E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60567003E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.48652603E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.05024614E-09    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.20928723E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.91932047E-08    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.12617352E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.69162143E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44438730E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.65872187E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.07198628E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.12258435E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55556125E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.88922982E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.95337114E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00342966E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.00123323E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54328653E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.72476758E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.88922982E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.95337114E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00342966E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.00123323E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54328653E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999963E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.72476758E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.74323375E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51821610E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16213038E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.31965351E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.68777754E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.59936865E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13505676E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.65055853E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.59932476E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26545022E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.18698719E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.88945189E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.93471178E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00042335E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.00610547E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.88945189E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.93471178E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00042335E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.00610547E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89062842E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.93375079E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00017160E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.00645332E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.90578081E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.99295375E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.46586262E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.63429382E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     6.01389727E-12    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     6.01389727E-12    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     3.29275141E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.99567226E-10    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.99567226E-10    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.11377720E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.71300828E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.82491162E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.56878961E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.89161139E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.00958425E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     9.99930381E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     6.96194044E-05    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     1.00205790E+02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.63556930E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.32120702E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.55267626E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.55267626E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.23658089E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.40066041E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34223407E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34223407E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     7.22031068E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     7.22031068E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.67453901E-12    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     3.67453901E-12    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     2.84051775E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.84051775E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     3.67453901E-12    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     3.67453901E-12    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     2.84051775E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.84051775E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.31016129E-06    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.31016129E-06    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     8.26203481E-11    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     8.26203481E-11    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     8.26203481E-11    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     8.26203481E-11    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     4.65402634E-11    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     4.65402634E-11    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     9.91956728E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.10988172E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.40040628E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.61022405E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.61022405E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.00078931E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.51123951E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31842189E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31842189E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     7.29373328E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     7.29373328E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.67911519E-12    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     6.67911519E-12    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.11390959E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.11390959E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     6.67911519E-12    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     6.67911519E-12    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.11390959E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.11390959E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.12057599E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.12057599E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.15573426E-10    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.15573426E-10    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.15573426E-10    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.15573426E-10    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     6.62752151E-11    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     6.62752151E-11    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY        25     3.79473886E-03   # h decays
#          BR         NDA      ID1       ID2
     5.41823679E-01    2           5        -5   # BR(h -> b       bb     )
     6.90627085E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.44477861E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.17842335E-04    2           3        -3   # BR(h -> s       sb     )
     2.24998928E-02    2           4        -4   # BR(h -> c       cb     )
     7.43646721E-02    2          21        21   # BR(h -> g       g      )
     2.59326340E-03    2          22        22   # BR(h -> gam     gam    )
     1.85065613E-03    2          22        23   # BR(h -> Z       gam    )
     2.54557701E-01    2          24       -24   # BR(h -> W+      W-     )
     3.24851072E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.24949228E+01   # H decays
#          BR         NDA      ID1       ID2
     9.11704155E-01    2           5        -5   # BR(H -> b       bb     )
     5.85214295E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.06917790E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.54094359E-04    2           3        -3   # BR(H -> s       sb     )
     7.12264092E-08    2           4        -4   # BR(H -> c       cb     )
     7.10529671E-03    2           6        -6   # BR(H -> t       tb     )
     3.46514332E-06    2          21        21   # BR(H -> g       g      )
     3.72411981E-08    2          22        22   # BR(H -> gam     gam    )
     2.91061662E-09    2          23        22   # BR(H -> Z       gam    )
     6.93513865E-07    2          24       -24   # BR(H -> W+      W-     )
     3.46329404E-07    2          23        23   # BR(H -> Z       Z      )
     4.81871082E-06    2          25        25   # BR(H -> h       h      )
     3.88267785E-25    2          36        36   # BR(H -> A       A      )
     8.58172208E-18    2          23        36   # BR(H -> Z       A      )
     7.31844116E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.60878836E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.65806632E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.31451199E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.08282539E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.22802877E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     4.16148974E+01   # A decays
#          BR         NDA      ID1       ID2
     9.30971330E-01    2           5        -5   # BR(A -> b       bb     )
     5.97553810E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.11280452E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.59496268E-04    2           3        -3   # BR(A -> s       sb     )
     7.32261683E-08    2           4        -4   # BR(A -> c       cb     )
     7.30075994E-03    2           6        -6   # BR(A -> t       tb     )
     2.15000385E-05    2          21        21   # BR(A -> g       g      )
     5.66460798E-08    2          22        22   # BR(A -> gam     gam    )
     2.11778994E-08    2          23        22   # BR(A -> Z       gam    )
     7.05348931E-07    2          23        25   # BR(A -> Z       h      )
     7.99181250E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.74081037E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.99431028E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.43375233E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.52458943E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.48365415E-03    2           4        -5   # BR(H+ -> c       bb     )
     5.49802096E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     1.94396611E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.49537497E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.14241856E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.35032234E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.32525906E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.49867481E-07    2          24        25   # BR(H+ -> W+      h      )
     4.75850227E-14    2          24        36   # BR(H+ -> W+      A      )
     4.41470650E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.59261277E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.01177610E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

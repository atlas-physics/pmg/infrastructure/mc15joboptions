#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15475152E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.99000000E+01   # M_1(MX)             
         2     9.99000000E+01   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     3.24338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04002204E+01   # W+
        25     1.25453021E+02   # h
        35     3.00012962E+03   # H
        36     2.99999979E+03   # A
        37     3.00094467E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03725882E+03   # ~d_L
   2000001     3.03202657E+03   # ~d_R
   1000002     3.03633997E+03   # ~u_L
   2000002     3.03325131E+03   # ~u_R
   1000003     3.03725882E+03   # ~s_L
   2000003     3.03202657E+03   # ~s_R
   1000004     3.03633997E+03   # ~c_L
   2000004     3.03325131E+03   # ~c_R
   1000005     8.95998294E+02   # ~b_1
   2000005     3.03116722E+03   # ~b_2
   1000006     8.91620756E+02   # ~t_1
   2000006     3.01556505E+03   # ~t_2
   1000011     3.00646339E+03   # ~e_L
   2000011     3.00193650E+03   # ~e_R
   1000012     3.00506074E+03   # ~nu_eL
   1000013     3.00646339E+03   # ~mu_L
   2000013     3.00193650E+03   # ~mu_R
   1000014     3.00506074E+03   # ~nu_muL
   1000015     2.98587698E+03   # ~tau_1
   2000015     3.02192134E+03   # ~tau_2
   1000016     3.00488673E+03   # ~nu_tauL
   1000021     2.34584656E+03   # ~g
   1000022     4.99222177E+01   # ~chi_10
   1000023     1.08522657E+02   # ~chi_20
   1000025    -2.99678207E+03   # ~chi_30
   1000035     2.99718629E+03   # ~chi_40
   1000024     1.08674393E+02   # ~chi_1+
   1000037     2.99789366E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886268E-01   # N_11
  1  2    -2.69632580E-03   # N_12
  1  3     1.48057571E-02   # N_13
  1  4    -9.85362555E-04   # N_14
  2  1     3.08428870E-03   # N_21
  2  2     9.99652258E-01   # N_22
  2  3    -2.60988621E-02   # N_23
  2  4     2.16807032E-03   # N_24
  3  1    -9.72360647E-03   # N_31
  3  2     1.69499368E-02   # N_32
  3  3     7.06818159E-01   # N_33
  3  4     7.07125337E-01   # N_34
  4  1    -1.11081140E-02   # N_41
  4  2     2.00198261E-02   # N_42
  4  3     7.06758608E-01   # N_43
  4  4    -7.07084215E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99316695E-01   # U_11
  1  2    -3.69613816E-02   # U_12
  2  1     3.69613816E-02   # U_21
  2  2     9.99316695E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99995282E-01   # V_11
  1  2    -3.07167944E-03   # V_12
  2  1     3.07167944E-03   # V_21
  2  2     9.99995282E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98532892E-01   # cos(theta_t)
  1  2    -5.41485327E-02   # sin(theta_t)
  2  1     5.41485327E-02   # -sin(theta_t)
  2  2     9.98532892E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99914940E-01   # cos(theta_b)
  1  2     1.30427284E-02   # sin(theta_b)
  2  1    -1.30427284E-02   # -sin(theta_b)
  2  2     9.99914940E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06880989E-01   # cos(theta_tau)
  1  2     7.07332501E-01   # sin(theta_tau)
  2  1    -7.07332501E-01   # -sin(theta_tau)
  2  2     7.06880989E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01765043E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.54751517E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44136524E+02   # higgs               
         4     7.47751299E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.54751517E+03  # The gauge couplings
     1     3.62234997E-01   # gprime(Q) DRbar
     2     6.41786374E-01   # g(Q) DRbar
     3     1.02568347E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.54751517E+03  # The trilinear couplings
  1  1     2.27498514E-06   # A_u(Q) DRbar
  2  2     2.27501852E-06   # A_c(Q) DRbar
  3  3     3.24338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.54751517E+03  # The trilinear couplings
  1  1     5.58089243E-07   # A_d(Q) DRbar
  2  2     5.58202447E-07   # A_s(Q) DRbar
  3  3     1.31284483E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.54751517E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.23506488E-07   # A_mu(Q) DRbar
  3  3     1.25006409E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.54751517E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52674735E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.54751517E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.10793073E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.54751517E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.04780267E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.54751517E+03  # The soft SUSY breaking masses at the scale Q
         1     4.99000000E+01   # M_1(Q)              
         2     9.99000000E+01   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.24190942E+04   # M^2_Hd              
        22    -9.06375396E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42973843E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.94948488E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47741261E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47741261E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52258739E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52258739E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.07842078E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.14774367E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.14490605E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.74031958E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.14854757E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.74887985E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.07234006E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.15678899E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     5.47782224E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.91794774E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69808356E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61718490E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.18211890E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.06239244E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.28984521E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.44241527E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.42860021E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19845579E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.97849809E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.82795539E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.71380142E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.69595862E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.77469166E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.31745953E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.81237148E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.10792374E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     5.92312559E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.11665569E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.71360780E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.66010299E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.93957859E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.05178663E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.31861153E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.29558230E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.96414868E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17690809E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60153325E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.52154474E-06    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.00264536E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.28046836E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39845131E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.12163656E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.04843390E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65561206E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.73635670E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.43853145E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.31282690E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.47639202E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.97107211E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66527945E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.56082247E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.33302640E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.67409746E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.41246406E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54391336E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.11665569E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.71360780E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.66010299E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.93957859E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.05178663E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.31861153E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.29558230E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.96414868E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17690809E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60153325E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.52154474E-06    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.00264536E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.28046836E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39845131E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.12163656E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.04843390E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65561206E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.73635670E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.43853145E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.31282690E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.47639202E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.97107211E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66527945E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.56082247E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.33302640E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.67409746E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.41246406E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54391336E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.07416428E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.53235479E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.02395428E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.64217555E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.17044760E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.02280993E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.67712423E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56605691E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99990502E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.49530831E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.11394032E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.23483999E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.07416428E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.53235479E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.02395428E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.64217555E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.17044760E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.02280993E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.67712423E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56605691E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99990502E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.49530831E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.11394032E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.23483999E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.84924433E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.42811477E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.19668324E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.37520199E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.78884111E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.50824390E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.17024755E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.23194829E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.39067418E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.32110534E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.40942981E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.07452821E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.71087473E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00128701E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     4.60581731E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.72103542E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.02762541E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.29431627E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.07452821E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.71087473E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00128701E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     4.60581731E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.72103542E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.02762541E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.29431627E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.07463377E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.71006025E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00103457E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.41462866E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     5.47147317E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02794640E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.29066485E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.80056777E-08   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.35839224E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     7.29659610E-09    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     3.35839224E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     7.29659610E-09    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     1.09616730E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     2.43223415E-09    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     1.09616730E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.43223415E-09    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     1.09088074E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     8.27551266E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.58958753E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.14244135E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.77801655E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.23565869E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.22377557E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.08816999E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.43835697E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.21220197E-10   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.13563464E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     5.08820994E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     6.45250191E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     5.08820994E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     6.45250191E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     7.38341594E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     5.50711176E-03    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     5.50711176E-03    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     5.79376053E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     8.93474725E-04    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     8.93474725E-04    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     8.92889218E-04    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
#         PDG            Width
DECAY   1000025     8.31758267E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.30833062E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.17887105E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.02854744E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.02854744E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.02882788E-02    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.05750893E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.60025922E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.60025922E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.18206062E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.18206062E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.04548594E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.04548594E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.21068948E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.01550234E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.03561046E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.12155026E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.12155026E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.36160193E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.37624158E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.57720496E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.57720496E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.21187664E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.21187664E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.53459822E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.53459822E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.89203334E-03   # h decays
#          BR         NDA      ID1       ID2
     6.65956205E-01    2           5        -5   # BR(h -> b       bb     )
     5.35419672E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.89538498E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.01980624E-04    2           3        -3   # BR(h -> s       sb     )
     1.73556628E-02    2           4        -4   # BR(h -> c       cb     )
     5.68623903E-02    2          21        21   # BR(h -> g       g      )
     1.95667722E-03    2          22        22   # BR(h -> gam     gam    )
     1.32988027E-03    2          22        23   # BR(h -> Z       gam    )
     1.79727729E-01    2          24       -24   # BR(h -> W+      W-     )
     2.26363306E-02    2          23        23   # BR(h -> Z       Z      )
     4.16384034E-05    2     1000022   1000022   # BR(h -> ~chi_10 ~chi_10)
#
#         PDG            Width
DECAY        35     1.41162579E+01   # H decays
#          BR         NDA      ID1       ID2
     7.36126084E-01    2           5        -5   # BR(H -> b       bb     )
     1.76163090E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.22870591E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.64885305E-04    2           3        -3   # BR(H -> s       sb     )
     2.15787721E-07    2           4        -4   # BR(H -> c       cb     )
     2.15262146E-02    2           6        -6   # BR(H -> t       tb     )
     3.15587253E-05    2          21        21   # BR(H -> g       g      )
     2.03435617E-08    2          22        22   # BR(H -> gam     gam    )
     8.30362416E-09    2          23        22   # BR(H -> Z       gam    )
     2.97346943E-05    2          24       -24   # BR(H -> W+      W-     )
     1.48490151E-05    2          23        23   # BR(H -> Z       Z      )
     7.99056418E-05    2          25        25   # BR(H -> h       h      )
    -2.13055121E-23    2          36        36   # BR(H -> A       A      )
     3.15448558E-18    2          23        36   # BR(H -> Z       A      )
     2.41782052E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.13070994E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.20073236E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.37724296E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.01655762E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.63782176E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32681812E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83250711E-01    2           5        -5   # BR(A -> b       bb     )
     1.87419345E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.62669091E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.13895254E-04    2           3        -3   # BR(A -> s       sb     )
     2.29669704E-07    2           4        -4   # BR(A -> c       cb     )
     2.28984175E-02    2           6        -6   # BR(A -> t       tb     )
     6.74336524E-05    2          21        21   # BR(A -> g       g      )
     4.28029802E-08    2          22        22   # BR(A -> gam     gam    )
     6.64180006E-08    2          23        22   # BR(A -> Z       gam    )
     3.15134313E-05    2          23        25   # BR(A -> Z       h      )
     2.62928485E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22046335E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30573601E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.98608738E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.02458349E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09316353E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.42781295E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.58415443E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.99623564E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.04471995E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.03786111E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.11348098E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.08734821E-05    2          24        25   # BR(H+ -> W+      h      )
     9.67212895E-14    2          24        36   # BR(H+ -> W+      A      )
     2.03677927E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.93858644E-08    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.07460510E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14980723E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04031615E+01   # W+
        25     1.25228390E+02   # h
        35     3.00009670E+03   # H
        36     2.99999993E+03   # A
        37     3.00109067E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03509922E+03   # ~d_L
   2000001     3.02997416E+03   # ~d_R
   1000002     3.03419167E+03   # ~u_L
   2000002     3.03193170E+03   # ~u_R
   1000003     3.03509922E+03   # ~s_L
   2000003     3.02997416E+03   # ~s_R
   1000004     3.03419167E+03   # ~c_L
   2000004     3.03193170E+03   # ~c_R
   1000005     8.49101657E+02   # ~b_1
   2000005     3.02877101E+03   # ~b_2
   1000006     8.46475331E+02   # ~t_1
   2000006     3.02302660E+03   # ~t_2
   1000011     3.00658975E+03   # ~e_L
   2000011     3.00116390E+03   # ~e_R
   1000012     3.00520412E+03   # ~nu_eL
   1000013     3.00658975E+03   # ~mu_L
   2000013     3.00116390E+03   # ~mu_R
   1000014     3.00520412E+03   # ~nu_muL
   1000015     2.98617749E+03   # ~tau_1
   2000015     3.02171309E+03   # ~tau_2
   1000016     3.00527345E+03   # ~nu_tauL
   1000021     2.34352968E+03   # ~g
   1000022     3.52770635E+02   # ~chi_10
   1000023     7.34626839E+02   # ~chi_20
   1000025    -2.99916612E+03   # ~chi_30
   1000035     2.99951940E+03   # ~chi_40
   1000024     7.34788966E+02   # ~chi_1+
   1000037     3.00029996E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889541E-01   # N_11
  1  2    -5.59821731E-05   # N_12
  1  3    -1.48295199E-02   # N_13
  1  4    -9.93881060E-04   # N_14
  2  1     4.61814807E-04   # N_21
  2  2     9.99621994E-01   # N_22
  2  3     2.70287383E-02   # N_23
  2  4     5.01022832E-03   # N_24
  3  1    -9.77977322E-03   # N_31
  3  2     1.55730474E-02   # N_32
  3  3    -7.06857343E-01   # N_33
  3  4     7.07117058E-01   # N_34
  4  1     1.11825022E-02   # N_41
  4  2    -2.26571273E-02   # N_42
  4  3     7.06683967E-01   # N_43
  4  4     7.07078055E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99269324E-01   # U_11
  1  2     3.82206450E-02   # U_12
  2  1    -3.82206450E-02   # U_21
  2  2     9.99269324E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99974911E-01   # V_11
  1  2    -7.08353026E-03   # V_12
  2  1    -7.08353026E-03   # V_21
  2  2    -9.99974911E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98559178E-01   # cos(theta_t)
  1  2    -5.36616067E-02   # sin(theta_t)
  2  1     5.36616067E-02   # -sin(theta_t)
  2  2     9.98559178E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99726255E-01   # cos(theta_b)
  1  2    -2.33969029E-02   # sin(theta_b)
  2  1     2.33969029E-02   # -sin(theta_b)
  2  2     9.99726255E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06953291E-01   # cos(theta_tau)
  1  2     7.07260238E-01   # sin(theta_tau)
  2  1    -7.07260238E-01   # -sin(theta_tau)
  2  2    -7.06953291E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00119465E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.49807231E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44081082E+02   # higgs               
         4     1.07106506E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.49807231E+03  # The gauge couplings
     1     3.62145919E-01   # gprime(Q) DRbar
     2     6.36900549E-01   # g(Q) DRbar
     3     1.02635953E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.49807231E+03  # The trilinear couplings
  1  1     2.14286573E-06   # A_u(Q) DRbar
  2  2     2.14289714E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.49807231E+03  # The trilinear couplings
  1  1     7.68581288E-07   # A_d(Q) DRbar
  2  2     7.68668879E-07   # A_s(Q) DRbar
  3  3     1.54709310E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.49807231E+03  # The trilinear couplings
  1  1     3.25944119E-07   # A_e(Q) DRbar
  2  2     3.25959961E-07   # A_mu(Q) DRbar
  3  3     3.30388885E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.49807231E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.52959767E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.49807231E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.81944653E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.49807231E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01242700E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.49807231E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.45163712E+04   # M^2_Hd              
        22    -9.06659762E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40792353E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.16496790E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47973951E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47973951E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52026049E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52026049E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.13247994E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.43652833E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     8.56347167E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.16033904E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.56546233E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.27114206E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.58018879E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     9.07743080E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.95331634E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.69841112E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.61471175E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.16625549E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     3.18525755E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.77692291E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.22307709E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.39638760E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.01358143E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.30852311E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.30395272E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.17143402E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -7.45185961E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.47287718E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.19758029E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.02095151E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.48440782E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.68550393E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.09876793E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55027636E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.00451407E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.40435765E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.10203174E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.00251880E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.28670348E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16846920E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56468345E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.04680383E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.25395107E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.05664727E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43531606E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.69077303E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.10314237E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.54910214E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.39536409E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.73293658E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.09639386E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.06921302E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.29346849E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66344091E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.44785604E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.65994246E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.08420575E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.66453023E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55521426E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.68550393E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.09876793E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55027636E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.00451407E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.40435765E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.10203174E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.00251880E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.28670348E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16846920E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56468345E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.04680383E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.25395107E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.05664727E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43531606E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.69077303E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.10314237E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.54910214E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.39536409E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.73293658E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.09639386E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.06921302E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.29346849E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66344091E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.44785604E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.65994246E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.08420575E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.66453023E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55521426E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59572802E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06055290E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98237306E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.22737527E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.97791336E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95707384E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.72199415E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52277745E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999806E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93842138E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.74230795E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.54372133E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59572802E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06055290E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98237306E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.22737527E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.97791336E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95707384E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.72199415E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52277745E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999806E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93842138E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.74230795E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.54372133E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58290746E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68299335E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10765175E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20935489E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53337200E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.76580299E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07998592E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.21876124E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.03160172E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15386583E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.20224552E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59573344E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06045283E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97751465E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.42724766E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.05602357E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96203245E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.59908146E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59573344E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06045283E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97751465E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.42724766E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.05602357E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96203245E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.59908146E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59616509E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06035137E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97724340E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.48289577E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.15486977E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96239840E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.76059443E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.35675060E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.07113276E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.35444040E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.63690479E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.53578178E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     5.98734015E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.96481090E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.82723964E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.30711206E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.22641030E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.36791521E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.63208479E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.08960550E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.14263186E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.05373629E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.81170629E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.81170629E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.05089195E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.94964080E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.35097094E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.35097094E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.65298304E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.65298304E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     5.22059452E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     5.22059452E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.99791607E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.83446097E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.93914839E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.87351901E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.87351901E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.20150646E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.38851723E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.31989742E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.31989742E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.72115642E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.72115642E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.13086639E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.13086639E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.63326795E-03   # h decays
#          BR         NDA      ID1       ID2
     5.58262810E-01    2           5        -5   # BR(h -> b       bb     )
     7.14916298E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.53081373E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.36977991E-04    2           3        -3   # BR(h -> s       sb     )
     2.33353264E-02    2           4        -4   # BR(h -> c       cb     )
     7.60359042E-02    2          21        21   # BR(h -> g       g      )
     2.60981902E-03    2          22        22   # BR(h -> gam     gam    )
     1.75545213E-03    2          22        23   # BR(h -> Z       gam    )
     2.36078686E-01    2          24       -24   # BR(h -> W+      W-     )
     2.96403131E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.69574721E+01   # H decays
#          BR         NDA      ID1       ID2
     9.00308298E-01    2           5        -5   # BR(H -> b       bb     )
     6.72875392E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.37912659E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.92157315E-04    2           3        -3   # BR(H -> s       sb     )
     8.18821084E-08    2           4        -4   # BR(H -> c       cb     )
     8.16826555E-03    2           6        -6   # BR(H -> t       tb     )
     8.68663562E-06    2          21        21   # BR(H -> g       g      )
     6.43504774E-08    2          22        22   # BR(H -> gam     gam    )
     3.35060064E-09    2          23        22   # BR(H -> Z       gam    )
     6.84624461E-07    2          24       -24   # BR(H -> W+      W-     )
     3.41890244E-07    2          23        23   # BR(H -> Z       Z      )
     5.50125026E-06    2          25        25   # BR(H -> h       h      )
    -2.95312002E-24    2          36        36   # BR(H -> A       A      )
     3.66726705E-19    2          23        36   # BR(H -> Z       A      )
     6.77480099E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.99811751E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.38476555E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.39506095E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23898126E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.20690768E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.61437244E+01   # A decays
#          BR         NDA      ID1       ID2
     9.20595613E-01    2           5        -5   # BR(A -> b       bb     )
     6.88007116E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.43262535E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.98776907E-04    2           3        -3   # BR(A -> s       sb     )
     8.43106082E-08    2           4        -4   # BR(A -> c       cb     )
     8.40589539E-03    2           6        -6   # BR(A -> t       tb     )
     2.47545576E-05    2          21        21   # BR(A -> g       g      )
     6.50295263E-08    2          22        22   # BR(A -> gam     gam    )
     2.43940763E-08    2          23        22   # BR(A -> Z       gam    )
     6.97378448E-07    2          23        25   # BR(A -> Z       h      )
     8.78234090E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.26969562E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.38699883E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.70483548E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.96912793E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46868047E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.26741723E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.21600586E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.39954348E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.30229001E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.67922931E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.24103731E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.36138653E-07    2          24        25   # BR(H+ -> W+      h      )
     5.11774693E-14    2          24        36   # BR(H+ -> W+      A      )
     4.69311860E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.03549855E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07715223E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

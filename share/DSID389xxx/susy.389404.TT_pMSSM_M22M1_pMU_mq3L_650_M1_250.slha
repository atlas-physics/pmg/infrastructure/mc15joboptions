#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.13972993E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04017688E+01   # W+
        25     1.24229403E+02   # h
        35     3.00017235E+03   # H
        36     2.99999969E+03   # A
        37     3.00093506E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03132772E+03   # ~d_L
   2000001     3.02622876E+03   # ~d_R
   1000002     3.03041321E+03   # ~u_L
   2000002     3.02778846E+03   # ~u_R
   1000003     3.03132772E+03   # ~s_L
   2000003     3.02622876E+03   # ~s_R
   1000004     3.03041321E+03   # ~c_L
   2000004     3.02778846E+03   # ~c_R
   1000005     7.39069320E+02   # ~b_1
   2000005     3.02553198E+03   # ~b_2
   1000006     7.37059719E+02   # ~t_1
   2000006     3.01211954E+03   # ~t_2
   1000011     3.00642385E+03   # ~e_L
   2000011     3.00157246E+03   # ~e_R
   1000012     3.00503138E+03   # ~nu_eL
   1000013     3.00642385E+03   # ~mu_L
   2000013     3.00157246E+03   # ~mu_R
   1000014     3.00503138E+03   # ~nu_muL
   1000015     2.98570445E+03   # ~tau_1
   2000015     3.02207142E+03   # ~tau_2
   1000016     3.00498474E+03   # ~nu_tauL
   1000021     2.33864074E+03   # ~g
   1000022     2.51704855E+02   # ~chi_10
   1000023     5.27485988E+02   # ~chi_20
   1000025    -2.99934412E+03   # ~chi_30
   1000035     3.00007960E+03   # ~chi_40
   1000024     5.27648706E+02   # ~chi_1+
   1000037     3.00066350E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886521E-01   # N_11
  1  2    -8.47715767E-04   # N_12
  1  3     1.49101097E-02   # N_13
  1  4    -1.97868222E-03   # N_14
  2  1     1.25967984E-03   # N_21
  2  2     9.99621328E-01   # N_22
  2  3    -2.68761625E-02   # N_23
  2  4     5.76929635E-03   # N_24
  3  1    -9.12856075E-03   # N_31
  3  2     1.49352043E-02   # N_32
  3  3     7.06861459E-01   # N_33
  3  4     7.07135409E-01   # N_34
  4  1    -1.19175401E-02   # N_41
  4  2     2.30959030E-02   # N_42
  4  3     7.06683973E-01   # N_43
  4  4    -7.07051846E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99276866E-01   # U_11
  1  2    -3.80229459E-02   # U_12
  2  1     3.80229459E-02   # U_21
  2  2     9.99276866E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966679E-01   # V_11
  1  2    -8.16334107E-03   # V_12
  2  1     8.16334107E-03   # V_21
  2  2     9.99966679E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98856783E-01   # cos(theta_t)
  1  2    -4.78030026E-02   # sin(theta_t)
  2  1     4.78030026E-02   # -sin(theta_t)
  2  2     9.98856783E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99916401E-01   # cos(theta_b)
  1  2     1.29302363E-02   # sin(theta_b)
  2  1    -1.29302363E-02   # -sin(theta_b)
  2  2     9.99916401E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06938915E-01   # cos(theta_tau)
  1  2     7.07274608E-01   # sin(theta_tau)
  2  1    -7.07274608E-01   # -sin(theta_tau)
  2  2     7.06938915E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01874449E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.39729925E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44408886E+02   # higgs               
         4     7.13244080E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.39729925E+03  # The gauge couplings
     1     3.61937021E-01   # gprime(Q) DRbar
     2     6.37701538E-01   # g(Q) DRbar
     3     1.02794472E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.39729925E+03  # The trilinear couplings
  1  1     1.83749334E-06   # A_u(Q) DRbar
  2  2     1.83752087E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.39729925E+03  # The trilinear couplings
  1  1     4.83154575E-07   # A_d(Q) DRbar
  2  2     4.83243461E-07   # A_s(Q) DRbar
  3  3     1.07427697E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.39729925E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.01455286E-07   # A_mu(Q) DRbar
  3  3     1.02657941E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.39729925E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53485480E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.39729925E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.14172999E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.39729925E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06543251E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.39729925E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.59923559E+04   # M^2_Hd              
        22    -9.08272787E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41152830E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.63696171E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48242729E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48242729E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51757271E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51757271E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.92875239E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     3.75900444E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.95001377E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.67408579E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.03186470E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.30991304E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.35470621E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.74000535E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.46408527E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.27016561E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.59948040E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50722125E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.98180032E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.61925999E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.43506839E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.65032670E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.80616646E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.23486598E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.88409432E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     9.47534393E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.01853753E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.79370277E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -1.39521030E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27896911E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.95076739E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.25427512E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.11916607E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88535284E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.93184751E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59740784E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.43474177E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.40960027E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.19537995E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.00741004E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.14789315E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18214941E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57521854E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.38814452E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.44674454E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.82212512E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42477893E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.89053926E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.03651228E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59523733E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.41321443E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.33903128E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.18967748E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.31568999E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.15471698E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.67542811E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.47853928E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.78945472E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.38910267E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.24317077E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55214536E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88535284E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.93184751E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59740784E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.43474177E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.40960027E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.19537995E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.00741004E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.14789315E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18214941E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57521854E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.38814452E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.44674454E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.82212512E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42477893E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.89053926E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.03651228E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59523733E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.41321443E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.33903128E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.18967748E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.31568999E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.15471698E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.67542811E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.47853928E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.78945472E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.38910267E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.24317077E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55214536E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80968598E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01069572E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00074799E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.72117081E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.51347093E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98855613E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.35282753E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54222409E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998488E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.51181965E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.86223673E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.42490271E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.80968598E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01069572E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00074799E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.72117081E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.51347093E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98855613E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.35282753E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54222409E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998488E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.51181965E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.86223673E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.42490271E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70349093E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56244036E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14905036E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28850928E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64630628E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.64697047E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.12092294E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.06260843E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.19961197E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23175997E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.20390762E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80990631E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01620979E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99044054E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.84633232E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     3.08471213E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99334962E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.58845539E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80990631E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01620979E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99044054E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.84633232E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     3.08471213E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99334962E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.58845539E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.81017478E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01612197E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99017751E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.81610212E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.02675764E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99369532E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.13919922E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.71649992E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.62849685E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.75287751E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.22026621E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.67964400E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.91753953E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.15319915E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.74462761E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.22903733E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.43343495E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.99420744E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.50057926E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.64861990E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.39938289E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.85773111E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.71807071E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.71807071E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.46547617E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.06451609E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.64200870E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.64200870E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27770446E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27770446E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     4.90043032E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     4.90043032E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.56712004E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.28993212E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.05393572E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.78520510E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.78520510E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.46262828E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.18857791E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.61436943E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.61436943E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30396416E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30396416E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.88332444E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     6.88332444E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.76153644E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85564314E-01    2           5        -5   # BR(h -> b       bb     )
     5.44952621E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.92917746E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.09927106E-04    2           3        -3   # BR(h -> s       sb     )
     1.76912077E-02    2           4        -4   # BR(h -> c       cb     )
     5.70306983E-02    2          21        21   # BR(h -> g       g      )
     1.92419097E-03    2          22        22   # BR(h -> gam     gam    )
     1.22414367E-03    2          22        23   # BR(h -> Z       gam    )
     1.61521256E-01    2          24       -24   # BR(h -> W+      W-     )
     1.99460832E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39475079E+01   # H decays
#          BR         NDA      ID1       ID2
     7.44016328E-01    2           5        -5   # BR(H -> b       bb     )
     1.78296819E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.30414946E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.74148302E-04    2           3        -3   # BR(H -> s       sb     )
     2.18496395E-07    2           4        -4   # BR(H -> c       cb     )
     2.17964291E-02    2           6        -6   # BR(H -> t       tb     )
     3.09197818E-05    2          21        21   # BR(H -> g       g      )
     4.12897045E-08    2          22        22   # BR(H -> gam     gam    )
     8.37704162E-09    2          23        22   # BR(H -> Z       gam    )
     3.31907422E-05    2          24       -24   # BR(H -> W+      W-     )
     1.65749116E-05    2          23        23   # BR(H -> Z       Z      )
     8.55968053E-05    2          25        25   # BR(H -> h       h      )
    -3.83667771E-23    2          36        36   # BR(H -> A       A      )
     1.16768662E-17    2          23        36   # BR(H -> Z       A      )
     2.11274631E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09861941E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.05355233E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.88485221E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.03485450E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.11871881E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.32543219E+01   # A decays
#          BR         NDA      ID1       ID2
     7.82992752E-01    2           5        -5   # BR(A -> b       bb     )
     1.87615313E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.63361986E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.14746278E-04    2           3        -3   # BR(A -> s       sb     )
     2.29909850E-07    2           4        -4   # BR(A -> c       cb     )
     2.29223605E-02    2           6        -6   # BR(A -> t       tb     )
     6.75041649E-05    2          21        21   # BR(A -> g       g      )
     3.79345331E-08    2          22        22   # BR(A -> gam     gam    )
     6.65051921E-08    2          23        22   # BR(A -> Z       gam    )
     3.47941267E-05    2          23        25   # BR(A -> Z       h      )
     2.64854129E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22159462E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32064998E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.97482587E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.00604483E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.09556864E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.47254299E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.74230892E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.01162826E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.13766601E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.05698311E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.13385553E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.59117430E-05    2          24        25   # BR(H+ -> W+      h      )
     9.36449284E-14    2          24        36   # BR(H+ -> W+      A      )
     2.00100546E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.12050271E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.42280570E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419727E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     4.49900000E+02   # M_1(MX)             
         2     8.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04044487E+01   # W+
        25     1.24827683E+02   # h
        35     3.00004229E+03   # H
        36     2.99999993E+03   # A
        37     3.00108475E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04036747E+03   # ~d_L
   2000001     3.03520500E+03   # ~d_R
   1000002     3.03946372E+03   # ~u_L
   2000002     3.03667135E+03   # ~u_R
   1000003     3.04036747E+03   # ~s_L
   2000003     3.03520500E+03   # ~s_R
   1000004     3.03946372E+03   # ~c_L
   2000004     3.03667135E+03   # ~c_R
   1000005     1.00499772E+03   # ~b_1
   2000005     3.03307164E+03   # ~b_2
   1000006     1.00268992E+03   # ~t_1
   2000006     3.02433306E+03   # ~t_2
   1000011     3.00647474E+03   # ~e_L
   2000011     3.00167236E+03   # ~e_R
   1000012     3.00509143E+03   # ~nu_eL
   1000013     3.00647474E+03   # ~mu_L
   2000013     3.00167236E+03   # ~mu_R
   1000014     3.00509143E+03   # ~nu_muL
   1000015     2.98620064E+03   # ~tau_1
   2000015     3.02156238E+03   # ~tau_2
   1000016     3.00498837E+03   # ~nu_tauL
   1000021     2.35014205E+03   # ~g
   1000022     4.52757423E+02   # ~chi_10
   1000023     9.38166404E+02   # ~chi_20
   1000025    -2.99586677E+03   # ~chi_30
   1000035     2.99642533E+03   # ~chi_40
   1000024     9.38326544E+02   # ~chi_1+
   1000037     2.99713341E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887189E-01   # N_11
  1  2    -1.30548681E-04   # N_12
  1  3    -1.49441205E-02   # N_13
  1  4    -1.50503161E-03   # N_14
  2  1     5.58887868E-04   # N_21
  2  2     9.99584082E-01   # N_22
  2  3     2.79482720E-02   # N_23
  2  4     7.08828481E-03   # N_24
  3  1    -9.49851743E-03   # N_31
  3  2     1.47550772E-02   # N_32
  3  3    -7.06874156E-01   # N_33
  3  4     7.07121625E-01   # N_34
  4  1     1.16221646E-02   # N_41
  4  2    -2.47776653E-02   # N_42
  4  3     7.06628965E-01   # N_43
  4  4     7.07054806E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99218680E-01   # U_11
  1  2     3.95225221E-02   # U_12
  2  1    -3.95225221E-02   # U_21
  2  2     9.99218680E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99949774E-01   # V_11
  1  2    -1.00224095E-02   # V_12
  2  1    -1.00224095E-02   # V_21
  2  2    -9.99949774E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98481979E-01   # cos(theta_t)
  1  2    -5.50793756E-02   # sin(theta_t)
  2  1     5.50793756E-02   # -sin(theta_t)
  2  2     9.98481979E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99722573E-01   # cos(theta_b)
  1  2    -2.35537053E-02   # sin(theta_b)
  2  1     2.35537053E-02   # -sin(theta_b)
  2  2     9.99722573E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06968504E-01   # cos(theta_tau)
  1  2     7.07245031E-01   # sin(theta_tau)
  2  1    -7.07245031E-01   # -sin(theta_tau)
  2  2    -7.06968504E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00129636E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64197273E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43923712E+02   # higgs               
         4     1.03868426E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64197273E+03  # The gauge couplings
     1     3.62451937E-01   # gprime(Q) DRbar
     2     6.36452791E-01   # g(Q) DRbar
     3     1.02427732E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64197273E+03  # The trilinear couplings
  1  1     2.58287572E-06   # A_u(Q) DRbar
  2  2     2.58291205E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64197273E+03  # The trilinear couplings
  1  1     9.35148906E-07   # A_d(Q) DRbar
  2  2     9.35251814E-07   # A_s(Q) DRbar
  3  3     1.84818259E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64197273E+03  # The trilinear couplings
  1  1     3.74960055E-07   # A_e(Q) DRbar
  2  2     3.74978027E-07   # A_mu(Q) DRbar
  3  3     3.79970253E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64197273E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50567440E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64197273E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.73468810E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64197273E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.00619364E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64197273E+03  # The soft SUSY breaking masses at the scale Q
         1     4.49900000E+02   # M_1(Q)              
         2     8.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.17317872E+04   # M^2_Hd              
        22    -9.03267282E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40588824E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.38565177E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47897761E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47897761E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52102239E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52102239E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.17753810E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.82957925E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     6.17042075E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12109433E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.67445772E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.68465728E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.41615405E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.33980629E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.01794197E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67760958E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59902945E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12553260E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.68931719E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.72066644E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.27933356E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     5.24845414E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.05598552E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     7.02160344E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.10583341E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.88970913E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -3.95654770E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.61423466E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.85863172E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.76932624E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.41637113E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.43824712E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.24390613E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.48947168E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.82829957E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.37728358E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.98027948E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.19871402E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.46780834E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.14128410E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.55367850E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.16728914E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.20011034E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.74928745E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.44632079E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.44366838E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.25791576E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.48822457E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.80486484E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.24514776E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     2.97469420E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     5.17653777E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.47449516E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64685599E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.40842535E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.18224478E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.17478594E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.62290792E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55915727E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.43824712E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.24390613E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.48947168E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.82829957E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.37728358E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.98027948E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.19871402E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.46780834E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.14128410E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.55367850E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.16728914E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.20011034E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.74928745E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.44632079E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.44366838E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.25791576E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.48822457E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.80486484E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.24514776E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     2.97469420E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.17653777E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.47449516E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64685599E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.40842535E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.18224478E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.17478594E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.62290792E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55915727E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.33250325E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.12529309E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.96107927E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.57553215E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.33919631E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.91362713E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.37170335E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.49808578E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999731E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.66349673E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.41118842E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.72608691E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.33250325E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.12529309E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.96107927E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.57553215E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.33919631E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.91362713E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.37170335E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.49808578E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999731E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.66349673E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.41118842E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.72608691E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.43613061E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.83982442E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.05554507E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.10463051E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.39180241E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.92402451E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.02738570E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.64629525E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.41723394E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.04811759E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.65846483E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.33233802E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.12581499E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.95550888E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.55164186E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.18805897E-08    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.91867594E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.04229877E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.33233802E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.12581499E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.95550888E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.55164186E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.18805897E-08    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.91867594E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.04229877E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.33250955E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.12571485E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.95521162E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.42837905E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.15998925E-08    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.91905448E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.88849255E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.31620098E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.51367180E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.30209266E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.61468593E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.85499871E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.17402062E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.06884142E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.00524360E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.58407990E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.91815660E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.99171277E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     7.00828723E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.52688571E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.27531972E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.54452578E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.99043776E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.99043776E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.85403556E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.64443119E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.34534861E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.34534861E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.38120423E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.38120423E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.18191678E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.18191678E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.44118239E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.61169269E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.63332139E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.05523011E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.05523011E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.34734969E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.97859454E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.30874664E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.30874664E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.44706190E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.44706190E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.41903767E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.41903767E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.57948833E-03   # h decays
#          BR         NDA      ID1       ID2
     5.64446866E-01    2           5        -5   # BR(h -> b       bb     )
     7.23359286E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.56072187E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.43662725E-04    2           3        -3   # BR(h -> s       sb     )
     2.36249082E-02    2           4        -4   # BR(h -> c       cb     )
     7.65035701E-02    2          21        21   # BR(h -> g       g      )
     2.61363900E-03    2          22        22   # BR(h -> gam     gam    )
     1.71919132E-03    2          22        23   # BR(h -> Z       gam    )
     2.29341007E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86151548E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.48323354E+01   # H decays
#          BR         NDA      ID1       ID2
     8.95760491E-01    2           5        -5   # BR(H -> b       bb     )
     7.13914807E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.52423216E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.09977058E-04    2           3        -3   # BR(H -> s       sb     )
     8.68799362E-08    2           4        -4   # BR(H -> c       cb     )
     8.66682747E-03    2           6        -6   # BR(H -> t       tb     )
     1.23993563E-05    2          21        21   # BR(H -> g       g      )
     8.20357494E-08    2          22        22   # BR(H -> gam     gam    )
     3.55411029E-09    2          23        22   # BR(H -> Z       gam    )
     7.54207559E-07    2          24       -24   # BR(H -> W+      W-     )
     3.76638938E-07    2          23        23   # BR(H -> Z       Z      )
     6.00716300E-06    2          25        25   # BR(H -> h       h      )
    -1.78098526E-24    2          36        36   # BR(H -> A       A      )
     8.95894557E-20    2          23        36   # BR(H -> Z       A      )
     5.54577600E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.07960255E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     2.77049734E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.27482454E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24941576E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.02762777E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.40698494E+01   # A decays
#          BR         NDA      ID1       ID2
     9.15840456E-01    2           5        -5   # BR(A -> b       bb     )
     7.29886982E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.58070234E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.16963836E-04    2           3        -3   # BR(A -> s       sb     )
     8.94427019E-08    2           4        -4   # BR(A -> c       cb     )
     8.91757290E-03    2           6        -6   # BR(A -> t       tb     )
     2.62613989E-05    2          21        21   # BR(A -> g       g      )
     6.43436617E-08    2          22        22   # BR(A -> gam     gam    )
     2.58847142E-08    2          23        22   # BR(A -> Z       gam    )
     7.68224834E-07    2          23        25   # BR(A -> Z       h      )
     8.85406292E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.49737511E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.42204999E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.78444270E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.75444908E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46221439E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.62577416E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.34271213E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.35816054E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.37675239E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.83242238E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.20498092E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.98321098E-07    2          24        25   # BR(H+ -> W+      h      )
     5.26516972E-14    2          24        36   # BR(H+ -> W+      A      )
     4.63659983E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.22238041E-10    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07769546E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

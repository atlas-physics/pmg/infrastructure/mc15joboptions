#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.15008841E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.49900000E+02   # M_1(MX)             
         2     2.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     7.49900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03978328E+01   # W+
        25     1.24087788E+02   # h
        35     3.00013369E+03   # H
        36     2.99999981E+03   # A
        37     3.00091712E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03546776E+03   # ~d_L
   2000001     3.03028987E+03   # ~d_R
   1000002     3.03455109E+03   # ~u_L
   2000002     3.03163091E+03   # ~u_R
   1000003     3.03546776E+03   # ~s_L
   2000003     3.03028987E+03   # ~s_R
   1000004     3.03455109E+03   # ~c_L
   2000004     3.03163091E+03   # ~c_R
   1000005     8.43234520E+02   # ~b_1
   2000005     3.02948250E+03   # ~b_2
   1000006     8.41540926E+02   # ~t_1
   2000006     3.01634534E+03   # ~t_2
   1000011     3.00644188E+03   # ~e_L
   2000011     3.00181254E+03   # ~e_R
   1000012     3.00504453E+03   # ~nu_eL
   1000013     3.00644188E+03   # ~mu_L
   2000013     3.00181254E+03   # ~mu_R
   1000014     3.00504453E+03   # ~nu_muL
   1000015     2.98578512E+03   # ~tau_1
   2000015     3.02199603E+03   # ~tau_2
   1000016     3.00491359E+03   # ~nu_tauL
   1000021     2.34368456E+03   # ~g
   1000022     1.50672806E+02   # ~chi_10
   1000023     3.20491415E+02   # ~chi_20
   1000025    -2.99762236E+03   # ~chi_30
   1000035     2.99818715E+03   # ~chi_40
   1000024     3.20653370E+02   # ~chi_1+
   1000037     2.99883703E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99888306E-01   # N_11
  1  2    -1.15244623E-03   # N_12
  1  3     1.48277453E-02   # N_13
  1  4    -1.47810522E-03   # N_14
  2  1     1.54855927E-03   # N_21
  2  2     9.99644160E-01   # N_22
  2  3    -2.63388661E-02   # N_23
  2  4     3.92676213E-03   # N_24
  3  1    -9.41843093E-03   # N_31
  3  2     1.58609081E-02   # N_32
  3  3     7.06841908E-01   # N_33
  3  4     7.07130994E-01   # N_34
  4  1    -1.15008731E-02   # N_41
  4  2     2.14162748E-02   # N_42
  4  3     7.06725491E-01   # N_43
  4  4    -7.07070119E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99305269E-01   # U_11
  1  2    -3.72690040E-02   # U_12
  2  1     3.72690040E-02   # U_21
  2  2     9.99305269E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99984557E-01   # V_11
  1  2    -5.55753322E-03   # V_12
  2  1     5.55753322E-03   # V_21
  2  2     9.99984557E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98824817E-01   # cos(theta_t)
  1  2    -4.84663279E-02   # sin(theta_t)
  2  1     4.84663279E-02   # -sin(theta_t)
  2  2     9.98824817E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99914428E-01   # cos(theta_b)
  1  2     1.30819218E-02   # sin(theta_b)
  2  1    -1.30819218E-02   # -sin(theta_b)
  2  2     9.99914428E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06925091E-01   # cos(theta_tau)
  1  2     7.07288425E-01   # sin(theta_tau)
  2  1    -7.07288425E-01   # -sin(theta_tau)
  2  2     7.06925091E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01826424E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.50088407E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44257979E+02   # higgs               
         4     7.47580961E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.50088407E+03  # The gauge couplings
     1     3.62175404E-01   # gprime(Q) DRbar
     2     6.38947981E-01   # g(Q) DRbar
     3     1.02634739E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.50088407E+03  # The trilinear couplings
  1  1     2.12505103E-06   # A_u(Q) DRbar
  2  2     2.12508325E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.50088407E+03  # The trilinear couplings
  1  1     5.41394379E-07   # A_d(Q) DRbar
  2  2     5.41499018E-07   # A_s(Q) DRbar
  3  3     1.23347582E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.50088407E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.18016350E-07   # A_mu(Q) DRbar
  3  3     1.19444561E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.50088407E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.51818242E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.50088407E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.13280701E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.50088407E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.05689002E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.50088407E+03  # The soft SUSY breaking masses at the scale Q
         1     1.49900000E+02   # M_1(Q)              
         2     2.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.34542999E+04   # M^2_Hd              
        22    -9.05892731E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     7.49899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41722534E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.18756238E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48222746E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48222746E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51777254E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51777254E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     7.55825383E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.42644124E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.05061265E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.80674323E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.01870663E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.46161887E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.14016384E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.30566698E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     6.03898426E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.30548686E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.57332199E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.50041931E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.96456023E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     7.34139026E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.63606403E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.52228002E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.31411357E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.21611057E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.94405276E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.77757281E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.50086271E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.46929040E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     5.12887150E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29311074E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.89411396E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.19451106E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.03322174E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.02307713E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.86032137E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.63524286E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.24190451E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.06333104E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.27072771E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.54490146E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.03542552E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.17887452E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59364035E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.76094811E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.21145493E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.18519204E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40635569E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.02814672E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.00262580E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.63267958E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.68633231E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.40207103E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.26498603E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.08872997E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.04230403E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66854244E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53624719E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.07052607E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     2.16345424E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.11837017E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54637416E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.02307713E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.86032137E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.63524286E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.24190451E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.06333104E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.27072771E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.54490146E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.03542552E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.17887452E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59364035E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.76094811E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.21145493E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.18519204E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40635569E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.02814672E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.00262580E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.63267958E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.68633231E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.40207103E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.26498603E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.08872997E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.04230403E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66854244E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53624719E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.07052607E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     2.16345424E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.11837017E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54637416E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.96647020E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.79799765E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.01165992E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.16967825E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.05426948E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00854006E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     2.18293979E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.55845585E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99997642E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.35605210E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.94058382E-10    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.74865605E-10    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.96647020E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.79799765E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.01165992E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.16967825E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.05426948E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00854006E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     2.18293979E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.55845585E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99997642E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.35605210E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.94058382E-10    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.74865605E-10    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.79110023E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.48614022E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17492748E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.33893230E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.73171038E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.56873615E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.14746922E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.18496731E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.33619021E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.28340762E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.34893659E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.96677390E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.87262112E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99941185E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.36546556E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     4.98924740E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01332595E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     3.23692614E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.96677390E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.87262112E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99941185E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.36546556E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     4.98924740E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01332595E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     3.23692614E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.96693202E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.87179309E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99915559E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.24792106E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.80075638E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.01365498E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.00415589E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.63674294E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.38467393E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.68813163E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.16854397E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.43503300E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.10680673E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.20512781E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.93621217E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.37823766E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     1.90554352E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.89878593E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.51012141E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.41015405E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.36209560E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.56676876E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.90276389E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.90276389E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.41786413E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.55211088E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.61354511E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.61354511E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.25036762E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.25036762E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.65781696E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.65781696E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.32282788E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.23125390E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.53351007E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.97753615E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.97753615E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.42257861E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.84355015E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.58841509E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.58841509E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.27687741E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.27687741E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.34527144E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.34527144E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.71898177E-03   # h decays
#          BR         NDA      ID1       ID2
     6.85877666E-01    2           5        -5   # BR(h -> b       bb     )
     5.49133495E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.94398353E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.13166134E-04    2           3        -3   # BR(h -> s       sb     )
     1.78343785E-02    2           4        -4   # BR(h -> c       cb     )
     5.72890955E-02    2          21        21   # BR(h -> g       g      )
     1.93347870E-03    2          22        22   # BR(h -> gam     gam    )
     1.21912038E-03    2          22        23   # BR(h -> Z       gam    )
     1.60558545E-01    2          24       -24   # BR(h -> W+      W-     )
     1.97668020E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39977140E+01   # H decays
#          BR         NDA      ID1       ID2
     7.45688099E-01    2           5        -5   # BR(H -> b       bb     )
     1.77655112E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.28146023E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.71363391E-04    2           3        -3   # BR(H -> s       sb     )
     2.17668641E-07    2           4        -4   # BR(H -> c       cb     )
     2.17138491E-02    2           6        -6   # BR(H -> t       tb     )
     2.83756934E-05    2          21        21   # BR(H -> g       g      )
     1.48362973E-08    2          22        22   # BR(H -> gam     gam    )
     8.35405665E-09    2          23        22   # BR(H -> Z       gam    )
     3.16981832E-05    2          24       -24   # BR(H -> W+      W-     )
     1.58295450E-05    2          23        23   # BR(H -> Z       Z      )
     8.35641436E-05    2          25        25   # BR(H -> h       h      )
     2.61050228E-23    2          36        36   # BR(H -> A       A      )
     3.16338819E-18    2          23        36   # BR(H -> Z       A      )
     2.31374718E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11830821E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.15299355E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.21190783E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.90780727E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.88725079E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33149415E+01   # A decays
#          BR         NDA      ID1       ID2
     7.83998605E-01    2           5        -5   # BR(A -> b       bb     )
     1.86761156E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.60341894E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.11036972E-04    2           3        -3   # BR(A -> s       sb     )
     2.28863137E-07    2           4        -4   # BR(A -> c       cb     )
     2.28180016E-02    2           6        -6   # BR(A -> t       tb     )
     6.71968347E-05    2          21        21   # BR(A -> g       g      )
     3.12798564E-08    2          22        22   # BR(A -> gam     gam    )
     6.61575222E-08    2          23        22   # BR(A -> Z       gam    )
     3.31989174E-05    2          23        25   # BR(A -> Z       h      )
     2.62608176E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21157855E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30859166E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.94305159E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.01801461E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.10259113E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.44345632E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.63946554E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.05657221E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.07723112E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.04454971E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.17419381E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.34889617E-05    2          24        25   # BR(H+ -> W+      h      )
     8.39498372E-14    2          24        36   # BR(H+ -> W+      A      )
     2.02474767E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.83715233E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.30978296E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

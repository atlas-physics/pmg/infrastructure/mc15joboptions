#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16440980E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04004748E+01   # W+
        25     1.23714906E+02   # h
        35     3.00008936E+03   # H
        36     2.99999995E+03   # A
        37     3.00088744E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04052698E+03   # ~d_L
   2000001     3.03542633E+03   # ~d_R
   1000002     3.03961595E+03   # ~u_L
   2000002     3.03645656E+03   # ~u_R
   1000003     3.04052698E+03   # ~s_L
   2000003     3.03542633E+03   # ~s_R
   1000004     3.03961595E+03   # ~c_L
   2000004     3.03645656E+03   # ~c_R
   1000005     1.00034345E+03   # ~b_1
   2000005     3.03440464E+03   # ~b_2
   1000006     9.98921467E+02   # ~t_1
   2000006     3.02042850E+03   # ~t_2
   1000011     3.00626889E+03   # ~e_L
   2000011     3.00212764E+03   # ~e_R
   1000012     3.00487635E+03   # ~nu_eL
   1000013     3.00626889E+03   # ~mu_L
   2000013     3.00212764E+03   # ~mu_R
   1000014     3.00487635E+03   # ~nu_muL
   1000015     2.98560316E+03   # ~tau_1
   2000015     3.02194542E+03   # ~tau_2
   1000016     3.00462210E+03   # ~nu_tauL
   1000021     2.35026310E+03   # ~g
   1000022     2.50989397E+02   # ~chi_10
   1000023     5.28551508E+02   # ~chi_20
   1000025    -2.99545297E+03   # ~chi_30
   1000035     2.99620756E+03   # ~chi_40
   1000024     5.28714778E+02   # ~chi_1+
   1000037     2.99678634E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99886495E-01   # N_11
  1  2    -8.46907911E-04   # N_12
  1  3     1.49118488E-02   # N_13
  1  4    -1.97891286E-03   # N_14
  2  1     1.25848053E-03   # N_21
  2  2     9.99622137E-01   # N_22
  2  3    -2.68474444E-02   # N_23
  2  4     5.76314205E-03   # N_24
  3  1    -9.12965849E-03   # N_31
  3  2     1.49192390E-02   # N_32
  3  3     7.06861829E-01   # N_33
  3  4     7.07135362E-01   # N_34
  4  1    -1.19189826E-02   # N_41
  4  2     2.30712354E-02   # N_42
  4  3     7.06684658E-01   # N_43
  4  4    -7.07051943E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99278410E-01   # U_11
  1  2    -3.79823510E-02   # U_12
  2  1     3.79823510E-02   # U_21
  2  2     9.99278410E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99966750E-01   # V_11
  1  2    -8.15463757E-03   # V_12
  2  1     8.15463757E-03   # V_21
  2  2     9.99966750E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98761791E-01   # cos(theta_t)
  1  2    -4.97482144E-02   # sin(theta_t)
  2  1     4.97482144E-02   # -sin(theta_t)
  2  2     9.98761791E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99907323E-01   # cos(theta_b)
  1  2     1.36141621E-02   # sin(theta_b)
  2  1    -1.36141621E-02   # -sin(theta_b)
  2  2     9.99907323E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06950620E-01   # cos(theta_tau)
  1  2     7.07262908E-01   # sin(theta_tau)
  2  1    -7.07262908E-01   # -sin(theta_tau)
  2  2     7.06950620E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01782371E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64409798E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44070406E+02   # higgs               
         4     7.75713612E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64409798E+03  # The gauge couplings
     1     3.62482401E-01   # gprime(Q) DRbar
     2     6.37903053E-01   # g(Q) DRbar
     3     1.02428472E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64409798E+03  # The trilinear couplings
  1  1     2.55822983E-06   # A_u(Q) DRbar
  2  2     2.55826737E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64409798E+03  # The trilinear couplings
  1  1     6.80117046E-07   # A_d(Q) DRbar
  2  2     6.80239415E-07   # A_s(Q) DRbar
  3  3     1.50086765E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64409798E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     1.43949893E-07   # A_mu(Q) DRbar
  3  3     1.45682143E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64409798E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.49520344E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64409798E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.15617200E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64409798E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06657551E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64409798E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.38054523E+04   # M^2_Hd              
        22    -9.02792984E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41247746E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.40463007E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48177047E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48177047E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51822953E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51822953E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     6.32461267E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     1.92041495E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.02791451E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.78004400E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     9.88901407E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.62422464E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.68287573E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.39985631E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.13640800E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.38303215E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.54396325E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.48246195E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     2.91590105E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     6.14240208E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.15384080E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.51237500E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.27224092E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.19458131E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.94627841E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.84328878E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.30129130E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.24074123E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.48846378E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.29625505E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.88415905E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.19040503E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.01203167E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.87556014E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.97910842E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.60621388E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.82896724E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.81257982E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.21299010E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     2.03770709E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.12100377E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15852827E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.59419527E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.41217539E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.11485693E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.83284696E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.40580202E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88069809E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.08439550E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60403655E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.68436799E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.04931101E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.20727668E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.60032641E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.12783669E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65298220E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.53542790E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.86233005E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.02088334E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.96170350E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54645644E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.87556014E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.97910842E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.60621388E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.82896724E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.81257982E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.21299010E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     2.03770709E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.12100377E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15852827E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.59419527E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.41217539E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.11485693E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.83284696E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.40580202E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88069809E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.08439550E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60403655E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.68436799E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.04931101E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.20727668E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.60032641E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.12783669E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65298220E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.53542790E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.86233005E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.02088334E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.96170350E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54645644E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.81195443E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01317460E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99991860E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.55957888E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     3.78651122E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98690639E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.65398689E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54729385E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998488E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.50846002E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.66799438E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     2.23701409E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.81195443E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01317460E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99991860E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.55957888E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     3.78651122E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98690639E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.65398689E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54729385E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998488E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.50846002E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.66799438E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     2.23701409E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70702318E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56860575E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14699057E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28440368E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64984905E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65330497E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11876368E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.46027428E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.62422072E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.22745690E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.65998214E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.81216706E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01869128E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.98961720E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.05847287E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.43301812E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99169136E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.22698252E-09    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.81216706E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01869128E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.98961720E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.05847287E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.43301812E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99169136E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.22698252E-09    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.81214543E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01860845E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.98934889E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     4.79002941E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.88935746E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99202464E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.78830655E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.84828924E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     7.98639953E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.75250592E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.04454080E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     3.44309941E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     7.43316416E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.31957896E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     7.24339382E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     7.80591471E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.47727698E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.02696154E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.49730385E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.00406457E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.50847428E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.22032072E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.21558637E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.21558637E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.16599763E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.22574228E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.55744218E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.55744218E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.27439738E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.27439738E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.59109385E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     2.59109385E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     7.92458619E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.94096041E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.20845068E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.29157676E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     7.29157676E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.58358919E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.60996607E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.52586671E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.52586671E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.30166315E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.30166315E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.19611070E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.19611070E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.62832580E-03   # h decays
#          BR         NDA      ID1       ID2
     6.88205575E-01    2           5        -5   # BR(h -> b       bb     )
     5.58104958E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.97575798E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.20166907E-04    2           3        -3   # BR(h -> s       sb     )
     1.81397431E-02    2           4        -4   # BR(h -> c       cb     )
     5.78332826E-02    2          21        21   # BR(h -> g       g      )
     1.94700073E-03    2          22        22   # BR(h -> gam     gam    )
     1.20118103E-03    2          22        23   # BR(h -> Z       gam    )
     1.57028274E-01    2          24       -24   # BR(h -> W+      W-     )
     1.92167052E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.40176624E+01   # H decays
#          BR         NDA      ID1       ID2
     7.48791822E-01    2           5        -5   # BR(H -> b       bb     )
     1.77399750E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.27243125E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.70256144E-04    2           3        -3   # BR(H -> s       sb     )
     2.17317964E-07    2           4        -4   # BR(H -> c       cb     )
     2.16788597E-02    2           6        -6   # BR(H -> t       tb     )
     2.36176347E-05    2          21        21   # BR(H -> g       g      )
     1.03806972E-08    2          22        22   # BR(H -> gam     gam    )
     8.36071128E-09    2          23        22   # BR(H -> Z       gam    )
     3.04202866E-05    2          24       -24   # BR(H -> W+      W-     )
     1.51913885E-05    2          23        23   # BR(H -> Z       Z      )
     8.08498384E-05    2          25        25   # BR(H -> h       h      )
    -1.57773502E-23    2          36        36   # BR(H -> A       A      )
     5.89998084E-19    2          23        36   # BR(H -> Z       A      )
     2.09574986E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.09373608E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.04507858E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     6.84304427E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     4.66416551E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.59145144E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.33735028E+01   # A decays
#          BR         NDA      ID1       ID2
     7.84939810E-01    2           5        -5   # BR(A -> b       bb     )
     1.85943354E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.57450348E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.07485548E-04    2           3        -3   # BR(A -> s       sb     )
     2.27860976E-07    2           4        -4   # BR(A -> c       cb     )
     2.27180846E-02    2           6        -6   # BR(A -> t       tb     )
     6.69025848E-05    2          21        21   # BR(A -> g       g      )
     3.76789010E-08    2          22        22   # BR(A -> gam     gam    )
     6.58978218E-08    2          23        22   # BR(A -> Z       gam    )
     3.17686198E-05    2          23        25   # BR(A -> Z       h      )
     2.61850457E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.21122244E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.30567414E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     7.89511600E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     1.03530554E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.11348680E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.40262368E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.49509131E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     7.12630447E-06    2           2        -5   # BR(H+ -> u       bb     )
     4.99239183E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.02709554E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.23729547E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.10990476E-05    2          24        25   # BR(H+ -> W+      h      )
     6.99739409E-14    2          24        36   # BR(H+ -> W+      A      )
     1.94234723E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.02213611E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.09774938E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

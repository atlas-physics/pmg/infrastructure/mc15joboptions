#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.14468788E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     2.49900000E+02   # M_1(MX)             
         2     4.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     6.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04008484E+01   # W+
        25     1.25424005E+02   # h
        35     3.00011122E+03   # H
        36     2.99999992E+03   # A
        37     3.00109378E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.03319300E+03   # ~d_L
   2000001     3.02797953E+03   # ~d_R
   1000002     3.03228230E+03   # ~u_L
   2000002     3.03012631E+03   # ~u_R
   1000003     3.03319300E+03   # ~s_L
   2000003     3.02797953E+03   # ~s_R
   1000004     3.03228230E+03   # ~c_L
   2000004     3.03012631E+03   # ~c_R
   1000005     7.93751470E+02   # ~b_1
   2000005     3.02711409E+03   # ~b_2
   1000006     7.91091593E+02   # ~t_1
   2000006     3.02243622E+03   # ~t_2
   1000011     3.00674051E+03   # ~e_L
   2000011     3.00098378E+03   # ~e_R
   1000012     3.00535179E+03   # ~nu_eL
   1000013     3.00674051E+03   # ~mu_L
   2000013     3.00098378E+03   # ~mu_R
   1000014     3.00535179E+03   # ~nu_muL
   1000015     2.98617574E+03   # ~tau_1
   2000015     3.02188300E+03   # ~tau_2
   1000016     3.00548628E+03   # ~nu_tauL
   1000021     2.34107873E+03   # ~g
   1000022     2.52198197E+02   # ~chi_10
   1000023     5.29875607E+02   # ~chi_20
   1000025    -3.00044690E+03   # ~chi_30
   1000035     3.00061785E+03   # ~chi_40
   1000024     5.30038450E+02   # ~chi_1+
   1000037     3.00147215E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891088E-01   # N_11
  1  2     7.59397947E-05   # N_12
  1  3    -1.47500155E-02   # N_13
  1  4    -4.93283133E-04   # N_14
  2  1     3.15275325E-04   # N_21
  2  2     9.99646189E-01   # N_22
  2  3     2.64150241E-02   # N_23
  2  4     3.10551724E-03   # N_24
  3  1    -1.00794169E-02   # N_31
  3  2     1.64842097E-02   # N_32
  3  3    -7.06838132E-01   # N_33
  3  4     7.07111400E-01   # N_34
  4  1     1.07758067E-02   # N_41
  4  2    -2.08749179E-02   # N_42
  4  3     7.06728052E-01   # N_43
  4  4     7.07095171E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99302241E-01   # U_11
  1  2     3.73501050E-02   # U_12
  2  1    -3.73501050E-02   # U_21
  2  2     9.99302241E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99990365E-01   # V_11
  1  2    -4.38984429E-03   # V_12
  2  1    -4.38984429E-03   # V_21
  2  2    -9.99990365E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98579778E-01   # cos(theta_t)
  1  2    -5.32768896E-02   # sin(theta_t)
  2  1     5.32768896E-02   # -sin(theta_t)
  2  2     9.98579778E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99722476E-01   # cos(theta_b)
  1  2    -2.35578221E-02   # sin(theta_b)
  2  1     2.35578221E-02   # -sin(theta_b)
  2  2     9.99722476E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06939422E-01   # cos(theta_tau)
  1  2     7.07274101E-01   # sin(theta_tau)
  2  1    -7.07274101E-01   # -sin(theta_tau)
  2  2    -7.06939422E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00128468E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.44687884E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44137350E+02   # higgs               
         4     1.07920616E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.44687884E+03  # The gauge couplings
     1     3.62030778E-01   # gprime(Q) DRbar
     2     6.37604868E-01   # g(Q) DRbar
     3     1.02714995E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.44687884E+03  # The trilinear couplings
  1  1     1.98966490E-06   # A_u(Q) DRbar
  2  2     1.98969494E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.44687884E+03  # The trilinear couplings
  1  1     7.06033654E-07   # A_d(Q) DRbar
  2  2     7.06116937E-07   # A_s(Q) DRbar
  3  3     1.44606270E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.44687884E+03  # The trilinear couplings
  1  1     3.13646283E-07   # A_e(Q) DRbar
  2  2     3.13661728E-07   # A_mu(Q) DRbar
  3  3     3.18014393E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.44687884E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.53869651E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.44687884E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.87791160E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.44687884E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01989159E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.44687884E+03  # The soft SUSY breaking masses at the scale Q
         1     2.49900000E+02   # M_1(Q)              
         2     4.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.55596920E+04   # M^2_Hd              
        22    -9.08065587E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     6.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41114083E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.41448326E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48005630E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48005630E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51994370E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51994370E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.80405064E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     2.98939029E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.49521251E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.20584847E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.17514302E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.56029519E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.97719004E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.98754497E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     7.63798595E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.92888945E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.70230698E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.62060340E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.18244212E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.55988417E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     3.81545727E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.01801027E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.60044400E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.47849424E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.00238838E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.55622708E-08    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.72745163E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.64134011E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -7.82911686E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.38870568E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.37572719E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.04770841E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.52523780E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.88025748E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.00165218E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.59793771E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.83164316E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.02648003E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.19757431E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.11759605E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.14447094E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.18182917E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57744741E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.49741953E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.21076736E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     8.14950174E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42255229E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.88539561E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.98945211E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.59691574E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.07716593E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.72820529E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.19189502E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.38591457E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.15129155E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.66969343E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.49062431E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     4.26251356E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.77632494E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.00580377E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55093749E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.88025748E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.00165218E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.59793771E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.83164316E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.02648003E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.19757431E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.11759605E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.14447094E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.18182917E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57744741E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.49741953E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.21076736E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     8.14950174E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42255229E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.88539561E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.98945211E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.59691574E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.07716593E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.72820529E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.19189502E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.38591457E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.15129155E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.66969343E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.49062431E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     4.26251356E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.77632494E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.00580377E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55093749E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.80750849E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.01516178E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.99711434E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     6.46744868E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.15102567E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.98772376E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.09226948E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54263814E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     9.46499619E-08    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.31926177E-11    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     7.00528914E-12    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.80750849E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.01516178E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.99711434E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     6.46744868E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.15102567E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.98772376E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.09226948E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54263814E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999905E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     9.46499619E-08    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.31926177E-11    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     7.00528914E-12    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.70100738E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.56910862E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.14536138E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.28553000E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.64713899E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.65087937E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.11803762E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.06937866E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.03957822E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.23078090E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.04772531E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.80765802E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.01409705E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99329003E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.67436616E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.30455164E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.99261288E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     8.18955690E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.80765802E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.01409705E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99329003E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.67436616E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.30455164E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.99261288E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     8.18955690E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80817950E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.01400484E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99303126E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.76704571E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.43680332E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.99295982E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.03755211E-07    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     4.67164791E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     9.29677676E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.38477037E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.60967067E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.26369621E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.00439538E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.93812572E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.84698894E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.26602689E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.66052118E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.04760529E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.95239471E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     9.31958910E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.05875307E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.68654341E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     5.83067533E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     5.83067533E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.57204264E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.33856835E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.33376736E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.33376736E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.81105547E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.81105547E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     6.10971942E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.10971942E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     9.22275205E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.36561853E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.32662332E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.89349720E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.89349720E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.10969205E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.95363275E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.30604877E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.30604877E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.88271161E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.88271161E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     4.84987392E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.84987392E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.66778521E-03   # h decays
#          BR         NDA      ID1       ID2
     5.56102193E-01    2           5        -5   # BR(h -> b       bb     )
     7.09322699E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.51100288E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.32612499E-04    2           3        -3   # BR(h -> s       sb     )
     2.31447759E-02    2           4        -4   # BR(h -> c       cb     )
     7.56372744E-02    2          21        21   # BR(h -> g       g      )
     2.60221590E-03    2          22        22   # BR(h -> gam     gam    )
     1.76924387E-03    2          22        23   # BR(h -> Z       gam    )
     2.38944685E-01    2          24       -24   # BR(h -> W+      W-     )
     3.00836297E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.78552514E+01   # H decays
#          BR         NDA      ID1       ID2
     9.02151828E-01    2           5        -5   # BR(H -> b       bb     )
     6.56920526E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.32271400E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.85229652E-04    2           3        -3   # BR(H -> s       sb     )
     7.99433950E-08    2           4        -4   # BR(H -> c       cb     )
     7.97486730E-03    2           6        -6   # BR(H -> t       tb     )
     7.56721844E-06    2          21        21   # BR(H -> g       g      )
     5.21049068E-08    2          22        22   # BR(H -> gam     gam    )
     3.26887868E-09    2          23        22   # BR(H -> Z       gam    )
     6.91060964E-07    2          24       -24   # BR(H -> W+      W-     )
     3.45104446E-07    2          23        23   # BR(H -> Z       Z      )
     5.33613142E-06    2          25        25   # BR(H -> h       h      )
    -1.93084278E-25    2          36        36   # BR(H -> A       A      )
     6.03467633E-19    2          23        36   # BR(H -> Z       A      )
     7.75144552E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.01390563E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.87362464E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.52387722E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.21893526E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.28953791E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.70234140E+01   # A decays
#          BR         NDA      ID1       ID2
     9.22434719E-01    2           5        -5   # BR(A -> b       bb     )
     6.71659816E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.37482529E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.91677859E-04    2           3        -3   # BR(A -> s       sb     )
     8.23073575E-08    2           4        -4   # BR(A -> c       cb     )
     8.20616826E-03    2           6        -6   # BR(A -> t       tb     )
     2.41663804E-05    2          21        21   # BR(A -> g       g      )
     6.45491957E-08    2          22        22   # BR(A -> gam     gam    )
     2.38049749E-08    2          23        22   # BR(A -> Z       gam    )
     7.03880964E-07    2          23        25   # BR(A -> Z       h      )
     8.84561658E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.19516415E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.41992500E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.70423734E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     4.05243421E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.47107575E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.13858368E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.17045346E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.41487324E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.27551985E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.62415450E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.25444087E-01    2           6        -5   # BR(H+ -> t       bb     )
     6.44179045E-07    2          24        25   # BR(H+ -> W+      h      )
     5.08465260E-14    2          24        36   # BR(H+ -> W+      A      )
     4.82966771E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     4.49143452E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07137586E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419566E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     9.99000000E+01   # M_1(MX)             
         2     1.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03944661E+01   # W+
        25     1.25084236E+02   # h
        35     3.00002552E+03   # H
        36     2.99999994E+03   # A
        37     3.00109472E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04044788E+03   # ~d_L
   2000001     3.03521759E+03   # ~d_R
   1000002     3.03953295E+03   # ~u_L
   2000002     3.03665986E+03   # ~u_R
   1000003     3.04044788E+03   # ~s_L
   2000003     3.03521759E+03   # ~s_R
   1000004     3.03953295E+03   # ~c_L
   2000004     3.03665986E+03   # ~c_R
   1000005     9.96539535E+02   # ~b_1
   2000005     3.03285927E+03   # ~b_2
   1000006     9.94441480E+02   # ~t_1
   2000006     3.02406512E+03   # ~t_2
   1000011     3.00654020E+03   # ~e_L
   2000011     3.00171766E+03   # ~e_R
   1000012     3.00514230E+03   # ~nu_eL
   1000013     3.00654020E+03   # ~mu_L
   2000013     3.00171766E+03   # ~mu_R
   1000014     3.00514230E+03   # ~nu_muL
   1000015     2.98599002E+03   # ~tau_1
   2000015     3.02184667E+03   # ~tau_2
   1000016     3.00502734E+03   # ~nu_tauL
   1000021     2.35014885E+03   # ~g
   1000022     1.00780128E+02   # ~chi_10
   1000023     2.17469889E+02   # ~chi_20
   1000025    -2.99588815E+03   # ~chi_30
   1000035     2.99595563E+03   # ~chi_40
   1000024     2.17629824E+02   # ~chi_1+
   1000037     2.99685909E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99891836E-01   # N_11
  1  2     7.63894485E-04   # N_12
  1  3    -1.46858103E-02   # N_13
  1  4     2.45450130E-04   # N_14
  2  1    -3.81890693E-04   # N_21
  2  2     9.99661665E-01   # N_22
  2  3     2.60042385E-02   # N_23
  2  4     4.34570481E-04   # N_24
  3  1     1.02214495E-02   # N_31
  3  2    -1.86889708E-02   # N_32
  3  3     7.06779422E-01   # N_33
  3  4     7.07113211E-01   # N_34
  4  1    -1.05684878E-02   # N_41
  4  2     1.80746769E-02   # N_42
  4  3    -7.06803335E-01   # N_43
  4  4     7.07100176E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99324303E-01   # U_11
  1  2     3.67550992E-02   # U_12
  2  1    -3.67550992E-02   # U_21
  2  2     9.99324303E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99999812E-01   # V_11
  1  2    -6.12989113E-04   # V_12
  2  1    -6.12989113E-04   # V_21
  2  2    -9.99999812E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98481617E-01   # cos(theta_t)
  1  2    -5.50859375E-02   # sin(theta_t)
  2  1     5.50859375E-02   # -sin(theta_t)
  2  2     9.98481617E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99702102E-01   # cos(theta_b)
  1  2    -2.44071149E-02   # sin(theta_b)
  2  1     2.44071149E-02   # -sin(theta_b)
  2  2     9.99702102E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06916137E-01   # cos(theta_tau)
  1  2     7.07297374E-01   # sin(theta_tau)
  2  1    -7.07297374E-01   # -sin(theta_tau)
  2  2    -7.06916137E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00180010E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64195664E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43903648E+02   # higgs               
         4     1.02264530E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64195664E+03  # The gauge couplings
     1     3.62449845E-01   # gprime(Q) DRbar
     2     6.39864829E-01   # g(Q) DRbar
     3     1.02430173E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64195664E+03  # The trilinear couplings
  1  1     2.55979472E-06   # A_u(Q) DRbar
  2  2     2.55983480E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64195664E+03  # The trilinear couplings
  1  1     8.86909222E-07   # A_d(Q) DRbar
  2  2     8.87019228E-07   # A_s(Q) DRbar
  3  3     1.85917772E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64195664E+03  # The trilinear couplings
  1  1     4.09550609E-07   # A_e(Q) DRbar
  2  2     4.09571188E-07   # A_mu(Q) DRbar
  3  3     4.15447118E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64195664E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50742863E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64195664E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.87054793E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64195664E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.03392678E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64195664E+03  # The soft SUSY breaking masses at the scale Q
         1     9.99000000E+01   # M_1(Q)              
         2     1.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -3.49823738E+04   # M^2_Hd              
        22    -9.03627848E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.42144456E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.43384459E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47924390E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47924390E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52075610E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52075610E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     1.11708674E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.27268380E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     3.16008438E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.71264724E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.13206831E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.84846941E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.81123474E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.65656801E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.32949517E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     2.98729226E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.66568826E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.60524454E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.14713070E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     1.10120638E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.32586027E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.42254670E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.44486727E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.39413447E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.09482318E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.45655429E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.27245379E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.18040511E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.20919256E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.43354378E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     7.18192636E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     1.03424426E-01    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.50347905E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     7.06417520E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.97951130E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.65091287E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     4.37135740E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.01184186E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.30449064E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.11967794E-10    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     4.98480053E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.16800376E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.60689540E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.32733452E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.37866989E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.46914381E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.39310409E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     7.06916458E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.88118863E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.65074413E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.88186621E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.50127686E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.29873855E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.19056419E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     4.99169990E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.65402275E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.58006294E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     6.63345120E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.66759856E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.90781695E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.54199356E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     7.06417520E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.97951130E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.65091287E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     4.37135740E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.01184186E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.30449064E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.11967794E-10    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     4.98480053E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.16800376E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.60689540E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.32733452E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.37866989E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.46914381E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.39310409E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     7.06916458E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.88118863E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.65074413E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.88186621E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.50127686E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.29873855E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.19056419E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     4.99169990E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.65402275E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.58006294E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     6.63345120E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.66759856E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.90781695E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.54199356E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     4.02335319E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.76731229E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00827290E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.53351598E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.19694742E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.01499548E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.39936177E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.56513672E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999852E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.44670721E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.57702686E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.64716277E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     4.02335319E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.76731229E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00827290E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.53351598E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.19694742E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.01499548E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.39936177E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.56513672E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999852E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.44670721E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.57702686E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.64716277E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.82249096E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.46663011E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.17838975E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.35498014E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.76389744E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.54756857E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.15120841E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.51180407E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.31020649E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.30078918E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.51640323E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     4.02365158E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.70950346E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.00922537E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.89237897E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.56254509E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.01982415E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.92765388E-12    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     4.02365158E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.70950346E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.00922537E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.89237897E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.56254509E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.01982415E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.92765388E-12    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     4.02383553E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.70868651E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.00897027E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.72240757E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.39952491E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.02014331E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.76418461E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.15605791E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.77035862E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.35558622E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.45170419E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.88021420E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.59269649E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.08727117E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.42293123E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.82416821E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.86511269E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#
#         PDG            Width
DECAY   1000025     8.77040634E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.10053887E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.18672878E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.42718620E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.42718620E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.31066026E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.66795032E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.29419743E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.29419743E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.68766473E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.68766473E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.27886669E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.27886669E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.80666428E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.02561710E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.39635309E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.40025232E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.40025232E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.29609509E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.34378941E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.19074053E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.19074053E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.66139194E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.66139194E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.30825059E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.30825059E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.64603557E-03   # h decays
#          BR         NDA      ID1       ID2
     5.64043473E-01    2           5        -5   # BR(h -> b       bb     )
     7.11762915E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.51965771E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.34729176E-04    2           3        -3   # BR(h -> s       sb     )
     2.32320510E-02    2           4        -4   # BR(h -> c       cb     )
     7.55486357E-02    2          21        21   # BR(h -> g       g      )
     2.58693882E-03    2          22        22   # BR(h -> gam     gam    )
     1.72643518E-03    2          22        23   # BR(h -> Z       gam    )
     2.31889256E-01    2          24       -24   # BR(h -> W+      W-     )
     2.90102247E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.52557181E+01   # H decays
#          BR         NDA      ID1       ID2
     8.96195441E-01    2           5        -5   # BR(H -> b       bb     )
     7.05337171E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.49390369E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.06252932E-04    2           3        -3   # BR(H -> s       sb     )
     8.58534641E-08    2           4        -4   # BR(H -> c       cb     )
     8.56442928E-03    2           6        -6   # BR(H -> t       tb     )
     1.20940651E-05    2          21        21   # BR(H -> g       g      )
     6.26093723E-08    2          22        22   # BR(H -> gam     gam    )
     3.49839686E-09    2          23        22   # BR(H -> Z       gam    )
     8.89062669E-07    2          24       -24   # BR(H -> W+      W-     )
     4.43982897E-07    2          23        23   # BR(H -> Z       Z      )
     5.93597906E-06    2          25        25   # BR(H -> h       h      )
    -5.74417334E-25    2          36        36   # BR(H -> A       A      )
    -2.60407510E-20    2          23        36   # BR(H -> Z       A      )
     9.43865232E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.39883950E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.72356010E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.88957652E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.23767946E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.29245376E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.44710471E+01   # A decays
#          BR         NDA      ID1       ID2
     9.16633160E-01    2           5        -5   # BR(A -> b       bb     )
     7.21392059E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.55066637E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.13274795E-04    2           3        -3   # BR(A -> s       sb     )
     8.84017066E-08    2           4        -4   # BR(A -> c       cb     )
     8.81378410E-03    2           6        -6   # BR(A -> t       tb     )
     2.59557508E-05    2          21        21   # BR(A -> g       g      )
     6.13810820E-08    2          22        22   # BR(A -> gam     gam    )
     2.55394761E-08    2          23        22   # BR(A -> Z       gam    )
     9.05920454E-07    2          23        25   # BR(A -> Z       h      )
     9.82821784E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.53450087E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.91831780E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.98473395E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.76604861E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46237080E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.60538853E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.33550427E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.35916155E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.37251593E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.82370661E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.20571263E-01    2           6        -5   # BR(H+ -> t       bb     )
     8.30626985E-07    2          24        25   # BR(H+ -> W+      h      )
     5.49437826E-14    2          24        36   # BR(H+ -> W+      A      )
     5.44811459E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     7.97892290E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.08278335E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

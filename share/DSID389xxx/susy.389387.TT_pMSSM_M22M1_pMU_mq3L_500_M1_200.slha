#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.12256285E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     1.99900000E+02   # M_1(MX)             
         2     3.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23     3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     4.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04010432E+01   # W+
        25     1.24733113E+02   # h
        35     3.00021341E+03   # H
        36     2.99999956E+03   # A
        37     3.00096237E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.02388392E+03   # ~d_L
   2000001     3.01870356E+03   # ~d_R
   1000002     3.02296539E+03   # ~u_L
   2000002     3.02068423E+03   # ~u_R
   1000003     3.02388392E+03   # ~s_L
   2000003     3.01870356E+03   # ~s_R
   1000004     3.02296539E+03   # ~c_L
   2000004     3.02068423E+03   # ~c_R
   1000005     5.68508668E+02   # ~b_1
   2000005     3.01819855E+03   # ~b_2
   1000006     5.65481449E+02   # ~t_1
   2000006     3.00211696E+03   # ~t_2
   1000011     3.00663140E+03   # ~e_L
   2000011     3.00114577E+03   # ~e_R
   1000012     3.00523735E+03   # ~nu_eL
   1000013     3.00663140E+03   # ~mu_L
   2000013     3.00114577E+03   # ~mu_R
   1000014     3.00523735E+03   # ~nu_muL
   1000015     2.98584820E+03   # ~tau_1
   2000015     3.02217669E+03   # ~tau_2
   1000016     3.00534502E+03   # ~nu_tauL
   1000021     2.32930056E+03   # ~g
   1000022     2.01744910E+02   # ~chi_10
   1000023     4.23614342E+02   # ~chi_20
   1000025    -3.00198809E+03   # ~chi_30
   1000035     3.00262501E+03   # ~chi_40
   1000024     4.23776052E+02   # ~chi_1+
   1000037     3.00324272E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99887591E-01   # N_11
  1  2    -9.62412470E-04   # N_12
  1  3     1.48625640E-02   # N_13
  1  4    -1.72717848E-03   # N_14
  2  1     1.36594157E-03   # N_21
  2  2     9.99633464E-01   # N_22
  2  3    -2.66016985E-02   # N_23
  2  4     4.83960750E-03   # N_24
  3  1    -9.27050408E-03   # N_31
  3  2     1.53995145E-02   # N_32
  3  3     7.06851674E-01   # N_33
  3  4     7.07133384E-01   # N_34
  4  1    -1.17045781E-02   # N_41
  4  2     2.22455990E-02   # N_42
  4  3     7.06705147E-01   # N_43
  4  4    -7.07061505E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99291480E-01   # U_11
  1  2    -3.76369213E-02   # U_12
  2  1     3.76369213E-02   # U_21
  2  2     9.99291480E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99976549E-01   # V_11
  1  2    -6.84847628E-03   # V_12
  2  1     6.84847628E-03   # V_21
  2  2     9.99976549E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98891061E-01   # cos(theta_t)
  1  2    -4.70812941E-02   # sin(theta_t)
  2  1     4.70812941E-02   # -sin(theta_t)
  2  2     9.98891061E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99920523E-01   # cos(theta_b)
  1  2     1.26074456E-02   # sin(theta_b)
  2  1    -1.26074456E-02   # -sin(theta_b)
  2  2     9.99920523E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06921605E-01   # cos(theta_tau)
  1  2     7.07291909E-01   # sin(theta_tau)
  2  1    -7.07291909E-01   # -sin(theta_tau)
  2  2     7.06921605E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.01929135E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.22562850E+03  # DRbar Higgs Parameters
         1     3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.44690789E+02   # higgs               
         4     6.61778363E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.22562850E+03  # The gauge couplings
     1     3.61498965E-01   # gprime(Q) DRbar
     2     6.38016401E-01   # g(Q) DRbar
     3     1.03086903E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.22562850E+03  # The trilinear couplings
  1  1     1.38244394E-06   # A_u(Q) DRbar
  2  2     1.38246493E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.22562850E+03  # The trilinear couplings
  1  1     3.54113510E-07   # A_d(Q) DRbar
  2  2     3.54181820E-07   # A_s(Q) DRbar
  3  3     8.03062840E-07   # A_b(Q) DRbar
#
BLOCK AE Q=  1.22562850E+03  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     7.54520812E-08   # A_mu(Q) DRbar
  3  3     7.63422033E-08   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.22562850E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.56423796E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.22562850E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.12778050E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.22562850E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.06002036E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.22562850E+03  # The soft SUSY breaking masses at the scale Q
         1     1.99900000E+02   # M_1(Q)              
         2     3.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -8.90783885E+04   # M^2_Hd              
        22    -9.12844367E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     4.99899994E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.41295445E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     8.23536950E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.48250068E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.48250068E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.51749932E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.51749932E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     9.79319612E-01   # stop1 decays
#          BR         NDA      ID1       ID2
     4.83664037E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     9.51633596E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.05288640E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     6.17497423E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.08603582E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.19836611E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.20031103E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.63716644E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.51929433E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.01644638E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     5.49859739E-01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.21684317E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     8.78315683E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
#
#         PDG            Width
DECAY   2000005     4.25493891E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.86381295E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.12506370E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.78658062E-06    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.58823836E-06    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
    -5.20671642E-09    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.27692029E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     2.95178654E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     4.26712487E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     6.14732450E-02    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.96509925E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     5.85325019E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.61026458E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.07966277E-09    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.43659156E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.22098251E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     3.16143413E-09    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.11022016E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.19942057E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.56801457E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.84222869E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.47245553E-09    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.68386249E-09    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.43198252E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.97025759E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     5.97127158E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.60795290E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.21388947E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.33046836E-08    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.21527052E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.04227746E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.11706247E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.68951441E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.45874247E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.08172573E-08    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     5.65964997E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     8.36389307E-10    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55412493E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.96509925E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.85325019E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.61026458E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.07966277E-09    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.43659156E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.22098251E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     3.16143413E-09    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.11022016E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.19942057E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.56801457E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.84222869E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.47245553E-09    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.68386249E-09    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.43198252E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.97025759E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     5.97127158E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.60795290E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.21388947E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.33046836E-08    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.21527052E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.04227746E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.11706247E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.68951441E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.45874247E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.08172573E-08    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     5.65964997E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     8.36389307E-10    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55412493E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.89221694E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.91524414E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.00735738E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.06757090E-10    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.40868502E-10    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     6.00111815E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.49755287E-09    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.54607495E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99998191E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.80890158E-06    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
#
#         PDG            Width
DECAY   1000013     3.89221694E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.91524414E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.00735738E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.06757090E-10    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.40868502E-10    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     6.00111815E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.49755287E-09    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.54607495E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99998191E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.80890158E-06    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
#
#         PDG            Width
DECAY   1000015     2.74740992E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.51480051E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.16509445E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.32010504E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.68899617E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.59816306E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.13739691E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.13517065E-06    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     9.34386880E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.26417312E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     9.21275218E-06    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.89249180E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     9.97743160E-02    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.99634401E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.22798830E-10    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.87254387E-10    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.00591282E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     5.16382655E-11    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.89249180E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     9.97743160E-02    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.99634401E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.22798830E-10    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.87254387E-10    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.00591282E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     5.16382655E-11    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.89297586E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     9.97655486E-02    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.99608748E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.64627512E-10    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.53319310E-10    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     6.00625632E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     7.04856422E-08    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     5.14669173E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.93704264E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.70364824E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     7.30157663E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     7.37790133E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.70468933E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.07896677E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.54328771E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.95356788E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     4.20552603E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.75335251E-02    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     9.52466475E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.96207385E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.31677165E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.51181964E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.51429232E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.51429232E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     8.48244105E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.19276010E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.67943252E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.67943252E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     2.25651745E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     2.25651745E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     2.35627420E-12    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     2.35627420E-12    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.35627420E-12    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     2.35627420E-12    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     6.73178187E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     6.73178187E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.87599192E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     8.34140731E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.18577661E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.58003897E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.58003897E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.37046808E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.77880452E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.65530818E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.65530818E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     2.28219539E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     2.28219539E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.16912538E-11    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.16912538E-11    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.16912538E-11    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.16912538E-11    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     8.88987669E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     8.88987669E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     4.87683498E-03   # h decays
#          BR         NDA      ID1       ID2
     6.81519082E-01    2           5        -5   # BR(h -> b       bb     )
     5.34347795E-02    2         -15        15   # BR(h -> tau+    tau-   )
     1.89161683E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.01627896E-04    2           3        -3   # BR(h -> s       sb     )
     1.73292717E-02    2           4        -4   # BR(h -> c       cb     )
     5.65342100E-02    2          21        21   # BR(h -> g       g      )
     1.90909858E-03    2          22        22   # BR(h -> gam     gam    )
     1.25101769E-03    2          22        23   # BR(h -> Z       gam    )
     1.66677011E-01    2          24       -24   # BR(h -> W+      W-     )
     2.07547406E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.39014544E+01   # H decays
#          BR         NDA      ID1       ID2
     7.41426862E-01    2           5        -5   # BR(H -> b       bb     )
     1.78889841E-01    2         -15        15   # BR(H -> tau+    tau-   )
     6.32511728E-04    2         -13        13   # BR(H -> mu+     mu-    )
     7.76721733E-04    2           3        -3   # BR(H -> s       sb     )
     2.19270583E-07    2           4        -4   # BR(H -> c       cb     )
     2.18736660E-02    2           6        -6   # BR(H -> t       tb     )
     3.38488240E-05    2          21        21   # BR(H -> g       g      )
     5.58107738E-08    2          22        22   # BR(H -> gam     gam    )
     8.38545220E-09    2          23        22   # BR(H -> Z       gam    )
     3.49112438E-05    2          24       -24   # BR(H -> W+      W-     )
     1.74341012E-05    2          23        23   # BR(H -> Z       Z      )
     8.83289502E-05    2          25        25   # BR(H -> h       h      )
     4.33431172E-23    2          36        36   # BR(H -> A       A      )
     3.48872680E-17    2          23        36   # BR(H -> Z       A      )
     2.24247059E-03    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.11472307E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     1.11795168E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     7.11082037E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.20363035E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     6.31094543E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     1.31840355E+01   # A decays
#          BR         NDA      ID1       ID2
     7.81830250E-01    2           5        -5   # BR(A -> b       bb     )
     1.88615515E-01    2         -15        15   # BR(A -> tau+    tau-   )
     6.66898454E-04    2         -13        13   # BR(A -> mu+     mu-    )
     8.19089798E-04    2           3        -3   # BR(A -> s       sb     )
     2.31135531E-07    2           4        -4   # BR(A -> c       cb     )
     2.30445627E-02    2           6        -6   # BR(A -> t       tb     )
     6.78640424E-05    2          21        21   # BR(A -> g       g      )
     3.34306200E-08    2          22        22   # BR(A -> gam     gam    )
     6.68513839E-08    2          23        22   # BR(A -> Z       gam    )
     3.66683074E-05    2          23        25   # BR(A -> Z       h      )
     2.66481621E-03    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     1.22518673E-04    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     1.32844253E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     8.03043603E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     9.86473312E+00   # H+ decays
#          BR         NDA      ID1       ID2
     1.08383475E-03    2           4        -5   # BR(H+ -> c       bb     )
     2.52162091E-01    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.91583647E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     6.93653140E-06    2           2        -5   # BR(H+ -> u       bb     )
     5.23963823E-05    2           2        -3   # BR(H+ -> u       sb     )
     1.07796207E-03    2           4        -3   # BR(H+ -> c       sb     )
     7.06651150E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.90844081E-05    2          24        25   # BR(H+ -> W+      h      )
     1.10356861E-13    2          24        36   # BR(H+ -> W+      A      )
     2.07040490E-03    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     3.82863324E-09    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     3.59545527E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.5                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.16419703E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.5  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.7.3       # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   #  nonUniversal                                     
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.19000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77690000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         3     2.00000000E+01   # tanb                
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0    -1.00000000E+00   # Set                 
         1     3.49900000E+02   # M_1(MX)             
         2     6.99900000E+02   # M_2(MX)             
         3     2.20000000E+03   # M_3(MX)             
        11     2.94338000E+03   # At(MX)              
        12     0.00000000E+00   # Ab(MX)              
        13     0.00000000E+00   # Atau(MX)            
        23    -3.00000000E+03   # mu(MX)              
        25     2.00000000E+01   # tan                 
        26     3.00000000E+03   # mA(pole)            
        31     3.00000000E+03   # meL(MX)             
        32     3.00000000E+03   # mmuL(MX)            
        33     3.00000000E+03   # mtauL(MX)           
        34     3.00000000E+03   # meR(MX)             
        35     3.00000000E+03   # mmuR(MX)            
        36     3.00000000E+03   # mtauR(MX)           
        41     3.00000000E+03   # mqL1(MX)            
        42     3.00000000E+03   # mqL2(MX)            
        43     8.99900000E+02   # mqL3(MX)            
        44     3.00000000E+03   # muR(MX)             
        45     3.00000000E+03   # mcR(MX)             
        46     3.00000000E+03   # mtR(MX)             
        47     3.00000000E+03   # mdR(MX)             
        48     3.00000000E+03   # msR(MX)             
        49     3.00000000E+03   # mbR(MX)             
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04024177E+01   # W+
        25     1.24881402E+02   # h
        35     3.00003806E+03   # H
        36     2.99999994E+03   # A
        37     3.00108610E+03   # H+
         5     4.81337004E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.04035701E+03   # ~d_L
   2000001     3.03521003E+03   # ~d_R
   1000002     3.03945141E+03   # ~u_L
   2000002     3.03666655E+03   # ~u_R
   1000003     3.04035701E+03   # ~s_L
   2000003     3.03521003E+03   # ~s_R
   1000004     3.03945141E+03   # ~c_L
   2000004     3.03666655E+03   # ~c_R
   1000005     1.00158564E+03   # ~b_1
   2000005     3.03301613E+03   # ~b_2
   1000006     9.99356155E+02   # ~t_1
   2000006     3.02422666E+03   # ~t_2
   1000011     3.00645714E+03   # ~e_L
   2000011     3.00168925E+03   # ~e_R
   1000012     3.00507141E+03   # ~nu_eL
   1000013     3.00645714E+03   # ~mu_L
   2000013     3.00168925E+03   # ~mu_R
   1000014     3.00507141E+03   # ~nu_muL
   1000015     2.98613646E+03   # ~tau_1
   2000015     3.02161267E+03   # ~tau_2
   1000016     3.00496410E+03   # ~nu_tauL
   1000021     2.35014498E+03   # ~g
   1000022     3.52201197E+02   # ~chi_10
   1000023     7.35627349E+02   # ~chi_20
   1000025    -2.99591718E+03   # ~chi_30
   1000035     2.99628401E+03   # ~chi_40
   1000024     7.35790786E+02   # ~chi_1+
   1000037     2.99705814E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.99889514E-01   # N_11
  1  2    -5.59578789E-05   # N_12
  1  3    -1.48313327E-02   # N_13
  1  4    -9.94002538E-04   # N_14
  2  1     4.61614623E-04   # N_21
  2  2     9.99622414E-01   # N_22
  2  3     2.70137192E-02   # N_23
  2  4     5.00744719E-03   # N_24
  3  1    -9.78097270E-03   # N_31
  3  2     1.55643915E-02   # N_32
  3  3    -7.06857528E-01   # N_33
  3  4     7.07117047E-01   # N_34
  4  1     1.11838761E-02   # N_41
  4  2    -2.26445408E-02   # N_42
  4  3     7.06684319E-01   # N_43
  4  4     7.07078086E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.99270136E-01   # U_11
  1  2     3.81994247E-02   # U_12
  2  1    -3.81994247E-02   # U_21
  2  2     9.99270136E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.99974939E-01   # V_11
  1  2    -7.07960299E-03   # V_12
  2  1    -7.07960299E-03   # V_21
  2  2    -9.99974939E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.98481950E-01   # cos(theta_t)
  1  2    -5.50799013E-02   # sin(theta_t)
  2  1     5.50799013E-02   # -sin(theta_t)
  2  2     9.98481950E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99718027E-01   # cos(theta_b)
  1  2    -2.37458731E-02   # sin(theta_b)
  2  1     2.37458731E-02   # -sin(theta_b)
  2  2     9.99718027E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1    -7.06960111E-01   # cos(theta_tau)
  1  2     7.07253421E-01   # sin(theta_tau)
  2  1    -7.07253421E-01   # -sin(theta_tau)
  2  2    -7.06960111E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.00140352E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  1.64197030E+03  # DRbar Higgs Parameters
         1    -3.00000000E+03   # mu(Q)MSSM           
         2     2.00000000E+01   # tan                 
         3     2.43903735E+02   # higgs               
         4     1.03236363E+07   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  1.64197030E+03  # The gauge couplings
     1     3.62453592E-01   # gprime(Q) DRbar
     2     6.37009229E-01   # g(Q) DRbar
     3     1.02428737E+00   # g3(Q) DRbar
#
BLOCK AU Q=  1.64197030E+03  # The trilinear couplings
  1  1     2.57532813E-06   # A_u(Q) DRbar
  2  2     2.57536562E-06   # A_c(Q) DRbar
  3  3     2.94338000E+03   # A_t(Q) DRbar
#
BLOCK AD Q=  1.64197030E+03  # The trilinear couplings
  1  1     9.18592079E-07   # A_d(Q) DRbar
  2  2     9.18697258E-07   # A_s(Q) DRbar
  3  3     1.84805962E-06   # A_b(Q) DRbar
#
BLOCK AE Q=  1.64197030E+03  # The trilinear couplings
  1  1     3.83326862E-07   # A_e(Q) DRbar
  2  2     3.83345495E-07   # A_mu(Q) DRbar
  3  3     3.88555133E-07   # A_tau(Q) DRbar
#
BLOCK Yu Q=  1.64197030E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.50645604E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  1.64197030E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.76551817E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  1.64197030E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01292943E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  1.64197030E+03  # The soft SUSY breaking masses at the scale Q
         1     3.49900000E+02   # M_1(Q)              
         2     6.99900000E+02   # M_2(Q)              
         3     2.20000000E+03   # M_3(Q)              
        21    -4.02854457E+04   # M^2_Hd              
        22    -9.03427724E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     8.99899993E+02   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.40843669E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     6.40513240E+01   # gluino decays
#          BR         NDA      ID1       ID2
     2.47907790E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     2.47907790E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.52092210E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.52092210E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     2.47404849E+00   # stop1 decays
#          BR         NDA      ID1       ID2
     4.22840954E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.51702640E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     7.06013264E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     1.12429089E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     5.75661368E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.49932854E-04    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     7.03921043E-04    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.33370376E-04    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     3.00880384E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.67464510E-01    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.59992516E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.12909230E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
#
#         PDG            Width
DECAY   1000005     2.27679035E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     5.10477062E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.90162298E-01    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     5.58789996E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.28255902E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     3.08886967E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.40847798E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.12884192E-05    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.96135882E-05    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.98940316E-08    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.57111245E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
     6.93308948E-02    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     9.89899189E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     1.43578112E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     6.67887062E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.12789394E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.55542274E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.05137550E-08    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.72714025E-08    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.11232583E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.54570954E-08    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     5.27097136E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     4.15260081E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.57630525E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.06638659E-08    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.26576239E-08    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.62610148E-08    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.42369415E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.68411073E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     6.13229433E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.55424756E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     5.12182343E-08    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.01875668E-07    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     3.10668025E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     4.68357004E-07    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     5.27774303E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.64988714E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     4.48121707E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.71650960E-09    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.35611342E-09    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     4.30869982E-09    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.55187813E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.67887062E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.12789394E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.55542274E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.05137550E-08    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.72714025E-08    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.11232583E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.54570954E-08    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     5.27097136E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     4.15260081E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.57630525E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.06638659E-08    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.26576239E-08    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.62610148E-08    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.42369415E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.68411073E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     6.13229433E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.55424756E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     5.12182343E-08    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.01875668E-07    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     3.10668025E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     4.68357004E-07    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     5.27774303E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.64988714E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     4.48121707E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.71650960E-09    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.35611342E-09    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     4.30869982E-09    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.55187813E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59620273E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.06226187E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.98180261E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.65282691E-09    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.08275011E-09    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.95593508E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.83796103E-08    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.52578576E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99999803E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.93596118E-07    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.45230359E-09    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     1.66532085E-09    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     3.59620273E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.06226187E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.98180261E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.65282691E-09    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.08275011E-09    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.95593508E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.83796103E-08    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.52578576E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99999803E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.93596118E-07    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.45230359E-09    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     1.66532085E-09    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.58455405E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.68713661E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.10626905E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.20659434E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.53500631E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.77002774E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     2.07854084E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.56802230E-05    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.35750544E-05    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.15098098E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.57885284E-05    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59620290E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.06216274E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.97694748E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.57261006E-09    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.67922560E-09    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     5.96088961E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     9.59093853E-10    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59620290E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.06216274E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.97694748E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.57261006E-09    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.67922560E-09    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     5.96088961E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     9.59093853E-10    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.59637747E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06207114E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     2.97666957E-01    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     5.44285077E-09    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     9.44448302E-09    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     5.96124122E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     1.79232388E-06    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.37821510E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000037     8.58555557E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.31247302E-01    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     6.57233150E-01    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.87420473E-07    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     6.29680937E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.07056146E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     6.12652127E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     6.65803402E-02    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.23822862E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.38254998E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     5.61745002E-01    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     8.60256813E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.20456076E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.26342691E-02    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     6.11044591E-02    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     6.11044591E-02    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     9.57085760E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.05461100E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     3.32098737E-01    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     3.32098737E-01    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     6.43980583E-02    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     6.43980583E-02    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     3.23622167E-07    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     3.23622167E-07    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000035     8.51206706E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     9.31750916E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.04041462E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.17823500E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     6.17823500E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.27075571E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.63862032E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.28721052E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     3.28721052E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     6.50886507E-02    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     6.50886507E-02    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.39437053E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     2.39437053E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY        25     3.59396810E-03   # h decays
#          BR         NDA      ID1       ID2
     5.64444693E-01    2           5        -5   # BR(h -> b       bb     )
     7.20786592E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.55161176E-04    2         -13        13   # BR(h -> mu+     mu-    )
     5.41682868E-04    2           3        -3   # BR(h -> s       sb     )
     2.35378714E-02    2           4        -4   # BR(h -> c       cb     )
     7.62881308E-02    2          21        21   # BR(h -> g       g      )
     2.60775812E-03    2          22        22   # BR(h -> gam     gam    )
     1.72042008E-03    2          22        23   # BR(h -> Z       gam    )
     2.29832972E-01    2          24       -24   # BR(h -> W+      W-     )
     2.86926507E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.49519519E+01   # H decays
#          BR         NDA      ID1       ID2
     8.95785475E-01    2           5        -5   # BR(H -> b       bb     )
     7.11470489E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.51558963E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.08915810E-04    2           3        -3   # BR(H -> s       sb     )
     8.65862073E-08    2           4        -4   # BR(H -> c       cb     )
     8.63752587E-03    2           6        -6   # BR(H -> t       tb     )
     1.22934612E-05    2          21        21   # BR(H -> g       g      )
     6.79752296E-08    2          22        22   # BR(H -> gam     gam    )
     3.53921782E-09    2          23        22   # BR(H -> Z       gam    )
     7.81433914E-07    2          24       -24   # BR(H -> W+      W-     )
     3.90235249E-07    2          23        23   # BR(H -> Z       Z      )
     5.98682446E-06    2          25        25   # BR(H -> h       h      )
    -2.79358352E-24    2          36        36   # BR(H -> A       A      )
    -2.75720095E-20    2          23        36   # BR(H -> Z       A      )
     7.14593821E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     4.22993433E-05    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.57019765E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.53076593E-04    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     2.24777863E-02    2     1000006  -1000006   # BR(H -> ~t_1    ~t_1*  )
     5.08939049E-06    2     1000005  -1000005   # BR(H -> ~b_1    ~b_1*  )
#
#         PDG            Width
DECAY        36     3.41805031E+01   # A decays
#          BR         NDA      ID1       ID2
     9.16037157E-01    2           5        -5   # BR(A -> b       bb     )
     7.27524097E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.57234776E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.15937720E-04    2           3        -3   # BR(A -> s       sb     )
     8.91531464E-08    2           4        -4   # BR(A -> c       cb     )
     8.88870378E-03    2           6        -6   # BR(A -> t       tb     )
     2.61763820E-05    2          21        21   # BR(A -> g       g      )
     6.87160413E-08    2          22        22   # BR(A -> gam     gam    )
     2.57918904E-08    2          23        22   # BR(A -> Z       gam    )
     7.96104579E-07    2          23        25   # BR(A -> Z       h      )
     9.27229385E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     4.51675520E-05    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     4.63174620E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.85829484E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
#
#         PDG            Width
DECAY        37     3.75905285E+01   # H+ decays
#          BR         NDA      ID1       ID2
     1.46228086E-03    2           4        -5   # BR(H+ -> c       bb     )
     6.61766246E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.33984403E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     9.35858595E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.37506680E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.82895458E-04    2           4        -3   # BR(H+ -> c       sb     )
     9.20529848E-01    2           6        -5   # BR(H+ -> t       bb     )
     7.25126207E-07    2          24        25   # BR(H+ -> W+      h      )
     5.29129666E-14    2          24        36   # BR(H+ -> W+      A      )
     4.95212679E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.52117270E-11    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.07953193E-02    2     1000006  -1000005   # BR(H+ -> ~t_1    ~b_1*  )

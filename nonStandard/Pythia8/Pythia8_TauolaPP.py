## TAUOLA config for PYTHIA

## Disable native tau decays
assert hasattr(genSeq, "Pythia8")
genSeq.Pythia8.Commands += ["15:onMode = off"]

## Enable TAUOLA
include("MC15JobOptions/nonStandard/TauolaPP_Fragment.py")



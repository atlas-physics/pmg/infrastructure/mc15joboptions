#! /bin/bash

#
# Remove space or tab at the end of the line
#
sed -ri.bak  '/\s+$/s///' share/evgeninputfiles.csv

#
# Remove duplicated lines
#
sort -u share/evgeninputfiles.csv > share/evgeninputfiles.csv.unique
mv share/evgeninputfiles.csv.unique share/evgeninputfiles.csv

#
# Sort the evgen file in the following way: DSID, CM energy, all the rest
#
cat share/evgeninputfiles.csv | sort -k 1,1n -k 2,2n > evgen_tmp
mv evgen_tmp share/evgeninputfiles.csv

#
# Search duplicated lines with different evgen files
#
cat share/evgeninputfiles.csv | awk -F, '{print $1 $2}'  > entries.txt
sort entries.txt | uniq -c -d


#Based on JOs from 411233
#---------------------------------------------------------------------------
# Title: PowhegHerwig7EvtGen_tt_hdamp258p75_713_control.py 
#-----------------------------------------------------------------------------

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig713 ttbar production with Powheg hdamp equal 1.5*top mass, H7.1-Default tune, MET/HT filter, ME NNPDF30 NLO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.tune        = "H7.1-Default"
evgenConfig.contact     = [ 'nedaa.asbah@cern.ch']
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]


#--------------------------------------------------------------
# Herwig7 (H7.1-Default tune) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Semi-leptonic decay filter
include('MC15JobOptions/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# DSIDs for samples with HT filters
thisDSID = runArgs.runNumber

HTcuts = {
    411335: (1500, 9000),
    411336: (1000, 1500),
    411337: ( 600, 1000),
}

# Provide the HT cuts in GeV
def setupStandardHTFilter(HTrange):
    HTmin,HTmax = HTrange
    include('MC15JobOptions/HTFilter.py')
    htfilt = filtSeq.HTFilter
    htfilt.MinJetPt = 35.*GeV # Min pT to consider jet in HT
    htfilt.MaxJetEta = 2.5 # Max eta to consider jet in HT
    htfilt.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
    htfilt.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
    htfilt.MaxLeptonEta = 2.5 # Max eta to consider muon in HT
    filtSeq.HTFilter.MinHT = HTmin*GeV # Min HT to keep event
    filtSeq.HTFilter.MaxHT = HTmax*GeV # Max HT to keep event

if thisDSID in HTcuts.keys():
    setupStandardHTFilter(HTcuts[thisDSID])


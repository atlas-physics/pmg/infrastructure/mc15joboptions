# setup Herwig++
include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )
include ( 'MC15JobOptions/Herwigpp_EvtGen.py')

try:
    mgluino = runArgs.jobConfig[0].split('_')[5]
    mbino   = runArgs.jobConfig[0].split('_')[6]
    lifetime= runArgs.jobConfig[0].split('_')[7].split('n')[0]
    dsid = runArgs.runNumber
except:
    raise RuntimeError('DSID %s is not found in the grid point dictionary. Aborting!' % runArgs.runNumber)

slha_file = 'susy.%s.gl_bino_focus_%s_%s_%sns.slha' %(dsid,mgluino,mbino,lifetime)

# Add Herwig++ parameters for this process
include ( 'MC15JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM gluino-bino m_gluino=%s GeV, m_bino=%s GeV, bino lifetime %s ns bino lifetime' %(mgluino,mbino,lifetime)
evgenConfig.keywords = ['SUSY', 'gluino', 'bino']
evgenConfig.contact = [ 'osamu.jinnouchi@cern.ch']

genSeq.Herwigpp.Commands += cmds.splitlines()

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 0.
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = 11
filtSeq.ParticleFilter.PDG = 1000022
filtSeq.ParticleFilter.MinParts = 2

# TestHepMC max vtx
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000

del cmds

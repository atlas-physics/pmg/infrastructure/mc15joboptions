## JobOption fragment for Herwig 7.0.1

## Herwig7 config for Herwig7_QED and EvtGen
include("MC15JobOptions/nonStandard/Herwig7_701_QED_Common.py")
include("MC15JobOptions/Herwig7_701_EvtGen.py")

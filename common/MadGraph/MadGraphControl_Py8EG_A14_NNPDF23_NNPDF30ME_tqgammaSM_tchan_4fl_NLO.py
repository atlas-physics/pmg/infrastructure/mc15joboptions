from MadGraphControl.MadGraphUtils import *
import os,subprocess,fileinput

# General settings 

nevents=int(1.1*runArgs.maxEvents)
mode=0
#gridpack_mode=True
#gridpack_dir='madevent/'

name = 'tqgammaSM_tchan_4fl_NLO'
runName='madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

param_card_name = "aMcAtNlo_param_card_tqgamma_4FS.dat" 

get_param_card = subprocess.Popen(['get_files','-data',param_card_name])
get_param_card.wait()
if not os.access(param_card_name, os.R_OK):
	print('ERROR: Could not get param card')
	raise RuntimeError("parameter card '%s' missing!"%param_card_name)



fcard = open('proc_card_mg5.dat','w')

fcard.write("""
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~ 
define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
define vl = ve vm vt 
define vl~ = ve~ vm~ vt~
generate p p > t b~ j a $$ w+ w- [QCD]
add process p p > t~ b j a $$ w+ w- [QCD] 
output -f
""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")



#Fetch default LO run_card.dat and set parameters

extras = { 'lhaid'         :260400, #NNPDF30_nlo_as_0118_nf_4
           'lhe_version'  : '3.0',
           'pdlabel'       :'lhapdf',
           'parton_shower' :'PYTHIA8',
           'ptgmin'        :10,
           'r0gamma'       :0.2, 
           'maxjetflavor'  :4,
           'dynamical_scale_choice': '3', #sum of the transverse mass divided by 2
           'reweight_scale':'True',
		   'reweight_pdf'  :'True',  
           'ptl'           :0.0,
           'ptj'           :0.0,
           'etal'          :5.0,
           'etagamma'      :5.0,
           'etaj'          :-1,
           'drll'          :0.0,
           'PDF_set_min': 260401,
           'PDF_set_max': 260500,
           'store_rwgt_info':True,
           }

process_dir = new_process()#grid_pack=gridpack_dir)


build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)
               
               
madspin_card_loc='madspin_card.dat'                                                                                                                                    

mscard = open(madspin_card_loc,'w')                                                                                                                                    
mscard.write("""#************************************************************                                                                                          
#*                        MadSpin                           *                                                                                                              
#*                                                          *                                                                                                              
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *                                                                                                              
#*                                                          *                                                                                                              
#*    Part of the MadGraph5_aMC@NLO Framework:              *                                                                                                              
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *                                                                                                              
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *                                                                                                              
#*                                                          *                                                                                                              
#************************************************************                                                                                                              
#Some options (uncomment to apply)                                                                                                                                         
#                                                                                                                                                                          
# set seed 1                                                                                                                                                               
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight                                                                                     
# set BW_cut 15                # cut on how far the particle can be off-shell                                                                                              
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event                                                                                        
#                                                                                                                                                                          
set seed %i                                                                                                                                                                
# specify the decay for the final state particles                                                                                                                          
decay t > w+ b, w+ > l+ vl                                                                                                                                               
decay t~ > w- b~, w- > l- vl~                                                                                                                                                                                                                                                                                                      
# running the actual code                                                                                                                                                  
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()

print_cards()

generate(run_card_loc='run_card.dat',param_card_loc=param_card_name,madspin_card_loc=madspin_card_loc,mode=mode,proc_dir=process_dir,random_seed=runArgs.randomSeed,nevents=nevents)#,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,gridpack_compile=True)
outputDS=arrange_output(proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3)

#### Shower
evgenConfig.description = 'aMC@NLO_'+str(name)
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.keywords+= ['SM', 'top',  'photon','singleTop','lepton'] 
evgenConfig.contact = ['bjorn.wendland@cern.ch']
runArgs.inputGeneratorFile=outputDS


include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")



if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): 
        mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

include("MC15JobOptions/Pythia8_aMcAtNlo.py")
#include("MC15JobOptions/Pythia8_ShowerWeights.py")

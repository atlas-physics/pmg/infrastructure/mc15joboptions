import math
from MadGraphControl.MadGraphUtils import *
include ( 'MC15JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

top_mass = 173. # GeV

def get_widths(leptoquark_coupling, coupling_value, leptoquark_mass):
  if not (leptoquark_coupling == 'lam1122'):
    raise NotImplementedError('I do not know what to do for coupling %s at value' % leptoquark_coupling)
  MS1 = leptoquark_mass
  yRR1x1 = coupling_value
  yRR1x2 = 0.
  yRR1x3 = 0.
  yRR2x1 = 0.
  yRR2x2 = coupling_value
  yRR2x3 = 0.
  yRR3x1 = 0.
  yRR3x2 = 0.
  yRR3x3 = 0.
  yLL1x1 = 0.
  yLL1x2 = 0.
  yLL1x3 = 0.
  yLL2x1 = 0.
  yLL2x2 = 0.
  yLL2x3 = 0.
  yLL3x1 = 0.
  yLL3x2 = 0.
  yLL3x3 = 0.
  width = (MS1*(2*abs(yLL1x1)**2 + 2*abs(yLL1x2)**2 + 2*abs(yLL1x3)**2 + 2*abs(yLL2x1)**2 + 2*abs(yLL2x2)**2 + 2*abs(yLL2x3)**2 + abs(yLL3x1)**2 + abs(yLL3x2)**2 + abs(yLL3x3)**2 + abs(yRR1x1)**2 + abs(yRR1x2)**2 + abs(yRR1x3)**2 + abs(yRR2x1)**2 + abs(yRR2x2)**2 + abs(yRR2x3)**2) + ((MS1**2 - top_mass**2)**2*(abs(yLL3x1)**2 + abs(yLL3x2)**2 + abs(yLL3x3)**2 + abs(yRR3x1)**2 + abs(yRR3x2)**2 + abs(yRR3x3)**2))/MS1**3)/(16.*3.14159265359) 
  return width

# Sample job options should have name such as MC15.999900.MGPy8EG_A14N23LO_LO_LQ_S1_lam1122_500_1p0.py
jo_name_list = runArgs.jobConfig[0].rstrip('.py').split('.')
mcid        = jo_name_list[1]
sample_name = jo_name_list[2]

def sample_name_to_parameters(sample_name):
  sample_name_list = sample_name.split('_')
  leptoquark_mass = float( sample_name_list[-2] )
  leptoquark_coupling = sample_name_list[-3]
  coupling_value  = float( sample_name_list[-1].replace('p','.') )
  return leptoquark_mass, leptoquark_coupling, coupling_value

leptoquark_mass, leptoquark_coupling, coupling_value = sample_name_to_parameters(sample_name)

gentype = 'LO_LQ_S1'
decaytype = leptoquark_coupling

param_card_old = None
if leptoquark_coupling == 'lam1122':
  param_card_old = 'MadGraph_param_card_SM.'+gentype+"."+leptoquark_coupling+'.dat' #old convention: 'param_card.SM.'+gentype+'.'+lambda_coupling+'.dat'
  masses['9000005'] = leptoquark_mass #S1
  leptoquark_width = get_widths(leptoquark_coupling, coupling_value, leptoquark_mass)
  decays=({'9000005':"""DECAY 9000005  %g #leptoquark decay""" % leptoquark_width})
else:
  raise NotImplementedError('I do not recognise the coupling requested: %s' % leptoquark_coupling)

fcard = open('proc_card_mg5.dat', 'w')
fcard.write("""
  import model LO_LQ_S1
  define lepton = l+ l-
  define top = t t~
  define charm = c c~
  define up = u u~
  define e = e- e+
  define mu = mu- mu+\n""")
#'''

if leptoquark_coupling == 'lam1122':
  fcard.write("""
  generate    g g > e mu charm up 
  output -f \n""")
  fcard.close()
  # Set the matching scale. By default this is MG ktdurham cut and Pythia8 CKKW-L TMS.
  xqcut = .25*( masses['9000005']+top_mass )
else:
  raise NotImplementedError('I do not recognise the coupling requested: %s' % leptoquark_coupling)

njets =1 

process_dir = new_process()
extras = {'pdlabel': "'lhapdf'", 'lhaid': 260000, 'ktdurham': leptoquark_mass * 0.25} 
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', nevts=runArgs.maxEvents*1.1,rand_seed=runArgs.randomSeed,beamEnergy=0.5*runArgs.ecmEnergy,extras=extras)

evgenLog.info('Registered generation of run number '+str(runArgs.runNumber))
build_param_card(param_card_old=param_card_old, param_card_new='MadGraph_param_card_SM.'+sample_name+'.dat', masses=masses, decays=decays, extras = {} )

runName = 'run_01'
process_dir = new_process()
generate(run_card_loc='run_card.dat',
         param_card_loc='MadGraph_param_card_SM.'+sample_name+'.dat',
         mode=0,
         proc_dir=process_dir,
         run_name=runName)


keepOutput=False ### remove me for ATLAS production!
evgenConfig.description = 'Single Leptoquark coupling %s with value %f. m_S1 = %s GeV' % (leptoquark_coupling, coupling_value, leptoquark_mass)
evgenConfig.contact  = [ "Holly Ann Pacey <holly.ann.pacey@cern.ch>" ]
evgenConfig.keywords += ['BSM','exotic', 'scalar', 'leptoquark']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile = runName + '._00001.events.tar.gz'
arrange_output(run_name=runName, proc_dir=process_dir, outputDS=runName + '._00001.events.tar.gz', lhe_version=3, saveProcDir=True)

if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = pp>LEPTONSj" ]


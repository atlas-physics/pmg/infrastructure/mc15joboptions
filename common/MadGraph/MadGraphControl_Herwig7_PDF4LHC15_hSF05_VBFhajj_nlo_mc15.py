from MadGraphControl.MadGraphUtils import *
import os
import fileinput
import re

qCut=25

#---------------------------------------------------------------------------
# Process Metadata:
#---------------------------------------------------------------------------
evgenConfig.keywords = ['Higgs','photon','bottom','VBFHiggs']
evgenConfig.contact = ['Hava Schwartz <Hava.Rhian.Schwartz@cern.ch>']
name='MG5_aMCatNLO262.346965.VBFhajj_PDF4LHC_13TeV'
evgenConfig.generators  += ["aMcAtNlo","Herwig7"]
evgenConfig.description = 'MadGraph_higgs_NLO_Herwigpp_PSVariation'
evgenConfig.keywords+=['jets']
evgenConfig.tune =  "H7.1-Default"
#evgenConfig.nEventsPerJob = 10000
#evgenConfig.inputFilesPerJob = 1

#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME
from Herwig7_i.Herwig7ConfigLHEF import Hw7ConfigLHEF

#include("Herwig7_i/Herwig72_LHEF.py")

genSeq += Herwig7()

Herwig7Config = Hw7ConfigLHEF(genSeq,runArgs)
HERWIGPP_qCut=qCut 
HERWIGPP_nJetMax=1 
runArgs.inputGeneratorFile=runArgs.inputGeneratorFile.split('.tar.gz.1')[0]+'.events'
#print(runArgs.inputGeneratorFile)

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="PDF4LHC15_nlo_mc_pdfas")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO") 


# add EvtGen
include("MC15JobOptions/Herwig71_EvtGen.py")

if "HERWIG7VER" in os.environ:
   version = os.getenv("HERWIG7VER")
   verh7 = version.split(".")[1]
else:
   verh7 = 0

if int(verh7 == 0):
#   Herwig7Config.add_commands("""
# insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
#set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV   
    Herwig7Config.add_commands("""
 do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
 do /Herwig/Particles/h0:PrintDecayModes
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 #### Hard Scale Factor Variation
 set /Herwig/Shower/ShowerHandler:HardScaleFactor 0.5
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 
 """)

else:
#   Herwig7Config.add_commands("""
   Herwig7Config.add_commands("""
## ------------------
## Hard process setup
## ------------------
 do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
 do /Herwig/Particles/h0:PrintDecayModes
 set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
 set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
 #### Hard Scale Factor Variation
 set /Herwig/Shower/ShowerHandler:HardScaleFactor 0.5
 cd /Herwig/Shower
 library FxFxHandler.so
 create Herwig::FxFxHandler FxFxHandler
 set /Herwig/Shower/FxFxHandler:ShowerModel /Herwig/Shower/ShowerModel
 set /Herwig/Shower/FxFxHandler:SplittingGenerator /Herwig/Shower/SplittingGenerator
 set /Herwig/Shower/FxFxHandler:HardProcessDetection Automatic
 #set /Herwig/Shower/FxFxHandler:ihrd 3
 #set /Herwig/Shower/FxFxHandler:njetsmax 3
 #set /Herwig/Shower/FxFxHandler:HeavyQVeto Yes
 #set /Herwig/Shower/FxFxHandler:VetoingIsOn
 #set /Herwig/Shower/FxFxHandler:MergeMode FxFx #merging mode
 #set /Herwig/Shower/FxFxHandler:ETClus 15.0*GeV #Merging scale
 #set /Herwig/Shower/FxFxHandler:RClus .4 #jet radius used in clustering merging
 #set /Herwig/Shower/FxFxHandler:EtaClusMax 10.0 #max eta for jets in clustering in merging
 #set /Herwig/Shower/FxFxHandler:RClusFactor 1.5 #default 1.5 factor to decide if a jet matches a parton in merging: if DR(parton,jet)<rclusfactor*rclus the parton and jet are said to have been matched
 """)


# run Herwig7
Herwig7Config.run()



from MadGraphControl.MadGraphUtils import *
import fileinput
import shutil
import subprocess
import os

mode=0
nJobs=1
gridpack_mode=True
gridpack_dir='madevent/'
cluster_type=None 
cluster_queue=None 
cluster_nb_retry=5

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot                                                                                                                                                                                           
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = njobs
    nJobs=int(njobs)
    mode=2
    print opts

name = runArgs.jobConfig[0]
thisDSID = int(name.split(".")[1])
runName = str(thisDSID)

isHInv = [312243]
isPythia = [312243]

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=1.2*runArgs.maxEvents if runArgs.maxEvents>0 else 5500

if(thisDSID in isHInv):
   fcard = open('proc_card_mg5.dat','w')
   fcard.write(""" 
   set complex_mass_scheme
   import model loop_qcd_qed_sm_Gmu-atlas
   define p = g u c d s u~ c~ d~ s~ b b~
   define j = g u c d s u~ c~ d~ s~ b b~
   generate  p p > h a j j [QCD]
   output -f""")
   fcard.close()
else:
   print "Unknown DSID"
   raise RuntimeError("DSID not known")


name = "hajj"
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack="madevent/")

#os.system("cp FKS_params.dat "+process_dir+"/Cards/")
#os.system("cp setscales.f  "+process_dir+"/SubProcesses/setscales.f")

maxjetflavor = 5

parton_shower='PYTHIA8'

extras = { 'lhe_version'   :'3.0',
           'pdlabel'      : "'lhapdf'",
           'lhaid'         : 260000, # maybe we use 303400?
           'maxjetflavor'  : maxjetflavor,
           'parton_shower' : "'"+parton_shower+"'",
           'jetalgo'       : -1,
           'jetradius'     : 0.4,
           'ptj'           : 15.0,
           'etaj'          : -1,
           'ickkw'         : 0,
          'ptgmin'        : 10.0,
          'epsgamma'      :'0.1',
          'R0gamma'       :'0.1',
          'xn'            :'2',
          'isoEM'         :'True',
          'etagamma'      :'3.0',
           'bwcutoff'      :'15',
           'store_rwgt_info' : '.true.',
           'event_norm' : 'sum',
           'reweight_scale': '.true.',
           'reweight_PDF'  : '.true.',
           'dynamical_scale_choice': 10  
         }

madspin_card_loc=None
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

print_cards()
    
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,random_seed=runArgs.randomSeed,required_accuracy=0.001,nevents=nevents)

outputDS=arrange_output(run_name=runName, proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)

runArgs.inputGeneratorFile=outputDS

if (thisDSID in isPythia):

    #---------------------------------------------------------------------------------------------------
    # Pythia8 Showering with A14_NNPDF23LO
    #---------------------------------------------------------------------------------------------------
    include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    #include("MC15JobOptions/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py")
    include("MC15JobOptions/Pythia8_aMcAtNlo.py")

    #--------------------------------------------------------------
    # Higgs at Pythia8
    #--------------------------------------------------------------
    #genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 4' ]
    genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                                 '25:onIfMatch = 23 23',
                                 '23:onMode = off',    # decay of Z
                                 '23:mMin = 2.0',
                                 '23:onIfMatch = 12 12',
                                 '23:onIfMatch = 14 14',
                                 '23:onIfMatch = 16 16',
                             ]

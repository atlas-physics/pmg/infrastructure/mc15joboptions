include('MC15JobOptions/MadGraphControl_SimplifiedModelPreInclude.py')


### parse options from MC job-options filename
tokens = runArgs.jobConfig[0].replace(".py","").split('_')

gentype, decaytype = tokens[2], tokens[3]
mstop, mstau       = int(tokens[4]), int(tokens[5])

### production defaults
njets              = 2
madspindecays      = True
pythiadecays       = False
for arg in tokens[6:]:
  if arg == "noMadSpin":
    madspindecays = False
  elif arg == "PythiaDecay":
    madspindecays = False
    pythiadecays = True
  elif arg.startswith("njets"):
    njets = int(arg[5:])
    if njets > 2:
      raise RuntimeError("Cannot do more than 2 jets (%d requested)" % njets)


### SUSY masses
masses['1000006'] = mstop # stop
masses['1000015'] = mstau # stau
masses['1000022'] = 0.5   # "massless" neutralino 


### MadGraph commands
# the following lines tell Madgraph to generate stop-pairs with 0,1,2 extra jets.
process = "generate p p > t1 t1~ {decay} @1\n"
if njets > 0:
  process += "add process p p > t1 t1~ j {decay} @2\n"
if njets > 1:
  process += "add process p p > t1 t1~ j j {decay} @3\n"
if madspindecays or pythiadecays:
  process = process.format(decay="$ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~")
else:
  # do decay in MadGraph (takes very long, need to use gridpacks)
  process = process.format(decay=", (t1  > b ta1+ vt), (t1~ > b~ ta1- vt~) $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~")


### MadSpin card
if madspindecays:
  madspin_card='madspin_card_TT_stau.dat'
  card = """
set seed {randomSeed}
set max_weight_ps_point 400 # default
set spinmode none # necessary for 3-body decay!

decay t1 > b ta1+ vt
decay t1~ > b~ ta1- vt~

# also do stau decay in MadSpin
decay ta1+ > ta+ n1 
decay ta1- > ta- n1

# will run the actual code
launch
""".format(randomSeed = runArgs.randomSeed)
  with open(madspin_card, 'w') as outf:
    outf.write(card)


### evgenConfig
evgenConfig.contact  = [ "mann@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel', 'stop']
evgenConfig.description = 'stop direct pair production, stop->stau+nu+b, stau->tau+neutralino in simplified model, m_stop = %s GeV, m_stau = %s GeV, m_N1 = %s GeV, njets = %i.' % (masses['1000006'], masses['1000015'], masses['1000022'], njets)
if madspindecays:
  evgenConfig.description += " Uses MadSpin for stop and stau decays."
if pythiadecays:
  evgenConfig.description += " Uses Pythia for all decays."
#evgenConfig.inputconfcheck = "MadGraph_NNPDF23LO_TT_stau_%03i_%03i" % (mstop, mstau)


### post-include stuff
if njets > 0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"] # do not need Pythia's guess option (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure#AGENE_1511_Pythia_guess_Option_f)

# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure#PDG_merging_cuts_in_MG_2_6_2
extras['pdgs_for_merging_cut']='1, 2, 3, 4, 21, 1000001, 1000002, 1000003, 1000004, 1000021, 2000001, 2000002, 2000003, 2000004'

# last line
include('MC15JobOptions/MadGraphControl_SimplifiedModelPostInclude.py')

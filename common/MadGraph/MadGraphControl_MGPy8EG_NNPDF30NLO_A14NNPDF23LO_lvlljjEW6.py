from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.keywords = ['SM', 'diboson', 'WZ', 'electroweak', '3lepton', 'VBS']
evgenConfig.contact = ['carsten.bittrich@cern.ch']

gridpack_dir = 'madevent/'

# ---------------------------------------------------------------------------
# Process type based on runNumber:
# ---------------------------------------------------------------------------
if runArgs.runNumber == 364739:
    runName = 'lvlljjEW6_OFMinus'
    description = 'MadGraph_lvlljj_EW6_OFMinus_masslessLeptons'
    mgproc = """
generate  p p > mu+ mu- e- ve~ j j QCD=0 @0
add process p p > ta+ ta- e- ve~ j j QCD=0 @0
add process p p > e+ e- mu- vm~ j j QCD=0 @0
add process p p > ta+ ta- mu- vm~ j j QCD=0 @0
add process p p > e+ e- ta- vt~ j j QCD=0 @0
add process p p > mu+ mu- ta- vt~ j j QCD=0 @0"""
elif runArgs.runNumber == 364740:
    runName = 'lvlljjEW6_OFPlus'
    description = 'MadGraph_lvlljj_EW6_OFPlus_masslessLeptons'
    mgproc = """
generate  p p > mu+ mu- e+ ve j j QCD=0 @0
add process p p > ta+ ta- e+ ve j j QCD=0 @0
add process p p > e+ e- mu+ vm j j QCD=0 @0
add process p p > ta+ ta- mu+ vm j j QCD=0 @0
add process p p > e+ e- ta+ vt j j QCD=0 @0
add process p p > mu+ mu- ta+ vt j j QCD=0 @0"""
elif runArgs.runNumber == 364741:
    runName = 'lvlljjEW6_SFMinus'
    description = 'MadGraph_lvlljj_EW6_SFMinus_masslessLeptons'
    mgproc = """
generate p p > e+ e- e- ve~ j j QCD=0 @0
add process p p > mu+ mu- mu- vm~ j j QCD=0 @0
add process p p > ta+ ta- ta- vt~ j j QCD=0 @0"""
elif runArgs.runNumber == 364742:
    runName = 'lvlljjEW6_SFPlus'
    description = 'MadGraph_lvlljj_EW6_SFPlus_masslessLeptons'
    mgproc = """
generate p p > e+ e- e+ ve j j QCD=0 @0
add process p p > mu+ mu- mu+ vm j j QCD=0 @0
add process p p > ta+ ta- ta+ vt j j QCD=0 @0"""
    pass
else:
    raise RuntimeError(
        "runNumber %i not recognised in these jobOptions." % runArgs.runNumber)

evgenConfig.description = description

# ---------------------------------------------------------------------------
# write MG5 Proc card
# ---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat', 'w')
fcard.write("""
import model sm-no_masses
# massless b and massless taus
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
""" + mgproc + """
output -f
""")
fcard.close()

# ----------------------------------------------------------------------------
# Random Seed
# ----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs, 'randomSeed'):
    randomSeed = runArgs.randomSeed

# ----------------------------------------------------------------------------
# Beam energy
# ----------------------------------------------------------------------------
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

# ---------------------------------------------------------------------------
# Number of Events
# ---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs, 'maxEvents') and runArgs.maxEvents > 0:
    nevents = int(int(runArgs.maxEvents) * safefactor)
else:
    nevents = int(5000 * safefactor)

extras = {
    'pdlabel': "'lhapdf'",
    'lhaid': 260000,
    'ptl': 4.0,
    'ptj': 15.0,
    'ptb': 15.0,
    'ptheavy': 2.0,
    'mmll': 4.0,
    'drll': 0.2,
    'drjl': 0.2,
    'drjj': 0.4,
    'dral': 0.1,
    'etaj': 5.5,
    'etal': 5.0,
    'maxjetflavor': 5,
    'asrwgtflavor': 5,
    'auto_ptj_mjj': False,
    'cut_decays': True,
    'use_syst': True,
    'event_norm': 'sum',
    'systematics_program': 'systematics',
    'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo,MMHT2014nlo68clas118,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(grid_pack=gridpack_dir)

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
               run_card_new='run_card.dat', xqcut=0,
               nevts=nevents, rand_seed=runArgs.randomSeed,
               beamEnergy=beamEnergy)
modify_run_card(run_card='run_card.dat',
                run_card_backup='run_card_backup.dat',
                settings=extras)
print_cards()

generate(required_accuracy=0.001, run_card_loc='run_card.dat',
         param_card_loc=None, mode=0, njobs=1, cluster_type=None,
         cluster_queue=None, proc_dir=process_dir, run_name=runName,
         grid_pack=True, gridpack_compile=False, gridpack_dir=gridpack_dir,
         nevents=nevents, random_seed=runArgs.randomSeed)

# replaced output_suffix+'._00001.events.tar.gz' with runArgs.outputTXTFile
arrange_output(run_name=runName, proc_dir=process_dir,
               outputDS=runArgs.outputTXTFile, lhe_version=3,
               saveProcDir=True)


runArgs.inputGeneratorFile = runArgs.outputTXTFile

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
# fix for VBS processes (requires version>=8.230)
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=on"]

include("MC15JobOptions/Pythia8_MadGraph.py")

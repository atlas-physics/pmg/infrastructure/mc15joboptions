import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------
# General Settings
#---------------------------------------------------------------------------
mode=1 # 0: single core; 1: cluster; 2: multicore
cluster_type="condor"
cluster_queue=None
nevents=runArgs.maxEvents*1.1
njobs=14
if mode!=2: njobs=1

gridpack_mode=True
gridpack_dir='madevent/'


#---------------------------------------------------------------------------
# Process type based on runNumber:
#---------------------------------------------------------------------------
if not hasattr(runArgs,'runNumber'):
    raise RuntimeError("No run number found.")

model=""
process=""
name=""
description=""
keywords=[]

masses={'25': '1.250000e+02'}        ## Higgs mass
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width
extras_cuts = {} ## mass filters

# 4l, on-shell Z->ll
if runArgs.runNumber>=346884 and runArgs.runNumber<=346889:
    ## SBI, m4l < 130
    if runArgs.runNumber==346884:
        model="import model sm"
        process="generate p p > j j l+ l- l+ l- QCD=0 QED=6"
        name="VBF4l_SBI"
        description='qq->jjllll sample, off-shell Higgs included'
        keywords=['VBF','ZZ', '4lepton', 'Higgs']

        extras_cuts={'mmnlmax':"130",
                     'drll':"0.03",
        }
    ## SBI, m4l > 130
    if runArgs.runNumber==346885:
        model="import model sm"
        process="generate p p > j j l+ l- l+ l- QCD=0 QED=6"
        name="VBF4l_SBI"
        description='qq->jjllll sample, off-shell Higgs included'
        keywords=['VBF','ZZ', '4lepton', 'Higgs']

        extras_cuts={'mmnl':"130",
                     'drll':"0.03",
        }
    ## B, m4l > 100
    if runArgs.runNumber==346886:
        model="import model sm"
        process="generate p p > j j l+ l- l+ l- /h QCD=0 QED=6"
        name="VBF4l_B"
        description='qq->jjllll sample, off-shell Higgs excluded'
        keywords=['VBF','ZZ', '4lepton', 'Higgsless']

        extras_cuts={'mmnl':"100",
                     'drll':"0.03",
        }
    ## SBI5, m4l > 130
    if runArgs.runNumber==346887:
        model="import model sm\nimport model sm_5hw"
        process="generate p p > j j l+ l- l+ l- QCD=0 QED=6"
        name="VBF4l_SBI5"
        description='qq->jjllll sample, off-shell Higgs included, 5 times SM Higgs total width'
        keywords=['VBF','ZZ', '4lepton', 'BSMHiggs']

        masses={'25': '1.250000e+02'}        ## Higgs mass
        decays={'25': 'DECAY  25 2.035e-02'} ## Higgs width, x5
        extras_cuts={'mmnl':"130",
                     'drll':"0.03",
        }
    ## SBI10, m4l > 130
    if runArgs.runNumber==346888:
        model="import model sm\nimport model sm_10hw"
        process="generate p p > j j l+ l- l+ l- QCD=0 QED=6"
        name="VBF4l_SBI10"
        description='qq->jjllll sample, off-shell Higgs included, 10 times SM Higgs total width'
        keywords=['VBF','ZZ', '4lepton', 'BSMHiggs']

        masses={'25': '1.250000e+02'}        ## Higgs mass
        decays={'25': 'DECAY  25 4.07e-02'}  ## Higgs width, x10
        extras_cuts={'mmnl':"130",
                     'drll':"0.03",
        }
    ## S, m4l > 130
    if runArgs.runNumber==346889:
        model="import model sm"
        process="generate p p > h > j j l+ l- l+ l- QCD=0 QED=6"
        name="VBF4l_S"
        description='qq->h>jjllll sample, off-shell Higgs included'
        keywords=['VBF','ZZ', '4lepton', 'Higgs']

        extras_cuts={'mmnl':"130",
                     'drll':"0.03",
        }
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)

#---------------------------------------------------------------------------
# MG5 Proc card, based on runNumber
#---------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
"""+model+"""
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
#define za = z a
define za = z
"""+process+"""
output -f
""")
fcard.close()

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safefactor
else: nevents = nevents*safefactor

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'asrwgtflavor':"5",
    'lhe_version':"3.0",
    'ptj':"5",
    'ptb':"5",
    'pta':"0",
    'ptl':"3",
    'etaj':"6.5",
    'etab':"6.5",
    'etal':"3.0",
    'drjj':"0",
    'draa':"0",
    'draj':"0",
    'drjl':"0",
    'dral':"0",
    'mmjj':"10",
    'mmbb':"10",
    'maxjetflavor':"5" ,
    'cut_decays'  :'T',
    'auto_ptj_mjj': 'F',
}
extras.update(extras_cuts)

if gridpack_mode:
  process_dir = new_process(card_loc='proc_card_mg5.dat',grid_pack=gridpack_dir)
else:
  process_dir = new_process(card_loc='proc_card_mg5.dat')

build_run_card(run_card_old=get_default_runcard(process_dir),
               run_card_new='run_card.dat',
               nevts=nevents,
               rand_seed=randomSeed,
               beamEnergy=beamEnergy,
               extras=extras)


#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------
str_param_card='param_card_new.dat'
build_param_card(param_card_old=os.path.join(process_dir,'Cards/param_card.dat'),
   param_card_new=str_param_card,
   masses=masses,decays=decays)

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
if gridpack_mode:
  if mode==1:
    generate(run_card_loc='run_card.dat',param_card_loc=str_param_card,mode=mode,proc_dir=process_dir,run_name=name,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=randomSeed,cluster_type=cluster_type,cluster_queue=cluster_queue)
  else:
    generate(run_card_loc='run_card.dat',param_card_loc=str_param_card,mode=mode,njobs=njobs,proc_dir=process_dir,run_name=name,grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=randomSeed)
else:
  generate(run_card_loc='run_card.dat',param_card_loc=str_param_card,mode=mode,proc_dir=process_dir,run_name=name)


#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
arrange_output(run_name=name,
               proc_dir=process_dir,
               outputDS=stringy+'._00001.events.tar.gz',
               saveProcDir=True,
               )

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

#### Shower

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_MadGraph.py")

# to turn on the dipole shower
genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = description
evgenConfig.keywords+=keywords
evgenConfig.contact = ['Lailin Xu <lailin.xu@cern.ch>', 'Martina Javurkova <Martina.Pagacova@cern.ch>']

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

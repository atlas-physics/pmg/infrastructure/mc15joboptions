#--------------------------------------------------------------                                                                                                                                                          
# EVGEN configuration                                                                                                                                                                                                    
#--------------------------------------------------------------                                                                                                                                                          
evgenConfig.description = 'MG5_aMC@NLO+Herwig7.1.3 ttbar production EvtGen from DSID 412121 LHE files with HT filter and H7p1 default tune, ME NNPDF 3.0 NLO'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'nedaa.asbah@cern.ch' ]
evgenConfig.generators += ["aMcAtNlo","Herwig7"]
evgenConfig.tune         = "H7.1-Default"

# initialize Herwig7 generator configuration for showering of LHE files                                                                                               
include("MC15JobOptions/Herwig7_LHEF.py")

# configure Herwig7 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

#include("MC15JobOptions/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen                                                                                                                                                                                                                
include("MC15JobOptions/Herwig71_EvtGen.py")

# run Herwig7                                                                                                                                                                                                          
Herwig7Config.run()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------

include("MC15JobOptions/TTbarWToLeptonFilter.py")
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1 #(-1: non-all had, 0: all had, 1: l+jets, 2: dilepton)
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0

# Depending on the DSID, set up MET/HT filters
thisDSID = runArgs.runNumber
HT1k5_filter    = [412182]
HT1k_1k5_filter = [412183]
HT6c_1k_filter  = [412184]

if thisDSID in HT6c_1k_filter:
    include('MC15JobOptions/HTFilter.py')
    filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
    filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
    filtSeq.HTFilter.MinHT = 600.*GeV # Min HT to keep event
    filtSeq.HTFilter.MaxHT = 1000.*GeV # Max HT to keep event
    filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
    filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
    filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT

if thisDSID in HT1k_1k5_filter:
    include('MC15JobOptions/HTFilter.py')
    filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
    filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
    filtSeq.HTFilter.MinHT = 1000.*GeV # Min HT to keep event
    filtSeq.HTFilter.MaxHT = 1500.*GeV # Max HT to keep event
    filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
    filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
    filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT  

if thisDSID in HT1k5_filter:
    include('MC15JobOptions/HTFilter.py')
    filtSeq.HTFilter.MinJetPt = 35.*GeV # Min pT to consider jet in HT
    filtSeq.HTFilter.MaxJetEta = 2.5 # Max eta to consider jet in HT
    filtSeq.HTFilter.MinHT = 1500.*GeV # Min HT to keep event
    filtSeq.HTFilter.MaxHT = 9000.*GeV # Max HT to keep event
    filtSeq.HTFilter.UseLeptonsFromWZTau = True # Include e/mu from the MC event in the HT
    filtSeq.HTFilter.MinLeptonPt = 25.*GeV # Min pT to consider muon in HT
    filtSeq.HTFilter.MaxLeptonEta = 2.5 # Max eta to consider muon in HT


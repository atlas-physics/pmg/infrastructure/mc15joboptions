from MadGraphControl.MadGraphUtils import *

######
## number of events to generate + safety margin
nevents=1.1*runArgs.maxEvents
runName='run_01'
gridpack_dir='madevent/'
gridpack_mode=False
mode=2 #mode=0 for single-core run, mode=1 if access to acluster, mode=2 for multicore production
njobs=132
cluster_type='lsf'
cluster_queue='8nh'

######

## map DSID to process settings
# select diagram removal scheme: DR1 or DR2?
tWH_DR1 = []
tWH_DR2 = [ 346486, 346511, 346545, 346546, 346547, 346548, 346549, 346550, 346589, 346590, 346591, 346592, 346593, 346594, 346678, 346759, 346760, 346761, 346762, 346763, 346883, 346768, 346769, 346770, 346771 ]

# select any BSM top Yukawa couplings (default: SM):
tWH_CPalpha_0  = [ 346678, 346759, 346883]
tWH_CPalpha_15 = [ 346545, 346589]
tWH_CPalpha_30 = [ 346546, 346590]
tWH_CPalpha_45 = [ 346547, 346591]
tWH_CPalpha_60 = [ 346548, 346592]
tWH_CPalpha_75 = [ 346549, 346593]
tWH_CPalpha_90 = [ 346550, 346594]

tWH_yt_minus1 = []
tWH_yt_plus2 = []

tWH_yt_minus1_CPalpha_0 = [346760, 346768]
tWH_yt_plus0p5_CPalpha_0 = [346761, 346769]
tWH_yt_plus2_CPalpha_0 = [346762, 346770]
tWH_yt_plus2_CPalpha_45 = [346763, 346771]

# set shower subtraction terms: defaul: Pythia8, this array H7
tWH_H7 = [346883]
# in case of H7, we also set the Higgs decay channel to H7...
# diphotons:
tWH_H7_yy = [346883]

parton_shower='PYTHIA8'
if runArgs.runNumber in tWH_H7:
    parton_shower='HERWIGPP'

tWH_BSM = tWH_yt_minus1+tWH_yt_plus2+tWH_CPalpha_0+tWH_CPalpha_15+tWH_CPalpha_30+tWH_CPalpha_45+tWH_CPalpha_60+tWH_CPalpha_75+tWH_CPalpha_90+tWH_yt_minus1_CPalpha_0+tWH_yt_plus0p5_CPalpha_0+tWH_yt_plus2_CPalpha_0+tWH_yt_plus2_CPalpha_45

DR_mode=''
if runArgs.runNumber in tWH_DR1:
    DR_mode='DR1'
elif runArgs.runNumber in tWH_DR2:
    DR_mode='DR2'
else:
    raise RuntimeError("runNumber %i not recognised in these jobOptions."%runArgs.runNumber)


######
mgproc="generate p p > t w- x0 [QCD]"
mgprocadd="add process p p > t~ w+ x0 [QCD]"
name='tWH'
process="pp>tWH"
topdecay='''decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all'''
gridpack_mode=False#MARIA
gridpack_dir='madevent/'
    
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model HC_NLO_X0_UFO-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define q = u c d s b t
define q~ = u~ c~ d~ s~ b~ t~
"""+mgproc+"""
"""+mgprocadd+"""
output -f
""")
fcard.close()
    
extras = {'pdlabel'        : "'lhapdf'",
          'lhaid'          : 260000,
          'parton_shower'  : parton_shower,
          'reweight_scale' : 'True',
          'reweight_PDF'   : 'True',
          'PDF_set_min'    : 260001,
          'PDF_set_max'    : 260100,
          'bwcutoff'       : 50.,
          'fixed_ren_scale' : "False",
	  'fixed_fac_scale' : "False",
          'dynamical_scale_choice' : 3 }


parameters_cosa_1={
    'frblock':{
        'Lambda':'1.000000e+03',
        'cosa':  '1.000000e+00',
        'kSM':   '1.000000e+00',
        'kHtt':  '1.000000e+00',
        'kAtt':  '0.000000e+00'}
}

parameters = parameters_cosa_1

runName = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(grid_pack=gridpack_dir)


# fetch helpers required by do_+DR_mode+.py
DR_helper = 'DR_functions.py'
subprocess.Popen(['get_files','-data',DR_helper]).communicate()
if not os.access(DR_helper, os.R_OK):
    raise RuntimeError("Could not get_file required for automated DR removal: '%s"%DR_helper)



if DR_mode!='':
    include('MC15JobOptions/do_'+DR_mode+'.py')
else:
    raise RuntimeError("No DR mode found, this calculation will not converge!")

build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
               extras=extras)


######
## write madspin card
madspin_dir = 'my_madspin'
madspin_card_loc='madspin_card.dat'
if not hasattr(runArgs, 'inputGenConfFile'):
    fMadSpinCard = open('madspin_card.dat','w')
    fMadSpinCard.write('import Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+madspin_dir+'\n')
    fMadSpinCard.write('#set use_old_dir True\n')
else:
    os.unlink(gridpack_dir+'Cards/madspin_card.dat')
    fMadSpinCard = open(gridpack_dir+'Cards/madspin_card.dat','w')
    fMadSpinCard.write('import '+gridpack_dir+'Events/'+runName+'/events.lhe.gz\n')
    fMadSpinCard.write('set ms_dir '+gridpack_dir+'MadSpin\n')
    fMadSpinCard.write('set ms_dir '+madspin_dir+'\n')
    fMadSpinCard.write('set seed '+str(10000000+int(runArgs.randomSeed))+'\n')
# for these numbers I get negligible(<1) amount of events above weights / 10k decayed events
fMadSpinCard.write('''set Nevents_for_max_weigth 2000 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 500  # number of PS to estimate the maximum for each event (default: 400)
'''+topdecay+'''
launch''')
fMadSpinCard.close()
    


######
## select param card; default/None = SM
#param_card_loc=None
build_param_card(param_card_old=process_dir+'/Cards/param_card.dat',param_card_new='param_card_new.dat',params=parameters)

if runArgs.runNumber not in tWH_BSM:
    generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,madspin_card_loc=madspin_card_loc,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001)

# run with modified param card
else:

    # BSM case: run with modified param card
    mod_paramcard_name = ''
    if runArgs.runNumber in tWH_yt_minus1:
        mod_paramcard_name = 'param_card_yt_minus1.dat'
    elif runArgs.runNumber in tWH_yt_plus2:
        mod_paramcard_name = 'param_card_yt_plus2.dat'
    elif runArgs.runNumber in tWH_CPalpha_0:
        mod_paramcard_name = 'param_card_CPalpha_0.dat'
    elif runArgs.runNumber in tWH_CPalpha_15:
        mod_paramcard_name = 'param_card_CPalpha_15.dat'
    elif runArgs.runNumber in tWH_CPalpha_30:
        mod_paramcard_name = 'param_card_CPalpha_30.dat'
    elif runArgs.runNumber in tWH_CPalpha_45:
        mod_paramcard_name = 'param_card_CPalpha_45.dat'
    elif runArgs.runNumber in tWH_CPalpha_60:
        mod_paramcard_name = 'param_card_CPalpha_60.dat'
    elif runArgs.runNumber in tWH_CPalpha_75:
        mod_paramcard_name = 'param_card_CPalpha_75.dat'
    elif runArgs.runNumber in tWH_CPalpha_90:
        mod_paramcard_name = 'param_card_CPalpha_90.dat'
    elif runArgs.runNumber in tWH_yt_minus1_CPalpha_0:
        mod_paramcard_name = 'param_card_yt_minus1_CPalpha_0.dat'
    elif runArgs.runNumber in tWH_yt_plus0p5_CPalpha_0:
        mod_paramcard_name = 'param_card_yt_plus0p5_CPalpha_0.dat'
    elif runArgs.runNumber in tWH_yt_plus2_CPalpha_0:
        mod_paramcard_name = 'param_card_yt_plus2_CPalpha_0.dat'
    elif runArgs.runNumber in tWH_yt_plus2_CPalpha_45:
        mod_paramcard_name = 'param_card_yt_plus2_CPalpha_45.dat'
    else:
        raise RuntimeError("No modified param card instuction found for %i ."%runArgs.runNumber)

    mod_paramcard = subprocess.Popen(['get_files','-data',mod_paramcard_name]).communicate()
    if not os.access(mod_paramcard_name, os.R_OK):
        print 'ERROR: Could not get param card'
        raise RuntimeError("parameter card '%s' missing!"%mod_paramcard_name)
    param_card_loc=mod_paramcard_name

    generate(run_card_loc='run_card.dat',param_card_loc=param_card_loc,mode=mode,njobs=njobs,proc_dir=process_dir,run_name=runName,
         madspin_card_loc=madspin_card_loc,nevents=nevents,random_seed=runArgs.randomSeed,required_accuracy=0.001)



###### 
#MadSpin DR hack: needed to properly handle diagram removal in madspin 
msfile=process_dir+'/Cards/madspin_card.dat'
mstilde=process_dir+'/Cards/madspin_card.dat~'
shutil.copyfile(msfile,mstilde)
with open(msfile,"w") as myfile, open(mstilde,'r') as f:
    for line in f:
        if '#set use_old_dir: True' in line:
            line = line.replace('#',' ')
        myfile.write(line)
os.remove(mstilde)


MadSpin_DR_hack = 'do_MadSpin_'+DR_mode+'.py'
subprocess.Popen(['get_files','-data',MadSpin_DR_hack]).communicate()
if not os.access(MadSpin_DR_hack, os.R_OK):
    raise RuntimeError("Could not get_file required for MadSpin DR hack: '%s"%MadSpin_DR_hack)

full_madspin_dir = process_dir+'/'+madspin_dir
do_hack_MS = subprocess.Popen(['python', MadSpin_DR_hack, full_madspin_dir], stdout=subprocess.PIPE, cwd=".")
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen(["make", "clean"], stdout=subprocess.PIPE, cwd=process_dir)
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The first compilation attempt fails, with errors written to the log
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen("make", stdout=subprocess.PIPE, cwd=process_dir) #The second compilation attempt works, producing a new tarfile in the process directory
do_hack_MS.communicate()
do_hack_MS=subprocess.Popen(['bin/aMCatNLO', 'decay_events', runName, '-f'], stdout=subprocess.PIPE, cwd=process_dir)
do_hack_MS.communicate()



###### 
outputDS=arrange_output(run_name=runName,proc_dir=process_dir,lhe_version=3,saveProcDir=True)



###### 
evgenConfig.description = 'aMcAtNlo tHW'
evgenConfig.keywords+=['Higgs', 'tHiggs']
evgenConfig.contact = ['maria.moreno.llacer@cern.ch']
# running on the fly, name depends on MGUtils, so no check for particular pattern here:
evgenConfig.inputfilecheck = ''
runArgs.inputGeneratorFile=outputDS


###### 
## shower settings:
## make sure the 'parton_shower' argument to MG is consistent with shower you use
if not runArgs.runNumber in tWH_H7:
    include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("MC15JobOptions/Pythia8_aMcAtNlo.py") 
    include("MC15JobOptions/Pythia8_SMHiggs125_inc.py")
    ## don't include shower weights, see ATLMCPROD-6135
    #include("MC15JobOptions/Pythia8_ShowerWeights.py")
elif runArgs.runNumber in tWH_H7:    
    evgenConfig.generators += ["aMcAtNlo", "Herwig7"]
    evgenConfig.description = 'MG5_aMC@NLO+Herwig7+EvtGen '+name+' OTF, H7p1 default tune, ME NNPDF 3.0 NLO'
    evgenConfig.tune = "H7.1-Default"
    evgenConfig.contact += ['liza.mijovic@cern.ch']
    include("MC15JobOptions/Herwig7_LHEF.py")
    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
    include("MC15JobOptions/Herwig71_EvtGen.py")
    if runArgs.runNumber in tWH_H7_yy:
        # only consider H->gamgam decays
        Herwig7Config.add_commands("""
        # force H->gamgam decays
        do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;
        # print out Higgs decays modes and branching ratios to log.generate
        do /Herwig/Particles/h0:PrintDecayModes
        """)
    # common to all tWH_H7
    Herwig7Config.run()    
else:
    theApp.finalize()
    theApp.exit()


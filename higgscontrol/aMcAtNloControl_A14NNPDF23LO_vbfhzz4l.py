#---------------------------------------------------------------------------------------------------
# parsing the Generate_tf.py arguments
#---------------------------------------------------------------------------------------------------

def parseargs(runArgs,safefactor=1.1,nevents=20000):
    options = {}
    if not hasattr(runArgs,'runNumber'):
        raise RuntimeError("no runNumber supplied as argument")
    else: 
        options["runNumber"] = runArgs.runNumber

    if hasattr(runArgs,'ecmEnergy'):
        options["beamEnergy"] = runArgs.ecmEnergy / 2.
    else: 
        raise RuntimeError("No center of mass energy found.")

    if hasattr(runArgs,"maxEvents") and runArgs.maxEvents > 0:
        options["nevents"]=runArgs.maxEvents*safefactor
    else:
        options["nevents"]=nevents*safefactor

    if hasattr(runArgs,"randomSeed"):
        options["randomSeed"] = runArgs.randomSeed
    else:
        raise RuntimeError("No random seed provided.")

    if hasattr(runArgs,"outputTXTFile"):
        options["runName"]=runArgs.outputTXTFile
    else:
        options["runName"]='run_01'

    if os.environ.get("GRIDPACK_DIR"):
        options["gridpack_mode"]=True
        options["gridpack_dir"]=os.environ.get("GRIDPACK_DIR")
    else:
        options["gridpack_mode"]=False
        options["gridpack_dir"]=None


    options["mode"] = 0

    runArgs.inputGeneratorFile=options["runName"]+'._00001.events.tar.gz'
    options["ingenfile"] = runArgs.inputGeneratorFile

    return options

options = parseargs(safefactor=1.5,runArgs=runArgs)


if options["runNumber"] not in range(345355,345375):
  raise RuntimeError("runNumber {0:d} not recognised in these jobOptions. Available options are: 100001".format(options["runNumber"]))




#---------------------------------------------------------------------------------------------------
# defining the dsid- eft parameters correspondence
#---------------------------------------------------------------------------------------------------
parameters=[1.0,1.0,0.0,0.0]
#ksm, cos, khvv, kAvv

if options["runNumber"] == 345355 : 
    parameters=[1.0,1.0,0.0,0.0]
if options["runNumber"] == 345356 : 
    parameters=[1.0,1.0,5.0,0.0]
if options["runNumber"] == 345357 :
    parameters=[1.0,1.0,-5.0,0.0]
if options["runNumber"] == 345358 :
    parameters=[1.0,1.0,2.5,0.0]
if options["runNumber"] == 345359 :
    parameters=[1.0,1.0,-2.5,0.0]
if options["runNumber"] == 345360 :
    parameters=[0.0,1.0,10.0,0.0]
if options["runNumber"] == 345361 :
    parameters=[1.0,0.7071,0.0,5.0]
if options["runNumber"] == 345362 :
    parameters=[1.0,0.7071,0.0,-5.0]
if options["runNumber"] == 345363 :
    parameters=[1.0,0.7071,0.0,2.5]
if options["runNumber"] == 345364 :
    parameters=[1.0,0.7071,0.0,-2.5]
if options["runNumber"] == 345365 :
    parameters=[0.0,0.7071,0.0,15.0]

# for 2D
if options["runNumber"] == 345366 :
    parameters=[1.0,0.7071,2.5,-5.0]
if options["runNumber"] == 345367 :
    parameters=[1.0,0.7071,-2.5,-5.0]
if options["runNumber"] == 345368 :
    parameters=[1.0,0.7071,-5.0,5.0]
if options["runNumber"] == 345369 :
    parameters=[1.0,0.7071,-5.0,-6.0]
if options["runNumber"] == 345370 :
    parameters=[1.0,0.7071,5.0,6.0]
if options["runNumber"] == 345371 :
    parameters=[1.0,0.7071,5.0,5.0]

# for 2D, additional samples
if options["runNumber"] == 345372 :
    parameters=[1.0,0.7071,5.0,10.0]
if options["runNumber"] == 345373 :
    parameters=[1.0,0.7071,-10.0,10.0]
if options["runNumber"] == 345374 :
    parameters=[1.0,0.7071,10.0,-10.0]


######################################################################
# Helper functions to work with files
######################################################################

from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)

######################################################################




#---------------------------------------------------------------------------------------------------
# generating process cards in HC_NLO_X0_UFO model
#---------------------------------------------------------------------------------------------------

def writeproccard(content):
    fcard = open('proc_card_mg5.dat','w')
    fcard.write(content)
    fcard.close()


#---------------------------------------------------------------------------------------------------
# generating vhzz4l
#---------------------------------------------------------------------------------------------------

writeproccard(content="""import model HC_NLO_X0_UFO
    define p,j = g u c d s u~ c~ d~ s~
    define v = z w+ w-
    generate p p > j j x0 $$ v /a [QCD]
output PROC_vhvbfhzz4l""")


#---------------------------------------------------------------------------------------------------
# Setting EFT parameters in HC model
#---------------------------------------------------------------------------------------------------

def writeparamcard(options,masses,parameters):
    from MadGraphControl.MadGraphUtils import new_process
    process_dir = new_process(grid_pack=options["gridpack_dir"])

    # Bypass pole checking..
    replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
    replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
    replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")

    options["processdir"] = process_dir
    
    from MadGraphControl.MadGraphUtils import build_param_card
    from os.path import join as pathjoin
    build_param_card(param_card_old=pathjoin(options["processdir"],'Cards/param_card.dat'),param_card_new='param_card_new.dat',
                     masses=masses,params=parameters)


writeparamcard(masses={'25': '1.250000e+02'},options=options,
    parameters={'frblock':
            {'kAbb': 0.0,
            'kHaa': 0.0,
            'kAaa': 0.0,
            'cosa': parameters[1],
            'kHdwR': 0.0,
            'kAza': 0.0,
            'kAll': 0.0,
            'kHbb': 0.0,
            'kHll': 0.0,
            'kAzz': parameters[3],
            'kSM': parameters[0],
            'kHdwI': 0.0,
            'kHdz': 0.0,
            'kAww': parameters[3],
            'kAtt': 0.0,
            'kHda': 0.0,
            'kHza': 0.0,
            'kHtt': 0.0,
            'kHww': parameters[2],
            'kHzz': parameters[2],
            'Lambda': 1000.0}})



#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
#---------------------------------------------------------------------------------------------------

def writeruncard(options,extras):
    from MadGraphControl.MadGraphUtils import build_run_card,get_default_runcard
    rand_seed_tmp=int(options["randomSeed"])% (30081*30081) 
    build_run_card(run_card_old=get_default_runcard(options["processdir"]),run_card_new='run_card.dat',
                   nevts=options["nevents"],rand_seed=rand_seed_tmp,
                   beamEnergy=options["beamEnergy"],extras=extras,xqcut=0.)


#---------------------------------------------------------------------------------------------------
# creating run_card.dat for vhzz4l
#---------------------------------------------------------------------------------------------------


writeruncard(
    extras = {'lhe_version'   : '2.0',
          'pdlabel'       : "'lhapdf'",
          'lhaid'         : 260000,
          'parton_shower' :'PYTHIA8',
          'reweight_scale': 'True',
          'reweight_PDF'  : 'True',
          'PDF_set_min'   : 260001,
          'PDF_set_max'   : 260100},
    options=options)


#
# creating mad spin card
#

madspin_card_loc='madspin_card.dat'                                                                                                                                    

mscard = open(madspin_card_loc,'w')                                                                                                                                    
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
set spinmode none
define l+ = e+ mu+
define l- = e- mu-
decay x0 > l+ l- l+ l-
define v = z w+ w-
decay v > j j
# running the actual code
launch"""%runArgs.randomSeed)                                                                                                                                              
mscard.close()



#---------------------------------------------------------------------------------------------------
# Calling Madgraph
#---------------------------------------------------------------------------------------------------

def callgenerate(options):
    from MadGraphControl.MadGraphUtils import print_cards,generate,arrange_output
#    import shutil
    print_cards()
    generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=options["mode"],proc_dir=options["processdir"],run_name=options["runName"],madspin_card_loc=madspin_card_loc,grid_pack=options["gridpack_mode"],gridpack_dir=options["gridpack_dir"])    
#    fname_in = options["processdir"]+"/Events/"+options["runName"]+"/unweighted_events.lhe"
#    fname_tmp = options["processdir"]+"/Events/"+options["runName"]+"/tmp.lhe"
#    shutil.move(fname_in,fname_tmp)
    arrange_output(run_name=options["runName"],proc_dir=options["processdir"],outputDS=options["runName"]+'._00001.events.tar.gz',lhe_version=3,saveProcDir=True)
    
callgenerate(options=options)

if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_aMcAtNlo.py")


evgenConfig.keywords = ['4lepton','4electron','4muon', 'BSM', 'Higgs', 'VBF', 'VBFHiggs', 'mH125', 'BSMHiggs', 'dijet', 'resonance', 'ZZ']
evgenConfig.contact = ['Karolos Potamianos <Karolos.Potamianos@cern.ch>']
evgenConfig.generators = ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.description = """NLO VBF 125 GeV Higgs production in the Higgs Characterization model decaying to zz4l."""
evgenConfig.inputfilecheck = options['runName']


